package manualTests;

import android.database.Cursor;

import com.mishuto.todo.App;
import com.mishuto.todo.common.TObjects;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.database.DBField;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.DBOpenHelper;
import com.mishuto.todo.database.SQLTable;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Test;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import dalvik.system.DexFile;

import static org.junit.Assert.assertFalse;

/**
 * Проверка целостности связи Модель - БД
 * Используется для проверки релизов, в которых изменилась структура данных.
 * Использование:
 * 1. Сделать чекаут старого релиза. Удалить приложение в эмуляторе
 * 2. Запустить тест в старом релизе - в БД создадутся таблицы всех объектов.
 * 3. Сделать чекаут в новом релизе
 * 4. Запустить тест в новом релизе - будет проведена проверка соответствия корректности и количества столбцов в каждой таблице, а так же, соответствие количества таблиц и объектов
 * Это утилита, а не тест, поэтому он не запускается в общем списке тестов
 *
 * Created by Michael Shishkin on 11.10.2017.
 */

public class CheckIntegrity {

    private boolean mErrorExists = false;

    @SuppressWarnings("unchecked")
    @Test
    public void checkIntegrity() throws Exception {

        final String [] indirectlyTables = {"ARRAYS", "ARRAY_ITEMS", "COLLECTIONS", "COLLECTION_ITEMS", "DELAYER_STAT", "MAP_ITEMS", "MAP_OBJECTS", "android_metadata", "sqlite_sequence"};

        TreeSet<String> newTables = new TreeSet<>();  // множество порождаемых объектами таблиц
        Collections.addAll(newTables, indirectlyTables);    // включаем в него таблицы, порождаемые опосредованно

        @SuppressWarnings("deprecation")
        DexFile df = new DexFile(App.getAppContext().getPackageCodePath());         // Получаем apk-файл
        for (Enumeration<String> itr = df.entries(); itr.hasMoreElements(); ) {     // проходим по названиям классов в apk
            String s = itr.nextElement();
            if(s.contains("todo.model") || s.contains("todo.common")) {             // фильтруем классы проекта в которых могут быть наследуемые от DBObject классы
                Class clazz = Class.forName(s);
                if(DBObject.class.isAssignableFrom(clazz) &&                        // фильтруем только сабклассы DBObject
                        ! Modifier.isAbstract(clazz.getModifiers())) {

                    TSystem.debug(this, "CI: Creating object of " + clazz.getSimpleName());

                    DBObject dbObject = (DBObject) TObjects.createInstance(clazz);  // создаем объект отфильтрованного класса. Должна создаться (если не было) таблица
                    SQLTable sqlTable = dbObject.getSQLTable();
                    HashMap<String, DBField> mFields = (HashMap<String, DBField>) TestUtils.Reflection.getField(DBObject.class, dbObject, "mFields");

                    Cursor cursor = sqlTable.query();                                    // запрашиваем все строки таблицы, чтобы получить список столбцов
                    List<String> colNames = Arrays.asList(cursor.getColumnNames()); // этот

                    int nCols = 0;                                                  // подсчитываем число столбцов, которое должно быть в таблице
                    for(DBField field : mFields.values())
                        nCols += field.isDBObject() ? 2 : 1;                        // на DBObject прходится 2 столбца, на другие типы - 1

                    if(nCols != cursor.getColumnCount() - 1)                        // количество полей класса должно быть равно количеству столбцов таблицы (минус id)
                        print("Count of columns at " + sqlTable.getTableName() + " table mismatches to count of fields at class " + clazz.getSimpleName() + ".\n");

                    for( DBField field : mFields.values())
                        if(!colNames.contains(field.getColumnName()))               // названия полей должны соответствовать названию столбцов
                            print("Table " + sqlTable.getTableName() + " does not contain the column '" + field.getColumnName() + "'. Check class " + clazz.getName() + "\n");

                    cursor.close();
                    newTables.add(sqlTable.getTableName());                              // добавляем таблицу в множество

                    clearCache();                                                   // обнуляем кеш объектов DBObject
                }
            }
        }

        // сравниваем таблицы, созданные классами и таблицы в БД
        Cursor cursor =  DBOpenHelper.getInstance().getReadableDatabase()
                .query("sqlite_master", new String[]{"name"}, "type = 'table'", null, "name", null, null);

        ArrayList<String> oldTables = new ArrayList<>();
        // цикл по таблицам в БД, ищем их среди новых
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            String tableName = cursor.getString(0);
            oldTables.add(tableName);
            if(!newTables.contains(tableName))
                print("redundant table: " + cursor.getString(0) + "\n");
        }

        cursor.close();

        // цикл по собранным классам. Ищем их среди старых таблиц
        for (String tableName : newTables)
            if(!oldTables.contains(tableName))
                print("missing table: " + cursor.getString(0) + "\n");

        assertFalse(mErrorExists);
    }

    private void print(String msg) {
        TSystem.debug(this, "CI: " + msg);
        mErrorExists = true;
    }

    private void clearCache() throws Exception {
        Class DBCacheClass =  Class.forName("com.mishuto.todo.database.DBCache");
        Object cacheInstance = TestUtils.Reflection.getStaticField(DBCacheClass, "sInstance");
        HashMap cacheMap = (HashMap) TestUtils.Reflection.getField(DBCacheClass, cacheInstance, "mCache");
        cacheMap.clear();
    }
}
