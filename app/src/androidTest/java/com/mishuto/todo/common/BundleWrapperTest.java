package com.mishuto.todo.common;

import android.os.Bundle;

import com.mishuto.todo.database.DBObject;

import org.junit.Test;

import java.io.Serializable;

import static com.mishuto.todo.common.BundleWrapper.getDBObject;
import static com.mishuto.todo.common.BundleWrapper.getSerializable;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Тестирование BundleWrapper
 * Created by Michael Shishkin on 28.01.2018.
 */
public class BundleWrapperTest {

    private final static String TEST_STR = "BundleWrapperTest";

    // Serializable должны сохраняться и читаться из бандла
    @Test
    public void serializableTest() throws Exception {
        Bundle bundle = toBundle(TEST_STR);

        String s = (String) getSerializable(bundle);

        assertEquals(TEST_STR, s);
    }

    // DBObject олжны сохраняться и читаться из бандла.
    @Test
    public void dbObjectTest() throws Exception {
        StringContainer s = new StringContainer(TEST_STR);
        Bundle bundle = toBundle(s);

        assertNotEquals(0, s.getRecordID());
        StringContainer s1 = (StringContainer) getDBObject(bundle); // при сохранении в бандл объект создается в базе

        assertEquals(new StringContainer(TEST_STR), s1);
        assertEquals(0, s.getRecordID());                           /// при извлечении из бандла - объект удаляется из базы
    }

    // Цепочка параметров сохраняется и извлекается из бандла
    @Test
    public void chainTest() throws Exception {
        Long l = 100L;
        DBObject s = new StringContainer(TEST_STR);
        Serializable string = "chainTest";

        Bundle bundle = new BundleWrapper()
                .addParam(l)
                .addParam((DBObject) null)
                .addParam(s)
                .addParam(string)
                .addParam(200L)
                .getBundle();

        BundleWrapper bundleWrapper = new BundleWrapper(bundle);

        assertEquals(100, (long)bundleWrapper.getLong());
        assertEquals(null, bundleWrapper.getDBObject());
        assertEquals(TEST_STR, bundleWrapper.getDBObject().toString());
        assertEquals("chainTest", bundleWrapper.getSerializable());
        assertEquals(200, (long)bundleWrapper.getLong());
    }
}