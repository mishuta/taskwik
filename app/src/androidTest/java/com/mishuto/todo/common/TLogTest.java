package com.mishuto.todo.common;

import com.mishuto.todo.App;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;

import static com.mishuto.todo.common.TLog.IS_LOG_SAVED;
import static com.mishuto.todo.common.TLog.SHARED_FILE;
import static com.mishuto.todo.view_model.TestUtils.Reflection.getField;
import static com.mishuto.todo.view_model.TestUtils.Reflection.newInstance;
import static com.mishuto.todo.view_model.TestUtils.Reflection.setField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Тестирование вывода лога в файл лога
 * Created by Michael Shishkin on 15.09.2018.
 */
public class TLogTest {

    private static TLog sTLog;
    private static PersistentValues.PVBoolean sLogFlag;
    private static boolean sInitLogFlag;
    private static PersistentValues sPV = PersistentValues.getFile(SHARED_FILE);

    // устанавливанм флаг isLogSaved чтобы проверить запись в лог
    @BeforeClass
    public static void setUp() throws Exception {
        sLogFlag = sPV.getBoolean(IS_LOG_SAVED, false);
        sInitLogFlag = sLogFlag.get();
        sLogFlag.set(true);
        setField(TLog.class, null, "sInstance", newInstance(TLog.class)); // пересоздаем объект TLog с новым параметром
        sTLog = TLog.getInstance();
    }

    // восстанавливаем флаг isLogSaved
    @AfterClass
    public static void tearDown() {
        sLogFlag.set(sInitLogFlag);
    }

    //вызов log должен сохранять строку в файле лога
    @Test
    public void log() throws Exception {
        File logFile = (File)getField(TLog.class, sTLog, "mLogFile");
        FileWriter fileWriter = (FileWriter) getField(TLog.class, sTLog, "mFileWriter");
        fileWriter.flush();                             // фиксируем текущицй лог
        long n = logFile.length();                      // вычисляем текущую длину
        sTLog.log(TLog.LEVEL.ERR, "test", "test_log");  // добавляем 40 симворлов
        fileWriter.flush();                             // фиксируем
        assertEquals(40, logFile.length() - n);         // проверяем что они записались в файл
    }

    //вызов saveLog сохраняет логн в отдельном файле лога
    @Test
    public void saveLog() {
        clearExtraLogFiles();                               // очищаем все отдельные файлы лога
        sTLog.log(TLog.LEVEL.ERR, "test", "test_saveLog");  // сохраняем строку в логе
        sTLog.saveLog();                                    // лог сохраняется в отдельном файле taskwik1.log
        assertTrue(new File(App.getAppContext().getFilesDir(), TLog.LOG_FILE_NAME + "1.log").exists()); // проверяем существование taskwik1.log
    }

    // не может быть создано больше чем MAX_LOG_NUM отдельных файлов лога
    @Test
    public void maxFiles() {
        sTLog.log(TLog.LEVEL.ERR, "test", "test_maxFiles");
        for(int i = 0; i < TLog.MAX_LOG_NUM + 2; i++)
            sTLog.saveLog();

        assertEquals(TLog.MAX_LOG_NUM + 1, getFileNames().length);  // всего MAX_LOG_NUM taskwikN.log + taskwik.log
    }

    // logUnhandledException создает отдельный лог с эксепшеном
    @Test
    public void logUnhandledException() {
        clearExtraLogFiles();
        Throwable throwable = new AssertionError("logUnhandledException");
        sTLog.logUnhandledException(throwable);
        assertTrue(new File(App.getAppContext().getFilesDir(), TLog.LOG_FILE_NAME + "1.log").exists());
    }

    private String[] getFileNames() {
        return App.getAppContext().getFilesDir().list(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.startsWith(TLog.LOG_FILE_NAME);
                    }
                });
    }

    private void clearExtraLogFiles() {
        for(String name : getFileNames())
            if(!name.equals(TLog.LOG_FILE_NAME + ".log"))
                //noinspection ResultOfMethodCallIgnored
                new File(App.getAppContext().getFilesDir(), name).delete();

        PersistentValues.PVInt currentLogNo = sPV.getInt("currentLogNo", 0);
        currentLogNo.set(0);                                // сбрасываем счетчик текущего отдельного файла
    }
}