package com.mishuto.todo.common;

import org.junit.Assert;
import org.junit.Test;

import static com.mishuto.todo.common.TCalendar.now;
import static java.util.Calendar.DATE;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.YEAR;

/**
 * Тестирование методов TCalendar
 * Created by Michael Shishkin on 27.05.2017.
 */
public class TCalendarTest extends Assert {

    private TCalendar t1, t2;

    @Test
    // 7.6.17 20:45 - 3.6.17 17:20 = 4 дня
    public void intervalFrom1() {

        t1 = new TCalendar(2017, 5, 3, 17, 20);
        t2 = new TCalendar(2017, 5, 7, 20, 45);
        assertEquals(7 - 3, t2.intervalFrom(DATE, t1));
    }

    @Test
    // 7.6.17 10:30 - 3.6.17 21:10 = 4 дня
    public void intervalFrom2() {
        t1 = new TCalendar(2017, 5, 3, 21, 10);
        t2 = new TCalendar(2017, 5, 7, 10, 30);

        assertEquals(7 - 3, t2.intervalFrom(DATE, t1));
    }

    @Test
    // 3.6.17 00:00 - 3.6.17 10:00 = 0 дней
    public void intervalFrom3() {
        t1 = new TCalendar(2027, 5, 3, 10, 0);
        t2 = new TCalendar(2027, 5, 3, 0, 0);
        assertEquals(0, t2.intervalFrom(DATE, t1));
    }

    @Test
    // 2.6.17 00:00 - 3.6.17 10:00 = 0 лет
    public void intervalFrom4() {
        t1 = new TCalendar(2027, 5, 3, 10, 0);
        t2 = new TCalendar(2027, 5, 2, 0, 0);
        assertEquals(0, t2.intervalFrom(YEAR, t1));
    }

    // 31.12.17 5:00 - 1.1.18 10:00 = -1 день
    @Test
    public void intervalFrom5() {
        t1 = new TCalendar(2018, 0, 1, 10, 0);
        t2 = new TCalendar(2017, 11, 31, 5, 0);
        assertEquals(-1, t2.intervalFrom(DATE, t1));
    }

    @Test
    public void clipToDate() {
        t1 = now();
        t1.clipTo(DATE);
        assertEquals(0, t1.getTimeInMillis() % (60 * 60 * 1000L)); // минуты и секунды = 0
        assertEquals(0, t1.get(HOUR_OF_DAY));   // часы = 0. Учитываем TZ
    }

    @Test
    public void clipToMinute() {
        t1 = now();
        t1.clipTo(MINUTE);
        assertEquals(0, t1.getTimeInMillis() % (60 * 1000));
    }

    // 22:13 округлить до 15 = 22:15
    @Test
    public void round1() {
        t1 = new TCalendar(2017, 1, 1, 22, 13);
        assertEquals(new TCalendar(2017, 1, 1, 22, 15), t1.roundTo(15));
    }

    // 22:13 округлить до 30 = 22:00
    @Test
    public void round2() {
        t1 = new TCalendar(2017, 1, 1, 22, 13);
        assertEquals(new TCalendar(2017, 1, 1, 22, 0), t1.roundTo(30));
    }

    // 22:13 округлить до 8 часов = 00:00 след. дня
    @Test
    public void round3() {
        t1 = new TCalendar(2017, 1, 1, 22, 13);
        assertEquals(new TCalendar(2017, 1, 2, 0, 0), t1.roundTo(8 * 60));
    }
}