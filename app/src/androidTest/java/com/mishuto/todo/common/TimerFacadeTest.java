package com.mishuto.todo.common;

import android.os.SystemClock;

import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.BeforeClass;
import org.junit.Test;

import static java.util.Calendar.SECOND;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Тестирование методов таймера TimerFacade
 * Created by Michael Shishkin on 23.12.2017.
 */
public class TimerFacadeTest {

    @BeforeClass
    public static void setUp() {
        TaskList.get().load();  // предварительно загружаем задачи, чтобы все таймеры отработали
    }

    private final static int ACCURACY = 5; // минимальная длительность срабатывания таймера в ОС
    private final static int DELAY = 1;
    private boolean fired;

    // таймер должен срабатывать через DELAY секунд не позднее чем через DELAY + ACCURACY
    @Test
    public void timerSet() throws Exception {
        fired = false;
        TCalendar t = TCalendar.now();
        t.add(SECOND, DELAY);

        final Table sTimerTable = (Table)TestUtils.Reflection.getStaticField(TimerFacade.AlarmReceiver.class, "sTimerTable");
        int oldSize = sTimerTable.size();
        TimerFacade timer = new TimerFacade();

        timer.setTimer(new TimerFacade.Alarm() {
                                 @Override
                                 public void onFire(int timerId, TCalendar time){
                                     TSystem.debug(this, "STOP ALARM");
                                     assertTrue(TCalendar.now().intervalFrom(SECOND, time) < ACCURACY);
                                     fired = true;
                                 }
                             }, t
        );

        TSystem.debug(this, "START ALARM");

        for(int i = 0; i < DELAY + ACCURACY; i++) { // раз в секунду проверяем не сработал ли таймер
            SystemClock.sleep(1000);
            if(fired)
                break;
        }

        assertTrue(fired);                          // таймер сработал
        assertEquals(oldSize, sTimerTable.size());  // таймер создался и удалился
        assertNull(timer.getAlarmTime());
    }

    // таймер не должен сработать после его отмены
    @Test
    public void cancel() throws Exception {
        TCalendar t = TCalendar.now();
        t.add(SECOND, DELAY);
        TimerFacade timer = new TimerFacade();

        timer.setTimer(new TimerFacade.Alarm() {
                                 @Override
                                 public void onFire(int timerId, TCalendar time){
                                     assertTrue(false);
                                 }
                             }, t
        );

        SystemClock.sleep(100);
        timer.cancel();
        assertNull(timer.getAlarmTime());

        SystemClock.sleep((DELAY + ACCURACY) * 1000);
    }

    // таймер можно переустановить на новое время
    @Test
    public void resetTimer() throws Exception {
        fired = false;
        TCalendar t1 = TCalendar.now();
        t1.add(SECOND, DELAY);

        TCalendar t2 = TCalendar.now();
        t2.add(SECOND, DELAY + ACCURACY + 1);

        TimerFacade timer = new TimerFacade();

        final TCalendar newTime = t2;

        TimerFacade.Alarm hook = new TimerFacade.Alarm() {
            @Override
            public void onFire(int timerId, TCalendar time) {
                assertEquals(newTime, time);
                assertTrue(TCalendar.now().intervalFrom(SECOND, time) < ACCURACY);
                fired = true;
            }
        };

        final Table sTimerTable = (Table)TestUtils.Reflection.getStaticField(TimerFacade.AlarmReceiver.class, "sTimerTable");
        int oldSize = sTimerTable.size();

        timer.setTimer(hook, t1);
        SystemClock.sleep(100);
        timer.setTimer(hook, t2);

        for(int i = 0; i < (DELAY + ACCURACY + 1) + ACCURACY; i++) { // раз в секунду проверяем не сработал ли таймер
            SystemClock.sleep(1000);
            if(fired)
                break;
        }

        assertTrue(fired);
        assertNull(timer.getAlarmTime());
        assertEquals(oldSize, sTimerTable.size());
    }
}