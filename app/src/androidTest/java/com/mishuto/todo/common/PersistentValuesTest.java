package com.mishuto.todo.common;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertSame;

/**
 * Тесты PersistentValues
 * Created by Michael Shishkin on 16.09.2018.
 */
public class PersistentValuesTest {
    @Test
    //разные запросы с одинаковыми ключами возвращают одинаковые объекты
    public void identityObjects() {
        PersistentValues pv1 = PersistentValues.getFile("PVTest");
        PersistentValues.PVInt pvInt1 = pv1.getInt("pvInt", 1);

        PersistentValues pv2 = PersistentValues.getFile("PVTest");
        PersistentValues.PVInt pvInt2 = pv2.getInt("pvInt", 2);

        pvInt1.set(5);
        assertEquals((Integer) 5, pvInt2.get());
        assertSame(pv1, pv2);
        assertSame(pvInt1, pvInt2);

    }
}