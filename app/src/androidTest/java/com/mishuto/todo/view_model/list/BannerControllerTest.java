package com.mishuto.todo.view_model.list;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.PersistentValues;
import com.mishuto.todo.model.ReleaseNotes;
import com.mishuto.todo.todo.BuildConfig;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasTextColor;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static org.hamcrest.core.IsNot.not;

/**
 * Тест баннера WhatsNew
 * Created by Michael Shishkin on 19.01.2019.
 */
public class BannerControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class, false, false);

    @AfterClass
    public static void tearDown() throws Exception {
        setLastVersion(BuildConfig.VERSION_CODE);
    }

    // баннер должен появиться, если установлена новая версия и есть заметки
    @Test
    public void showBanner() throws Exception {
        setLastVersion(10);
        mActivityRule.launchActivity(null);
        onView(withId(R.id.banner_text)).check(matches(isDisplayed()));
    }

    // баннер не должен появляться после рестарта приложения (новая версия стала старой)
    @Test
    public void hideBannerIfRestart() throws Exception {
        setLastVersion(10);
        ReleaseNotes.getInstance();
        TestUtils.Reflection.setField(ReleaseNotes.class, null, "sReleaseNotes", null);

        mActivityRule.launchActivity(null);

        onView(withId(R.id.banner_text)).check(matches(not(isDisplayed())));
    }

    // баннер не должен появляться если версия новая, но нет заметок
    @Test
    public void hideBannerIfNoNotes() throws Exception{
        setLastVersion(13);
        mActivityRule.launchActivity(null);
        onView(withId(R.id.banner_text)).check(matches(not(isDisplayed())));
    }

    // баннер должен подкрашиваться, если есть hot
    @Test
    public void hotBanner() throws Exception {
        setLastVersion(10);
        mActivityRule.launchActivity(null);
        onView(withId(R.id.banner_text)).check(matches(hasTextColor(R.color.Red)));
    }

    // баннер не должен подкрашиваться, если нет hot
    @Test
    public void notHotBanner() throws Exception{
        setLastVersion(11);
        mActivityRule.launchActivity(null);
        onView(withId(R.id.banner_text)).check(matches(hasTextColor(R.color.colorMainText)));
    }

    // нажатие на Dismiss баннера: появляется снек, а баннер - пропадает
    @Test
    public void onDismiss() throws Exception {
        setLastVersion(10);
        mActivityRule.launchActivity(null);
        clickOn(R.id.btn_dismiss);
        onSnackBar().check(matches(withText(R.string.what_s_new_reminder)));
        onView(withId(R.id.banner_text)).check(matches(not(isDisplayed())));
    }

    // нажатие на LearnMore баннера: появляется диалог, а баннер - пропадает
    @Test
    public void onMore() throws Exception {
        setLastVersion(10);
        mActivityRule.launchActivity(null);
        clickOn(R.id.btn_more);
        onView(withText(R.string.release_notes)).check(matches(isDisplayed()));
        pressBack();
        onView(withId(R.id.banner_text)).check(matches(not(isDisplayed())));
    }

    // устанавливаем lastVersion = version и подменяем файл ReleaseNotes на тестовый
    public static void setLastVersion(int version) throws Exception {
        PersistentValues.getFile("Banner").getInt("lastVersion", 0).set(version);
        TestUtils.Reflection.setField(ReleaseNotes.class, null, "RELEASE_NOTES_FILE", R.raw.whatsnew_test);
        TestUtils.Reflection.setField(ReleaseNotes.class, null, "sReleaseNotes", null);
    }
}