package com.mishuto.todo.view_model.details.field_controllers;

import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.widget.EditText;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.database.SQLHelper;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onTextInputLayout;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static junit.framework.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

/**
 * Тестирование AutocompleteFreezeView с исполнителем в карточке задачи
 * Created by Michael Shishkin on 22.05.2017.
 */
public class ExecutorControllerTest  {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private final static String TEST_PREFIX = "Test1254";
    private final static String CUT_NAME = "test12";

    private String mTestName;


    @Test
    public void checkExecutor() {
        Task task = createTask("checkExecutor");

        mTestName = TextUtils.uniqueStr(Executors.getInstance().getTags(),TEST_PREFIX);
        onView(withId(R.id.taskDetails_label_executor)).perform(ViewActions.scrollTo(), ViewActions.click());
        onTextInputLayout(R.id.taskDetails_executor).perform(scrollTo(),
                typeText(mTestName), pressImeActionButton(),    // вводим тестовое имя "test1254"
                ViewActions.closeSoftKeyboard());
        sleep(200);

        clickOn(R.id.taskDetails_label_executor);
        // вводим "test12", появляется подсказка test1254
        onTextInputLayout(R.id.taskDetails_executor).perform(replaceText(CUT_NAME));
        closeSoftKeyboard();
        // ищем "test1254" среди перечня подсказок в списке автокомплита.
        onView(withText(mTestName)).inRoot(isPlatformPopup()).perform(ViewActions.click());
        // проверяем что нужное test1254 есть в списке Executor и сохраняем
        onTextInputLayout(R.id.taskDetails_executor).perform(pressImeActionButton());
        onView(allOf(withText(mTestName), not(isAssignableFrom(EditText.class)))).perform(scrollTo()).check(matches(isDisplayed()));

        clickOn(SAVE);

        sleep(200);
        task.reload();
        assertThat(task.getExecutor().toString(), is(mTestName));

        TaskList.remove(task);
    }

    @After
    public void tearDown() {
        StringContainer stringContainer = new StringContainer(mTestName);
        Executors.getInstance().remove(stringContainer);
        assertEquals(0, stringContainer.getSQLTable().query(SQLHelper.whereString("STRING=?", mTestName)).countNClose());
    }
}