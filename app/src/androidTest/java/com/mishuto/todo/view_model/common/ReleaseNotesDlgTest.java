package com.mishuto.todo.view_model.common;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.BundleWrapper;
import com.mishuto.todo.todo.BuildConfig;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.hasTextColor;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSubstring;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.hasItemsCount;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerView;
import static com.mishuto.todo.view_model.list.BannerControllerTest.setLastVersion;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Тест диалога ReleaseNotes
 * Created by Michael Shishkin on 19.01.2019.
 */
public class ReleaseNotesDlgTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @AfterClass
    public static void tearDown() throws Exception {
        setLastVersion(BuildConfig.VERSION_CODE);   // приводим LastVersion в исходное
    }

    // диалог выводит первую строку файла с заданным title и description. title красный, т.к. hot=true
    @Test
    public void checkFirstLine() throws Exception {
        setLastVersion(0);
        DialogHelper.createDialog(new ReleaseNotesDlg(), BundleWrapper.toBundle(0));
        onRecyclerView(R.id.recycler_view,
                allOf(withId(R.id.title), hasSibling(withText("1.0.4.13"))))
                .check(matches(withSubstring("Title 11 10413 true")))
                .check(matches(hasTextColor(R.color.Red)));

        onRecyclerView(R.id.recycler_view,
                allOf(withId(R.id.description), hasSibling(withText("1.0.4.13"))))
                .check(matches(withSubstring("Description 11 10413 true")));

    }

    // заголовок последней строчки черный, т.к. hot=false
    @Test
    public void checkLastLine() throws Exception {
        setLastVersion(0);
        DialogHelper.createDialog(new ReleaseNotesDlg(), BundleWrapper.toBundle(0));
        onRecyclerView(R.id.recycler_view,
                allOf(withId(R.id.title), hasSibling(withText("1.1.1.3")))).check(matches(hasTextColor(R.color.colorMainText)));
    }

    // вывод неполного списка (с 11-й версии). Выводится только 3 строки, 1.0.4.13 - отсутствует
    @Test
    public void checkLastVersion() throws Exception {
        setLastVersion(0);
        DialogHelper.createDialog(new ReleaseNotesDlg(), BundleWrapper.toBundle(11));

        onView(withId(R.id.recycler_view)).check(matches(hasItemsCount(3)));
        onView(withText("1.0.4.13")).check(doesNotExist());

    }
}