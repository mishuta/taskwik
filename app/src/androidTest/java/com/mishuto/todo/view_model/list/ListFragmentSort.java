package com.mishuto.todo.view_model.list;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.task_fields.Order;
import com.mishuto.todo.model.task_fields.Priority;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerView;
import static com.mishuto.todo.view_model.TestUtils.Card.COMPLETE_BTN;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setPriority;
import static com.mishuto.todo.view_model.TestUtils.Filters.clearFilters;
import static com.mishuto.todo.view_model.TestUtils.List.ACTIVATE;
import static com.mishuto.todo.view_model.TestUtils.List.COMPLETE;
import static com.mishuto.todo.view_model.TestUtils.List.clickTaskListMenu;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.Reflection.getField;
import static org.junit.Assert.assertTrue;

/**
 * Тесты сортировки в списке задач
 * Created by Michael Shishkin on 08.12.2018.
 */
public class ListFragmentSort {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // сортировка изменением приоритета в карточке
    @Test
    public void sort1() throws Exception {
        Task t1 = createTask("sort1 t1");
        clickOn(SAVE);
        Task t2 = createTask("sort1 t2");
        setPriority(Priority.URGENTLY);
        clickOn(SAVE);

        sleep(200);
        assertTrue(getPosInList(t1) > getPosInList(t2));

        onRecyclerView(R.id.recycler_view, withText(t2.toString())).perform(click());
        setPriority(Priority.NO_IMPORTANT);
        clickOn(SAVE);

        assertTrue(getPosInList(t1) < getPosInList(t2));

        TaskList.remove(t1);
        TaskList.remove(t2);
    }

    //сортировка изменением статуса завершен в списке
    @Test
    public void sort2()throws Exception {
        // сбрасываем фильтр незавершенных
        boolean filterState = FilterManager.incomplete().isFilterSet();
        FilterManager.incomplete().setFilter(false);

        Task t1 = createTask("sort2 t1");
        setPriority(Priority.URGENTLY);
        sleep(400);

        clickOn(COMPLETE_BTN);
        clickOn(SAVE);

        sleep(100);
        Task t2 = createTask("sort2 t2");
        clickOn(SAVE);

        assertTrue(getPosInList(t1) > getPosInList(t2));

        TaskList.remove(t1);
        TaskList.remove(t2);
        FilterManager.incomplete().setFilter(filterState);
    }

    // сортировка изменением порядка в группе из 4 задач
    @Test
    public void sort3() throws Exception {
        Task t1 = createTask("sort3 t1"); clickOn(SAVE);
        Task t2 = createTask("sort3 t2"); clickOn(SAVE);
        Task t3 = createTask("sort3 t3"); clickOn(SAVE);
        Task t4 = createTask("sort3 t4");
        TestUtils.Card.setOrder(Order.HIGH);
        clickOn(SAVE);

        assertTrue(getPosInList(t4) < getPosInList(t1));

        TaskList.remove(t1);
        TaskList.remove(t2);
        TaskList.remove(t3);
        TaskList.remove(t4);

    }

    // Корректная сортировка при одновременном изменении двух задач по времени
    @Test
    public void groupSortA() throws Exception {
        clearFilters();
        Task t1 = createTask("groupSortA t1"); clickOn(SAVE);
        Task t2 = createTask("groupSortA t2"); clickOn(SAVE);
        Task t3 = createTask("groupSortA t3"); clickOn(SAVE);

        TestUtils.setTaskTimerAndSleep(t3, t2);
        assertTrue(getPosInList(t2) < getPosInList(t3));
        assertTrue(getPosInList(t3) < getPosInList(t1));

        TaskList.remove(t1);
        TaskList.remove(t2);
        TaskList.remove(t3);
    }

    // Корректная сортировка при одновременном изменении двух задач по зависимости
    @Test
    public void groupSortB() throws Exception {
        clearFilters();
        Task t1 = createTask("groupSortB t1"); clickOn(SAVE);
        Task t2 = createTask("groupSortB t2"); clickOn(SAVE);
        Task t3 = createTask("groupSortB t3"); clickOn(SAVE);

        t1.setPrev(t3);                     // t1 зависит от t3
        clickTaskListMenu(t3, COMPLETE);    // t3 завершаем (со списком ничего не происходит)
        clickTaskListMenu(t3, ACTIVATE);    // t3 активируем. t1 уходит в конец списка как отложенный, а t1 и t3 остаются активными

        assertTrue(getPosInList(t2) < getPosInList(t3));    // причем t3 должен оказаться ниже t2, т.к. создан позднее

        TaskList.remove(t1);
        TaskList.remove(t2);
        TaskList.remove(t3);
    }


    // Узнать позицию задачи в списке задач
    private int getPosInList(Task task) throws Exception {
        ListFragment fragment = (ListFragment) mActivityRule.getActivity().getMainFragment();
        ListController listController = (ListController) getField(ListFragment.class, fragment, "mListController");
        //noinspection unchecked
        RecyclerViewWidget<Task> recyclerView = (RecyclerViewWidget<Task>) getField(ListController.class, listController, "mTasksRecyclerView");
        return recyclerView.getDataList().indexOf(task);
    }
}
