package com.mishuto.todo.view_model.settings;

import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;

import com.mishuto.todo.App;
import com.mishuto.todo.common.BackupManager;
import com.mishuto.todo.common.PersistentValues;
import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.database.DBOpenHelper;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.ProjectsSet;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.EspressoHelper;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.common.BackupManager.getDB;
import static com.mishuto.todo.common.TSystem.copyFile;
import static com.mishuto.todo.view_model.EspressoHelper.scrollClick;
import static com.mishuto.todo.view_model.TestUtils.List.SETTINGS;
import static com.mishuto.todo.view_model.TestUtils.List.clickListOptionsMenu;
import static com.mishuto.todo.view_model.settings.DataTest.DBManage.restoreDB;
import static com.mishuto.todo.view_model.settings.DataTest.DBManage.saveDB;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/** 
 * Тестирование работы с бд в Settings
 * 
 * !!!Функционал работы BackupManager тестируется в LoadCheckerActivityTest
 * 
 * Created by Michael Shishkin on 21.07.2018.
 */
public class DataTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // превинтивно включаем пермишен
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE);

    private final ProjectsSet PROJECTS = ProjectsSet.getInstance();

    // включаем режим TEST_MODE чтобы избежать перзагрузки аппы во время теста
    @BeforeClass
    public static void setUp() throws Exception {
        TestUtils.Reflection.setField(App.class, null, "TEST_MODE", true);
    }

    // после очистки базы, файл удаляется
    @Test
    public void clearDB(){
        saveDB();
        clickListOptionsMenu(SETTINGS);
        scrollClick(R.id.settings_clearDB);
        onView(withText(R.string.buttonClear)).perform(click());

        assertFalse(BackupManager.getDB().exists());
        restoreDB();
    }

    //очистить->отмена не удаляет файл базы
    @Test
    public void clearDBCancel() {
        clickListOptionsMenu(SETTINGS);
        scrollClick(R.id.settings_clearDB);
        onView(withText(android.R.string.cancel)).perform(click());

        assertTrue(BackupManager.getDB().exists());
    }

    // при восстановлении базы, восстанавливается сохраненный в бэкапе проект
    @Test
    public void RestoreDB() {
        StringContainer test = addProject("RestoreDB"); // создаем новый проект
        BackupManager.getInstance().makeBackUp();       // сохраняем бэкап

        PROJECTS.remove(test);                          // удаляем проект

        clickListOptionsMenu(SETTINGS);
        scrollClick(R.id.settings_restore);             // восстановить
        onView(withText(R.string.buttonRecover)).perform(click());
        assertTrue(checkProject(test));                 // проект есть
    }

    //в случае ошибки во время восстановления должен показываться снек
    @Test
    public void restoreFail() {
        //noinspection ResultOfMethodCallIgnored
        BackupManager.getBackup().delete();                                             // удаляем бэкап, имитируя сбой
        clickListOptionsMenu(SETTINGS);
        scrollClick(R.id.settings_restore);
        onView(withText(R.string.buttonRecover)).perform(click());                      // жмем восстановить
        EspressoHelper.onSnackBar().check(matches(withText(R.string.cantRestoreMsg)));  // получаем снек
    }
/*
    // проверка пары экспорт-импорт
    // после экспорта можно выбрать тасквик и импортировать ту же базу. При этом текущая база созраняется в бэкап
    @Test
    public void export_import() throws Exception {
        clickListOptionsMenu(SETTINGS);
        scrollClick(R.id.settings_export);                              // жмем экспорт
        StringContainer test = addProject("export_import");             // в этот момент создаем в базе новый проект

        TestUtils.Automation.clickOnText("Taskwik");                    // выбираем в чузере приложение Taskwik
        onView(withText(R.string.importDB_button)).perform(click());    // подтверждаем импорт
        assertFalse(checkProject(test));                                // созданного проекта нет (импорт перезаписал файл)

        BackupManager.getInstance().restoreBackup();                    // восстанавливаем бэкап
        assertTrue(checkProject(test));                                 // созданый проект был (импорт создал бэкап)
    }
*/
    private StringContainer addProject(String project) {
        StringContainer test = new StringContainer(TextUtils.uniqueStr(PROJECTS.getTags(), project));
        PROJECTS.add(test);
        return test;
    }

    private boolean checkProject(StringContainer project) {
        PROJECTS.reload();                              // перегружаем проекты
        return PROJECTS.remove(project);

    }

    // класс для работы с BackupManager
    public static class DBManage {
        private static File saved = new File(App.getAppContext().getFilesDir(), "saved.db");    // файл для временного хранения текущей БД

        public static void saveDB() {
            BackupManager.getInstance().exportDB(saved);
        }

        public static void restoreDB() {
            copyFile(saved, getDB());
            DBOpenHelper.getInstance().close();
            TaskList.get().reload();
        }

        // установить флаг наличия бэкапа
        public static void setBackupFlag(boolean flag) throws Exception {
            PersistentValues.PVBoolean isBackupExists = (PersistentValues.PVBoolean) TestUtils.Reflection.getField(BackupManager.class, BackupManager.getInstance(), "isBackupExists");
            isBackupExists.set(flag);
        }

        // установить флаг ошибки
        public static void setFailure() throws Exception {
            PersistentValues.PVInt mAttempts = (PersistentValues.PVInt) TestUtils.Reflection.getField(BackupManager.class, BackupManager.getInstance(), "mAttempts");
            mAttempts.set((int)TestUtils.Reflection.getStaticField(BackupManager.class, "MAX_ATTEMPTS") + 1);
        }
    }
}
