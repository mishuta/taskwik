package com.mishuto.todo.view_model.details.field_controllers;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.Card.setPrevTask;
import static com.mishuto.todo.view_model.TestUtils.Card.setTaskDate;
import static com.mishuto.todo.view_model.TestUtils.Card.setTaskTime;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.getObjectsCount;
import static java.util.Calendar.APRIL;
import static java.util.Calendar.AUGUST;
import static java.util.Calendar.DATE;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * Тестирование взаимовлияния атрибутов задачи: Term, Time, Recurrence, PrevTask
 * Created by Michael Shishkin on 27.05.2017.
 */
public class ConstraintsTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private static final TCalendar INIT_DATE = new TCalendar(2025, AUGUST, 19, 18, 30);
    private static final TCalendar CHANGED_DATE = new TCalendar(2030, APRIL, 27, 20, 15);

    private void setInitTime() {
        setTaskDate(INIT_DATE);
        setTaskTime(INIT_DATE.get(HOUR_OF_DAY), INIT_DATE.get(MINUTE));
    }

    private Task createPrevTask() {
        Task prev = TaskList.addNew();                         // Создаем еще одну задачу
        prev.getTitle().setTaskName(TextUtils.uniqueStr(TaskList.getTasks(), "createPrevTask"));
        return prev;
    }

    // тест на изменение даты - время не меняется, повторы пересчитываются
    @Test
    public void onTermChanged() {
        Task task = createTask("onTermChanged");

        setInitTime();

        setTaskDate(CHANGED_DATE);                                                                    // устанавливаем дату в CHANGED_DATE

        assertThat(task.getTaskTime().isTimeSet(), is(true));                                                  // флаг времени не сбросился после установки даты
        assertThat(task.getTaskTime().getHour(), is(INIT_DATE.get(HOUR_OF_DAY)));                              // время не изменилось

        cancelTask();
    }


    // тест на сброс даты
    @Test
    public void onTermReset() {
        Task task = createTask("onTermReset");

        setTaskTime(23, 50);
        clickOn(R.id.taskDetails_clearTerm);

        assertThat(task.getTaskTime().isTimeSet(), is(false));                                             // флаг времени сброшен
        assertThat(task.getTaskTime().isDateSet(), is(false));                                             // дата сброшена

        cancelTask();
    }

    // тест установки времени
    @Test
    public void onTimeChanged() {
        Task task = createTask("onTimeChanged");
        setInitTime();
        setTaskTime(CHANGED_DATE.get(HOUR_OF_DAY), CHANGED_DATE.get(MINUTE));                         // устанавливаем время

        assertThat(task.getTaskTime().getCalendar().intervalFrom(DATE, INIT_DATE), is(0));                     // дата не изменилась
        assertThat(task.getTaskTime().getCalendar().intervalFrom(MINUTE, INIT_DATE), is(60 + 45));             // время изменилось на 01:45 часа

        cancelTask();
    }

    // дата сбрасывается, когда устанавливается предыдущая задача
    @Test
    public void onPrevTaskChanged() {
        int n = getObjectsCount(Task.class);
        Task prev = createPrevTask();
        Task task = createTask("onPrevTaskChanged");
        setPrevTask(prev);

        assertThat(task.getTaskTime().isDateSet(), is(false));
        cancelTask();
        TaskList.remove(prev);
        assertEquals(n, getObjectsCount(Task.class));
    }

    //предыдущая задача сбрасывается, когда установленна дата
    @Test
    public void PrevResetWhenTermChanged() {
        int n = getObjectsCount(Task.class);
        Task prev = createPrevTask();
        Task task = createTask("PrevResetWhenTermChanged");
        setPrevTask(prev);

        setTaskDate(CHANGED_DATE);
        onSnackBar().check(matches(isDisplayed()));                                                             // проверяем наличие предупреждения о скипе предыдущей задачи
        assertNull(task.getPrev());

        cancelTask();
        TaskList.remove(prev);
        assertEquals(n, getObjectsCount(Task.class));
    }
}