package com.mishuto.todo.view_model.common;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.App;
import com.mishuto.todo.common.BackupManager;
import com.mishuto.todo.common.PersistentValues;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.common.BackupManager.BACKUP_NAME;
import static com.mishuto.todo.common.BackupManager.getBackup;
import static com.mishuto.todo.common.BackupManager.getDB;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.settings.DataTest.DBManage.restoreDB;
import static com.mishuto.todo.view_model.settings.DataTest.DBManage.saveDB;
import static com.mishuto.todo.view_model.settings.DataTest.DBManage.setBackupFlag;
import static com.mishuto.todo.view_model.settings.DataTest.DBManage.setFailure;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Тестирование функционала LoadCheckerActivity
 */

// По умолчанию тест закомментирован, т.к. манипуляции с БД влияют на последующие тесты.
// требуется перезагрузка всех статических переменных и контекстов классов, т.е. уничтожение процесса
// Кроме того уничтожается сама БД
//@Ignore
public class LoadCheckerActivityTest {
    // по умолчанию активити не стартует самостоятельно!
    @Rule
    public ActivityTestRule<LoadCheckerActivity> mActivityRule = new ActivityTestRule<>(LoadCheckerActivity.class, false, false);

    private BackupManager mManager = BackupManager.getInstance();

    @BeforeClass
    public static void setUp() throws Exception {
        // сбрасываем флаг showed для WelcomeActivity, чтобы не загрузилась WelcomeActivity
        PersistentValues.getFile("WelcomeActivity").getBoolean("showed", false).set(true);
    }

    // должен создаваться бэкап при загрузке, если бэкапа не было
    @Test
    public void createBackup() throws Exception {
        App.getAppContext().deleteDatabase(BACKUP_NAME);            // удаляем бэкап
        setBackupFlag(false);                                       // сбрасываем флаг бэкапа
        mManager.resetAttemptsCounter();                            // сбрасываем счетчик неуспешных (на всякий случай)
        mActivityRule.launchActivity(null);                         // запускаем активность
        assertTrue(getBackup().exists());                           // бэкап появился
    }

    // база восстановливается из бэкапа после сбоя
    // фаза 1. Создаем задачу
    // фаза 2. Сохраняем бэкап, переименовываем задачу
    // фаза 3. Восстанавливаем бэкап, проверяем что задача имеет старое название
    @Test
    public void restoreBackup() throws Exception {
        // фаза 1.
        mManager.resetAttemptsCounter();
        mActivityRule.launchActivity(null);
        Task t = createTask("restoreBackup");                       // создаем задачу
        clickOn(SAVE);
        setBackupFlag(false);                                       // сбрасываем флаг бэкапа
        AbstractActivity.getCurrentActivity().finish();             // завершаем активность
        sleep(100);

        // фаза 2
        mActivityRule.launchActivity(null);                         // здесь сохраняется бэкап
        TestUtils.List.clickTask(t);
        TestUtils.Card.setTitle("renamed");                         // переименовываем задачу
        clickOn(SAVE);
        sleep(400);
        AbstractActivity.getCurrentActivity().finish();
        sleep(100);

        // фаза 3
        setFailure();                                               // имитируем сбой
        sleep(100);
        mActivityRule.launchActivity(null);
        onView(withText(R.string.buttonRecover)).perform(click());  // в окне восстановления выбираем восстановить
        onView(withText(R.string.buttonRecover)).perform(click());  // подтверждаем

        TaskList.get().reload();                          // TaskList между фазами не перегружается, имитируем загрузку списка
        assertEquals("restoreBackup", t.toString());                // убеждаемся что восстановилась задача со стрым именем
        TaskList.remove(t);                                        // удаляем ее
    }

    // база удаляется после сбоя (из диалога восстановления)
    @Test
    public void clearDBRestoreDialog() throws Exception {
        saveDB();
        // фаза 1. запускаем нормальную активность - база есть или создастся
        mManager.resetAttemptsCounter();
        mActivityRule.launchActivity(null);                         // запускаем активность
        sleep(100);
        AbstractActivity.getCurrentActivity().finish();             // завершаем активность

        // фаза 2. удаляем базу, проверяем что ее нет
        setBackupFlag(true);
        setFailure();                                               // имитируем сбой
        mActivityRule.launchActivity(null);
        onView(withText(R.string.buttonClear)).perform(click());    // жмем очистить
        onView(withText(R.string.buttonClear)).perform(click());    // подтверждаем очистить

        assertFalse(getDB().exists());                              // проверяем что файла нет
        restoreDB();
    }

    // база удаляется после сбоя (из диалога очистки)
    @Test
    public void clearDBClearDialog() throws Exception {
        saveDB();
        // фаза 1. запускаем нормальную активность - база есть или создастся
        mManager.resetAttemptsCounter();
        mActivityRule.launchActivity(null);                         // запускаем активность
        sleep(100);
        AbstractActivity.getCurrentActivity().finish();             // завершаем активность

        // фаза 2. удаляем базу, проверяем что ее нет
        setBackupFlag(false);                                       // бэкапа нет
        setFailure();                                               // имитируем сбой
        mActivityRule.launchActivity(null);
        onView(withText(R.string.buttonClear)).perform(click());    // жмем очистить

        assertFalse(getDB().exists());                              // проверяем что файла нет
        assertFalse(mManager.isFailureDetected());                  // счетчик сброшен
        restoreDB();
    }

    // обход негативных веток диалога восстановления
    @Test
    public void negativeBranchesRecoverDialog() throws Exception {
        setFailure();
        setBackupFlag(true);
        mActivityRule.launchActivity(null);

        onView(withText(R.string.LoadFailure_recover)).check(matches(isDisplayed()));       // попадаем в RecoverDialog
        onView(withText(R.string.buttonClear)).perform(click());                            // очистить
        onView(withText(R.string.LoadFailure_clearWarning)).check(matches(isDisplayed()));  // предупреждение об очистке
        onView(withText(android.R.string.cancel)).perform(click());                         // отмена
        onView(withText(R.string.LoadFailure_recover)).check(matches(isDisplayed()));       // возвращаемся в предыдущее окно

        onView(withText(R.string.buttonRecover)).perform(click());                          // восстановить
        onView(withText(startsWith(App.getAppContext().getString(R.string.LoadFailure_restoreWarning).substring(0, 15))))
                .check(matches(isDisplayed()));                                             // предупреждение о восстановлении (первые 15 символов)
        onView(withText(android.R.string.cancel)).perform(click());                         // отмена
        onView(withText(R.string.LoadFailure_recover)).check(matches(isDisplayed()));       // возвращаемся в предыдущее окно

        onView(withText(R.string.buttonCancel)).perform(click());                           // выйти
        assertFalse(mManager.isFailureDetected());                                          // счетчик сброшен
    }

    // обход негативных веток диалога очистки (только выйти)
    @Test
    public void negativeBranchesClearDialog() throws Exception {
        setFailure();
        setBackupFlag(false);
        mActivityRule.launchActivity(null);

        onView(withText(R.string.LoadFailure_clear)).check(matches(isDisplayed()));         // попадаем в ClearDialog
        onView(withText(R.string.buttonRecover)).check(doesNotExist());                     // кнопки восстановить - нет
        onView(withText(R.string.buttonCancel)).perform(click());                           // выйти
        assertFalse(mManager.isFailureDetected());                                          // счетчик сброшен
    }
}