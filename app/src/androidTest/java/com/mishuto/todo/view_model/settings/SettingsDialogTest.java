package com.mishuto.todo.view_model.settings;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.Time;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.EspressoHelper.scrollClick;
import static com.mishuto.todo.view_model.TestUtils.List.SETTINGS;
import static com.mishuto.todo.view_model.TestUtils.List.clickListOptionsMenu;
import static com.mishuto.todo.view_model.TestUtils.setTimePicker;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Тесты диалога настроек
 * Created by Michael Shishkin on 19.01.2018.
 */
public class SettingsDialogTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private static Settings.WorkTime WORK_TIME = Settings.getWorkTime();
    private static Settings.NotificationSettings NOTIFICATIONS = Settings.getNotificationSettings();

    //Установленные знвчения WorkTime сохраняются в БД
    @Test
    public void setWorkTime() throws Exception {
        WORK_TIME.isOn.set(false);
        WORK_TIME.onlyWorkDays.set(true);
        WORK_TIME.setBeginningWork(new Time(9,0));
        WORK_TIME.setEndWork(new Time(20,0));

        clickListOptionsMenu(SETTINGS);

        clickOn(R.id.settings_workTime);

        clickOn(R.id.settings_begin_label);
        setTimePicker(10, 30);

        clickOn(R.id.settings_end);
        setTimePicker(19, 0);

        clickOn(R.id.settings_workDayOnlySwitch);

        assertTrue(WORK_TIME.isOn.get());
        assertEquals(new Time(10, 30), WORK_TIME.getStartWork());
        assertEquals(new Time(19, 0), WORK_TIME.getEndWork());
        assertFalse(WORK_TIME.onlyWorkDays.get());
    }

    //Установленные знвчения Notifications сохраняются в БД
    @Test
    public void setNotification() throws Exception {
        NOTIFICATIONS.isOn.set(false);
        NOTIFICATIONS.doLight.set(false);
        NOTIFICATIONS.doSound.set(false);
        NOTIFICATIONS.doVibrate.set(true);

        clickListOptionsMenu(SETTINGS);

        clickOn(R.id.settings_notifySwitch);
        clickOn(R.id.settings_soundSwitch);
        clickOn(R.id.settings_vibrateSwitch);
        scrollClick(R.id.settings_indicatorSwitch);

        assertTrue(NOTIFICATIONS.isOn.get());
        assertTrue(NOTIFICATIONS.doSound.get());
        assertTrue(NOTIFICATIONS.doLight.get());
        assertFalse(NOTIFICATIONS.doVibrate.get());
    }

    // при включении/выключении рабочего времени - виджеты разрешены/запрещены
    @Test
    public void checkWTEnables() throws Exception {
        WORK_TIME.isOn.set(false);
        clickListOptionsMenu(SETTINGS);     // при загрузке disabled

        onView(withId(R.id.settings_workTime)).check(matches(isEnabled()));
        checkWTEnables(false);

        clickOn(R.id.settings_workTime);    // при клике на включение - все enabled
        checkWTEnables(true);

        clickOn(R.id.settings_workTime);    // при клике на включение - все disabled
        checkWTEnables(false);
    }

    private void checkWTEnables(boolean enabled) {
        onView(withId(R.id.settings_begin)).check(matches(enabled ?  isEnabled() : not(isEnabled())));
        onView(withId(R.id.settings_begin_label)).check(matches(enabled ?  isEnabled() : not(isEnabled())));
        onView(withId(R.id.settings_end)).check(matches(enabled ?  isEnabled() : not(isEnabled())));
        onView(withId(R.id.settings_end_label)).check(matches(enabled ?  isEnabled() : not(isEnabled())));
        onView(withId(R.id.settings_workDayOnlySwitch)).check(matches(enabled ?  isEnabled() : not(isEnabled())));
    }

    // при включении/выключении нотификаций - виджеты разрешены/запрещены
    @Test
    public void checkNotifyEnables() throws Exception {
        NOTIFICATIONS.isOn.set(false);
        clickListOptionsMenu(SETTINGS);

        onView(withId(R.id.settings_notifySwitch)).check(matches(isEnabled()));
        checkNotifyEnables(false);

        clickOn(R.id.settings_notifySwitch);
        checkNotifyEnables(true);

        clickOn(R.id.settings_notifySwitch);
        checkNotifyEnables(false);
    }

    private void checkNotifyEnables(boolean enabled) {
        onView(withId(R.id.settings_soundSwitch)).check(matches(enabled ?  isEnabled() : not(isEnabled())));
        onView(withId(R.id.settings_vibrateSwitch)).check(matches(enabled ?  isEnabled() : not(isEnabled())));
        onView(withId(R.id.settings_indicatorSwitch)).check(matches(enabled ?  isEnabled() : not(isEnabled())));
    }

    // некорректная установка начала/конца рабочего времени не сохраняется в объекте и вызывает снек
    @Test
    public void checkWTRestrictions() throws Exception {
        WORK_TIME.isOn.set(true);
        WORK_TIME.setBeginningWork(new Time(9,0));
        WORK_TIME.setEndWork(new Time(18,0));

        clickListOptionsMenu(SETTINGS);

        clickOn(R.id.settings_end);
        setTimePicker(12, 0);

        assertEquals(new Time(18, 0), WORK_TIME.getEndWork());
        onSnackBar().check(matches(isDisplayed())); // нарываемся  на снек
    }

    // при нажатии на текст Release Notes открыввется диалог Release Notes
    @Test
    public void clickReleaseNotes() {
        clickListOptionsMenu(SETTINGS);
        scrollClick(R.id.settings_whats_new);
        onView(withText(R.string.release_notes)).check(matches(isDisplayed()));
    }
}