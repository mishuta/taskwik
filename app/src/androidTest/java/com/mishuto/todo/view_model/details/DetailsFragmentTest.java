package com.mishuto.todo.view_model.details;

import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.widget.EditText;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.Transactions;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.events.TimerPublisher;
import com.mishuto.todo.model.tags_collections.ContextsSet;
import com.mishuto.todo.model.task_fields.Comments;
import com.mishuto.todo.model.task_fields.recurrence.SegmentSelector;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.common.NotificationControllerTest;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.common.TextUtils.uniqueStr;
import static com.mishuto.todo.view.Constants.OPAQUE_FULL;
import static com.mishuto.todo.view.Constants.OPAQUE_MID;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.hasAlpha;
import static com.mishuto.todo.view_model.EspressoHelper.hasItemsCount;
import static com.mishuto.todo.view_model.EspressoHelper.isButtonChecked;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.EspressoHelper.onTextInputLayout;
import static com.mishuto.todo.view_model.EspressoHelper.selectSpinner;
import static com.mishuto.todo.view_model.TestUtils.Automation.goHomeAndBack;
import static com.mishuto.todo.view_model.TestUtils.Automation.pressHome;
import static com.mishuto.todo.view_model.TestUtils.Automation.rotateBack;
import static com.mishuto.todo.view_model.TestUtils.Automation.rotateLeft;
import static com.mishuto.todo.view_model.TestUtils.Card.CANCEL;
import static com.mishuto.todo.view_model.TestUtils.Card.COMPLETE_BTN;
import static com.mishuto.todo.view_model.TestUtils.Card.OK;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.addComment;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.Card.setContext;
import static com.mishuto.todo.view_model.TestUtils.Card.setPrevTask;
import static com.mishuto.todo.view_model.TestUtils.Card.setTaskDate;
import static com.mishuto.todo.view_model.TestUtils.Card.setTitle;
import static com.mishuto.todo.view_model.TestUtils.List.SHOW_COMPLETED;
import static com.mishuto.todo.view_model.TestUtils.List.assertTaskExists;
import static com.mishuto.todo.view_model.TestUtils.List.assertTaskNotExists;
import static com.mishuto.todo.view_model.TestUtils.List.clickListOptionsMenu;
import static com.mishuto.todo.view_model.TestUtils.List.clickTask;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.getObjectsCount;
import static com.mishuto.todo.view_model.TestUtils.setTaskTimerAndSleep;
import static com.mishuto.todo.view_model.list.ListController.DELETE_SNACK_DURATION;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Тест меню тулбара карточки задач
 * Created by Michael Shishkin on 23.01.2018.
 */

public class DetailsFragmentTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);


   //при завершении задачи виджеты дизейблятся
    @Test
    public void completeTask() {
        if(!FilterManager.incomplete().isFilterSet())
            clickListOptionsMenu(SHOW_COMPLETED);

        Task task = createTask("completeTaskCard");

        clickOn(COMPLETE_BTN);

        onView(allOf(withText(task.toString()), not(isAssignableFrom(EditText.class)))).check(matches(not(isEnabled())));       // проверяем что виджет задачи запрещен
        onView(allOf(withText(task.toString()), not(isAssignableFrom(EditText.class)))).check(matches(hasAlpha(OPAQUE_MID)));   // и затемнен
        onView(withId(COMPLETE_BTN)).check(matches(isButtonChecked()));                                                 // кнопка Complete нажата

        clickOn(SAVE);

        onSnackBar().check(matches(withText(R.string.taskList_completedWarn))); // проверка вывода корректного снека

        TaskList.remove(task);
        clickListOptionsMenu(SHOW_COMPLETED);
    }

    // при заверешении задачи recurrence должен быть доступен
    @Test
    public void completedRecurrence() {
        createTask("completeTaskCard");
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), ViewActions.click());                             // устанавливаем Recurrence
        clickOn(R.id.recurrence_buttonOk);

        clickOn(COMPLETE_BTN);

        onView(withId(R.id.taskDetails_textView_recurrence)).check(matches(isEnabled()));                                       // виджет Recurrence разрешен
        onView(withId(R.id.taskDetails_textView_recurrence)).check(matches(hasAlpha(OPAQUE_FULL)));                             // и нормального цвета
        onView(withId(R.id.taskDetails_image_recWarn)).check(matches(not(isDisplayed())));                                      // иконка предупреждения выключена

        cancelTask();
    }


    // активация задачи в карточке приводит к разблокировке всех элементов
    @Test
    public void activateTaskCard() {

        Task task = createTask("activateTaskCard");
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), ViewActions.click()); // устанавливаем Recurrence
        clickOn(R.id.recurrence_buttonOk);

        clickOn(COMPLETE_BTN);
        clickOn(COMPLETE_BTN);

        onView(allOf(withText(task.toString()), not(isAssignableFrom(EditText.class)))).check(matches(isEnabled()));
        onView(allOf(withText(task.toString()), not(isAssignableFrom(EditText.class)))).check(matches(hasAlpha(OPAQUE_FULL)));
        onView(withId(R.id.taskDetails_textView_recurrence)).check(matches(isEnabled()));           // виджет Recurrence разрешен
        onView(withId(R.id.taskDetails_textView_recurrence)).check(matches(hasAlpha(OPAQUE_FULL))); // нормального цвета
        onView(withId(R.id.taskDetails_image_recWarn)).check(matches((isDisplayed())));             // иконка предупреждения включена
        onView(withId(R.id.taskDetails_clearRepeat)).check(matches(isEnabled()));                   // кнопка очистки recurrence активирована
        onView(withId(R.id.taskDetails_clearTerm)).check(matches(not(isEnabled())));                // кнопка очистки у даты (неустановлена) - деактивирована
        onView(withId(COMPLETE_BTN)).check(matches(not(isButtonChecked())));               // кнопка activate не нажата

        cancelTask();
    }

   // удаление существующей задачи
    @Test
    public void deleteExisting() {
        Task task = createTask("deleteInCard");
        clickOn(SAVE);
        clickTask(task);
        clickOn(R.id.toolbar_remove);
        checkRemoved(task);
    }

    // удаление новой задачи: снек не показывается, задача удалена из списка
    @Test
    public void deleteNew()  {
        Task task = createTask("deleteNew");
        clickOn(R.id.toolbar_remove);
        onSnackBar().check(doesNotExist());
        assertFalse(TaskList.getTasks().contains(task));
    }

    @SuppressWarnings("unchecked")
    public static void checkRemoved(Task t)  {
        int tasks = getObjectsCount(Task.class);

        onSnackBar().check(matches(isDisplayed()));
        assertTrue(TaskList.getTasks().contains(t));
        sleep(DELETE_SNACK_DURATION + 200);
        assertTaskNotExists(t.toString());
        assertFalse(TaskList.getTasks().contains(t));
        assertThat(getObjectsCount(Task.class), is(tasks-1));
    }

    // При создании задачи появляется снек, задача появляется в списке, объект задачи синхронен БД
    @Test
    public void newTask()  {
        Task task = createTask("newTask");
        clickOn(SAVE);
        sleep(200);
        String title = task.toString();

        onSnackBar().check(matches(withText(R.string.taskList_taskAdded)));     // снек задача добавлена
        assertTaskExists(title);                                                 // задача с таким именем существует
        task.reload();
        assertEquals(title, task.toString());

        TaskList.remove(task);
    }

    // Сохранение задачи после изменения - снек, изменение в списке, в БД
    @Test
    public void saveTask()  {
        Task task = createTask("saveTask");
        clickOn(SAVE);

        clickTask(task);
        String title = setTitle("saveTask N");
        clickOn(SAVE);
        sleep(200);
        onSnackBar().check(matches(withText(R.string.taskList_taskChangeSaved)));   // снек задача обновлена
        assertTaskExists(title);                                                 // в списке название обновилось
        task.reload();
        assertEquals(title, task.toString());                                   // в базе обновилось

        TaskList.remove(task);
    }

    // сохранение без изменений не вызывает снек
    @Test
    public void saveWithoutChanges() {
        Task task = createTask("saveWithoutChanges");
        clickOn(SAVE);

        clickTask(task);
        sleep(1000);            // ждем пропадания снека в списке
        clickOn(SAVE);
        onSnackBar().check(doesNotExist()); // снек не появляется
        assertTaskExists(task.toString());   // в списке название осталось

        TaskList.remove(task);
    }

    // отмена без изменений не вызывает снек и не требует подтверждения
    @Test
    public void cancelWithoutChanges() {
        Task task = createTask("saveWithoutChanges");
        clickOn(SAVE);

        clickTask(task);
        sleep(1000);            // ждем пропадания снека в списке
        clickOn(R.id.toolbar_cancel);
        onSnackBar().check(doesNotExist()); // нет снека об изменениях
        assertTaskExists(task.toString());   // в списке название осталось

        TaskList.remove(task);
    }

    // отмена новой задачи не добавляет задачу в список
    @Test
    public void cancelNew() {
        int n = TaskList.getTasks().size();
        createTask("cancelNew");
        cancelTask();
        onSnackBar().check(doesNotExist());
        onView((withId(R.id.recycler_view))).check(matches(hasItemsCount(n)));
    }

    //отмена отмены
    @Test
    public void cancelOfCancel() {
        Task task = createTask("cancelOfCancel");
        clickOn(R.id.toolbar_cancel);
        clickOn(CANCEL);
        clickOn(SAVE);
        assertTaskExists(task.toString());

        TaskList.remove(task);
    }

    // отмена задачи восстанавливает объект и не изменяет список
    @Test
    public void cancelTaskTest() {
        Task task = createTask("cancelTaskTest");
        String title = task.toString();
        clickOn(SAVE);

        clickTask(task);
        setTitle("Changed: cancelTaskTest");
        cancelTask();

        sleep(100);
        assertTaskExists(title);
        task.reload();
        assertEquals(title, task.toString());

        TaskList.remove(task);
    }

    // отмена задачи восстанавливает значение справочника
    @Test
    public void cancelTaskWithContext() {
        int n = ContextsSet.getInstance().getTags().size();
        createTask("cancelTaskTest");

        setContext("cancelTaskTest_context");
        cancelTask();

        assertEquals(n, ContextsSet.getInstance().getTags().size());
    }

    // отмена задачи восстанавливает значение счетчика ссылок у предыдущей задачи
    @Test
    public void cancelTaskWithPrevTask() throws Exception {
        Task prev = createTask("cancelTaskWithPrevTask - prev");
        clickOn(SAVE);

        createTask("cancelTaskWithPrevTask");

        setPrevTask(prev);
        cancelTask();

        Integer n = (Integer) TestUtils.Reflection.execute(DBObject.class, "getRefCounter", prev);

        assertThat(n, is(1));

        TaskList.remove(prev);
    }

    //сохраненные комментарии не сохраняются в базе после отменены задачи
    @Test
    public void cancelWithSavedComments() {
        int n = getObjectsCount(Comments.Description.class);

        createTask("cancelWithSavedComments");
        addComment("Comment: cancelWithSavedComments");
        cancelTask();

        assertThat(getObjectsCount(Comments.Description.class), is(n));
    }

    //после отмены задачи, зависимая задача восстановила статус
    @Test
    public void cancelStatus() {
        Task prev = createTask("cancelStatus - prev");
        clickOn(SAVE);

        Task dependent = createTask("cancelStatus - dependent");
        setPrevTask(prev);                          // устанавливаем предыдущую задачу
        clickOn(SAVE);

        clickTask(prev);
        clickOn(COMPLETE_BTN);                  // завершаем предыдущую задачу
        assertTrue(dependent.isStateActive());  // убеждаемся что зависимая стала активной

        cancelTask();                           // отменяем изменения
        assertTrue(dependent.isSateDeferred()); // убеждаемся что зависимая стала отложенной

        TaskList.remove(prev);
        TaskList.remove(dependent);
    }

    // после отмены задачи, таймер должен восстановиться
    @Test
    public void cancelTimer() {
        TCalendar date = TCalendar.now();
        date.add(Calendar.DATE, 1);

        Task task = createTask("cancelTimer");
        setTaskDate(date);
        clickOn(SAVE);

        TCalendar timer = task.getTimerTime().clone();
        date.add(Calendar.DATE, 3);

        clickTask(task);
        setTaskDate(date);
        assertNotEquals(timer, task.getTimerTime());

        cancelTask();
        assertEquals(timer.get(Calendar.DATE), task.getTaskTime().getDay());
        assertEquals(timer, task.getTimerTime());

        TaskList.remove(task);
    }

    //кнопка back ведет себя как cancel
    @Test
    public void backIsCancel() {
        int n = TaskList.getTasks().size();
        createTask("backIsCancel");
        sleep(200);
        pressBack();
        sleep(600);
        clickOn(OK);
        onView((withId(R.id.recycler_view))).check(matches(hasItemsCount(n)));
    }

    // две повторяемые задачи, сработавшие во время открытой карточки задачи, сохраняют статус после отмены задачи в карточке
    // проверка работы блокировок и параллельных изменений задач во время открытой транзакции
    @Test
    public void cancelRecurrenceState() throws Exception {
        Task rec1 = createRepeatedTask("cancelRecurrenceState - RecA");
        Task rec2 = createRepeatedTask("cancelRecurrenceState - RecB");

        createTask("cancelRecurrenceState - main");

        assertTrue(rec1.isStateRepeated());
        assertTrue(rec2.isStateRepeated());

        sleep(DELAY * 1000);

        assertTrue(rec1.isStateActive());
        assertTrue(rec2.isStateActive());

        cancelTask();

        assertTrue(rec1.isStateActive());
        assertTrue(rec2.isStateActive());
        sleep(3000);

        TaskList.remove(rec1);
        TaskList.remove(rec2);


    }

    private final static int DELAY = 9;
    // создание завершенной повторяемой задачи, которая должна повторится через DELAY сек
    private Task createRepeatedTask(String prefix) throws Exception {
        Task task = createTask(prefix);
        clickOn(COMPLETE_BTN);                  //завершаем задачу
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click()); // делаем ее повторяемой
        clickOn(R.id.recurrence_buttonOk);
        clickOn(SAVE);                          // сохраняем

        TCalendar tCalendar = TCalendar.now();  // устанавливаем таймер на DELAY секунд в будущем
        tCalendar.add(Calendar.SECOND, DELAY);
        TimerPublisher timerPublisher = (TimerPublisher) TestUtils.Reflection.getField(Task.class, task, "mTimer");
        timerPublisher.setTime(tCalendar);

        return task;
    }

    /* ***********************************/
    /* Тестирование перехода в бэкграунд */
    /* ***********************************/

    // Переход в бэкграунд и обратно в карточке не влияет на процесс сохранения изменений
    @Test
    public void commitAfterHome() throws Exception {
        Task task = createTask("commitAfterHome");
        clickOn(SAVE);

        String title = uniqueStr(TaskList.getTasks(), "commitAfterHome - changed");
        clickTask(task);
        setTitle(title);
        goHomeAndBack();
        clickOn(SAVE);

        assertTaskExists(title);
        assertEquals(title, task.toString());

        TaskList.remove(task);
    }

    // Пользователь открывает карточку, переводит приложение в фон, получает нотификацию, открывает список
    // Все введенные данные задачи в карточке должны сохраниться, все транзакции - закрыться
    @Test
    public void commitAfterNotify() throws Exception {
        Settings.getNotificationSettings().isOn.set(true);

        Task notifyTask = createTask("commitAfterNotify - notify");                 // задача, вызывающая нотификацию
        clickOn(SAVE);

        String title = uniqueStr(TaskList.getTasks(), "commitAfterNotify");
        Task task = createTask(title);                                              // сохраняемая задача

        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());             // открываем диалог Recurrence
        selectSpinner(R.id.recurrence_page_selector, SegmentSelector.class, SegmentSelector.MONTH); // выбираем месяц
        onTextInputLayout(R.id.rec_mon_monDay).perform(replaceText("10"));                          // устанавливаем 10-е число

        pressHome();
        setTaskTimerAndSleep(notifyTask);                                           // создаем таймер и дожидаемся нотификации
        NotificationControllerTest.clickOnNotification(1);                          // кликаем по нотификации
        sleep(100);

        assertFalse(Transactions.inTransaction());                                  // все транзакции закрыты
        assertThat(task.getRecurrence().getMonth().getDayOfMonth(), is(10));        // повтор сохранен
        clickOn(SAVE);
        assertTaskExists(title);                                                    // задача с заданным именем появилась в списке

        TaskList.remove(task);
        TaskList.remove(notifyTask);
    }

    //сохранение при переводе в бэк, ротации, прибивании системой
    //комменты: добавить коммент, сохранить через бэкграунд, отменить - ФР: обновился виджет комментов
    // отмена после автосохранения после бэка и кила для обычного поля и для комита

    //Отмена новой неизмененной задачи после перехода в бэк приводит к удалению задачи из списка
    @Test
    public void cancelUnchangedAfterBack() throws Exception {
        int n = TaskList.getTasks().size();
        clickOn(R.id.floatingActionButton);
        goHomeAndBack();
        sleep(600);
        onSnackBar().check(doesNotExist()); // нет снека о сохранении
        clickOn(R.id.toolbar_cancel);
        onSnackBar().check(doesNotExist()); // нет снека об изменениях

        assertEquals(n, TaskList.getTasks().size());
    }

    //Отмена задачи после сохранения в бэкграунде приводит к сохранению изменений в списке и отсутствию запроса на подтверждение отмены
    @Test
    public void cancelAfterSaveInBack() throws Exception {
        Task task = createTask("cancelAfterSaveInBack");
        clickOn(SAVE);

        clickTask(task);
        String title = setTitle(task.toString() + " - changed");
        goHomeAndBack();
        sleep(600);

        onSnackBar().check(matches(withText(R.string.taskList_taskChangeSaved))); // снек о сохраненных изменениях

        clickOn(R.id.toolbar_cancel);
        onSnackBar().check(doesNotExist()); // нет снека об изменениях

        assertTaskExists(title);
        assertEquals(title, task.toString());

        TaskList.remove(task);
    }

    //поворот не должен приводить к сохранению
    @Test
    public void cancelRotated() throws Exception {
        Task task = createTask("cancelRotated");
        String title = task.toString();
        clickOn(SAVE);

        clickTask(task);
        setTitle(title + " - changed");
        rotateLeft();
        onSnackBar().check(doesNotExist()); // нет снека о сохранении
        rotateBack();
        sleep(500);

        cancelTask();                       // запрашивается подтверждение
        sleep(100);
        assertTaskExists(title);            // в списке задача со старым именем

        TaskList.remove(task);
    }
}