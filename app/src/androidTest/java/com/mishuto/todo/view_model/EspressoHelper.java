package com.mishuto.todo.view_model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.action.ViewActions;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewParent;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.mishuto.todo.view.ImageCheckedButton;
import com.mishuto.todo.view.LayoutWrapper;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollTo;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;

/**
 * Содержит набор вспомогательных статических методов ViewInteractions, Matches
 * для использования в Espresso тестах
 * Created by Michael Shishkin on 22.07.2017.
 */

@SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
public class EspressoHelper {
    // выполнить клик по view
    public static ViewInteraction clickOn(int resId) {
        return onView(withId(resId)).perform(click());
    }

    // проскроллить до view и кликнуть
    public static ViewInteraction scrollClick(int resId) {
        return onView(withId(resId)).perform(ViewActions.scrollTo(), click());
    }

    // устанавливает спинер
    public static <T> ViewInteraction selectSpinner(int spinnerId, Class<T> itemClass, T item) {
        clickOn(spinnerId);                                         // клик по спинеру
        return onData(allOf(is(instanceOf(itemClass)), is(item))).  // установка спинера
                inRoot(isPlatformPopup()).perform(click());
    }

    //проверяет выбранный элемент в спиннере
    public static ViewInteraction checkSelectedSpinner(int spinnerId, String itemLabel) {
        return onView(withId(spinnerId)).check(matches(withSpinnerText(itemLabel)));
    }

    public static ViewInteraction onSnackBar() {
        return onView(withId(android.support.design.R.id.snackbar_text));
    }

    // нажать кнопку Action в снеке
    public static ViewInteraction clickOnSnackButton() {
        return onView(withId(android.support.design.R.id.snackbar_action)).perform(ViewActions.click());
    }

    // Получение EditText, заключенного внутри TextInputLayout по id ресурса
    public static ViewInteraction onTextInputLayout(int textInputLayoutId) {
        return onView(
                allOf(                                          // найти View для которой верно:
                        isDescendantOfA(withId(textInputLayoutId)), // является дочерним от view с id=textInputLayoutId
                        isAssignableFrom(EditText.class)            // наследуется от EditText
                )
        );
    }

    // Получение EditText, заключенного внутри TextInputLayout по матчеру
    public static ViewInteraction onTextInputLayout(Matcher<View> textInputLayout) {
        return onView(allOf(isDescendantOfA(textInputLayout), isAssignableFrom(EditText.class)));
    }

    // Скроллирует RecyclerView на view, указанную матчером, после чего возвращает ViewInteraction для найденной view.
    public static ViewInteraction onRecyclerView(int recycler_id, Matcher<View> matcher) {
        onView(withId(recycler_id)).perform(scrollTo(hasDescendant(matcher)));
        return onView(matcher);
    }

    // Аналог onRecyclerView(...) для коллапсирующего AppBar
    // Обходит баг в эспрессо со скроллингом: сначала свайпит вверх для коллапса тулбара, а затем выполняет скролл
    public static ViewInteraction onRecyclerViewInCollapsedAppBar(Matcher<View> recycler, Matcher<View> child) {
        onView(withId(android.R.id.content)).perform(ViewActions.swipeUp());
        onView(recycler).perform(scrollTo(hasDescendant(child)));
        return onView(child);
    }

    /**
     * Матчер, получающий View внутри элемента RecyclerView с заданной позицией
     * Неприменим для случая, когда в разметке несколько одинаковых recyclerViewId
     * @param recyclerViewId id RecyclerView
     * @param targetViewId id View внутри выбранного элемента RecyclerView
     * @param position позиция выбранного элемента RecyclerView
     * @return сматченный view
     */
    public static Matcher<View> withRecyclerPositionItemView(final int recyclerViewId, final int targetViewId, final int position) {
        return new TypeSafeMatcher<View>() {
            View mItemView;

            @Override
            public void describeTo(Description description) {
                description.appendText("view [" + targetViewId + "] in position " + position + " in RecyclerView [" + recyclerViewId + "]");
            }

            @Override
            public boolean matchesSafely(View view) {
                if (mItemView == null ) {
                    RecyclerView recyclerView = view.getRootView().findViewById(recyclerViewId);

                    if (recyclerView != null )
                        mItemView = recyclerView.findViewHolderForAdapterPosition(position).itemView;
                    else
                        return false;
                }
                return view == mItemView.findViewById(targetViewId);
            }
        };
    }


    /**
     * матчит RecyclerView, у которого есть view в заданной позиции
     * @param targetView - матчер view, которую ищем в нужной позиции
     * @param position - номер позиции
     * @return матчер RecyclerView
     *
     * пример использования:
     * onView(allOf(withId(R.id.recycler_view), isDisplayed()))
     * .check(matches(hasViewInPosition(allOf(withId(R.id.textView), withText(R.string.no_project)), 0)));
     */
    public static Matcher<View> hasViewInPosition(final Matcher<View> targetView, final int position) {
        return new TypeSafeMatcher<View>() {

            @Override
            public void describeTo(Description description) {
                targetView.describeTo(description.appendText("view in position " + position + " "));
            }

            @Override
            public boolean matchesSafely(View view) {
                if (view instanceof RecyclerView ) {    // для RecyclerView получаем ItemView заданной позиции
                    View childItem = ((RecyclerView)view).findViewHolderForAdapterPosition(position).itemView;
                    for(View subView : LayoutWrapper.createLayoutIterator(childItem))
                        if(targetView.matches(subView))
                            return true;
                }

                return false;
            }
        };
    }

    /**
     * Матчит view, которая находится в заданной позиции заданного RecyclerView
     * @param recycler матчер RecyclerView, в котором ищется view
     * @param position позиция в RecyclerView
     * @return  матчер view, подпадающей под условия
     *
     * пример использования:
     * onView(allOf(withId(R.id.textView),                                      // view c id = R.id.textView
     * withPositionIn(allOf(withId(R.id.recycler_view), isDisplayed()), 0)))    // у которой позиция 0 в отображаемом RecyclerView с id = R.id.recycler_view
     * .perform(click());
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public static Matcher<View> withPositionIn(final Matcher<View> recycler, final int position) {
        return new TypeSafeMatcher<View>() {

            @Override
            protected boolean matchesSafely(View item) {
                View recyclerView = getParent(item.getParent(), recycler);
                return recyclerView != null && recyclerView instanceof RecyclerView
                        && getParent(item.getParent(), ((RecyclerView) recyclerView).findViewHolderForAdapterPosition(position).itemView) != null;
            }

            @Override
            public void describeTo(Description description) {
                recycler.describeTo(description.appendText("view in position " + position + " "));
            }

            private View getParent(ViewParent viewParent, Object parent) {
                if (!(viewParent instanceof View))
                    return null;

                if(parent instanceof Matcher) {
                    if (((Matcher)parent).matches(viewParent))
                        return (View)viewParent;

                    else
                        return getParent(viewParent.getParent(), parent);
                }
                else if(parent instanceof View)
                      return  (parent == viewParent) ? (View)viewParent : getParent(viewParent.getParent(), parent);

                return null;
            }
        };
    }

    /**
     * Матчит view, которая находится в заданной позиции заданного GridView
     * @param gridView матчер GridView, в котором ищется view
     * @param position позиция в GridView
     * @return  матчер view, подпадающей под условия
     *
     * пример использования:
     * onView(withPositionInGrid(withId(R.id.grid_view), 6) ).perform(click());  // кликнуть по 6-й кнопке (счет с 0)

     */
    @SuppressWarnings("TypeMayBeWeakened")
    public static Matcher<View> withPositionInGrid(final Matcher<View> gridView, final int position) {
        return new TypeSafeMatcher<View>() {

            @Override
            protected boolean matchesSafely(View item) {
                ViewParent parent = item.getParent();

                return parent instanceof View && gridView.matches(parent) &&
                        item == (((GridView) parent).getChildAt(position));
            }

            @Override
            public void describeTo(Description description) {
                gridView.describeTo(description.appendText("view in position " + position + " "));
            }

        };
    }

    /** Матчер, получающий количество элементов в RecyclerView и Spinner
     * @param count количество элементов, которое должно быть у списка/спинера
     * @return матчер списка/спинера с заданным количеством элементов
     * Пример использования:
     * onView(withId(R.id.taskDetails_Spinner_Priority)).check(matches(hasItemsCount(2)));
     */
    public static Matcher<View> hasItemsCount(final int count) {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View view) {

                if(view instanceof RecyclerView)
                    return  ((RecyclerView)view).getAdapter().getItemCount() == count;

                else
                    return (view instanceof Spinner) && ((Spinner)view).getAdapter().getCount() == count;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("RecycleView with itemCount: " + count);
            }
        };
    }

    // view имеет непрозрачность = alpha (почему-то метода ViewMatchers.withAlpha() на самом деле нет)
    public static Matcher<View> hasAlpha(final float alpha) {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View view) {
                return (view.getAlpha() == alpha);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("view has alpha " + alpha);
            }

            @Override
            protected void describeMismatchSafely(View item, Description mismatchDescription) { // не вызывается в случае ошибки по неизвестной причине
                mismatchDescription.appendText(String.format("View's alpha is %f", item.getAlpha()));
            }
        };
    }

    // проверка состояния нажатия кнопки класса ImageCheckedButton
    public static Matcher<View> isButtonChecked() {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                return item instanceof ImageCheckedButton && ((ImageCheckedButton)item).isChecked();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checked state of ImageCheckedButton");
            }
        };
    }

    //матчер ImageView имеет иконку с заданным id
    public static Matcher<View> withImage(final int imageId) {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View target) {
                if (!(target instanceof ImageView)){
                    return false;
                }

                Drawable pattern = target.getContext().getResources().getDrawable(imageId);

                if (pattern == null) {
                    return false;
                }

                Bitmap targetBitmap = getBitmap (((ImageView)target).getDrawable());
                Bitmap patternBitmap = getBitmap(pattern);
                return targetBitmap.sameAs(patternBitmap);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has image with id: " + imageId);
            }

            private Bitmap getBitmap(Drawable drawable){
                Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                drawable.draw(canvas);
                return bitmap;
            }
        };
    }



    // пустой матчер, используется для отладки кода Espresso для того чтобы получить в дебаге найденный виджет
    // пример использования:
    // onView(allOf(withText(R.string.listMenu_showCompleted), emptyMatcher())).check(matches(isDisplayed()));
    public static Matcher<View> emptyMatcher() {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("empty matcher, only for debug");
            }
        };
    }
}
