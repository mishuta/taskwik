package com.mishuto.todo.view_model.details.field_controllers;

import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.task_fields.Priority;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.mishuto.todo.model.task_fields.Priority.DEFAULT_NEW_PRIORITY;
import static com.mishuto.todo.model.task_fields.Priority.NO_IMPORTANT;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.hasItemsCount;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.Card.checkPriority;
import static com.mishuto.todo.view_model.TestUtils.Card.setPrevTask;
import static com.mishuto.todo.view_model.TestUtils.Card.setPriority;
import static com.mishuto.todo.view_model.TestUtils.Card.setTaskDate;
import static com.mishuto.todo.view_model.TestUtils.Card.setTaskTime;
import static com.mishuto.todo.view_model.TestUtils.List.clickTask;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.hamcrest.CoreMatchers.not;

/**
 * Проверка изменения приоритета в карточке
 * Created by Michael Shishkin on 23.04.2017.
 */
public class PriorityControllerTest  {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);


    //установленный в карточке приоритет сохраняется при повторном вхождении в карточку
    @Test
    public void changePriority() {
        Task task = createTask("changePriority");
        setPriority(Priority.IMPORTANT);
        sleep(300);
        clickOn(SAVE);

        clickTask(task);
        checkPriority(Priority.IMPORTANT);
        clickOn(SAVE);

        TaskList.remove(task);
    }

    //установленный в карточке приоритет с выбранным временем, сохраняется при повторном вхождении в карточку
    @Test
    public void changePriority2() {
        Task task = createTask("changePriority2");
        TCalendar time = TCalendar.now();
        time.add(Calendar.DATE, 1);
        setTaskDate(time);
        setTaskTime(time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE));
        setPriority(Priority.URGENTLY);
        clickOn(SAVE);

        clickTask(task);
        checkPriority(Priority.URGENTLY);
        clickOn(SAVE);

        TaskList.remove(task);
    }

    // приоритет меняется на дефолтовый при установке времени и возвращается к старому значению при сбросе времени
    @Test
    public void changePriorityByTime() {
        createTask("changePriorityByTime");

        setPriority(NO_IMPORTANT);                              // устанавливаем приоритет =4
        sleep(500);
        setTaskTime(23,59);                                     // устанавливаем время
        sleep(100);

        checkPriority(DEFAULT_NEW_PRIORITY);                    // проверяем, что приоритет поменялся на DEFAULT_NEW_PRIORITY
        onView(withId(R.id.taskDetails_image_priorityWarn))
                .check(matches(isDisplayed()));                 // проверяем что иконка с предупреждением отображается

        clickOn(R.id.taskDetails_clearTerm);                    // стираем дату

        onView(withId(R.id.taskDetails_image_priorityWarn))
                .check(matches(not(isDisplayed())));            // проверяем что иконка с предупреждением не отображается

        cancelTask();
    }

    // проверка корректной смены адаптеров при переходе через разные статусы
    @Test
    public void checkChangeAdapters() {
        Task prev = TaskList.addNew();
        prev.getTitle().setTaskName(TextUtils.uniqueStr(TaskList.getTasks(), "checkChangeAdapters"));

        createTask("checkChangeAdapters");

        setTaskTime(23,59);
        onView(withId(R.id.taskDetails_Spinner_Priority)).check(matches(hasItemsCount(2)));

        setPrevTask(prev);
        onView(withId(R.id.taskDetails_Spinner_Priority)).check(matches(hasItemsCount(1)));

        onView(withId(R.id.taskDetails_clearPrev)).perform(ViewActions.scrollTo(), click());
        onView(withId(R.id.taskDetails_Spinner_Priority)).check(matches(hasItemsCount(4)));

        cancelTask();
        TaskList.remove(prev);
    }
}