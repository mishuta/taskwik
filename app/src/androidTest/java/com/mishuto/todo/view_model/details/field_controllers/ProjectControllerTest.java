package com.mishuto.todo.view_model.details.field_controllers;

import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.ProjectsSet;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setProject;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.getObjectsCount;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isIn;

/**
 * Тест изменение проекта в карточке задачи
 * Created by Michael Shishkin on 23.05.2017.
 */
public class ProjectControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private StringContainer mProj1;
    private StringContainer mProj2;
    private int mCount;

    @Before
    public void setUp() {
        mCount = getObjectsCount(StringContainer.class);
    }

    @Test
    public void changeProject() {
        mProj1 = new StringContainer(TextUtils.uniqueStr(ProjectsSet.getInstance().getTags(), "changeProjectA"));
        mProj2 = new StringContainer(TextUtils.uniqueStr(ProjectsSet.getInstance().getTags(), "changeProjectB"));

        Task task = createTask("changeProject");
        setProject(mProj1.toString());
        setProject(mProj2.toString());

        SystemClock.sleep(300);
        onView(withId(R.id.taskDetails_project)).check(matches(withText(mProj2.toString())));                   // проверяем что в карточке обновился проект

        ProjectsSet projectsSet = ProjectsSet.getInstance();

        clickOn(SAVE);

        SystemClock.sleep(400);
        task.reload();
        assertThat(task.getProject(), is(mProj2));                                                             // в задаче сохранился mProj2
        assertThat(mProj1, isIn(projectsSet.getTags()));                                                        // mProj1 и mProj2 есть в в множестве проектов
        assertThat(mProj2, isIn(projectsSet.getTags()));

        TaskList.remove(task);
    }

    @After
    public void tearDown() {
        ProjectsSet.getInstance().remove(mProj1);
        ProjectsSet.getInstance().remove(mProj2);
        assertThat(getObjectsCount(StringContainer.class), is(mCount));
    }
}