package com.mishuto.todo.view_model.details.field_controllers;

import android.support.test.rule.ActivityTestRule;
import android.widget.EditText;

import com.mishuto.todo.database.Transactions;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerView;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.EspressoHelper.onTextInputLayout;
import static com.mishuto.todo.view_model.TestUtils.Automation.goHomeAndBack;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.List.clickTask;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static junit.framework.Assert.assertFalse;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;

/**
 * Проверка функционала комментариев задачи
 * Created by Michael Shishkin on 16.05.2017.
 */

public class CommentControllerTest  {

    private static String COMMENT = "Test comment";
    private static String COMMENT2 = "Test comment 2";

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // должно отоброжаться количество комментариев, только если их > 1
    @Test
    public void checkCommentsCount()  {
        createTask("checkCommentsCount");
        onView(withId(R.id.taskDetails_comment_count)).check(matches(not(isDisplayed())));  // 0
        TestUtils.Card.addComment(COMMENT);

        onView(withId(R.id.taskDetails_comment_count)).check(matches(not(isDisplayed())));  // 1
        TestUtils.Card.addComment(COMMENT2);

        onView(withId(R.id.taskDetails_comment_count)).check(matches(withText("+ 1")));      // 2
        cancelTask();
    }

    @Test
    public void addComment() {
        Task task = createTask("addComment");
        TestUtils.Card.addComment(COMMENT);
        checkCommentInController(COMMENT);
        clickOn(SAVE);

        checkCommentInTaskList(task, COMMENT);
        assertThat(task.getComments().getList(), hasSize(1));
        assertThat(task.getComments().getActual().getComment(), is(COMMENT));

        TaskList.remove(task);
    }


    @Test
    public void deleteComment() {
        Task task = createTask("deleteComment");
        TestUtils.Card.addComment(COMMENT);                     // сохраняем коммент
        clickOn(R.id.taskDetails_comment);                      // повторно вызываем диалог
        clickOn(R.id.dateView).perform(closeSoftKeyboard());    // клик по комменту для отображения кнопок
        clickOn(R.id.buttonDelete);                             // удаляем комментарий
        pressBack();                                            // закрываем диалог
        checkCommentInController("");
        clickOn(SAVE);

        checkCommentInTaskList(task, "");
        TaskList.remove(task);
    }

    @Test
    public void editComment() {
        Task task = createTask("editComment");
        TestUtils.Card.addComment(COMMENT);                                                         // вводим первый комментарий
        clickOn(R.id.taskDetails_comment);
        clickOn(R.id.dateView);
        sleep(100);
        onTextInputLayout(R.id.commentView).perform(replaceText(COMMENT2), closeSoftKeyboard());    // стираем первый, пишем второй
        clickOn(R.id.buttonOk);                                                                     // ок
        pressBack();                                                                                // закрыть диалог

        checkCommentInController(COMMENT2);
        clickOn(SAVE);

        checkCommentInTaskList(task, COMMENT2);
        assertThat(task.getComments().getList(), hasSize(1));                              // количество комментариев = 1
        assertThat(task.getComments().getActual().getComment(), is(COMMENT2));             // комментарий = COMMENT2
        TaskList.remove(task);
    }



    // отмена изменения
    @Test
    public void cancelComment() {
        Task task = createTask("cancelComment");
        TestUtils.Card.addComment(COMMENT);                                 // вводим первый комментарий
        clickOn(R.id.taskDetails_comment);
        clickOn(R.id.dateView);
        sleep(100);

        onTextInputLayout(R.id.commentView).perform(replaceText(COMMENT2), closeSoftKeyboard());    // стираем первый, пишем второй
        clickOn(R.id.buttonCancel);                                                                 // cancel

        onView(allOf(isDescendantOfA(withId(R.id.commentView)), not(isAssignableFrom(EditText.class)))).check(matches(withText(COMMENT)));
        pressBack();                                                                                // закрыть диалог
        checkCommentInController(COMMENT);
        clickOn(SAVE);
        checkCommentInTaskList(task, COMMENT);

        TaskList.remove(task);
    }

    //Отмена комментария и задачи после сохранения в бэкграунде
    //комментарий должен сохраниться
    //должен обновиться в карточке
    //должен обновиться в списке
    // отмена задачи не запрашивает подтверждения
    @Test
    public void cancelCommentAfterSave() throws Exception {
        Task task = createTask("cancelCommentAfterSave");
        clickOn(SAVE);

        String comment = "cancelCommentAfterSave - comment";
        clickTask(task);
        TestUtils.Card.addComment(comment);

        clickOn(R.id.taskDetails_comment);
        clickOn(R.id.dateView);
        sleep(100);
        onTextInputLayout(R.id.commentView).perform(replaceText(COMMENT2), closeSoftKeyboard()); // стираем первый, пишем второй
        clickOn(R.id.buttonOk);

        goHomeAndBack();

        pressBack();
        //onSnackBar().check(matches(withText(R.string.taskList_taskChangeSaved)));   // снек о сохраненных изменениях [закрывается раньше чем отрабатывает проверка]

        checkCommentInController(COMMENT2);
        assertEquals(COMMENT2, task.getComments().getActual().getComment());        // комментарий сохранен
        clickOn(R.id.toolbar_cancel);                                               // отменяем задачу
        sleep(200);
        assertFalse(Transactions.inTransaction());                                  // транзакции закрыты
        onSnackBar().check(doesNotExist());                                         // нет снека о сохранении
        checkCommentInTaskList(task, COMMENT2);

        TaskList.remove(task);
    }

    private void checkCommentInController(String comment) {
        onView(withId(R.id.taskDetails_comment)).check(matches(withText(comment)));
    }

    private void checkCommentInTaskList(Task task, String comment) {
        onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_pos1), withParent(hasSibling(withChild(withText(task.toString()))))))
                .check(matches(withText(comment)));
    }
}
