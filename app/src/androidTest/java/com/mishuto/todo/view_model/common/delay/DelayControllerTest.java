package com.mishuto.todo.view_model.common.delay;

import android.support.test.espresso.contrib.PickerActions;
import android.support.test.rule.ActivityTestRule;
import android.widget.DatePicker;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.database.DBField;
import com.mishuto.todo.database.SQLHelper;
import com.mishuto.todo.database.SQLTable;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.mishuto.todo.common.TCalendar.now;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.EspressoHelper.scrollClick;
import static com.mishuto.todo.view_model.EspressoHelper.withPositionInGrid;
import static com.mishuto.todo.view_model.TestUtils.Card.COMPLETE_BTN;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.Card.setPrevTask;
import static com.mishuto.todo.view_model.TestUtils.List.DELAY;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.DECEMBER;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Проверка функционала Delay
 * Created by Michael Shishkin on 23.01.2018.
 */
public class DelayControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // Клик по фиксированной кнопке откладывает задачу на заданное ей время, а в БД сохраняется статистика нажатия
    @Test
    public void fixTimeDelay() {
        Task task = createTask("fixTimeDelay");
        clickOn(R.id.toolbar_delay);                                                                // Выбираем меню Отложить

        onView(withPositionInGrid(withId(R.id.grid_view), 5) ).perform(click());                    // Кликаем по кнопке 5 ("Завтра")
        clickOn(SAVE);

        sleep(200);
        task.reload();

        assertEquals(1, task.getTaskTime().getCalendar().intervalFrom(Calendar.DATE, now()));       // Проверяем что начало задачи через на 1 день

        SQLTable sqlTable = new SQLTable("DELAYER_STAT");
        SQLHelper.DBCursor cursor = sqlTable.query("_id = (select max(_id) from DELAYER_STAT)");    // Читаем последнюю записанную строку в DELAYER_STAT

        assertEquals((Integer) 4, DBField.getInteger("BUTTON_NUMBER", cursor));                     // Завтра = кнопка №4
        assertEquals(task.getTaskTime().getCalendar(), DBField.getCalendar("TARGET_DATE", cursor)); // TargetDate = времени начала задачи
        assertEquals(0, now().intervalFrom(MINUTE, (TCalendar) DBField.getCalendar("START_DATE", cursor)));     // StartDate = now() с точностью до минуты

        cursor.close();
        TaskList.remove(task);
    }

    // Клик по 8-й конопке в списке задач откладывает задачу на неделдю
    @Test
    public void delayInList() {
        Task task = createTask("delayInList");
        clickOn(SAVE);
        TestUtils.List.clickTaskListMenu(task, DELAY);                                          // открываем диалог в меню
        onView(withPositionInGrid(withId(R.id.grid_view), 8) ).perform(click());                // кликаем по 8-й кнопке (отложить на неделю)
        assertTrue(task.getTaskTime().isDateSet());                                             // у задачи непустой TaskTime
        assertEquals(7, task.getTaskTime().getCalendar().intervalFrom(Calendar.DATE, now()));   // отстоящий на 7 дней вперед
        TaskList.remove(task);
    }

    // Клик по кастом-кнопке открывает окно диалога для установки кастом-дата. Время не выбирается
    @Test
    public void customTimeDelayWithoutTime() {
        final TCalendar CHECK_DATE = new TCalendar(2017, DECEMBER, 10);         // тестовая дата
        Settings.getWorkTime().isOn.set(false);
        Task task = createTask("customTimeDelayWithoutTime");
        clickOn(R.id.toolbar_delay);

        onView(withId(R.id.image)).perform(click());                            // клик по кастом-кнопке

        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).    // выбираем дату в пикере
                perform(PickerActions.setDate(CHECK_DATE.get(YEAR), CHECK_DATE.get(MONTH) + 1, CHECK_DATE.get(DAY_OF_MONTH)));
        scrollClick(R.id.buttonOk);                                             // ОК

        assertEquals(CHECK_DATE, task.getTaskTime().getCalendar());             // дата установлена
        assertFalse(task.getTaskTime().isTimeSet());                            // время не установлено

        cancelTask();
    }

    // Завершенную задачу нельзя отложить
    @Test
    public void completedTaskDoesNotDelayed() {
        createTask("completedTaskDoesNotDelayed");
        clickOn(COMPLETE_BTN);                              // завершаем задачу
        clickOn(R.id.toolbar_delay);                        // пытаемся отложить
        onSnackBar().check(matches(isDisplayed()));         // появляется предупреждение
        onView(withId(R.id.image)).check(doesNotExist());   // а диалога с иконкой - нет

        cancelTask();
    }

    // появляется предупреждение при наличии предыдущей задачи
    @Test
    public void delayTaskWithPrevious()  {
        Task prevTask = createTask("delayTaskWithPrevious - P");    // создаем предыдущую задачу
        clickOn(SAVE);
        Task task = createTask("delayTaskWithPrevious - A");
        setPrevTask(prevTask);                                      // устанавливаем предыдущую задачу
        clickOn(R.id.toolbar_delay);
        onView(withPositionInGrid(withId(R.id.grid_view), 6) ).perform(click()); // Кликаем по кнопке 6 ("Завтра")
        onSnackBar().check(matches(isDisplayed()));                 // появляется снек, что предыдущая сброшена
        clickOn(SAVE);

        assertNull(task.getPrev());                                 // предыдущая - сброшена

        TaskList.remove(prevTask);
        TaskList.remove(task);
    }

}