package com.mishuto.todo.view_model.details.field_controllers;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.DateFormatWrapper;
import com.mishuto.todo.common.KeyString;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.Table;
import com.mishuto.todo.common.Time;
import com.mishuto.todo.common.TimerFacade;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.model.task_fields.recurrence.AbstractSegment;
import com.mishuto.todo.model.task_fields.recurrence.BaseMonthYear;
import com.mishuto.todo.model.task_fields.recurrence.Day;
import com.mishuto.todo.model.task_fields.recurrence.Month;
import com.mishuto.todo.model.task_fields.recurrence.SegmentSelector;
import com.mishuto.todo.model.task_fields.recurrence.Week;
import com.mishuto.todo.model.task_fields.recurrence.Year;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.common.ResourceConstants.Recurrence.NUM_WEEKS_ARRAY;
import static com.mishuto.todo.common.TCalendar.getOrdinalOfDayOfWeek;
import static com.mishuto.todo.common.TCalendar.now;
import static com.mishuto.todo.model.task_fields.recurrence.Day.DailyFactor.TENTH_DAY;
import static com.mishuto.todo.view_model.EspressoHelper.checkSelectedSpinner;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.EspressoHelper.onTextInputLayout;
import static com.mishuto.todo.view_model.EspressoHelper.scrollClick;
import static com.mishuto.todo.view_model.EspressoHelper.selectSpinner;
import static com.mishuto.todo.view_model.TestUtils.Automation.goHomeAndBack;
import static com.mishuto.todo.view_model.TestUtils.Card.COMPLETE_BTN;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.List.checkActiveState;
import static com.mishuto.todo.view_model.TestUtils.List.clickTask;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.getLocalized;
import static java.util.Calendar.DATE;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONDAY;
import static java.util.Calendar.SUNDAY;
import static java.util.Calendar.TUESDAY;
import static java.util.Calendar.WEDNESDAY;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Проверка изменений повторяемости задачи
 * Created by Michael Shishkin on 01.05.2017.
 */
public class RecurrenceControllerTest  {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private final static int OK = R.id.recurrence_buttonOk;
    private final static int CANCEL = R.id.recurrence_buttonCancel;

    // введенные в Ежедневный Повтор атрибуты должны сохраниться
    @Test
    public void everyDay() {

        Task task = createTask("everyDay");
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());     // клик по Повтору

        final Day.HourlyFactor SELECTED = Day.HourlyFactor.SIXTH_HOUR;                      // выбор каждого 6-го часа

        clickOn(R.id.recurrence_day_radio_hour);                                            // клик по радиокнопке "каждый час"
        selectSpinner(R.id.recurrence_day_hour_spinner, Day.HourlyFactor.class, SELECTED);  // установка спиннера каждого 6-го часа
        clickOn(OK);
        clickOn(SAVE);

        sleep(200);
        task.reload();
        Day day = task.getRecurrence().getDay();

        assertFalse(day.isDaySelected());                                                   // должен быть выбран часовой спинер
        assertEquals(SELECTED, day.getHourlyFactor());                                      // должен быть выбран 6-й час

        TaskList.remove(task);
    }

    // введенные в Еженедельный Повтор атрибуты должны сохраниться
    @Test
    public void everyWeek() {
        Task task = createTask("everyWeek");
        int todayDay = now().get(Calendar.DAY_OF_WEEK);

        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        sleep(100);
        selectSpinner(R.id.recurrence_page_selector, SegmentSelector.class, SegmentSelector.WEEK);  // установка спинера "Еженедельно"

        if(todayDay != MONDAY )
            if(TCalendar.getOrdinalOfDayOfWeek(MONDAY) == 0)                                        // установка понедельника
                clickOn(R.id.day1);
            else
                clickOn(R.id.day2);

        selectSpinner(R.id.rec_week_spinner, Week.WeeklyFactor.class, Week.WeeklyFactor.SECOND_WEEK);  // установка спинера каждая вторая неделя

        scrollClick(OK);
        clickOn(SAVE);

        sleep(200);
        task.reload();
        Week week = task.getRecurrence().getWeek();

        assertTrue(week.getWeekDays()[getOrdinalOfDayOfWeek(MONDAY)].isChecked());
        assertFalse(week.getWeekDays()[getOrdinalOfDayOfWeek(todayDay!= TUESDAY ? TUESDAY : WEDNESDAY)].isChecked());
        assertEquals(Week.WeeklyFactor.SECOND_WEEK, week.getWeeklyFactor());

        TaskList.remove(task);
    }

    // неделя должна начинаться с ПН или ВС  зависимости от локали.
    // нужно проверять и с русской и с английской локалями!
    @Test
    public void weekMondayCheck() {
        createTask("weekMondayCheck");
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        selectSpinner(R.id.recurrence_page_selector, SegmentSelector.class, SegmentSelector.WEEK);

        onView(allOf(isDisplayed(), withParent(withId(R.id.day1)))).check(matches(withText(
                DateFormatWrapper.getDayOfWeekName(TCalendar.isFirstDaySunday() ? SUNDAY : MONDAY, true).substring(0,2))));

        scrollClick(OK);
        cancelTask();
    }

    // введенные в Ежемесячный Повтор атрибуты должны сохраниться. День месяца
    @Test
    public void everyMonth() {
        Task task = createTask("everyMonth");

        TCalendar time = now();
        time.setTimePart(10, 0);

        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        sleep(100);
        selectSpinner(R.id.recurrence_page_selector, SegmentSelector.class, SegmentSelector.MONTH); // установка спинера "Ежемесячно"

        onTextInputLayout(R.id.rec_mon_monDay).perform(replaceText("10"));                          // каждое 10 число
        clickOn(R.id.recurrence_time_switch);                                                       // установка свитча "точное время"
        onTextInputLayout(R.id.rec_time_text_layout).perform(replaceText(time.getFormattedTime())); // в 10:00 в местном формате
        clickOn(OK);
        clickOn(SAVE);

        sleep(200);
        task.reload();

        Month month = task.getRecurrence().getMonth();
        assertThat(month.getMonthFactor(), is(Month.MonthlyFactor.EVERY_MONTH));
        assertThat(month.getDayOfMonth(), is(10));
        assertThat(month.isTimeSet(), is(true));
        assertThat(month.getTime().getHour(), is(10));

        TaskList.remove(task);
    }

    // введенные в Ежемесячный Повтор атрибуты должны сохраниться. День недели
    @Test
    public void everyMonthDayOfWeek() {
        Task task = createTask("everyMonthDayOfWeek");

        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        sleep(100);
        selectSpinner(R.id.recurrence_page_selector, SegmentSelector.class, SegmentSelector.MONTH); // установка спинера "Ежемесячно"
        clickOn(R.id.rec_month_radio_week);                                                         // радиокнопка - день недели
        selectSpinner(R.id.rec_mon_weekNum_spinner, BaseMonthYear.WeekNumber.class, BaseMonthYear.WeekNumber.SECOND_WEEK);
        selectSpinner(R.id.rec_mon_weekDay_spinner, KeyString.class, BaseMonthYear.getWeekDaysArray()[0]);

        clickOn(OK);
        clickOn(SAVE);

        sleep(200);
        task.reload();

        Month month = task.getRecurrence().getMonth();
        assertThat(month.getMonthFactor(), is(Month.MonthlyFactor.EVERY_MONTH));
        assertThat(month.isDayOfMonthSelected(), is(false));
        assertThat(month.getWeekNum(), is(BaseMonthYear.WeekNumber.SECOND_WEEK));
        assertThat(month.getWeekDay(), is(BaseMonthYear.getWeekDaysArray()[0]));

        sleep(400);
        clickTask(task);
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        checkSelectedSpinner(R.id.rec_mon_weekNum_spinner, NUM_WEEKS_ARRAY[1]);
        checkSelectedSpinner(R.id.rec_mon_weekDay_spinner, BaseMonthYear.getWeekDaysArray()[0].toString());

        clickOn(OK);
        clickOn(SAVE);

        TaskList.remove(task);
    }

    // Должны сохраниться дефолтовые значения, если не было ничего введено в ежемесячный Повтор
    @Test
    public void everyMonthOnlySave()  {
        Task task = createTask("everyMonthOnlySave");

        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        sleep(100);
        selectSpinner(R.id.recurrence_page_selector, SegmentSelector.class, SegmentSelector.MONTH);
        clickOn(OK);
        clickOn(SAVE);

        sleep(200);
        Integer day = task.getRecurrence().getMonth().getDayOfMonth();
        task.reload();
        assertEquals(day, task.getRecurrence().getMonth().getDayOfMonth());

        TaskList.remove(task);
    }

    // введенные в Ежегодный Повтор атрибуты должны сохраниться
    @Test
    public void everyYear()  {
        Task task = createTask("everyYear");

        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        sleep(100);
        selectSpinner(R.id.recurrence_page_selector, SegmentSelector.class, SegmentSelector.YEAR);                                      // установка спинера "Ежегодно"
        selectSpinner(R.id.rec_mon_weekNum_spinner, BaseMonthYear.WeekNumber.class, BaseMonthYear.WeekNumber.LAST_WEEK);// последний
        selectSpinner(R.id.rec_mon_weekDay_spinner, KeyString.class,
                BaseMonthYear.getWeekDaysArray()[TCalendar.isFirstDaySunday() ? 1 : 0]);                                // понедельник
        selectSpinner(R.id.rec_year_month, KeyString.class, Year.getMonthsInYearArray()[11]);                           // декабря
        selectSpinner(R.id.rec_year_foldYear, Year.YearlyFactor.class, Year.YearlyFactor.SECOND_YEAR);                  // каждого второго года
        clickOn(R.id.rec_month_radio_week);                                                                             // радиокнопка - недели
        scrollClick(OK);
        clickOn(SAVE);

        sleep(200);
        task.reload();

        Year year = task.getRecurrence().getYear();

        assertThat(year.getYearFactor(), is(Year.YearlyFactor.SECOND_YEAR));
        assertThat(year.getWeekNum(), is(BaseMonthYear.WeekNumber.LAST_WEEK));
        assertThat(year.getMonth(), is(11));
        assertThat(year.isDayOfMonthSelected(), is(false));

        TaskList.remove(task);
    }

    // Если повтор должен сработать сегодня в определенное время,
    // а пользователь удаляет точное время в повторе - задача должна перейти в активное состояние
    @Test
    public void changeStateWhenChangedRecurrence() throws Exception {
        Task task = createTask("changeStateWhenChangedRecurrence");

        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        Time time = new Time(23, 59);
        clickOn(R.id.recurrence_time_switch);                                                       // установка свитча "точное время"
        onTextInputLayout(R.id.rec_time_text_layout).perform(replaceText(time.toString()));         // в 23:59 в местном формате
        clickOn(OK);
        clickOn(COMPLETE_BTN);                                                              // завершаем

        TCalendar yest = now();
        yest.add(DATE, -1);
        TaskTime taskTime = new TaskTime(yest, true);                                               // подменяем стартовое время на вчерашний день
        TestUtils.Reflection.setField(AbstractSegment.class, task.getRecurrence().getDay(), "mStartTime", taskTime);

        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        clickOn(R.id.recurrence_time_switch);                                                       // установка свитча "время" - в состояние день
        clickOn(OK);
        clickOn(SAVE);                                                                              // возвращаемся в список

        checkActiveState(task);                                                           // проверяем что задача активна

        TaskList.remove(task);
    }

    // установка Повтора должна порождать таймер
    @Test
    public void recurrenceAlarm() throws Exception {
        createTask("recurrenceAlarm");

        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        TCalendar alarm = now();
        alarm.add(DATE, 1);
        alarm.clipTo(MINUTE);
        Time time = new Time(alarm.get(HOUR_OF_DAY), alarm.get(MINUTE));
        clickOn(R.id.recurrence_time_switch);                                                       // установка свитча "точное время"
        onTextInputLayout(R.id.rec_time_text_layout).perform(replaceText(time.toString()));         // устанавливаем завтра и текущее время
        clickOn(OK);
        clickOn(COMPLETE_BTN);                                                             // завершаем

        //noinspection unchecked
        Table<TimerFacade> timerTable =  (Table<TimerFacade>) TestUtils.Reflection.getStaticField(TimerFacade.AlarmReceiver.class, "sTimerTable");
        boolean found = false;
        for(TimerFacade timerFacade : timerTable)
            if (timerFacade.getAlarmTime().equals(alarm)) {                                         // проверяем наличие заданного времени в таблице таймеров
                found = true;
                break;
            }

        assertTrue(found);

        cancelTask();
    }

    //При уходе в/возвращении из бэкграунда, данные должны сохраняться
    @Test
    public void backgroundTest() throws Exception {
        Task t = createTask("backgroundTest");
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        selectSpinner(R.id.recurrence_day_daily_spinner, Day.DailyFactor.class, TENTH_DAY);
        goHomeAndBack();
        checkSelectedSpinner(R.id.recurrence_day_daily_spinner, TENTH_DAY.toString());

        sleep(2000);
        clickOn(OK);
        // cancelTask();
        clickOn(SAVE);
        TaskList.remove(t);
    }

    //при возвращении в/из бэка в неизмененный диалог, снек не отображается
    @Test
    public void backUnchanged() throws Exception {
        Task task = createTask("backUnchanged");
        clickOn(SAVE);

        clickTask(task);
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        goHomeAndBack();
        onSnackBar().check(doesNotExist());
        clickOn(OK);
        clickOn(SAVE);

        TaskList.remove(task);
    }

    //при отмене изменения выбранного сегмента и самого сегмента должны откатываться
    @Test
    public void cancelRecurrence()  {
        Task task = createTask("cancelRecurrence");
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        clickOn(OK);                                                                                // сначала выбираем день и сохраняем

        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        selectSpinner(R.id.recurrence_page_selector, SegmentSelector.class, SegmentSelector.MONTH); // теперь выбираем месяц
        int day = task.getRecurrence().getMonth().getDayOfMonth();                                  // сохраняем предыдущее значение
        onTextInputLayout(R.id.rec_mon_monDay).perform(replaceText("10"));                          // устанавливаем 10-е
        clickOn(CANCEL);

        assertThat(task.getRecurrence().getSegmentSelector(), is(SegmentSelector.DAY));
        assertThat(task.getRecurrence().getMonth().getDayOfMonth(), is(day));
        onView(withId(R.id.taskDetails_textView_recurrence)).check(matches(withText(getLocalized("Ежедневно", "Everyday"))));

        cancelTask();
    }
}