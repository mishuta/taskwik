package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.mishuto.todo.common.ResourcesFacade.getString;
import static com.mishuto.todo.model.task_filters.FilterManager.date;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setTaskDate;
import static com.mishuto.todo.view_model.TestUtils.Card.setTaskTime;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.DATE;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.PROJECT;
import static com.mishuto.todo.view_model.TestUtils.Filters.openFilterDialog;
import static com.mishuto.todo.view_model.TestUtils.Filters.scrollToTab;
import static com.mishuto.todo.view_model.TestUtils.Filters.selectSingleTag;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Тесты диалога DateFilter
 * Created by Michael Shishkin on 14.02.2019.
 */
public class DateFilterFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // при выборе фильтра задача отфильтровывается
    @Test
    public void taskFiltered() {
        TCalendar tCalendar = new TCalendar(2019, 1, 1);            // пробная дата
        String title = date().toString(tCalendar);    // текст даты

        Task t1 = createTask("taskFiltered Filtered");              // создаем задачу с пробной датой
        setTaskDate(tCalendar);
        TestUtils.Card.setTaskTime(23,54);
        clickOn(SAVE);

        Task t2 = createTask("taskFiltered NoFiltered");
        setTaskDate(TCalendar.now());
        clickOn(SAVE);

        selectSingleTag(DATE, title);             // выбираем ее в фильтре
        TestUtils.Filters.checkToolbarTitleView(title);             // фильтр отображается
        TestUtils.List.assertTaskExists(t1.toString());             // задача 1 отображается
        TestUtils.List.assertTaskNotExists(t2.toString());          // задача 2 не отображается
        assertEquals(new TCalendar(2019,1,1,23,54), t1.getTaskTime().getCalendar());

        TaskList.remove(t1);
        TaskList.remove(t2);
    }

    // при создании задачи с выбранным фильтром, задача инициируется датой из фильтра
    @Test
    public void createTaskWithDate() {
        TCalendar tCalendar = new TCalendar(2019, 1, 2, 10, 20);
        Task t1 = createTask("createTaskWithDate A");
        setTaskDate(tCalendar);
        setTaskTime(tCalendar.get(Calendar.HOUR_OF_DAY), tCalendar.get(Calendar.MINUTE));
        clickOn(SAVE);

        selectSingleTag(DATE, date().toString(tCalendar));
        Task t2 = createTask("createTaskWithDate B");
        clickOn(SAVE);

        assertEquals(new TCalendar(2019, 1, 2), t2.getTaskTime().getCalendar());
        assertFalse(t2.getTaskTime().isTimeSet());

        TaskList.remove(t1);
        TaskList.remove(t2);
    }

    //при создании задачи с выбранным фильтром [без даты] задача не инициируется датой
    @Test
    public void createTaskWithNoDate() {
        selectSingleTag(DATE, getString(R.string.no_date));
        Task t1 = createTask("createTaskWithNoDate");
        clickOn(SAVE);

        assertFalse(t1.getTaskTime().isDateSet());

        TaskList.remove(t1);
    }

    //при открытии фильтра с датами FAB скрывается. При переключении на другой фильтр - показывается
    @Test
    public void hideFAB() {
        openFilterDialog(DATE);                                                         // открываем фильтр даты
        onView(withId(R.id.floatingActionButton)).check(matches(not(isDisplayed())));   // FAB нету
        scrollToTab(PROJECT);                                                           // открываем проекты
        onView(withId(R.id.floatingActionButton)).check(matches(isDisplayed()));        // FAB появился
        scrollToTab(DATE);                                                              // обратно в даты
        onView(withId(R.id.floatingActionButton)).check(matches(not(isDisplayed())));   // FAB пропал
    }
}