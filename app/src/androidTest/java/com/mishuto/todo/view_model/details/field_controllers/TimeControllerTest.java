package com.mishuto.todo.view_model.details.field_controllers;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.TestUtils.Card.COMPLETE_BTN;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.junit.Assert.assertEquals;


/**
 * Проверка изменения времени и нового приоритета задачи
 * Created by Michael Shishkin on 30.04.2017.
 */

public class TimeControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private final static int HH = 21;
    private final static int MM = 10;

    // проверка установки времени и приоритета в диалоге
    @Test
    public void changeTime() {
        Task task = createTask("changeTime");
        TestUtils.Card.setTaskTime(HH, MM);
        clickOn(SAVE);
        sleep(200);
        task.reload();

        assertEquals(HH, task.getTaskTime().getHour());
        assertEquals(MM, task.getTaskTime().getMinute());

        TaskList.remove(task);
    }

    //если задача завершена - время сбрасывается
    @Test
    public void resetTimeAfterComplete() {
        createTask("resetTimeAfterComplete");
        TestUtils.Card.setTaskTime(HH, MM);
        clickOn(COMPLETE_BTN);
        onView(withId(R.id.taskDetails_textView_time)).check(matches(withText(R.string.NOT_SET)));
        cancelTask();
    }
}