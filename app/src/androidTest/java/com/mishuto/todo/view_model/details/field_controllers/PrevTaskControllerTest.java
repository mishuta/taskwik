package com.mishuto.todo.view_model.details.field_controllers;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.model.tags_collections.ProjectsSet;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;

import java.util.UUID;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view.Constants.OPAQUE_FULL;
import static com.mishuto.todo.view.Constants.OPAQUE_MID;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.hasAlpha;
import static com.mishuto.todo.view_model.EspressoHelper.hasItemsCount;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerView;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.EspressoHelper.scrollClick;
import static com.mishuto.todo.view_model.EspressoHelper.withRecyclerPositionItemView;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.Card.setPrevTask;
import static com.mishuto.todo.view_model.TestUtils.List.clickTask;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.hamcrest.CoreMatchers.not;

/**
 * Тест диалога поиска предыдущей задачи
 * Created by Michael Shishkin on 30.10.2017.
 */
public class PrevTaskControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // проверка, что данный текст фильтруется в списке в соответствии с матчером
    private void checkPrevTask(String text, Matcher<? super View> viewMatcher) {
        createTask("checkPrevTask-" + text);
        scrollClick(R.id.taskDetails_label_prevTask);
        sleep(100);
        onView(withId(R.id.prev_searchView)).perform(typeText(text));
        onView(withId(R.id.tasks_recyclerView)).check(matches(viewMatcher));
        Espresso.pressBack();   // убираем клавиатуру
        sleep(100);
        Espresso.pressBack();   // убираем диалог
        sleep(100);
        cancelTask();
    }


    // Предыдущая задача отображается
    @Test
    public void checkPrev() {
        Task prevTask = createTask("checkPrev");
        clickOn(SAVE);

        Task task = createTask("PrevTaskControllerTest6");
        setPrevTask(prevTask);
        clickOn(SAVE);

        onRecyclerView(R.id.recycler_view, withText(task.toString())).perform(click());

        onView(withId(R.id.taskDetails_textView_prevTask)).perform(scrollTo())
                .check(matches(withText(prevTask.toString()))).check(matches(hasAlpha(OPAQUE_FULL)));   // предыдущая задача есть
        onView(withId(R.id.taskDetails_ico_prevComp)).check(matches(not(isDisplayed())));               // иконки завершено - нет

        clickOn(SAVE);

        TaskList.remove(prevTask);
        TaskList.remove(task);
    }

    //Предыдущая завершенная задача - отображается с иконкой
    @Test
    public void checkPrevCompleted() {
        Task prevTask = createTask("checkPrevCompleted");
        clickOn(SAVE);
        prevTask.setPerformed(true);

        Task task = createTask("PrevTaskControllerTest7");
        setPrevTask(prevTask);
        clickOn(SAVE);

        onRecyclerView(R.id.recycler_view, withText(task.toString())).perform(click());

        onView(withId(R.id.taskDetails_textView_prevTask)).perform(scrollTo())
                .check(matches(withText(prevTask.toString()))).check(matches(hasAlpha(OPAQUE_MID)));   // предыдущая задача есть
        onView(withId(R.id.taskDetails_ico_prevComp)).check(matches(isDisplayed()));                   // иконка завершено - есть

        clickOn(SAVE);

        TaskList.remove(prevTask);
        TaskList.remove(task);
    }

    // Поиск в имени
    @Test
    public void searchByName() {
        Task searched = TaskList.addNew();
        searched.getTitle().setTaskName(TextUtils.uniqueStr(TaskList.getTasks(), "searchByName"));

        checkPrevTask(searched.toString(), hasItemsCount(1));

        TaskList.remove(searched);
    }

    // Поиск в комментарии
    @Test
    public void searchByComment() {
        Task searched = TaskList.addNew();
        searched.getTitle().setTaskName(TextUtils.uniqueStr(TaskList.getTasks(), "searchByComment"));
        searched.getComments().add("a neverending dream is a dream of you");    // будем искать подстроку во втором комментарии
        searched.getComments().add("actual comment");

        checkPrevTask("neverending", not(hasItemsCount(0)));

        TaskList.remove(searched);
    }

    // Поиск по исполнителю
    @Test
    public void searchByExecutor() {
        Task searched = TaskList.addNew();
        StringContainer executor =  TestUtils.createTag(Executors.getInstance(), "SBE-executor");
        searched.getTitle().setTaskName(TextUtils.uniqueStr(TaskList.getTasks(), "searchByExecutor"));
        searched.setExecutor(executor);

        checkPrevTask(executor.toString(), hasItemsCount(1));

        TaskList.remove(searched);
        Executors.getInstance().remove(executor);
    }

    // Поиск по проекту
    @Test
    public void searchByProject() {
        Task searched = TaskList.addNew();
        StringContainer project =  TestUtils.createTag(ProjectsSet.getInstance(), "SBP-project");
        searched.getTitle().setTaskName(TextUtils.uniqueStr(TaskList.getTasks(), "searchByExecutor"));
        searched.setProject(project);

        checkPrevTask(project.toString(), hasItemsCount(1));

        TaskList.remove(searched);
        ProjectsSet.getInstance().remove(project);
    }

    // Поиск несуществующей строки
    @Test
    public void notFound() {
        Task searched = TaskList.addNew();
        searched.getTitle().setTaskName(TextUtils.uniqueStr(TaskList.getTasks(), "notFound"));

        checkPrevTask(UUID.randomUUID().toString(), hasItemsCount(0));

        TaskList.remove(searched);
    }

    // Проверка невозможности выбрать задачу - последователь текущей
    @Test
    public void succeeded() {
        Task task = createTask("succeeded");
        clickOn(SAVE);

        Task t1 = createTask("succeeded A");    // задача А - последователь текущей
        TestUtils.Card.setPrevTask(task);
        clickOn(SAVE);

        Task t2 = createTask("succeeded B");    // задача В - последователь А
        TestUtils.Card.setPrevTask(t1);
        clickOn(SAVE);

        clickTask(task);
        scrollClick(R.id.taskDetails_label_prevTask);
        onView(withId(R.id.prev_searchView)).perform(typeText(t2.toString()));
        onView(withRecyclerPositionItemView(R.id.tasks_recyclerView, R.id.search_taskName, 0)).perform(ViewActions.click()); // Кликаем по задаче В
        onSnackBar().check(matches(isDisplayed())); // нарываемся  на снек
        Espresso.pressBack();                       // убираем снек
        Espresso.pressBack();                       // убираем диалог
        clickOn(SAVE);

        TaskList.remove(t1);
        TaskList.remove(t2);
        TaskList.remove(task);
    }
}