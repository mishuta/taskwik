package com.mishuto.todo.view_model.list;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.ContextsSet;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.model.tags_collections.ProjectsSet;
import com.mishuto.todo.model.tags_collections.Tags;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.withImage;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setTaskDate;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.DATE;
import static com.mishuto.todo.view_model.TestUtils.Filters.clearFilters;
import static com.mishuto.todo.view_model.TestUtils.Filters.doSearch;
import static com.mishuto.todo.view_model.TestUtils.Filters.selectSingleTag;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.createTag;
import static com.mishuto.todo.view_model.list.HistoryFilterMenu.ICONS;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Тест истории фильтров
 * Created by Michael Shishkin on 01.06.2018.
 */
public class HistoryFilterMenuTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    //тегфильтр после сохранения в HistoryFilter должен появиться в меню
    @Test
    public void pushTagFilter() {
        pushTagFilter(ProjectsSet.getInstance(), "pushProject", FilterManager.PROJECT, TestUtils.Filters.FilterTab.PROJECT);
        pushTagFilter(Executors.getInstance(), "pushExecutor", FilterManager.EXECUTOR, TestUtils.Filters.FilterTab.EXECUTOR);
        pushTagFilter(ContextsSet.getInstance(), "pushContext", FilterManager.CONTEXT, TestUtils.Filters.FilterTab.CONTEXT);
    }

    //search после сохранения в HistoryFilter должен появиться в меню
    @Test
    public void pushSearch() {
        doSearch("pushSearch");
        checkMenuItem(FilterManager.SEARCH, "pushSearch");
        pressBack();
        clearFilters();
    }

    // дата после сохранения в HistoryFilter появляется в меню
    @Test
    public void pushDate() {
        TCalendar tCalendar = new TCalendar(2019, 2, 2);            // пробная дата
        String title = FilterManager.date().toString(tCalendar);    // текст даты

        Task task = createTask("pushDate");
        setTaskDate(tCalendar);
        clickOn(SAVE);

        TestUtils.Filters.selectSingleTag(DATE, title);             // выбираем дату в фильтре

        checkMenuItem(FilterManager.DATE, title);                   // проверяем наличие даты
        pressBack();
        clearFilters();
        TaskList.remove(task);
    }

    // выбранный из истории проект - фильтрует задачи
    @Test
    public void filterProject() {
        StringContainer project = new StringContainer(TextUtils.uniqueStr(ProjectsSet.getInstance().getTags(), "filterProject"));
        Task t1 = createTask("filterProject A");        // задача, которая отфильтруется
        TestUtils.Card.setProject(project.toString());  // имеет проект filterProject
        clickOn(SAVE);

        Task t2 = createTask("filterProject B");        // задача, которая не попадет в фильтр
        clickOn(SAVE);

        selectSingleTag(TestUtils.Filters.FilterTab.PROJECT, project.toString());           // устанавливаем фмльтр, он сохраняется в истории
        clearFilters();

        clickOn(R.id.history_button);
        onView(allOf(withId(R.id.title), withText(project.toString()))).perform(click());   // выбираем фильтр из истории

        sleep(200);
        TestUtils.List.assertTaskExists(t1.toString());     // проверяем что задача 1 отфильтровалась
        TestUtils.List.assertTaskNotExists(t2.toString());  // задача два - нет

        TaskList.remove(t1);
        TaskList.remove(t2);
        ProjectsSet.getInstance().remove(project);
        clearFilters();
    }

    // выбранный из истории серч - фильтрует задачи
    @Test
    public void filterQuery() {
        final String query = "filterQuery";
        Task t1 = createTask(query);            // задача, которая отфильтруется
        clickOn(SAVE);

        Task t2 = createTask("filter_A_Query"); // задача, которая не попадет в фильтр
        clickOn(SAVE);

        doSearch(query);                        // запрос, который попадет в историю
        clearFilters();

        clickOn(R.id.history_button);           // выбираем фильтр из истории
        onView(allOf(withId(R.id.title), withText(query))).perform(click());

        sleep(200);
        TestUtils.List.assertTaskExists(t1.toString());     // проверяем что задача 1 отфильтровалась
        TestUtils.List.assertTaskNotExists(t2.toString());  // задача два - нет

        TaskList.remove(t1);
        TaskList.remove(t2);
        clearFilters();
    }


    private void pushTagFilter(Tags tags, String title, FilterManager label, TestUtils.Filters.FilterTab tab) {
        StringContainer tag = createTag(tags, title);
        tags.add(tag);

        selectSingleTag(tab, tag.toString());

        checkMenuItem(label, tag.toString());
        pressBack();
        tags.remove(tag);
    }

    private void checkMenuItem(FilterManager label, String title) {
        clickOn(R.id.history_button);
        onView(allOf(withId(R.id.title), withText(title))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.icon), hasSibling(withChild(withText(title))))).check(matches(withImage(ICONS.get(label))));
    }
}