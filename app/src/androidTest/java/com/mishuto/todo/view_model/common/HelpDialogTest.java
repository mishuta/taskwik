package com.mishuto.todo.view_model.common;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.TestUtils.getLocalized;

/**
 * Тест диалога Справки
 * Created by Michael Shishkin on 03.07.2018.
 */
public class HelpDialogTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private final static String ARTICLE = getLocalized("Советы по планированию", "Planning tips");
    private final static String CATEGORY = getLocalized("Задачи", "Tasks");

    // при выборе меню справки должеен открыться список статей,
    // при клике на статью открывается статья
    @Test
    public void checkArticle() {
        TestUtils.List.clickListOptionsMenu(TestUtils.List.HELP);   // клик по меню Справка в списке задач
        onView(withText(CATEGORY)).check(matches(isDisplayed()));   // есть категория
        onView(withText(ARTICLE)).perform(click());                 // есть статья. Кликаем

        onView(withText(CATEGORY)).check(doesNotExist());           // проверяем что открылось новое окно (нет категорий)
        onView(withText(ARTICLE)).check(matches(isDisplayed()));    // а заголовок есть
    }
}