package com.mishuto.todo.view_model.common;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import com.mishuto.todo.common.DateFormatWrapper;
import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.clickOnSnackButton;
import static com.mishuto.todo.view_model.EspressoHelper.hasItemsCount;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Filters.clearFilters;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.setTaskTimerAndSleep;
import static junit.framework.Assert.assertFalse;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertNotNull;

/**
 * Тестирование уведомлений задачи
 * Created by Michael Shishkin on 10.01.2018.
 */
public class NotificationControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private UiDevice mDevice;

    @Before
    public void setUp() {
        Settings.getNotificationSettings().isOn.set(true);
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    @After
    public void tearDown() {
        mDevice.pressBack();                                             // закрыть уведомления (если тест провалился)
    }

    // если активность в бэкраунде, должно появится уведомление, содержащее заголовок и текст задачи
    @Test
    public void showNotify() throws Exception {

        Task task = createTask("showNotify");
        clickOn(SAVE);
        mDevice.pressHome();                                            // выводим активность в бэкграунд
        setTaskTimerAndSleep(task);                           // устанавливаем время задачи + 5 сек, ждем

        // вычисляем текст нотификации
        String notificationTitle = getNotificationTitle(1);
        String notificationText = DateFormatWrapper.getTimeString(task.getTaskTime().getCalendar()) + ". " + task.toString();

        mDevice.openNotification();                                         // открываем уведомления
        mDevice.wait(Until.hasObject(By.text(notificationTitle)), 1000);    // ищем заголовок уведомления
        UiObject2 title = mDevice.findObject(By.text(notificationTitle));   // объект, содержащий заголовок
        UiObject2 text = mDevice.findObject(By.text(notificationText));     // объект, содержащий текст
        assertNotNull(title);                                               // заголовок найден
        assertNotNull(text);                                                // текст найден

        title.click();
        sleep(200);
        clickOn(SAVE);

        TaskList.remove(task);
    }

    // если наступил срок по двум задачам, то заголовок уведомления должен содержать множественное число задач
    @Test
    public void multiTaskNotify() throws Exception {
        Task task1 = createTask("multiTaskNotify A");
        clickOn(SAVE);
        Task task2 = createTask("multiTaskNotify B");
        clickOn(SAVE);
        mDevice.pressHome();

        setTaskTimerAndSleep(task1, task2);

        clickOnNotification(2);

        TaskList.remove(task1);
        TaskList.remove(task2);
    }

    // если приложение активно - уведомления не появляются
    @Test
    public void startedActivityDoesNotShowNotify() throws Exception {
        Task task = createTask("startedActivityDoesNotShowNotify");
        clickOn(SAVE);
        setTaskTimerAndSleep(task);

        checkNotificationNotExists();

        TaskList.remove(task);
    }

    //возвращает строку - заголовок уведомления
    private static String getNotificationTitle(int n) {
        Context context = InstrumentationRegistry.getTargetContext();
        return context.getResources().getQuantityString(R.plurals.task_activated_plural, n, n);
    }

    public static void clickOnNotification(int n) {
        String notificationTitle = getNotificationTitle(n);
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        device.openNotification();                                         // открываем уведомления
        device.wait(Until.hasObject(By.text(notificationTitle)), 2000);    // ищем заголовок уведомления
        UiObject2 title = device.findObject(By.text(notificationTitle));   // объект, содержащий заголовок
        assertNotNull(title);                                              // заголовок найден

        title.click();                                                      // клик по уведомлению
    }

    // проверяем что нет уведомлений для одной или двух задач
    private static void checkNotificationNotExists() {
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        device.openNotification();                                          // открываем уведомления
        String notificationTitle1 = getNotificationTitle(1);
        assertFalse(device.wait(Until.hasObject(By.text(notificationTitle1)), 1000));    // ищем заголовок уведомления по одной задаче

        String notificationTitle2 = getNotificationTitle(2);
        assertFalse(device.wait(Until.hasObject(By.text(notificationTitle2)), 1000));    // ищем заголовок уведомления по двум задачам
    }

    // открытие карточки задачи по клику на уведомление с одной задачей
    @Test
    public void openCard() throws Exception {
        clearFilters();

        Task task = createTask("openCard");
        clickOn(SAVE);
        mDevice.pressHome();
        setTaskTimerAndSleep(task);
        clickOnNotification(1);                         // клик по нотификации

        TestUtils.Card.checkTitle(task.toString());     // убеждаемся что открыта карточка
        clickOn(SAVE);

        onView(allOf(isDescendantOfA(withId(R.id.toolbar_filter_value)), isDisplayed()))    // проверяем что фильтр не установлен
                .check(matches(withText(ResourcesFacade.getString(R.string.app_name))));

        TaskList.remove(task);
    }

    // открытие карточки задачи по клику на уведомление с одной задачей если основная активность была закрыта,
    // приводит к открытию карточки, а фильтр activated() не установлен
    @Test
    public void openCardAfterFinish() throws Exception {
        clearFilters();

        Task task = createTask("openCardAfterFinish");
        clickOn(SAVE);
        mDevice.pressHome();
        setTaskTimerAndSleep(task);
        mActivityRule.getActivity().finish();           // убиваем активность
        clickOnNotification(1);                         // клик по нотификации

        TestUtils.Card.checkTitle(task.toString());     // убеждаемся что открыта карточка
        clickOn(SAVE);

        onView(allOf(isDescendantOfA(withId(R.id.toolbar_filter_value)), isDisplayed()))    // проверяем что фильтр не установлен
                .check(matches(withText(ResourcesFacade.getString(R.string.app_name))));

        TaskList.remove(task);
    }

    // при срабатывании уведомления по двум задачам, по клику на уведомление должен открыться отфильтрованный список из этих задач
    @Test
    public void openFiltered() throws Exception {
        clearFilters();

        Task t1 = createTask("openFiltered A");
        clickOn(SAVE);

        Task t2 = createTask("openFiltered B");
        clickOn(SAVE);

        mDevice.pressHome();
        setTaskTimerAndSleep(t1, t2);
        clickOnNotification(2);                                                             // клик по нотификации

        onView(allOf(isDescendantOfA(withId(R.id.toolbar_filter_value)), isDisplayed()))    // проверяем что установлен фильтр активированных задач
                .check(matches(withText(ResourcesFacade.getString(R.string.activated_task_filter))));

        onView(withId(R.id.recycler_view)).check(matches(hasItemsCount(2)));
        sleep(400);
        TestUtils.List.assertTaskExists(t1.toString());
        TestUtils.List.assertTaskExists(t2.toString());

        TaskList.remove(t1);
        TaskList.remove(t2);
    }

    // если приложение открывается не из уведомлений, уведомления очищаются, появояется снек с кнопкой открытия задачи из уведомления
    @Test
    public void openAppWhileNotifySingleTask() throws Exception {
        Task t = createTask("openAppWhileNotifySingleTask");
        clickOn(SAVE);

        // уходим в бэк и ждем уведомления
        mDevice.pressHome();
        setTaskTimerAndSleep(t);

        // возвращаемся в приложение, не открывая уведомление
        mDevice.pressRecentApps();
        UiObject taskwik = mDevice.findObject(new UiSelector().text("Taskwik"));
        taskwik.click();

        onSnackBar().check(matches(withText(getNotificationTitle(1)))); // проверяем снек
        clickOnSnackButton();                                           // кликаем на кнопку снека

        TestUtils.Card.checkTitle(t.toString());                        // убеждаемся что открыта карточка задачи
        clickOn(SAVE);

        checkNotificationNotExists();                                   // убеждаемся что уведомления сброшены
        mDevice.pressBack();

        TaskList.remove(t);
    }

    // если приложение открывается не из уведомлений, уведомления очищаются, появояется снек с кнопкой открытия задачи из уведомления
    @Test
    public void openAppWhileNotifyDoubleTask() throws Exception {
        Task t1 = createTask("openAppWhileNotifyTask A");
        clickOn(SAVE);

        Task t2 = createTask("openAppWhileNotifyTask B");
        clickOn(SAVE);

        // уходим в бэк и ждем уведомления
        mDevice.pressHome();
        setTaskTimerAndSleep(t1, t2);

        // возвращаемся в приложение, не открывая уведомление
        mDevice.pressRecentApps();
        UiObject taskwik = mDevice.findObject(new UiSelector().text("Taskwik"));
        taskwik.click();

        onSnackBar().check(matches(withText(getNotificationTitle(2)))); // проверяем снек
        clickOnSnackButton();                                           // кликаем на кнопку снека

        onView(allOf(isDescendantOfA(withId(R.id.toolbar_filter_value)), isDisplayed()))    // проверяем что установлен фильтр активированных задач
                .check(matches(withText(ResourcesFacade.getString(R.string.activated_task_filter))));

        onView(withId(R.id.recycler_view)).check(matches(hasItemsCount(2)));
        TestUtils.List.assertTaskExists(t1.toString());
        TestUtils.List.assertTaskExists(t2.toString());

        checkNotificationNotExists();                                   // убеждаемся что уведомления сброшены
        mDevice.pressBack();

        TaskList.remove(t1);
        TaskList.remove(t2);
    }
}