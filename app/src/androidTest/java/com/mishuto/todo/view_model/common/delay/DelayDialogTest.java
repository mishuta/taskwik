package com.mishuto.todo.view_model.common.delay;

import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Delayer;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view_model.common.AbstractActivity;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Тестирование корректности вычисления названий времени суток и дат на кнопках диалога Отложить
 * Created by Michael Shishkin on 19.12.2017.
 */
public class DelayDialogTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private class CheckItem {
        private int intervalMinutes;
        private TaskTime mTaskTime;
        private String speakingInterval;
        private String formattedTime;

        private CheckItem(TCalendar start, int interval, boolean isTime, String speakingInterval, String formattedTime) {
            intervalMinutes = interval;
            this.speakingInterval = speakingInterval;
            this.formattedTime = formattedTime;

            TCalendar taskTime = start.clone();
            taskTime.add(Calendar.MINUTE, interval);
            mTaskTime = new TaskTime(taskTime, isTime);
        }
    }

    @Test
    public void checkTimeStrings() throws Exception {
        DelayDialog delayDialog = new DelayDialog();

        // корректно создаем объект диалога, привязывая его к активности
        DialogHelper.createDialog(delayDialog, AbstractActivity.getCurrentActivity().getMainFragment(), null, toBundle(new Delayer()));
        SystemClock.sleep(200);

        Settings.getWorkTime().isOn.set(false);
        Settings.getWorkTime().onlyWorkDays.set(false);

        TCalendar startDate = TCalendar.now();
        startDate.setTimePart(0, 0);                // стартовое время: сегодня 00:00

        if(!AbstractActivity.getContext().getResources().getConfiguration().locale.getCountry().equals("RU"))   // тест выполняется только на русской локали. Для английской надо писать отдельный тест
          return;

        CheckItem[] testData = {                                                                                // тестовые данные
                new CheckItem(startDate, 25, true, "25 минут", "0:25"),
                new CheckItem(startDate, 65, true, "1 час", "1:05"),
                new CheckItem(startDate, 165, true, "3 часа", "2:45"),
                new CheckItem(startDate, 245, true, "Ночь", "4:05"),
                new CheckItem(startDate, 365, true, "Утро", "6:05"),
                new CheckItem(startDate, 725, true, "День", "12:05"),
                new CheckItem(startDate, 18 * 60 + 30, true, "Вечер", "18:30"),
                new CheckItem(startDate, (int)(6.5 * 24 * 60 + 60), false, "Неделю", null),   //  дата не вычисляется из-за сложности
                new CheckItem(startDate, (24 + 9) * 60, false, "Завтра", null)
        };

        for (CheckItem item : testData) {                                                                       // валидация
            assertThat(delayDialog.speakingInterval(item.intervalMinutes, item.mTaskTime.getCalendar()), equalTo(item.speakingInterval));
            if(item.formattedTime != null)
                assertThat(delayDialog.formattedTime(item.mTaskTime), equalTo(item.formattedTime));
        }
    }
}