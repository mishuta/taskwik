package com.mishuto.todo.view_model.details;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.TestUtils.Automation.rotateBack;
import static com.mishuto.todo.view_model.TestUtils.Automation.rotateLeft;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;

/**
 * Тестирование активити TaskDetails
 * Created by Michael Shishkin on 16.10.2017.
 */
public class DetailsActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // Поворот устройства не должен приводить к падению
    @Test
    public void rotate() throws Exception {
        final String str = "rotate";

        createTask(str);
        clickOn(R.id.taskDetails_comment);
        onView(withId(R.id.dlg_description_newItem)).perform(typeText(str));            // вводим текст в комментарий
        pressBack();    // убрать клавиатуру
        rotateLeft();                                                                   // поворачиваем влево-вправо
        rotateBack();
        sleep(5000);
        onView(withId(R.id.dlg_description_newItem)).check(matches(withText(str)));     // проверяем что введенный текст на месте
        pressBack();    // вернуться в карточку
        sleep(100);
        cancelTask();
    }
}