package com.mishuto.todo.view_model.list;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.details.DetailsFragmentTest;

import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.common.TextUtils.uniqueStr;
import static com.mishuto.todo.view.Constants.OPAQUE_FULL;
import static com.mishuto.todo.view.Constants.OPAQUE_MID;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.clickOnSnackButton;
import static com.mishuto.todo.view_model.EspressoHelper.hasAlpha;
import static com.mishuto.todo.view_model.EspressoHelper.hasItemsCount;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerView;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.TestUtils.Card.COMPLETE_BTN;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setExecutor;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.EXECUTOR;
import static com.mishuto.todo.view_model.TestUtils.Filters.clearFilters;
import static com.mishuto.todo.view_model.TestUtils.Filters.selectSingleTag;
import static com.mishuto.todo.view_model.TestUtils.List.ACTIVATE;
import static com.mishuto.todo.view_model.TestUtils.List.COMPLETE;
import static com.mishuto.todo.view_model.TestUtils.List.REMOVE;
import static com.mishuto.todo.view_model.TestUtils.List.SHOW_COMPLETED;
import static com.mishuto.todo.view_model.TestUtils.List.assertTaskExists;
import static com.mishuto.todo.view_model.TestUtils.List.assertTaskNotExists;
import static com.mishuto.todo.view_model.TestUtils.List.clickListOptionsMenu;
import static com.mishuto.todo.view_model.TestUtils.List.clickTask;
import static com.mishuto.todo.view_model.TestUtils.List.clickTaskListMenu;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.getObjectsCount;
import static com.mishuto.todo.view_model.list.ListController.DELETE_SNACK_DURATION;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Тесты активити - фильтрация списка задач.
 * Тесты с тагфильтрами и поиском - см в ToolbarFiltersControlTest
 * Created by Michael Shishkin on 20.11.2017.
 */
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // проверка что при установленном в фильтре исполнителе, нужная задача отфильтровывается, а ненужная - нет
    @Test
    public void taskListFilters() {
        StringContainer executor = new StringContainer(uniqueStr(Executors.getInstance().getTags(), "taskListFilters"));
        Executors.getInstance().add(executor);

        String title = uniqueStr(TaskList.getTasks(), "taskListFilters");
        Task task1 = TaskList.addNew();
        task1.getTitle().setTaskName(title);
        task1.setExecutor(executor);

        Task task2 = TaskList.addNew();

        selectSingleTag(EXECUTOR, executor.toString());
        onView(withId(R.id.recycler_view)).check(matches(hasItemsCount(1)));        // отфильтровался только 1 элемент
        onView(withId(R.id.taskListItem_title)).check(matches(withText(title)));    // и это Тот элемент

        clearFilters();
        TaskList.remove(task1);
        TaskList.remove(task2);
        Executors.getInstance().remove(executor);
    }

    // IncompleteFilter должен фильтровать завершенные задачи
    @Test
    public void testCompletedFilter() {
        if(FilterManager.incomplete().isFilterSet())
            clickListOptionsMenu(SHOW_COMPLETED); // сбрасываем фильтр, если был установлен

        // создаем новую завершенную задачу
        Task task = createTask("testCompletedFilter");
        clickOn(COMPLETE_BTN);
        clickOn(SAVE);

        // проверяем что задача есть
        assertTaskExists(task.toString());

        //устанавливаем фильтр
        clickListOptionsMenu(SHOW_COMPLETED);
        sleep(100);
        assertTaskNotExists(task.toString());
        clickListOptionsMenu(SHOW_COMPLETED);

        TaskList.remove(task);
    }

    // проверка что задача пропадает из списка, если была изменена в карточке так, чтобы не попасть в фильтр
    @Test
    public void disappearIfFiltered() {
        StringContainer executor = TestUtils.createTag(Executors.getInstance(), "disappearIfFiltered");

        Task task = createTask("disappearIfFiltered");
        setExecutor(executor.toString());
        clickOn(SAVE);

        selectSingleTag(EXECUTOR, executor.toString());
        onView(withId(R.id.recycler_view)).check(matches(hasItemsCount(1)));

        onView(allOf(withId(R.id.taskListItem_title), withText(task.toString()))).perform(click());
        setExecutor("");
        clickOn(SAVE);

        onView(withId(R.id.recycler_view)).check(matches(hasItemsCount(0)));

        clickOn(R.id.toolbar_filter_btn);  // сбрасываем установленный фильтр
        TaskList.remove(task);
        Executors.getInstance().remove(executor);
    }

    // завершенная задача в списке красится в серый и имеет статус завершенной
    @Test
    public void completeTask() {
        // сбрасываем фильтр
        boolean filterState = FilterManager.incomplete().isFilterSet();
        FilterManager.incomplete().setFilter(false);

        //создаем задачу
        Task task = createTask("completeTask");

        clickOn(SAVE);
        sleep(1600);

        clickTaskListMenu(task, COMPLETE);  // выбираем меню завершить

        //проверяем что задача покрасилась в серый и выполнена
        sleep(200);
        onRecyclerView(R.id.recycler_view,withText(task.toString())).check(matches(hasAlpha(OPAQUE_MID)));
        assertTrue(task.isStateInPerformed());

        TaskList.remove(task);
        FilterManager.incomplete().setFilter(filterState);
    }

    // активированная задач меняет статус и красится черным в списке
    @Test
    public void activateTask() {
        // сбрасываем фильтр
        boolean filterState = FilterManager.incomplete().isFilterSet();
        FilterManager.incomplete().setFilter(false);

        // создаем завершенную задачу (ошибка проявлялась при наличии завершенной задачи)
        Task t1 = TaskList.addNew();
        t1.setPerformed(true);
        //создаем тестовую задачу
        Task task = createTask("activateTask");
        clickOn(COMPLETE_BTN);

        clickOn(SAVE);
        sleep(1600);

        clickTaskListMenu(task, ACTIVATE);  // выбираем меню активировать

        //проверяем что задача покрасилась в черный и незавершена
        sleep(300);
        onRecyclerView(R.id.recycler_view,withText(task.toString())).check(matches(hasAlpha(OPAQUE_FULL)));
        assertFalse(task.isStateInPerformed());
        TaskList.remove(task);
        TaskList.remove(t1);
        FilterManager.incomplete().setFilter(filterState);
    }

    // удаление задачи
    @Test
    public void deleteTask() {
        Task task = createTask("deleteTask");
        clickOn(SAVE);
        sleep(1600);
        clickTaskListMenu(task, REMOVE);  // выбираем меню удалить
        DetailsFragmentTest.checkRemoved(task);
    }

    // удаление последовательно двух задач из списка
    @Test
    public void deleteDoubleTasks()  {
        Task t1 = createTask("deleteDoubleTasksA");
        clickOn(SAVE);
        Task t2 = createTask("deleteDoubleTasksB");
        clickOn(SAVE);
        sleep(1600);

        int tasks = getObjectsCount(Task.class);

        clickTaskListMenu(t1, REMOVE);
        sleep(DELETE_SNACK_DURATION + 500);

        clickTaskListMenu(t2, REMOVE);

        sleep(DELETE_SNACK_DURATION + 500);
        assertFalse(TaskList.getTasks().contains(t1));
        assertFalse(TaskList.getTasks().contains(t2));
        assertThat(getObjectsCount(Task.class), is(tasks-2));   // проверякм что обе задачи удалены
    }

    // отменить удаление
    @Test
    public void undo() {
        Task task = createTask("undo");
        clickOn(SAVE);
        clickTask(task);

        clickOn(R.id.toolbar_remove);
        clickOnSnackButton();               // жмем undo
        onSnackBar().check(doesNotExist());
        onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_title), withText(task.toString()))).check(matches(isDisplayed()));
        assertTrue(TaskList.getTasks().contains(task));

        TaskList.remove(task);
    }

}