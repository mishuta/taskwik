package com.mishuto.todo.view_model.details.field_controllers;

import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.task_fields.Order;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.junit.Assert.assertEquals;

/**
 * Проверка изменения очередности в карточке
 * Created by Michael Shishkin on 24.04.2017.
 */
public class OrderControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void changeOrder() {
        Task task = createTask("changeOrder");

        TestUtils.Card.setOrder(Order.LOW);
        assertEquals(Order.LOW, task.getOrder());

        clickOn(SAVE);

        SystemClock.sleep(200);
        task.reload();
        assertEquals(Order.LOW, task.getOrder());

        TaskList.remove(task);
    }
}