package com.mishuto.todo.view_model.details.field_controllers;

import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.database.SQLHelper;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.ContextsSet;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.hasItemsCount;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setContext;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static junit.framework.Assert.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;

/**
 * Тесты изменения контекста в карточке задачи
 * Created by Michael Shishkin on 23.05.2017.
 */
public class ContextControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private final static StringContainer CONTEXT1 = new StringContainer("ContextControllerTest A");
    private final static StringContainer CONTEXT2 = new StringContainer("ContextControllerTest B");
    private final static StringContainer CONTEXT3 = new StringContainer("ContextControllerTest C");

    private ContextsSet mContextsSet;

    @Before
    public void setUp() {
        mContextsSet = ContextsSet.getInstance();
    }

    // добавление двух контекстов в задачу
    @Test
    public void add2Contexts() {
        mContextsSet.add(CONTEXT1.toString()); // создаем один невыбираемый контекст. Типа уже был.

        Task task = createTask("add2Contexts");
        setContext(CONTEXT2.toString(), CONTEXT3.toString());

        onView(withId(R.id.taskDetails_context_expand)).check(matches(isDisplayed()));                                      // кнопка раскрыть список отображается
        onView(withId(R.id.taskDetails_context_collapse)).check(matches(not(isDisplayed())));                               // кнопка закрыть список отображается
        onView(withId(R.id.taskDetails_context_desc)).check(matches(withText(startsWith("2 "))));                           // в описании: 2 контекста

        clickOn(R.id.taskDetails_context_expand);                                                                           // раскрываем список
        onView(withId(R.id.taskDetails_context_expand)).check(matches(not(isDisplayed())));                                 // кнопка раскрыть не отображается
        onView(withId(R.id.taskDetails_context_collapse)).check(matches(isDisplayed()));                                    // кнопка закрыть отображается
        onView(withId(R.id.taskDetails_context_recycler)).check(matches(hasItemsCount(2)));                                 // в recyclerView 2 элемента

        clickOn(SAVE);

        SystemClock.sleep(200);
        task.reload();

        assertThat(task.getContext(), contains(CONTEXT2, CONTEXT3));                                                       // проверяем что в mTask сохранилось 2 контекста
        assertThat(CONTEXT1, isIn(mContextsSet.getTags()));                                                                // а ContextSet содержит все 3
        assertThat(CONTEXT2, isIn(mContextsSet.getTags()));
        assertThat(CONTEXT3, isIn(mContextsSet.getTags()));

        TaskList.remove(task);
    }

    @After
    public void tearDown() {
        mContextsSet.remove(CONTEXT1);
        mContextsSet.remove(CONTEXT2);
        mContextsSet.remove(CONTEXT3);

        assertEquals(0, CONTEXT1.getSQLTable().query(SQLHelper.whereString("STRING=?", CONTEXT1.toString())).countNClose());
        assertEquals(0, CONTEXT1.getSQLTable().query(SQLHelper.whereString("STRING=?", CONTEXT2.toString())).countNClose());
        assertEquals(0, CONTEXT1.getSQLTable().query(SQLHelper.whereString("STRING=?", CONTEXT3.toString())).countNClose());
    }
}