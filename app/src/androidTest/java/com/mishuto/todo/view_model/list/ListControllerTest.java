package com.mishuto.todo.view_model.list;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.model.task_filters.FilterManager.incomplete;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.EspressoHelper.withPositionInGrid;
import static com.mishuto.todo.view_model.TestUtils.Card.COMPLETE_BTN;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setPrevTask;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.DATE;
import static com.mishuto.todo.view_model.TestUtils.List.COMPLETE;
import static com.mishuto.todo.view_model.TestUtils.List.COMPLETE_AND_REPEAT;
import static com.mishuto.todo.view_model.TestUtils.List.DELAY;
import static com.mishuto.todo.view_model.TestUtils.List.REMOVE;
import static com.mishuto.todo.view_model.TestUtils.List.checkActiveState;
import static com.mishuto.todo.view_model.TestUtils.List.checkCompletedState;
import static com.mishuto.todo.view_model.TestUtils.List.checkDeferredState;
import static com.mishuto.todo.view_model.TestUtils.List.clickTask;
import static com.mishuto.todo.view_model.TestUtils.List.clickTaskListMenu;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.list.ListController.DELETE_SNACK_DURATION;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;

/**
 * Тестирование контроллера списка задач
 * Created by Michael Shishkin on 26.12.2018.
 */
public class ListControllerTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // завершение/активация предыдущей задачи
    @Test
    public void prevEventComplete() {
        boolean curFilter = incomplete().isFilterSet();
        incomplete().setFilter(false);

        Task t1 = createTask("prevEvent-t1");   // создаем задачу 1
        clickOn(SAVE);

        Task t2 = createTask("prevEvent-t2");   // создаем задачу 2
        setPrevTask(t1);                        // задача 1 - предшествует задаче 2

        clickOn(SAVE);

        checkActiveState(t1);                   // задача 1 - активна
        checkDeferredState(t2);                 // задача 2 - отложена

        clickTaskListMenu(t1, COMPLETE);        // выбираем меню завершить

        sleep(200);
        checkActiveState(t2);     // задача 2 - активна
        checkCompletedState(t1);  // задача 1 - завершена

        TaskList.remove(t1);
        TaskList.remove(t2);
        incomplete().setFilter(curFilter);
    }

    // удаление/активация предшествующей задачи
    @Test
    public void prevEventDelete() {
        TestUtils.Filters.clearFilters();
        boolean curFilter = incomplete().isFilterSet();
        incomplete().setFilter(false);

        Task t1 = createTask("prevEventDelete A");          // создаем задачу 1
        clickOn(SAVE);

        Task t2 = createTask("prevEventDelete B");          // создаем задачу 2
        setPrevTask(t1);                                    // задача 1 - предшествует задаче 2
        clickOn(SAVE);
        sleep(1600);

        clickTaskListMenu(t1, REMOVE);  // выбираем меню удалить

        checkDeferredState(t2);   // задача 2 - пока отложена

        sleep(DELETE_SNACK_DURATION + 500);    // ждем пока исчезнет undo

        checkActiveState(t2);     // задача 2 - активна

        TaskList.remove(t2);
        incomplete().setFilter(curFilter);
    }

    // задача должна изменить статус в списке, когда его меняет предыдущая задача
    @Test
    public void changeStateWhenPrevChanged() {
        Task t1 = createTask("changeStateWhenPrevChanged - prev");  // предыдущая задача
        clickOn(SAVE);
        Task t2 = createTask("changeStateWhenPrevChanged - A");     // основная задача
        clickOn(SAVE);
        t2.setPrev(t1);                                             // назначаем задаче A задачу prev как предыдущую. У А статус - отложена
        clickTask(t1);                                    // входим в prev.
        clickOn(COMPLETE_BTN);                             // завершаем ее. у A должен измениться статус на активный
        clickOn(SAVE);
        checkActiveState(t2);                                       // проверяем

        TaskList.remove(t2);
        TaskList.remove(t1);
    }



    // задача в списке удаляется до начала редактирования другой задачи
    @Test
    public void editAfterDelete() {
        Task t1 = createTask("editAfterDelete A");
        clickOn(SAVE);

        Task t2 = createTask("editAfterDelete B");
        clickOn(SAVE);

        int n = TaskList.getTasks().size();

        clickTaskListMenu(t1, REMOVE);
        clickTask(t2);

        clickOn(R.id.toolbar_cancel);       // отмена задачи должна быть без предупреждения об изменении
        TestUtils.List.assertTaskNotExists(t1.toString());
        assertThat(TaskList.getTasks().size(), is(n-1));
        assertThat(t1.getRecordID(), is(0L));

        TaskList.remove(t2);
    }

    // если задача повторяемая, то при ее завершении должен появиться снек
    @Test
    public void completeAndRepeatSnack() {
        TCalendar today = TCalendar.now();
        today.add(Calendar.DATE, 1);

        Task task = createTask("completeAndRepeatSnack");
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        clickOn(R.id.recurrence_buttonOk);
        clickOn(SAVE);
        clickTaskListMenu(task, COMPLETE_AND_REPEAT);
        onSnackBar().check(matches(withText(containsString(today.getSpeakingDate()))));

        TaskList.remove(task);
    }

    // после удаления повторяемости статус задачи в списке меняется
    @Test
    public void completeAndRepeatState() {
        // создаем завершенную повторяемую задачу
        Task task = createTask("completeAndRepeatState");
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), click());
        clickOn(R.id.recurrence_buttonOk);
        clickOn(COMPLETE_BTN);
        clickOn(SAVE);
        TestUtils.List.checkDeferredState(task);    // задача в статусе отложена

        //удаляем повтор
        TestUtils.List.clickTask(task);
        onView(withId(R.id.taskDetails_clearRepeat)).perform(scrollTo(), click());
        clickOn(SAVE);
        TestUtils.List.checkCompletedState(task);   // задача завершена

        TaskList.remove(task);
    }

    // Проверка появления снека после активации задачи в результате деактивации предшествующей
    @Test
    public void snackAfterStateChange() {
        Task t = createTask("snackAfterStateChange");
        clickOn(SAVE);

        Task p = createTask("snackAfterStateChange P");
        TestUtils.Card.setPrevTask(t);
        clickOn(SAVE);

        clickTaskListMenu(t, COMPLETE);
        onSnackBar().check(matches(withText(startsWith(ResourcesFacade.getString(R.string.task_activated).substring(0,4)))));

        TaskList.remove(t);
        TaskList.remove(p);
    }

    // Проверка появления снека после активации задачи в результате удаления предшествующей
    @Test
    public void snackAfterDelete() {
        Task t = createTask("snackAfterDelete");
        clickOn(SAVE);

        Task p = createTask("snackAfterDelete P");
        TestUtils.Card.setPrevTask(t);
        clickOn(SAVE);

        clickTaskListMenu(t, REMOVE);
        sleep(DELETE_SNACK_DURATION + 300);
        onSnackBar().check(matches(withText(startsWith(ResourcesFacade.getString(R.string.task_activated).substring(0,4)))));

        TaskList.remove(p);
    }

    // Снек появляется после откладывания, если выбран фильтр Дата
    @Test
    public void snackAfterDelay() {
        TCalendar probe = new TCalendar(2019, 1, 1);
        Task task = createTask("snackAfterDelay");
        TestUtils.Card.setTaskDate(probe);
        clickOn(SAVE);

        TestUtils.Filters.selectSingleTag(DATE, probe.getSpeakingDate());
        clickTaskListMenu(task, DELAY);
        onView(withPositionInGrid(withId(R.id.grid_view), 6) ).perform(click());
        onSnackBar().check(matches(withText(R.string.taskList_filteredWarn)));

    }
}