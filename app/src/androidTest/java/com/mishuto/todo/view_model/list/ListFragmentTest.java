package com.mishuto.todo.view_model.list;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Rule;
import org.junit.Test;

import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.List.REMOVE;
import static com.mishuto.todo.view_model.TestUtils.List.clickTaskListMenu;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * тест фрагмента списка
 * Created by Michael Shishkin on 05.09.2017.
 */
@SuppressWarnings("unchecked")
public class ListFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    //При потере фокуса (открытии окна фильтров, например) - должны удаляться задачи, находящиеся в удаленных
    @Test
    public void deletingTaskWhenFiltersDialogOpens() {
        Task task = createTask("deletingTaskWhenFiltersDialogOpens");
        clickOn(SAVE);
        clickTaskListMenu(task, REMOVE);
        TestUtils.Filters.openFilterDialog(TestUtils.Filters.FilterTab.CONTEXT);
        assertThat(TaskList.getTasks(), not(hasItem(task)));
    }
}