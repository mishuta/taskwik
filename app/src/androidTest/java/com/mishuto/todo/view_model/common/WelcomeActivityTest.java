package com.mishuto.todo.view_model.common;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.PersistentValues;
import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Constants;
import com.mishuto.todo.view_model.EspressoHelper;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.ViewPagerActions.scrollToPage;
import static android.support.test.espresso.matcher.ViewMatchers.hasChildCount;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Тесты WelcomeActivity
 * Created by Michael Shishkin on 25.08.2018.
 */
public class WelcomeActivityTest {
    @Rule
    public ActivityTestRule<LoadCheckerActivity> mActivityRule = new ActivityTestRule<>(LoadCheckerActivity.class, false, false);

    private String titles[] = ResourcesFacade.getStringArray(R.array.welcome_titles);

    private void start() {
        PersistentValues.getFile("WelcomeActivity").getBoolean("showed", false).set(false);
        PersistentValues.getFile("BackupManager").getInt("Attempts", 0).set(0);
        mActivityRule.launchActivity(null);
    }

    // при первом старте запускатся WelcomeActivity
    @Test
    public void firstTimeLoadsWelcomeActivity()  {
        start();
        onView(withId(R.id.go)).check(matches(isDisplayed()));  // убеждаемся в наличии кнопки go
    }

    // при НЕпервом старте запускатся MainActivity
    @Test
    public void secondTimeLoadsMainActivity() {
        PersistentValues.getFile("WelcomeActivity").getBoolean("showed", false).set(true);
        mActivityRule.launchActivity(null);
        onView(withId(R.id.history_button)).check(matches(isDisplayed()));  // убеждаемся в наличии кнопки history
    }

    @Test
    public void backToMain()  {
        start();
        pressBack();
        onView(withId(R.id.history_button)).check(matches(isDisplayed()));  // убеждаемся в наличии кнопки history
    }

    //при старте на первой странице отображается корректный текст, количество точек = количеству подписей, одна точка темнее
    @Test
    public void checkStartPage() {
        start();
        onView(allOf(withId(R.id.title), isDisplayed())).check(matches(withText(titles[0])));
        onView(withId(R.id.dots)).check(matches(hasChildCount(titles.length)));
        onView(allOf(withParent(withId(R.id.dots)), EspressoHelper.hasAlpha(Constants.OPAQUE_FULL))).check(matches(isDisplayed()));
    }

    // на последненй странице отображается последний заголовок
    @Test
    public void checkLastPage() {
        start();
        int last = titles.length - 1;
        onView(withId(R.id.view_pager)).perform(scrollToPage(last));
        onView(allOf(withId(R.id.title), isDisplayed())).check(matches(withText(titles[last])));
    }

    @Test
    public void checkAlbumLastPage() throws Exception {
        start();
        int last = titles.length - 1;
        onView(withId(R.id.view_pager)).perform(scrollToPage(last));
        TestUtils.Automation.rotateLeft();
        sleep(2000);
        onView(allOf(withId(R.id.title), isDisplayed())).check(matches(withText(titles[last])));
        TestUtils.Automation.rotateBack();
    }
}