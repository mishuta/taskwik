package com.mishuto.todo.view_model;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.action.GeneralClickAction;
import android.support.test.espresso.action.GeneralLocation;
import android.support.test.espresso.action.Press;
import android.support.test.espresso.action.Tap;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.database.SQLTable;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.events.TimerPublisher;
import com.mishuto.todo.model.tags_collections.ContextsSet;
import com.mishuto.todo.model.tags_collections.Tags;
import com.mishuto.todo.model.task_fields.Order;
import com.mishuto.todo.model.task_fields.Priority;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.model.task_filters.Choice;
import com.mishuto.todo.model.task_filters.Exclusiveness;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.model.task_filters.SearchFilter;
import com.mishuto.todo.todo.R;

import org.hamcrest.Matchers;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Locale;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.ViewPagerActions.scrollToPage;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.hasFocus;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.common.TextUtils.uniqueStr;
import static com.mishuto.todo.view.Constants.OPAQUE_FULL;
import static com.mishuto.todo.view.Constants.OPAQUE_MID;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.hasAlpha;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerView;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerViewInCollapsedAppBar;
import static com.mishuto.todo.view_model.EspressoHelper.onTextInputLayout;
import static com.mishuto.todo.view_model.EspressoHelper.scrollClick;
import static com.mishuto.todo.view_model.TestUtils.Reflection.getField;
import static com.mishuto.todo.view_model.TestUtils.Reflection.setField;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertTrue;

/**
 * библиотека общих статических функции, используемых в эспрессо-тестах
 * Created by Michael Shishkin on 30.08.2017.
 */

@SuppressWarnings("UnusedReturnValue")
public class TestUtils {

    /******************* макросы для списка задач ***************************************/
    public static class List {
        // проверка что задача в списке в активном состоянии (есть иконка очередь)
        public static ViewInteraction checkActiveState(Task task) {
            return onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_Order), hasSibling(withChild(withChild(withText(task.toString())))))).check(matches(isDisplayed()));

        }

        // проверка что задача в списке в состоянии отложена (нет иконки и альфа=100%)
        public static ViewInteraction checkDeferredState(Task task) {
            onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_title), withText(task.toString()))).check(matches(hasAlpha(OPAQUE_FULL)));
            return onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_Order), hasSibling(withChild(withChild(withText(task.toString())))))).check(matches(not(isDisplayed())));
        }

        // проверка что задача в списке в состоянии завершена (нет иконки и альфа=50%)
        public static ViewInteraction checkCompletedState(Task task) {
            onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_title), withText(task.toString()))).check(matches(hasAlpha(OPAQUE_MID)));
            return onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_Order), hasSibling(withChild(withChild(withText(task.toString())))))).check(matches(not(isDisplayed())));
        }

        public static final int COMPLETE = R.string.taskMenu_complete;
        public static final int COMPLETE_AND_REPEAT = R.string.taskMenu_completeAndRepeat;
        public static final int REMOVE = R.string.taskMenu_remove;
        public static final int ACTIVATE = R.string.taskMenu_activate;
        public static final int DELAY = R.string.taskMenu_delay;

        // клик по меню задачи и выполнение команды
        public static void clickTaskListMenu(Task task, int menuString) {
            // кликаем по кнопке меню
            onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_menuButton), hasSibling(withChild(withChild(withText(task.toString()))))))
                    .perform(click());

            sleep(100);
            //выбираем команду
            onView(withText(menuString)).inRoot(isPlatformPopup()).perform(click());
        }

        public static final int SHOW_DELAYED = R.string.listMenu_showDelayed;
        public static final int SHOW_COMPLETED = R.string.listMenu_showCompleted;
        public static final int SETTINGS = R.string.listMenu_settings;
        public static final int HELP = R.string.listMenu_help;

        // клик по пункту меню списка задач в тулбаре
        public static void clickListOptionsMenu(int item) {
            openContextualActionModeOverflowMenu();
            onView(withText(item)).perform(click());
        }

        // создание задачи из списка задач
        public static Task createTask(String prefix) {
            clickOn(R.id.floatingActionButton);
            Card.setTitle(prefix);
            return TaskList.getLast();
        }

        // клик по задаче в списке
        public static void clickTask(Task task) {
            onRecyclerView(R.id.recycler_view, withText(task.toString())).perform(click());
        }

        //проверка существования задачи с заданным именем в списке
        public static void assertTaskExists(String title) {
            onRecyclerView(R.id.recycler_view, Matchers.allOf(withId(R.id.taskListItem_title), withText(title))).check(matches(isDisplayed()));
        }

        //проверка отсутствия задачи с заданным именем в списке
        public static void assertTaskNotExists(String title) {
            onView(Matchers.allOf(withId(R.id.taskListItem_title), withText(title))).check(doesNotExist());
        }
    }
    /* ****************** макросы диалога и тулбара фильтров *********************************************************** */
    public static class Filters {

        public enum FilterTab {DATE, PROJECT, CONTEXT, EXECUTOR}  // список диалогов

        // открытие диалога фильтров на нужной странице
        public static void openFilterDialog(FilterTab filterTab) {
            if(Choice.getActiveChoiceFilter() == null || FilterManager.search().isFilterSet())
                clickOn(R.id.toolbar_filter_btn);    // если фильтр не установлен (или установлен серч) - просто нажимаем кнопку
            else                                     // иначе кликаем по заголовку
                onView(allOf(isDescendantOfA(withId(R.id.toolbar_filter_value)),not(isAssignableFrom(EditText.class)))).perform(click());

            scrollToTab(filterTab);
        }

        public static void scrollToTab(FilterTab tab) {
            onView(withId(R.id.view_pager)).perform(scrollToPage(tab.ordinal()));
        }

        //открыть диалог фильтров, выбрать заданный тег
        public static void selectSingleTag(FilterTab tab, String tag) {
            openFilterDialog(tab);
            onTag(tag).perform(click());
        }

        // удалить тег в окне фильтра
        public static void removeTag(String tag) {
            onRecyclerViewInCollapsedAppBar(Matchers.allOf(withId(R.id.recycler_view), isDisplayed()),
                    Matchers.allOf(withId(R.id.popup_menu), hasSibling(withChild(withText(tag)))))
                    .perform(click());                  // находим нужный проект и кликаем на кнопку меню

            onView(withText(R.string.taskMenu_remove)).inRoot(isPlatformPopup()).perform(click());  // кликаем Удалить
        }

        // получить контрол-чекбокс в диалоге фильтра-контекста
        public static ViewInteraction onCheckContext(String context) {
            return onRecyclerViewInCollapsedAppBar(allOf(withId(R.id.recycler_view), hasFocus()), allOf(withId(R.id.check), hasSibling(withChild(withText(context)))));
        }

        // получить виджет-название тега по имени
        public static ViewInteraction onTag(String tag) {
            return onRecyclerViewInCollapsedAppBar(
                    allOf(withId(R.id.recycler_view), isDisplayed()),
                    allOf(withText(tag), not(isAssignableFrom(EditText.class))));
        }

        // сбросить фильтры в списке задач
        public static void clearFilters() {
            Exclusiveness filter = Exclusiveness.getExclusiveFilter();
            if(filter != null)                          // если фильтр установлен
                if(filter instanceof SearchFilter)      // и это серч
                    clickOn(R.id.toolbar_search_btn);   // отжимаем серч
                else
                    clickOn(R.id.toolbar_filter_btn);   // иначе отжимаем кнопку фильтры
        }

        // проверка на соответствие тулбара с фильтрами заданному тесту
        public static void checkToolbarTitleView(String title) {
            onView(Matchers.allOf(isDescendantOfA(withId(R.id.toolbar_filter_value)), isDisplayed()))
                    .check(matches(withText(title == null ? ResourcesFacade.getString(R.string.app_name) : title)));
        }

        // ввести поисковый запрос в тулбар
        public static void doSearch(String query) {
            clickOn(R.id.toolbar_search_btn);
            onTextInputLayout(R.id.toolbar_filter_value).perform(ViewActions.typeText(query), pressImeActionButton());
        }
    }

    /*************************************************************************************
    ******************* макросы для карточки задачи **************************************
    **************************************************************************************/
    public static class Card {

        public static final int SAVE = R.id.toolbar_save;
        public static final int OK = android.R.id.button1;
        public static final int CANCEL = android.R.id.button2;
        public static final int COMPLETE_BTN = R.id.toolbar_perform;


        // отменяет и подтверждает отмену измененной задачи. Если задача новая - она удаляется
        public static void cancelTask() {
            clickOn(R.id.toolbar_cancel);
            onView(withText(R.string.yes)).perform(click());
        }


        public static String setTitle(String prefix) {
            String title = uniqueStr(TaskList.getTasks(), prefix);
            clickOn(R.id.taskDetails_titleView);
            onTextInputLayout(R.id.taskDetails_titleView).perform(replaceText(title), pressImeActionButton());
            return title;
        }

        public static void checkTitle(String title) {
            onView(allOf(isDescendantOfA(withId(R.id.taskDetails_titleView)),not(isAssignableFrom(EditText.class)), withText(title))).check(matches(isDisplayed()));
        }

        // установка времени задачи из карточки.
        public static void setTaskTime(int hh, int mm) {
            scrollClick(R.id.taskDetails_label_time);
            setTimePicker(hh, mm);
        }

        // установка даты задачи из карточки.
        public static void setTaskDate(TCalendar date) {
            clickOn(R.id.taskDetails_label_term);
            onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(date.get(YEAR), date.get(MONTH) + 1, date.get(DAY_OF_MONTH)));
            clickOn(android.R.id.button1);
        }

        // установка предшествующей задачи
        public static void setPrevTask(Task prevTask) {
            scrollClick(R.id.taskDetails_label_prevTask);
            onRecyclerView(R.id.tasks_recyclerView,
                    Matchers.allOf(withId(R.id.search_taskName), withText(prevTask.toString()))).perform(closeSoftKeyboard(),click());
        }

        public static void setExecutor(String executor) {
            scrollClick(R.id.taskDetails_label_executor);
            onTextInputLayout(R.id.taskDetails_executor).perform(replaceText(executor), closeSoftKeyboard());
        }

        public static void setProject(String project) {
            scrollClick(R.id.taskDetails_label_project);
            onTextInputLayout(R.id.dlg_tag_newTag).perform(typeText(project), pressImeActionButton());
        }

        public static void setPriority(Priority priority) {
            clickOn(R.id.taskDetails_Spinner_Priority);
            onData(allOf(is(instanceOf(Priority.class)), is(priority))).perform(click());
        }

        public static void checkPriority(Priority checkedPriority) {
            onView(withId(R.id.prioritySpinner_Text)).check(matches(withText(checkedPriority.title)));
        }

        public static void setContext(String... contexts) {
            onView(withId(R.id.taskDetails_label_context)).perform(scrollTo(), ViewActions.click());
            for (String context : contexts)
                // если контекст уже есть - просто кликаем его. Если нет - добавляем
                if(ContextsSet.getInstance().contains(context))
                    onView(withText(context)).perform(click());
                else
                    onTextInputLayout(R.id.dlg_tag_newTag).perform(typeText(context), pressImeActionButton(), closeSoftKeyboard());

            clickOn(OK);
        }

        public static void addComment(String comment) {
            clickOn(R.id.taskDetails_comment);
            onView(withId(R.id.dlg_description_newItem)).perform(typeText(comment));
            clickOn(R.id.addButton);
        }

        public static void setOrder(Order order) {
            onView(withId(R.id.taskDetails_seekBar_order)).perform(
                    new GeneralClickAction(                 // выполнить клик по seekBar
                            Tap.SINGLE,                     // однократное нажатие
                            order == Order.LOW ? GeneralLocation.CENTER_LEFT : order == Order.HIGH ? GeneralLocation.CENTER_RIGHT : GeneralLocation.VISIBLE_CENTER, // в заданную позицию
                            Press.FINGER, 0, 0));           // область = указательный палец
        }
    }

    /********************** прочие методы*****************************************************/

    // создать уникальный объект типа Tags (Executor, Project, Context)
    public static StringContainer createTag(Tags tagsObject, String prefix) {
        StringContainer tag = new StringContainer(uniqueStr(tagsObject.getTags(), prefix));
        tagsObject.add(tag);
        return tag;
    }

    // добавить задачу в список TaskList (не через интерфейс)
    public static Task addNewTask(String title) {
        Task task = TaskList.addNew();
        task.getTitle().setTaskName(title);
        return task;
    }

    public static void setTimePicker(int hour, int minute) {
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(hour, minute));
        clickOn(android.R.id.button1);
    }

    // запрос количества объектов данного класса в БД
    public static int getObjectsCount(Class c) {
        SQLTable table = new SQLTable(c.getSimpleName(), (SQLTable.Column[]) null);
        return table.doesTableExist() ? table.query().countNClose() : 0;
    }

    // возвращает русскую или английскую строку, в зависимости от выбранной локализации
    public static String getLocalized(String ruStr, String enStr) {
        return (Locale.getDefault().getLanguage().equals(new Locale("ru").getLanguage())) ? ruStr : enStr;
    }


    // устанаовка таймера задач на DELAY секунд впреред и ожидание его срабатывания
    private final static int MIN_TIMER_DELAY = 5; // минимальный интервал срабатывания таймера
    public static void setTaskTimerAndSleep(Task... tasks) throws Exception {
        TCalendar event = TCalendar.now();
        event.add(Calendar.SECOND, MIN_TIMER_DELAY);                                // вычисляем время таймера

        for (Task task : tasks) {
            TaskTime taskTime = (TaskTime) getField(Task.class, task, "mTaskTime"); // поскольку штатно можно установить только минутные интервалы
            setField(TaskTime.class, taskTime, "mCalendar", event);                 // измененяем атрибуты времени задачи mTaskTime и таймера задачи mTimerPublisher
            setField(TaskTime.class, taskTime, "isTimeSet", true);                  // изменяем поля, чтобы не использовать блокирующий update() в методах setXXX

            TimerPublisher timerPublisher = (TimerPublisher) getField(Task.class, task, "mTimer");
            timerPublisher.setTime(event);
        }
        sleep(MIN_TIMER_DELAY * 1000 + 200);
    }

    /***************** AUTOMATION *******************************/
    public static class Automation {
        private static final UiDevice UI_DEVICE = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        public static void pressHome() {
            UI_DEVICE.pressHome();
        }

        // выйти в бэкграунд и вернуться
        public static void goHomeAndBack() throws Exception {
            UI_DEVICE.pressHome();
            sleep(200);
            UI_DEVICE.pressRecentApps();
            clickOnText("Taskwik");
        }

        public static void rotateLeft() throws Exception {
            UI_DEVICE.setOrientationLeft();
        }

        public static void rotateBack() throws Exception {
            UI_DEVICE.setOrientationNatural();
        }

        public static void clickOnText(String s) throws Exception {
            UI_DEVICE.wait(Until.hasObject(By.text(s)), 1000);
            UiObject text = UI_DEVICE.findObject(new UiSelector().text(s));
            assertTrue(text.exists());
            text.click();
        }

        // имитация уничтожения приложения операционной системой.
        // Внимание: предварительно необходимо перевести приложение в бэкграунд, например командой pressHome()
        // Внимание: работает только с api 21 и выше
        public static void killApp() throws Exception {
            UI_DEVICE.executeShellCommand("am kill com.mishuto.todo.taskwik.alarm");
        }

        // запуск Taskwik из списка приложений
        public static void runApp() throws Exception {
            UiObject allAppsButton = UI_DEVICE.findObject(new UiSelector().description("Приложения"));    // ярлык на список приложений
            if(!allAppsButton.exists())   // английская локаль
                allAppsButton = UI_DEVICE.findObject(new UiSelector().description("Apps"));

            allAppsButton.clickAndWaitForNewWindow();                                                   // открываем
            UiScrollable appViews = new UiScrollable(new UiSelector().scrollable(true));                // скролируемый список кнопок
            UiObject taskwik;

            if(android.os.Build.VERSION.SDK_INT > 19) {                                                 // в 6-м андроиде
                appViews.flingForward();                                                                // скролим вниз
                taskwik = appViews.getChild(new UiSelector().className("android.widget.TextView").text("Taskwik"));
            }
            else {                                                                                      // в 4-м андроиде
                UiObject appsTab = UI_DEVICE.findObject(new UiSelector().text("Приложения"));             // открываем вкладку с приложениями
                if(!appsTab.exists())   // английская локаль
                    appsTab = UI_DEVICE.findObject(new UiSelector().description("Apps"));

                appsTab.click();
                appViews.setAsHorizontalList();
                taskwik = appViews.getChildByText(new UiSelector().className(android.widget.TextView.class.getName()), "Taskwik");  // почему-то эта строка не работает в 6-ке
            }
            taskwik.clickAndWaitForNewWindow();
        }
    }

    /*********************** Reflection ********************************/
    public static class Reflection {
        // получение статического объекта-закрытого члена класса
        public static <T> Object getStaticField(Class<T> classWhereFieldIs, String field) throws NoSuchFieldException, IllegalAccessException {
            return getField(classWhereFieldIs, null, field);
        }

        // получение объекта - закрытого члена класса
        public static <T> Object getField(Class<T> classWhereFieldIs, Object usedObject, String field) throws NoSuchFieldException, IllegalAccessException {
            Field f = classWhereFieldIs.getDeclaredField(field);
            f.setAccessible(true);
            return f.get(usedObject);
        }

        // установка объекта - закрытого члена класса
        public static <T> void setField(Class<T> classWhereFieldIs, Object usedObject, String field, Object value) throws NoSuchFieldException, IllegalAccessException {
            Field f = classWhereFieldIs.getDeclaredField(field);
            f.setAccessible(true);
            f.set(usedObject, value);
        }

        // вызов закрытого метода класса без параметров
        public static <T> Object execute(Class<T> classWhereMethodIs, String methodName, Object usedObject) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
            Method method = classWhereMethodIs.getDeclaredMethod(methodName);
            method.setAccessible(true);
            return method.invoke(usedObject);
        }

        // создание объекта закрытым конструктором без параметров
        public static <T> Object newInstance(Class<T> classWhereConstructorIs) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
            Constructor<T> constructor = classWhereConstructorIs.getDeclaredConstructor((Class<T>[])null);
            constructor.setAccessible(true);
            return constructor.newInstance((Object[])null);
        }

    }
}
