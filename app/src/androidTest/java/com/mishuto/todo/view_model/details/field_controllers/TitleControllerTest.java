package com.mishuto.todo.view_model.details.field_controllers;

import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.common.TextUtils.uniqueStr;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.EspressoHelper.onTextInputLayout;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.Card.checkTitle;
import static com.mishuto.todo.view_model.TestUtils.Card.setTitle;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Тесты контроллера названия задачи
 * Created by Michael Shishkin on 14.05.2017.
 */

public class TitleControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // измененное название сохраняется в базе
    @Test
    public void changeTitle() {
        Task task =createTask("changeTitle");

        String title = setTitle("changeTitle - A");

        clickOn(SAVE);

        sleep(200);
        task.reload();
        assertThat(task.getTitle().getTaskName(), is(title));

        TaskList.remove(task);
    }

    // если пользователь начал вводить название, приложение ушло в бэк, вернулось - введенное название должно сохраниться. Задача должна сохраниться
    @Test
    public void saveInBackground() throws Exception {
        while (!TaskList.get().hasLoaded()) sleep(50);  // т.к. в следующей операции мы получаем список задач до взаимодействия с UI, тупо ждем пока задачи загрузятся
        String title = uniqueStr(TaskList.getTasks(), "saveInBackground");

        Task task = createTaskAndTypeName(title);

        TestUtils.Automation.goHomeAndBack();
        sleep(100);
        checkTitle(title);                      // название сохранилось

        clickOn(SAVE);
        sleep(200);
        TestUtils.List.assertTaskExists(title); // задача сохранилась

        TaskList.remove(task);
    }

    // нажатие кнопки back во время ввода имени возвращает контрол в ViewMode
    @Test
    public void backWhenType() {
        Task task = createTaskAndTypeName("backWhenType");  // начинаем вводить имя
        pressBack();                                        // отработал обработчик контрола. контрол вернулся в ViewMode
        pressBack();                                        // отработал обработчик фрагмента. отработал doCancel()
        onView(withText(R.string.no));                      // отвечаем нет на предложение отменить изменения
        clickOn(SAVE);                                      // сохраняем задачу
        TestUtils.List.assertTaskExists("backWhenType");    // проверяем что она есть
        TaskList.remove(task);
    }

    // если при вводе первых 3-х символов находится задача с таким названием, она должна отобразиться (и только она)
    @Test
    public void checkShortNameTasks() {
        String name = "zxc";
        String name0 = "zx";
        String name1 = "zxcv";

        Task probe1 = createIfNotExists(name0);                                         // создаем 3 задачи с похожими названиями
        Task probe2 = createIfNotExists(name);
        Task probe3 = createIfNotExists(name1);

        createTaskAndTypeName(name);                                                    // печатаем 3 символа zxc

        onView(withText(name)).inRoot(isPlatformPopup()).check(matches(isDisplayed())); // убеждаемся что отображается только одна задача
        onView(withText(name0)).inRoot(isPlatformPopup()).check(doesNotExist());
        onView(withText(name1)).inRoot(isPlatformPopup()).check(doesNotExist());

        clickOn(R.id.toolbar_cancel);
        TaskList.remove(probe1);
        TaskList.remove(probe2);
        TaskList.remove(probe3);
    }

    // при вводе >THRESHOLD символов должны отобразиться все задачи
    @Test
    public void checkTasksLikeThis() {
        String name = "12345";
        String name1 = "zx12345";
        String name2 = "123456zxcv";
        String name3 = "zx12345cv";

        Task probe1 = createIfNotExists(name1);                                             // создаем 3 задачи с похожей сигнатурой
        Task probe2 = createIfNotExists(name2);
        Task probe3 = createIfNotExists(name3);

        createTaskAndTypeName(name);                                                        // вводим подстроку

        onView(withText(name1)).inRoot(isPlatformPopup()).check(matches(isDisplayed()));    // проверяем что все задачи отображаются в списке автокомплита
        onView(withText(name2)).inRoot(isPlatformPopup()).check(matches(isDisplayed()));
        onView(withText(name3)).inRoot(isPlatformPopup()).check(matches(isDisplayed()));

        cancelTask();
        TaskList.remove(probe1);
        TaskList.remove(probe2);
        TaskList.remove(probe3);
    }


    //при выборе существующей задачи она загружается в карточку. Новая задача исчезает
    @Test
    public void changeTaskInCard() {
        String name = "changeTaskInCard";
        Task probe = createIfNotExists(name);
        probe.getComments().add(name);                                              // записываем комментарий ,чтобы опознать задачу

        int n = TaskList.getTasks().size();                                         // запоминаем количество задач в списке

        clickOn(R.id.floatingActionButton);                                         // создаем новую
        selectTaskInAutoComplete(name);                                             // выбираем пробу

        checkTitle(name);                                                           // имя задачи в карточке = пробе
        onView(withId(R.id.taskDetails_comment)).check(matches(withText(name)));    // комментарий = пробе, значит задача перезагрузилась
        onSnackBar().check(matches(isDisplayed()));                                 // снек о том что задача загрузилась в карточку

        clickOn(SAVE);

        assertThat(TaskList.getTasks().size(), is(n));                              // убеждаемся что новая задача не сохранилась
        TaskList.remove(probe);
    }

    // при загрузке в карточку существующей задачи, в ней можно добавить комментарий (баг #250)
    @Test
    public void changeTaskAndEnterComment() {
        String name = "changeTaskAndEnterComment";
        Task probe = createIfNotExists(name);

        clickOn(R.id.floatingActionButton);
        selectTaskInAutoComplete(name);
        TestUtils.Card.addComment("comment");                                       // добавляем к выбранной задаче комментарий
        clickOn(SAVE);
        assertThat(probe.getComments().getActual().getComment(), is("comment"));    // комментарий добавился

        TaskList.remove(probe);
    }

    //если в задаче были изменения, появляется диалог. Ответ сохранить - сохраняет задачу
    @Test
    public void oldTaskSaves() {
        String name = "probe-ots";
        Task probe = createIfNotExists(name);
        probe.getComments().add(name);

        Task task = createTask("oldTaskSaves");
        selectTaskInAutoComplete(name);

        onView(withText(R.string.yes)).perform(click());

        checkTitle(name);
        onView(withId(R.id.taskDetails_comment)).check(matches(withText(name)));
        sleep(100);
        onSnackBar().check(matches(isDisplayed()));

        clickOn(SAVE);
        TestUtils.List.assertTaskExists(task.toString());

        TaskList.remove(probe);
        TaskList.remove(task);
    }

    //если в задаче были изменения, появляется диалог. Ответ не сохранять - не сохраняет задачу
    @Test
    public void oldTaskNotSave() {
        String name = "probe-onts";
        Task probe = createIfNotExists(name);
        probe.getComments().add(name);

        int n = TaskList.getTasks().size();

        createTask("oldTaskNotSave");
        selectTaskInAutoComplete(name);

        onView(withText(R.string.no)).perform(click());

        checkTitle(name);
        onView(withId(R.id.taskDetails_comment)).check(matches(withText(name)));
        sleep(100);
        onSnackBar().check(matches(isDisplayed()));

        clickOn(SAVE);
        assertEquals(n, TaskList.getTasks().size());

        TaskList.remove(probe);
    }

    //если в задаче были изменения, появляется диалог. Ответ отмена - не загружает новую задачу
    @Test
    public void cancelNotLoadsNewTask() {
        String name = "probe-cnlnt";
        Task probe = createIfNotExists(name);
        probe.getComments().add(name);

        createTask("cancelNotLoadsNewTask");
        selectTaskInAutoComplete(name);

        onView(withText(android.R.string.cancel)).perform(click());

        onView(withId(R.id.taskDetails_comment)).check(matches(withText("")));
        sleep(100);
        onSnackBar().check(doesNotExist());

        cancelTask();
        TaskList.remove(probe);
    }

    private Task createIfNotExists(String name) {
        for (Task task : TaskList.getTasks())
            if(task.toString().equalsIgnoreCase(name))
                return task;

        Task task = TaskList.addNew();
        task.getTitle().setTaskName(name);
        return task;
    }

    private void selectTaskInAutoComplete(String taskName) {
        clickOn(R.id.taskDetails_titleView);
        onTextInputLayout(R.id.taskDetails_titleView).perform(replaceText(taskName.substring(0,5)));
        onView(withText(taskName)).inRoot(isPlatformPopup()).perform(ViewActions.click());
    }

    // аналог TestUtils.List.createTask, но без  нажатия ImeActionButton для ввода имени
    private Task createTaskAndTypeName(String taskName) {
        clickOn(R.id.floatingActionButton);
        clickOn(R.id.taskDetails_titleView);
        onTextInputLayout(R.id.taskDetails_titleView).perform(replaceText(taskName));
        return TaskList.getLast();
    }
}
