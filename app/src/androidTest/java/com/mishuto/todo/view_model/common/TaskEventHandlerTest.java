package com.mishuto.todo.view_model.common;

import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.events.TimerPublisher;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.model.task_fields.recurrence.Recurrence;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerView;
import static com.mishuto.todo.view_model.EspressoHelper.onSnackBar;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setTaskTime;
import static com.mishuto.todo.view_model.TestUtils.Filters.clearFilters;
import static com.mishuto.todo.view_model.TestUtils.List.COMPLETE_AND_REPEAT;
import static com.mishuto.todo.view_model.TestUtils.List.checkActiveState;
import static com.mishuto.todo.view_model.TestUtils.List.checkDeferredState;
import static com.mishuto.todo.view_model.TestUtils.List.clickTaskListMenu;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.setTaskTimerAndSleep;
import static org.hamcrest.CoreMatchers.allOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Тестирование событий от таймера
 * Created by Michael Shishkin on 17.09.2017.
 */
public class TaskEventHandlerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // проверяем срабатывание таймера через 1 сек
    @Test
    public void testTimer() throws Exception {
        Task task = TaskList.addNew();

        TCalendar event = TCalendar.now();      // создаем событие срабатывания
        event.add(Calendar.MINUTE, 1);          // на следующую минуту

        task.getTaskTime().setCalendar(event, true);    //устанавливаем событие в задачу
        task.setTimerEvent();                     //заводим таймер

        // проверяем корректность установки
        assertEquals(event.get(Calendar.MINUTE), task.getTimerTime().get(Calendar.MINUTE));
        assertEquals(0, task.getTimerTime().get(Calendar.SECOND));
        assertEquals(0, task.getTimerTime().get(Calendar.MILLISECOND));

        setTaskTimerAndSleep(task);

        onSnackBar().check(matches(isDisplayed())); // проверяем что снек появился

        TaskList.remove(task);
    }

    // проверяем срабатывание таймера в списке задач
    @Test
    public void testTimerInList() throws Exception {
        clearFilters();
        TCalendar event = TCalendar.now();
        event.add(Calendar.MINUTE,2);

        Task task = createTask("testTimerInList");
        setTaskTime(event.get(Calendar.HOUR_OF_DAY), event.get(Calendar.MINUTE));
        clickOn(SAVE);

        checkDeferredState(task); // проверяем что задача стала отложенной

        setTaskTimerAndSleep(task);

        sleep(200);
        checkActiveState(task);   // проверяем что задача стала активной

        TaskList.remove(task);
    }

    // проверка создания таймера при чтении объекта Task
    @Test
    public void testLoadTimer() {
        Task task = TaskList.addNew();

        TCalendar event = TCalendar.now();
        event.add(Calendar.DATE, 1);

        task.getTaskTime().setCalendar(event, false);  // сохраняем срок задачи в БД, но не устанавливаем таймер

        task.reload();
        assertEquals(event.clipTo(Calendar.DATE), task.getTimerTime());

        TaskList.remove(task);
    }

    // проверка удаления таймера при удалении задачи
    @Test
    public void testEraseTimer() throws Exception {
        Task task = TaskList.addNew();

        TCalendar event = TCalendar.now();
        event.add(Calendar.DATE, 1);

        task.getTaskTime().setCalendar(event, false);
        task.setTimerEvent();
        TimerPublisher timerPublisher = (TimerPublisher) TestUtils.Reflection.getField(Task.class, task, "mTimer");

        assertTrue(timerPublisher.isAlarmOn());    // таймер установился

        TaskList.remove(task);

        assertFalse(timerPublisher.isAlarmOn());       // таймер сбросился
    }

    // проверка перехода задачи из повторяемой в активную при наступлении срока Recurrence
    @Test
    public void checkRecurrenceTimer() throws Exception {
        Task task = TaskList.addNew();
        task.setPerformed(true);
        task.getRecurrence().set(true); // устанавливаем Recurrence

        assertTrue(task.isStateRepeated());
        setTaskTimerAndSleep(task);         // устанавливаем таймер. По событию таймера флаг performed должен быть сброшен

        assertTrue(task.isStateActive());
        TaskList.remove(task);
    }

    // Проверка, что при загрузке Выполненной задачи из БД с истекшим временем повторения, задача переводится в статус Активная
    @Test
    public void testExpiredRecurrence() throws Exception {
        Task task = createTask("testExpiredRecurrence");                                  //создаем задачу
        onView(withId(R.id.taskDetails_label_recurrence)).perform(scrollTo(), ViewActions.click()); //кликаем на recurrence
        clickOn(R.id.recurrence_buttonOk);                                                          //сохраняем (ежедневные повторения)
        clickOn(SAVE);
        sleep(1600);
        clickTaskListMenu(task, COMPLETE_AND_REPEAT);                              // завершаем задачу

        TaskTime startTime = (TaskTime) TestUtils.Reflection.getField(Recurrence.class, task.getRecurrence(), "mStartEvent");
        startTime.setCalendar(new TCalendar(2017, 8, 28), false);                                   // подменяем время завершения, на событие в прошлом

        assertTrue(task.isStateRepeated());                                                         // проверяем что задача все еще в статусе Repeated
        task.reload();                                                                              // перегружаем
        assertTrue(task.isStateActive());                                                           // статус изменился

        TaskList.remove(task);
    }

    // задача в списке апдейтится (при возвращении из карточки), если за время нахождения в карточке пришло событие по таймеру
    @Test
    public void updateInListWhileTimeExpiredItIsInCard() throws Exception {
        Task task = createTask("updateInListWhileTimeExpiredItIsInCard");
        setTaskTime(23,59);
        clickOn(SAVE);

        onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_title), withText(task.toString()))).perform(ViewActions.click());
        setTaskTimerAndSleep(task);
        clickOn(SAVE);
        sleep(200);
        checkActiveState(task);

        TaskList.remove(task);
    }
}