package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.EspressoHelper;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.hasFocus;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerViewInCollapsedAppBar;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setExecutor;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.EXECUTOR;
import static com.mishuto.todo.view_model.TestUtils.Filters.clearFilters;
import static com.mishuto.todo.view_model.TestUtils.Filters.onTag;
import static com.mishuto.todo.view_model.TestUtils.Filters.openFilterDialog;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

/**
 * Тест выбора фильтра по исполнителю
 * Created by Michael Shishkin on 18.08.2017.
 */
public class ExecutorFilterFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // в диалоге фильтра исполнителя должно выводиться правильное количество задач, где он есть
    @Test
    public void getItemCount()  {
        Executors executors  = Executors.getInstance();

        StringContainer executor = TestUtils.createTag(executors, "getItemCount");

        Task t1 = createTask("getItemCountA");
        setExecutor(executor.toString());
        clickOn(SAVE);

        Task t2 = createTask("getItemCountB");
        setExecutor(executor.toString());
        clickOn(SAVE);

        openFilterDialog(EXECUTOR);

        onRecyclerViewInCollapsedAppBar(allOf(withId(R.id.recycler_view), hasFocus()), allOf(withId(R.id.num_tasks), hasSibling(withChild(withText(executor.toString())))))
                .check(matches(withText("2")));                                  // убеждаемся что к нему привязаны 2 задачи

        TaskList.remove(t2);
        TaskList.remove(t1);
        executors.remove(executor);
    }

    // наложение фильтра не влияет на количество задач в диалоге фильтра
    @Test
    public void getItemCount2() {
        Executors executors  = Executors.getInstance();

        //создаем задачу с заданным исполнителем
        StringContainer executor = TestUtils.createTag(executors, "getItemCount2");

        Task t1 = createTask("getItemCount2");
        setExecutor(executor.toString());
        clickOn(SAVE);

        // фильтруем задачу по исполнителю
        openFilterDialog(EXECUTOR);
        onTag(executor.toString()).perform(ViewActions.click());

        // проверяем что количество задач в диалоге Исполнителя - верное
        openFilterDialog(EXECUTOR);

        onRecyclerViewInCollapsedAppBar(allOf(withId(R.id.recycler_view), isDisplayed()),
                allOf(withId(R.id.num_tasks), hasSibling(withChild(withText(executor.toString())))))
                .check(matches(withText("1"))); // 1 задача

        Espresso.pressBack();
        clearFilters();
        executors.remove(executor);
        TaskList.remove(t1);
    }

    // при наличии давно неиспользуемых исролнителей - они удаляются при открытии фильтра с отображением снека
    @Test
    public void clearUnused() throws Exception {
        Settings.getEtc().executorsAutoClear.set(true);
        Executors executors  = Executors.getInstance();
        TCalendar lastUsed = new TCalendar(2017, 1, 1);                                                     // дата последнего использования

        StringContainer clearUnusedTag = TestUtils.createTag(executors, "clearUnused");                     // создаем неиспользуемого исполнителя

        Class StatClass = Class.forName("com.mishuto.todo.model.tags_collections.Executors$Statistics");
        Object stat = executors.getExecutorMap().get(clearUnusedTag);
        TestUtils.Reflection.setField(StatClass, stat, "mLastUsed", lastUsed);                                         // устанавливаем ему дату последнего использования февраль 2017

        openFilterDialog(EXECUTOR);
        EspressoHelper.onSnackBar().check(matches(isDisplayed()));                                          // фиксируем снек при открытии диалога

        assertThat(executors.getExecutorMap().keySet(), not(hasItem(clearUnusedTag)));                      // сам исполнитель удалился
    }
}