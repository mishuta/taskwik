package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.widget.EditText;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.ProjectsSet;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.hasFocus;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.common.TextUtils.uniqueStr;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.hasViewInPosition;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerView;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerViewInCollapsedAppBar;
import static com.mishuto.todo.view_model.EspressoHelper.withPositionIn;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setProject;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.PROJECT;
import static com.mishuto.todo.view_model.TestUtils.Filters.clearFilters;
import static com.mishuto.todo.view_model.TestUtils.Filters.onTag;
import static com.mishuto.todo.view_model.TestUtils.Filters.openFilterDialog;
import static com.mishuto.todo.view_model.TestUtils.Filters.removeTag;
import static com.mishuto.todo.view_model.TestUtils.Filters.selectSingleTag;
import static com.mishuto.todo.view_model.TestUtils.List.REMOVE;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.createTag;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

/**
 * Тестирование диалога ProjectFilter
 * Created by Michael Shishkin on 22.07.2017.
 */
public class ProjectFilterFragmentTest {

    private ProjectsSet mProjectsSet = ProjectsSet.getInstance();

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    // количества задач у проекта должно быть верным
    @Test
    public void getItemCount() {

        StringContainer project = createTag(mProjectsSet, "getItemCount");

        Task t1 = TaskList.addNew();                                           // привязываем к проекту две задачи
        t1.setProject(project);
        Task t2 = TaskList.addNew();
        t2.setProject(project);

        openFilterDialog(PROJECT);

        onRecyclerViewInCollapsedAppBar(allOf(withId(R.id.recycler_view), isDisplayed()),
                allOf(withId(R.id.num_tasks), hasSibling(withChild(withText(project.toString())))))
                .check(matches(withText("2")));                                   // убеждаемся что к проекту привязаны 2 задачи

        TaskList.remove(t2);
        TaskList.remove(t1);
        mProjectsSet.remove(project);
    }

    //Проект должен создаваться в диалоге
    @Test
    public void addProject() {
        StringContainer project = new StringContainer(uniqueStr(mProjectsSet.getTags(), "addProject"));  // получаем уникальное имя

        openFilterDialog(PROJECT);
        clickOn(R.id.floatingActionButton);
        onView(allOf(isAssignableFrom(EditText.class), isDisplayed())).perform(typeText(project.toString()), pressImeActionButton());


        onTag(project.toString()).check(matches(isDisplayed()));    // проверяем наличие нового проекта

        mProjectsSet.remove(project);
    }

    // Проект должен удаляться
    @Test
    public void removeProject() {
        StringContainer project = createTag(mProjectsSet, "removeProject");
        String removedProject = project.toString(); // копия названия удаляемого проекта, чтобы можно было проверить после удаления

        openFilterDialog(PROJECT);

        onRecyclerViewInCollapsedAppBar(allOf(withId(R.id.recycler_view), isDisplayed()),
                allOf(withId(R.id.popup_menu), hasSibling(withChild(withText(removedProject)))))
                .perform(click());                  // находим нужный проект и кликаем на кнопку меню

        onView(withText(R.string.taskMenu_remove)).inRoot(isPlatformPopup()).perform(click());  // кликаем Удалить
        sleep(100);
        onView(withText(removedProject)).check(doesNotExist());                                 // проверяем что проекта в списке нет
        assertFalse(ProjectsSet.getInstance().contains(removedProject));                        // и здесь - тоже
    }

    // контроль ссылок в тегах при манипуляции с фильтром
    @Test
    public void removeProjects() {
        StringContainer p1 = createTag(mProjectsSet, "removeProjects A");
        StringContainer p2 = createTag(mProjectsSet, "removeProjects B");
        assertThat(p1.getRefCounter(), is(1));      // после создания в ProjectsSet на тег ссылаетсмя только ProjectsSet
        assertThat(p2.getRefCounter(), is(1));

        selectSingleTag(PROJECT, p1.toString());    // выбираем по очереди каждый тег в фильтре
        selectSingleTag(PROJECT, p2.toString());
        clearFilters();
        FilterManager.project().setSelector(null);  // удалем ссылку на тег в селекторе фильтра

        assertThat(p1.getRefCounter(), is(2));      // теперь на тег ссылаются ProjectsSet и HistoryItem
        assertThat(p2.getRefCounter(), is(2));

        openFilterDialog(PROJECT);
        removeTag(p1.toString());                   // удаляем тег из фильтра = из ProjectsSet
        removeTag(p2.toString());

        assertThat(p1.getRefCounter(), is(1));      // теперь на тег ссылается только HistoryItem
        assertThat(p2.getRefCounter(), is(1));

        clickOn(R.id.button_left);
        clickOn(R.id.history_button);               // открываем History, которая очищает неиспользуемые теги

        assertThat(p1.getRefCounter(), is(0));      // нет ссылок
        assertThat(p2.getRefCounter(), is(0));
    }

    //Имя проекта должно изменяться
    @Test
    public void editProject()  {
        StringContainer project = createTag(mProjectsSet, "editProject");
        String newName = project.toString() + "1";

        openFilterDialog(PROJECT);                                      // открываем фильтры проекты

        onRecyclerViewInCollapsedAppBar(allOf(withId(R.id.recycler_view), hasFocus()), allOf(withId(R.id.popup_menu), hasSibling(withChild(withText(project.toString())))))
                .perform(click());                                      // кликаем по меню заданного проекту

        onView(withText(R.string.menu_rename)).inRoot(isPlatformPopup())
                .perform(click());                                      // выбираем Редактировать

        onView(allOf(isAssignableFrom(EditText.class), isDisplayed()))  //в редактируемом виджете вводим новое имя
                .perform(clearText(), typeText(newName), pressImeActionButton());

        onView(allOf(withText(newName), not(isAssignableFrom(EditText.class))))
                .check(matches(isDisplayed()));                         // проверяем что оно отобразилось в нередактируемом

        mProjectsSet.remove(project);
    }

    /* ******** Тесты пустого проекта *************/

    //  0-й элемент должен быть = "[ без проекта ]"  и не иметь меню
    @Test
    public void checkEmptyTag() {
        openFilterDialog(PROJECT);
        onView(allOf(withId(R.id.recycler_view), isDisplayed()))
                .check(matches(hasViewInPosition(allOf(withId(R.id.textView), withText(R.string.no_project)), 0))); // проверяем что 0-й элемент = [Без проекта]

        onView(allOf(withId(R.id.recycler_view), isDisplayed()))
                .check(matches(hasViewInPosition(allOf(withId(R.id.popup_menu), not(isDisplayed())), 0)));          // проверяем что у 0-го элемента меню отсутствует
    }

    // задача не имеющая проекта должна отфильтровываться при выборе фильтра [Без проекта]
    @Test
    public void selectEmpty()  {
        Task task = createTask("selectEmpty");
        clickOn(SAVE);

        openFilterDialog(PROJECT);

        onView(allOf(withId(R.id.textView),                                         // кликаем по тексту в 0-й позиции RecyclerView
                withPositionIn(allOf(withId(R.id.recycler_view), isDisplayed()), 0)))
                .perform(click());

        onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_title), withText(task.toString())))
                .check(matches(isDisplayed()));                                     // проверяем что задача отобразилась в списке задач

        TaskList.remove(task);
    }

    // нажатие FAB завершает редактирование тега
    @Test
    public void completeEditByFAB() {
        StringContainer project = new StringContainer(uniqueStr(mProjectsSet.getTags(), "completeEditByFAB"));  // получаем уникальное имя

        openFilterDialog(PROJECT);
        clickOn(R.id.floatingActionButton);
        onView(allOf(isAssignableFrom(EditText.class), isDisplayed())).perform(typeText(project.toString()));   // вводим новый тег
        clickOn(R.id.floatingActionButton);                                                                     // нажимаем FAB
        onTag(project.toString()).check(matches(isDisplayed()));                                                // проверяем что название добавилось
        onView(allOf(isAssignableFrom(EditText.class), isDisplayed())).check(doesNotExist());                   // проверяем что нет ни нового, ни старого тега в режиме редактирования
        mProjectsSet.remove(project);
    }

    // после удаления задача не учитывается в фильтре
    @Test
    public void countAfterDelete() {
        StringContainer project = createTag(mProjectsSet, "countAfterDelete");
        openFilterDialog(PROJECT);
        onTag(project.toString()).perform(click());     // создаем проект

        Task task = createTask("countAfterDelete");     // создаем задачу с этим проектом
        clickOn(SAVE);
        TestUtils.List.clickTaskListMenu(task, REMOVE); // и удаляем ее

        openFilterDialog(PROJECT);                      // открываем диалог проекты

        onRecyclerViewInCollapsedAppBar(allOf(withId(R.id.recycler_view), isDisplayed()),
                allOf(withId(R.id.num_tasks), hasSibling(withChild(withText(project.toString())))))
                .check(matches(withText("0")));          // убеждаемся что 0 задач имеют данный проект

        mProjectsSet.remove(project);
        Espresso.pressBack();
        clearFilters();                                 // сбрасываем фильтры
    }

    // сортировка должна быть в алфавитном порядке, сначала выводятся теги, имеющие задачи
    @Test
    public void sortingTest() {
        assertThat("the test requires no projects", ProjectsSet.getInstance().getTags(), Matchers.hasSize(1));

        StringContainer p4 = new StringContainer("sortingTest D");
        StringContainer p2 = createTag(ProjectsSet.getInstance(), "sortingTest B");
        StringContainer p1 = createTag(ProjectsSet.getInstance(), "sortingTest A");
        StringContainer p3 = new StringContainer("sortingTest C");


        Task t1 = createTask("sortingTest t1");
        setProject(p4.toString());
        clickOn(SAVE);

        Task t2 = createTask("sortingTest t2");
        setProject(p3.toString());
        clickOn(SAVE);

        openFilterDialog(PROJECT);
        onView(allOf(withId(R.id.recycler_view), isDisplayed()))
                .check(matches(hasViewInPosition(allOf(withId(R.id.textView), withText(R.string.no_project)), 0)));

        onView(allOf(withId(R.id.recycler_view), isDisplayed()))
                .check(matches(hasViewInPosition(allOf(withId(R.id.textView), withText(p3.toString())), 1)));

        onView(allOf(withId(R.id.recycler_view), isDisplayed()))
                .check(matches(hasViewInPosition(allOf(withId(R.id.textView), withText(p4.toString())), 2)));

        onView(allOf(withId(R.id.recycler_view), isDisplayed()))
                .check(matches(hasViewInPosition(allOf(withId(R.id.textView), withText(p1.toString())), 3)));

        onView(allOf(withId(R.id.recycler_view), isDisplayed()))
                .check(matches(hasViewInPosition(allOf(withId(R.id.textView), withText(p2.toString())), 4)));

        TaskList.remove(t1);
        TaskList.remove(t2);

        ProjectsSet.getInstance().remove(p1);
        ProjectsSet.getInstance().remove(p2);
        ProjectsSet.getInstance().remove(p3);
        ProjectsSet.getInstance().remove(p4);
    }
}