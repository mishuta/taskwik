package com.mishuto.todo.view_model.list;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.TestUtils.Card.COMPLETE_BTN;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.List.REMOVE;
import static com.mishuto.todo.view_model.TestUtils.List.SHOW_COMPLETED;
import static com.mishuto.todo.view_model.TestUtils.List.clickListOptionsMenu;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;

/**
 * Created by Michael Shishkin on 28.12.2018.
 */
public class EmptyMsgControllerTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    //если список задач пустой, должны отображаться сообщения об этом
    @Test
    public void emptyMessage() {
        assertEquals("Test requires no tasks in list", 0, TaskList.getTasks().size());     // пререквизит теста: пустой список

        onView(withId(R.id.empty_list_msg)).check(matches(withText(R.string.no_tasks)));    // проверяем наличие сообщения, об отсутствии задач
        onView(withId(R.id.empty_list_image)).check(matches(isDisplayed()));
        FilterManager.incomplete().setFilter(false);
        Task t = createTask("emptyMessage");                                                // создаем завершенную задачу
        clickOn(COMPLETE_BTN);
        clickOn(SAVE);

        onView(withId(R.id.empty_list_image)).check(matches(not(isDisplayed())));
        onView(withId(R.id.empty_list_msg)).check(matches(withText("")));                   // задача есть в списке: сообщение пустое

        clickListOptionsMenu(SHOW_COMPLETED);                                               // фильтруем завершенные
        onView(withId(R.id.empty_list_image)).check(matches(isDisplayed()));
        onView(withId(R.id.empty_list_msg)).check(matches(withText(R.string.all_tasks_filtered)));  // сообщение что все задачи отфильтрованы

        clickListOptionsMenu(SHOW_COMPLETED);

        TestUtils.List.clickTaskListMenu(t, REMOVE);                                        // удаляем задачу
        onView(withId(R.id.empty_list_image)).check(matches(isDisplayed()));
        onView(withId(R.id.empty_list_msg)).check(matches(withText(R.string.no_tasks)));    // сообщение об отсутствии задач
    }

}