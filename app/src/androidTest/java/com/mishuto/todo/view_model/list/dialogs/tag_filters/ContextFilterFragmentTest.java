package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.ContextsSet;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.isButtonChecked;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerView;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerViewInCollapsedAppBar;
import static com.mishuto.todo.view_model.EspressoHelper.withRecyclerPositionItemView;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.CONTEXT;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.DATE;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.PROJECT;
import static com.mishuto.todo.view_model.TestUtils.Filters.clearFilters;
import static com.mishuto.todo.view_model.TestUtils.Filters.onCheckContext;
import static com.mishuto.todo.view_model.TestUtils.Filters.openFilterDialog;
import static com.mishuto.todo.view_model.TestUtils.Filters.removeTag;
import static com.mishuto.todo.view_model.TestUtils.Filters.scrollToTab;
import static com.mishuto.todo.view_model.TestUtils.Filters.selectSingleTag;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static com.mishuto.todo.view_model.TestUtils.createTag;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

/**
 * Тесты фильтра по контексту
 * Created by Michael Shishkin on 31.07.2017.
 */
public class ContextFilterFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    //выбранные контексты должны оставаться выбранными после переоткрытия диалога (а невыбранные - не выбранными)
    @Test
    public void selectContexts() {
        ContextsSet contextsSet = ContextsSet.getInstance();

        // создаем три новых контекста
        StringContainer c1 = TestUtils.createTag(contextsSet, "selectContextsA");
        StringContainer c2 = TestUtils.createTag(contextsSet, "selectContextsB");
        StringContainer c3 = TestUtils.createTag(contextsSet, "selectContextsC");

        openFilterDialog(CONTEXT);
        onCheckContext(c1.toString()).perform(click());                         // кликаем по чекбоксу первого контекста
        onCheckContext(c2.toString()).perform(click());                         // кликаем по чекбоксу второго контекста
        clickOn(R.id.button_right);                                             // закрываем диалог, подтверждая выбор двух контекстов

        openFilterDialog(CONTEXT);
        onCheckContext(c1.toString()).check(matches(isButtonChecked()));        //первый выбран
        onCheckContext(c2.toString()).check(matches(isButtonChecked()));        //второй выбран
        onCheckContext(c3.toString()).check(matches(not(isButtonChecked())));   //третий не выбран

        contextsSet.remove(c1);
        contextsSet.remove(c2);
        contextsSet.remove(c3);

        Espresso.pressBack();
        clearFilters();
    }

    // задача без контекста должна отфильтроваться
    @Test
    public void selectEmptyContext() {
        Task task = createTask("selectEmptyContext");
        clickOn(SAVE);

        openFilterDialog(CONTEXT);
        onView(withRecyclerPositionItemView(R.id.recycler_view, R.id.check, 0)).perform(click());               // выбираем значение [без контекста]
        clickOn(R.id.button_right);
        onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_title), withText(task.toString())))
                .check(matches(isDisplayed()));                                                                 // созданая задача в списке

        TaskList.remove(task);
    }

    // тулбар контекста трансформируется в зависимости от того выбран хотя бы один контекст или нет
    @Test
    public void toolbarTransformation() {
        clearFilters();

        openFilterDialog(CONTEXT);                                                              // открываем диалог контекстов
        onView(withId(R.id.app_bar_title)).check(matches(withText(R.string.filtersDlg_title))); // заголовок = Фильтры
        onView(withId(R.id.button_right)).check(matches(not(isDisplayed())));                   // правой кнопки нет
        clickOn(R.id.button_left);                                                              // клик по левой кнопке
        onView(withId(R.id.toolbar_filter_btn)).check(matches(isDisplayed()));                  // возвращает обратно в список

        openFilterDialog(CONTEXT);                                                              // переоткрываем диалог
        onCheckContext(ResourcesFacade.getString(R.string.no_context)).perform(click());        // кликаем по 0-му контексту
        onView(withId(R.id.app_bar_title)).check(matches(withText("1")));                       // заголовок = 1
        onView(withId(R.id.button_right)).check(matches(isDisplayed()));                        // правая кнопка появилась
        clickOn(R.id.button_left);                                                              // клик по левой кнопке
        onCheckContext(ResourcesFacade.getString(R.string.no_context))
                .check(matches(not(isButtonChecked())));                                        // чек сброшен
        onView(withId(R.id.app_bar_title)).check(matches(withText(R.string.filtersDlg_title))); // заголовок = Фильтры
    }

    // при скроллинге между фильтрами, тулбар контекста не сбрасывается
    @Test
    public void toolbarTransformationAfterScroll() {
        clearFilters();
        openFilterDialog(CONTEXT);
        onCheckContext(ResourcesFacade.getString(R.string.no_context)).perform(click());    // кликаем по 0-му контексту
        scrollToTab(PROJECT);                                                               // скролл в PROJECT
        scrollToTab(CONTEXT);                                                               // и обратно
        onView(withId(R.id.button_right)).check(matches(isDisplayed()));                    // правая кнопка на месте

        scrollToTab(DATE);                                                                  // тоже самое для DATE
        scrollToTab(CONTEXT);
        onView(withId(R.id.button_right)).check(matches(isDisplayed()));

    }

    // тулбар обновляется после удаления тега в списке
    @Test
    public void toolbarUpdateAfterDelete() {
        ContextsSet contextsSet = ContextsSet.getInstance();
        StringContainer c = TestUtils.createTag(contextsSet, "toolbarTransformationAfterDelete");
        openFilterDialog(CONTEXT);
        onCheckContext(c.toString()).perform(click());

        onRecyclerViewInCollapsedAppBar(allOf(withId(R.id.recycler_view), isDisplayed()),
                allOf(withId(R.id.popup_menu), hasSibling(withChild(withText(c.toString())))))
                .perform(click());                  // находим нужный проект и кликаем на кнопку меню

        onView(withText(R.string.taskMenu_remove)).inRoot(isPlatformPopup()).perform(click());  // кликаем Удалить

        onView(withId(R.id.app_bar_title)).check(matches(withText(R.string.filtersDlg_title))); // заголовок = Фильтры

    }

    // контроль ссылок при манипуляции с тегами в фильтре
    @Test
    public void removeContexts() {
        StringContainer c1 = createTag(ContextsSet.getInstance(), "removeContexts A");
        StringContainer c2 = createTag(ContextsSet.getInstance(), "removeContexts B");
        assertThat(c1.getRefCounter(), is(1));      // после создания и сохранения в ContextSet, у тега есть 1 ссылка
        assertThat(c2.getRefCounter(), is(1));

        selectSingleTag(CONTEXT, c1.toString());    // выбираем тег c1 в фильтре
        assertThat(c1.getRefCounter(), is(3));      // Ссылки: ContextSet, ContextFilter.mSelector, HistoryItem. Хоть HistoryItem хранит клон селектора, но не клон тега.

        selectSingleTag(CONTEXT, c2.toString());    // выбираем тег c2 в фильтре
        assertThat(c2.getRefCounter(), is(3));      // Ссылки: ContextSet, ContextFilter.mSelector, HistoryItem
        assertThat(c1.getRefCounter(), is(2));      // Ссылки: ContextSet, HistoryItem
        clearFilters();
        assertThat(c2.getRefCounter(), is(2));      // Ссылки: ContextSet, HistoryItem

        openFilterDialog(CONTEXT);                  // удаляем теги из фильтра = удаляем из ContextSet
        removeTag(c1.toString());
        removeTag(c2.toString());

        assertThat(c1.getRefCounter(), is(1));      // теперь только осталась ссылка в HistoryItem
        assertThat(c2.getRefCounter(), is(1));

        clickOn(R.id.button_left);
        clickOn(R.id.history_button);               // открываем History, в этот момент все неиспользуемы теги очищаются

        assertThat(c1.getRefCounter(), is(0));      // ссылок больше нет
        assertThat(c2.getRefCounter(), is(0));
    }

    // с установленным в фильтре контекстом, новая задача создается с этим же контекстом
    @Test
    public void createTaskWithContext() {
        clearFilters();
        StringContainer c1 = TestUtils.createTag(ContextsSet.getInstance(), "createTaskWithContextA");
        StringContainer c2 = TestUtils.createTag(ContextsSet.getInstance(), "createTaskWithContextB");

        openFilterDialog(TestUtils.Filters.FilterTab.CONTEXT);
        onCheckContext(c1.toString()).perform(click());     // кликаем по чекбоксу первого контекста
        onCheckContext(c2.toString()).perform(click());     // кликаем по чекбоксу второго контекста

        clickOn(R.id.button_right);                         // закрываем диалог, подтверждая выбор двух контекстов

        Task task = createTask("createTaskWithContext");
        onView(withId(R.id.taskDetails_context_desc)).check(matches(withText(startsWith("2 "))));
        clickOn(SAVE);

        clearFilters();
        TaskList.remove(task);
        ContextsSet.getInstance().remove(c1);
        ContextsSet.getInstance().remove(c2);
    }

    // если установлен контекст [без контекста], он не устанавливается в задаче
    @Test
    public void createTaskWithNoContext() {
        clearFilters();
        openFilterDialog(TestUtils.Filters.FilterTab.CONTEXT);
        onView(withRecyclerPositionItemView(R.id.recycler_view, R.id.check, 0)).perform(click()); // выбираем значение [без контекста]
        clickOn(R.id.button_right);

        Task task = createTask("createTaskWithNoContext");
        onView(withId(R.id.taskDetails_context_desc)).check(matches(withText(R.string.NOT_SET)));
        clickOn(SAVE);

        assertThat(task.getContext(), hasSize(0));

        clearFilters();
        TaskList.remove(task);
    }
}

