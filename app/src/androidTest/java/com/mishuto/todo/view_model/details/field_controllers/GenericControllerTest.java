package com.mishuto.todo.view_model.details.field_controllers;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.view_model.TestUtils.Card.cancelTask;
import static com.mishuto.todo.view_model.TestUtils.Card.setTaskDate;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.hamcrest.Matchers.not;

/**
 * Общие тесты для контроллеров
 * Created by Michael Shishkin on 10.12.2017.
 */
public class GenericControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);


    // При создании задачи кнопка clear должна быть disabled, а значение = Нет
    @Test
    public void stateOnStart() throws Exception {
        createTask("stateOnStart");
        onView(withId(R.id.taskDetails_clearTerm)).check(matches(not(isEnabled())));
        onView(withId(R.id.taskDetails_TextView_Term)).check(matches(withText(R.string.NOT_SET)));
        cancelTask();
    }


    // При изменении даты кнопка clear должна быть enabled, а значение != Нет
    @Test
    public void stateOnChange() throws Exception {
        createTask("stateOnChange");
        setTaskDate(TCalendar.now());
        onView(withId(R.id.taskDetails_clearTerm)).check(matches(isEnabled()));
        onView(withId(R.id.taskDetails_TextView_Term)).check(matches(not(withText(R.string.NOT_SET))));
        cancelTask();
    }

    // При сбросе даты кнопка clear должна быть disabled, а значение = Нет
    @Test
    public void stateOnClear() throws Exception {
        createTask("stateOnClear");
        setTaskDate(TCalendar.now());
        onView(withId(R.id.taskDetails_clearTerm)).perform(click()).check(matches(not(isEnabled())));
        onView(withId(R.id.taskDetails_TextView_Term)).check(matches(withText(R.string.NOT_SET)));
        cancelTask();
    }
}