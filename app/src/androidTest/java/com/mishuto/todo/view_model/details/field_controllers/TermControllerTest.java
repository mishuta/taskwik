package com.mishuto.todo.view_model.details.field_controllers;

import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.view_model.EspressoHelper;
import com.mishuto.todo.view_model.TestUtils;
import com.mishuto.todo.view_model.list.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;

import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.junit.Assert.assertTrue;

/**
 * Проверка изменения даты задачи
 * Created by Michael Shishkin on 30.04.2017.
 */
public class TermControllerTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private final static TCalendar TEST_DATE = new TCalendar(2016, Calendar.AUGUST, 4);

    @Test
    public void changeTerm()  {
        Task task =  createTask("changeTerm");
        TestUtils.Card.setTaskDate(TEST_DATE);
        EspressoHelper.clickOn(SAVE);

        SystemClock.sleep(200);
        task.reload();
        assertTrue(task.getTaskTime().getCalendar().intervalFrom(Calendar.DATE, TEST_DATE) == 0);

        TaskList.remove(task);
    }
}