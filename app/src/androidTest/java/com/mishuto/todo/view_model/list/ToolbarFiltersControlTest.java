package com.mishuto.todo.view_model.list;

import android.support.test.rule.ActivityTestRule;

import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.ContextsSet;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.model.tags_collections.ProjectsSet;
import com.mishuto.todo.model.task_filters.Exclusiveness;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.EspressoHelper;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Rule;
import org.junit.Test;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.mishuto.todo.common.ResourceConstants.Filters.EMPTY_CONTEXT_NAME;
import static com.mishuto.todo.common.TextUtils.uniqueStr;
import static com.mishuto.todo.view_model.EspressoHelper.clickOn;
import static com.mishuto.todo.view_model.EspressoHelper.hasItemsCount;
import static com.mishuto.todo.view_model.EspressoHelper.isButtonChecked;
import static com.mishuto.todo.view_model.EspressoHelper.onRecyclerView;
import static com.mishuto.todo.view_model.EspressoHelper.onTextInputLayout;
import static com.mishuto.todo.view_model.TestUtils.Card.SAVE;
import static com.mishuto.todo.view_model.TestUtils.Card.setContext;
import static com.mishuto.todo.view_model.TestUtils.Card.setExecutor;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.CONTEXT;
import static com.mishuto.todo.view_model.TestUtils.Filters.FilterTab.PROJECT;
import static com.mishuto.todo.view_model.TestUtils.Filters.checkToolbarTitleView;
import static com.mishuto.todo.view_model.TestUtils.Filters.clearFilters;
import static com.mishuto.todo.view_model.TestUtils.Filters.onCheckContext;
import static com.mishuto.todo.view_model.TestUtils.Filters.openFilterDialog;
import static com.mishuto.todo.view_model.TestUtils.Filters.selectSingleTag;
import static com.mishuto.todo.view_model.TestUtils.List.assertTaskExists;
import static com.mishuto.todo.view_model.TestUtils.List.assertTaskNotExists;
import static com.mishuto.todo.view_model.TestUtils.List.createTask;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertNull;

/**
 * Тест функционала фильтрации/поиска в тулбаре списка задач
 * Created by Michael Shishkin on 12.08.2017.
 */
public class ToolbarFiltersControlTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    //выбранный в фильтре проект должен отображаться в app bar, кнопка фильтра нажата, кнопка поиска - отжата
    @Test
    public void openProjectFilterDialog() {
        StringContainer project = new StringContainer(uniqueStr(ProjectsSet.getInstance().getTags(), "openProjectFilterDialog"));
        ProjectsSet.getInstance().add(project);

        selectSingleTag(PROJECT, project.toString());

        checkToolbarTitleView(project.toString()); // проверяем что проект отобразился в app bar
        onView(withId(R.id.toolbar_filter_btn)).check(matches(isButtonChecked()));
        onView(withId(R.id.toolbar_search_btn)).check(matches(not(isButtonChecked())));

        clickOn(R.id.toolbar_filter_btn);               // сбрасываем фильтр
        ProjectsSet.getInstance().remove(project);
    }

    // фильтр должен сохраняться в app bar после перехода в карточку и обратно
    @Test
    public void checkFilterAfterReturnToFragment() {
        StringContainer project = new StringContainer(uniqueStr(ProjectsSet.getInstance().getTags(), "checkFilterAfterReturnToFragment"));
        ProjectsSet.getInstance().add(project);

        Task task = createTask("checkFilterAfterReturnToFragment");
        clickOn(SAVE);
        task.setProject(project);
        selectSingleTag(PROJECT, project.toString());                                                                                   //фильтруем задачу

        onRecyclerView(R.id.recycler_view, allOf(withId(R.id.taskListItem_title), withText(task.toString()))).perform(click());         //входим в карточку задачи
        clickOn(SAVE);                                                                                                                  //возвращаемся

        onView(allOf(isDescendantOfA(withId(R.id.toolbar_filter_value)), isDisplayed())).check(matches(withText(project.toString())));  // проверяем что проект отобразился в app bar

        clickOn(R.id.toolbar_filter_btn);                                                                                               // сбрасываем фильтр
        TaskList.remove(task);
        ProjectsSet.getInstance().remove(project);
    }

    // нажатие на кнопку фильтра должно сбрасывать фильтр списка задач
    // методика проверки: считаем количество задач, устанавливаем фильтр, сбрасывем фильтр - количество задач не меняется
    @Test
    public void resetFilter()  {
        clearFilters();

        StringContainer executor = TestUtils.createTag(Executors.getInstance(), "resetFilter");

        Task task1 = createTask("resetFilter - filtered");
        setExecutor(executor.toString());
        clickOn(SAVE);

        Task task2 = createTask("resetFilter - no filtered");
        clickOn(SAVE);

        int taskCount = FilterManager.computeFilteredTaskList().size();                   // вычисляем количество задач до фильтрации
        selectSingleTag(TestUtils.Filters.FilterTab.EXECUTOR, executor.toString());     // устанавливаем фильтр
        clickOn(R.id.toolbar_filter_btn);                                               // сбрасываем фильтр

        onView(withId(R.id.recycler_view)).check(matches(hasItemsCount(taskCount)));    //количество задач не изменилось

        TaskList.remove(task1);
        TaskList.remove(task2);
        Executors.getInstance().remove(executor);
    }


    /*
    Поиск в тулбаре должен фильтровать задачи.
    методика проверки: создется две задачи, фильтруется имя одной, и проверяется что есть ровно одна задача в списке.
    Больше поисковых запросов проверяется а PrevTaskControllerTest
     */
    @Test
    public void search() {
        Task t1 = createTask("search - filtered");
        clickOn(SAVE);
        Task t2 = createTask("search - no filtered");
        clickOn(SAVE);

        TestUtils.Filters.doSearch(t1.toString());
        onView(withId(R.id.recycler_view)).check(matches(EspressoHelper.hasItemsCount(1)));

        TaskList.remove(t1);
        TaskList.remove(t2);
    }

    // проверка корректности взаимных статусов кнопок тулбара
    @Test
    public void checkButtons() {
        TSystem.debug(this, "1");
        clickOn(R.id.toolbar_search_btn);                                                   // нажать search
        onTextInputLayout(R.id.toolbar_filter_value).check(matches(isDisplayed()));         // режим редактирования
        onView(withId(R.id.toolbar_search_btn)).check(matches(isButtonChecked()));          // search нажата
        onView(withId(R.id.toolbar_filter_btn)).check(matches(not(isButtonChecked())));     // filter отжата

        TSystem.debug(this, "2");
        clickOn(R.id.toolbar_search_btn);                                                   // еще раз нажать search
        checkToolbarTitleView(null);                                                        // режим view
        onView(withId(R.id.toolbar_search_btn)).check(matches(not(isButtonChecked())));     // search отжата
        onView(withId(R.id.toolbar_filter_btn)).check(matches(not(isButtonChecked())));     // filter отжата
        checkToolbarTitleView(null);                                                        // отображается название

        TSystem.debug(this, "3");
        clickOn(R.id.toolbar_search_btn);                                                   // нажать search
        selectSingleTag(CONTEXT, ResourcesFacade.getString(R.string.no_context));           // нажать filter и выбрать конеткст
        onView(withId(R.id.toolbar_search_btn)).check(matches(not(isButtonChecked())));     // search отжата
        onView(withId(R.id.toolbar_filter_btn)).check(matches(isButtonChecked()));          // filter нажата

        TSystem.debug(this, "4");
        clickOn(R.id.toolbar_filter_btn);                                                   // нажать фильтр
        onView(withId(R.id.toolbar_search_btn)).check(matches(not(isButtonChecked())));     // search отжата
        onView(withId(R.id.toolbar_filter_btn)).check(matches(not(isButtonChecked())));     // filter отжата
        checkToolbarTitleView(null);                                                        // отображается название
    }

    //выключение фильтра эквивалентно его сбросу
    @Test
    public void noFilter() {
        clearFilters();

        Task task = createTask("noFilter");
        clickOn(SAVE);

        openFilterDialog(CONTEXT);
        onCheckContext(EMPTY_CONTEXT_NAME).perform(click());
        clickOn(R.id.button_right);

        openFilterDialog(CONTEXT);
        onCheckContext(EMPTY_CONTEXT_NAME).perform(click());
        clickOn(R.id.button_left);

        checkToolbarTitleView(null); // отображается название
        assertNull(Exclusiveness.getExclusiveFilter());

        TaskList.remove(task);
    }

    // при нажатии back в диалоге фильтров, кнопка фильтра в корректном состоянии
    @Test
    public void backDialog() {
        clearFilters();                                                                 // сбросить фильтры
        openFilterDialog(CONTEXT);                                                      // нажать кнопку фильтра, открыв диалог
        pressBack();                                                                    // отменить
        onView(withId(R.id.toolbar_filter_btn)).check(matches(not(isButtonChecked()))); // кнопка фильтра отжата
    }

    // При смене фильтра контекста - меняется список задач и заголовок списка
    @Test
    public void changeContexts() {
        String c1 = "changeContexts A";
        String c2 = "changeContexts B";
        clearFilters();

        // Создаем задачу t1 с контекстом c1 и задачу t2 с контекстом c2
        Task t1 = createTask(c1);
        setContext(c1);
        sleep(300);
        clickOn(SAVE);

        Task t2 = createTask(c2);
        setContext(c2);
        sleep(300);
        clickOn(SAVE);

        selectSingleTag(CONTEXT, c1);           // выбираем контекст c1

        checkToolbarTitleView(c1);              // отображается c1
        sleep(200);
        assertTaskExists(t1.toString());        // t1 отфильтровалась
        assertTaskNotExists(t2.toString());     // t2 - нет

        selectSingleTag(CONTEXT, c2);           // меняем контекст

        checkToolbarTitleView(c2);              // отображается c2
        sleep(200);
        assertTaskExists(t2.toString());        // отфильтровывается t2
        assertTaskNotExists(t1.toString());

        clearFilters();

        TaskList.remove(t1);
        TaskList.remove(t2);

        ContextsSet.getInstance().remove(new StringContainer(c1));
        ContextsSet.getInstance().remove(new StringContainer(c2));
    }
}
