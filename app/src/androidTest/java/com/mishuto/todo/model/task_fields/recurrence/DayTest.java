package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.Time;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.task_fields.TaskTime;

import org.junit.Test;

import java.util.Calendar;

import static com.mishuto.todo.common.ResourceConstants.Recurrence.DAILY_FACTOR_VALUES;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Тест Day.getNextEvent() для разных параметров
 * Created by Michael Shishkin on 28.10.2017.
 */
public class DayTest {

    //10:00 + 02:00 = 12:00
    @Test
    public void dayHourly() throws Exception {
        Settings.getWorkTime().isOn.set(false);
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2017,9,31);
        taskTime.setTimePart(10,0);

        Day day = new Day(taskTime);
        day.setDaySelected(false);
        day.setHourlyFactor(Day.HourlyFactor.SECOND_HOUR);

        TCalendar pattern = new TCalendar(2017, 9, 31, 12, 0);
        assertTrue(pattern.compareTo(day.getNextEvent()) == 0);
    }


    //22:00 + 08:00 = 6:00 след дня
    @Test
    public void dayHourly2() throws Exception {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2017,9,31);
        taskTime.setTimePart(22,0);
        Settings.getWorkTime().isOn.set(false);

        Day day = new Day(taskTime);
        day.setDaySelected(false);
        day.setHourlyFactor(Day.HourlyFactor.EIGHTH_HOUR);

        TCalendar pattern = new TCalendar(2017, 10, 1, 6, 0);
        assertTrue(pattern.compareTo(day.getNextEvent()) == 0);
    }

    //только рабочее время: пт 17:00 + 02:00 = пн 09:00
    @Test
    public void dayHourly3() throws Exception {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2017,9,27);
        taskTime.setTimePart(17,0);
        Settings.getWorkTime().isOn.set(true);
        Settings.getWorkTime().onlyWorkDays.set(true);
        Settings.getWorkTime().setBeginningWork(new Time(9,0));
        Settings.getWorkTime().setEndWork(new Time(18,0));

        Day day = new Day(taskTime);
        day.setDaySelected(false);
        day.setHourlyFactor(Day.HourlyFactor.SECOND_HOUR);

        TCalendar pattern = new TCalendar(2017, 9, 30, 9, 0);
        assertTrue(pattern.compareTo(day.getNextEvent()) == 0);
    }

    //10:00 27.04.27 + кажд 3 день 16:00 = 16:00 30.04.27
    @Test
    public void daily() throws Exception {
        TaskTime taskTime = new TaskTime(new TCalendar(2027, Calendar.APRIL, 27, 10,0), true);
        Day day = new Day(taskTime);
        day.setDaySelected(true);
        day.setDailyFactor(Day.DailyFactor.THIRD_DAY);
        day.setTime(new Time(16, 0));

        TCalendar pattern = new TCalendar(2027, Calendar.APRIL, 30, 16, 0);
        assertTrue(pattern.compareTo(day.getNextEvent()) == 0);
    }

    @Test
    public void getShortValue() {
        Day day = new Day(new TaskTime(TCalendar.now(), false));
        day.setDaySelected(false);
        assertEquals(DAILY_FACTOR_VALUES[0], day.getShortValue());  // если установлены часы = ежедневно
        day.setDaySelected(true);
        assertEquals(DAILY_FACTOR_VALUES[0], day.getShortValue());  // если установлен каждый день = ежедневно
        day.setDailyFactor(Day.DailyFactor.SECOND_DAY);
        assertEquals(DAILY_FACTOR_VALUES[1], day.getShortValue());  // если установлен каждый второй день = каждый 2-й день
    }
}