package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.task_fields.TaskTime;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Тест Year.getNextEvent() для разных параметров
 * Created by Michael Shishkin on 28.10.2017.
 */
public class YearTest {

    // 01.10.27 +  10/01 каждого 4 года в 10:40 = 10.01.31 в 10:40
    @Test
    public void date1() {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2027,9,1);

        Year year = new Year(taskTime);
        year.setDayOfMonth(10);
        year.setMonth(0);
        year.setYearFactor(Year.YearlyFactor.FOURTH_YEAR);
        year.setDayOfMonthSelected(true);
        year.getTime().set(10,40);
        year.setTimeFlag(true);


        TCalendar answer = new TCalendar(2031, 0, 10, 10, 40);
        assertTrue( "got: " + year.getNextEvent().toString(), answer.compareTo(year.getNextEvent()) == 0);
    }

    // 1.10.27 + 1.01 каждого года = 1.01.28
    @Test
    public void month1() {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2027,9,1);

        Year year = new Year(taskTime);
        year.setDayOfMonth(1);
        year.setMonth(0);
        year.setYearFactor(Year.YearlyFactor.EVERY_YEAR);
        year.setDayOfMonthSelected(true);

        TCalendar answer = new TCalendar(2028, 0, 1);
        assertTrue( "got: " + year.getNextEvent().toString(), answer.compareTo(year.getNextEvent()) == 0);
    }

    // 1.10.27 + 1.10 каждого года = 1.10.28
    @Test
    public void month2() {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2027,9,1);

        Year year = new Year(taskTime);
        year.setDayOfMonth(1);
        year.setMonth(9);
        year.setYearFactor(Year.YearlyFactor.EVERY_YEAR);
        year.setDayOfMonthSelected(true);

        TCalendar answer = new TCalendar(2028, 9, 1);
        assertTrue( "got: " + year.getNextEvent().toString(), answer.compareTo(year.getNextEvent()) == 0);
    }

    // 1.10.27 + 1.10 каждого второго года = 1.10.29
    @Test
    public void month3() {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2027,9,1);

        Year year = new Year(taskTime);
        year.setDayOfMonth(1);
        year.setMonth(9);
        year.setYearFactor(Year.YearlyFactor.SECOND_YEAR);
        year.setDayOfMonthSelected(true);

        TCalendar answer = new TCalendar(2029, 9, 1);
        assertTrue( "got: " + year.getNextEvent().toString(), answer.compareTo(year.getNextEvent()) == 0);
    }
}