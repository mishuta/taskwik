package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.database.aggregators.DBTreeSet;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.tags_collections.ContextsSet;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.model.tags_collections.ProjectsSet;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static com.mishuto.todo.common.TextUtils.uniqueStr;
import static com.mishuto.todo.model.tags_collections.Tags.EMPTY_TAG;
import static com.mishuto.todo.view_model.TestUtils.addNewTask;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Тестирование фильтров задач
 * Created by Michael Shishkin on 31.07.2017.
 */
public class FiltersTest {

    @Before
    public void setUp() {
        TaskList.get().load();
    }

    //проверка IncompleteFilter
    @Test
    public void checkNotCompletedFilter() {

        Task t1 = addNewTask("checkNotCompletedFilter 1");                    // задача 1 завершена
        t1.setPerformed(true);

        Task t2 = addNewTask("checkNotCompletedFilter 2");                   // задача 2 незавершена
        t2.setPerformed(false);

        assertThat(FilterManager.incomplete().isTaskFiltered(t1), is(false));           // задача 1 не прошла фильтр
        assertThat(FilterManager.incomplete().isTaskFiltered(t2), is(true));            // задача 2 отфильтровалась

        TaskList.remove(t1);
        TaskList.remove(t2);
    }

    //проверка ContextFilter
    @Test
    public void checkContextFilter() {

        // подготовка контекстов и списков
        final ContextsSet contexts = ContextsSet.getInstance();

        StringContainer context1 = new StringContainer(uniqueStr(contexts.getTags(), "checkContextFilter"));
        contexts.add(context1);

        StringContainer context2 = new StringContainer(uniqueStr(contexts.getTags(), "checkContextFilter"));
        contexts.add(context2);

        DBTreeSet<StringContainer> c1 = new DBTreeSet<>();
        DBTreeSet<StringContainer> c2 = new DBTreeSet<>();
        c1.add(context1);
        c1.add(context2);
        c2.add(context2);

        Task t1 = addNewTask("checkContextFilter1");   // задача 1 содержит context1 и context2
        t1.setContext(c1);

        Task t2 =addNewTask("checkContextFilter2");   // задача 2 содержит только context2
        t2.setContext(c2);

        Task t3 = addNewTask("checkContextFilter3");   // задача 3 не содержит контекстов
        t3.setContext(null);

        DBTreeSet<StringContainer> selector = new DBTreeSet<>();
        selector.add(context1);        // фильтруются все задачи, содержащие context1

        ContextFilter filter = FilterManager.context();

        filter.setSelector(selector);

        assertThat(filter.isTaskFiltered(t1), is(true));   // задача 1 отфильтровалась
        assertThat(filter.isTaskFiltered(t2), is(false));  // задачи 2 и 3 не прошли фильтр
        assertThat(filter.isTaskFiltered(t3), is(false));

        TaskList.remove(t1);
        TaskList.remove(t2);
        TaskList.remove(t3);

        contexts.remove(context1);
        contexts.remove(context2);
    }

    // проверка загрузки/апдейта сохраняемых в БД фильтров
    @Test
    public void loadNotCompletedFilter() {
        IncompleteFilter filter = FilterManager.incomplete();
        boolean current = filter.isFilterSet();
        filter.flip();
        filter.mHelper.mAdapter.getProxy().reload();
        assertThat(filter.isFilterSet(), is(!current));

        filter.setFilter(current);
    }

    // проверка применимости нескольких фильтров: Исполнитель, Проект, Отложенные
    @Test
    public void applyAllFilters()  {
        Task t1 = addNewTask("applyAllFilters1");
        Task t2 = addNewTask("applyAllFilters2");

        // подготовка данных для фильтров ActualFilter, ExecutorFiltered, ProjectFilter
        StringContainer executor = new StringContainer(uniqueStr(Executors.getInstance().getTags(), "applyAllFilters"));
        StringContainer project = new StringContainer(uniqueStr(ProjectsSet.getInstance().getTags(), "applyAllFilters"));

        Executors.getInstance().add(executor);
        ProjectsSet.getInstance().add(project);

        boolean savedState = FilterManager.actual().isFilterSet();
        FilterManager.actual().setFilter(true);
        FilterManager.executor().setSelector(executor);
        FilterManager.project().setSelector(project);

        TCalendar date = new TCalendar();
        date.add(Calendar.HOUR_OF_DAY, 1);
        t1.getTaskTime().setCalendar(date, true);   // задача t1 является отложенной
        // в остальном t1 и t2 идентичны
        t1.setExecutor(executor);
        t2.setExecutor(executor);

        t1.setProject(project);
        t2.setProject(project);

        assertThat(FilterManager.applyAllFilters(t1), is(false)); // t1 не прошла фильтр, т.к. отложена
        assertThat(FilterManager.applyAllFilters(t2), is(true));  // t2 отфильтровалась

        // очистка созданных данных
        TaskList.remove(t1);
        TaskList.remove(t2);
        FilterManager.actual().setFilter(savedState);
        FilterManager.project().setFilter(false);   //несохраняемые фильтры надо очистить чтобы не было влияния на другие тесты
        FilterManager.executor().setFilter(false);
        Executors.getInstance().remove(executor);
        ProjectsSet.getInstance().remove(project);
    }

    // Проверка наличия и тождественности EMPTY_TAG
    static private Task sTask;

    //ниже два спаренных теста, выполняемые в цепочке. Они имитируют перезагрузку приложения
    @Test
    public void emptyTagTest1() {
        sTask = addNewTask("emptyTagTest1");
        assertSame(sTask.getExecutor(), EMPTY_TAG);
        assertSame(sTask.getProject(), EMPTY_TAG);
        assertTrue(ProjectsSet.getInstance().getTags().contains(EMPTY_TAG));
        assertTrue(Executors.getInstance().getTags().contains(EMPTY_TAG));
        assertTrue(ContextsSet.getInstance().getTags().contains(EMPTY_TAG));
    }

    @Test
    public void emptyTagTest2() {
        assertSame(sTask.getExecutor(), EMPTY_TAG);
        assertSame(sTask.getProject(), EMPTY_TAG);

        TaskList.remove(sTask);
    }

    //DateFilter.getItems() сортирует элементы в порядке возрастания дат
    @Test
    public void DateFilter_getItems() {
        Task t1 = addNewTask("DateFilter_getItems-1");
        t1.getTaskTime().setDate(2018, 11, 11);

        Task t2 = addNewTask("DateFilter_getItems-2");
        t2.getTaskTime().setDate(2018,5,6);

        assertThat(FilterManager.date().getItems(), contains(
                new DateFilter.DateItem(null),
                new DateFilter.DateItem(t2.getTaskTime().getCalendar()),
                new DateFilter.DateItem(t1.getTaskTime().getCalendar())
        ));

        TaskList.remove(t1);
        TaskList.remove(t2);
    }
}