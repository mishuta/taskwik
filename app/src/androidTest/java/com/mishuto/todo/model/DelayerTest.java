package com.mishuto.todo.model;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.Time;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Test;

import static java.util.Calendar.DECEMBER;
import static java.util.Calendar.JANUARY;
import static java.util.Calendar.MARCH;
import static org.junit.Assert.assertEquals;

/**
 * Проверка корректности вычисления отложенного времени
 * Created by Michael Shishkin on 19.12.2017.
 */
public class DelayerTest {

    private final Settings.WorkTime mWorkTime = Settings.getWorkTime();

    //Отложенное время без учета рабочего
    @Test
    public void checkWithoutWorkTime() throws Exception {
        mWorkTime.isOn.set(false);
        mWorkTime.onlyWorkDays.set(false);

        TCalendar start = new TCalendar(2017, DECEMBER, 29, 18, 20);            // пт. 18:20
        TaskTime[] results = {
                new TaskTime(new TCalendar(2017, DECEMBER, 29, 18, 50), true),  // 30 мин
                new TaskTime(new TCalendar(2017, DECEMBER, 29, 19, 20), true),  // 1 час
                new TaskTime(new TCalendar(2017, DECEMBER, 29, 20, 30), true),  // 2 часа: round(19:20+01:00, 0:30) = 20:30
                new TaskTime(new TCalendar(2017, DECEMBER, 29, 23, 0), true),   // 4 часа: round(20:30 + 02:00, 1:00) = 23:00
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 0, 0), false),   // сутки
                new TaskTime(new TCalendar(2017, DECEMBER, 31, 0, 0), false),   // 2 суток
                new TaskTime(new TCalendar(2018, JANUARY, 2, 0, 0), false),     // 4 суток
                new TaskTime(new TCalendar(2018, JANUARY, 5, 0, 0), false)      // неделя
        };

        checkList(start, results);
    }

    //Отложенное время с учетом рабочего
    @Test
    public void checkWithWorkTime() throws Exception {
        mWorkTime.onlyWorkDays.set(false);
        mWorkTime.setEndWork(new Time(18, 30));
        mWorkTime.setBeginningWork(new Time(14, 30));

        TCalendar start = new TCalendar(2017, DECEMBER, 29, 18, 20);            // пт. 18:20
        TaskTime[] results = {
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 14, 30), true),  // начало р.д.
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 15, 0), true),   // + 0,5 час
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 16, 0), true),   // + 1 часа
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 18, 0), true),   // + 2 часа
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 0, 0), false),   // сутки
                new TaskTime(new TCalendar(2017, DECEMBER, 31, 0, 0), false),   // 2 суток
                new TaskTime(new TCalendar(2018, JANUARY, 2, 0, 0), false),     // 4 суток
                new TaskTime(new TCalendar(2018, JANUARY, 5, 0, 0), false)      // неделя
        };

        checkList(start, results);
    }

    //Отложенное время после рабочего дня
    @Test
    public void checkAfterWorkTime() throws Exception {
        mWorkTime.onlyWorkDays.set(false);
        mWorkTime.isOn.set(true);
        mWorkTime.setEndWork(new Time(18, 30));
        mWorkTime.setBeginningWork(new Time(14, 30));

        TCalendar start = new TCalendar(2017, DECEMBER, 29, 19, 0);            // пт. 19:00
        TaskTime[] results = {
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 14, 30), true),  // начало р.д.
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 15, 0), true),   // + 0,5 час
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 16, 0), true),   // + 1 часа
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 18, 0), true),   // + 2 часа
                new TaskTime(new TCalendar(2017, DECEMBER, 30, 0, 0), false),   // сутки
                new TaskTime(new TCalendar(2017, DECEMBER, 31, 0, 0), false),   // 2 суток
                new TaskTime(new TCalendar(2018, JANUARY, 2, 0, 0), false),     // 4 суток
                new TaskTime(new TCalendar(2018, JANUARY, 5, 0, 0), false)      // неделя
        };

        checkList(start, results);
    }


    //Отложенное время с учетом рабочих дней
    @Test
    public void checkWithWorkDays() throws Exception {
        mWorkTime.isOn.set(true);
        mWorkTime.onlyWorkDays.set(true);
        mWorkTime.setEndWork(new Time(18, 30));
        mWorkTime.setBeginningWork(new Time(14, 30));

        TCalendar start = new TCalendar(2017, DECEMBER, 29, 18, 20);            // пт. 18:20
        TaskTime[] results = {
                new TaskTime(new TCalendar(2018, JANUARY, 1, 14, 30), true),    // начало р.д.
                new TaskTime(new TCalendar(2018, JANUARY, 1, 15, 0), true),     // + 0,5 час
                new TaskTime(new TCalendar(2018, JANUARY, 1, 16, 0), true),     // + 1 часа
                new TaskTime(new TCalendar(2018, JANUARY, 1, 18, 0), true),     // + 2 часа
                new TaskTime(new TCalendar(2018, JANUARY, 1, 0, 0), false),     // сутки
                new TaskTime(new TCalendar(2018, JANUARY, 2, 0, 0), false),     // 2 суток
                new TaskTime(new TCalendar(2018, JANUARY, 4, 0, 0), false),     // 4 суток
                new TaskTime(new TCalendar(2018, JANUARY, 5, 0, 0), false)      // неделя
        };

        checkList(start, results);
    }

    // 6-я кнопка (4-й день) через выходные
    @Test
    public void checkWithWorkDays2() throws Exception {
        mWorkTime.isOn.set(true);
        mWorkTime.onlyWorkDays.set(true);
        mWorkTime.setEndWork(new Time(18, 0));
        mWorkTime.setBeginningWork(new Time(9, 0));

        TCalendar start = new TCalendar(2019, MARCH, 21, 11, 0);
        TaskTime probe = new TaskTime(new TCalendar(2019, MARCH, 27), false);
        TaskTime result = getDelayer(start).getTargetTimes().get(6);

        assertEquals("expected <" + probe + "> but was <" + result + ">" , 0, probe.compareTo(result));
    }

    // валидация
    private void checkList(TCalendar start, TaskTime[] results) throws Exception {
        int i = 0;
        for(TaskTime taskTime : getDelayer(start).getTargetTimes())
            assertEquals("expected <" + results[i] + "> but was <" + taskTime + ">" , 0, taskTime.compareTo(results[i++]));
    }

    private Delayer getDelayer(TCalendar start) throws Exception {
        Delayer delayer = new Delayer();
        TestUtils.Reflection.setField(Delayer.class, delayer, "mStartTime", start);    // подменяем стартовое время с текущего на предустановленное
        TestUtils.Reflection.execute(Delayer.class, "initTargetTimes", delayer);       // вычисляем проверяемый список времен
        return delayer;
    }

}