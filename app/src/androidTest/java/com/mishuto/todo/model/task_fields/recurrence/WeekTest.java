package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.task_fields.TaskTime;

import org.junit.Test;

import static com.mishuto.todo.common.ResourceConstants.Recurrence.WEEKLY_FACTOR_VALUES;
import static com.mishuto.todo.common.TCalendar.getOrdinalOfDayOfWeek;
import static com.mishuto.todo.common.TCalendar.now;
import static com.mishuto.todo.common.TextUtils.capitalize;
import static com.mishuto.todo.model.task_fields.recurrence.Week.getShortWekDayName;
import static java.util.Calendar.FEBRUARY;
import static java.util.Calendar.SATURDAY;
import static java.util.Calendar.THURSDAY;
import static java.util.Calendar.TUESDAY;
import static java.util.Calendar.WEDNESDAY;
import static org.junit.Assert.assertEquals;

/**
 * Тест Week.getNextEvent()
 * Created by Michael Shishkin on 11.02.2018.
 */
public class WeekTest {

    // установка на сегодняшний день недели = срабатывание через неделю
    private final TaskTime START = new TaskTime(new TCalendar(2018, FEBRUARY, 7), false);    // среда;

    @Test
    public void today() throws Exception {
        Week week = new Week(START);
        week.getWeekDays()[ getOrdinalOfDayOfWeek(WEDNESDAY)].setChecked(true);

        TCalendar pattern = new TCalendar(2018, FEBRUARY, 14);

        assertEquals(0, week.getNextEvent().compareTo(pattern));
    }

    //установка вчера и завтра = срабатывание завтра
    @Test
    public void multiChoice() throws Exception {
        Week week = new Week(START);
        week.getWeekDays()[ getOrdinalOfDayOfWeek(THURSDAY)].setChecked(true);
        week.getWeekDays()[ getOrdinalOfDayOfWeek(TUESDAY)].setChecked(true);

        TCalendar pattern = new TCalendar(2018, FEBRUARY, 8);

        assertEquals(0, week.getNextEvent().compareTo(pattern));
    }

    //завтра каждые 3 недели
    @Test
    public void weeklyFactor() throws Exception {
        Week week = new Week(START);
        week.setWeeklyFactor(Week.WeeklyFactor.THIRD_WEEK);
        week.getWeekDays()[ getOrdinalOfDayOfWeek(THURSDAY)].setChecked(true);

        TCalendar pattern = new TCalendar(2018, FEBRUARY, 22);

        assertEquals(0, week.getNextEvent().compareTo(pattern));
    }

    // еженедельный повтор с установкой по дефолту, без предварительно установленных атрибутов = +1 неделя
    @Test
    public void everyWeekDefault() throws Exception {
        Week week = new Week(START);
        week.setDefaultWeekDay();

        TCalendar pattern = new TCalendar(2018, FEBRUARY, 14);

        assertEquals(0, week.getNextEvent().compareTo(pattern));
    }

    //ВТ,ЧТ,СБ каждой недели отображается как "Вт, Чт, Сб"
    @Test
    public void getShortValueEvery() {
        Week week = new Week(new TaskTime(now(), false));
        week.getWeekDays()[ getOrdinalOfDayOfWeek(TUESDAY)].setChecked(true);
        week.getWeekDays()[ getOrdinalOfDayOfWeek(THURSDAY)].setChecked(true);
        week.getWeekDays()[ getOrdinalOfDayOfWeek(SATURDAY)].setChecked(true);

        String answer =
                capitalize(getShortWekDayName(TUESDAY)) + ", " +
                capitalize(getShortWekDayName(THURSDAY)) + ", " +
                capitalize(getShortWekDayName(SATURDAY));

        assertEquals(answer, week.getShortValue());
    }

    //Каждая второая неделя отображается как "каждая 2-я неделя"
    @Test
    public void getShortValueSecond() {
        Week week = new Week(new TaskTime(now(), false));
        week.setWeeklyFactor(Week.WeeklyFactor.SECOND_WEEK);
        assertEquals(WEEKLY_FACTOR_VALUES[1], week.getShortValue());
    }
}