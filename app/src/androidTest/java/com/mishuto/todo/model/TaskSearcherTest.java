package com.mishuto.todo.model;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.model.tags_collections.ProjectsSet;

import org.junit.Test;

import java.util.List;

import static com.mishuto.todo.model.TaskSearcher.MatchLevel.COMMENT_CONTENTS;
import static com.mishuto.todo.model.TaskSearcher.MatchLevel.TAGS_CONTENTS;
import static com.mishuto.todo.model.TaskSearcher.MatchLevel.TITLE_CONTENTS;
import static com.mishuto.todo.model.TaskSearcher.MatchLevel.TITLE_MATCHES;
import static com.mishuto.todo.model.TaskSearcher.doSearch;
import static com.mishuto.todo.view_model.TestUtils.addNewTask;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 * Created by Michael Shishkin on 02.12.2018.
 */
public class TaskSearcherTest {

    @Test
    public void checkResults() {
        Task t1, t2, t3, t4, t5, t6;
        ProjectsSet.getInstance().add("asdv");

        t5 = addNewTask("t5");
        t5.setProject(new StringContainer("asdv"));

        t4 = addNewTask("t4");
        t4.getComments().add("asdv");

        t1 = addNewTask("as");
        t2 = addNewTask("asd");
        t3 = addNewTask("asdv");

        List<Task> list = TaskList.getTasks();

        assertThat(doSearch(list, "asd", TITLE_MATCHES), hasSize(1));       // только одна задача может совпадать по названию с шаблоном
        assertThat(doSearch(list, "asd", TITLE_MATCHES).get(0), is(t2));    // и эта задача t2

        assertThat(doSearch(list, "as", TITLE_CONTENTS), hasSize(3));       // 3 задачи содержат в названии сигнатуру "as"
        assertThat(doSearch(list, "as", COMMENT_CONTENTS), hasSize(4));     // 4 задачи содержат в названии и комментарии сигнатуру
        assertThat(doSearch(list, "as", TAGS_CONTENTS), hasSize(5));        // 5 задач содержат в названии, комментарии и тегах сигнатуру

        assertThat(doSearch(list, "as", TAGS_CONTENTS).get(3), is(t4));     // сортировка: сначала те, что имеют сигнатуру в названии, потом в комментарии
        assertThat(doSearch(list, "as", TAGS_CONTENTS).get(4), is(t5));     // потом в теге

        assertThat(doSearch(list, " asd ", TITLE_CONTENTS), hasSize(2));    // 2 задачи имеют в названии сигнатуру "asd" без учета пробелов

        t6 = addNewTask("qq asd");
        t6.getComments().add("qq asd");

        assertThat(doSearch(TaskList.getTasks(), "as", COMMENT_CONTENTS), hasSize(5));     // количество задач в результате увеличилось на 1 задачу

        TaskList.remove(t1);
        TaskList.remove(t2);
        TaskList.remove(t3);
        TaskList.remove(t4);
        TaskList.remove(t5);
        TaskList.remove(t6);

        ProjectsSet.getInstance().remove(new StringContainer("asdv"));

    }
}