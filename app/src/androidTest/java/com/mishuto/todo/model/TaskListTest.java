package com.mishuto.todo.model;

import com.mishuto.todo.view_model.TestUtils;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * Тестирование списка задач модели
 * Created by Michael Shishkin on 23.04.2017.
 */
public class TaskListTest {

    @Before
    public void setUp() {
        TaskList.get().load();
    }

    //после создания и удаления задачи, их общее количество не меняется
    @Test
    public void removeTask() {
        int n = TestUtils.getObjectsCount(Task.class);
        Task t1 = TaskList.addNew();
        Task t2 = TaskList.addNew();

        t1.setPrev(t2);
        TaskList.remove(t2);

        assertThat(n + 1, is(TestUtils.getObjectsCount(Task.class)));   // проверяем что одна из двух задач удалены из базы
        assertThat(n + 1, is(TaskList.getTasks().size()));
        assertThat(t1.getPrev(), is(nullValue()));

        TaskList.remove(t1);
        assertThat(n, is(TestUtils.getObjectsCount(Task.class)));
    }

    // добавление пустого контекста не вызывает эксепшн при удалении
    @Test
    public void removeTaskWithEmptyContext() {
        Task task = TaskList.addNew();
        task.setContext(null);
        TaskList.remove(task);
    }
}