package com.mishuto.todo.model;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.task_fields.Order;
import com.mishuto.todo.model.task_fields.Priority;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Calendar;

import static com.mishuto.todo.common.TCalendar.now;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Тестирование методов модели задачи
 * Created by Michael Shishkin on 04.09.2017.
 */
public class TaskTest {
    @BeforeClass
    public static void setUp() {
        TaskList.get().load();
    }

    /******************* Тестирование вариантов compare ***********************/

    private Task t1, t2;
    private int count;

    @Test
    public void comparePriority() {
        createTasks();

        t1.setPriority(Priority.IMPORTANT);
        t2.setPriority(Priority.NO_IMPORTANT);

        assertTrue(t1.compareTo(t2) < 0);       // важные выше неважных

        releaseTasks();
    }

    // сравнение по времени создания
    @Test
    public void compareTaskEvent1() {
        createTasks();

        assertTrue(t1.compareTo(t2) < 0);   // созданные раньше < созданых позднее

        releaseTasks();
    }

    //
    @Test
    public void compareTaskEvent2() {
        createTasks();

        t2.getTaskTime().setCalendar(now(), false);
        assertTrue(t1.compareTo(t2) > 0);   // без срока > со сроком

        releaseTasks();
    }

    @Test
    public void compareTaskEvent3() {
        createTasks();

        t1.getTaskTime().setCalendar(now(), true);
        t2.getTaskTime().setDate(now().get(Calendar.YEAR), now().get(Calendar.MONTH), now().get(Calendar.DATE));
        assertTrue(t1.compareTo(t2) < 0);   // сегодня в прошлом < сегодня без времени

        releaseTasks();
    }

    @Test
    public void compareTaskEvent4() {
        createTasks();

        TCalendar tCalendar = now();
        tCalendar.add(Calendar.MINUTE, 1);
        t1.getTaskTime().setCalendar(tCalendar, true);
        t2.getTaskTime().setDate(now().get(Calendar.YEAR), now().get(Calendar.MONTH), now().get(Calendar.DATE));
        assertTrue(t1.compareTo(t2) > 0);   // сегодня в будущем < сегодня без времени

        releaseTasks();
    }

    @Test
    public void order1() {
        createTasks();
        t1.setOrder(Order.HIGH);
        t2.setOrder(Order.LOW);

        assertTrue(t1.compareTo(t2) < 0);   // HIGH < LOW (выше в списке)

        releaseTasks();
    }

    @Test
    public void order2() {
        createTasks();
        t1.setPerformed(true);

        TCalendar term = now();
        term.add(Calendar.DATE, 1);

        t2.setTaskTime(new TaskTime(term, false));

        assertTrue(t1.compareTo(t2) > 0); // завершенные выше отложенных

        releaseTasks();
    }

    @Test
    public void compareExecutor() {
        createTasks();

        t2.setExecutor(new StringContainer("Иванов"));
        t1.setExecutor(new StringContainer("Петров"));
        assertTrue(t1.compareTo(t2) > 0);

        releaseTasks();
    }

    private void createTasks() {
        count = TestUtils.getObjectsCount(Task.class);
        t1 = TaskList.addNew();
        t2 = TaskList.addNew();
    }

    private void releaseTasks() {
       TaskList.remove(t1);
       TaskList.remove(t2);
        assertEquals(count, TestUtils.getObjectsCount(Task.class));
    }

    /* ******** Тестирование статусов задач ************************/

    @Test
    public void active() {
        Task task = new Task(true);
        TCalendar date = now();
        date.add(Calendar.DATE, -1);
        task.getTaskTime().setCalendar(date, false);   // устанавливаем дату в прошлом
        task.getRecurrence().set(true);                // устанавливаем повтор

        assertTrue(task.isStateActive());
        assertFalse(task.isSateDeferred());
        assertFalse(task.isStateRepeated());
        assertFalse(task.isStateCompleted());
        assertFalse(task.isStateInPostponed());
        assertFalse(task.isStateInPerformed());
    }

    @Test
    public void deferred() {
        Task task = new Task(true);
        TCalendar date = now();
        date.add(Calendar.DATE, 1);
        task.getTaskTime().setCalendar(date, false);   // устанавливаем дату в будущем

        assertFalse(task.isStateActive());
        assertTrue(task.isSateDeferred());
        assertFalse(task.isStateRepeated());
        assertFalse(task.isStateCompleted());
        assertTrue(task.isStateInPostponed());
        assertFalse(task.isStateInPerformed());
    }

    @Test
    public void repeated() {
        Task task = new Task(true);
        task.create();                          // задача должна быть в БД, чтобы выполнить операцию setPerformed
        task.getRecurrence().set(true);
        task.setPerformed(true);

        assertFalse(task.isStateActive());
        assertFalse(task.isSateDeferred());
        assertTrue(task.isStateRepeated());
        assertFalse(task.isStateCompleted());
        assertTrue(task.isStateInPostponed());
        assertTrue(task.isStateInPerformed());

        task.delete();
    }

    @Test
    public void completed() {
        Task task = new Task(true);
        task.create();
        task.setPerformed(true);

        assertFalse(task.isStateActive());
        assertFalse(task.isSateDeferred());
        assertFalse(task.isStateRepeated());
        assertTrue(task.isStateCompleted());
        assertFalse(task.isStateInPostponed());
        assertTrue(task.isStateInPerformed());

        task.delete();
    }
}