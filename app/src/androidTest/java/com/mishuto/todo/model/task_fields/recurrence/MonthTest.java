package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.task_fields.TaskTime;

import org.junit.Test;

import static com.mishuto.todo.common.TCalendar.now;
import static java.util.Calendar.APRIL;
import static java.util.Calendar.DATE;
import static java.util.Calendar.DECEMBER;
import static java.util.Calendar.FEBRUARY;
import static java.util.Calendar.FRIDAY;
import static java.util.Calendar.OCTOBER;
import static java.util.Calendar.SUNDAY;
import static junit.framework.Assert.assertTrue;

/**
 * Тест Month.getNextEvent() для разных параметров
 * Created by Michael Shishkin on 28.10.2017.
 */
public class MonthTest {

    // сегодня + сегодняшнее число каждого месяца = сегодняшнее число следующего
    @Test
    public void date() {
        TaskTime taskTime = new TaskTime();
        taskTime.setCalendar(now(), false);

        Month month = new Month(taskTime);
        month.setDayOfMonth(taskTime.getDay());
        month.setDayOfMonthSelected(true);

        TCalendar answer = new TCalendar(taskTime.getYear(), taskTime.getMonth()+1, taskTime.getDay());
        assertTrue("got: " + month.getNextEvent().toString(), answer.intervalFrom(DATE, month.getNextEvent()) == 0);
    }

    // 01.10 + каждое 2 число = 2.10
    @Test
    public void date2() {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2027,OCTOBER,1);

        Month month = new Month(taskTime);
        month.setDayOfMonth(2);
        month.setDayOfMonthSelected(true);

        TCalendar answer = new TCalendar(2027, OCTOBER, 2);
        assertTrue("got: " + month.getNextEvent().toString(), answer.compareTo(month.getNextEvent()) == 0);
    }

    // 01.02 + 31 число каждого месяца = 28.02
    @Test
    public void date3() {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2027,1,1);

        Month month = new Month(taskTime);
        month.setDayOfMonth(31);
        month.setDayOfMonthSelected(true);
        month.setMonthFactor(Month.MonthlyFactor.EVERY_MONTH);


        TCalendar answer = new TCalendar(2027, 1, 28);
        assertTrue("got: " + month.getNextEvent().toString(), answer.compareTo(month.getNextEvent()) == 0);
    }

    // 01.02 + 1-е число каждого второго месяца = 01.04
    @Test
    public void date4() {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2027,FEBRUARY,1);

        Month month = new Month(taskTime);
        month.setDayOfMonth(1);
        month.setDayOfMonthSelected(true);
        month.setMonthFactor(Month.MonthlyFactor.SECOND_MONTH);


        TCalendar answer = new TCalendar(2027, APRIL, 1);
        assertTrue("got: " + month.getNextEvent().toString(), answer.compareTo(month.getNextEvent()) == 0);

    }

    // каждое последнее воскресенье месяца для 01.10.27 = 31.10.27
    @Test
    public void week1() {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2027,9,1);

        Month month = new Month(taskTime);
        month.setMonthFactor(Month.MonthlyFactor.EVERY_MONTH);
        month.setDayOfMonthSelected(false);
        month.setWeekNum(BaseMonthYear.WeekNumber.LAST_WEEK);
        month.setWeekDay(SUNDAY);

        TCalendar answer = new TCalendar(2027, 9, 31);
        assertTrue("got: " + month.getNextEvent().toString(), answer.compareTo(month.getNextEvent()) == 0);
    }

    // последнее воскресенье каждого второго месяца для 01.10.27 = 26.12.27
    @Test
    public void week2() {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2027,OCTOBER,1);

        Month month = new Month(taskTime);
        month.setMonthFactor(Month.MonthlyFactor.SECOND_MONTH);
        month.setDayOfMonthSelected(false);
        month.setWeekNum(BaseMonthYear.WeekNumber.LAST_WEEK);
        month.setWeekDay(SUNDAY);

        TCalendar answer = new TCalendar(2027, DECEMBER, 26);
        assertTrue("got: " + month.getNextEvent().toString(), answer.compareTo(month.getNextEvent()) == 0);
    }

    // 29.10.27 ПТ + посл пятница каждого месяца = 26.11.27
    @Test
    public void week3() {
        TaskTime taskTime = new TaskTime();
        taskTime.setDate(2027,9,29);

        Month month = new Month(taskTime);
        month.setMonthFactor(Month.MonthlyFactor.EVERY_MONTH);
        month.setDayOfMonthSelected(false);
        month.setWeekNum(BaseMonthYear.WeekNumber.LAST_WEEK);
        month.setWeekDay(FRIDAY);

        TCalendar answer = new TCalendar(2027, 10, 26);
        assertTrue("got: " + month.getNextEvent().toString(), answer.compareTo(month.getNextEvent()) == 0);
    }
}