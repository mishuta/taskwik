package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.database.aggregators.DBLinkedList;
import com.mishuto.todo.database.aggregators.DBTreeSet;

import org.junit.Test;

import static com.mishuto.todo.model.task_filters.FilterManager.EXECUTOR;
import static com.mishuto.todo.model.task_filters.FilterManager.PROJECT;
import static com.mishuto.todo.model.task_filters.FilterManager.executor;
import static com.mishuto.todo.model.task_filters.FilterManager.project;
import static com.mishuto.todo.model.task_filters.FilterManager.search;
import static com.mishuto.todo.view_model.TestUtils.Reflection.getField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Тестирование истории фильтров (объект модели)
 * Created by Michael Shishkin on 04.06.2018.
 */
public class HistoryFiltersTest {

    private final HistoryFilters HISTORY = HistoryFilters.getInstance();

    // из нескольких запросов подряд сохраняется самый длинный
    @Test
    public void checkSearchDoubles() throws Exception {

        final SearchFilter searchFilter = search();

        searchFilter.setFilter(true);

        items().clear();

        searchFilter.setSelector("simple");
        HISTORY.pushFilter(search());

        searchFilter.setSelector("simpleQuery");
        HISTORY.pushFilter(search());

        searchFilter.setSelector("sim");
        HISTORY.pushFilter(search());

        assertEquals(1, items().size());
        assertEquals("simpleQuery", items().getFirst().getSelector());
    }

    // в истории не должно быть дублирующихся записей
    @Test
    public void checkFiltersDoubles() throws Exception {
        StringContainer selector = new StringContainer("checkFiltersDoubles");
        items().clear();

        executor().setSelector(selector);       // устанавливаем ExecutorFilter
        executor().setFilter(true);
        HISTORY.pushFilter(executor());

        project().setSelector(selector);        // затем ProjectFilter c тем же значением (не должен отброситься)
        project().setFilter(true);
        HISTORY.pushFilter(project());

        executor().setFilter(true);
        HISTORY.pushFilter(executor());  // и снова ExecutorFilter (должен отброситься)

        assertEquals(2, items().size());
        assertEquals(EXECUTOR, items().get(0).getFilterManager());    // на вершине оказывается последний добавленный Executor
        assertEquals(PROJECT, items().get(1).getFilterManager());     // ниже - Project
    }

    // два контекстных фильтра с одним селектором - сокращаются, а с разным - складываются
    @Test
    public void checkContextDoubles() throws Exception {
        DBTreeSet<StringContainer> s = new DBTreeSet<>();
        StringContainer s1 = new StringContainer("checkContextDoubles A");
        StringContainer s2 = new StringContainer("checkContextDoubles B");
        items().clear();

        s.add(s1);

        FilterManager.context().setSelector(s);
        FilterManager.context().setFilter(true);
        HISTORY.pushFilter(FilterManager.context());

        s.remove(s1);                               // имитация редактирования фильтра
        s.add(s2);
        HISTORY.pushFilter(FilterManager.context());

        s.remove(s2);
        s.add(s1);
        HISTORY.pushFilter(FilterManager.context());

        assertEquals(2, items().size());
        assertTrue(((DBTreeSet)items().get(0).getSelector()).contains(s1));    // верхний элемент: s1
        assertTrue(((DBTreeSet)items().get(1).getSelector()).contains(s2));    // нижний: s2
    }

    // неактуальный фильтр должен удалиться
    @Test
    public void notRelevantShouldClear() throws Exception {
        StringContainer selector = new StringContainer("notRelevantShouldClear");

        items().clear();

        executor().setSelector(selector);       // устанавливаем ExecutorFilter
        HISTORY.pushFilter(executor());         // сохраняем
        HISTORY.getHistory();                               // получаем список. Здесь старые очищаются
        assertEquals(0, items().size());
    }

    // длина списка истории не дожна превышать MAX_SIZE
    @Test
    public void checkLimit() throws Exception {
        items().clear();
        search().setFilter(true);
        for(Integer i = 0; i < HistoryFilters.MAX_SIZE + 5; i++) {
            search().setSelector(i.toString());
            HISTORY.pushFilter(search());
        }

        assertEquals(HistoryFilters.MAX_SIZE, items().size());
    }

    private DBLinkedList<HistoryFilters.HistoryItem> items() throws Exception {
        //noinspection unchecked
        return (DBLinkedList<HistoryFilters.HistoryItem>)getField(HistoryFilters.class, HISTORY, "mHistoryItems");
    }
}