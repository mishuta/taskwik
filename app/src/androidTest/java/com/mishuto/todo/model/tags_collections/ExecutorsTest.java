package com.mishuto.todo.model.tags_collections;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static com.mishuto.todo.model.tags_collections.Tags.EMPTY_TAG;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

/**
 * Тест методов Executors
 * Created by Michael Shishkin on 19.05.2017.
 */
public class ExecutorsTest extends Assert {

    private final StringContainer AARON = new StringContainer("Aaron");
    private final StringContainer BOB = new StringContainer("Bob");
    private final StringContainer CAROLINE = new StringContainer("Caroline");
    private final StringContainer FREDDY = new StringContainer("Freddy");
    private final StringContainer JOHNNY = new StringContainer("Johnny");

    private Executors mExecutors = Executors.getInstance();
    private StringContainer mExecutorsArr[] = {AARON, BOB, FREDDY, JOHNNY};

    @Before
    public void setUp() {
        for(StringContainer e: mExecutorsArr)
            mExecutors.add(e);
    }

    @After
    public void tearDown()  {
        for(StringContainer e: mExecutorsArr)
            mExecutors.getExecutorMap().remove(e);
    }

    @Test
    public void clearUnusedTest() {
        Executors.Statistics stat;
        Settings.getEtc().executorsAutoClear.set(true);
        changeTag(EMPTY_TAG, CAROLINE);         // присваеваем всем задачам с пустым тегом тег Каролина

        stat = mExecutors.getExecutorMap().get(JOHNNY);
        stat.mLastUsed.add(Calendar.MONTH, -1); // Интервал достаточный для удаления (30 дней), но частота использования слишком высокая. 10*2+12 > 30 Не должен удалиться
        stat.mUsedCount=10;

        stat = mExecutors.getExecutorMap().get(BOB);
        stat.mLastUsed.add(Calendar.DATE, -5); // Интервал недостаточен для удаления. Не должен удалиться. 5 < 12

        stat = mExecutors.getExecutorMap().get(FREDDY);
        stat.mLastUsed.add(Calendar.MONTH, -1); // Интервал достаточный для удаления. Должен удалиться

        stat = mExecutors.getExecutorMap().get(EMPTY_TAG);
        stat.mLastUsed.add(Calendar.MONTH, -1); // Интервал достаточный для удаления. Но пустой тег никогда не удаляется

        if(TaskList.getTasks().size() > 0) {
            stat = mExecutors.getExecutorMap().get(CAROLINE);
            stat.mLastUsed.add(Calendar.MONTH, -1); // Интервал достаточный для удаления. Но используемый тег не должен удаляться
        }

        TSystem.debug(this, "BEFORE: " + mExecutors.toString());

        mExecutors.clearUnused();
        TSystem.debug(this, "AFTER: " + mExecutors.toString());

        mExecutors.reload();
        // поскольку mExecutors на момент теста может быть не пустым, проверяем только на вхождение, а не на совпадение мапы
        assertThat(mExecutors.getExecutorMap().keySet(), hasItem(AARON));       // не удаляется
        assertThat(mExecutors.getExecutorMap().keySet(), hasItem(JOHNNY));      // не удаляется
        assertThat(mExecutors.getExecutorMap().keySet(), hasItem(BOB));         // не удаляется
        assertThat(mExecutors.getExecutorMap().keySet(), hasItem(EMPTY_TAG));   // не удаляется
        assertThat(mExecutors.getExecutorMap().keySet(), not(hasItem(FREDDY))); // удаляется

        changeTag(CAROLINE, EMPTY_TAG);

        mExecutors.remove(AARON);
        mExecutors.remove(JOHNNY);
        mExecutors.remove(BOB);
        mExecutors.remove(CAROLINE);
    }

    private void changeTag(StringContainer oldTag, StringContainer newTag) {
        for(Task task : TaskList.getTasks())
            if(task.getExecutor() == oldTag)
                task.setExecutor(newTag);

    }
}