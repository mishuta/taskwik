package com.mishuto.todo.database.aggregators;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.ORMBaseTest;

import org.junit.Test;

import java.util.ArrayList;
import java.util.ListIterator;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;

/**
 * Тестирование специфичных методов классов производных от DBList
 * Created by Michael Shishkin on 10.03.2017.
 */
public class DBListTest extends ORMBaseTest {

    private static class TestClass extends DBObject {
        DBLinkedList<Integer> mLinkedList;

        TestClass() {
            super("CollectionTestClass");
        }

        @Override
        public TestClass clone() throws CloneNotSupportedException {
            TestClass newObject;
            newObject = (TestClass) super.clone();
            //noinspection unchecked
            newObject.mLinkedList = (DBLinkedList<Integer>) mLinkedList.clone();
            return newObject;
        }
    }

    // проверка клонирования  DBLinkedList.clone()
    @Test
    public void clone_DBLinkedList() throws Exception {
        printTitle(this, "clone_DBLinkedList");

        TestClass testClass = new TestClass();
        testClass.mLinkedList = new DBLinkedList<>();

        TestClass testClass1 = testClass.clone();       //клонируем объекты вместе с пустым списком
        testClass.mLinkedList.addFirst(1);
        testClass.mLinkedList.addFirst(2);              // в оригинальном списке 2 элемента

        testClass1.mLinkedList.addFirst(3);             // в клоне 1 элемент

        assertEquals(1, testClass1.mLinkedList.size()); // в клоне должен быть роано один элемент
    }

    // проверка правильной последовательности записи/чтения для  DBLinkedList
    @Test
    public void ordered_DBLinkedListAdd() {
        printTitle(this, "ordered_DBLinkedList");
        DBLinkedList<Integer> list = new DBLinkedList<>();
        list.create();
        list.addLast(3);
        list.addLast(4);
        list.addFirst(2);
        list.addFirst(1);
        list.add(5);

        list.reload();

        for(int i = 0; i < list.size(); i++)
            assertEquals(i + 1, (int)list.get(i));
    }

    // проверка правильной последовательности записи/чтения для  DBArrayList
    @Test
    public void ordered_DBArrayListAdd() {
        printTitle(this, "ordered_DBArrayList");
        DBArrayList<Integer> list = new DBArrayList<>();
        list.create();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        list.reload();

        for(int i = 0; i < list.size(); i++)
            assertEquals(i + 1, (int)list.get(i));

    }

    // проверка правильной последовательности записи/чтения для  DBArrayList
    @Test
    public void ordered_DBArrayListRemove() {
        printTitle(this, "ordered_DBArrayList");
        DBArrayList<Integer> list = new DBArrayList<>();
        list.create();
        list.add(1);        // index = 0
        list.add(100);      // index = 1
        list.add(101);      // index = 2
        list.add(2);        // index = 3

        list.remove((Integer) 100); // удаляем из середины
        list.remove((Integer) 101); // индекс 1 и 2
        list.add(3);    // добавляем в конец. останутся 1,2,3 (у объекта 2 индекс поменяется на 1)

        list.reload();

        for(int i = 0; i < list.size(); i++)
            assertEquals(i + 1, (int)list.get(i));

    }

    // проверка метода DBArrayList.setNewList()
    @Test
    public void setNewList_DBArrayList() {
        printTitle(this, "setNewList_DBArrayList");
        DBArrayList<Integer> list = new DBArrayList<>();
        list.create();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        list.reload();

        ArrayList<Integer> newList = new ArrayList<>();
        newList.add(8);
        newList.add(9);

        list.setCollection(newList);

        assertEquals(2, list.queryItems().countNClose());
    }

    //listIterator.remove() удаляет элементы из базы
    @Test
    public void listIteratorRemove() {
        DBArrayList<StringContainer> list = new DBArrayList<>();
        StringContainer s1 = new StringContainer("aaa");
        StringContainer s2 = new StringContainer("bbb");
        StringContainer s3 = new StringContainer("ccc");

        list.create();
        list.add(s1);
        list.add(s2);
        list.add(s3);

        assertEquals(3, list.queryItems().countNClose());
        assertThat(s1.getRefCounter(), is(1));
        assertThat(s2.getRefCounter(), is(1));
        assertThat(s3.getRefCounter(), is(1));

        for(ListIterator<StringContainer> listIterator = list.listIterator(); listIterator.hasNext(); ) {
            listIterator.next();
            listIterator.remove();
        }

        assertThat(list, hasSize(0));
        assertEquals(0, list.queryItems().countNClose());
        assertThat(s1.getRefCounter(), is(0));
        assertThat(s2.getRefCounter(), is(0));
        assertThat(s3.getRefCounter(), is(0));

    }
}