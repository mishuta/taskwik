package com.mishuto.todo.database.aggregators;

import android.database.Cursor;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.ORMBaseTest;
import com.mishuto.todo.database.SQLHelper;
import com.mishuto.todo.database.SQLTable;

import org.junit.Test;

import static com.mishuto.todo.database.aggregators.DBCollection.ITEMS_TABLE;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

/**
 * Тест DBCollection
 * Created by Michael Shishkin on 24.02.2017.
 */
public class DBCollectionTest extends ORMBaseTest {

    // создание коллекции
    @Test
    public void create() {
        printTitle(this, "create");
        StringContainer stringContainer1 = new StringContainer("create-1");
        StringContainer stringContainer2 = new StringContainer("create-2");

        DBHashSet<StringContainer> set = new DBHashSet<>();
        set.create();
        set.add(stringContainer1);
        set.add(stringContainer2);

        assertObjectById(set, true);                // set создан
        assertEquals(2, set.queryItems().countNClose());  // в нем 2 элемента
        assertTrue(doesExist(stringContainer1));    // в таблице StringContainer есть
        assertTrue(doesExist(stringContainer2));    // оба объекта

        set.delete();
        assertCursors();
    }

    private static class InternalCollection extends DBObject {
        DBArrayList<Integer> mList;

        public void setList(DBArrayList<Integer> list) {
            mList = list;
            update();
        }
    }

    // создание коллекции внутри другого объекта не должно порождать лишних объектов
    @Test
    public void createInternalCollection() {
        InternalCollection test = new InternalCollection();
        test.create();
        DBArrayList<Integer> list = new DBArrayList<>();
        list.add(1);
        test.setList(list);
        assertThat(list.getRefCounter(), is(1));

        test.setList(null);
        assertThat(list.getRefCounter(), is(0));
    }

    // удаление коллекции
    @Test
    public void delete() {
        printTitle(this, "delete");

        StringContainer stringContainer1 = new StringContainer("delete-1");
        StringContainer stringContainer2 = new StringContainer("delete-2");

        DBHashSet<StringContainer> set = new DBHashSet<>();
        set.create();
        set.add(stringContainer1);
        set.add(stringContainer2);

        set.delete();

        assertEquals(0, set.queryItems().countNClose());      // после удаления в БД нет элементов коллекции
        assertObjectById(set, false);                   // и нет самой коллекции
        assertFalse(doesExist(stringContainer1));       // и элементы
        assertFalse(doesExist(stringContainer2));       // действительно удалились

        assertCursors();
    }

    // удаление элемента коллекции
    @Test
    public void deleteExactlyItem()  {
        printTitle(this, "deleteExactlyItem");
        StringContainer stringContainer1 = new StringContainer("deleteExactlyItem-1");
        StringContainer stringContainer2 = new StringContainer("deleteExactlyItem-2");

        DBHashSet<StringContainer> set = new DBHashSet<>();
        set.create();
        set.add(stringContainer1);
        set.add(stringContainer2);

        set.remove(stringContainer1);

        assertEquals(1, set.queryItems().countNClose());      // после удаления 1 элемента остается 1

        set.reload();

        assertFalse(set.contains(stringContainer1));    // удалился именно 1-й элемент
        assertFalse(doesExist(stringContainer1));       // он удалился так же из таблицы StringContainer

        set.delete();

        assertCursors();
    }

    //талбица ITEMS_TABLE может содержать элементы с одинаковыми id (value), но разных типов
    private static class TypeA extends DBObject { Integer a; }
    private static class TypeB extends DBObject { Integer b; }

    @Test
    public void deleteSameIdDifferentTypes() {
        SQLTable itemTable = new SQLTable(ITEMS_TABLE);
        int oldSize = itemTable.doesTableExist() ? itemTable.query().countNClose() : 0;
        //noinspection MismatchedQueryAndUpdateOfCollection
        DBHashSet<DBObject> set = new DBHashSet<>();
        set.create();

        TypeA a = new TypeA();  // создаем два объекта разных типов, причем RecordId после добавления в базу
        TypeB b = new TypeB();  // у них должны быть одинаковыми (1)

        set.add(a);
        set.add(b);

        assertThat(itemTable.query().countNClose() - oldSize, is(2));   // добавлено 2 элемента с одинаковыми ключами

        set.remove(a);
        assertThat(itemTable.query().countNClose() - oldSize, is(1));   // удалился только один элемент

        set.remove(b);
        assertThat(itemTable.query().countNClose() - oldSize, is(0));   // удалился второй элемент
    }

    // несозданные в БД коллекции не должны создавать в БД объекты при добавлении и удалять из БД при удалении
    @Test
    public void checkNotCreatedCollection() {
        DBHashSet<StringContainer> set = new DBHashSet<>();
        StringContainer stringContainer = new StringContainer("testNotCreatedCollection");
        set.add(stringContainer);
        set.remove(stringContainer);

        assertThat(stringContainer.getRecordID(), is(0L));
        assertThat(set.getRecordID(), is(0L));
    }

    // очистка коллекции
    @Test
    public void clear() {
        printTitle(this, "clear");

        StringContainer stringContainer1 = new StringContainer("clear-1");
        StringContainer stringContainer2 = new StringContainer("clear-2");

        DBHashSet<StringContainer> set = new DBHashSet<>();
        set.create();
        set.add(stringContainer1);
        set.add(stringContainer2);

        set.clear();

        assertEquals(0, set.queryItems().countNClose());      // после очистки в БД нет элементов
        assertFalse(doesExist(stringContainer1));       // и элементы
        assertFalse(doesExist(stringContainer2));       // действительно удалились

        assertObjectById(set, true);                   // а объект - жив

        assertCursors();
    }

    // чтение коллекции
    @Test
    public void read()  {
        printTitle(this, "read");

        StringContainer stringContainer1 = new StringContainer("read-1");
        StringContainer stringContainer2 = new StringContainer("read-2");

        DBHashSet<StringContainer> set = new DBHashSet<>();
        set.create();
        set.add(stringContainer1);
        set.add(stringContainer2);

        set.reload();

        assertThat(set, hasSize(2));                     // должно прочитаться 2 элемента

        set.delete();

        assertCursors();
    }

    // проверка создания/чтения не DBObject - коллекции
    @Test
    public void IntegerCollection() {
        DBTreeSet<Integer> set = new DBTreeSet<>();
        set.create();
        set.add(2);
        set.add(1);

        set.reload();

        assertThat(set, hasSize(2));
        assertThat(set, contains(1,2));
    }

    // проверка создания/чтения null значения
    @Test
    public void nullItem() {
        DBHashSet<StringContainer> set = new DBHashSet<>();
        set.create();
        set.add(null);
        set.reload();

        assertThat(set, hasSize(1));
        assertThat(set, contains((StringContainer)null));

        set.delete();
    }

    // проверка что при загрузке пустого объекта не возникает Exception
    private static class TestColClass extends DBObject {
        DBHashSet<String> col = new DBHashSet<>();
    }

    @Test
    public void loadSingleTone_nullable() {
        printTitle(this, "loadSingleTone_nullable");

        SQLTable testColSQLTable = new SQLTable(TestColClass.class.getSimpleName(), (SQLTable.Column[])null);
        testColSQLTable.deleteTable();

        TestColClass t1 = new TestColClass();
        t1.loadAsSingleton();
    }

    /********* Проверка работы коллекций с неуникальными, но разными объектами (t1.equals(t2), но t1!=t2) */
    @Test
    public void removeList() {
        DBArrayList<StringContainer> list = new DBArrayList<>();
        list.create();
        list.add(new StringContainer("1"));
        list.add(new StringContainer("2"));
        list.add(new StringContainer("1"));

        StringContainer survivor = list.get(2);

        assertThat(list, hasSize(3));
        assertThat(list.queryItems().countNClose(), is(3));

        list.remove(new StringContainer("1"));

        Cursor cursor = list.queryItems();
        cursor.moveToNext();

        assertThat(list.getItem(cursor).getRecordID(), is(survivor.getRecordID()));
        assertThat(list, hasSize(2));
        assertThat(cursor.getCount(), is(2));
        cursor.close();
    }

    @Test
    public void removeSet() {
        DBHashSet<StringContainer> set = new DBHashSet<>();
        set.create();
        set.add(new StringContainer("1"));
        set.add(new StringContainer("2"));

        set.remove(new StringContainer("1"));

        assertThat(set, hasSize(1));
        assertThat(set.queryItems().countNClose(), is(1));
        assertThat(set, contains(new StringContainer("2")));
    }

    @Test
    public void duplicatesInList() {
        DBArrayList<StringContainer> list = new DBArrayList<>();
        list.create();
        list.add(new StringContainer("1"));
        list.add(new StringContainer("2"));
        list.add(new StringContainer("1"));

        assertThat(list, hasSize(3));

        list.reload();
        assertThat(list, hasSize(3));
    }

    @Test
    public void duplicatesNotAllowed() {
        printTitle(this, "duplicatesNotAllowed");
        DBHashSet<StringContainer> set = new DBHashSet<>();
        set.create();
        set.add(new StringContainer("1"));
        set.add(new StringContainer("2"));
        set.add(new StringContainer("1"));

        assertThat(set, hasSize(2));
        assertThat(set.queryItems().countNClose(), is(2));
    }

    @Test
    public void duplicatesNotAllowedAfterReload() {
        DBHashSet<StringContainer> set = new DBHashSet<>();
        set.create();
        set.add(new StringContainer("1"));
        set.add(new StringContainer("2"));

        set.reload();
        set.add(new StringContainer("1"));

        assertThat(set, hasSize(2));
        assertThat(set.queryItems().countNClose(), is(2));
    }

    // есть ли в таблице StringContainer такой элемент
    private boolean doesExist(StringContainer stringContainer) {
        return stringContainer.getSQLTable().query(SQLHelper.whereString("STRING=?", stringContainer.toString())).countNClose() > 0;
    }
}