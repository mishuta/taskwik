package com.mishuto.todo.database;

import com.mishuto.todo.common.ImmutableStringContainer;
import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TObjects;
import com.mishuto.todo.common.Time;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static com.mishuto.todo.common.TObjects.eq;
import static com.mishuto.todo.view_model.TestUtils.getObjectsCount;
import static org.hamcrest.Matchers.is;

/**
 * Тест DBObject
 * Created by Michael Shishkin on 22.02.2017.
 */

public class DBObjectTest extends ORMBaseTest {

    private static class TestObject extends DBObject {
        String mString;
        Integer mInteger;

        void clear() {
            if (getSQLTable().doesTableExist())
                getSQLTable().delete(null);
        }
    }

    private static class InnerDBObject extends DBObject {
        StringContainer mStringContainer = new StringContainer();
    }

    // Создать простой объект
    @Test
    public void create() throws Exception {
        printTitle(this, "create");
        StringContainer stringContainer = new StringContainer();
        stringContainer.create();

        assertObjectById(stringContainer, true);

        stringContainer.delete();
        assertCursors();
    }


    // Создать объект DBObject с вложенным объектом DBObject
    @Test
    public void createInnerDBObject() throws Exception {
        printTitle(this, "createInnerDBObject");
        InnerDBObject innerDBObject = new InnerDBObject();
        innerDBObject.create();
        innerDBObject.mStringContainer.create();

        assertObjectById(innerDBObject, true);
        assertObjectById(innerDBObject.mStringContainer, true);

        innerDBObject.delete();
        assertCursors();
    }

    // тест чтения объекта из БД
    @Test
    public void read() throws Exception {
        printTitle(this, "read");

        StringContainer s = new StringContainer("read");
        s.create();
        s.reload();

        assertEquals("read", s.toString());

        s.delete();
        assertCursors();
    }

    // тест создания/чтения сабкласса объекта
    @Test
    public void readSubObject() throws Exception {
        printTitle(this, "readSubObject");

        InnerDBObject innerDBObject = new InnerDBObject();
        innerDBObject.mStringContainer = ImmutableStringContainer.getObject("readSubObject");
        innerDBObject.create();

        innerDBObject.reload();

        assertEquals(ImmutableStringContainer.class, innerDBObject.mStringContainer.getClass());
        assertEquals("readSubObject", innerDBObject.mStringContainer.toString());

        innerDBObject.delete();
        assertCursors();
    }


    // тест обновления объекта
    @Test
    public void update() throws Exception {
        printTitle(this, "update");
        StringContainer s = new StringContainer();
        s.create();
        s.setString("update");

        int count = getObjectsCount(StringContainer.class);
        s.update();

        s.reload();

        assertEquals("update", s.toString());
        assertEquals(count, getObjectsCount(StringContainer.class));

        s.delete();
    }

    // удаление вложенного объекта при обновлении непустого поля на пустое
    @Test
    public void update_delete() throws Exception {
        printTitle(this, "update_delete");

        InnerDBObject innerDBObject = new InnerDBObject();
        innerDBObject.mStringContainer = new StringContainer("update_delete");
        innerDBObject.create();

        int count = getObjectsCount(StringContainer.class);
        innerDBObject.mStringContainer = null;
        innerDBObject.update();

        assertNull(innerDBObject.mStringContainer);
        assertEquals(count - 1, getObjectsCount(StringContainer.class));

        innerDBObject.delete();
    }

    // создание вложенного объекта при обновлении пустого поля на непустое
    @Test
    public void update_create() throws Exception {
        printTitle(this, "update_create");

        InnerDBObject innerDBObject = new InnerDBObject();
        innerDBObject.mStringContainer = null;
        innerDBObject.create();

        int count = getObjectsCount(StringContainer.class);
        innerDBObject.mStringContainer = new StringContainer("update_create");
        innerDBObject.update();

        assertEquals("update_create", innerDBObject.mStringContainer.toString());
        assertEquals(count + 1, getObjectsCount(StringContainer.class));

        innerDBObject.delete();
        assertCursors();
    }

    private static class TestObjectUpd extends TestObject {
        private transient int updatedFields = 0;
        Time mTime; // StoredField
        TCalendar mCalendar;
        StringContainer mStringContainer; // DBObject


        int getUpdatedFields() {
            int n = updatedFields;
            updatedFields = 0;
            return n;
        }

        @Override
        void updateRecord() {
            updatedFields = getSQLTable().getCV().size();
            super.updateRecord();
        }
    }

    //должны апдейтится только необходимые поля
    @Test
    public void updateOnlyField() throws Exception {
        TestObjectUpd testObjectUpd  = new TestObjectUpd();
        testObjectUpd.create();

        testObjectUpd.mInteger = 1;
        testObjectUpd.mString = "a";
        testObjectUpd.update();

        assertEquals(2, testObjectUpd.getUpdatedFields());  // 2 поля обновилось

        testObjectUpd.mInteger = 2;
        testObjectUpd.update();
        assertEquals(1, testObjectUpd.getUpdatedFields());  // 1 поле обновилось

        testObjectUpd.update();
        assertEquals(0, testObjectUpd.getUpdatedFields());  // 0 полей обновилось, updateRecord не вызывался
    }

    @Test
    public void updateOnlyField2() throws Exception {
        TestObjectUpd testObjectUpd  = new TestObjectUpd();

        testObjectUpd.mTime = new Time(1,1);
        testObjectUpd.mStringContainer = new StringContainer("s");
        testObjectUpd.mCalendar = new TCalendar(2018, 3, 12);
        testObjectUpd.create();
        testObjectUpd.getUpdatedFields(); // обнуляем счетчик после create

        testObjectUpd.update();
        assertEquals(0, testObjectUpd.getUpdatedFields());

        testObjectUpd.mTime.set(1,1); // устанавливаем те же значения
        testObjectUpd.mCalendar.set(2018,3,12);
        testObjectUpd.mStringContainer.setString("s1"); // а в DBObject - иное значение. Поскольку DBObject не меняет id, то апдейта не происходит
        testObjectUpd.update();
        assertEquals(0, testObjectUpd.getUpdatedFields());  // не обновлялось

        testObjectUpd.mTime.set(1,2); // устанавливаем новые значения
        testObjectUpd.mCalendar.set(2018, 3, 13, 0, 0);

        testObjectUpd.mStringContainer = new StringContainer("s1");
        testObjectUpd.update();
        assertEquals(4, testObjectUpd.getUpdatedFields());  // все обновились, DBObject  = 2 поля
    }


    // тест удаления объкта
    @Test
    public void delete() throws Exception {
        printTitle(this, "delete");

        StringContainer s = new StringContainer();
        s.create();
        int count = getObjectsCount(StringContainer.class);

        s.delete();
        assertEquals(count - 1, getObjectsCount(StringContainer.class));

        assertCursors();
    }

    @Test
    public void deleteInnerDBObject() throws Exception {
        InnerDBObject innerDBObject = new InnerDBObject();
        innerDBObject.create();

        int count1 = getObjectsCount(InnerDBObject.class);
        int count2 = getObjectsCount(StringContainer.class);

        innerDBObject.delete();

        assertEquals(count1 - 1, getObjectsCount(InnerDBObject.class));
        assertEquals(count2 - 1, getObjectsCount(StringContainer.class));
    }


    // тест loadAsSingleton
    @Test
    public void loadAsSingleTone() throws Exception {
        printTitle(this, "loadAsSingleton");

        TestObject probe = new TestObject();
        probe.mInteger = 1000;
        probe.mString = "hello";
        probe.clear();

        probe.loadAsSingleton();
        probe.reload();

        assertEquals(probe.getSQLTable().query().countNClose(), 1);   // появилась одна запись
        assertEquals(1000, (long)probe.mInteger);                // записанный = прочитанный
        assertEquals("hello", probe.mString);

        probe.releaseCursor();
        assertCursors();
    }

    // тест падения если таблица синглтон содержит более одной строки
    @Test(expected = AssertionError.class)
    public void loadSingleTone_err() throws Exception {
        printTitle(this, "loadSingleTone_err");

        TestObject orig = new TestObject();
        TestObject test = new TestObject();

        orig.clear();

        test.create();
        orig.create();

        test.loadAsSingleton();              // возникает AssertError

        assertCursors();
    }

    // **********************      Проверка создания/чтения объектов, имеющих суперклассы

    private static class TestClass1 extends DBObject {
        String testClass1Str;
        TestObject mTestObject = new TestObject();
    }

    private static class TestClass2 extends TestClass1 {
        String testClass2Str;
    }

    private static class TestClass3 extends TestClass2 {
        String testClass3Str;

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        @Override
        public boolean equals(Object obj) {
            TestClass3 bro = (TestClass3)obj;
            return eq(testClass3Str, bro.testClass3Str) && eq(testClass2Str, bro.testClass2Str) && eq(testClass1Str, bro.testClass1Str);
        }
    }

    private static class TestClass4 extends TestClass3 {
        String testClass1Str; // такое же имя как у TestClass1
    }


    @Test
    public void testSubClasses() throws Exception {
        printTitle(this, "testSubClasses");

        TestClass3 testClassOrig = new TestClass3();
        testClassOrig.testClass1Str = "TestClass1";
        testClassOrig.testClass2Str = "TestClass2";
        testClassOrig.testClass3Str = "TestClass3";

        testClassOrig.create();
        TestClass3 testClassTest = TObjects.clone(testClassOrig);

        testClassOrig.reload();
        assertEquals(testClassOrig, testClassTest); // прочтенный объект из БД = созданному

        assertCursors();
    }

    // тест падения, в случае совпадения атрибутов у сабкласса и суперкласса
    @Test(expected = AssertionError.class)
    public void testSubClasses_withErr() throws Exception {
        printTitle(this, "createSubClasses_withErr");

        TestClass4 testClass4 = new TestClass4();   // Здесь должен быть AssertionError, т.к. поле testClass1Str у TestClass4 и у TestClass1 совпадает
    }

      // ********************************************** тестирование рекурсивных петлевых ссылок ********************************

    private static class A extends Base {
        B mB = new B();
    }
    private static class B extends Base {
        C mC = new C();
    }
    private static class C extends Base {
        A mA;
    }

    private static class D extends Base {
        C mC;
    }

    private static class Base extends DBObject {
        Integer count=0;
        Base() {
            if(getSQLTable().doesTableExist())
                count = getSQLTable().query().countNClose();
        }
    }

    // тестирование рекурсивных петлевых ссылок при создании объекта в БД
    @Test
    public void recurseLoopCreate() throws Exception {
        printTitle(this, "recurseLoopCreate");
        A a = new A();
        a.mB.mC.mA = a;                                                                 // петля A-B-C-A
        a.create();                                                                     // создается по одному объекту A, B, C
        assertEquals(a.count + 1, a.getSQLTable().query().countNClose());            // создан один А
        assertEquals(a.mB.mC.count + 1, a.mB.mC.getSQLTable().query().countNClose());// создан один С

        // А имеет 2 ссылки: a и a.mB.mC.mA, B и C имеют по одной ссылке  (a.mB и a.mB.mC)
        // A, B, C имеют по одной петле ABCA, BCAB, CABC
        // a создает объект А (Ar=1, Al=0)
        // a.mB создает объект B (Br=1 Bl=0)
        // a.mB.mC создает объект С (Cr=0, Cl=0)
        // a.mB.mC пытается создать А, выход из create (Ar=2, Br=1, Cr=1, Al=1, Bl=1, Cl=1)
        a.reload();
        assertThat(a.getRefCounter(),is(2));
        assertThat(a.mB.getRefCounter(),is(1));
        assertThat(a.mB.mC.getRefCounter(),is(1));


        assertEquals(1, getCountLoopsRef(a));
        assertEquals(1, getCountLoopsRef(a.mB));
        assertEquals(1, getCountLoopsRef(a.mB.mC));

        assertCursors();
    }

    @Test
    public void recurseLoopDelete() throws Exception {
        printTitle(this, "recurseLoopCreate");
        A a = new A();
        a.mB.mC.mA = a;                                                     // петля A-B-C-A
        a.create();                                                         // создается по одному объекту A, B, C
        assertEquals(a.count + 1, a.getSQLTable().query().countNClose());              // создан один А
        assertEquals(a.mB.mC.count + 1, a.mB.mC.getSQLTable().query().countNClose());  // создан один С

        a.mB.mC.mA = null;
        a.mB.mC.update();
        a.reload();
        assertThat(a.getRefCounter(),is(1));
        assertThat(a.mB.getRefCounter(),is(1));
        assertThat(a.mB.mC.getRefCounter(),is(1));

        assertEquals(0, getCountLoopsRef(a));
        assertEquals(0, getCountLoopsRef(a.mB));
        assertEquals(0, getCountLoopsRef(a.mB.mC));

        assertCursors();
    }

    @Test
    public void recurseLoopCreate2() {
        printTitle(this, "recurseLoopCreate");
        A a = new A();
        a.create();
        a.mB.mC.mA = a;                                                     // петля создается после создания основного объекта
        a.mB.mC.mA.create();
        assertEquals(a.count + 1, a.getSQLTable().query().countNClose());
        assertEquals(a.mB.mC.count + 1, a.mB.mC.getSQLTable().query().countNClose());

        assertCursors();
    }

    /* ****************************************************************
    Тестирование подсчета числа петель

                            A  <----+
                           /|\      |
                         D<-C->B<+  |
                         |     | |  |
                         F<----E=+--+
     */



    private static class A1 extends DBObject{ // 1 петля: A-C-B-E-A или A-B-E-A
        B1 b;
        C1 c;
        D1 d;

        @Override
        public String toString() { return "A"; }
    }
    private static class B1 extends DBObject{ // 3 петли: B-E-B, B-E-B-A-B, B-E-A-C-B
        E1 e;
        @Override
        public String toString() { return "B"; }

    }
    private static class C1 extends DBObject{ // 1 петля C-B-E-A
        B1 b;
        D1 d;
        @Override
        public String toString() { return "C"; }

    }
    private static class D1 extends DBObject{ // 0 петель
        F1 f;
        @Override
        public String toString() { return "D"; }
    }
    private static class E1 extends DBObject{ // 1 петля E-B-E
        B1 b;
        A1 a;
        F1 f;
        @Override
        public String toString() { return "E"; }
    }
    private static class F1 extends DBObject{ // 1 петля F-F
        F1 f;
        @Override
        public String toString() { return "F"; }
    }

    @Test
    public void countLoopRefs() throws Exception {
        A1 a = new A1();
        B1 b = new B1();
        C1 c = new C1();
        D1 d = new D1();
        E1 e = new E1();
        F1 f = new F1();

        a.b = c.b = e.b = b;
        a.c = c;
        a.d = c.d = d;
        b.e = e;
        e.a = a;
        d.f = e.f = f;
        f.f = new F1();
        f.f.create();

        assertThat(getCountLoopsRef(a), is(1));
        assertThat(getCountLoopsRef(b), is(3));
        assertThat(getCountLoopsRef(c), is(1));
        assertThat(getCountLoopsRef(d), is(0));
        assertThat(getCountLoopsRef(e), is(1));
        assertThat(getCountLoopsRef(f), is(0));
    }

    private static int getCountLoopsRef (DBObject object) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Method countLoopRef = DBObject.class.getDeclaredMethod("countLoopRefs", (Class[])null);
        countLoopRef.setAccessible(true);
        return (int)countLoopRef.invoke(object, (Object[])null);
    }

    /*  Создаем петлю a->A-B-C-A
        Удаляем a
        Должны удалиться все объекты
     */
    @Test
    public void recurseLoopDelete1() throws Exception {
        printTitle(this, "recurseLoopDelete1");
        A a = new A();

        a.create();
        a.mB.mC.mA = a;
        a.mB.mC.update();

        a.delete();

        // количество объектов А, B, C не изменилось
        assertEquals((int)a.count, a.getSQLTable().query().countNClose());             // количество объектов А не изменилось
        assertEquals((int)a.mB.count, a.mB.getSQLTable().query().countNClose());       // количество объектов В не изменилось
        assertEquals((int)a.mB.mC.count, a.mB.mC.getSQLTable().query().countNClose()); // количество объектов C не изменилось

        // RecordID  обнулены
        assertEquals(0, a.getRecordID() + a.mB.getRecordID() + a.mB.mC.getRecordID());

        assertCursors();
    }
    /*  Создаем петлю a->A-B-C-A
        Удаляем связь C-A
        Объекты не должны удаляться объекты
        Количество ссылок = 1, петель = 0 для всех объектов
     */
    @Test
    public void recurseLoopDelete2() throws Exception {
        printTitle(this, "recurseLoopDelete2");
        A a = new A();

        a.create();
        a.mB.mC.mA = a;
        a.mB.mC.update();

        a.mB.mC.mA = null;
        a.mB.mC.update();

        // количество объектов А, B, C стало больше на 1
        assertEquals(a.count + 1, a.getSQLTable().query().countNClose());
        assertEquals(a.mB.count + 1, a.mB.getSQLTable().query().countNClose());
        assertEquals(a.mB.mC.count + 1, a.mB.mC.getSQLTable().query().countNClose());

        assertThat(a.getRefCounter(), is(1));
        assertThat(a.mB.getRefCounter(), is(1));
        assertThat(a.mB.mC.getRefCounter(), is(1));

        assertEquals(0, getCountLoopsRef(a));
        assertEquals(0, getCountLoopsRef(a.mB));
        assertEquals(0, getCountLoopsRef(a.mB.mC));

        assertCursors();
    }

    //******************************* тестирование счетчика ссылок *******************************

    @Test
    public void refsCounterTest() {
        printTitle(this, "refsCounterTest");
        B b = new B();
        D d = new D();
        C c = d.mC = b.mC;                              // b и d ссылаются на c ...
        b.create();
        d.create();

        assertEquals(c.count + 1, c.getSQLTable().query().countNClose());    // ... но c создается 1 раз

        b.delete();
        d.delete();

        assertEquals((int)c.count, c.getSQLTable().query().countNClose());    // ... и удаляется 1 раз

        assertCursors();
    }

    // Проверка восстановления количества ссылок после чтения объекта
    @Test
    public void restoreRefsCounter() throws Exception {
        C c = new C();  // создаем один объект С
        D d1 = new D(); // и два объекта D
        D d2 = new D();

        d1.mC = c;
        d2.mC = c;

        d1.create();    // в базе создаются 2 ссылки
        d2.create();    // на с

        assertThat(c.getRefCounter(), is(2));

        c.reload();
        assertThat(c.getRefCounter(), is(2));    // проверяем что после загрузки из базы количество ссылок не изменилось (имитируем рестарт приложения)
    }
   }
