package com.mishuto.todo.database;

import android.os.SystemClock;

import com.mishuto.todo.common.StringContainer;

import org.junit.Test;

import static com.mishuto.todo.database.Transactions.beginTransaction;
import static com.mishuto.todo.database.Transactions.commitTransaction;
import static com.mishuto.todo.database.Transactions.rollbackTransaction;
import static org.junit.Assert.assertEquals;

/**
 * Тестирование транзакций
 * Created by Michael Shishkin on 15.02.2018.
 */
public class TransactionsTest {

    // после коммита все изменения объектов сохраняются
    @Test
    public void commit() throws Exception {
        StringContainer str1 = new StringContainer("A1");
        StringContainer str2 = new StringContainer("A2");

        beginTransaction();

        str1.setString("B1");
        str2.setString("B2");
        str1.create();
        str2.create();

        commitTransaction();

        str1.reload();
        str2.reload();

        assertEquals(new StringContainer("B1"), str1);
        assertEquals(new StringContainer("B2"), str2);
    }

    // после ролбэка все изменения объектов не сохраняются
    @Test
    public void rollback() throws Exception {
        StringContainer str1 = new StringContainer("A1");
        StringContainer str2 = new StringContainer("A2");
        str1.create();
        str2.create();

        beginTransaction();

        str1.setString("B1");
        str2.setString("B2");

        rollbackTransaction();

        str1.reload();
        str2.reload();

        assertEquals(new StringContainer("A1"), str1);
        assertEquals(new StringContainer("A2"), str2);
    }

    //внешний комит сохраняет изменения во внешней транзакции, а внутренний ролбэк отменяет изменения в внутренней транзакции
    @Test
    public void nested1() throws Exception {
        StringContainer str1 = new StringContainer("A1");
        StringContainer str2 = new StringContainer("A2");
        str1.create();
        str2.create();

        beginTransaction();

        str1.setString("B1");

        beginTransaction();

        str2.setString("B2");

        rollbackTransaction();
        commitTransaction();

        str1.reload();
        str2.reload();

        assertEquals(new StringContainer("B1"), str1);
        assertEquals(new StringContainer("A2"), str2);
    }

    //внешний ролбэк отменяет изменения внешней и внутренней транзакции
    @Test
    public void nested2() throws Exception {
        StringContainer str1 = new StringContainer("A1");
        StringContainer str2 = new StringContainer("A2");
        str1.create();
        str2.create();

        beginTransaction();

        str1.setString("B1");

        beginTransaction();

        str2.setString("B2");

        commitTransaction();
        rollbackTransaction();

        str1.reload();
        str2.reload();

        assertEquals(new StringContainer("A1"), str1);
        assertEquals(new StringContainer("A2"), str2);
    }

    //транзакция блокирует конкурирующий поток с изменениями базы
    private int flag = 1;
    @Test
    public void lockWrite() throws Exception {

        new Thread(new Runnable() {
            @Override
            public void run() {
                beginTransaction();
                StringContainer s = new StringContainer("b");
                s.create();
                SystemClock.sleep(1000);
                commitTransaction();
                flag = 100;
            }
        }).start();

        SystemClock.sleep(100);

        new Thread(new Runnable() {
            @Override
            public void run() {
                assertEquals(1, flag);
                StringContainer s = new StringContainer("a");
                s.create();
                assertEquals(100, flag);
                flag = 200;
            }
        }).start();

        SystemClock.sleep(1500);
        assertEquals(200, flag);
    }

    //транзакция не блокирует конкурирующий поток с чтением из базы
    @Test
    public void lockRead() throws Exception {

        new Thread(new Runnable() {
            @Override
            public void run() {
                beginTransaction();
                StringContainer s = new StringContainer("b");
                s.create();
                SystemClock.sleep(1000);
                commitTransaction();
                flag = 100;
            }
        }).start();

        SystemClock.sleep(100);

        new Thread(new Runnable() {
            @Override
            public void run() {
                assertEquals(1, flag);
                StringContainer s = new StringContainer("a");
                s.getSQLTable().query().close();
                assertEquals(1, flag);
                flag = 200;
            }
        }).start();

        SystemClock.sleep(1500);
        assertEquals(100, flag);
    }

    //ролбэк транзакции не откатывает изменения, сделанные конкурирующим потоком
    @Test
    public void parallelUpdate() throws Exception {

        final StringContainer str = new StringContainer("A");
        str.create();

        new Thread(new Runnable() { // запускается первый поток, в котором выполняются и откатываются изменения
            @Override
            public void run() {
                beginTransaction();
                StringContainer s = new StringContainer("FIRST");
                s.create();
                SystemClock.sleep(1000);
                rollbackTransaction();
            }
        }).start();

        SystemClock.sleep(100);

        new Thread(new Runnable() { // второй поток запускается чуть позже и блокируется до завершения первого
            @Override
            public void run() {
                str.setString("SECOND");
            }
        }).start();

        SystemClock.sleep(1500);
        str.reload();
        assertEquals(new StringContainer("SECOND"), str);
    }
}