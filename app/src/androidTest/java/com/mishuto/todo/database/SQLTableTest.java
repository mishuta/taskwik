package com.mishuto.todo.database;

import org.junit.Test;

/**
 * Тестирование методов SQLTable
 * Created by Michael Shishkin on 26.02.2017.
 */
public class SQLTableTest extends ORMBaseTest {
    @Test
    public void whereString() throws Exception {
        String where = "WHERE int=? and long=? and string=?";
        Integer p1 = 1;
        String p3 = " <xml = \"dd\": \\'qq' >  ";

        assertEquals("WHERE int=1 and long IS NULL and string=' <xml = \"dd\": \\''qq'' >  '", SQLHelper.whereString(where, p1, null, p3));
        assertEquals("WHERE int=21 and long=34 and string IS NULL", SQLHelper.whereString(where, 21, 34, null));
    }

    @Test
    public void createTable() {
        SQLTable sqlTable = new SQLTable("createTable_TEST", new SQLTable.Column("COL1", SQLTable.ColumnType.INTEGER, false), new SQLTable.Column("COL2", SQLTable.ColumnType.TEXT, false));

        sqlTable.getCV().put("COL1", 1);
        sqlTable.getCV().put("COL2", "createTable");
        sqlTable.insert();                   // AssertError, если таблица не создалась
        sqlTable.deleteTable();
    }

    @Test
    public void createIndex() {
        SQLTable sqlTable = new SQLTable("createIndex_TEST", new SQLTable.Column("COL1", SQLTable.ColumnType.INTEGER, true), new SQLTable.Column("COL2", SQLTable.ColumnType.TEXT, false));

        sqlTable.deleteTable();
    }

    @Test
    public void getDBCompatName() throws Exception {
        assertEquals("MONTH", DBUtils.getDBCompatName("month"));
        assertEquals("TEST", DBUtils.getDBCompatName("mTest"));
        assertEquals("S_ACCOUNT_NAME", DBUtils.getDBCompatName("sAccountName"));
        assertEquals("_SELECT", DBUtils.getDBCompatName("Select"));
    }
}