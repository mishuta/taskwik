package com.mishuto.todo.database;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

/**
 * Тестирование кеша
 * Created by Michael Shishkin on 13.11.2017.
 */
public class DBCacheTest {

    private static class TestString extends DBObject {
        String mString;

        TestString(String string) {
            mString = string;
        }

        TestString () {}
    }

    private static final DBCache sCache = DBCache.getInstance();

    //проверяем чтение сохраненного кешированного объекта А
    //меняем А, читаем его в В, убеждаемся что А==В, значение В изменено
    @Test
    public void loadCached() throws Exception {
        TestString a, b;
        a = new TestString("loadCached");
        a.create();
        a.mString = "loadCached_changed";

        b = DBObject.load(TestString.class, a.getRecordID());
        assertSame(a,b);
        assertEquals("loadCached_changed", b.mString);
    }

    //проверяем чтение сохраненного кешированного, но инвалидированного объекта А
    //инвалидируем А, меняем А, читаем его в В, убеждаемся что А==В, значение В НЕизменено
    @Test
    public void loadInvalidated() throws Exception {
        TestString a, b;
        a = new TestString("loadInvalidated");
        a.create();
        a.mString = "loadInvalidated_changed";
        a.invalidateCache();

        b = DBObject.load(TestString.class, a.getRecordID());
        assertSame(a,b);
        assertEquals("loadInvalidated", b.mString);
    }

    //проверяем чтение сохраненного некешированного объекта А
    //удаляем из кеша А, читаем А в В, убеждаемся что А!=В
    @Test
    public void loadNotCached() throws Exception {
        TestString a, b;
        a = new TestString("loadNotCached");
        a.create();
        sCache.remove(a);

        b = DBObject.load(TestString.class, a.getRecordID());
        assertNotSame(a,b);
    }

    // A->B, C->B. Делаем A.reload(), убеждаемся что A.B==C.B

    private static class DBCacheTest_A extends DBObject {
        StringContainer mStringContainer;
    }

    private static class DBCacheTest_C extends DBObject {
        StringContainer mStringContainer;
    }

    @Test
    public void reload() throws Exception {
        StringContainer b = new StringContainer("reload");
        DBCacheTest_A a = new DBCacheTest_A();
        a.mStringContainer = b;

        a.create();

        DBCacheTest_C c = new DBCacheTest_C();
        c.mStringContainer = b;

        c.create();

        a.reload();

        assertSame(a.mStringContainer, c.mStringContainer);
    }

    // clearDetach удаляет из кеша отсутствующие в БД элементы
    @Test
    public void clearDetach() throws Exception {
        HashMap<DBCache.CacheKey, DBCache.CacheElement> cacheMap = getCacheMap();
        DBCacheTest_A a = new DBCacheTest_A();
        a.mStringContainer = new StringContainer("clearDetach");
        a.create();
        long id1 = a.mStringContainer.getRecordID();

        Transactions.beginTransaction();
        a.mStringContainer = new StringContainer("clearDetach-2");                      // в транзакции создаем новый объект
        a.update();
        long id2 = a.mStringContainer.getRecordID();

        assertNotEquals(id1, id2);                                                      // проверяем что id у старого и нового объекта не совпадают
        assertNull(cacheMap.get(new DBCache.CacheKey(StringContainer.class, id1)));     // id1 уже удален из кеша
        assertNotNull(cacheMap.get(new DBCache.CacheKey(StringContainer.class, id2)));  // id2 есть в кеше

        Transactions.rollbackTransaction();                                                      // откатываем

        assertNotNull(cacheMap.get(new DBCache.CacheKey(StringContainer.class, id1)));
        assertNull(cacheMap.get(new DBCache.CacheKey(StringContainer.class, id2)));

    }

    private HashMap<DBCache.CacheKey, DBCache.CacheElement> getCacheMap() throws Exception {
        //noinspection unchecked
        return (HashMap<DBCache.CacheKey, DBCache.CacheElement>) TestUtils.Reflection.getField(DBCache.class, sCache, "mCache");
    }
}