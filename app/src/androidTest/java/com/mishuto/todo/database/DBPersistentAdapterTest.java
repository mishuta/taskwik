package com.mishuto.todo.database;

import org.junit.Test;

import static com.mishuto.todo.database.DBUtils.getDBCompatName;

/**
 * Тест Pesrsistent Adapter
 * Created by Michael Shishkin on 28.05.2018.
 */
public class DBPersistentAdapterTest extends ORMBaseTest {
    // тестовый класс для сохранения
    private static class PersistString implements DBPersistentAdapter.Persistable {
        String mString;
    }

    // PersistString создается и считывается из бд
    @Test
    public void create_read() {
        printTitle(this, "create_read");

        DBPersistentAdapter<PersistString> adapter = new DBPersistentAdapter<>(PersistString.class);
        DBPersistentAdapter.DBProxy<PersistString> proxy = adapter.getProxy();
        PersistString persistString = proxy.getPersist();

        persistString.mString = "a";
        proxy.create();

        persistString.mString = "";
        proxy.reload();

        assertEquals("a", persistString.mString);
    }

    //тестовый объект читается как синглтон-объект из бд и обновляется
    @Test
    public void loadSingleton_update() {
        printTitle(this, "loadSingleton_update");

        SQLTable sqlTable = new SQLTable(getDBCompatName(PersistString.class.getSimpleName()));
        if(sqlTable.doesTableExist())
            sqlTable.delete(null);

        DBPersistentAdapter<PersistString> adapter = new DBPersistentAdapter<>(PersistString.class);
        DBPersistentAdapter.DBProxy<PersistString> proxy = adapter.getProxy();
        PersistString persistString = proxy.getPersist();

        proxy.loadAsSingleton();
        persistString.mString="a";
        proxy.update();

        persistString.mString = "";
        proxy.loadAsSingleton();

        assertEquals("a", persistString.mString);
    }

    private static class DBPersistentContainer extends DBObject {
        DBPersistentAdapter<PersistString> mPersistent;
    }

    // DBObject - объект, который содержит в себе DBPersistentAdapter корректно реализует reload
    @Test
    public void outer_reload() {
        printTitle(this, "outer");
        DBPersistentContainer container = new DBPersistentContainer();
        container.mPersistent = new DBPersistentAdapter<>(PersistString.class);
        DBPersistentAdapter.DBProxy<PersistString> proxy = container.mPersistent.getProxy();
        proxy.getPersist().mString = "c";

        container.create();
        proxy.getPersist().mString = "z";
        container.reload();

        assertEquals("c", proxy.getPersist().mString);
    }
}