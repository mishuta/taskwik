package com.mishuto.todo.database.aggregators;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.ORMBaseTest;
import com.mishuto.todo.database.SQLTable;

import org.junit.Test;

import static com.mishuto.todo.database.aggregators.DBMap.ITEM_TABLE;
import static com.mishuto.todo.view_model.TestUtils.getObjectsCount;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsMapContaining.hasEntry;

/**
 * Тестирование DBMapObject
 * Created by Michael Shishkin on 02.03.2017.
 */
@SuppressWarnings("OverwrittenKey")
public class DBMapObjectTest extends ORMBaseTest {

    // тест операций создания/чтения
    @Test
    public void createNRead(){
        printTitle(this, "create");
        DBHashMap<Integer, StringContainer> map = new DBHashMap<>();
        map.create();
        map.put(1, new StringContainer("1"));
        map.put(2, new StringContainer("2"));

        assertObjectById(map, true);
        assertEquals(2, map.queryItems().countNClose());

        map.reload();

        assertThat(map, hasEntry(2, new StringContainer("2")));

        map.delete();
        assertCursors();
    }

    @Test
    public void deleteItem() {
        printTitle(this, "deleteItem");

        DBHashMap<Integer, StringContainer> map = new DBHashMap<>();
        map.create();
        map.put(1, new StringContainer("1"));
        map.put(2, new StringContainer("2"));

        StringContainer s = map.get(1);
        long id = s.getRecordID();
        map.remove(1);

        assertEquals(1, map.queryItems().countNClose());                // удален 1 объект
        assertEquals(0, s.getRecordID());
        assertEquals(0, s.getSQLTable().queryById(id).countNClose());   // удален именно этот объект

        assertCursors();
    }

    @Test
    public void clear() {
        printTitle(this, "delete");

        DBHashMap<Integer, StringContainer> map = new DBHashMap<>();
        map.create();
        map.put(1, new StringContainer("1"));
        map.put(2, new StringContainer("2"));

        map.clear();
        assertObjectById(map, true);                        // Объект есть
        assertEquals(0, map.queryItems().countNClose());    // а элементов - нет

        map.delete();
        assertCursors();
    }

    // тест апдейта value null -> String
    @Test
    public void updateItemValue_string()  {
        printTitle(this, "updateItemValue_string");

        DBHashMap<Integer, String> mapObject = new DBHashMap<>();
        mapObject.create();
        mapObject.put(1, null);
        mapObject.put(1, "test");

        mapObject.reload();

        assertEquals(mapObject.queryItems().countNClose(), 1);
        assertThat(mapObject, hasEntry(1, "test"));

        mapObject.delete();
        assertCursors();
    }

    // тест апдейта value StringContainer -> StringContainer
    @Test
    public void updateItemValue() {
        printTitle(this, "updateItemValue");

        DBHashMap<Integer, StringContainer> mapObject = new DBHashMap<>();
        mapObject.create();
        mapObject.put(1, new StringContainer("100"));
        mapObject.put(1, new StringContainer("1"));

        mapObject.reload();

        assertEquals(mapObject.queryItems().countNClose(), 1);
        assertThat(mapObject, hasEntry(1, new StringContainer("1")));

        mapObject.delete();
        assertCursors();
    }

    // тест апдейта value null -> StringContainer
    @Test
    public void updateItemValue_create() {
        printTitle(this, "updateItemValue_create");

        DBHashMap<Integer, StringContainer> mapObject = new DBHashMap<>();
        mapObject.create();
        mapObject.put(1, null);
        int oldCount = getObjectsCount(StringContainer.class);

        mapObject.put(1, new StringContainer("1"));

        mapObject.reload();

        assertEquals(oldCount + 1, getObjectsCount(StringContainer.class));   // стало на один StringContainer больше
        assertThat(mapObject, hasEntry(1, new StringContainer("1")));

        mapObject.delete();

        assertCursors();
    }

    // тест апдейта value StringContainer -> null
    @Test
    public void updateItemValue_delete()  {
        printTitle(this, "updateItemValue_delete");

        DBHashMap<Integer, StringContainer> mapObject = new DBHashMap<>();
        mapObject.create();
        mapObject.put(1, new StringContainer("1"));
        int oldCount = getObjectsCount(StringContainer.class);

        mapObject.put(1, null);

        assertEquals(oldCount - 1, getObjectsCount(StringContainer.class));   // стало на один StringContainer меньше
        assertThat(mapObject, hasEntry(1, null));

        mapObject.delete();

        assertCursors();
    }

    @Test
    public void checkNotCreatedMap() {
        DBHashMap<Integer, StringContainer> map = new DBHashMap<>();
        StringContainer s = new StringContainer("1");
        map.put(1, s);
        map.remove(1);
        assertThat(s.getRecordID(), is(0L));
        assertThat(map.getRecordID(), is(0L));
    }

    @Test
    public void deleteAfterReload()  {
        DBHashMap<StringContainer, StringContainer> map = new DBHashMap<>();
        map.create();
        map.put(new StringContainer("1"), new StringContainer("1"));
        map.reload();
        map.remove(new StringContainer("1"));

        assertEquals(0, map.queryItems().countNClose());
    }

    //талбица ITEM_TABLE может содержать элементы с одинаковыми ключами разных типов
    private static class TypeA extends DBObject { Integer a; }
    private static class TypeB extends DBObject { Integer b; }

    @Test
    public void deleteSameKeysDifferentTypes() {
        SQLTable itemTable = new SQLTable(ITEM_TABLE);
        int oldSize = itemTable.doesTableExist() ? itemTable.query().countNClose() : 0;
        //noinspection MismatchedQueryAndUpdateOfCollection
        DBHashMap<DBObject, Integer> map = new DBHashMap<>();
        map.create();

        TypeA a = new TypeA();  // создаем два объекта разных типов, причем RecordId после добавления в базу
        TypeB b = new TypeB();  // у них должны быть одинаковыми (1)

        map.put(a, 1);
        map.put(b, 1);

        assertThat(itemTable.query().countNClose() - oldSize, is(2));   // добавлено 2 элемента с одинаковыми ключами

        map.remove(a);
        assertThat(itemTable.query().countNClose() - oldSize, is(1));   // удалился только один элемент

        map.remove(b);
        assertThat(itemTable.query().countNClose() - oldSize, is(0));   // удалился второй элемент
    }

    // проверка согласования между DBTreeMap и RetrievableSet
    @Test
    public void dbTreeMap()  {
        DBTreeMap<StringContainer, Integer> map = new DBTreeMap<>();
        StringContainer s1 = new StringContainer("aaa");
        StringContainer s2 = new StringContainer("Aaa");

        map.create();
        map.put(s1, 1);
        assertSame(map.getKeyObject(s2), s1);
    }
}