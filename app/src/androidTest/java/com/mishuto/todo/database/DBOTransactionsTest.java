package com.mishuto.todo.database;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.database.aggregators.DBArrayList;
import com.mishuto.todo.view_model.TestUtils;

import org.junit.Test;

import java.util.Deque;
import java.util.Set;

import static com.mishuto.todo.database.Transactions.beginTransaction;
import static com.mishuto.todo.database.Transactions.commitTransaction;
import static com.mishuto.todo.database.Transactions.rollbackTransaction;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * Тесты управления транзакциями объектов
 * Created by Michael Shishkin on 16.02.2018.
 */
public class DBOTransactionsTest {

    private static class DBOTest extends DBObject {
        StringContainer str1;
        StringContainer str2;
        String mString;
    }

    private Set<DBObject> getChangedObjects() throws Exception {
        //noinspection unchecked
        Deque<Set<DBObject>> stack = (Deque<Set<DBObject>>)TestUtils.Reflection.getField(DBOTransactions.class, Transactions.getInstance(), "mChangedSetStack");
        return stack.peek();
    }

    // внутренние объекты откатываются, "не задевая" родителя
    @Test
    public void internalObjects() throws Exception {
        DBOTest dboTest = new DBOTest();
        dboTest.str1 = new StringContainer("A1");
        dboTest.str2 = new StringContainer("A2");
        dboTest.create();

        beginTransaction();
        dboTest.str1.setString("B1");
        dboTest.str2.setString("B2");
        assertThat(getChangedObjects(), hasSize(2));

        rollbackTransaction();

        assertEquals(new StringContainer("A1"), dboTest.str1);
        assertEquals(new StringContainer("A2"), dboTest.str2);

    }

    // родитель откатывается, "не задевая" детей
    @Test
    public void parentRollback() throws Exception {
        DBOTest dboTest = new DBOTest();
        dboTest.mString = "S";
        dboTest.create();

        beginTransaction();
        dboTest.mString="T";
        dboTest.update();
        assertThat(getChangedObjects(), hasSize(1));
        rollbackTransaction();

        assertEquals("S", dboTest.mString);
    }

    //корректное восстановление количества ссылок в новом объекте (уменьшение)
    @Test
    public void referenceCheckDec() throws Exception {
        DBOTest dboTest = new DBOTest();
        dboTest.str1 = new StringContainer("A");
        dboTest.create();                                   // объект А

        StringContainer other = new StringContainer("B");
        other.create();                                     // объект B

        beginTransaction();
        dboTest.str1 = other;
        dboTest.update();

        assertThat(other.getRefCounter(), is(2));           // на объект "В" ссылаются str1 и other, объект "A" удален
        assertThat(getChangedObjects(), hasSize(3));        // в обновленные попало 3 объекта: dboTest, В, A

        rollbackTransaction();

        assertThat(other, is(new StringContainer("B")));    // для объекта В...
        assertThat(other.getRefCounter(), is(1));           // восстановлена одна ссылка
        assertThat(dboTest.str1, is(new StringContainer("A"))); // восстановлен А
        assertThat(dboTest.str1.getRefCounter(), is(1));
    }

    //корректное восстановление количества ссылок в новом объекте (увеличение)
    @Test
    public void referenceCheckInc() throws Exception {
        DBOTest dboTest = new DBOTest();
        dboTest.create();

        StringContainer other = new StringContainer("B");
        other.create();

        dboTest.str1 = other;
        dboTest.update();

        assertThat(other.getRefCounter(), is(2));

        beginTransaction();
        dboTest.str1 = new StringContainer("A");
        dboTest.update();

        assertThat(other.getRefCounter(), is(1));

        rollbackTransaction();

        assertThat(other, is(new StringContainer("B")));
        assertThat(dboTest.str1, is(new StringContainer("B")));
        assertThat(other.getRefCounter(), is(2));
    }

    @Test
    public void toNull() throws Exception {
        DBOTest dboTest = new DBOTest();
        dboTest.create();

        beginTransaction();
        dboTest.str1 = new StringContainer("A");
        dboTest.update();

        assertThat(dboTest.str1.getRefCounter(), is(1));
        assertThat(getChangedObjects(), hasSize(2));

        rollbackTransaction();

        assertNull(dboTest.str1);
    }

    @Test
    public void toNotNull() throws Exception {
        DBOTest dboTest = new DBOTest();
        dboTest.str1 = new StringContainer("A");
        dboTest.create();

        beginTransaction();
        dboTest.str1 = null;
        dboTest.update();

        rollbackTransaction();

        assertEquals(new StringContainer("A"), dboTest.str1);
        assertThat(dboTest.str1.getRefCounter(), is(1));
    }

    // комитим внешнюю, отменяем внутреннюю
    @Test
    public void ExtCommitNestedRollback() throws Exception {
        DBOTest dboTest = new DBOTest();
        dboTest.mString = "S1";
        dboTest.str1 = new StringContainer("A");
        dboTest.create();

        beginTransaction();
        dboTest.mString = "S2";
        dboTest.str1 = new StringContainer("B");
        dboTest.update();

        beginTransaction();
        dboTest.mString = "S3";
        dboTest.str1 = new StringContainer("C");
        dboTest.update();

        rollbackTransaction();
        commitTransaction();

        assertEquals("S2", dboTest.mString);
        assertEquals(new StringContainer("B"), dboTest.str1);
    }

    // комитим внутреннюю, отменяем внешнюю
    @Test
    public void ExtRollbackNestedCommit() throws Exception {
        DBOTest dboTest = new DBOTest();
        dboTest.mString = "S1";
        dboTest.str1 = new StringContainer("A");
        dboTest.create();

        beginTransaction();
        dboTest.mString = "S2";
        dboTest.str1 = new StringContainer("B");
        dboTest.update();

        beginTransaction();
        dboTest.mString = "S3";
        dboTest.str1 = new StringContainer("C");
        dboTest.update();

        commitTransaction();
        rollbackTransaction();

        assertEquals("S1", dboTest.mString);
        assertEquals(new StringContainer("A"), dboTest.str1);
    }



    private static class DBOCollectionTest extends DBObject {
        DBArrayList<StringContainer> mStringContainers = new DBArrayList<>();
    }

    @Test
    public void collectionAdd() throws Exception {
        DBOCollectionTest dboCollectionTest = new DBOCollectionTest();
        dboCollectionTest.mStringContainers.add(new StringContainer("A"));
        dboCollectionTest.create();

        beginTransaction();
        dboCollectionTest.mStringContainers.add(new StringContainer("B"));
        rollbackTransaction();

        assertThat(dboCollectionTest.mStringContainers, hasSize(1));
        assertThat(dboCollectionTest.mStringContainers, hasItem(new StringContainer("A")));
    }

    @Test
    public void collectionToNull() throws Exception {
        DBOCollectionTest dboCollectionTest = new DBOCollectionTest();
        dboCollectionTest.create();

        beginTransaction();
        dboCollectionTest.mStringContainers.add(new StringContainer("A"));
        rollbackTransaction();

        assertThat(dboCollectionTest.mStringContainers, hasSize(0));
    }

    // проверка количества объектов, накапливающихся в стеке транзакций
    @Test
    public void checkNestedObjects() throws Exception {
        StringContainer s1 = new StringContainer("S1A");
        StringContainer s2 = new StringContainer("S2A");
        StringContainer s3 = new StringContainer("S3A");
        StringContainer s4 = new StringContainer("S4A");


        s1.create();
        s2.create();
        s3.create();
        s4.create();

        beginTransaction();     // 1
        s1.setString("S1B");
        s2.setString("S2B");
        assertThat(getChangedObjects(), hasSize(2));
        assertThat(getChangedObjects(), hasItems((DBObject)s1, s2));

        beginTransaction();     // 2
        s2.setString("S2C");
        s3.setString("S3C");
        assertThat(getChangedObjects(), hasSize(2));
        assertThat(getChangedObjects(), hasItems((DBObject)s2, s3));

        beginTransaction();     // 3
        s3.setString("_S3D");
        s4.setString("S4D");
        assertThat(getChangedObjects(), hasSize(2));
        assertThat(getChangedObjects(), hasItems((DBObject)s3, s4));

        commitTransaction();    // 3
        assertThat(getChangedObjects(), hasSize(3));
        assertThat(getChangedObjects(), hasItems((DBObject)s2, s3, s4));

        rollbackTransaction();  // 2
        assertThat(getChangedObjects(), hasSize(1));
        assertThat(getChangedObjects(), hasItem(s1));

        commitTransaction();    //1
    }
}