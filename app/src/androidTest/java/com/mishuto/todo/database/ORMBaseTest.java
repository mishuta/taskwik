package com.mishuto.todo.database;

import com.mishuto.todo.common.TSystem;

import org.junit.Assert;

/**
 * Базовый класс для всех тестов ORM. Содержит полезные статические методы
 * Created by Michael Shishkin on 24.02.2017.
 */

public class ORMBaseTest extends Assert {
    // проверка наличия (отсутствия) объекта в БД
    protected static void assertObjectById(DBObject object, boolean exists) {
        object.queryRecord();
        assertEquals(object.getSQLTable().getCursor().getCount(), exists ? 1 : 0);
        object.releaseCursor();
    }

    // проверка закрытия курсоров
    protected static void assertCursors() {
        assertEquals(0 , SQLHelper.DBCursor.getNumOpenCursors());
    }

    protected static void printTitle(Object o, String title) {
        TSystem.debug(o, "************************************** " + title + " **************************************");
    }
}
