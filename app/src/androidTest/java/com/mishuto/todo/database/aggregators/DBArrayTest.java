package com.mishuto.todo.database.aggregators;

import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.ORMBaseTest;
import com.mishuto.todo.database.SQLTable;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Тестирование DBArray
 * Created by Michael Shishkin on 19.03.2017.
 */
public class DBArrayTest extends ORMBaseTest {

    // создание тестового многомерного массива
    private static Integer[][][] init() {
        Integer[][][] array = new Integer[2][3][4];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 3; j++)
                for(int k = 0; k < 4; k++)
                    array[i][j][k] = i*100 + j * 10 + k;

        return array;
    }

    // тестовый класс для проверки массивов с элементами DBObject
    private static class TestArray extends DBObject {
        Integer mInteger;

        TestArray(Integer integer) {
            mInteger = integer;
        }

        TestArray() {}

        @Override
        public boolean equals(Object obj) {
            return ((TestArray)obj).mInteger.equals(mInteger);
        }

        @Override
        public TestArray clone() throws CloneNotSupportedException {
            return (TestArray) super.clone();
        }
    }



    @Test
    public void createArray() {
        printTitle(this, "Create DBArray");
        DBArray array = new DBArray(init());

        assertEquals(24, array.queryItems().countNClose());
    }

    @Test
    public void readArray() {
        printTitle(this, "Read DBArray");
        DBArray array = new DBArray(init());
        DBArray array1 = array.clone();

        array.reload();

        assertEquals(24, array.queryItems().countNClose());   // размер прочитанного массива должен быть равен 24
        assertTrue(array.equals(array1));                               // поэлементное сравнение массивов
    }

    //клонирование многомерного массива
    @Test
    public void cloneIntegerArray() throws Exception {
        printTitle(this, "cloneIntegerArray");
        DBArray array1 = new DBArray(), array2;

        array1.setArray(init());
        array2 = array1.clone();

        assertTrue(array1.equals(array2));                   // поэлементное сравнение массивов
    }

    // тест поэлементного сравнения массивов
    @Test
    public void equalArrays() throws Exception {
        printTitle(this, "equalArrays");
        Integer[] a1 = {100, 200, 300, 400};
        Integer[] a2 = {100, 200, 300, 400};
        DBArray array1 = new DBArray(), array2 = new DBArray();
        array1.setArray(a1);
        array2.setArray(a2);

        assertTrue(array1.equals(array2));
        a1[2] = 301;
        assertFalse(array1.equals(array2));
    }

    //клонирование массива DBObject
    @Test
    public void cloneDBObjectArray() throws Exception {
        printTitle(this, "cloneDBObjectArray");
        TestArray[] testArrays = {new TestArray(1), new TestArray(2), new TestArray(3)};
        DBArray array1 = new DBArray(), array2;

        array1.setArray(testArrays);
        array2 = array1.clone();

        assertNotSame(array1, array2);                          // объекты не должны совпадать
        assertNotSame(array1.getArray(), array2.getArray());    // массивы в объектах не должны совпадать

        for (int i = 0; i < testArrays.length; i++){
            assertEquals(testArrays[i], array2.getArray()[i]);  // объекты в массивах должны быть равны...
            assertNotSame(testArrays[i], array2.getArray()[i]); // но не должны совпадать
        }
    }

    // Создание пустого массива не должно падать
    @Test
    public void createNullArray() {
        printTitle(this, "createNullArray");
        String s1[][] = new String[3][5];

        DBArray array = new DBArray(s1);
        array.read(array.getRecordID());
    }

    // создание и чтение пустого объекта не должно падать
    @Test
    public void createAndReadEmptyObject() {
        printTitle(this, "EmptyObject");
        DBArray array = new DBArray();
        array.create();
        array.read(array.getRecordID());
    }

    // Тестирование массивов внутри объектов
    private static class Complex extends DBObject {
        DBArray mArray = new DBArray();
    }

    // создание объекта с массивом
    @Test
    public void createInternal() throws Exception {
        Integer[] arr =  {1,2,3};
        Complex complex = new Complex();
        complex.mArray.setArray(arr);
        complex.create();

        complex.reload();
        Integer[] arr1 = (Integer[]) complex.mArray.getArray();

        assertEquals(3, arr1.length);
    }

    // удаление объекта с массивом
    @Test
    public void deleteInternal() throws Exception {
        Integer[] arr =  {1,2,3};
        Complex complex = new Complex();
        complex.mArray.setArray(arr);
        complex.create();

        SQLTable colItemsTable = new SQLTable("ARRAY_ITEMS VALUES");
        int n = colItemsTable.query().countNClose();
        complex.delete();

        assertEquals(n-3, colItemsTable.query().countNClose());
    }


    // Тест создания большого массива
    @Ignore
    public void insertBigArray() {
        Integer[] arr = new Integer[1000];
        DBArray dbArray = new DBArray(arr);
    }
}