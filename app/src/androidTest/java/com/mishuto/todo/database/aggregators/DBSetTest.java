package com.mishuto.todo.database.aggregators;

import org.junit.Test;

import java.util.HashSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 * Created by Michael Shishkin on 12.11.2018.
 */
public class DBSetTest {

    //удаление после добавления в множество
    @Test
    public void addCollectionAndRemove() {
        DBHashSet<String> dbHashSet = new DBHashSet<>();
        dbHashSet.add("1");
        dbHashSet.add("2");

        dbHashSet.remove("1");

        assertThat(dbHashSet, hasSize(1));
    }

    // удаление после setCollection
    @Test
    public void setCollectionAndRemove() {
        HashSet<String> strings = new HashSet<>();
        strings.add("1");
        strings.add("2");

        DBHashSet<String> dbHashSet = new DBHashSet<>();
        dbHashSet.setCollection(strings);
        dbHashSet.remove("1");

        assertThat(dbHashSet, hasSize(1));

    }
}