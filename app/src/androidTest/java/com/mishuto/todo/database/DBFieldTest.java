package com.mishuto.todo.database;

import com.mishuto.todo.common.StringContainer;

import org.junit.Test;

import java.util.UUID;

/**
 * Тесты методов класса DBField
 * Created by Michael Shishkin on 06.03.2017.
 */
public class DBFieldTest extends ORMBaseTest {

    // Enum
    private static class TestEnum extends DBObject {
        enum Color { Red, Green, Brown }
        Color mColor;
    }


    @Test
    public void testEnums() {
        printTitle(this, "testEnums");

        TestEnum testEnum = new TestEnum();
        testEnum.mColor = TestEnum.Color.Brown;

        testEnum.create();
        testEnum.reload();

        assertEquals(testEnum.mColor, TestEnum.Color.Brown);
    }

    // UUID
    private static class TestUUID extends DBObject {
        UUID mUUID;
    }

    @Test
    public void testUUID()  {
        printTitle(this, "testUUID");

        UUID uuid = UUID.randomUUID();

        TestUUID testUUID = new TestUUID();
        testUUID.mUUID = uuid;

        testUUID.create();
        testUUID.reload();

        assertEquals(uuid, testUUID.mUUID);
    }

    // тест работы ORM с переменными общего типа

    private static class TestCommon extends DBObject {
        Object mObject;
    }

    // целое число сохраняется в Object
    @Test
    public void commonInteger() {
        TestCommon testCommon = new TestCommon();
        testCommon.create();
        Integer t = 1;
        testCommon.mObject = t;
        testCommon.update();
        testCommon.reload();
        assertEquals(t, testCommon.mObject);
    }

    // DBObject сохраняется в Object
    @Test
    public void commonDBObject() {
        TestCommon testCommon = new TestCommon();
        testCommon.create();
        StringContainer stringContainer = new StringContainer("abc");
        testCommon.mObject = stringContainer;
        testCommon.update();
        testCommon.reload();
        assertEquals(stringContainer, testCommon.mObject);
    }

    //Корректная работа при сохранении в переменную Boolean, а потом DBObject
    @Test
    public void commonBoolean2DBObject() {
        TestCommon testCommon = new TestCommon();
        testCommon.create();
        testCommon.mObject = true;
        testCommon.update();                                            // сохраняем в БД сначала Boolean
        StringContainer stringContainer = new StringContainer("cde");   // потом DBObject
        testCommon.mObject = stringContainer;
        testCommon.update();
        testCommon.reload();
        assertEquals(stringContainer, testCommon.mObject);              // переменная соджержит DBObject
        assertTrue(((DBObject)testCommon.mObject).getRecordID() > 0);   // DBObject создан
    }

    //Корректная работа при сохранении в переменную DBObject, потом String
    @Test
    public void commonDBObject2String() {
        TestCommon testCommon = new TestCommon();
        StringContainer stringContainer = new StringContainer("fgh");
        testCommon.mObject = stringContainer;
        testCommon.create();
        testCommon.mObject = "klm";
        testCommon.update();
        testCommon.reload();
        assertEquals("klm", testCommon.mObject);        // переменная содержит String
        assertEquals(0, stringContainer.getRecordID()); // объект DBObject удален
    }

    // после обнуления переменной тип корректно восстанавливается
    @Test
    public void commonEraseType() {
        TestCommon testCommon = new TestCommon();
        testCommon.mObject = "nop";
        testCommon.create();
        testCommon.mObject = null;
        testCommon.reload();
        assertEquals("nop", testCommon.mObject);
    }

    // null можно сохранить в Common тип
    @Test
    public void commonWithNull() {
        TestCommon testCommon = new TestCommon();
        testCommon.mObject = null;
        testCommon.create();
        testCommon.mObject = "a";
        testCommon.reload();
        assertNull(testCommon.mObject);
    }
}