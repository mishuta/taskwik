package com.mishuto.todo.common;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.mishuto.todo.common.TextUtils.itrToString;
import static org.junit.Assert.assertEquals;

/**
 * Тестирование TextUtils
 * Created by Michael Shishkin on 18.07.2017.
 */
public class TextUtilsTest {
    @Test
    public void uniqueStr() {
        String prefix = "Project";
        List<String> list = Arrays.asList("list", " project 1", "project ", "more", "Project2");

        String test = TextUtils.uniqueStr(list, prefix);
        assertEquals("Project 2", test);
    }

    @Test
    public void arrayToString() {
        String[] strings = {"aa", null, "bb"};
        assertEquals("aa, null, bb", itrToString(strings, ','));
        assertEquals("", itrToString((Object[])null, ','));
        assertEquals("", itrToString(new Object[0], ','));
    }
}