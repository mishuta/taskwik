package com.mishuto.todo.common;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Michael Shishkin on 20.04.2018.
 */
public class ArraySetTest {

    @Test
    public void addAll() {
        Integer[] arr = {1, 8, 2, 1, 8};
        ArraySet<Integer> set = new ArraySet<>();
        set.add(1);
        set.addAll(Arrays.asList(arr));
        assertThat(set.size(), is(3));
        assertThat(set, hasItems(1, 2, 8));
    }
}