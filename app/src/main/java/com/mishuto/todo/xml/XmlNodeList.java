package com.mishuto.todo.xml;

import android.support.annotation.NonNull;

import org.w3c.dom.NodeList;

import java.util.Iterator;

/**
 * Враппер NodeList для списка узлов XML-файла
 * Created by Michael Shishkin on 26.06.2018.
 */
public class XmlNodeList {
    private NodeList mNodeList;

    // интерфейс для поиска узла в списке
    public interface NodeMatcher {
        boolean match(XmlNode node);   // метод возращает true, если node - искомый элемент
    }

    // интерфейс объекта, создающегося из ноды
    public interface Element<T> {
        T createElement(XmlNode node);
    }

    public XmlNodeList(NodeList nodeList) {
        mNodeList = nodeList;
    }

    // найти узел по матчеру
    public XmlNode findFirst(NodeMatcher matcher) {
        Iterator<XmlNode> iterator = getIterable(matcher).iterator();
        return iterator.hasNext() ? iterator.next() : null;
    }

    // найти узел по имени
    public XmlNode findFirst(final String name) {
        return findFirst(new NodeMatcher() {
            @Override
            public boolean match(XmlNode node) {
                return node.getName().equals(name);
            }
        });
    }

    // получить итератор по всем объектам-узлов ноды, соответствующим матчеру.
    // Объекты создаются динамически при обходе списка. Если требуется многократный проход по итератору, рекомендуется закешировать объекты в отдельном списке
    public <T> Iterable<T> getIterable(final NodeMatcher matcher, final Element<T> element) {
        return new Iterable<T>() {
            @NonNull
            @Override
            public Iterator<T> iterator() {
                return new Iterator<T>() {
                    int index = 0;

                    @Override
                    public boolean hasNext() {
                        while (index < mNodeList.getLength() && !matcher.match(new XmlNode(mNodeList.item(index), index)))
                            index++;

                        return index < mNodeList.getLength();
                    }

                    @Override
                    public T next() {
                        return element.createElement(new XmlNode(mNodeList.item(index), index++));
                    }
                };
            }
        };
    }

    // получить итератор по всем узлам ноды, соответствующим матчеру.
    public Iterable<XmlNode> getIterable(NodeMatcher matcher) {
        return getIterable(matcher, new Element<XmlNode>() {
            @Override
            public XmlNode createElement(XmlNode node) {
                return node;
            }
        });
    }

    // получить итератор по всем объектам-узлов ноды из списка
    public <T> Iterable<T> getIterable(Element<T> element) {
        return getIterable(new NodeMatcher() {
            @Override
            public boolean match(XmlNode node) {
                return true;
            }
        }, element);
    }
}
