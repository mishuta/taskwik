package com.mishuto.todo.xml;

import org.w3c.dom.Node;

/**
 * Враппер Node для узла XML-файла
 * Created by Michael Shishkin on 26.06.2018.
 */
public class XmlNode {
    private Node mNode;
    private int mIndex;

    public XmlNode(Node node, int index) {
        mNode = node;
        mIndex = index;
    }

    public String getName() {
        return mNode.getNodeName();
    }

    public String getContent() {
        return mNode.getTextContent();
    }

    public String getAttrValue(String attrName) {
        Node attr = mNode.getAttributes().getNamedItem(attrName);
        return attr != null ? attr.getNodeValue() : null;
    }

    public XmlNodeList getChildNodes() {
        return new XmlNodeList(mNode.getChildNodes());
    }

    public int getIndex() {
        return mIndex;
    }
}
