package com.mishuto.todo.xml;

import com.mishuto.todo.App;

import org.w3c.dom.Document;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Хелпер для загрузки XML-файлов из raw-ресурса в структуру XmlNodeList
 * Created by Michael Shishkin on 10.01.2019.
 */
public class XmlDoc {

    private  Document mDocument;

    // параметр - id файла в виде R.raw.fileId
    public XmlDoc(int xmlFileId) {
        InputStream is = App.getAppContext().getResources().openRawResource(xmlFileId);
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            mDocument = builder.parse(is);
            is.close();
        }
        catch (Exception e){
            throw new Error(e);
        }
    }

    public XmlNodeList getNodeList(String tagName) {
        return new XmlNodeList(mDocument.getElementsByTagName(tagName));
    }

    public XmlNodeList getNodeList() {
        return new XmlNodeList(mDocument.getElementsByTagName("*"));
    }


}
