package com.mishuto.todo.database;

import android.database.Cursor;
import android.support.annotation.NonNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static com.mishuto.todo.common.TObjects.createInstance;
import static com.mishuto.todo.common.TObjects.eq;
import static com.mishuto.todo.common.TSystem.approveThat;
import static com.mishuto.todo.database.DBField.isDBObject;

/**
 * Класс для хранения объектов модели в БД. Обеспечивает ORM-интерфейс работы с БД, имплементирует CRUD
 * DBObject создает таблицу БД для каждого типа объектов, выполняет маппинг атрибутов объекта на строку БД.
 * Несохраняемые в БД атрибуты класса должны иметь модификатор transient.
 * static атрибуты не сохраняются в БД
 * Класс объявлен как abstract, поскольку не используется самостоятельно, а должен наследоваться сабклассами, которые будут храниться в БД
 *
 * Объекты кешируются после чтения или создания в БД. При запросе объекта из БД, если он есть в кеше, будет передана ссылка на объект из кеша
 *
 * Ограничения ORM:
 *
 * * Каждый класс объекта, сохраняемого в БД (кроме "простых" типов), должен наследоваться от DBObject либо использовать DBObject через DBPersistentAdapter.
 *
 * * Класс должен иметь дефолтный конструктор, т.к. при чтении из БД создается объект данного типа (либо должен быть переопределен метод read(id) в родителе, в котором
 * будут создаваться и читаться вложенные объекты)
 *
 * * Среди сохраняемых атрибутов класса не должно быть коллизий по названию атрибутов в суперклассе (затенения атрибутов),
 * поскольку все атрибуты суперклассов и сабклассов сохраняются в одной таблице
 *
 * * Все сохраняемые классы должны либо иметь разные названия (даже если они в разных пакетах) либо явно задавать имя таблицы при создании объекта в конструкторе
 *
 * * Объекты простых типов: Long, String, TCalendar, enum и.т.д сохраняются как копии. Возможные ссылки на них из других объектов не учитываются
 *
 * * Переменные примитивных типов long, int, boolean и т.д. не сохраняются в БД и должны быть заменены на соответствующие объекты.
 *
 * Created by Michael Shishkin on 06.02.2017.
 */

public abstract class DBObject implements Cloneable {
    private long mRecordId;                                                 // идентификтор строки БД (поле _ID таблицы объекта)
    SQLTable mSQLTable;                                                     // объект для работы с БД на уровне SQLTable-команд
    private HashMap<String, DBField> mFields = new HashMap<>();             // Map для хранения смаппированных на таблицу БД полей сабкласса

    private Integer mRefCounter = 0;                                        // счетчик ссылок на объект. Меняется при создании и удалении объекта
    private final static DBCache sCache = DBCache.getInstance();            // Кэш объектов DBObject
    private final static String REF_COUNTER_FIELD = "REFERENCE_COUNT";      // имя поля таблицы для хранения счетчика ссылок на объект

    // Конструктор по умолчанию. Должен быть обязательно для дюбых объектов, которые могут быть агрегированы другими сохраняемыми объектами, в том числе, для элементов коллекций.
    // Имя таблицы вычисляется из имени класса
    protected DBObject() {
        this(null);
    }

   // В конструкторе задается имя таблицы для хранения объекта
    protected DBObject(String tableName) {
        if(tableName == null)
            tableName = getClass().getSimpleName();

        mSQLTable = new SQLTable(tableName);
        bind(getClass(), DBObject.class, this); // выполняется связывание полей сабкласса с полями таблицы БД
        mSQLTable.setColumns(getColumns());
    }

    /**
     * Выполняет маппинг полей сабкласса и всех его предков на столбцы таблицы.
     * @param clazz класс объекта, в котором содержатся его поля
     * @param baseConstraint - класс-базовое ограничение (DBObject для наследующих DBObject объектов или Object для Persistable объектов
     * @param parent объект, в котором хранятся поля
     */
    <T> void bind (Class <?> clazz, Class<? super T> baseConstraint, T parent ) {
        if(clazz == baseConstraint) {       // базовое ограничение рекурсии
            mFields.put(REF_COUNTER_FIELD, new DBField(DBObject.class, this, "mRefCounter"));   // для каждого объекта хранится количество ссылок на него
            return;
        }
        else
            bind(clazz.getSuperclass(), baseConstraint, parent);                                // рекурсивный вызов для всех суперклассов объекта

        for(Field field : clazz.getDeclaredFields()) {
            if(!Modifier.isTransient(field.getModifiers()) && !Modifier.isStatic(field.getModifiers()))
                if(mFields.put(field.getName(), new DBField(parent, field, getSQLTable())) != null)  // проверка на отсутствие затененных атрибутов класса
                    throw new AssertionError("field '" + field.getName() + "' in super class is overrided by " + clazz.getSimpleName());
        }
    }

    // получение списка полей таблицы из списка DBField класса
    // вызывается из конструктора после bind()
    protected SQLTable.Column[] getColumns() {
        Set<SQLTable.Column> cols = new HashSet<>();

        for(DBField field : mFields.values()) {
            cols.add(new SQLTable.Column(field.getColumnName(), field.getDBColumnType(), false));

            if (field.getColumnTypeName() != null)  // для объекта DBObject создается второй столбец (см DBField.getDBObject())
                cols.add(new SQLTable.Column(field.getColumnTypeName(), SQLTable.ColumnType.TEXT, false));
        }

        return cols.toArray(new SQLTable.Column[cols.size()]);
    }

    /**
     * Создание объекта в таблице БД. Если таблицы нет, она создается автоматически. Все вложенные объекты создаются рекурсивно.
     * При попытке создать существующий объект, увеличивается счетчик ссылок на объект и возвращается его id
     * @return  _ID объекта в таблице
     */
    public long create() {
        mRefCounter++;                                      // каждый вызов create увеличивает счетчик ссылок на объект

        if(mRecordId != 0) {                                // если объект создан или в процессе создания
          updateRefCounter();                               // обновляем счетчик ссылок на объект в БД
          return mRecordId;                                 // и сразу возвращаем его id
        }

        setRecordID(mSQLTable.insertEmpty());               //Резервируем id в таблице еще до полного создания объекта, чтобы избежать бесконечной рекурсии в петлях

        for ( DBField field : mFields.values() ) {
            if(field.isDBObject() && field.getValue() != null)
                ((DBObject) field.getValue()).create();

            field.put();
        }

        updateRecord();

        onUpdate();
        return mRecordId;
    }

    /**
     * Чтение объекта из БД.
     * Не рекомендуется вызывать напрямую из клиента. Основной способ чтения объекта из БД - использование метода load(...)
     * @param id _ID объекта в таблице
     */
    public void read(long id) {
        readRecord(id);

        for ( DBField field : mFields.values() )
            readField(field);

        releaseCursor();
    }

    /** чтение конкретного поля из открытого курсора
     *  вызывается из read(id)
     *  служит для возможности переопределения в сабклассах стандартного поведения
     */
    @SuppressWarnings("WeakerAccess")
    protected void readField(DBField field) {
        field.setValue(field.get());
    }

    /**
     * Восстановление (откат изменения) объекта из БД
     * Восстанавливается только данный объект, и рекурсивно, все объекты, которые в него входят.
     */
    public final void reload() {
        invalidateCache();
        read(mRecordId);
    }

    /**
     * Читает строку таблицы БД, связанную с объектом по данному id и подготавливает курсор.
     * @param id _ID объекта в таблице
     */

    private void readRecord(long id) {
        if(id == 0)
            throw new AssertionError("trying to read object with zero id");

        setRecordID(id);

        if(!queryRecord())
            throw new AssertionError("Read error: object with id " + mRecordId + " not found in " + mSQLTable.getTableName());
    }

    /**
     * Обновление всех полей записи в таблице для текущего _ID. Нерекурсивная операция, поля-объекты DBObject должны обновляться самостоятельно.
     * Обычно данный метод используется в сеттерах объекта, прозрачно для манипулирующих им объектов.
     */
     public void update() {
        if(mRecordId == 0)                                          // Все наследники DBObject по умолчанию вызывают update() после каждого изменения. Но они не обязаны быть созданы в БД
            return;                                                 // Для этого случая update() является простой заглушкой

        for (DBField field : mFields.values() )
            if(prepareFieldUpdate(field.getCachedValue(), field.getValue()))
                field.put();

        if(mSQLTable.getCV().size() != 0) {                             // Если ни одно поле не обновилось, физическое обновление в БД не происходит
            updateRecord();                                             // update текущей записи
            onUpdate();
        }
    }

    /**
     * Метод вызываемый перед обновлением поля. Решает две задачи:
     * 1. Выполняет, при необходимости, удаление из БД старого объекта и создание нового, если объект DBObject
     * 2. Определяет изменилось ли значение поля, соответственно, нужен ли вообще update
     * @param oldVal  старое значение поля
     * @param newVal  новое значение
     * @return  true, если обновление требуется, false - если нет
     */
    protected static boolean prepareFieldUpdate(Object oldVal, Object newVal) {
         boolean isDBOOld = isDBObject(oldVal);
         boolean isDBONew = isDBObject(newVal);

         if(!isDBOOld && !isDBONew)         // если оба объекта не являются DBObject (в т.ч. если оба == null)
             return !eq(oldVal, newVal);    // обновление требуется только при их неравенстве

        if(isDBOOld && isDBONew &&          // если оба объекта ненулевые и являются DBObject
                ((DBObject)oldVal).getRecordID() == ((DBObject) newVal).getRecordID()   // и ссылаются на одну и ту же запись в БД
                && oldVal.getClass().equals(newVal.getClass()))                         // в одной таблице,
            return false;                   // то обновление не требуется

        if(isDBOOld)                        // удаляем старый DBObject
            ((DBObject)oldVal).delete();

         if(isDBONew )                      // создаем новый DBObject
             ((DBObject) newVal).create();

        return true;
    }

    // Обновляет строку БД, связанную с объектом
    void updateRecord() {
        if(mRecordId == 0)
            throw new AssertionError("trying to update record with zero _ID");

        if(mSQLTable.update(mRecordId) == 0)
            throw new AssertionError("id " + mRecordId + " not found in " + mSQLTable.getTableName());
    }


    private boolean isDeleteInProgress = false;  // флаг удаления объекта, для избегания повторного входа в метод при петлевых ссылках

    /**
     * Удаление текущего объекта из БД
     */
    public void delete() {
        if(isDeleteInProgress)                          //Проверка что объект не в процессе удаления (для петлевых ссылок)
            return;

        approveThat(mRecordId != 0, toString() + ": trying to delete the record with zero _ID");            // удаляемый объект должен иметь валидный id
        approveThat(mRefCounter >= 1, "An object " + toString() + " has " + mRefCounter + " references");   // у объекта должна быть хотя бы одна ссылка

        if(--mRefCounter - countLoopRefs() > 0 ) {      // физическое удаление происходит тогда и только тогда, когда есть ровно одна внешняя ссылка на объект,
            updateRefCounter();                         // не считая петлевых. Если количество ссылок > 1, то удаление просто уменьшает кол-во ссылок
            return;                                     // и обновляет счетчик ссылок на объет в БД
        }

        isDeleteInProgress = true;                      // Начало удаления. Устанавливаем флаг, защищающий от повторного удаления в петле

        for ( DBObject child : getChildren() )
            child.delete();                             // Рекурсивно удаляются все вложенные объекты

        deleteRecord();                                 // после чего удаляется текущая запись
        setRecordID(0);                                 // RecordId = 0
        mRefCounter = 0;                                // обнуляем количество ссылок (может быть не 0 в случаях с петлями)
        isDeleteInProgress = false;                     // сбрасываем флаг процесса удаления
        onUpdate();
    }

    // Удаляет строку БД, связанную с объектом
    private void deleteRecord() {
        if(mRecordId == 0)
            throw new AssertionError("trying to delete record with zero _ID");

        if(mSQLTable.delete(mRecordId) == 0)
            throw new AssertionError("id " + mRecordId + " not found in " + mSQLTable.getTableName());
    }

    public final long getRecordID() {
        return mRecordId;
    }

    // Обновление  RecordId (create, read, delete) с обновлением кеша
     private void setRecordID(long newId) {
        if(mRecordId == newId) {            // если RecordId при обращении к БД не изменяется
            sCache.validate(this, true);    // требуется только валидировать объект в кеше
            return;
        }

        approveThat(mRecordId * newId == 0);  // изменение RecordId возможно только 0 -> !0 (create, read) , либо !0 - > 0 (delete)
        approveThat(newId != 0 || mRefCounter - countLoopRefs() <= 0, "trying to delete the object that has a references");

        if(mRecordId != 0)
            sCache.remove(this);
        mRecordId = newId;

        if(newId != 0)
            sCache.put(this);
    }

    // возвращает счетчик ссылок на объект
    public Integer getRefCounter() {
        return mRefCounter;
    }

    // обновление счетчика ссылок на объект в базе
    private void updateRefCounter() {
        mFields.get(REF_COUNTER_FIELD).put();
        updateRecord();
        onUpdate();
    }

    // счечтик циклических ссылок на объект
    private int countLoopRefs() {
        class LoopCounter {                                                 // локальный класс - как абстракция направленного графа
            private HashMap<DBObject, Integer> mVisited = new HashMap<>();  // список посещенных узлов, содержит количество циклических ссылок на каждый узел

            private int loopsCount() {
                doCount(DBObject.this);
                return mVisited.get(DBObject.this);
            }

            private void doCount(DBObject dbObject) {                       // рекурсивный обход в глубину
                mVisited.put(dbObject, 0);                                  // сохраняем вершину в списке посещенных
                for (DBObject vertex : dbObject.getChildren())              // перебираем смежные вершины
                    if (mVisited.containsKey(vertex))                       // если вершина была посещена
                        mVisited.put(vertex, mVisited.get(vertex) + 1);     // увеличиваем счетчик циклических связей
                    else
                        doCount(vertex);                                    // иначе посещаем вершину
            }
        }
        return(new LoopCounter()).loopsCount();
    }


    // Освобождает курсор
    final void releaseCursor() {
        if(mSQLTable.getCursor() != null)
            mSQLTable.getCursor().close();
    }

    // запрос строки таблицы из БД, связанной с текущим объектом
    // возвращает false, если запись не найдена (или есть дубль, что невероятно)
    final boolean queryRecord() {
        Cursor cursor = mSQLTable.queryById(mRecordId);
        return cursor.getCount() == 1;
    }

    // запрос id для объекта-синглтона. Должен располагаться в единственной строке таблицы.
    // если объекта не существует, возвращается 0. Если в таблице больше одной строки, генерируется исключение
    private long getSingleTonId() {
        if(!mSQLTable.doesTableExist())
            return 0;

        long id = 0;
        mSQLTable.query();
        int count = mSQLTable.getCursor().getCount();

        if(count == 1)
            id = mSQLTable.getCursor().getLong(0);

        releaseCursor();

        if(count > 1 )
            throw new AssertionError("Singleton query found more then one result in the " + mSQLTable.getTableName());

        return id;
    }

    // загрузка синглтона в объект, или создание, если его нет в БД
    final public void loadAsSingleton() {
        long id = getSingleTonId();

        if(id == 0)
            create();
        else
            read(id);
    }

    public final SQLTable getSQLTable() {
        return mSQLTable;
    }

    @SuppressWarnings("unchecked")
    @Override
    public DBObject clone() throws CloneNotSupportedException {
        DBObject newObject =  (DBObject) super.clone();
        newObject.mFields = new HashMap<>();
        newObject.bind(getClass(), DBObject.class, this);
        newObject.mSQLTable = new SQLTable(mSQLTable.getTableName());
        newObject.mRecordId = 0;
        newObject.mRefCounter = 0;
        return newObject;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ":" + mRecordId;
    }

    // Сообщает объекту DBOTransaction о том, что объект изменился и нуждается в обратной синхронизации при ролбэке
    protected void onUpdate() {
       Transactions.getInstance().onChange(this);
    }

    // Сброс информации о привязке объекта к БД
    void reset() {
        mRefCounter = 0;
        if(mRecordId != 0)
            setRecordID(0);
    }

    //************* Методы для работы с кешом
    // рекурсивая инвалидация объекта и его дочерних объектов в кеше
    public void invalidateCache() {

        if(!sCache.isValid(this))                   // если объекта в кеше нет или он невалиден, выходим - это позволяет избежать зацикливания на петлевых ссылках
            return;

        sCache.validate(this, false);

        for ( DBObject child : getChildren() )    // рекурсивно помечаем все ссылки объекта как невалидные
            child.invalidateCache();
    }

    //попытка загрузки объекта сначала из кеша (если он там есть и валиден), а потом из БД (в ином случае).
    public static <T extends DBObject> T load(final Class<T> objectClass, long recordId) {
        return sCache.load(objectClass, recordId, new DBCache.DBObjectFactory<T>() {
            @Override
            public T create() {
                return createInstance(objectClass); // если объекта в кеше нет, для его создания вызывается дефолтный конструктор
            }
        });
    }

    //возвращает кешированный объект или null, если объекта нет в кеше
    protected static DBObject getCached(Class<? extends DBObject> objectClass, long recordId) {
        return sCache.get(objectClass, recordId);
    }

    // тип объекта, сохраняемого в БД
    // может отличаться от DBObject, если объект сохраняет не самого себя (DBProxy)
    Class<?> getStoredObjectType() {
        return getClass();
    }

    boolean isParentOf(final DBObject child) {

        class Trackman {
            private Set<DBObject> visited = new HashSet<>();

            private boolean traverse(DBObject object) {
                for(DBObject vertex : getChildren()) {
                    if (vertex.equals(child))
                        return true;

                    if(!visited.contains(vertex)) {
                        visited.add(vertex);
                        traverse(vertex);
                    }
                }

                return false;
            }
        }

        Trackman trackman = new Trackman();
        return trackman.traverse(this);
    }

    //Итератор по всем дочерним ненулевым DBObject
    private Iterable<DBObject> getChildren() {
        return new Iterable<DBObject>() {
            @NonNull
            @Override
            public Iterator<DBObject> iterator() {
                return new Iterator<DBObject>() {
                    private DBObject nextElement = null;
                    private boolean nextRead = false;
                    private Iterator<DBField> mFieldIterator = mFields.values().iterator();

                    @Override
                    public boolean hasNext() {
                        return (nextRead ?  nextElement  : next()) != null ;
                    }

                    @Override
                    public DBObject next() {
                        if(nextRead) {
                            nextRead = false;
                            return nextElement;
                        }

                        nextRead = true;
                        nextElement = null;

                        while (mFieldIterator.hasNext()) {
                            DBField field = mFieldIterator.next();
                            if(field.isDBObject() && field.getValue() != null) {
                                nextElement = (DBObject) field.getValue();
                                break;
                            }
                        }
                        return nextElement;
                    }
                };
            }
        };
    }
}
