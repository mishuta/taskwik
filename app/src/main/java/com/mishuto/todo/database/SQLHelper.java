package com.mishuto.todo.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;

import com.mishuto.todo.common.TextUtils;

import static android.database.DatabaseUtils.appendEscapedSQLString;
import static com.mishuto.todo.database.SQLDebug.CURSORS;
import static com.mishuto.todo.database.SQLDebug.SELECT;
import static com.mishuto.todo.database.SQLDebug.log;

/**
 * Вспомогательный класс для низкоуровневой работы с базой
 * Created by Michael Shishkin on 01.08.2018.
 */
public class SQLHelper {
    private DBOwner mDBOwner;

    // владелец БД
    // используется абстракция, поскольку SQLHelper должен работать с различными базами, а не только с объектом DBOpenHelper
    public interface DBOwner {
        SQLiteDatabase getOpenedDB();           // вернуть ссылку на открытую БД
    }

    // объект владельца инжектится в конструктор DBHelper.
    // Передать просто ссылку на БД недостаточно, поскольку один объект базы разделяется между множеством объектов. Если при этом, база изменит
    // состояние с open на close, то объект базы станет невалиден сразу во всех объектах.
    // Поэтому, владелец должен гарантированно возвращать открытую базу при каждом использовании
    public SQLHelper(DBOwner dbOwner) {
        mDBOwner = dbOwner;
    }

    SQLiteDatabase db() {
        return mDBOwner.getOpenedDB();
    }

    public void execRawSQL(String rawQuery) {
        log(rawQuery);
        db().execSQL(rawQuery);
    }

    // враппер для select к БД
    @SuppressWarnings("SameParameterValue")
    DBCursor query(String tableName, String[] cols, String whereClause, String[] whereArgs, String groupBy, String having, String orderBy) {
        log(SELECT,
                "SELECT " + (cols == null ? "*" : TextUtils.itrToString(cols, ';')) +
                " FROM " + tableName +
                (whereClause != null ? " WHERE " + whereClause : "") +
                (whereArgs != null ? " [" + TextUtils.itrToString(whereArgs, ';')  + "] " : "") +
                (groupBy != null ? " GROUP BY " + groupBy : "") +
                (having != null ? " HAVING " + having : "") +
                (orderBy != null ? " ORDER BY " + orderBy : ""));

        DBCursor cursor = new DBCursor(db().query(tableName, cols, whereClause, whereArgs, groupBy, having, orderBy));

        log(CURSORS, "found " + cursor.getCount() + " row(s).");
        if(DBCursor.getNumOpenCursors() > 1 )
            log(CURSORS, "There are " + DBCursor.getNumOpenCursors() + " open cursors");

        cursor.moveToFirst();
        return cursor;
    }

    // запрос на существование таблицы
    public boolean doesTableExist(String tableName) {
        int count = query("sqlite_master", null, whereString("type=? AND name=?", "table", tableName), null, null, null, null).countNClose() ;

        return count > 0;
    }

    /**
     * Преобразует строку и параметры WhereClause в строку для подстановки в запросы. В отличии от стандартного подхода whereClause/whereArgs распознает типы
     * (Long, Int, String, DBObject) и не кавычит целые значения как строки
     * @param whereClause строка условия с сигнатурой "=?" там, где должны быть параметры. Например: "COL_ID=? AND VALUE=?"
     * @param params Перечень параметров. Должен соответствовать сигнатурам "=?" в whereClause. Например: dbObject, "Man's"
     * @return строка для подстановки в запросы query, update, delete. Например: "COL_ID=21 AND VALUE="Man''s"
     */
    public static String whereString(String whereClause, Object ... params){
        StringBuilder buffer = new StringBuilder(); // результирующий буфер
        int current = 0;                            // текущий номер параметра

        for(int i = 0; i < whereClause.length(); i ++) {

            if(whereClause.charAt(i) == '?' ) {

                if (params[current] == null) {
                    buffer.deleteCharAt(buffer.length()-1);
                    buffer.append(" IS NULL");
                }
                else
                    switch (DBFieldType.getType(params[current].getClass())) {
                        case DB_OBJECT:
                            buffer.append(String.valueOf(((DBObject) params[current]).getRecordID()));
                            break;

                        case STORED_FIELD:
                            appendEscapedSQLString(buffer, ((DBField.StoredField) params[current]).asClauseParam());
                            break;

                        case INTEGER:
                        case LONG:
                            buffer.append(String.valueOf(params[current]));
                            break;

                        case STRING:
                            appendEscapedSQLString(buffer, (String) params[current]);
                            break;

                        case CLASS:
                            appendEscapedSQLString(buffer, ((Class)params[current]).getName());
                            break;

                        default:
                            throw new AssertionError("SQLTable.whereString: Class [" + params[current].getClass().getSimpleName() + "] is not supported");
                    }
                current++;
            }
            else
                buffer.append(whereClause.charAt(i));
        }

        return buffer.toString();
    }

    /**
     * Враппер для курсора - мониторит открытые курсоры
     * Created by Michael Shishkin on 13.02.2017.
     */
    @SuppressWarnings("WeakerAccess")
    public static class DBCursor extends CursorWrapper {

        private static int MAX_OPENED_CURSORS = 10; // ограничение количества открытых курсоров (либо где-то забыли закрыть, либо бесконечная рекурсия)
        private static int sCursors;

        DBCursor(Cursor cursor) {
            super(cursor);
            sCursors++;

            if(sCursors > MAX_OPENED_CURSORS)
                throw new AssertionError("There are too much opened cursors");
        }

        //вычисление количества строк в запросе с закрытием курсора. Используется в цепочке с query
        public int countNClose() {
            int n = getCount();
            close();
            return n;
        }

        @Override
        public void close() {
            if(isClosed())
                return;

            super.close();
            sCursors--;

            log(CURSORS, "cursor closed. " + sCursors + " cursor(s) left");
        }

        static int getNumOpenCursors() {
            return sCursors;
        }
    }
}
