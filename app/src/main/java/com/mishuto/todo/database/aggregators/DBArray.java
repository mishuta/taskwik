package com.mishuto.todo.database.aggregators;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.mishuto.todo.common.TObjects;
import com.mishuto.todo.database.DBField;
import com.mishuto.todo.database.SQLHelper;
import com.mishuto.todo.database.SQLTable;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import static com.mishuto.todo.database.DBField.getBlob;
import static com.mishuto.todo.database.SQLTable.ColumnType.INTEGER;
import static com.mishuto.todo.database.SQLTable.ColumnType.NUMERIC;
import static com.mishuto.todo.database.SQLTable.ColumnType.TEXT;

/**
 * Класс для хранения массивов любой размерности в БД.
 * Created by Michael Shishkin on 15.03.2017.
 */
// TODO: 03.11.2017 убрать избыточность хранения типов в таблице массивов и в таблице элементов массива
public class DBArray extends DBMultiObject {

    private final static String ARRAY_TABLE = "ARRAYS";         // Таблица объектов массивов
    private final static String ITEMS_TABLE = "ARRAY_ITEMS";    // Таблица элементов массивов


    private IntArray mSizes = new IntArray();                   // Размерности массива. Например для Integer[3][4] mSizes={3,4}
    private Class<?> mItemClass;                                // Класс объекта массива

    private transient Object[] mArray;                          // Собственно объект массив
    private transient Class[] mClasses;                         // кэширование классов каждой размерности. Например для Integer[3][4] mClasses={Integer[],Integer}

    @SuppressWarnings("WeakerAccess")
    public DBArray(Object[] array) {
        super(ARRAY_TABLE, itemsTable());
        store(array);
    }

    public DBArray() {
        super(ARRAY_TABLE, itemsTable());
    }

    // объект таблицы элементов массива
    private static SQLTable itemsTable() {
        return new SQLTable(ITEMS_TABLE,
                new SQLTable.Column(VALUE, NUMERIC, false),
                new SQLTable.Column(PARENT_ID, INTEGER, true),
                new SQLTable.Column(CLASS_TYPE, TEXT, false));
    }

    // Получить массив из объекта
    public Object[] getArray() {
        return mArray;
    }

    // Задать массив, без сохранения в БД
    // Чтобы потенциально не потерялась информация о старом массиве, в случае если объект уже связан с БД, бросается исключение
    public void setArray(Object[] array) {
        if(getRecordID() != 0)
            throw new AssertionError("Trying to set an array for object with a DB-reference");

        mArray = array;
        calculateArrayConfiguration();
    }

    // Перезаписать массив новым. Если у объекта еще не существовало массива, то он пересоздается.
    private void store(Object[] arr){
        if(getRecordID() != 0) {
            deleteAllItems();
            mArray = arr;
            calculateArrayConfiguration();
            createAllItems();
        }
        else {
            mArray = arr;
            calculateArrayConfiguration();
            create();
        }
    }

    @NonNull
    @Override
    public Iterator iterator() {
        return new Itr();
    }

    // Чтение массива из БД
    @Override
    public void read(long id) {
        super.read(id);         // читаем размерность

        if(mSizes == null)
            return;

        mClasses = new Class[mSizes.getArray().length];
        setClasses(0);

        SQLHelper.DBCursor cursor = queryItems();

        mArray = readArray(0, cursor);
        cursor.close();
    }


    // Рекурсивное чтение массива из курсора
    // n - текущая размерность: 0...mSizes.length - 1
    private Object[] readArray(int n, Cursor cursor) {

        Object[] arr = (Object[]) Array.newInstance(mClasses[n], mSizes.getArray()[n]);

        if (n == mSizes.getArray().length - 1) {

            for (int i = 0; i < mSizes.getArray()[n]; i++) {
                if(cursor.isAfterLast())
                    throw  new AssertionError("The array size mismatches to DB table");

                arr[i] = getItem(cursor);
                cursor.moveToNext();
            }
            return arr;
        }
        else {
            for (int i = 0; i < mSizes.getArray()[n]; i++)
                arr[i] = readArray(n+1, cursor);

            return arr;
        }
    }

    @Override
    public DBArray clone()  {
        try {
            DBArray clone = (DBArray) super.clone();
            if(mArray != null)
                clone.mArray = copyArray(mArray, 0);
            return clone;
        }
        catch(CloneNotSupportedException e) {
            throw new InternalError();
        }
    }

    // операция глубокого копирования массива. Вспомогательный метод для clone()
    private Object[] copyArray(Object[] srcArr, int n) {

        Object[] arr = (Object[]) Array.newInstance(mClasses[n], mSizes.getArray()[n]);

        if (n == mSizes.getArray().length - 1) {
            for(int i = 0; i < mSizes.getArray()[n]; i++)
                arr[i] = TObjects.clone(srcArr[i]);

            return arr;
        }
        else {
            for (int i = 0; i < mSizes.getArray()[n]; i++)
                arr[i] = copyArray((Object[]) srcArr[i], n+1);

            return arr;
        }
    }

    //рекурсивное вычисление класса для каждой размерности. Используется после чтения массива из БД
    private Class setClasses(int n) {
        if(n == mSizes.getArray().length - 1)
            mClasses[n] = mItemClass;
        else
            mClasses[n] = Array.newInstance(setClasses(n+1), 1).getClass();

        return mClasses[n];
    }

    //Вычисление mSizes и mClasses при инициализации DBArray новым массивом
    private void calculateArrayConfiguration() {
        ArrayList<Integer> dims = new ArrayList<>();
        ArrayList<Class<?>> classes = new ArrayList<>();

        Class clazz = mArray.getClass();
        Object arr = mArray;

        while(clazz.isArray() ) {
            clazz = clazz.getComponentType();
            classes.add(clazz);

            dims.add(((Object[])arr).length);
            arr = ((Object[])arr)[0];
        }

        mItemClass = clazz;

        mSizes.setArray(dims.toArray(new Integer[dims.size()]));
        mClasses = classes.toArray(new Class[classes.size()]);
    }

    // массивы равны, если их размеры, типы и элементы равны между собой
    @Override
    public boolean equals(Object obj) {
        DBArray other;
        if(obj instanceof DBArray)
            other = (DBArray) obj;
        else
            return false;

        //noinspection unchecked
        return Arrays.equals(mSizes.getArray(), other.mSizes.getArray()) &&
                Arrays.equals(mClasses, other.mClasses ) &&
                equalItems(other);
    }

    // хешкод объекта равен сумме хешкодов всех элементов (чтобы соответствовать equals)
    @Override
    public int hashCode() {
        int hash = 0;
        for (Object o : this)
            hash += o.hashCode();
        return hash;
    }

    // Класс, реализующий хранение одномерного массива целых чисел
    static class IntArray implements DBField.StoredField<IntArray> {
        Integer[] array;

        @Override
        public void put(String columnName, ContentValues cv) {
            if(array == null)
                cv.put(columnName, (Byte) null);

            else {
                ByteBuffer buffer = ByteBuffer.allocate(array.length * 4);
                for (Integer i : array)
                    buffer.putInt(i);

                cv.put(columnName, buffer.array());
            }
        }

        @Override
        public IntArray get(String columnName, Cursor cursor, Object parent) {
            ByteBuffer buffer = getBlob(columnName, cursor);
            array = new Integer[buffer.array().length / 4];
            for(int i = 0; i < array.length; i++)
                array[i] = buffer.getInt();

            return this;
        }

        @Override
        public String asClauseParam() {
            throw new Error("not supported");
        }

        @Override
        public Object clone()  {
            try {
                IntArray newObj = (IntArray) super.clone();
                if(array != null)
                    newObj.array = Arrays.copyOf(array, array.length);
                return this;
            }
            catch (CloneNotSupportedException e) {
                throw new Error(e);
            }
        }

        @Override
        public boolean equals(Object obj) {
            return obj != null && (obj instanceof IntArray) && Arrays.equals(array, ((IntArray)obj).array);
        }

        public Integer[] getArray() {
            return array;
        }

        public void setArray(Integer[] array) {
            this.array = array;
        }
    }

    /**
     * Итератор для возможности последовательного чтения многомерного массива, поскольку в таблице БД, многомерный массив хранится как одномерный
     */
    private class Itr implements Iterator {
        int curPos;     // текущая позиция итератора
        int limit = 1;  // размер одномерного массива (произведение всех размерностей многомерного массива)

        Itr() {
            if (mSizes.getArray() == null)
                return;

            for(int d: mSizes.getArray())
                limit *= d;
        }

        @Override
        public boolean hasNext() {
            return mSizes.array != null && curPos < limit;
        }

        @Override
        public Object next() {

            int index= -1;  // вычисленный индекс для текущего массива
            int k = limit;  // произведение всех размерностей массивов, ниже текущего
            int p = 0;      // сумма произведений индексов и размерностей массивов выше текущего

            Object[] arr = mArray;
            for (int i = 0; i < mSizes.getArray().length; i++) {
                k /= mSizes.getArray()[i];
                index = (curPos - p)/k;
                p += k * index;

                if( i < mSizes.getArray().length - 1 )
                    arr = (Object[]) arr[index];
            }

            curPos++;
            return arr[index];
        }
    }
}
