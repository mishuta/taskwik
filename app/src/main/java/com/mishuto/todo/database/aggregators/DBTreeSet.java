package com.mishuto.todo.database.aggregators;

import java.util.TreeSet;

/**
 * TreeSet, сохраняемый в БД
 * Created by Michael Shishkin on 14.10.2017.
 */

public class DBTreeSet<E> extends DBSet<TreeSet<E>, E> {
    public DBTreeSet() {
        super(new TreeSet<E>());
    }
}
