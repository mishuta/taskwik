package com.mishuto.todo.database.aggregators;

import java.util.TreeMap;

/**
 * HashMap, сохраняемый в базе
 * Created by Michael Shishkin on 07.03.2017.
 */

public class DBTreeMap<K,V> extends DBMap<K,V> {
    public DBTreeMap() {
        super(new TreeMap<K, V>());
    }
}
