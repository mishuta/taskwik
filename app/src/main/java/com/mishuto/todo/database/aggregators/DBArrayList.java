package com.mishuto.todo.database.aggregators;

import java.util.ArrayList;

/**
 * ArrayList, сохраняемый в базе
 * Created by Michael Shishkin on 07.03.2017.
 */

public class DBArrayList<T> extends DBList<ArrayList<T>, T> {
    public DBArrayList() {
        super(new ArrayList<T>());
    }
}
