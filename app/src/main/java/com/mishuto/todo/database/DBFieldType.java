package com.mishuto.todo.database;

import java.util.Calendar;
import java.util.UUID;

/**
 * Определение "элементарных" типов, сохраняемых в одном поле таблицы БД
 * Created by Michael Shishkin on 19.11.2017.
 */

enum DBFieldType {
    INTEGER, LONG, BOOLEAN, STRING, ENUM, CLASS, CALENDAR, _UUID, STORED_FIELD, DB_OBJECT,
    COMMON; // класс Object. Может принимать любой из заданных типов

    // Преобразование типа объекта в значение enum AllowableTypes
    // для удобства дальнейшего использования
    static DBFieldType getType(Class<?> type) {

        if (DBObject.class.isAssignableFrom(type))
            return DB_OBJECT;

        if(type == String.class)
            return STRING;

        if(type == Integer.class)
            return INTEGER;

        if(type == Long.class)
            return LONG;

        if(type == Boolean.class)
            return BOOLEAN;

        if(Calendar.class.isAssignableFrom(type))
            return CALENDAR;

        if(type == Class.class)
            return CLASS;

        if(Enum.class.isAssignableFrom(type))
            return ENUM;

        if(type == UUID.class)
            return _UUID;

        if (DBField.StoredField.class.isAssignableFrom(type))
            return STORED_FIELD;

        if(type == Object.class)
            return COMMON;

        throw new AssertionError("Type [" + type.getName() + "] does not supported.");
    }
}
