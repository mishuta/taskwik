package com.mishuto.todo.database.aggregators;

import java.util.Set;

/**
 * Реализация интерфейса Set, для сохраняемых в БД множеств
 * Created by Michael Shishkin on 07.03.2017.
 */

abstract class DBSet<S extends Set<E>, E> extends DBCollection<S, E> implements Set<E> {

    transient private RetrievableSet<E> mShadowSet; //теневая копия коллекции для получения реального объекта при удаленнии

    DBSet(S set) {
        super(set);
        mShadowSet = new RetrievableSet<>();
    }

    @Override
    public boolean add(E element) {
        if(!mCollection.contains(element))    //оптимизация метода super.add() - он не выполняется если объект уже есть в БД
            if(super.add(element)) {
                mShadowSet.put(element);      //сохранение объекта в теневой копии
                return true;
            }

        return false;
    }

    @Override
    public void setCollection(S collection) {
        super.setCollection(collection);
        mShadowSet.set(collection);
    }

    @Override
    public boolean remove(Object o) {
        if(!mShadowSet.contains(o))
            return false;

        removeElement(mShadowSet.remove(o));
        return true;
    }

    @Override
    public void read(long id) {
        super.read(id);
        mShadowSet.set(mCollection);
    }
}
