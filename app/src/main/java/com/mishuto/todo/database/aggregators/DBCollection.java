package com.mishuto.todo.database.aggregators;

import android.support.annotation.NonNull;

import com.mishuto.todo.common.TObjects;
import com.mishuto.todo.database.SQLHelper;
import com.mishuto.todo.database.SQLTable;

import java.util.Collection;
import java.util.Iterator;

import static com.mishuto.todo.database.SQLTable.ColumnType.INTEGER;
import static com.mishuto.todo.database.SQLTable.ColumnType.NUMERIC;
import static com.mishuto.todo.database.SQLTable.ColumnType.TEXT;

/**
 * Базовый класс для обеспечения хранения объектов-коллекций в БД.
 *
 * Created by Michael Shishkin on 08.02.2017.
 */

abstract class DBCollection <C extends Collection<E>, E> extends DBMultiObject<E> implements Collection<E> {

    private final static String COLLECTION_TABLE = "COLLECTIONS";   // Таблица объектов коллекций
    final static String ITEMS_TABLE = "COLLECTION_ITEMS";           // Таблица элементов коллекций
    final static String IDX = "_INDEX";                             // Поле таблицы элементов: Индекс элемента, используется в списках

    transient C mCollection;                                        // объект, содержащий коллекцию (или ее сабклассы)

    DBCollection(C collection) {
        super(COLLECTION_TABLE,
                new SQLTable(ITEMS_TABLE,
                    new SQLTable.Column(VALUE, NUMERIC, false),
                    new SQLTable.Column(PARENT_ID, INTEGER, true),
                    new SQLTable.Column(CLASS_TYPE, TEXT, false),
                    new SQLTable.Column(IDX, INTEGER, false)));

        mCollection = collection;
    }

    // чтение объекта коллекции и ее элементов из БД
    @Override
    public void read (long id)  {
        super.read(id);                                     // читаем объект коллекции
        SQLHelper.DBCursor cursor = queryItems();                 // курсор на список элементов коллекции
        mCollection.clear();                                // очищаем mCollection, на случай если там была предыдущая коллекция

        for(; !cursor.isAfterLast(); cursor.moveToNext())   // чтение элементов коллекции
            mCollection.add(getItem(cursor));               // получаем элемент и сохраняем его в коллекции

        cursor.close();
    }

    //Заменить текущую коллекцию на новую
    public void setCollection(C collection) {
        if(getRecordID() == 0)
            mCollection = collection;
        else {
            deleteAllItems();               // удаляем все элементы текущей коллекции
            mCollection = collection;
            createAllItems();               // создаем элементы коллекции с тем же id
        }
    }

    // клонирование объекта-коллекции
    @Override
    public DBCollection clone()  {
        DBCollection newObject;
        try{
            newObject = (DBCollection) super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new Error(e);
        }

        newObject.mCollection = getShallowCopy();
        return newObject;
    }

    // возвращает копию коллекции C (не DBCollection!) без копирования объектов
    public C getShallowCopy() {
        return TObjects.clone(mCollection);
    }

    /* ******************************
     * Методы интерфейса Collection *
     * *****************************/

    // Добавление элемента в коллекцию и сохранение его в БД
    @Override
    public boolean add(E e) {
        boolean result = mCollection.add(e);
        if(result)
            createItem(e);

        return result;
    }

    /*  Удаление элемента коллекции
        Метод должен быть определен в сабклассе, поскольку передаваемый в метод объект может быть равен тому что в коллекции, но при этом быть другим объектом:
        o1.equals(o2) && o1!=o2. Однако, из БД должен удаляться тот объект, который находится в коллекции.
        Поэтому в методе remove конкретного класса должен быть вызван метод removeElement c оригинальным объектом, находящимся в коллекции
    */
    @Override
    abstract public boolean remove(Object o);

    /** Удаление элемента коллекции. Вызывается из метода remove() конкрентой коллекции.
     * @param o удаляемый объект коллекции, должен быть не просто равен (o1.equals(o2)), а быть тем самым удаляемым объектом: o1==o2
     */
    void removeElement(E o) {
        if (mCollection.remove(o))
            deleteItem(o);
    }

    @Override
    public int size() {
        return mCollection.size();
    }

    @Override
    public boolean isEmpty() {
        return mCollection.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return mCollection.contains(o);
    }

    @NonNull
    @Override
    public Iterator<E> iterator() {
        return mCollection.iterator();
    }

    @NonNull
    @Override
    public Object[] toArray() {
        return mCollection.toArray();
    }

    @SuppressWarnings("SuspiciousToArrayCall") @NonNull
    @Override
    public <U> U[] toArray(@NonNull U[] a) {
        return mCollection.toArray(a);
    }

    @Override
    public boolean containsAll(@NonNull Collection<?> c) {
        return mCollection.containsAll(c);
    }

    @Override
    public boolean addAll(@NonNull Collection<? extends E> c) {
        notSupported();
        return mCollection.addAll(c);
    }

    @Override
    public boolean removeAll(@NonNull Collection<?> c) {
        notSupported();
        return mCollection.removeAll(c);
    }


    @Override
    public boolean retainAll(@NonNull Collection<?> c) {
        notSupported();
        return mCollection.retainAll(c);
    }

    @Override
    public void clear() {
        deleteAllItems();
        mCollection.clear();
    }

    @Override
    public int hashCode() {
        return super.hashCode() + mCollection.hashCode();
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object obj) {
        return mCollection.equals(obj);
    }

    static void notSupported() {
        throw new UnsupportedOperationException();
    }
}
