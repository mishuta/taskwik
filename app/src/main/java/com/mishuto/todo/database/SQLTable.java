package com.mishuto.todo.database;

import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.database.SQLHelper.DBCursor;

import static com.mishuto.todo.database.SQLDebug.DELETE;
import static com.mishuto.todo.database.SQLDebug.INSERT;
import static com.mishuto.todo.database.SQLDebug.SCHEMA;
import static com.mishuto.todo.database.SQLDebug.UPDATE;
import static com.mishuto.todo.database.SQLDebug.log;

/**
 * Класс инкапсулирует низкоуровневую работу с таблицей БД
 *
 * Created by Michael Shishkin on 10.02.2017.
 */
@SuppressWarnings({"UnusedReturnValue", "SameParameterValue"})
public class SQLTable {
    public static final String ID = "_ID";                          // имя ключевого автоинкрементного столбца

    private final String mTableName;                                // имя таблицы текущего объекта
    private ContentValues mContentValues = new ContentValues();     // ключевая пара: имя столбца таблицы-значение
    private DBCursor mCursor;                                       // курсор последнего query
    private Column[] mColumns;                                      // список полей таблицы
    private SQLHelper mSQLHelper;

    //Аффинированные типы столбцов таблицы
    public enum ColumnType {
        INTEGER, TEXT, NUMERIC, BLOB // BLOB преобразуется в NONE
    }

    // Объявление столбца таблицы
    public static class Column {

        private String mName;       // имя столбца
        private ColumnType mType;   // тип
        private boolean mIndexed;   // является ли индексируемым

        public Column(String name, ColumnType type, boolean indexed) {
            mName = name;
            mType = type;
            mIndexed = indexed;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            mName = name;
        }

        public ColumnType getType() {
            return mType;
        }

        public void setType(ColumnType type) {
            mType = type;
        }

        public boolean isIndexed() {
            return mIndexed;
        }

        public void setIndexed(boolean indexed) {
            mIndexed = indexed;
        }
    }

    public SQLTable(String tableName, Column... cols) {
        this(tableName);
        mColumns = cols;
    }

    public SQLTable(String tableName) {
        mTableName = DBUtils.getDBCompatName(tableName);
        mSQLHelper = new SQLHelper(DBOpenHelper.getInstance());
    }

    void setColumns(Column[] cols) {
        mColumns = cols;
    }
    
    public SQLiteDatabase db() {
        return mSQLHelper.db();
    }

    // враппер для SQLTable insert
    public long insert() {
        long id;

        assertCVNotNull("insert");

        log(INSERT, "INSERT INTO " + mTableName + " VALUES: ( " + mContentValues.toString() + " )");
        try {
            id = db().insertOrThrow(mTableName, null, mContentValues);
        }
        catch (SQLException e) {                                // Если во время SQLTable-запроса возник эксепшен, то
            createTable();                                      // вероятнее всего из-за того что таблицы еще нет. Создаем ее
            id = db().insert(mTableName, null, mContentValues); // и пытаемся повторить операцию
        }
        log(INSERT, "id=" + id);

        if(id==-1)
            throw new AssertionError("insertion failed");

        mContentValues.clear();

        return id;
    }

    // вставить пустую строку с дефолтовыми значениями (для _ID срабатывает автоинкремент)
    long insertEmpty() {

        String query = "INSERT INTO " + mTableName + " DEFAULT VALUES;";
        log(INSERT, query);

        try {
            db().execSQL(query);
        }
        catch (SQLException e) {
            createTable();
            db().execSQL(query);
        }

        DBCursor cursor = mSQLHelper.query(mTableName, new String[] {("max(" + ID + ")")}, null, null, null, null, null);       // запрашиваем max _ID
        long id = cursor.getLong(0);

        log(INSERT, "id=" + id);
        cursor.close();
        return id;
    }

    // select * from [mTableName] where _ID=[id]
    public DBCursor queryById(long id) {
        return query(ID + " = " + id);
    }

    // select * from [mTableName]
    public DBCursor query() {
        return query(null);
    }

    // select * from [mTableName] where [whereClause]
    public DBCursor query(String whereClause) {
        return query(whereClause, null);
    }

    // select * from [mTableName] where [whereClause] orderBy [orderBy]
    public DBCursor query(String whereClause, String orderBy) {
        try {
            mCursor = mSQLHelper.query(mTableName, null, whereClause, null, null, null, orderBy);
        }
        catch (SQLException e) {
            createTable();
            mCursor = mSQLHelper.query(mTableName, null, whereClause, null, null, null, orderBy);
        }

        return mCursor;
    }

    // враппер для SQLTable update
    int update(long id) {
        return update(ID + " = " + id);
    }

    public int update(String whereClause) {
        assertCVNotNull("update");

        log(UPDATE, "UPDATE " + mTableName + " SET " + mContentValues.toString() + " WHERE " + whereClause);

        int numRows = db().update(mTableName, mContentValues, whereClause, null);

        mContentValues.clear();
        log(UPDATE, numRows + " row(s) updated");

        return numRows;
    }

    // враппер для delete
    public int delete(String whereClause) {
        int numRows;
        log(DELETE, "DELETE FROM " + mTableName +  (whereClause != null ? " WHERE " + whereClause : ""));
        try {
            numRows = db().delete(mTableName, whereClause, null);   // попытка удаления из несуществующей таблицы допустима для таблиц, создаваемых напрямую
        }
        catch (SQLiteException e) {
            createTable();                                          // в этом случае таблица создается
            numRows = db().delete(mTableName, whereClause, null);   // и попытка удалить повторяется
        }
        log(DELETE, numRows + " row(s) deleted");

        return numRows;
    }

    // удаление строки с заданным id
    int delete(long id) {
        return delete(ID + " = " + id);
    }

    private void createTable() {
        if(doesTableExist())
            return;

        TSystem.approveThat(mColumns != null, "It's need to call a setColumns(...) before using SQLTable commands for table " + mTableName);

        StringBuilder sqlString = new StringBuilder("CREATE TABLE " + mTableName + " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT");

        for(Column column : mColumns)
            sqlString.append(", ").append(column.mName).append(" ").append(column.mType);

        sqlString.append(")");

        log(SCHEMA, sqlString.toString());
        db().execSQL(sqlString.toString());

        for(Column column : mColumns)
            if(column.mIndexed)
                createIndex(column.mName);
    }

    // удаляет таблицу и обновляет mTableExist
    public void deleteTable() {
        if(!doesTableExist())
            return;

        String sql = "DROP TABLE " + mTableName;
        log(SCHEMA, sql);
        db().execSQL(sql);
    }

    // создание индекса для столбца таблицы
    private void createIndex(String colName) {
        String sqlString = "CREATE INDEX IDX_" + (colName + "_OF_" + mTableName) + " ON " + mTableName + " ( " + colName + " )";
        log(SCHEMA, sqlString);
        db().execSQL(sqlString);
    }

    public ContentValues getCV() {
        return mContentValues;
    }

    DBCursor getCursor() {
        if(mCursor == null || mCursor.isClosed())
            return null;

        return mCursor;
    }

    public void setCursor(DBCursor cursor) {
        mCursor = cursor;
    }

    public String getTableName() {
        return mTableName;
    }

    public boolean doesTableExist() {
        return mSQLHelper.doesTableExist(mTableName);
    }

    private void assertCVNotNull(String command) {
        if(mContentValues.size() == 0)
            throw new AssertionError("Content Values did not set before " + command);
    }
}
