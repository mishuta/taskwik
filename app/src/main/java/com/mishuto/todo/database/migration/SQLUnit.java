package com.mishuto.todo.database.migration;

import android.database.sqlite.SQLiteDatabase;

import com.mishuto.todo.common.TSystem;

import static com.mishuto.todo.database.DBUtils.execSQLBatch;

/**
 * Скрипт миграции, состоящий только из заданного в res\raw SQL-скрипта
 * Created by Michael Shishkin on 17.09.2018.
 */
public class SQLUnit extends MigrationUnit {
    private int mSQLRes;

    public SQLUnit(int version, int SQLRes) {
        super(version);
        mSQLRes = SQLRes;
    }

    @Override
    public void migrate(SQLiteDatabase db) {
        TSystem.debug(this, "Executing SQL batch for db ver: " + getVersion());
        execSQLBatch(db, mSQLRes);
    }
}
