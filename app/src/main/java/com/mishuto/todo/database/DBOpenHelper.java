package com.mishuto.todo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mishuto.todo.App;
import com.mishuto.todo.database.migration.MigrationUnit;
import com.mishuto.todo.database.migration.SQLUnit;
import com.mishuto.todo.todo.R;

import java.util.Set;
import java.util.TreeSet;

/**
 * Класс для управления объектом БД
 * Created by Michael Shishkin on 05.02.2017.
 */

public class DBOpenHelper extends SQLiteOpenHelper implements SQLHelper.DBOwner {

    public static final String DB_NAME = "taskwik.db";
    private static final int DB_VERSION = 1010268;

    private static final Set<MigrationUnit> sMigrationUnits = new TreeSet<>();  // упорядоченное по номеру версии множество миграционных скриптов

    static {
        sMigrationUnits.add(new SQLUnit(10572, R.raw.migration_010572));
        sMigrationUnits.add(new SQLUnit(1010199, R.raw.migrate_01010199));
        sMigrationUnits.add(new SQLUnit(1010201, R.raw.migrate_01010201));
        sMigrationUnits.add(new SQLUnit(1010268, R.raw.migration_01010268));
    }

    private static DBOpenHelper sDBOpenHelper;

    // кастомный класс, определяющий ошибки в базе
    public class DBError extends Error {
        DBError(String msg) {
            super(msg);
        }
    }

    public static DBOpenHelper getInstance() {
        if(sDBOpenHelper == null)
            sDBOpenHelper = new DBOpenHelper(App.getAppContext());
        return sDBOpenHelper;
    }

    private DBOpenHelper(Context context) {
        super(context.getApplicationContext(), DB_NAME, null, DB_VERSION);
    }

    // во время открытия базы разрешаем WAL, чтобы не блокировать чтение во время длительных транзакций
    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.enableWriteAheadLogging();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Таблицы создаются динамически ORM при обращении к ним
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // кумулятивное применение скриптов миграции до последней версии
        for(MigrationUnit unit : sMigrationUnits)
            if(oldVersion < unit.getVersion())
                unit.migrate(db);
    }

    @Override
    public SQLiteDatabase getOpenedDB() {
        return getWritableDatabase();
    }

    @Override
    public synchronized void close() {
        if(Transactions.inTransaction())    // БД не должна закрываться, при наличии незакрытых транзакций. Это приведет к неопределенному состоянию файла базы
            throw new DBError("attempt to close the database while transaction is open.");

        super.close();
    }
}
