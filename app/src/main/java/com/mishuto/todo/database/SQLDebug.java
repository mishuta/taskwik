package com.mishuto.todo.database;

import com.mishuto.todo.common.TSystem;

import static com.mishuto.todo.common.TObjects.in;

/**
 * многоуровневое логирование SQL-команд
 * Created by Michael Shishkin on 09.12.2018.
 */
enum SQLDebug {
    // режимы вывода
    CURSORS,    // изменение открытых курсоров
    SCHEMA,     // изменение схемы: создание/удаление таблиц и индексов

    // SQL-команды
    SELECT,
    UPDATE,
    DELETE,
    INSERT,

    // групповой режим
    NONE,       // отключить логирование
    ALL,        // самое полное логирование
    READ,       // SELECT + MODIFY
    MODIFY;     // UPDATE + DELETE + INSERT

    private final static String PREFIX = "DATABASE";    // префикс-строка для лога
    private static SQLDebug sMode = MODIFY;             // дефолтный режим вывода

    static void setMode(SQLDebug mode) {
        sMode = mode;
    }

    static void log(SQLDebug mode, String s) {

        if(needLog(mode))
            TSystem.debug(PREFIX, s);
    }

    // логирование sql-запроса. Тип запроса определяется по его команде
    static void log(String query) {
        if(query.toLowerCase().startsWith("select"))
            log(SELECT, query);
        else if(query.toLowerCase().startsWith("insert"))
            log(INSERT, query);
        else if(query.toLowerCase().startsWith("update"))
            log(UPDATE, query);
        else if(query.toLowerCase().startsWith("delete"))
            log(DELETE, query);
        else
            throw new AssertionError("Unknown query: " + query);
    }


    private static boolean needLog(SQLDebug mode) {
        switch (sMode) {
            case NONE:      return false;
            case ALL:       return true;
            case READ:      return in(mode, SELECT, UPDATE, DELETE, INSERT);
            case MODIFY:    return in(mode, UPDATE, DELETE, INSERT);
            default:        return sMode == mode;
        }
    }
}
