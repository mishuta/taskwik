package com.mishuto.todo.database.aggregators;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * Класс, требующийся для хранения объектов коллекций с их последующим извлечением, поскольку сами коллекции не достают объекты, которые они хранят
 * Используется, например, для удаления элементов коллекции из БД, когда требуется удалить именно тот объект, который хранится в коллекции, а не равный ему (t1.equals(t2), но t1!=t2)
 * Created by Michael Shishkin on 16.08.2017.
 */

@SuppressWarnings("SuspiciousMethodCalls")
public class RetrievableSet<E> {
    private HashMap<E, E> mMap;

    public RetrievableSet() {
        mMap = new HashMap<>();
    }

    public RetrievableSet(Set<E> initialSet) {
        this();
        set(initialSet);
    }


    public void put(E e) {
       mMap.put(e, e);
   }

   public E remove(Object o) {
       return mMap.remove(o);
   }

   // инициация множества
   public void set(Set<E> c) {
       mMap.clear();
       for(E e: c) put(e);
   }

   public boolean contains(Object o) {
       return mMap.containsKey(o);
   }

   public void clear() {
       mMap.clear();
   }

   public E getElement(E e) {
       return mMap.get(e);
   }

   public Collection<E> getElements() {
       return mMap.values();
   }
}
