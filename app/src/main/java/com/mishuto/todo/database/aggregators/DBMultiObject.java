package com.mishuto.todo.database.aggregators;

import android.database.Cursor;

import com.mishuto.todo.database.DBField;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.SQLHelper;
import com.mishuto.todo.database.SQLTable;
import com.mishuto.todo.database.Transactions;

import java.util.Iterator;

import static com.mishuto.todo.common.TObjects.eq;
import static com.mishuto.todo.database.DBField.getColumnTypeName;
import static com.mishuto.todo.database.DBField.getType;
import static com.mishuto.todo.database.DBField.isDBObject;

/**
 * Базовый абстрактный класс для сохранения массивов и коллекций
 * Created by Michael Shishkin on 17.03.2017.
 */

abstract class DBMultiObject<E> extends DBObject implements Iterable<E> {

    // поля таблицы элементов объекта массива/коллекции
    final static String PARENT_ID = "PARENT_ID";                // Поле - FK на объект массива/коллекции
    final static String VALUE = "VALUE";                        // Поле, в котором хранится значение элемента (item) или его id, если item реализует DBObject
    final static String CLASS_TYPE = getColumnTypeName(VALUE);  // Поле, в котором хранится тип элемента

    transient SQLTable mItemTable;                              // Таблица элементов массива/коллекции


    DBMultiObject(String parentTableName, SQLTable itemTable) {
        super(parentTableName);
        mItemTable = itemTable;
    }

    // создание объекта массива/коллекции и его элементов в БД
    @Override
    public long create() {
        super.create();
        if(getRefCounter() == 1)    // создаем элементы только при физическом создании коллекции.
            createAllItems();       // При простом увеличении счетчика ссылок на коллекцию, счетчик ссылок на дочерние эдементы не должен увеличиваться
        return getRecordID();
    }

    // удаление объекта коллекции
    @Override
    public void delete() {
        if(getRefCounter() == 1)    // удаление или уменьшение счетчика ссылок элементов возникает только при физическом удалении коллекции
            deleteAllItems();       // удаляем все элементы
        super.delete();             // и запись об объекте массива/коллекции
    }

    // Запросить элемент из таблицы БД
   @SuppressWarnings("unchecked")
   public E getItem(Cursor cursor) {
       Class<?> type = DBField.getClass(CLASS_TYPE, cursor);
       return (E) DBField.get(VALUE, type, cursor, this);
   }

    // Запрос из БД всех элементов коллекции
    SQLHelper.DBCursor queryItems() {
        return mItemTable.query(PARENT_ID + "=" + getRecordID(), SQLTable.ID);
    }

    // создание элемента массива/коллекции в БД
    void createItem(E value) {
        if(getRecordID() == 0)  // элемент коллекции создается в базе, только если сама коллекция существует в БД
            return;

        if(isDBObject(value))
            ((DBObject)value).create();

        storeFields(value, getType(value));
        mItemTable.insert();

        onUpdate();
    }

    // сохранение информации об элементе перед вставкой.
    // оформленно в виде отдельного метода для возможности переопределения в сабклассах
    void storeFields(E value, Class<?> type) {
        DBField.put(PARENT_ID, getRecordID(), mItemTable.getCV());
        DBField.put(VALUE, value, type, mItemTable.getCV());
        DBField.put(CLASS_TYPE, type, mItemTable.getCV());
    }

    // удалить один элемент
    void deleteItem(Object o) {
        if ( getRecordID() == 0 )    // элемент удаляется из БД, только если коллекция сама есть в БД
            return;

        Object value = o;

        if(isDBObject(o) ){         // Если элемент коллекции- объект DBObject, тогда удаляем из БД сначала его
            DBObject dbObject = (DBObject)o;
            value = dbObject.getRecordID();
            dbObject.delete();
        }

        // удаляем запись об элементе коллекции
        mItemTable.delete(SQLHelper.whereString(VALUE + "=? AND " + PARENT_ID + "=? AND " + CLASS_TYPE + "=?", value, this, getType(o)));

        onUpdate();
    }

    // удалить все элементы
    void deleteAllItems() {
        if( getRecordID() == 0)     // элементы удаляются из БД, только если коллекция сама есть в БД
            return;

        for(E e : this)
            if(isDBObject(e))
                ((DBObject) e).delete();                    // Если коллекция/массив состоит из объектов DBObject, тогда удаляем из БД сначала объекты

        mItemTable.delete(PARENT_ID + "=" + getRecordID()); // один запрос на удаление всей коллекции/массива
    }

    // создать в БД все элементы коллекции
    void createAllItems() {
        Transactions.beginTransaction();

        for (E e : this)    // и каждый ее элемент
            createItem(e);

        Transactions.commitTransaction();
    }

    // поэлементное сравнение массива/коллекции
    boolean equalItems(Iterable<E> checked) {
        Iterator<E> itr = iterator();
        for(E e : checked)
           if(!itr.hasNext() || !eq(e, itr.next()) )
                return false;

        return true;
    }

    @Override
    public void invalidateCache() {
        super.invalidateCache();

        for(E e: this)
            if(isDBObject(e))
                ((DBObject)e).invalidateCache();
    }
}
