package com.mishuto.todo.database;

import java.util.HashMap;

/**
 * Класс для кэширования всех объектов DBObject, имеющих реальный RecordId.
 * Служит для ускорения чтения объектов из БД без обращения к самой БД и для гарантии ссылочной целостности на хранящиеся в БД объекты
 * Важный побочный эффект: позволяет не попасть в бесконечную рекурсию при чтении из БД в случае наличии петелевых ссылок друг на друга:
 * объект попадает в кеш, и при кольцевой ссылке на него, не читается опять из базы (что приведет к зацикливанию), а берется целиком из кеша.
 * В случае принудительного чтения объектов из БД, объекты не удаляются из кеша, а объявляются недействительными (с последующим перечитыванием и валидацией), чтобы не ломались ссылки
 * на объекты в кеше
 * Класс является вспомогательным для DBObject и не должен использоваться напрямую внешними клиентами (поэтому объявлен как пакетно-закрытый)
 * Created by Michael Shishkin on 12.11.2017.
 */

  class DBCache {

    private final static DBCache sInstance = new DBCache();             // синглтон объект содержит кеш всех объектов DBObject
    private HashMap<CacheKey, CacheElement> mCache = new HashMap<>();   // собственно кеш

    //фабрика, пораждающая объект DBObject
    //в методе load, в случае если объект не найден в кеше - он создается фабрикой.
    //Фабрика необходима для создания нестандартного объекта (например DBProxy<Persistable>)
    //метод DBObject.load(...) использует дефолтную реализацию (вызов дефолтного конструктора)
    interface DBObjectFactory<T extends DBObject> {
        T create(); // метод, создающий объект
    }

    static DBCache getInstance() {
        return sInstance;
    }

    // класс Ключа объекта в map
    static class CacheKey {
        long mRecordId;        // id объекта в БД
        Class<?> mObjectClass; // класс хранимого объекта (один класс = одна таблица БД). Может не быть DBObject (например, Persistable)

        CacheKey(Class<?> objectClass, long recordId) {
            mRecordId = recordId;
            mObjectClass = objectClass;
        }

        CacheKey(DBObject object) {
            mRecordId = object.getRecordID();
            mObjectClass = object.getStoredObjectType();
        }

        @Override
        public int hashCode() {
            return mObjectClass.hashCode() * 0x10000 + (short)mRecordId;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj == null || !(obj instanceof CacheKey))
                return false;

            CacheKey bro = (CacheKey) obj;

            return mObjectClass.equals(bro.mObjectClass) && mRecordId == bro.mRecordId;
        }

        @Override
        public String toString() {
            return mObjectClass.getSimpleName() + ":" + mRecordId;
        }
    }

    // класс элемента map
    static class CacheElement {
        DBObject mObject;       // собственно сохраняемый объект
        boolean isValid = true; // Валиден ли объект. Если нет, требуется перечитывание из БД

        CacheElement(DBObject object) {
            mObject = object;
        }
    }

    // добавление объекта в кеш
    // объект должен существовать в БД, и не должен уже храниться в кеше (даже в невалидном состоянии)
    void put(DBObject element ) {
        if(element.getRecordID() == 0)
            throw new AssertionError("trying to put an object that does not exist in the database");

        if( mCache.put(new CacheKey(element), new CacheElement(element)) != null )
            throw new AssertionError("trying to put to cache an existing object [" + element.getClass().getSimpleName() + "]. Possible causes: using read() instead of load(); trying to save existing object to DB ");
    }

    // запрос объекта из кеша. Возвращается валидный объект либо null
     DBObject get(Class<? extends DBObject> objectClass, long id) {
        CacheKey key = new CacheKey(objectClass, id);
        CacheElement value = mCache.get(key);
        return value == null || !value.isValid ? null : value.mObject;
    }

    // валидация элемента в кеше. Если элемент невалиден, при первом обращении к кешу, он будет перечитан из БД
    void validate(DBObject element, boolean validity) {
        mCache.get(new CacheKey(element)).isValid = validity;
    }

    void remove(DBObject element) {
        mCache.remove(new CacheKey(element));
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    boolean isValid(DBObject element) {
        CacheElement value = mCache.get(new CacheKey(element));
        return value != null && value.isValid;
    }

    // загрузка объекта.
    // Если объект есть в кеше и валиден - он сразу возвращается
    // Если объект в кеше есть, но невалиден, он перечитывается из БД и валидируется
    // Если объекта в кеше нет - он создается в фабрике и читается из БД
     <T extends DBObject> T load(Class<?> objectClass, long id, DBObjectFactory<T> factory) {
        T dbObject;
        CacheKey key = new CacheKey(objectClass, id);
        CacheElement value = mCache.get(key);

        if(value == null) {
            dbObject = factory.create();
            dbObject.read(id);
        }
        else {
            //noinspection unchecked
            dbObject = (T)value.mObject;

            if(!value.isValid)      // если объект в кеше невалиден
                dbObject.read(id);  // перечитывем
        }
        return dbObject;
    }
}
