package com.mishuto.todo.database;

import android.database.sqlite.SQLiteDatabase;

import com.mishuto.todo.App;
import com.mishuto.todo.common.TSystem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashSet;

/**
 * Библиотека вспомогательных статических методов для работы с БД
 * Created by Michael Shishkin on 21.02.2017.
 */

public class DBUtils {

    // Ключевые слова SQLite
    private static final String[] KEY_WORDS = new String[] {
            "", "ABORT", "ACTION", "ADD", "AFTER", "ALTER", "ANALYZE", "AND", "AS", "ASC", "ATTACH", "AUTOINCREMENT", "BEFORE", "BEGIN", "BETWEEN", "BY",
            "CASCADE", "CASE", "CAST", "CHECK", "COLLATE", "COLUMN", "COMMIT", "CONFLICT", "CONSTRAINT", "CREATE", "CROSS", "CURRENT_DATE", "CURRENT_TIME", "CURRENT_TIMESTAMP",
            "DATABASE", "DEFAULT", "DEFERRABLE", "DEFERRED","DELETE", "DESC", "DETACH", "DISTINCT", "DROP", "EACH", "ELSE", "END", "ESCAPE", "EXCEPT", "EXCLUSIVE", "EXISTS", "EXPLAIN",
            "FAIL", "FOR", "FOREIGN", "FROM", "FULL", "GLOB", "GROUP", "HAVING", "IF", "IGNORE", "IMMEDIATE", "IN", "INDEX", "INDEXED", "INITIALLY", "INNER", "INSERT", "INSTEAD",
            "INTERSECT", "INTO", "IS", "ISNULL", "JOIN", "KEY", "LEFT", "LIKE", "LIMIT", "MATCH", "NATURAL", "NO", "NOT", "NOTNULL", "NULL", "OF", "OFFSET", "ON", "OR", "ORDER", "OUTER",
            "PLAN", "PRAGMA", "PRIMARY", "QUERY", "RAISE", "RECURSIVE", "REFERENCES", "REGEXP", "REINDEX", "RELEASE", "RENAME", "REPLACE", "RESTRICT", "RIGHT", "ROLLBACK", "ROW",
            "SAVEPOINT", "SELECT", "SET", "TABLE", "TEMP", "TEMPORARY", "THEN", "TO", "TRANSACTION", "TRIGGER", "UNION", "UNIQUE", "UPDATE", "USING", "VACUUM", "VALUES", "VIEW", "VIRTUAL",
            "WHEN", "WHERE", "WITH", "WITHOUT"
    };

    // Set, содержащий ключевые слова SQLite
    private static final HashSet<String> sKeyWords = new HashSet<>();
    static {
        Collections.addAll(sKeyWords, KEY_WORDS);
    }

    // метод преобразует строку, например имя поля класса (Camel Name), в название столбца или таблицы. Например: "mDBMapObject" преобразуется в DBMAP_OBJECT
    public static String getDBCompatName(String camelName) {
        StringBuilder compatName = new StringBuilder();
        int i = 0;
        if(camelName.length() > 1 && camelName.charAt(0) == 'm' && Character.isUpperCase(camelName.charAt(1)))  // если первая буква m, а следующая заглавная - игнорируем
            i++;

        compatName.append(camelName.charAt(i++));

        for(; i < camelName.length(); i++) {
            if( Character.isUpperCase(camelName.charAt(i)) && !Character.isUpperCase(camelName.charAt(i-1)) && camelName.charAt(i-1) != '_')
                compatName.append("_");
            compatName.append(camelName.charAt(i));
        }

        if(sKeyWords.contains(compatName.toString().toUpperCase())) // если имя = ключевое слово,
            compatName.insert(0, "_");                              // то имя = _имя

        return compatName.toString().toUpperCase();
    }

    // метод исполняет sql-скрипт, находящийся в res\raw
    public static void execSQLBatch(SQLiteDatabase db, int sqlBatchId) {
        db.beginTransactionNonExclusive();
        try {
            InputStream is = App.getAppContext().getResources().openRawResource(sqlBatchId);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            StringBuilder sqlCommand = new StringBuilder("");           // буфер для одной SQL-команды

            while (reader.ready()) {                                    // пока не дошли до конца скрипта
                String line = reader.readLine();                        // читаем построчно

                if(line == null)                                        // еще один признак EOF
                    break;

                if(line.startsWith("--"))                               // комментарии игнорируем
                    continue;

                sqlCommand.append(" ").append(line);                    // остальное включаем в команду

                if(sqlCommand.toString().endsWith(";")) {               // если в конце строки ";" - значит это конец команды
                    sqlCommand.deleteCharAt(sqlCommand.length() - 1);   // удаляем ";"

                    TSystem.debug("execSQLBatch", "SQL EXEC: " + sqlCommand);
                    db.execSQL(sqlCommand.toString());                  // исполняем команду

                    sqlCommand = new StringBuilder("");                 // очищаем буфер
                }
                else if (sqlCommand.toString().contains(";"))
                    throw new AssertionError("error in " + line + "\n';' should always be at the end of the line");
            }
            reader.close();
            is.close();

            db.setTransactionSuccessful();
        }
        catch (IOException e) {
            throw new Error(e);
        }
        finally {
            db.endTransaction();
        }
    }
}
