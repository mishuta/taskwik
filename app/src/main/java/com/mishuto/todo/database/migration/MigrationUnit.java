package com.mishuto.todo.database.migration;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

/**
 * Абстрактный класс, задающий интерфейс для скриптов миграции, исполняемых в DBOpenHelper
 * Created by Michael Shishkin on 17.09.2018.
 */
public abstract class MigrationUnit implements Comparable<MigrationUnit> {
    private int mVersion;                               // номер версии БД, к которой применяется скрипт

    public abstract void migrate(SQLiteDatabase db);    // исполнение скрипта

    MigrationUnit(int version) {
        mVersion = version;
    }

    public int getVersion() {
        return mVersion;
    }

    @Override
    public int compareTo(@NonNull MigrationUnit o) {
        return mVersion - o.mVersion;
    }
}
