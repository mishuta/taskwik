package com.mishuto.todo.database.aggregators;

import android.support.annotation.NonNull;

import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.database.DBField;
import com.mishuto.todo.database.DBOpenHelper;
import com.mishuto.todo.database.SQLHelper;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Реализация интерфейса List для хранимого в базе объекта. Предназначен для сабклассирования конкрентыми списками (например, ArrayList)
 * Created by Michael Shishkin on 07.03.2017.
 */

abstract class DBList<L extends List<E>, E> extends DBCollection<L, E> implements List<E> {

    private transient int mStoredIndex = -1;  // индекс добавляемого элемента. -1 (по умолчанию), если элемент добавляется в конец списка

    DBList(L list) {
        super(list);
    }

    @Override
    public boolean remove(Object o) {
        if(!contains(o))
            return false;

        int removedIndex = indexOf(o);
        removeElement(get(removedIndex));
        updateIndexes(removedIndex, false);
        return true;
    }

    // сохранение индекса элемента при сохранении элемента в БД
    // вызывается из createItem(...)
    // если индекс не указан явно (index==-1), то IDX=size-1
    @Override
    void storeFields(E value, Class<?> type) {
        super.storeFields(value, type);
        if(mStoredIndex >= 0) {
            TSystem.approveThat(mStoredIndex < size());
            DBField.put(IDX, mStoredIndex, mItemTable.getCV());
            mStoredIndex = -1;
        }
        else
            DBField.put(IDX, size() - 1, mItemTable.getCV());
    }

    // список из БД запрашивается в порядке возрастания индексов
    @Override
    SQLHelper.DBCursor queryItems() {
        return mItemTable.query(PARENT_ID + "=" + getRecordID(), IDX);
    }

    /**
     * коррекция индексов элементов в БД, перед вставкой (после удаления) элемента в середину списка
     * @param from индекс элемента, начиная с которого модифицируются индексы в списке
     * @param inc true, если индексы увеличиваются (вставка), false если уменьшаются (удаление)
     */
    void updateIndexes(int from, boolean inc) {
        mStoredIndex = from;
        SQLHelper sqlHelper = new SQLHelper(DBOpenHelper.getInstance());
        if(sqlHelper.doesTableExist(mItemTable.getTableName())) {   // проверяем наличие таблицы, чтобы не упасть при апдейте в путой базе
            sqlHelper.execRawSQL(                                   // нельзя использовать SQLTable.update, поскольку в качестве параметра set передается не число, а формула
                    "update " + mItemTable.getTableName() +
                            " set " + IDX + "=" + IDX + (inc ? "+" : "-") + "1" +
                            " where " + SQLHelper.whereString(PARENT_ID + "=? AND " + IDX + ">=?", getRecordID(), from));
        }
    }

    @Override
    public boolean addAll(int index, @NonNull Collection<? extends E> c) {
        notSupported();
        return mCollection.addAll(index, c);
    }

    @Override
    public E get(int index) {
        return mCollection.get(index);
    }

    @Override
    public E set(int index, E element) {
        notSupported();
        return mCollection.set(index, element);
    }

    @Override
    public void add(int index, E element) {
        notSupported();
        mCollection.add(index, element);
    }

    @Override
    public E remove(int index) {
        notSupported();
        return mCollection.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return mCollection.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return mCollection.lastIndexOf(o);
    }

    // переопределение итератора для корректной работы iterator.remove()
    @Override @NonNull
    public ListIterator<E> listIterator() {
        return new ListIterator<E>() {
            ListIterator<E> mIterator = mCollection.listIterator();
            E mCurrent;

            @Override
            public boolean hasNext() {
                return mIterator.hasNext();
            }

            @Override
            public E next() {
                mCurrent = mIterator.next();
                return mCurrent;
            }

            @Override
            public boolean hasPrevious() {
                return mIterator.hasPrevious();
            }

            @Override
            public E previous() {
                mCurrent = mIterator.previous();
                return mCurrent;
            }

            @Override
            public int nextIndex() {
                return mIterator.nextIndex();
            }

            @Override
            public int previousIndex() {
                return mIterator.previousIndex();
            }

            @Override
            public void remove() {
                int removedIndex = indexOf(mCurrent);
                mIterator.remove();
                deleteItem(mCurrent);
                updateIndexes(removedIndex, false);
            }

            @Override
            public void set(E e) {
                notSupported();
            }

            @Override
            public void add(E e) {
                notSupported();
            }
        };
    }

    @NonNull
    @Override
    public ListIterator<E> listIterator(int index) {
        notSupported();
        return mCollection.listIterator(index);
    }

    @NonNull
    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return mCollection.subList(fromIndex, toIndex);
    }
}
