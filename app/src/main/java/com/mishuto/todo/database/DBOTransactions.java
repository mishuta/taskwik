package com.mishuto.todo.database;

import com.mishuto.todo.common.ArraySet;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Set;

import static com.mishuto.todo.common.TSystem.debug;
import static com.mishuto.todo.database.Transactions.inTransaction;

/**
 * Управление транзакциями для объектов DBObject
 * Цель класса - обеспечить автоматическую обратную синхронизацию изменений из БД в объект после ролбэков,
 * чтобы не перечитывать всю базу целиком, а только те объекты, которые изменились
 * Created by Michael Shishkin on 16.02.2018.
 */

public class DBOTransactions extends Transactions.BaseTransactions {

    private Deque<ArraySet<DBObject>> mChangedSetStack = new LinkedList<>();   // стек множеств измененных объектов внутри каждой транзакции

    // Объект вызывается из (саб)класса DBObject, чтобы сообщить транзакции о том, что объект был изменен и должен быть восстановлен после ролбэка
    void onChange(DBObject newObject)  {
        if(!inTransaction())
            return;

        mChangedSetStack.peek().add(newObject); // добавляем объект к множеству измененных объектов текущей транзакции
    }

    // при открытии транзакции, создаем пустое множество измененных объектов для транзакции
    @Override
    public void begin() {
        super.begin();
        mChangedSetStack.push(new ArraySet<DBObject>());
    }

    // Подтверждение транзакции.
    // Добавляем список измененных объектов подтверждаемой вложенной транзакции к списку внешней транзакции.
    @Override
    public void commit() {
        super.commit();
        ArraySet<DBObject> changedObjects = mChangedSetStack.pop(); // вытаскиваем из стека множество измененных на текущей транзакции объектов
        if(!mChangedSetStack.isEmpty())                             // если стек не пустой
            mChangedSetStack.peek().addAll(changedObjects);         // добавляем список измененных в этой транзакции объектов к внешней транзакции, чтобы их можно было отменить в ней
    }

    // откат транзакции. Восстанавливаем измененные объекты.
    // Вычитаем список восстановленных объектов подтверждаемой вложенной транзакции из списка измененных внешней транзакции.
    @Override
    public void rollback() {
        super.rollback();
        ArraySet<DBObject> changedObjects = mChangedSetStack.pop();
        sync(changedObjects);
        if(!mChangedSetStack.isEmpty())
            mChangedSetStack.peek().removeAll(changedObjects);
    }

    // изменялись ли данные с момента старта транзакции
    public boolean dataWasChanged() {
        return inTransaction() && !mChangedSetStack.peek().isEmpty();
    }

    // обратная синхронизация измененных объектов из БД
    private void sync(Set<DBObject> changedObjects) {

        // первый проход: отделяем несуществующие в БД объекты (удаляем их из кеша) и инвалидируем существующие
        for(DBObject dbObject : changedObjects   ) {
            boolean existsInDB = dbObject.queryRecord();
            dbObject.releaseCursor();

            if(!existsInDB) {                               // если после ролбэка было уничтожено соответствие между объектом в памяти и в БД (например, объект был создан в транзакции)
                debug(this, "reset: " + dbObject);
                dbObject.reset();                           // обнуляем ссылки, удаляем из кеша
            }
            else
                dbObject.invalidateCache();                 // иначе - инвалидируем кеш
        }

        // второй проход: перечитываем объекты из БД
        // двухпроходной алгоритм позволяет перечитывать каждый объект из базы только один раз
        // прочитанный объект валидирует кеш и не читается повторно при рекурсивной загрузке родительского объекта
        for(DBObject dbObject : changedObjects) {
            if(dbObject.getRecordID() != 0 && !DBCache.getInstance().isValid(dbObject)) {
                debug(this, "re-read: " + dbObject);
                dbObject.read(dbObject.getRecordID());
            }
        }
    }
}
