package com.mishuto.todo.database.aggregators;

import java.util.HashMap;

/**
 * HashMap, сохраняемый в базе
 * Created by Michael Shishkin on 07.03.2017.
 */

public class DBHashMap<K,V> extends DBMap<K,V> {
    public DBHashMap() {
        super(new HashMap<K,V>());
    }
}
