package com.mishuto.todo.database;

import android.content.ContentValues;
import android.database.Cursor;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TObjects;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.UUID;

import static com.mishuto.todo.common.TObjects.createInstance;
import static com.mishuto.todo.common.TObjects.in;
import static com.mishuto.todo.database.DBFieldType.CALENDAR;
import static com.mishuto.todo.database.DBFieldType.STORED_FIELD;

/**
 * Класс для маппинга поля класса модели на поле таблицы
 * Created by Michael Shishkin on 15.02.2017.
 */
@SuppressWarnings("WeakerAccess")
public class DBField {
    private final Object mParent;         // объект класса модели, который содержит данное поле (необязательно DBObject, если используется DBPersistantAdapter)
    private final Field mField;           // поле класса модели
    private final String mColumnName;     // нормализованное название столбца таблицы
    private final SQLTable mSQLTable;     // таблица, где храненится поле
    private Object mCachedValue;          // закешированное значение поле. Кеширование выполняется при записив /чтении из БД

    /*  Интерфейс, который должны реализовывать кастомные объекты, сохраняемые в одном поле таблицы,
        аналогично стандартным типам (String, Long и т.д).
        Напротив, объекты, реализующие DBObject сохраняются в отдельной таблице.
        StoredField в отличии от DBObject упрощает объекты, экономит ресурсы и кол-во обращений к БД.
        Но подходит только для самых простых классов, которые могут себя сконвертировать в стандартный сохраняемый тип.
        Не допускается одновременная реализация классом интерфейсов DBObject и StoredField
     */
    public interface StoredField<T extends StoredField> extends Cloneable {
        void put(String columnName, ContentValues cv);              // сохранение объекта в поле таблицы БД
        T get(String columnName, Cursor cursor, Object parent);     // получение объекта из поля таблицы БД
        String asClauseParam();                                     // преобразование объекта в параметр SQLTable-запроса where. Используетс в SQLTable.whereClause()
        boolean equals(Object object);                              // реализация equals для сравнения текущего значения с кешируемым
        Object clone();                                             // реализация клона для кеширования
    }

    /**
     * Основной конструктор
     * @param object Объект класса, для которого определено поле. Из/в него будут передаваться значения поля
     * @param field Поле класса
     */
    DBField(Object object, Field field, SQLTable sqlTable) {
        mField = field;
        mParent = object;
        mColumnName = DBUtils.getDBCompatName(field.getName());
        mSQLTable = sqlTable;

        mField.setAccessible(true);
    }

    /**
     * Конструктор для прямого создания объекта DBField по имени поля класса
     * @param parentClass класс, которому принадлежит поле
     * @param parent Объект класса, для которого определено поле. Из/в него будут передаваться значения поля
     * @param fieldName Имя поля класса
     */
     DBField(Class parentClass, DBObject parent, String fieldName) {
        this(parent, getFieldFromParent(parentClass, fieldName), parent.getSQLTable());
    }

    //получить поле из заданного класса по имени без возбуждения checked exception
    private static Field getFieldFromParent(Class parentClass, String fieldName) {
        try {
            return parentClass.getDeclaredField(fieldName);
        }
        catch (NoSuchFieldException e) {
            throw new Error(e);
        }
    }

    @Override
    public String toString() {
        return mColumnName;
    }

    // получение типа поля
    // если значение переменной null, возвращается декларируемый тип, иначе - реальный тип объекта
    private Class<?> getFieldType() {
        Object v = getValue();
        return v == null ? mField.getType() : v.getClass();
    }

    /**
     * метод помещает значение поля в ContentViews перед выполнением операции модификации (Create и Update)
     * объект поля может быть одного из разрешенных классов, либо наследоваться от DBObject
     */
     public void put() {
         if(mField.getType() != Object.class)
             put(mColumnName, setCachedValue(getValue()), getFieldType(), mSQLTable.getCV());
         else                                                                                       // для общего типа используется свой метод put - putCommon
             putCommon(mColumnName, setCachedValue(getValue()), getFieldType(), mSQLTable.getCV()); // он сначала сохраняет тип, а потом вызывает put
    }

    /**
     * метод извлекает объект из курсора
     * объект поля может быть одного из разрешенных классов.
     *
     * @return Объект, извлеченный из поля таблицы
     */
    public Object get() {
        return setCachedValue(get(mColumnName, getFieldType(), mSQLTable.getCursor(), mParent));
    }

    // запросить данные из поля
    Object getValue() {
        Object value;
        try {
            value = mField.get(mParent);
        }
        catch (IllegalAccessException e) {
            throw new Error(e);
        }
         return value;
    }

    // передать данные в поле
    void setValue(Object value) {
        try {
            mField.set(mParent, value);
        }
        catch (IllegalAccessException e) {
            throw new Error(e);
        }
    }

    Object getCachedValue() {
        return mCachedValue;
    }

    // сохранение значения поля в кеше и возврат оригинального объекта
    private Object setCachedValue(Object value) {
        // Если поле - immutable или DBObject, то кешируется оригинальный объект (DBObject апдейтится только если изменилась ссылка на объект)
        // В противном случае, для типов: STORED_FIELD и CALENDAR - кешируются клоны объектов, т.к. сами объекты могут изменяться
        mCachedValue = (value != null && in(DBFieldType.getType(getFieldType()), STORED_FIELD, CALENDAR)) ? TObjects.clone(value) : value;
        return value;
    }

    // Сохранить значение объекта в ContentView
    public static void put(String columnName, Object value, Class<?> valueClass, ContentValues cv) {
        if(value == null)
            put(columnName, (String)null, cv);

        else
            switch (DBFieldType.getType(valueClass)) {
                case STRING:
                    put(columnName, (String) value, cv);
                    break;

                case INTEGER:
                    put(columnName, (Integer) value, cv);
                    break;

                case LONG:
                    put(columnName, (Long) value, cv);
                    break;

                case BOOLEAN:
                    put(columnName, (Boolean) value, cv);
                    break;

                case CALENDAR:
                    put(columnName, (Calendar) value, cv);
                    break;

                case CLASS:
                    put(columnName, (Class<?>) value, cv);
                    break;

                case ENUM:
                    put(columnName, (Enum<?>) value, cv);
                    break;

                case _UUID:
                    put(columnName, (UUID) value, cv);
                    break;

                case DB_OBJECT:
                    put(columnName, (DBObject) value, cv);
                    break;

                case STORED_FIELD:
                    put(columnName, (StoredField) value, cv);
                    break;
            }
    }

    // Получить значение объекта из курсора
    @SuppressWarnings("unchecked")
    public static Object get(String columnName, Class<?> valueClass, Cursor cursor, Object parent) {
        if(cursor.isNull(cursor.getColumnIndexOrThrow(columnName)))
            return null;

        switch (DBFieldType.getType(valueClass)) {
            case STRING:
                return getString(columnName, cursor);

            case INTEGER:
                return  getInteger(columnName, cursor);

            case LONG:
                return getLong(columnName, cursor);

            case BOOLEAN:
                return getBoolean(columnName, cursor);

            case CALENDAR:
                return getCalendar(columnName, cursor);

            case CLASS:
                return getClass(columnName, cursor);

            case ENUM:
                return getEnum(columnName, (Class<? extends Enum>)valueClass, cursor);

            case _UUID:
                return getUUID(columnName, cursor);

            case DB_OBJECT:
                return getDBObject(columnName, cursor);

            case STORED_FIELD:
                return getStoredObject(columnName, (Class <StoredField<StoredField>>)valueClass, cursor, parent);

            case COMMON:
                return getCommon(columnName, cursor, parent);

            default: return null;   // unreachable
        }
    }

    // Набор методов put и get для конкретных классов, которые могут сохраняться в БД непосредственно

    //************ String
    public static String getString(String columnName, Cursor cursor) {
       return cursor.getString(cursor.getColumnIndexOrThrow(columnName));
    }

    public static void put(String columnName, String value, ContentValues cv) {
        cv.put(columnName, value);
    }

    //************ Integer
    public  static Integer getInteger(String columnName, Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(columnName));
    }

    public  static void put(String columnName, Integer value, ContentValues cv) {
        cv.put(columnName, value);
    }

    //************ Long
    public static Long getLong(String columnName, Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(columnName));
    }

    public Long getLong() {
        return getLong(mColumnName, mSQLTable.getCursor());
    }

    public static void put(String columnName, Long value, ContentValues cv) {
        cv.put(columnName, value);
    }

    //************ Boolean
    public static void put(String columnName, Boolean value, ContentValues cv) {
        put(columnName, value ? 1 : 0, cv);
    }

    public static Boolean getBoolean(String columnName, Cursor cursor) {
        return getInteger(columnName, cursor) == 1;
    }

    //************ Calendar
    public static Calendar getCalendar(String columnName, Cursor cursor) {
        return new TCalendar(getLong(columnName, cursor));
    }

    public static void put(String columnName, Calendar value, ContentValues cv) {
        put(columnName, value.getTimeInMillis(), cv);
    }

    //************ Class
    public static Class<?> getClass(String columnName, Cursor cursor) {
        String className = getString(columnName, cursor);

        if(className == null)
            return null;

        Class<?> clazz;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new Error("Object Class not found\n" + e.toString());
        }

        return clazz;
    }

    public static void put(String columnName, Class<?> value, ContentValues cv) {
        put(columnName, value.getName(), cv);
    }

    //************ Enum
    public static <T extends Enum<T>> T getEnum(String columnName, Class<T> clazz, Cursor cursor) {
        return Enum.valueOf(clazz, getString(columnName, cursor));
    }

    public static void put(String columnName,  Enum<?> value, ContentValues cv) {
        put(columnName, value.name(), cv);
    }

    //************ UUID
    public static UUID getUUID(String columnName, Cursor cursor) {
        ByteBuffer buffer = getBlob(columnName, cursor);
        long lsb = buffer.getLong();
        long msb = buffer.getLong();

        return new UUID(msb, lsb);
    }

    public static void put(String columnName, UUID value, ContentValues cv) {
        ByteBuffer buffer = ByteBuffer.allocate(16);
        buffer.putLong(value.getLeastSignificantBits());
        buffer.putLong(value.getMostSignificantBits());

        cv.put(columnName, buffer.array()); // сохраняем byte[16]
    }

    //************ StoredField
    public static <T extends StoredField> T getStoredObject(String columnName, Class<StoredField<T>> clazz, Cursor cursor, Object parent) {
        return createInstance(clazz).get(columnName, cursor, parent);
    }

    public static void put(String columnName, StoredField value, ContentValues cv) {
        value.put(columnName, cv);
    }

    /************ DBObject
     * для типа DBObject в таблице отводится два столбца:
     * columnName, где хранится id объекта
     * columnTypeName (вычисляемое), где хранится актуальный тип поля. В общем случае тип сохраняемого объекта может не совпадать с декларируемым и, следовательо, содержаться в другой таблице
     * сам объект хранится в отдельной таблице
     */
    public static DBObject getDBObject(String columnName, Cursor cursor) {
        long recordId = getLong(columnName, cursor);                // читаем из ячейки таблицы id объекта

        if(recordId == 0)
            return null;

        // читаем актуальный тип объекта, чтобы вычислить правильную таблицу
        //noinspection unchecked
        Class<? extends DBObject> clazz = (Class<? extends DBObject>) getClass(getColumnTypeName(columnName), cursor);

        return DBObject.load(clazz, recordId); // загружаем объект через кеш
    }

    public static void put(String columnName, DBObject value, ContentValues cv) {
        put(columnName, value.getRecordID(), cv);                   // сохраняем id
        put(getColumnTypeName(columnName), value.getClass(), cv);   // сохраняем тип объекта
    }

    //************ Common
    // общий тип, используется для хранения как DBObject, так и других разрешенных типов, например String
    // при сохранении объекта, сохраняется тип в отдельном столбце базы, после чего вызывается put
    // при запросе - сначала читается тип, а потом вызывается get()

    public static Object getCommon(String columnName, Cursor cursor, Object parent) {
        Class<?> clazz = getClass(getColumnTypeName(columnName), cursor);
        return get(columnName, clazz, cursor, parent);
    }

    public static void putCommon(String columnName, Object value, Class<?> type,  ContentValues cv) {
        put(columnName, value, type, cv);
        put(getColumnTypeName(columnName), getType(value), cv);
    }

    // может ли класс поля содержать объекты типа DBObject
    public static boolean isDBObjectType(Class<?>  fieldClass) {
        boolean result = DBObject.class.isAssignableFrom(fieldClass) || fieldClass == Object.class;

        // проверка, что класс не реализует одновременно DBObject и StoredField
        if(result && StoredField.class.isAssignableFrom(fieldClass))
            throw new AssertionError("Not allowed to inherit from the DBObject and StoredField at the same time");

        return result;
    }

    public static Class<?> getType(Object o) {
        return o == null ? Object.class : o.getClass();
    }

    // объект является DBObject, если у него тип DBObject и он не равен null
    public static boolean isDBObject(Object o) {
        return o != null && isDBObjectType(o.getClass());
    }

    // вычисление афинированного типа поля БД по классу объекта
    SQLTable.ColumnType getDBColumnType() {
            switch (DBFieldType.getType(getFieldType())) {
                case DB_OBJECT:
                case INTEGER:
                case LONG:
                case BOOLEAN:
                case CALENDAR:
                    return SQLTable.ColumnType.INTEGER;

                case STRING:
                case CLASS:
                case ENUM:
                    return SQLTable.ColumnType.TEXT;

                case _UUID:
                    return SQLTable.ColumnType.BLOB;

                case COMMON:
                case STORED_FIELD:
                    return SQLTable.ColumnType.NUMERIC;  // общие типы мапятся в Numeric

                default:
                    throw new AssertionError("undefined type");
            }
    }

    // является ли хранимый в поле объект производным от типа DBObject
    public boolean isDBObject() {
        return isDBObjectType(getFieldType());
    }

    public String getColumnName() {
        return mColumnName;
    }

    // вычисление названия столбца для сохранения реального типа объекта DBObject
    String getColumnTypeName() {
        return isDBObjectType(mField.getType()) ? getColumnTypeName(mColumnName) : null ;
    }

    // вычисление названия столбца для сохранения реального типа объекта DBObject
    public static String getColumnTypeName(String columnName) {
        return columnName + "_TYPE_";
    }

    // запрос поля Blob и сохранение его в ByteBuffer
    public static ByteBuffer getBlob(String columnName, Cursor cursor) {
        byte[] bytes = cursor.getBlob(cursor.getColumnIndexOrThrow(columnName));
        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        buffer.put(bytes);
        buffer.flip();
        return buffer;
    }
}
