package com.mishuto.todo.database;

import android.database.sqlite.SQLiteDatabase;

import com.mishuto.todo.common.TSystem;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Управление транзакциями SQLite
 * Поддерживается работа вложенных транзакций, поскольку де-факто андроид вложенные транзакции не реализует
 * Created by Michael Shishkin on 15.02.2018.
 */

public class Transactions {

    private final static String SP_NAME = "point_";             // префикс имени savepoint
    private final static int MAX_OPEN_TRANSACTIONS = 3;         // контроль открытых транзакций
    private static DBOTransactions sInstance;

    private static SQLiteDatabase db() {
        return DBOpenHelper.getInstance().getWritableDatabase();
    }

    public static void beginTransaction() {
        getInstance().begin();
    }

    public static void commitTransaction() {
        getInstance().commit();
    }

    public static void rollbackTransaction() {
        getInstance().rollback();
    }

    // проверка, находится ли БД в состоянии транзакции
    public static boolean inTransaction() {
        return !getInstance().mStack.isEmpty();
    }

    // завершение всех открытых транзакций
    public static void commitAll() {
        while (inTransaction())
            commitTransaction();
    }

    public static DBOTransactions getInstance() {
        if(sInstance == null)
            sInstance = new DBOTransactions();
        return sInstance;
    }

    static class BaseTransactions {

        private int pointNum;                                       // счетчик, обеспечивающий уникальность имен
        Deque<String> mStack = new LinkedList<>();          // стек транзакций. Хранит имена savepoint
        // открытие транзакции
        void begin() {
            if (mStack.size() > MAX_OPEN_TRANSACTIONS)
                throw new AssertionError("Too much transactions has opened");

            TSystem.debug(this, "trying to lock before start transaction ");
            lock(true);

            String pointName = SP_NAME + pointNum++;
            String command = "savepoint " + pointName;
            TSystem.debug(this, "[" + command + "]. Transaction " + mStack.size() + " has begun");
            db().execSQL(command);
            mStack.push(pointName);
        }

        //успешное завершение транзакции
        void commit() {
            if (mStack.isEmpty())
                throw new AssertionError("commit without begin");

            String command = "release " + mStack.pop();
            TSystem.debug(this, "[" + command + "]. Transaction " + mStack.size() + " has committed");
            db().execSQL(command);

            lock(false);
        }

        // откат транзакции
        public void rollback() {
            if (mStack.isEmpty())
                throw new AssertionError("rollback without begin");

            String command = ";rollback to " + mStack.pop();    // лидирующая ";" нужна, чтобы андроид не пытался парсить команду в свою транзакцию
            TSystem.debug(this, "[" + command + "]. Transaction " + mStack.size() + " has rolled back");
            db().execSQL(command);

            lock(false);
        }

        //блокировка(разблокировка) БД перед первой(после последней) внешней транзакцией
        //необходимо, чтобы другие потоки блокировались при попытке изменить базу во время транзакции
        private void lock(boolean needLock) {
            if (mStack.size() == 0)
                if (needLock)
                    db().beginTransactionNonExclusive();

                else {
                    db().setTransactionSuccessful();
                    db().endTransaction();
                }
        }
    }
}
