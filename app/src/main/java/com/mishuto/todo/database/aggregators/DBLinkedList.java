package com.mishuto.todo.database.aggregators;

import android.support.annotation.NonNull;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * LinkedList для хранения в БД
 * В отличии от обычного LinkedList, накладывается дополнительное ограничение на элементы списка: они должны реализовывать Comparable для того,
 * чтобы можно было восстанавливать требуемую последовательность при чтении из БД
 * Created by Michael Shishkin on 07.03.2017.
 */

public class DBLinkedList<T> extends DBList<LinkedList<T>, T> implements Deque<T>, Cloneable {
    public DBLinkedList() {
        super(new LinkedList<T>());
    }

    /******************************************/
    @Override
    public void addFirst(T t) {
        mCollection.addFirst(t);
        updateIndexes(0, true);
        createItem(t);
    }

    @Override
    public void addLast(T t) {
        mCollection.addLast(t);
        createItem(t);
    }

    @Override
    public boolean offerFirst(T t) {
        notSupported();
        return mCollection.offerFirst(t);
    }

    @Override
    public boolean offerLast(T t) {
        notSupported();
        return mCollection.offerLast(t);
    }

    @Override
    public T removeFirst() {
        if(size()==0)
            throw new NoSuchElementException();

        T element = getFirst();
        removeElement(element);

        return element;
    }

    @Override
    public T removeLast() {
        if(size()==0)
            throw new NoSuchElementException();

        T element = getLast();
        removeElement(element);

        return element;
    }

    @Override
    public T pollFirst() {
        notSupported();
        return mCollection.pollFirst();
    }

    @Override
    public T pollLast() {
        notSupported();
        return mCollection.pollLast();
    }

    @Override
    public T getFirst() {
        return mCollection.getFirst();
    }

    @Override
    public T getLast() {
        return mCollection.getLast();
    }

    @Override
    public T peekFirst() {
        return mCollection.peekFirst();
    }

    @Override
    public T peekLast() {
        return mCollection.pollLast();
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        notSupported();
        return mCollection.removeFirstOccurrence(o);
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        notSupported();
        return mCollection.removeLastOccurrence(o);
    }

    @Override
    public boolean offer(T t) {
        notSupported();
        return mCollection.offer(t);
    }

    @Override
    public T remove() {
        notSupported();
        return mCollection.remove();
    }

    @Override
    public T poll() {
        notSupported();
        return mCollection.poll();
    }

    @Override
    public T element() {
        return mCollection.element();
    }

    @Override
    public T peek() {
        return mCollection.peek();
    }

    @Override
    public void push(T t) {
        notSupported();
        mCollection.push(t);
    }

    @Override
    public T pop() {
        notSupported();
        return mCollection.pop();
    }

    @NonNull
    @Override
    public Iterator<T> descendingIterator() {
        return mCollection.descendingIterator();
    }
}
