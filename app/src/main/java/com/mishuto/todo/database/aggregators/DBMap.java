package com.mishuto.todo.database.aggregators;

import android.support.annotation.NonNull;

import com.mishuto.todo.database.DBField;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.SQLHelper;
import com.mishuto.todo.database.SQLTable;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import static com.mishuto.todo.common.TObjects.eq;
import static com.mishuto.todo.database.DBField.getColumnTypeName;
import static com.mishuto.todo.database.DBField.getType;
import static com.mishuto.todo.database.DBField.isDBObject;
import static com.mishuto.todo.database.SQLTable.Column;
import static com.mishuto.todo.database.SQLTable.ColumnType.INTEGER;
import static com.mishuto.todo.database.SQLTable.ColumnType.NUMERIC;
import static com.mishuto.todo.database.SQLTable.ColumnType.TEXT;
import static com.mishuto.todo.database.aggregators.DBCollection.notSupported;

/**
 * Класс-хелпер для храненя объектов-отображений (Map) в БД.
 * Используется совместно с классом-враппером конкретного отображения
 *
 * Created by Michael Shishkin on 08.02.2017.
 */

public abstract class DBMap<K, V> extends DBObject implements Map<K, V> {

    //названия таблиц
    private static final String MAP_TABLE = "MAP_OBJECTS";
    static final String ITEM_TABLE = "MAP_ITEMS";
    // поля таблицы MAP_ITEMS
    private final static String MAP_ID = "MAP_ID";
    private final static String KEY = "KEY";
    private final static String KEY_TYPE = getColumnTypeName(KEY);
    private final static String VALUE = "VALUE";
    private final static String VALUE_TYPE = getColumnTypeName(VALUE);

    private transient Map<K, V> mMap;               // объект класса-враппера конкретного отображения
    private transient RetrievableSet<K> mShadowSet; // теневая коллекция для хранения объектов ключей. Требуется для их корректного удаления
    private transient SQLTable itemsTable;          // таблица MAP_ITEMS

    // В конструкторе задается имя таблицы объекта отображения и сохраняется ссылка на объект класса-враппера конкретного Map
    DBMap(Map<K, V> map) {
        super(MAP_TABLE);
        mMap = map;
        mShadowSet = new RetrievableSet<>();

        itemsTable = new SQLTable(ITEM_TABLE,
                    new Column(KEY, NUMERIC, false),
                    new Column(KEY_TYPE, TEXT, false),
                    new Column(VALUE, NUMERIC, false),
                    new Column(VALUE_TYPE, TEXT, false),
                    new Column(MAP_ID, INTEGER, true));
    }

    // создание объекта Map и его элементов в БД
    @Override
    public long create() {
        super.create();

        for (Map.Entry<K, V> entry : entrySet())
           createItem(entry.getKey(), entry.getValue());

        return getRecordID();
    }

    // удаление объекта map
    @Override
    public void delete() {
        deleteAllItems();
        super.delete();
    }

    // удаление всех элементов из map
    private void deleteAllItems() {
        for(K key : keySet())
            deleteItem(key);
    }

    // апдейт элемента Map, когда меняется value при неизменном key
    private void updateItemValue(K key, V newValue) {

       if(!DBObject.prepareFieldUpdate(get(key), newValue))
           return;

        DBField.put(VALUE, newValue, getType(newValue), itemsTable.getCV());                    // Апдейтим ITEM_TABLE
        DBField.put(VALUE_TYPE, getType(newValue), itemsTable.getCV());
        itemsTable.update(SQLHelper.whereString(MAP_ID + "=? AND " + KEY + " =? AND " + KEY_TYPE + "=?", getRecordID(), key, key.getClass()));
    }

    // чтение объекта Map и его элементов из БД
    @SuppressWarnings("unchecked")
    @Override
    public void read (long id)  {
        super.read(id);                                     // запросить запись объекта отображения
        mMap.clear();                                       // очищаем предыдущее состояние

        SQLHelper.DBCursor cursor = queryItems();
        if(cursor == null)                                  // если таблицы не существует, значит объектов нет
            return;

        for(; !cursor.isAfterLast(); cursor.moveToNext()){  // для каждого элемента получаем ключ и значение Map из БД

            itemsTable.setCursor(cursor);
            Class<?> keyType = DBField.getClass(KEY_TYPE, cursor);
            Class<?> valType = DBField.getClass(VALUE_TYPE, cursor);

            K key = (K)DBField.get(KEY, keyType, cursor, this);
            V value = (V)DBField.get(VALUE, valType, cursor, this);

            mMap.put(key, value);                            // сохраняем key и value в Map
        }

        mShadowSet.set(mMap.keySet());                      // заполняем теневую копию ключами
        cursor.close();
    }

    // Запрос из БД всех элементов map
    SQLHelper.DBCursor queryItems() {
        if(!itemsTable.doesTableExist())
            return null;

        return itemsTable.query(MAP_ID + "=" + getRecordID());
    }

    // Метод вызывается при добавлении элемента в коллекцию (сохранение элемента в БД)
    private void add(K key, V value) {
        if(getRecordID() > 0)
            createItem(key, value);
    }

    // Удаление элемента map
    private void deleteItem(Object key) {
        if(getRecordID() == 0)
            return;

        Object keyObject = key;
        Object value = get(key);

        if(isDBObject(value))
            ((DBObject)value).delete();

        if(isDBObject(key)) {
            keyObject = ((DBObject)key).getRecordID();
            ((DBObject)key).delete();
        }

        itemsTable.delete(SQLHelper.whereString(MAP_ID + "=? AND " + KEY + "=? AND " + KEY_TYPE + "=?", getRecordID(), keyObject, key.getClass()));
    }


    // создание элемента коллекции в БД
    private void createItem(K key, V value) {
        Class<?> keyType = getType(key);
        Class<?> valType = getType(value);

        if(isDBObject(key) )
            ((DBObject)key).create();

        if(isDBObject(value) )
            ((DBObject)value).create();

        DBField.put(MAP_ID, getRecordID(), itemsTable.getCV());
        DBField.put(KEY_TYPE, keyType, itemsTable.getCV());
        DBField.put(VALUE_TYPE, valType, itemsTable.getCV());
        DBField.put(KEY, key, keyType, itemsTable.getCV());
        DBField.put(VALUE, value, valType, itemsTable.getCV());
        itemsTable.insert();
    }

    @Override
    public void invalidateCache() {
        super.invalidateCache();
        for(K k: keySet())
            if(isDBObject(k))
                ((DBObject)k).invalidateCache();

        for(V v: values())
            if(isDBObject(v) )
                ((DBObject)v).invalidateCache();
    }

    @Override
    public int hashCode() {
        return super.hashCode() + mMap.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && mMap.equals(obj);
    }

    // возвращает оригинальный объект ключа
    public K getKeyObject(K key) {
        return mShadowSet.getElement(key);
    }

    /* ***********************
     * Методы интерфейса Map *
     * ***********************/

    @Override
    public int size() {
        return mMap.size();
    }

    @Override
    public boolean isEmpty() {
        return mMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return mMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return mMap.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return mMap.get(key);
    }

    @Override
    public V put(K key, V value) {

        if(!containsKey(key)) {
            add(key, value);
            mShadowSet.put(key);
            return mMap.put(key, value);
        }
        else if(!eq(value, get(key))){
            updateItemValue(key, value);
            return mMap.put(key, value);
        }
        return value;
    }

    @Override
    public V remove(Object key) {
        //noinspection SuspiciousMethodCalls
        if(mMap.containsKey(key)) {
            deleteItem(mShadowSet.remove(key));
            return mMap.remove(key);
        }
        return null;
    }

    @Override
    public void putAll(@NonNull Map<? extends K, ? extends V> m) {
        notSupported();
        mMap.putAll(m);
    }

    @Override
    public void clear() {
        deleteAllItems();
        mMap.clear();
        mShadowSet.clear();
    }

    @NonNull
    @Override
    public Set<K> keySet() {
        return mMap.keySet();
    }

    @NonNull
    @Override
    public Collection<V> values() {
        return mMap.values();
    }

    /**
     * Поскольку метод mMap.entrySet().iterator().remove() (используется, например, в Executor.clearUnused())
     * не вызывает метод mMap.remove(object), необходимо первый переопределить, чтобы удалить объект из БД.
     * Для этого, метод создает анонимный класс Set (обертку), в котором делегируются методы mMap.entrySet(),
     * и обертку для Iterator, в которой переопределяется iterator().remove()
     */
    @NonNull
    @Override
    public Set<Entry<K, V>> entrySet() {
        return   new Set<Entry<K,V>>() {
                @Override
                public int size() {
                    return mMap.entrySet().size();
                }

                @Override
                public boolean isEmpty() {
                    return mMap.entrySet().isEmpty();
                }

                @Override
                public boolean contains(Object o) {
                    return mMap.entrySet().contains(o);
                }

                @NonNull
                @Override
                public Iterator<Entry<K, V>> iterator() {
                    return new Iterator<Entry<K, V>>() {
                        Iterator<Entry<K, V>> mIterator = mMap.entrySet().iterator();
                        K mKey;

                        @Override
                        public boolean hasNext() {
                            return mIterator.hasNext();
                        }

                        @Override
                        public Entry<K, V> next() {
                            Entry<K, V> entry = mIterator.next();
                            mKey = entry.getKey();
                            return entry;
                        }

                        @Override
                        public void remove() {
                            mIterator.remove();
                            deleteItem(mKey);
                        }
                    };
                }

                @NonNull
                @Override
                public Object[] toArray() {
                    return mMap.entrySet().toArray();
                }

                @NonNull
                @Override
                public <T> T[] toArray(@NonNull T[] a) {
                    //noinspection SuspiciousToArrayCall
                    return mMap.entrySet().toArray(a);
                }

                @Override
                public boolean add(Entry<K, V> kvEntry) {
                    return mMap.entrySet().add(kvEntry);
                }

                @Override
                public boolean remove(Object o) {
                    return mMap.entrySet().remove(o);
                }

                @Override
                public boolean containsAll(@NonNull Collection<?> c) {
                    return mMap.entrySet().containsAll(c);
                }

                @Override
                public boolean addAll(@NonNull Collection<? extends Entry<K, V>> c) {
                    return mMap.entrySet().addAll(c);
                }

                @Override
                public boolean retainAll(@NonNull Collection<?> c) {
                    return mMap.entrySet().retainAll(c);
                }

                @Override
                public boolean removeAll(@NonNull Collection<?> c) {
                    return mMap.entrySet().retainAll(c);
                }

                @Override
                public void clear() {
                    mMap.entrySet().clear();
                }
            };
    }
}
