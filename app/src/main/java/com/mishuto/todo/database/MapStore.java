package com.mishuto.todo.database;

import com.mishuto.todo.database.aggregators.DBHashMap;

/**
 * Класс, "якорящий" объекты DBObject.
 * Рекомендуемый кейс использования: в классе, не являющимся наследником DBObject, нужно иметь атрибут DBObject, причем массового типа (например StringContainer).
 * Т.е. такой объект, который иначе пришлось бы оборачивать в синглтон, единственной целью которого - сохранить себя в БД.
 * Чтобы не создавать такой синглтон, такие объекты привязываются в общее хранилище с доступом к ним по уникальному ключу.
 * Created by Michael Shishkin on 17.10.2018.
 */
public class MapStore extends DBObject {

    private static MapStore sInstance = new MapStore();
    private DBHashMap<String, DBObject> mMap = new DBHashMap<>();   // мапа ключ-значение

    private MapStore() {
        loadAsSingleton();
    }

    public static MapStore getInstance() {
        return sInstance;
    }

    /**
     * Попытка получить объект из хранилища по ключу. Если такой не существует - возвращается дефолтное значение, которое сохраняется в хранилище
     * @param parentClass класс родительского объекта, в котором находится запрашиваемый объект. Нужен для создания уникального ключа
     * @param varName имя переменной запрашиваемого объекта
     * @param def дефолтный объект, который сохраняется в хранилище и возвращается в случае, если объекта с таким ключом в хранилище еще нет
     * @return объект с заданным ключом, либо def
     */
    public <T extends DBObject> T get(Class<?> parentClass, String varName, T def) {
        String key = makeKey(parentClass, varName);
        if(mMap.containsKey(key))
            //noinspection unchecked
            return (T)mMap.get(key);
        else {
            mMap.put(key, def);
            return def;
        }
    }

    public void remove(Class<?> parentClass, String varName) {
        mMap.remove(makeKey(parentClass, varName));
    }

    private String makeKey(Class<?> clazz, String varName) {
        return clazz.getName() + "." + varName;
    }
}
