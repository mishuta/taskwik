package com.mishuto.todo.database;

import static com.mishuto.todo.common.TObjects.createInstance;

/**
 * Адаптер, позволяющий хранить в БД объекты, которые, по каким-то причинам не могут наследоваться от DBObject
 * Адаптер позволяет объектам наследоваться от других классов, но за это приходится расплачиваться более сложным интерфейсом взаимодействия
 * Чтобы сохранить в БД произвольный объект, требуется:
 * 1. Пометить класс сохраняемого объекта, как производный от Persistable
 * 2. создать объект DBPersistentAdapter, проинжектив в него класс Persistable-объекта
 * 3. DBPersistentAdapter создаст DBObject-объект DBProxy и собственно объект Persistable.
 * Persistable-объект обязан иметь дефолтный конструктор и выполнять те же ограничения что и DBObject
 * 4. Далее с объектом можно выполнять те же манипуляции что и с DBObject, получая доступ к нему через DBProxy
 *
 * Смысл класса DBPersistentAdapter - корректно создать DBProxy, т.к. последний не имеет дефолтного конструктора
 *
 * Если Persistable-объект является синглтоном, то создавать в базе объект DBPersistentAdapter нет необходимости. Достаточно вызвать DBPersistentAdapterObject.getProxy().loadAsSingleton()
 * В ином случае, DBPersistentAdapter сохраняется как обычный DBObject объект.
 * Пример:
 * class DBPersistentContainer extends DBObject {
 *         DBPersistentAdapter mAdapter;
 * }
 *
 * Created by Michael Shishkin on 28.05.2018.
 */
public class DBPersistentAdapter<T extends DBPersistentAdapter.Persistable> extends DBObject {

    /*
    Маркирующий интерфейс
    Объекты Persistable должны удовлетворять тем же ограничениям что и объекты, наследующие DBObject:
    закрытый перечень допустимых типов, дефолтный конструктор.
    Несохраняемые атрибуты должны иметь модификаторы transient или static
     */
    public interface Persistable {}

    private Class<T> mPersistObjectClass;   // класс хранимого объекта
    private DBProxy<T> mProxy;              // Прокси хранимого объекта

    // вызывается клиентом для создания в базе Persistable-объекта
    public DBPersistentAdapter(Class<T> persistableClass) {
        mPersistObjectClass = persistableClass;
        T persistable = createInstance(mPersistObjectClass);
        mProxy = new DBProxy<>(persistable);
    }

    // вызывается автоматически при восстановлении объекта из DB
    private DBPersistentAdapter() {}

    public DBProxy<T> getProxy() {
        return mProxy;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void readField(DBField field) {
        if(field.getColumnName().equals(DBUtils.getDBCompatName("mProxy"))) {   // обработка создания объекта mProxy
            long recordId = field.getLong();
            mProxy = DBCache.getInstance().load(mPersistObjectClass, recordId, (DBCache.DBObjectFactory<DBProxy>) () -> {
                T persistable = createInstance(mPersistObjectClass);
                return  new DBProxy<>(persistable);
            });
        }
        else
            super.readField(field);
    }

    public static class DBProxy<T extends Persistable> extends DBObject {
        private transient T mPersistObject; // сохраняемый объект

        DBProxy() {
            throw new Error("DBProxy must be created by DBPersistentAdapter object only");
        }

        DBProxy(T persistObject) {
        mSQLTable = new SQLTable(persistObject.getClass().getSimpleName());
        bind(persistObject.getClass(), Object.class, persistObject);
        mSQLTable.setColumns(getColumns());
        mPersistObject = persistObject;
        }

        public T getPersist() {
            return mPersistObject;
        }

        @Override
        Class<?> getStoredObjectType() {
            return mPersistObject.getClass();
        }
    }
}
