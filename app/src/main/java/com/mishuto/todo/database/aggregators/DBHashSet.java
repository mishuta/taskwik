package com.mishuto.todo.database.aggregators;

import java.util.HashSet;

/**
 * HashSet, сохраняемый в БД
 * Created by Michael Shishkin on 07.03.2017.
 */

public class DBHashSet<T> extends DBSet<HashSet<T>, T>  {

    public DBHashSet() {
        super(new HashSet<T>());
    }
}
