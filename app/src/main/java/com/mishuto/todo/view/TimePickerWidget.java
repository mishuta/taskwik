package com.mishuto.todo.view;

import android.os.Build;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TimePicker;

import com.mishuto.todo.common.TCalendar;

import java.util.Calendar;


/**
 * Таймпикер виджет, используется в TimeDialog
 * Created by Michael Shishkin on 11.09.2016.
 */

public class TimePickerWidget {
    private  TimePicker mTimePicker;

    public TimePickerWidget(View view) {
        mTimePicker = (TimePicker)view;
        mTimePicker.setIs24HourView(DateFormat.is24HourFormat(view.getContext())); // Выбираем 12/24 часовой вид в зависимости от установленной локали
    }

    @SuppressWarnings("deprecation")
    private void setHour(int hour) {
        if (Build.VERSION.SDK_INT >= 23 )
            mTimePicker.setHour(hour);
        else
            mTimePicker.setCurrentHour(hour);
    }

    @SuppressWarnings("deprecation")
    private void setMinute(int minute) {
        if (Build.VERSION.SDK_INT >= 23 )
            mTimePicker.setMinute(minute);
        else
            mTimePicker.setCurrentMinute(minute);
    }

    @SuppressWarnings("deprecation")
    public int getHour() {
        if (Build.VERSION.SDK_INT >= 23 )
            return mTimePicker.getHour();
        else
            return mTimePicker.getCurrentHour();
    }

    @SuppressWarnings("deprecation")
    public int getMinute() {
        if (Build.VERSION.SDK_INT >= 23 )
            return mTimePicker.getMinute();
        else
            return mTimePicker.getCurrentMinute();

    }

    public void setTime(int hour, int minute) {
        setHour(hour);
        setMinute(minute);
    }

    public TimePicker getTimePicker() {
        return mTimePicker;
    }

    // Возращает заданную дату с установленным в пикере временем
    public TCalendar getDateTime(TCalendar date) {
        TCalendar calendar = date.clone();
        calendar.set(Calendar.HOUR_OF_DAY, getHour());
        calendar.set(Calendar.MINUTE, getMinute());
        return calendar.clipTo(Calendar.MINUTE);
    }
}
