package com.mishuto.todo.view.dialog;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.todo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Добавляет Paged Dialog функциональность вкладок
 * Created by Michael Shishkin on 28.11.2017.
 */

abstract public class TabbedDialog extends PagedDialog {

    // класс вкладки
    protected static class Tab {
        private Class<? extends Fragment> mFragmentType;
        private String mTitle;
        private int mIcon;

        public Tab(Class<? extends Fragment> fragmentType, int title, int icon) {
            mFragmentType = fragmentType;
            mTitle = ResourcesFacade.getString(title);
            mIcon = icon;
        }
    }

    // класс Набора вкладок
    protected static class Tabs {
        private List<Tab> mTabs;
        private int mDefaultPos;        // позиция дефолтной вкладки

        public Tabs(List<Tab> tabs, int defaultPos) {
            mTabs = tabs;
            mDefaultPos = defaultPos;
        }

        List<Class<? extends Fragment>> getFragmentTypes() {
            List<Class<? extends Fragment>> fragmentTypeList = new ArrayList<>();
            for(Tab tab : mTabs)
                fragmentTypeList.add(tab.mFragmentType);

            return fragmentTypeList;
        }

        public List<Tab> getTabs() {
            return mTabs;
        }

        int getDefaultPos() {
            return mDefaultPos;
        }
    }

    private Tabs mTabs;

    protected TabbedDialog(Tabs tabs) {
        super(tabs.getFragmentTypes(), tabs.mDefaultPos);
        mTabs = tabs;
        mToolbarLayoutId = R.layout.fs_dlg_tablayout_part;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root =  super.onCreateView(inflater, container, savedInstanceState);


        //noinspection ConstantConditions
        TabLayout tabLayout = root.findViewById(R.id.tabbi);
        tabLayout.setupWithViewPager(getViewPager()); // связываем вкладки с ViewPage
        TabSelectedListener tabSelected = new TabSelectedListener(tabLayout);

        for (int i = 0; i < mTabs.getTabs().size(); i++) {
            //noinspection ConstantConditions
            tabLayout.getTabAt(i).setIcon(mTabs.getTabs().get(i).mIcon);
            tabSelected.setTabIconColor(tabLayout.getTabAt(i), i == mTabs.getDefaultPos());
        }

        return root;
    }

    @Override
    FragmentPagerAdapter createAdapter() {
        return new TabAdapter();
    }

    class TabAdapter extends SimplePagerAdapter {
        @Override
        public CharSequence getPageTitle(int position) {
            return mTabs.getTabs().get(position).mTitle;
        }
    }

    // слушатель событий выбора вкладки
    public static class TabSelectedListener implements TabLayout.OnTabSelectedListener {
        private final static int TAB_ICON_SELECTED_COLOR = R.color.White;
        private final static int TAB_ICON_UNSELECTED_COLOR = R.color.White70;

        private TabLayout mTabLayout;

        public TabSelectedListener(TabLayout tabLayout) {
            tabLayout.addOnTabSelectedListener(this);
            mTabLayout = tabLayout;
        }

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            setTabIconColor(tab, true);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            setTabIconColor(tab, false);
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) { }

        // подсветка иконок
        void setTabIconColor(TabLayout.Tab tab, boolean highlight) {
            int tabIconColor = ContextCompat.getColor(mTabLayout.getContext(), highlight ? TAB_ICON_SELECTED_COLOR : TAB_ICON_UNSELECTED_COLOR);
            //noinspection ConstantConditions
            tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        }
    }
}
