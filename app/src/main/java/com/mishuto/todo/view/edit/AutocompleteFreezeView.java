package com.mishuto.todo.view.edit;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.mishuto.todo.todo.R;

/**
 * CompoundView, состоящий из TextView и AutocompleteTextView
 * Created by Michael Shishkin on 20.01.2017.
 */

public class AutocompleteFreezeView extends EditFreezeView {

    private AutoCompleteTextView mAutoCompleteTextView; //Приведенный к AutoCompleteTextView объект mEditView базового класса

    public AutocompleteFreezeView(Context context) {
        super(context, null);
    }

    public AutocompleteFreezeView(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public AutocompleteFreezeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    // Инициализация mAutoCompleteTextView
    @Override
    EditText instantiateEditView() {
        mAutoCompleteTextView = (AutoCompleteTextView) super.instantiateEditView();
        return mAutoCompleteTextView;
    }

    // инициализация автокомплит-адаптера для EditView
    public <T> void setAutoCompleteAdapter(T[] array) {
        ArrayAdapter<T> adapter = new ArrayAdapter<>(getContext(), R.layout.item_dropdown, array);
        mAutoCompleteTextView.setAdapter(adapter);
    }
}
