package com.mishuto.todo.view.dialog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.common.Table;
import com.mishuto.todo.view_model.common.AbstractActivity;


/**
 * Класс-синглтон, служащий для для упрощения создания диалогов
 * Создает диалог и передает ему входной параметр
 * Возвращает выходной параметр инициатору диалога
 *
 * Диалог принимает/возвращает параметр типа Bundle. Для конвертации обычных типов из/в Bundle служит класс BundleWrapper
 *
 * Created by Michael Shishkin on 30.10.2016.
 */

public final class DialogHelper {

    private DialogHelper() {}   // конструктор синглтона

    /**
     * объект, вызывающий диалог, должен реализовать интерфейс DialogHelperListener, чтобы получить данные из диалога
     */
     public interface DialogHelperListener {
        /**
         * Вызывается при закрытии диалога
         * @param resultCode код закрытия: если диалог вызывает doOk(), возвращается RESULT_OK; иначе RESULT_CANCELED
         * @param resultObject возращаемый диалогом объект. Может быть null
         */
         void onCloseDialog(int resultCode, Bundle resultObject);
    }

    private static DialogHelper sDialogHelper = new DialogHelper();    // единственный объект этого класса
    private Table<DialogHelperListener> mTable = new Table<>();        // таблица объектов, вызывающих диалог

     /**
     * Метод создает диалог, передает ему входной параметр и отображает диалог на экране
     * @param dialogFragment    объект сабкласса DialogFragment, который необходимо отобразить
     * @param targetFragment    родительский фрагмент, который получит управление после закрытия диалога
     * @param listener          слушатель интерфейса DialogHelperListener, который получит выходной параметр, возвращаемый диалогом
     * @param inParameter       входной параметр для инициализации диалога
     */
    public static void createDialog(DialogFragment dialogFragment, Fragment targetFragment, DialogHelperListener listener, Bundle inParameter) {
        dialogFragment.setArguments(inParameter);
        sDialogHelper.createDialogFragment(listener, dialogFragment, targetFragment);    //  создание фрагмента диалога
    }

    public static void createDialog(DialogFragment dialogFragment, Bundle inParameter) {
        createDialog(dialogFragment, null, null, inParameter);
    }

    /**
     * Распаковка входного параметра диалога.
     * Метод вызывается из диалога после создания, обычно в методе onCreateDialog
     * @param dialogFragment объект диалога
     * @return  входной параметр диалога
     */
    public static Bundle inParameter(DialogFragment dialogFragment){
        return dialogFragment.getArguments();
    }

    /**
     * Упаковка выходного параметра диалога и отправка вызывающему фрагменту
     * Метод вызывается из диалога перед уничтожением, обычно в обработчике нажатия кнопки ОК.
     * @param dialogFragment объект диалога
     * @param outParameter готовый к упаковке выходной параметр диалога
     */
    public static void doOk(DialogFragment dialogFragment, Bundle outParameter){
        Intent intent = new Intent();
        if(outParameter != null)
            intent.putExtras(outParameter);

        // метод onActivityResult в целевом фрагменте вызывает метод this.onCloseDialog
        dialogFragment.getTargetFragment().onActivityResult(dialogFragment.getTargetRequestCode(), Activity.RESULT_OK, intent);
    }

    /**
     * Отправка сообщения о закрытии диалога вызывающему фрагменту в случае отмены.
     * Выходной параметр не возращается
     * Метод вызывается из диалога перед уничтожением, обычно в обработчике нажатия кнопки Cancel.
     * @param dialogFragment объект диалога
     */
     public static void doCancel(DialogFragment dialogFragment){
         dialogFragment.getTargetFragment().onActivityResult(dialogFragment.getTargetRequestCode(), Activity.RESULT_CANCELED, null);
     }

    // Метод создает фрагмент с диалогом, сохраняет информацию о нем в mTable и отображает диалог на экране
    private void createDialogFragment(DialogHelperListener listener, DialogFragment dialog, Fragment target){
        Integer dialogId = 0;                                           // идентификатор диалога в таблице, он же тег диалога
        FragmentActivity fa = AbstractActivity.getCurrentActivity();    // запрашиваем объект FragmentActivity
        FragmentManager manager = fa.getSupportFragmentManager();       // получавем FragmentManager

        if(target != null && listener != null) {                        // если необходимо вернуть результат из диалога в таргет-фрагмент
            dialogId = mTable.put(listener);                            // сохраняем вызывающий объект
            dialog.setTargetFragment(target, dialogId);                 // устанавливаем таргет-фрагмент. Вторым аргументом передается идентификатор диалога
        }
        dialog.show(manager, dialogId.toString());                      // добавляем диалог в FragmentManager с тегом диалога
    }

    /**
     * Метод служит для передачи выходного параметра диалога вызывающему объекту.
     * Вызывается из метода onActivityResult таргет-фрагмента, в который возвращается управление после закрытия диалога
     * @param requestCode идентификатор диалога (mDialogID)
     * @param resultCode код кнопки закрытия диалога
     * @param intent интент, содержащий возвращаемый диалогом объект
     */
    public static void onCloseDialog(int requestCode, int resultCode, Intent intent) {

        if (sDialogHelper.mTable.get(requestCode) == null)
            TSystem.debug(sDialogHelper, "Unexpected returned code");
        else {
            DialogHelperListener listener = sDialogHelper.mTable.pull(requestCode);
            Bundle resultObject = intent != null ? intent.getExtras() : null;
            listener.onCloseDialog(resultCode, resultObject);
        }
    }
}

