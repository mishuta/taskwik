package com.mishuto.todo.view.edit;

import android.content.Context;
import android.util.AttributeSet;

import com.mishuto.todo.todo.R;

/**
 * CompaundView: TextView с TextInputLayout+EditText
 * Created by Michael Shishkin on 24.11.2018.
 */
public class TIEditFreezeView extends AbstractTextInputFreezeView {

    public TIEditFreezeView(Context context) {
        this(context, null);
    }

    public TIEditFreezeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TIEditFreezeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr, R.layout.cv_input_text_edit_view, R.id.textView);   // инициализация виджета шаблоном cv_input_text_edit_view
    }
}
