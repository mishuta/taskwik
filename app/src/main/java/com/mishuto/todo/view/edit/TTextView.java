package com.mishuto.todo.view.edit;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Обертка для обычного TextView, которая реализует ITextView.
 * Сделана для унификации всех текстовых виджетов в одном интерфейсе
 * Created by Michael Shishkin on 26.01.2019.
 */
public class TTextView extends AppCompatTextView implements ITextView {
    public TTextView(Context context) {
        super(context);
    }

    public TTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setText(String text) {
        super.setText(text);
    }

    @Override
    public String getText() {
        return super.getText().toString();
    }
}
