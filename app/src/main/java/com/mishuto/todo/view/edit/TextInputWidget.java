package com.mishuto.todo.view.edit;

import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import org.jetbrains.annotations.NotNull;

import static com.mishuto.todo.view.ViewCommon.hideKeyboard;

/**
 * Врапер для виджета ввода текста, построенного на TextInputLayout
 * Created by Michael Shishkin on 14.12.2016.
 */

public class TextInputWidget implements TextWatcher, ITextView {

    // Интерфейс для генерации события изменения текстового поля
    public interface OnChangeTextListener {
        void onTextChanged(String text);
    }

    private TextInputLayout mInputLayout;   // основоной layout виджета
    private EditText mEditText;             // текстовое поле
    private OnChangeTextListener mListener; // лисенер в вызывающем классе, реализующий интерфейс OnChangeTextListener

    public TextInputWidget(@NotNull TextInputLayout layout, @Nullable OnChangeTextListener listener) {
        this(layout, null, listener);
    }

    public TextInputWidget(@NotNull TextInputLayout layout, @Nullable String initText, @Nullable OnChangeTextListener listener) {

        mInputLayout = layout;
        mEditText = layout.getEditText();
        assert mEditText != null;

        setText(initText);
        mEditText.addTextChangedListener(this);
        mListener = listener;
    }

    // инициировать виджет текстом
    public void setText(String s) {
        mEditText.setText(s);
    }

    public String getText() {
        return mEditText.getText().toString();
    }

    public void setHint(int hintId) {
        mInputLayout.setHint(mInputLayout.getResources().getString(hintId));
    }

    // привести виджет в начальное состояние после ввода
    public void resetView() {
        setText(null);              // убираем текст
        hideError();                // убираем ошибку
        hideKeyboard(mEditText);    // скрываем клавиатуру
    }

    // отобразить ошибку
    public void showError(String s) {
        mInputLayout.setError(s);
    }

    public void showError(int resId) {
        showError(mInputLayout.getResources().getString(resId));
    }

    // скрыть ошибку
    public void hideError() {
        mInputLayout.setError(null);
    }

    // событие изменения текста в виджете
    @Override
    public void afterTextChanged(Editable s) {
        if (mListener != null)
            mListener.onTextChanged(mEditText.getText().toString());
    }

    public EditText getEditText() {
        return mEditText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}
}
