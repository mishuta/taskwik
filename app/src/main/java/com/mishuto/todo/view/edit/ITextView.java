package com.mishuto.todo.view.edit;

/**
 * Интерфейс для объединения иерархий классов текстовых виджетов TextView, AbstractCompoundEditFreezeView, EditView,
 * который позволяет устанавливать и запрашивать текст из виджета, независимо от реализации
 * Created by Michael Shishkin on 26.01.2019.
 */
public interface ITextView {
    String getText();
    void setText(String text);
}
