package com.mishuto.todo.view.recycler;

import android.support.v7.widget.RecyclerView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SearchView;

import java.util.List;

/**
 * Класс для работы с фильтруемыми списками, которые состоят из двух виджетов: SearchView и RecycleView
 * Наследуется от RecycleViewWidget и добавляет к нему функционал обработки ввода в SearchView и асинхронной фильтрации
 * Created by Michael Shishkin on 28.12.2016.
 */

public class FilteredListView<T> extends RecyclerViewWidget<T> implements Filterable, SearchView.OnQueryTextListener
{
    // Интерфейс для реализации клиентом механизма фильтрации
    public interface FilterListener<U> {
        /**
         * Метод, реализуемый в клиенте для фильтрации списка
         * @param constraint строка, приходящая из
         * @return новый отфильтрованный список
         */
        List<U> getFilteredResults(String constraint);
    }

    private FilterListener<T> mFilterListener;  // лисенер мехаинизма фильтрации
    final private List<T> mOriginalList;        // здесь хранится оригинальный список, переданный конструктору объекта

    /**
     * конструктор объекта. Первые 4 параметра соответствуют конструктору суперкласса
     * @param recyclerView виджет RecycleView
     * @param layout layout элемента списка
     * @param list исходный список элементов модели
     * @param eventsListener лисенер для интерфейса EventsListener
     * @param searchView виджет SearchView
     * @param filterListener лисенер интерфейса FilterListener
     */
    public FilteredListView(RecyclerView recyclerView,
                             int layout,
                             List<T> list,
                             EventsListener<T> eventsListener,
                             SearchView searchView,
                             FilterListener<T> filterListener) {

        super(recyclerView, layout, list, eventsListener);
        mFilterListener = filterListener;

        mOriginalList = mDataList;
        searchView.setOnQueryTextListener(this);
    }

    // Метод выполняет фильтрацию списка по подстроке
    @SuppressWarnings("WeakerAccess")
    public void doFilter(String constraint) {
        if(constraint != null && constraint.length() > 0)
            getFilter().filter(constraint);
        else                            // если подстрока пустая - восстанавливается оригинальный список
            if(mOriginalList != mDataList)
                setDataList(mOriginalList);
    }

    // создание объекта фильтрованного списка
    @Override
    public Filter getFilter() {
        return new FilteredList();
    }

    // обработчик события изменения текста в SearchView
    @Override
    public boolean onQueryTextChange(String newText) {
        doFilter(newText);
        return true;
    }

    // класс, реализующий фильтр
    private class FilteredList extends Filter {
        // выполнение фильтрации в отдельном потоке
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            results.values = mFilterListener.getFilteredResults(constraint.toString());
            return results;
        }

        // передача результата в виде отфильтрованного списка в адаптер RecycleView
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            setDataList((List<T>) results.values);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {return false;}
}
