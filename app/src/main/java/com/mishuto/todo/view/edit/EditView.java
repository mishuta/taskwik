package com.mishuto.todo.view.edit;

import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.mishuto.todo.todo.R;

import static com.mishuto.todo.view.ViewCommon.hideKeyboard;

/**
 * Враппер обычного EditText с возможностью изменять цвет подчеркивания во время ввода в случае некорректных данных.
 * Если не задавать OnChangeListener, то поведение будет полностью идентичено EditText
 * Created by Michael Shishkin on 28.01.2017.
 */
@SuppressWarnings("WeakerAccess")
public class EditView implements TextWatcher, ITextView {

    public interface OnChangeListener {
        /**
         * Колбэк для обработки вводимого текста на лету.
         * Цвет линии подчеркивания меняется в зависимости от введенного текста
         * @param text введенный пользователем текст
         * @param view данный объект
         * @return true - если ввод корректный, false - если ошибка
         */
        boolean onTextValidate(EditView view, String text);
    }

    private OnChangeListener mListener;
    private EditText mEditText;
    private int mErrUnderlineColor = R.color.colorErroneousText;
    private int mNormalUnderlineColor = R.color.colorAccent;

    public EditView(View editText, OnChangeListener listener) {
        mEditText = (EditText) editText;
        mListener = listener;
        mEditText.addTextChangedListener(this);
        setUnderlineColor(mNormalUnderlineColor);
    }

    public EditText getEditText() {
        return mEditText;
    }

    public void setListener(OnChangeListener listener) {
        mListener = listener;
    }

    public String getText() {
        return mEditText.getText().toString();
    }

    public void setText(String s) {
        mEditText.setText(s);
    }

    public void setErrUnderlineColor(int errUnderlineColor) {
        mErrUnderlineColor = errUnderlineColor;
    }

    public void setNormalUnderlineColor(int normalUnderlineColor) {
        mNormalUnderlineColor = normalUnderlineColor;
        setUnderlineColor(mNormalUnderlineColor);
    }

    // сброс виджета
    public void reset() {
        setText(null);              // удаляем текст
        highlightError(false);      // убираем ошибку
        hideKeyboard(mEditText);    // удаляем фокус и клавиатуру
    }

    // Меняет цвет линии подчеркивания EditText в зависимости от того что возвращает onTextValidate
    @Override
    public void afterTextChanged(Editable s) {
        if (mListener != null)
            highlightError(!mListener.onTextValidate(this, s.toString()));
    }

    // выделение ошибки подчеркиванием
    public void highlightError(boolean isError) {
        setUnderlineColor(isError ? mErrUnderlineColor : mNormalUnderlineColor);
    }

    private void setUnderlineColor(int color) {
        mEditText.getBackground().mutate().setColorFilter(ContextCompat.getColor(mEditText.getContext(), color), PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {}

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
}
