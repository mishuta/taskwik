package com.mishuto.todo.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mishuto.todo.todo.R;

/**
 * Базовый класс, для отображения полноэкранного диалога, вызываемого из Activity
 * Created by Michael Shishkin on 29.06.2017.
 */
abstract public class BaseFullScreenDialog extends DialogFragment {

    private View mRootView;         // корневое View окна диалога
    private ToolBar mToolBar;       // тулбар диалога

    protected int mToolbarLayoutId; // layout тулбара
    protected int mBodyLayoutId;    // layout основного содержимого

    protected BaseFullScreenDialog(int toolbarLayoutId, int bodyLayoutId) {
        mToolbarLayoutId = toolbarLayoutId;
        mBodyLayoutId = bodyLayoutId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fs_dlg_host, container, false);
        // загрузка основного содержимого
        includeLayout(R.id.toolbar_layout, mToolbarLayoutId);
        includeLayout(R.id.main_layout, mBodyLayoutId);
        mToolBar = new ToolBar(mRootView);

        return mRootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        //noinspection ConstantConditions
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    // основной трюк для создания полноэкранного диалога
    // растягиваем окно диалога и, обязательно определяем бэкграунд (без этого окно полностью не растянется)
    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            //noinspection ConstantConditions
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        }
    }

    // загрузка разметки в заданный layout
    protected void includeLayout(int to, int what) {
        ViewGroup rootLayout = mRootView.findViewById(to);                                                          // находим Layout внутри текущего окна
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);  // получаем системный инфлатер
        //noinspection ConstantConditions
        View child = inflater.inflate(what, rootLayout, false);                                                     // получаем View-объект из встраемого шаблона

        rootLayout.addView(child);                                                                                  // вставляем в Layout полученную дочерниюю view
    }

   // Статический метод создания диалога. Вызывается из Activity
    public static void createDialog(BaseFullScreenDialog dialog, Bundle inParam) {
        DialogHelper.createDialog(dialog, inParam);
    }

    //Отображение и установка лисенера для FAB
    public void setFABListener(FloatingActionButton.OnClickListener listener) {
        final FloatingActionButton fab = mRootView.findViewById(R.id.floatingActionButton);
        fab.setVisibility(listener == null ? View.INVISIBLE : View.VISIBLE);
        fab.setOnClickListener(listener);   // устанавливаем обработчик нажатия для FAB
    }

    public ToolBar getToolBar() {
        return mToolBar;
    }

    // ToolBar полноэкранного диалога
    //@SuppressWarnings("WeakerAccess")
    public static class ToolBar implements View.OnClickListener {
        // интерфейс, реализуемый клиентом для отработки нажатия на кнопку тулбара
        public interface OnButtonClickListener {
            void onClick(ButtonIcon buttonIcon);
        }

        // возможные иконки кнопок, отображаемые на тулбаре
        public enum ButtonIcon {
            UP(R.drawable.ic_arrow_left_white_24dp),
            CONFIRM(R.drawable.ic_check_white_24dp),
            CANCEL(R.drawable.ic_close_white_24dp),
            NONE(0);

            private final int resId;

            ButtonIcon(int resId) {
                this.resId = resId;
            }
        }

        // Класс - кнопка тулбара
        private class Button {
            ImageButton mView;      // кнопка
            ButtonIcon mIcon;       // иконка

            Button(View root, int resId) {
                mView = root.findViewById(resId);
                mView.setOnClickListener(ToolBar.this);
            }

            // установка/замена иконки у кнопки
            private void setButton(ButtonIcon buttonIcon) {
                mIcon = buttonIcon;
                if(mIcon == ButtonIcon.NONE)
                    mView.setVisibility(View.INVISIBLE);
                else {
                    mView.setVisibility(View.VISIBLE);
                    mView.setImageResource(buttonIcon.resId);
                }
            }
        }

        private Button leftButton;              // левая кнопка тулбара
        private Button rightButton;             // правая кнопка тулбара
        private TextView title;                 // заголовок диалога
        private OnButtonClickListener mButtonClickListener; // слушатель нажатия кнопок

        ToolBar(View root) {
            leftButton = new Button(root, R.id.button_left);
            rightButton = new Button(root, R.id.button_right);
            title = root.findViewById(R.id.app_bar_title);
        }

        public void setLeftButton(ButtonIcon buttonIcon) {
            leftButton.setButton(buttonIcon);
        }

        public void setRightButton(ButtonIcon buttonIcon) {
            rightButton.setButton(buttonIcon);
        }

        public void setTitle(int resId) {
            title.setText(resId);
        }

        public void setTitle(CharSequence s) {
            title.setText(s);
        }

        public void setButtonClickListener(OnButtonClickListener listener) {
            mButtonClickListener = listener;
        }

        @Override
        public void onClick(View v) {
            if(mButtonClickListener != null) 
                mButtonClickListener.onClick((v.getId() == leftButton.mView.getId()) ? leftButton.mIcon : rightButton.mIcon);
        }
    }
}
