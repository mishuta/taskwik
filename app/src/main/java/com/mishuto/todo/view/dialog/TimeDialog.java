package com.mishuto.todo.view.dialog;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import com.mishuto.todo.common.Time;

import java.util.Calendar;

import static com.mishuto.todo.common.BundleWrapper.getSerializable;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.common.TCalendar.now;
import static com.mishuto.todo.view.dialog.DialogHelper.doOk;
import static com.mishuto.todo.view.dialog.DialogHelper.inParameter;

/**
 * Диалог для ввода времени
 * Created by Michael Shishkin on 05.11.2016.
 */

public class TimeDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // инициализация виджетов

        Time inParam = (Time) getSerializable(inParameter(this));       //Входной параметр - установленное время

        int hour, minute;

        if( inParam == null) {                                          //Если время не задано, по умолчанию время = ближайший следующий час
            Calendar c = now();
            c.add(Calendar.HOUR_OF_DAY, 1);
            hour = c.get(Calendar.HOUR_OF_DAY);
            minute = 0;
        }
        else {
            hour = inParam.getHour();
            minute = inParam.getMinute();
        }

        return new TimePickerDialog(getContext(), this, hour, minute, DateFormat.is24HourFormat(getContext()));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        doOk(this, toBundle(new Time(hourOfDay, minute)));
    }
}
