package com.mishuto.todo.view.recycler;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Виджет на базе класса RecyclerView для отображения вертикального списка, который в качестве data source использует список List объектов типа T.
 * Инкапсулирует в себе работу с классами RecyclerView, RecyclerView.Adapter, RecyclerView.ViewHolder
 *
 * Created by Michael Shishkin on 25.12.2016.
 */

public class RecyclerViewWidget<T> {

    /**
     * События пораждаемые классом для обработки в клиенте
     */
    public interface EventsListener<U> {
        /**
         * событие связывания строки списка View c объектом модели, отображаемой в списке
         * служит для инициализации view в строке
         * @param root корневой элемент строки списка
         * @param item объект модели, ассоциированный с элементом списка
         * @param viewHolder объект ViewHolder для строки
         */
        void onBind(View root, U item, RecyclerViewWidget<U>.ViewHolder viewHolder);

        /**
         * Событие нажатия на строку списка
         * @param view виджет, на который нажали.
         * @param viewHolder объект ViewHolder для строки
         */
        void onClick(View view, RecyclerViewWidget<U>.ViewHolder viewHolder);
        // TODO: 10.12.2017 сделать автоматическое назначение OnClickListener всех view внутри layout на ViewHolder, удалить назначения в коде клиентов
    }


    private RecyclerView mRecyclerView; // виджет RecyclerView
    private Adapter mAdapter;           // адаптер, содержащий список задач для RecyclerView. Служит контейнером для объектов ViewHolder
    private EventsListener<T> mListener;// клиентский объект, реализующий интерфейс
    List<T> mDataList;                  // список объектов модели, которые должны быть ассоциированы со списком RecyclerView

    /**
     * Конструктор виджета
     * @param recyclerView виджет RecyclerView
     * @param itemLayout xml-layout элемента списка
     * @param dataList список отображаемых объектов модели
     * @param listener вызывающий объект, реализующий колбэк-интерфейс
     */
    public RecyclerViewWidget(RecyclerView recyclerView, int itemLayout, List<T> dataList, EventsListener<T> listener) {
        this(recyclerView, itemLayout, dataList, listener, new LinearLayoutManager(recyclerView.getContext()));
    }

    // Расширенный конструктор для кастомного LayoutManager
    public RecyclerViewWidget(RecyclerView recyclerView, int itemLayout, List<T> dataList, EventsListener<T> listener, RecyclerView.LayoutManager layoutManager) {
        mRecyclerView = recyclerView;
        mListener = listener;
        mDataList = dataList;
        mRecyclerView.setLayoutManager(layoutManager); // для отображения вертикального списка задается LinearLayoutManager и сабклассы
        mAdapter = new Adapter(itemLayout);            // Адаптер для связывания Задач с ViewHolder
        mRecyclerView.setAdapter(mAdapter);            // устанавливаем адаптер для RecycleView
    }

    // изменяет отображаемый список при вставке элемента и скроллит на вставленный элемент
    public void insertAt(int pos, T element) {
        mDataList.add(pos, element);
        notifyInsertedAt(pos);
    }

    // добавляет элемент в конец списка
    public void add(T element) {
        mDataList.add(element);
        notifyInsertedAt(mDataList.size() - 1);
    }

    // вызвается если список менять не нужно (он уже изменен), а только обновить
    public void notifyInsertedAt(int pos) {
        mAdapter.notifyItemInserted(pos);
        scrollTo(pos);
    }

    // изменяет отображаемый список при удалении
    public void deleteAt(int pos) {
        mDataList.remove(mDataList.get(pos));
        mAdapter.notifyItemRemoved(pos);
    }

    // обновляет элемент в заданной позиции
    public void changeAt(int pos) {
        mAdapter.notifyItemChanged(pos);
        scrollTo(pos);
    }

    // перемещает элемент в списке из oldPos в newPos
    public void move(int oldPos, int newPos) {
        T tmp = mDataList.get(oldPos);  // перемещаем элемент в списке:
        mDataList.remove(oldPos);       // удаляем, затем вставляем
        mDataList.add(newPos, tmp);

        mAdapter.notifyItemMoved(oldPos, newPos);
        scrollTo(newPos);
    }

    public Adapter getAdapter() {
        return mAdapter;
    }

    public void setDataList(List<T> dataList){
        mDataList = dataList;
        mAdapter.notifyDataSetChanged();
    }

    public void refreshList() {
        mAdapter.notifyDataSetChanged();
    }

    public List<T> getDataList() {
        return mDataList;
    }

    public int getPosition(T element) {
        return mDataList.indexOf(element);
    }

    public void scrollTo(int pos) {
        mRecyclerView.getLayoutManager().scrollToPosition(pos);
    }
    /**
     * Класс ViewHolder управляет виджетами внутри строки списка
     * Служит для связывания виджетов строки с объектом модели
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private View mRootView;             // Корневая View строки списка
        private T mModelItem;               // объект модели для отображения

        ViewHolder(View rootView) {
            super(rootView);
            mRootView = rootView;
            rootView.setOnClickListener(this);//Обработчик клика на строку RecyclerView
        }

        @Override
        // обработка нажатия на строку списка.
        public void onClick(View view) {
            mListener.onClick(view, this);
        }

        public View getRootView() {
            return mRootView;
        }

        public T getItem() {
            return mModelItem;
        }

        // удаление текущего элемента из списка данных
        public void remove() {
            mDataList.remove(mModelItem);
            mAdapter.notifyItemRemoved(getAdapterPosition());
        }
    }

    /**
     * Класс, для управления списком и связывания его с ViewHolder
     */
    @SuppressWarnings("WeakerAccess")
    public class Adapter extends RecyclerView.Adapter<ViewHolder> {

        private final int mItemLayout;          // id xml ресурса с шаблоном элемента списка

        Adapter(int item_layout) {
            mItemLayout = item_layout;
        }

        // Событие создания ViewHolder
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());   // получаем объект LayoutInflater
            View view = layoutInflater.inflate(mItemLayout, parent, false);             // создаем View по заданному шаблону
            return new ViewHolder(view);
        }

        /**
         * В заданной строке списка связываются виджеты шаблона строки с полями модели Task
         * @param holder объект, управляющий отображением строки списка
         * @param position номер позиции строки списка
         */
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.mModelItem = mDataList.get(position);  // сопоставляем позиции списка адаптера объект модели из той же позиции в списке объектов
            mListener.onBind(holder.mRootView, holder.mModelItem, holder);
        }

        @Override
        public int getItemCount() {
            return mDataList.size();
        }
    }
}
