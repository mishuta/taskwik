package com.mishuto.todo.view.edit;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.ViewCommon;

import static com.mishuto.todo.view.ViewCommon.setBackPressListener;

/**
 * CompoundView, объединяющий TextView и EditText. Объявлен как абстратный, потому что должен быть унаследован конкретным классом, в котором задается шаблон View
 * Редактируемый виджет, может быть переопределен в сабклассе
 * Created by Michael Shishkin on 09.01.2017.
 */

abstract public class AbstractCompoundEditFreezeView extends FrameLayout implements ITextView {

    public interface AutoChangeModeListener {
        /**
         * Событие автоизменения Mode
         * Не генерируется при явном вызове setEditMode()/setViewMode
         * @param view Данная view
         * @param toViewMode true если был переход во viewMode; false, если переход в editMode
         */
        void onAutoChangeMode(AbstractCompoundEditFreezeView view, boolean toViewMode);
    }

    public interface OnClickListener {
        // Событие клика на TextView
        // Генерируется только, если объект находится в ViewMode и автопереход в EditMode выключен
        void onClick(AbstractCompoundEditFreezeView view);
    }

    private TextView mTextView;             // виджет для ViewMode
    private EditText mEditView;             // виджет для EditMode
    private boolean mViewMode;              // состояние. true для ViewMode, false для EditMode
    private boolean doEditModeByClick;      // автоматический переход в EditMode по клику на TextView (задается в XML)
    private boolean mViewModeByFocusLost;   // автоматический переход в ViewMode при потере фокуса EditText (задается в XML)
    private boolean mViewModeByActionDone;  // автоматический переход в ViewMode при нажатии Enter на клавиатуре (задается в XML)

    private AutoChangeModeListener mAutoChangeListener; // лисенер события автоматического изменения EditMode на ViewMode
    private OnClickListener mClickListener;             // лисенер события клика по TextView

    public AbstractCompoundEditFreezeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    // Запрос EditView. Метод переопределяется сабклассами для дополнительной инициализации редактируемого виджета
    abstract EditText instantiateEditView();

    // запрос текста из виджета
    public String getText() {
        if(mViewMode)
            return mTextView.getText().toString();
        else
            return mEditView.getText().toString();
    }

    public TextView getTextView() {
        return mTextView;
    }

    public EditText getEditTextView() {
        return mEditView;
    }

    public boolean isEditMode() {
        return !mViewMode;
    }

    // инициализация виджета текстом
    public void setText(String text) {
        if(mViewMode)
            mTextView.setText(text);
        else
            mEditView.setText(text);
    }

    public void setText(int id) {
        String text = mTextView.getResources().getString(id);
        setText(text);
    }

    // переход в EditMode
    public void setEditMode() {
        if(mViewMode) {
            mTextView.setVisibility(GONE);
            mEditView.setVisibility(VISIBLE);
            mEditView.setText(mTextView.getText());
            ViewCommon.showKeyboard(mEditView);
            mViewMode = false;
        }
    }

    // переход в ViewMode
    public void setViewMode() {
        if(!mViewMode) {
            mViewMode = true;
            mTextView.setText(mEditView.getText());
            mTextView.setVisibility(VISIBLE);
            mEditView.setVisibility(GONE);
            ViewCommon.hideKeyboard(mEditView);
        }
    }

    // перевод в view mode и генерация события
    private void changeToViewMode() {
        setViewMode();

        if(mAutoChangeListener != null )
            mAutoChangeListener.onAutoChangeMode(AbstractCompoundEditFreezeView.this, true);
    }

    public void setAutoChangeModeListener(AutoChangeModeListener listener) {
        mAutoChangeListener = listener;
    }

    public void setOnClickListener(OnClickListener listener) {
        mClickListener = listener;
    }

    public void setAutoChangeMode(boolean mode) {
        doEditModeByClick = mode;
    }

    @Override
    public void setEnabled(boolean enabled) {
        mTextView.setEnabled(enabled);
        mEditView.setEnabled(enabled);
    }

    @Override
    public void setAlpha(float alpha) {
        mTextView.setAlpha(alpha);
        mEditView.setAlpha(alpha);
    }


    // класс для обработчиков событий TextView
    private class TextViewListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if(doEditModeByClick && mViewMode) {
                setEditMode();
                if(mAutoChangeListener != null)
                    mAutoChangeListener.onAutoChangeMode(AbstractCompoundEditFreezeView.this, false);
            }
            else if(mClickListener != null)
                mClickListener.onClick(AbstractCompoundEditFreezeView.this);
        }
    }

    // класс для обработчиков событий EditView
    private class EditTextListener implements OnFocusChangeListener, EditText.OnEditorActionListener {
        // обработка потери фокуса
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(!hasFocus && mViewModeByFocusLost && !mViewMode)
                changeToViewMode();
        }

        // обработка нажатия Enter
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if(actionId == EditorInfo.IME_ACTION_DONE && mViewModeByActionDone && !mViewMode) {
                changeToViewMode();
                return true;
            }
            return false;
        }
    }

    // инициализация CompoundView.
    // Вызывается из конструкторов конкретных классов, поскольку требует дополнительной параметризации
    @SuppressWarnings("deprecation")
    void initialize(int layout, int textViewId, Context context, AttributeSet attrs) {
        // инициализация сабвиджетов
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //noinspection ConstantConditions
        inflater.inflate(layout, this);

        if(!isInEditMode()) {   // инициализация не отрабатывает для режима отображения в Android Studio

            // загрузка xml-атрибутов
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AbstractCompoundEditFreezeView);
            doEditModeByClick =     typedArray.getBoolean(R.styleable.AbstractCompoundEditFreezeView_editModeByClick, false);
            mViewModeByActionDone = typedArray.getBoolean(R.styleable.AbstractCompoundEditFreezeView_viewModeByActionDone, false);
            mViewModeByFocusLost =  typedArray.getBoolean(R.styleable.AbstractCompoundEditFreezeView_viewModeByFocusLost, false);
            int style =             typedArray.getResourceId(R.styleable.AbstractCompoundEditFreezeView_style, 0);
            int ems =               typedArray.getInt(R.styleable.AbstractCompoundEditFreezeView_ems, 0);
            boolean multiline =     typedArray.getBoolean(R.styleable.AbstractCompoundEditFreezeView_multiline, false);
            int maxLinesText =      typedArray.getInt(R.styleable.AbstractCompoundEditFreezeView_maxLinesText, 0);
            int ellipsize =         typedArray.getInt(R.styleable.AbstractCompoundEditFreezeView_ellipsize, -1);
            boolean clickable =     typedArray.getBoolean(R.styleable.AbstractCompoundEditFreezeView_clickable, true);
            int background =        typedArray.getResourceId(R.styleable.AbstractCompoundEditFreezeView_background, 0);
            int hint =              typedArray.getResourceId(R.styleable.AbstractCompoundEditFreezeView_hint, 0);
            int textStyleEdit =     typedArray.getInt(R.styleable.AbstractCompoundEditFreezeView_textStyleEdit, -1);
            int textStyleView =     typedArray.getInt(R.styleable.AbstractCompoundEditFreezeView_textStyleView, -1);
            typedArray.recycle();

            mTextView = findViewById(textViewId);
            mEditView = instantiateEditView();  // инициализируем EditView
            mViewMode = false;                  // устанавливаем ViewMode
            setViewMode();

            // инициация сабвиджетов xml-атрибутами
            if(ems > 0)
                mEditView.setEms(ems);

            if(maxLinesText > 0)
                mTextView.setMaxLines(maxLinesText);

            if(multiline) {
                mEditView.setSingleLine(false);
                mEditView.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            }

            if(style > 0)
                if (Build.VERSION.SDK_INT < 23){
                    mTextView.setTextAppearance(context, style);
                    mEditView.setTextAppearance(context, style);
                }
                else {
                    mTextView.setTextAppearance(style);
                    mEditView.setTextAppearance(style);
                }

            mTextView.setEllipsize(ellipsize == -1 ? null : TextUtils.TruncateAt.values()[ellipsize]);
            mTextView.setClickable(clickable);

            // установка лисенеров на события
            if(clickable)
                mTextView.setOnClickListener(new TextViewListener());

            if(background > 0)
                mEditView.setBackgroundResource(background);

            if(hint > 0)
                mEditView.setHint(hint);

            if(textStyleEdit > -1)
                mEditView.setTypeface(mEditView.getTypeface(), textStyleEdit);

            if(textStyleView > -1)
                mTextView.setTypeface(mEditView.getTypeface(), textStyleView);

            EditTextListener editTextListener = new EditTextListener();
            mEditView.setOnFocusChangeListener(editTextListener);
            mEditView.setOnEditorActionListener(editTextListener);

            setBackPressListener(mEditView, this::changeToViewMode);    // нажатие кнопки back в edit mode переводит виджет в view mode
        }
    }
}
