package com.mishuto.todo.view.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.mishuto.todo.common.TCalendar;

import static com.mishuto.todo.common.BundleWrapper.getSerializable;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.view.dialog.DialogHelper.doOk;
import static com.mishuto.todo.view.dialog.DialogHelper.inParameter;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;


/**
 * Диалог для ввода даты
 * Created by Michael Shishkin on 04.11.2016.
 */

public class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        TCalendar inParam = (TCalendar) getSerializable(inParameter(this));     // входной параметр - дата
        TCalendar initDate = inParam == null ? TCalendar.now() : inParam;       // Если дата установлена, она инициализирует диалог, иначе диалог инициализируется текущией датой

        int day, month, year;

        year =  initDate.get(YEAR);
        month = initDate.get(MONTH);
        day =   initDate.get(DAY_OF_MONTH);

        return new DatePickerDialog(getContext(), this, year, month, day);
    }

    // обработчик нажатия ОК в диалоге
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        doOk(this, toBundle(new TCalendar(year, month, dayOfMonth)));
    }
}
