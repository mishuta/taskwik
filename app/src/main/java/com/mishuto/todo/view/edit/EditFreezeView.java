package com.mishuto.todo.view.edit;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.mishuto.todo.todo.R;

/**
 * CompoundView, состоящий из TextView и EditView
 * Created by Michael Shishkin on 20.01.2017.
 */

public class EditFreezeView extends AbstractCompoundEditFreezeView {
    
    EditView mEditView; // редактируемый сабвиджет

    public EditFreezeView(Context context) {
        this(context, null);
    }

    public EditFreezeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EditFreezeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(R.layout.cv_text_edit_view, R.id.textView, context, attrs);   // Инициализируем виджет шаблоном cv_text_edit_view, описывающим виджет
    }

    @Override
    EditText instantiateEditView() {
        mEditView = new EditView(findViewById(R.id.autocompleteTextView), null);
        return mEditView.getEditText();
    }
    
    public EditView getEditView() {
        return mEditView;
    }

}
