package com.mishuto.todo.view.edit;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.widget.EditText;

import com.mishuto.todo.todo.R;

/**
 * Базовый класс для CompoundView, состоящий из TextView и TextInputLayout
 * Created by Michael Shishkin on 21.01.2017.
 */

abstract class AbstractTextInputFreezeView extends AbstractCompoundEditFreezeView {

    TextInputLayout mInputLayout;

    public AbstractTextInputFreezeView(Context context, AttributeSet attrs, int defStyleAttr, int layout, int textViewId) {
        super(context, attrs, defStyleAttr);
        initialize(layout, textViewId, context, attrs);
    }

    // Инициализация mInputLayout, mEditText
    @Override
    EditText instantiateEditView() {
        mInputLayout = findViewById(R.id.textInputView);
        return mInputLayout.getEditText();
    }

    @Override
    public void setViewMode() {
        super.setViewMode();
        mInputLayout.setVisibility(GONE);
    }

    @Override
    public void setEditMode() {
        super.setEditMode();
        mInputLayout.setVisibility(VISIBLE);
    }

    public TextInputWidget getTextInputWidget(TextInputWidget.OnChangeTextListener listener) {
        return new TextInputWidget(mInputLayout, listener);
    }
}
