package com.mishuto.todo.view;

/**
 * Используемые константы
 * Created by Michael Shishkin on 06.12.2017.
 */

public interface Constants {

    // константы прозрачности
    float OPAQUE_FULL = 1;
    float OPAQUE_MID = 0.5f;
    float OPAQUE_FAINT = 0.26f;
}
