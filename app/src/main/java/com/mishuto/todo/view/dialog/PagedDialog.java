package com.mishuto.todo.view.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishuto.todo.common.TObjects;
import com.mishuto.todo.todo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Базовый класс полноэкранного диалога с прокручиваемыми в PageView страницами - фрагментами
 * Created by Michael Shishkin on 26.11.2017.
 */
abstract public class PagedDialog extends BaseFullScreenDialog implements ViewPager.OnPageChangeListener
{
    private ViewPager mViewPager;   // виджет пейджинга

    private List<Fragment> mPages;  // список отображаемых страниц/вкладок
    private List<Class<? extends Fragment>> mPageTypes; // список типов страниц
    private int mInitialPage;       // номер начальной страницы

    // интерфейс для вызова события смены страницы.
    // Должен быть реализован самой страницей, если ей требуется такое событие
    public interface OnPageSelectedListener {
        void onPageSelected();
    }

    // создание диалога со страницами
    // в диалог передаются не сами объекты фрагментов страниц, а их типы, чтобы не порождать объекты, если они существуют (после пересоздания фрагментов в ОС)
    public PagedDialog(List<Class<? extends Fragment>> pageTypes, int initialPage) {
        super(R.layout.fs_dlg_toolbar_part, R.layout.fs_dlg_view_page_part);
        mInitialPage = initialPage;
        mPageTypes = pageTypes;
    }

    // здесь создаются фрагменты-страницы, только если они не существуют (например, в случае воссоздания диалога после выгрузки приложения из памяти)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPages = new ArrayList<>();
        for(int i = 0 ; i < mPageTypes.size(); i++) {
            Fragment page = getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.view_pager + ":" + i);
            if(page == null)
                page = TObjects.createInstance(mPageTypes.get(i));

            mPages.add(i, page);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = super.onCreateView(inflater, container, savedInstanceState);

        //noinspection ConstantConditions
        mViewPager = root.findViewById(R.id.view_pager);
        mViewPager.setAdapter(createAdapter());
        mViewPager.addOnPageChangeListener(this);

        if(mInitialPage == 0)
            onPageSelected(0);  // нулевая страница - по умолчанию - сама не вызывает onPageSelected
        else
            setPage(mInitialPage);

        return root;
    }

    // вызов события смены страницы в самой странице. Для этого страница должна реализовать интерфейс OnPageSelectedListener
    @Override
    public void onPageSelected(int position) {
        Fragment page = mPages.get(position);
        if(page instanceof OnPageSelectedListener)
            ((OnPageSelectedListener)page).onPageSelected();
    }

    // установить страницу по номеру в списке
    public void setPage(int pageNum) {
        mViewPager.setCurrentItem(pageNum);
    }

    // фабричный метод создания адаптера
     FragmentPagerAdapter createAdapter() {
        return new SimplePagerAdapter();
    }

    ViewPager getViewPager() {
        return mViewPager;
    }

    public List<Fragment> getPages() {
        return mPages;
    }

    // простой адаптер страниц PageView для ограниченного количества  страниц
     class SimplePagerAdapter extends FragmentPagerAdapter {
        SimplePagerAdapter() {
            super(getChildFragmentManager());                   // запрашиваем FragmentManager у фрагмента отображаемого диалога
        }

        @Override
        public Fragment getItem(int position) {
            return mPages.get(position);
        }

        @Override
        public int getCount() {
            return mPages.size();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

    @Override
    public void onPageScrollStateChanged(int state) {  }
}
