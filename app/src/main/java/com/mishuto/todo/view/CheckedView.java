package com.mishuto.todo.view;

/**
 * Интерфейс для контролов, имитирующих нажатую кнопку
 * Контрол аналогичен ToggledButton, но гибче и удобнее
 * Created by Michael Shishkin on 28.10.2018.
 */
public interface CheckedView {
    // Интерфейс обратного вызова при изменении состояния (нажатии кнопки)
    interface OnChangeStateListener {
        void onChangeState(CheckedView button, boolean checked);
    }

    void setChecked(boolean checked);                           // установить статус кнопки
    boolean isChecked();                                        // получить статус кнопки
    void setOnChangeListener(OnChangeStateListener listener);   // установить слушателя события
}
