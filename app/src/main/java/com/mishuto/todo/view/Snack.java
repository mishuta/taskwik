package com.mishuto.todo.view;

import android.os.Build;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Враппер снэкбара.
 * Является синглтоном, поскольку в каждый момент времени на экране может жить только один объект снекбара.
 * Created by Michael Shishkin on 29.09.2017.
 */

@SuppressWarnings("WeakerAccess")
public class Snack {

    private static final int SNACK_MAX_LINES = 10;  // Максимальное количество строк снекбара
    private static final int MSEC_ON_LETTER = 40;   // Время экспонирование снека зависит от длины сообщения. Эмпирически выбрано 40мс на букву
    private static final int MSEC_MINIMAL = 1000;   // Минимальное время экспонирования (для коротких сообщений)

    private static Snack instance = new Snack();
    private String mMessage;                        // сообщение в снеке
    private boolean isShown = false;                // состояние снека
    private Snackbar mSnackBar;                     // объект снекбар

    /**
     * Вывод ручного снек-сообщения
     * @param view виджет, порождающий снек (для привязки снека к родительскому окну виджета
     * @param stringRes id ресурса строки
     */
    public static Snackbar show(View view, int stringRes){
        return show(view, view.getResources().getString(stringRes), null, null, 0, 0);
    }

    //Вывод ручного снек-сообщения
    public static Snackbar show(View view, String msg) {
        return show(view, msg, null, null, 0, 0);
    }

    /**
     * Вывод ручного снек сообщения. Размер окна снека зависит от длины сообщения
     * @param view виджет снека
     * @param msg сообщение
     * @param eventListener обработчик эвента исчезновения снека
     * @param actionListener обработчик нажатия на кнопку Action
     * @param actionTextId текст кнопки Action
     * @param duration длительность снека. Если duration = 0, то используется значение по умолчанию, в зависимости от длины текста
     * @return снекбар
     */
    public static Snackbar show(View view, String msg, OnDismissedEvent eventListener, View.OnClickListener actionListener, int actionTextId, int duration){

        instance.mMessage = msg;
        if(duration == 0) duration = msg.length() * MSEC_ON_LETTER + MSEC_MINIMAL;
        Snackbar snackbar = Snackbar.make(view, msg, duration);
        TextView textView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(SNACK_MAX_LINES);

        if (Build.VERSION.SDK_INT < 25.1)
            //noinspection deprecation
            snackbar.setCallback(eventListener != null ? eventListener : new OnDismissedEvent());
        else
            snackbar.addCallback(eventListener != null ? eventListener : new OnDismissedEvent());

        if(actionListener != null)
            snackbar.setAction(actionTextId, actionListener);

        snackbar.show();

        instance.isShown = true;
        instance.mSnackBar = snackbar;

        return snackbar;
    }

    public static Snack getSnack() {
        return instance;
    }

    public String getMessage() {
        return mMessage;
    }

    public boolean isShown() {
        return isShown;
    }

    public Snackbar getSnackBar() {
        return isShown ? mSnackBar : null;
    }

    /*
     обработчик события исчезновения эвента.
     Клиент, при необходимости собственной обработки, должен сабклассить этот класс вместо Snackbar.Callback
     */
    public static class OnDismissedEvent extends Snackbar.Callback  {
        @Override
        public void onDismissed(Snackbar transientBottomBar, int event) {
            super.onDismissed(transientBottomBar, event);
            if (event != Snackbar.Callback.DISMISS_EVENT_CONSECUTIVE)
                instance.isShown = false;
        }
    }
}
