package com.mishuto.todo.view.spinner;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.common.AbstractActivity;

/**
 * Адаптер для кастомизации спиннера. Служит для того, чтобы использовать в спинере кастомный layout
 * В передаваемом layout (resource) должен быть TextView (textViewResourceId) в котором отображается переданный
 * объект из массива T[] objects с помощью метода toString.
 * Метод toString для T object должен быть определен!
 * Для более сложных случаев, надо создать класс-потомок и переопределить метод getCustomView либо методы
 * getCustomView и getView
 *
 * Created by Michael Shishkin on 23.11.2016.
 */

public class SpinnerAdapter<T> extends ArrayAdapter<T> {

    private int mLayoutId;      // id файла из res\layout
    private int mTextViewId;    // id TextView основного элемента TextView, отображаемого спиннером
    private T mSelected;        // текущий выбранный элемент


    /**
     * Короткий конструктор. При создании используется дефолтный шаблона спинера
     * @param objects массив объектов, отображаемых в спинере, которые хранит адаптер
     * @param selected выбранный объект по умолчанию
     */
    @SuppressWarnings("WeakerAccess")
    public SpinnerAdapter(T[] objects, T selected) {
        this(R.layout.item_dropdown, android.R.id.text1, objects, selected);
    }

    /**
     * Полный конструктор
     * @param resource id файла из res\layout
     * @param textViewResourceId id TextView основного элемента TextView, отображаемого спиннером
     * @param objects массив объектов, отображаемых в спинере, которые хранит адаптер
     */
    public SpinnerAdapter(int resource, int textViewResourceId, T[] objects, T selected) {
        super(AbstractActivity.getContext(), resource, textViewResourceId, objects);
        mLayoutId = resource;
        mTextViewId = textViewResourceId;
        mSelected = selected;
    }

    T getSelected() {
        return mSelected;
    }

    void setSelectedPosition(int position) {
        mSelected = getItem(position);
    }

    int getSelectedPosition() {
        return getPosition(mSelected);
    }

    // метод вызываемый для отрисовки элемента при открытии спинера
    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        //return (convertView == null) ? getCustomView(position, parent, true) : convertView;
        return getCustomView(position, parent, true);  // в API25 строка выше порождает мусор в спинере
    }

    // метод вызываемый при отрисовке спинера на форме
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return (convertView == null) ? getCustomView(position, parent, true) : convertView;
    }

    /*
    * отрисовка элемента objects[position]
    * по умолчанию вызывается метод toString и передается в TextView.
    * Если этого недостаточно, необходимо переопределить метод
    */
    protected View getCustomView(int position, ViewGroup parent, boolean isView){
        View root = getRoot(parent);
        //noinspection ConstantConditions
        getTextViewItem(root).setText(getItem(position).toString());
        return root;
    }

    // получение корневого view
    protected View getRoot(ViewGroup parent) {
        LayoutInflater inflater = AbstractActivity.getCurrentActivity().getLayoutInflater();

        return inflater.inflate(mLayoutId, parent, false);
    }

    // получение TextView
    protected TextView getTextViewItem(View parent) {
        return (TextView) parent.findViewById(mTextViewId);
    }
}
