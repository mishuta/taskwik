package com.mishuto.todo.view;

import android.widget.SeekBar;

/**
 * Обертка для виджета SeekBar
 * Created by Michael Shishkin on 29.10.2016.
 */

public class SeekWidget implements SeekBar.OnSeekBarChangeListener
{
    public interface SeekWidgetListener {
        void onProgressChanged(int progress);
    }

    private SeekBar mSeekBar;
    private SeekWidgetListener mListener;

    public SeekWidget(SeekBar seekBar, int startProgress, SeekWidgetListener listener) {
        mSeekBar = seekBar;
        mSeekBar.setProgress(startProgress);
        mListener = listener;

        mSeekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser)
            mListener.onProgressChanged(progress);
    }

    public SeekBar getSeekBar() {
        return mSeekBar;
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}
}
