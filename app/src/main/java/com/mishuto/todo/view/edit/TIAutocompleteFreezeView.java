package com.mishuto.todo.view.edit;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.mishuto.todo.todo.R;

/**
 * CompaundView: TextView с TextInputLayout+автокомплит
 * Created by Michael Shishkin on 24.11.2018.
 */
public class TIAutocompleteFreezeView extends AbstractTextInputFreezeView {

    public TIAutocompleteFreezeView(Context context) {
        this(context, null);
    }

    public TIAutocompleteFreezeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TIAutocompleteFreezeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr, R.layout.cv_input_text_autocomplete_view, R.id.textView);
    }

    @Override
    public AutoCompleteTextView getEditTextView() {
        return (AutoCompleteTextView) mInputLayout.getEditText();
    }

    // инициализация автокомплит-адаптера для EditView
    public <T> void setAutoCompleteAdapter(T[] array) {
        ArrayAdapter<T> adapter = new ArrayAdapter<>(getContext(), R.layout.item_dropdown, array);
        getEditTextView().setAdapter(adapter);
    }
}
