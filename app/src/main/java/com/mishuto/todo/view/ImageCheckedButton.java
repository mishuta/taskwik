package com.mishuto.todo.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;

import com.mishuto.todo.todo.R;

/**
 * Класс custom view управляющий виджетом-кнопкой, имеющей два состояние: checked/unchecked
 * Аналогичен ToggleButton, но построен на ImageView и имеет соответствующие атрбуты для задания checked и unchecked ресурсов иконок
 * Created by Michael Shishkin on 18.11.2017.
 */

public class ImageCheckedButton extends AppCompatImageView implements View.OnClickListener, CheckedView {

    private int mCheckedRes;                            // ресурс checked иконки
    private int mUncheckedRes;                          // ресурс unchecked иконки
    private boolean isChecked;                          // состояние кнопки
    private OnChangeStateListener mChangeStateListener; // слушатель события изменения состояния кнопки
    private OnClickListener mClickListener;             // слушатель события нажатия на view. Служит для встраивания в цепочку перехватчиков клика

    public ImageCheckedButton(Context context) {
        this(context, null);
    }

    public ImageCheckedButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageCheckedButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // загрузка xml-атрибутов
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ImageCheckedButton);
        mCheckedRes = typedArray.getResourceId(R.styleable.ImageCheckedButton_checkedImageSource, 0);
        mUncheckedRes = typedArray.getResourceId(R.styleable.ImageCheckedButton_uncheckedImageSource, 0);
        isChecked = typedArray.getBoolean(R.styleable.ImageCheckedButton_checked, false);
        typedArray.recycle();

        super.setOnClickListener(this);
        updateImage();
    }

    @Override
    public void setChecked(boolean checked) {
        if(isChecked != checked ) { // обновление картинки происходит только если необходимо
            isChecked = checked;
            updateImage();
        }
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void setOnChangeListener(OnChangeStateListener listener) {
        mChangeStateListener = listener;
   }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        mClickListener = l;
    }

    private void updateImage() {
        setImageResource(isChecked ? mCheckedRes : mUncheckedRes);
   }

    @Override
    public void onClick(View v) {
        isChecked = !isChecked;
        updateImage();

        if(mChangeStateListener != null)
            mChangeStateListener.onChangeState(this, isChecked);

        if(mClickListener != null)      // передаем onClick по цепочке
            mClickListener.onClick(v);
    }
}
