package com.mishuto.todo.view;

import android.widget.CompoundButton;
import android.widget.Switch;

import com.mishuto.todo.common.Accessor;

/**
 * Декоратор виджета Switch, связывающий виджет с объектом Boolean модели
 * Created by Michael Shishkin on 16.07.2018.
 */
public class SwitchWidget implements CompoundButton.OnCheckedChangeListener {

    private Switch mSwitch;
    private Accessor<Boolean> mAccessor;
    private boolean isManualChange = true;

    public SwitchWidget(Switch aSwitch, Accessor<Boolean> accessor) {
        mSwitch = aSwitch;
        mAccessor = accessor;
        mSwitch.setOnCheckedChangeListener(this);
        update();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isManualChange)
            mAccessor.set(isChecked);
    }

    public void update() {
        isManualChange = false;
        mSwitch.setChecked(mAccessor.get());
        isManualChange = true;
    }

    public Switch getSwitch() {
        return mSwitch;
    }

    // переключение свича
    public void flip() {
        mSwitch.setChecked(!mAccessor.get());
    }
}
