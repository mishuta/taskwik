package com.mishuto.todo.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.mishuto.todo.todo.R;

/**
 * Виджет CheckedView, реализованный на Button
 * Представляет собой FrameLayout, содержащий две Button, одну видимую, другую невидимую
 * Использует два произвольных layout ("нажатый" и "отжатый") для создания нажатой и отжатой кнопки
 * Created by Michael Shishkin on 28.10.2018.
 */
public class CheckedButton extends FrameLayout implements CheckedView, View.OnClickListener {

    private boolean isChecked;
    private Button mCheckedButton;
    private Button mUnCheckedButton;
    private OnChangeStateListener mChangeStateListener;

    public CheckedButton(@NonNull Context context) {
        this(context, null);
    }

    public CheckedButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CheckedButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // загрузка xml-атрибутов
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CheckedButton);
        int checkedLayout = typedArray.getResourceId(R.styleable.CheckedButton_checkedButtonLayout, 0);
        int unCheckedLayout = typedArray.getResourceId(R.styleable.CheckedButton_uncheckedButtonLayout, 0);
        isChecked = typedArray.getBoolean(R.styleable.CheckedButton_checked, false);
        String text = typedArray.getString(R.styleable.CheckedButton_text);
        typedArray.recycle();

        // Создаем кнопки в FrameLayout
        mCheckedButton = (Button) LayoutInflater.from(getContext()).inflate(checkedLayout, this, false);
        mUnCheckedButton = (Button)LayoutInflater.from(getContext()).inflate(unCheckedLayout, this, false);
        addView(mCheckedButton);
        addView(mUnCheckedButton);

        mCheckedButton.setOnClickListener(this);
        mUnCheckedButton.setOnClickListener(this);

        if(text != null)
            setText(text);  // надпись на конпке

        updateWidget();
    }

    @Override
    public void setChecked(boolean checked) {
        if(checked != isChecked) {
            isChecked = checked;
            updateWidget();
            if(mChangeStateListener != null)
                mChangeStateListener.onChangeState(this, isChecked);
        }
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void setOnChangeListener(OnChangeStateListener listener) {
        mChangeStateListener = listener;
    }

    // надпись на кнопке
    public void setText(String text) {
        mCheckedButton.setText(text);
        mUnCheckedButton.setText(text);
    }

    @Override
    public void onClick(View v) {
        setChecked(!isChecked);
    }

    private void updateWidget() {
        mCheckedButton.setVisibility(isChecked ? VISIBLE : GONE);
        mUnCheckedButton.setVisibility(isChecked ? GONE : VISIBLE);
    }
}
