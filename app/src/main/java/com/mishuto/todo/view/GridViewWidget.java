package com.mishuto.todo.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;

import java.util.List;

/**
 * Виджет на базе GridView, для отображения таблицы, использующей List в качестве data source
 * Created by Michael Shishkin on 10.12.2017.
 */

public class GridViewWidget<T> implements AdapterView.OnItemClickListener {

    public interface EventsListener<T> {
        void onBind(View root, T item);
        void onClickItemView(View view, T item);
    }

    private List<T> mDataSource;
    private EventsListener<T> mListener;
    private int mLayoutResId;   // ресурс layout элемента грида

    public GridViewWidget(GridView gridView, int layoutItemResId, List<T> dataSource, EventsListener<T> listener) {
        mDataSource = dataSource;
        mListener = listener;
        mLayoutResId = layoutItemResId;

        gridView.setAdapter(createAdapter());
        gridView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(mListener != null)
            mListener.onClickItemView(view, mDataSource.get(position));
    }

    protected Adapter createAdapter() {
        return new Adapter();
    }

    protected class Adapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mDataSource.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View root = convertView == null ? createItemView(position, parent, mLayoutResId) : convertView;

            if(mListener != null)
                mListener.onBind(root, mDataSource.get(position));

            return root;
        }

        // создание view
        protected View createItemView(int position, ViewGroup parent, int layoutResId) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());   // получаем объект LayoutInflater
            return layoutInflater.inflate(layoutResId, parent, false);                  // создаем View по заданному шаблону
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return mDataSource.get(position);
        }
    }
}
