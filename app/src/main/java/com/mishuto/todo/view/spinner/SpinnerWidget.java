package com.mishuto.todo.view.spinner;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

/**
 * Обертка для spinner widget
 * По умолчанию используется item_dropdown.xml в качестве layout и SpinnerAdapter в качестве адаптера.
 * layout и адаптер можно переопределить.
 *
 * Created by Michael Shishkin on 19.11.2016.
 */

public class SpinnerWidget<T> implements Spinner.OnItemSelectedListener {


    public interface OnChangeSelectionListener<T> {
        /**
         * колбэк при выборе элемента спинера
         * @param item выбранный объект в спиннере
         * @param index индес выбранного объекта в спиннере
         * @param spinnerId View.id спиннера
         */
        void onChangeSelection(T item, int index, int spinnerId);
    }

    private Spinner mSpinner;                       // виджет спинера
    private OnChangeSelectionListener<T> mListener; // лисенер, передаваемый вызываемым объектом
    protected SpinnerAdapter<T> mAdapter;           // адаптер спинера

    /**
     * Короткий конструктор для дефолтного шаблона и дефолтного адаптера. Используется в большинстве случаев
     * @param spinner виджет
     * @param array массив объектов
     * @param initItem  начальный выбранный объект
     * @param listener  вызывающий объект, обрабатывающий событие смены элемента в списке. Если null, то не вызывается
     */
    public SpinnerWidget(Spinner spinner, T[] array, T initItem, @Nullable OnChangeSelectionListener<T> listener) {
        this(spinner, listener);
        setAdapter(new SpinnerAdapter<>(array, initItem)); // создание адаптера с дефолтным шаблоном
    }


    /**
     * Конструктор для кастомного шаблона и дефолтного адаптера
     * @param spinner виджет
     * @param array массив объектов
     * @param initItem  начальный выбранный объект
     * @param listener  вызывающий объект, обрабатывающий событие смены элемента в списке
     * @param layoutId ресурс layout xml
     * @param textViewId id TextView в layout
     */
    public SpinnerWidget(Spinner spinner, T[] array, T initItem, @Nullable OnChangeSelectionListener<T> listener, int layoutId, int textViewId) {
        this(spinner, listener);
        setAdapter(new SpinnerAdapter<>(layoutId, textViewId, array, initItem));
    }

    /**
     * Конструктор для кастомного адаптера (в этом случае шаблон задается в адаптере)
     * @param spinner виджет
     * @param listener  вызывающий объект, обрабатывающий событие смены элемента в списке
     * @param adapter кастомный адаптер SpinnerAdapter
     */
    public SpinnerWidget(Spinner spinner, @Nullable OnChangeSelectionListener<T> listener, SpinnerAdapter<T> adapter){
        this(spinner, listener);

        setAdapter(adapter);
    }


    /**
     * Общий для всех конструктор. Не создает адаптер, поэтому не должен быть использован напрямую, а должен вызваться из другого конструктора
     * @param spinner виджет
     * @param listener вызывающий объект, обрабатывающий событие смены элемента в списке. Если null, то не вызывается
     */
    @SuppressWarnings("WeakerAccess")
    protected SpinnerWidget(Spinner spinner, @Nullable OnChangeSelectionListener<T> listener ) {
        mSpinner = spinner;
        mListener = listener;

        spinner.setOnItemSelectedListener(this);
    }

    // Биндинг адаптера к спинеру
    public void setAdapter(SpinnerAdapter<T> adapter) {
        mAdapter = adapter;
        mSpinner.setAdapter(mAdapter);
        mSpinner.setSelection(mAdapter.getSelectedPosition());
    }


    // спинер виджет
    public Spinner getSpinner() {
        return mSpinner;
    }

    public SpinnerAdapter<T> getAdapter() {
        return mAdapter;
    }

    // Текущий выбранный объект
    public T getSelectedItem() {
        return mAdapter.getSelected();
    }

    // установить элемент как выбранный в спинере
    public void setSelected(T item) {
        int position = mAdapter.getPosition(item);
        if(position == -1)
            throw new AssertionError("Item " + item.toString() + " not found in spinner");

        mAdapter.setSelectedPosition(position);
        mSpinner.setSelection(position);
    }

    // Обработка смены элемента в спинере
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(mAdapter.getSelectedPosition() == position)   // фильтрация ложного срабатывания
            return;

        mAdapter.setSelectedPosition(position);

        if (mListener != null)
            mListener.onChangeSelection(mAdapter.getSelected(), position, mSpinner.getId());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}
}
