package com.mishuto.todo.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import static com.mishuto.todo.view_model.common.AbstractActivity.getContext;

/**
 * Библиотека методов для работы с виджетами
 * Created by Michael Shishkin on 16.02.2019.
 */
public class ViewCommon {

    public interface OnBackPressListener {
        void onBackPress();
    }

    public static void setBackPressListener(View view, OnBackPressListener listener) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener((v1, keyCode, event) -> {
            if( keyCode == KeyEvent.KEYCODE_BACK  && event.getAction() == KeyEvent.ACTION_UP && listener != null ) {
                listener.onBackPress();
                return true;
            }
            return false;
        });
    }

    // преобразовывает идентификатор цвета в ресурсах в значение цвета для установки цвета в виджетах
    public static int color(int colorId) {
        return ContextCompat.getColor(getContext(), colorId);
    }

    // Передать фокус виджету и отобразить клавиатуру
    public static void showKeyboard(View v) {
        v.requestFocus();
        InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);

    }

    // Убрать фокус у виджета и скрыть клавиатуру
    public static void hideKeyboard(View v) {
        v.clearFocus();
        InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}
