package com.mishuto.todo.view;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Статические методы для работы с групповыми View
 * Created by Michael Shishkin on 08.12.2017.
 */
public class LayoutWrapper {

    // Создает итератор по всем дочерним несоставным View внутри данного layout
    public static Iterable<View> createLayoutIterator(final View layout) {
        return new Iterable<View>() {
            @NonNull
            @Override
            public Iterator<View> iterator() {
                return new LayoutIterator(layout);
            }
        };
    }

    // Определяет, является ли child потомком parent
    @SuppressWarnings("SimplifiableIfStatement")
    public static boolean isParent(ViewGroup parent, View child) {
        ViewParent viewParent = child.getParent();
        if(!(viewParent instanceof ViewGroup))
            return false;

        if(viewParent == parent)
            return true;

        return isParent(parent, (View) viewParent);
    }

    // итератор для ViewGroup
    private static class LayoutIterator implements Iterator<View> {

        private Deque<LayoutElement> mStack = new LinkedList<>();   // стек для сохранения текущего состояния итератора
        private boolean mReadAhead = false;                         // флаг: считан ли уже следующий элемент
        private View mNext;                                         // хранит следующий элемент, если mReadAhead установлен

        // класс для сохранения Layout текущего уровня вложенности в стеке
        private static class LayoutElement {
            private ViewGroup mLayout;  // текущий Layout
            private int mPosition;      // номер текущего дочернего элемента

            LayoutElement(ViewGroup layout) {
                mLayout = layout;
                mPosition = 0;
            }

            View getChild() {
                return mPosition == mLayout.getChildCount() ? null : mLayout.getChildAt(mPosition++);
            }
        }

        // В конструкторе в стек кладем корневой layout
        private LayoutIterator(View rootLayout) {
            if (rootLayout instanceof ViewGroup)
                mStack.push(new LayoutElement((ViewGroup) rootLayout));
        }

        @Override
        public View next() {
            if (mReadAhead) {                // Если следующий уже прочитан
                mReadAhead = false;         // сбрасываем флаг
                return mNext;               // возвращаем сохраненное значение
            }

            // Получение следующего значения
            View next;
            boolean isChildALayout;

            do {                                                                        // нисходящий цикл. Спускается вниз по всем стартующим групповым view
                do {                                                                        // восходящий цикл. Поднимается вверх по исчерпанным групповым view
                    if (mStack.isEmpty())                                                   // если стек исчерпан - итератор дошел до конца
                        return null;

                    LayoutElement element = mStack.peek();                                  // берем layout из вершины стека, без изъятия
                    next = element.getChild();                                              // получаем его потомка в текущей для layout позициии

                    if (next == null)                                                       // если перебрали уже всех детей (или layout - пустой)
                        mStack.pop();                                                       // удаляем его из стека, поднимаемся вверх по иерархии
                }
                while (next == null);                                                       // до тех пор, пока наткнемся на первый неисчерпанный layout (или пустой стек)

                isChildALayout = next instanceof ViewGroup &&                               // Определяем, является ли дочерний элемент составным
                        !(next instanceof Spinner) &&                                       // при этом, виджеты, наследующие от ViewGroup
                        !(next instanceof DatePicker) &&                                    // считаем атомарными
                        !(next instanceof TimePicker);

                if (isChildALayout)
                    mStack.push(new LayoutElement((ViewGroup) next));                   // Если да - сохраняем его в стеке ...

            }
            while (isChildALayout);                                                      // и спускаемся на следующий уровень

            return next;                                                                // иначе - возвращаем готовый элемент
        }

        @Override
        public boolean hasNext() {
            if (!mReadAhead) {       // Если следующий элемент еще не был прочитан (hasNext() еще не вызывался после последнего next() )
                mNext = next();     // получаем следующий элемент
                mReadAhead = true;
            }

            return mNext != null;   // если следующий элемент не null, то он есть
        }
    }
}
