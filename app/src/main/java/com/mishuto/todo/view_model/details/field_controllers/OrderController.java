package com.mishuto.todo.view_model.details.field_controllers;

import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.task_fields.Order;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.SeekWidget;

/**
 * Класс-контроллер для управления Очередностью на форме DetailsFragment
 * Created by Michael Shishkin on 29.10.2016.
 */

public class OrderController extends AbstractEditableController implements SeekWidget.SeekWidgetListener
{
    private Task mTask;                 // модель
    private ImageView mOrderIcon;       // графический значок Очереди

    public OrderController(View view, Task t) {
        mOrderIcon = view.findViewById(R.id.taskDetails_image_order);
        TextView label = view.findViewById(R.id.taskDetails_label_order);
        SeekBar seekBar = view.findViewById(R.id.taskDetails_seekBar_order);

        mTask = t;
        SeekWidget seekWidget = new SeekWidget(seekBar, t.getOrder().ordinal(), this);

        updateWidgets();
        setPerformChangedViews(seekWidget.getSeekBar(), label);
        onPerformEvent(t.isStateInPerformed());
    }

    @Override
    public void onProgressChanged(int progress) {
        mTask.setOrder(Order.values()[progress]);
        updateWidgets();
    }

    // Обновление значка очереди на форме
    @Override
    public void updateWidgets(){
        mOrderIcon.setImageLevel(mTask.getOrder().ordinal());
    }
}
