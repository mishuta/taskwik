package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.model.task_filters.Choice;
import com.mishuto.todo.model.task_filters.ChoiceTagFilter;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.edit.AbstractCompoundEditFreezeView;
import com.mishuto.todo.view.edit.EditFreezeView;
import com.mishuto.todo.view.edit.EditView;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Фрагмент, отображающий абстрактный тэгфильтр для списка задач. Конкретные тэги (проекты, контексты, исполнители) определяются в сабклассах
 * Фрагмент является частью диалога, создаваемого в классе TabbedDialog
 * Created by Michael Shishkin on 23.06.2017.
 */
public abstract class AbstractFilterTagFragment<S extends DBObject>
        extends AbstractFilterFragment<S, StringContainer, ChoiceTagFilter<S>>
        implements FloatingActionButton.OnClickListener
{
    String mDefaultTagName;                                             //дефолтное название тега при его создании
    private boolean mJustAdded = false;                                 //флаг, устанавливается после нажатия на FAB
    private ViewHolderActions mEditingTag;                              //текущий редактируемый тег или null, если тег не редактируется

    public AbstractFilterTagFragment(int lineLayout, ChoiceTagFilter<S> tagFilter, String defaultTagName) {
        super(lineLayout, tagFilter);
        mDefaultTagName = defaultTagName;
    }

    @Override
    public void onPageSelected() {
        super.onPageSelected();
        getParentDialog().setFABListener(this);
    }

    @Override
    public void onBind(View root, Choice.Item<StringContainer> item, RecyclerViewWidget<Choice.Item<StringContainer>>.ViewHolder viewHolder) {
        super.onBind(root, item, viewHolder);

        // отображаем меню и устанавливаем слушатель
        root.findViewById(R.id.popup_menu).setVisibility(viewHolder.getItem().isMissingItem() ? INVISIBLE: VISIBLE);
        root.findViewById(R.id.popup_menu).setOnClickListener(viewHolder);

        if(mJustAdded) {                                                    // если была нажата FAB
            ViewHolderActions actions = new ViewHolderActions(viewHolder.getItem().getValue(), viewHolder.getRootView());
            actions.editTitle();                                            // переводим тег в режим редактирования
            getTitleView(root).setText("");                                 // сбрасываем название
            mJustAdded = false;                                             // сбрасываем флаг
        }
    }

    void showMenu(View menuButton, RecyclerViewWidget<Choice.Item<StringContainer>>.ViewHolder viewHolder) {
        //noinspection ConstantConditions
        final PopupMenu popupMenu = new PopupMenu(getContext(), menuButton);
        popupMenu.inflate(R.menu.fs_dlg_tag_menu);
        popupMenu.setOnMenuItemClickListener(new ViewHolderActions(viewHolder.getItem().getValue(), viewHolder.getRootView()));
        popupMenu.show();                                                                                       // показываем popup меню
        popupMenu.getMenu().findItem(R.id.menu_remove).setEnabled(getItemCount(viewHolder.getItem()) == 0);    // "Удалить" доступна только для тегов с 0 задач
    }

    // обработка нажатия FAB
    @Override
    public void onClick(View v) {
        if(mEditingTag != null) {       // если какой-то тег редактируется
            mEditingTag.toViewMode();   // нажатие FAB закрывает редактирование, но не добавляет новый тег
            return;
        }

        List<Choice.Item<StringContainer>> items = mRecyclerViewWidget.getDataList();
        StringContainer newTag = new StringContainer(TextUtils.uniqueStr(items, mDefaultTagName));

        mFilter.getTagCollection().add(newTag);                                    // добавляем к списку тегов новое уникальное название

        mRecyclerViewWidget.add(mFilter.createItem(newTag));     // добавляем его в конец списка виджета
        mJustAdded = true;                                              // устанавливаем флаг для обрабоотки нового тега в onBind
    }

    // События изменения тега

    void onEditStart(ViewHolderActions editingTag) {
        mEditingTag = editingTag;
    }

    void onEditComplete(ViewHolderActions editingTag) {
        mEditingTag = null;
    }

    void onDelete(ViewHolderActions editingTag) {
    }

    // Класс для обработки действий над элементом списка RecyclerView
    class ViewHolderActions implements
            PopupMenu.OnMenuItemClickListener,
            EditView.OnChangeListener,
            AbstractCompoundEditFreezeView.AutoChangeModeListener
    {
        StringContainer mTag;
        View mRoot;

        ViewHolderActions(StringContainer tag, View root) {
            mTag = tag;
            mRoot = root;
        }

        // перевод виджета в режим редактирования, установка лисенеров
        private void editTitle() {
            onEditStart(this);
            EditFreezeView titleView = (EditFreezeView) getTitleView(mRoot);
            titleView.setEditMode();
            titleView.setAutoChangeModeListener(this);
            titleView.getEditView().setListener(this);
        }

        private String getItemTitle() {
            return mTag.toString();
        }

        // обработка нажатия пунктов меню
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_edit:
                    editTitle();
                    return true;

                case R.id.menu_remove:
                    mFilter.getTagCollection().remove(mTag);
                    mRecyclerViewWidget.deleteAt(mRecyclerViewWidget.getPosition(mFilter.createItem(mTag)));
                    onDelete(this);
                    return true;
            }
            return false;
        }

        // валидация ввводимого текста: не пустой и не равен другому тегу
        @Override
        public boolean onTextValidate(EditView view, String text) {
            return !TextUtils.empty(text) && !( mFilter.getTagCollection().contains(text) && !text.equals(getItemTitle()) );
        }

        // завершение ввода
        @Override
        public void onAutoChangeMode(AbstractCompoundEditFreezeView view, boolean toViewMode) {
            if(toViewMode)
                onViewMode((EditFreezeView) view);
        }

        //завершение редактирования
        void toViewMode() {
            EditFreezeView titleView = (EditFreezeView) getTitleView(mRoot);
            if(titleView.isEditMode()) {
                titleView.setViewMode();
                onViewMode(titleView);
            }
        }

        // сохранение изменения тега
        void onViewMode(EditFreezeView view) {
            onEditComplete(this);

            if (view.getText().equals(getItemTitle()))                              // если тег не изменился, делать ничего не надо
                return;

            if (!mFilter.getTagCollection().rename(mTag, view.getText())) {                    // пытаемся переименовать тег
                Snack.show(getView(), R.string.tagNotAllowed);                      // если неуспешно - показываем предупреждение
                view.setText(getItemTitle());                                       // отображаем новое сгенерированное название
            }
        }
    }
}