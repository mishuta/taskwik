package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;

/**
 * Диалог фильтра исполнителей
 * Created by Michael Shishkin on 11.08.2017.
 */

public class ExecutorFilterFragment extends SingleSelectFilterFragment {

    private int mCleared = 0;   // количество очищенных исполнителей при открытии фильтра

    public ExecutorFilterFragment() {
        super(FilterManager.executor(), ResourcesFacade.getString(R.string.def_executor_name));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mCleared = Executors.getInstance().clearUnused();   // перед отображением списка исполнителей удаляем редкоиспользуемых
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        if( mCleared > 0 ) {    // Если редкоиспользуемые исполнители были удалены, то выводим снек
            //noinspection ConstantConditions
            Snack.show(getView(), getView().getResources().getQuantityString(R.plurals.executorsCleared_plural, mCleared, mCleared));
            mCleared = 0;
        }
    }
}
