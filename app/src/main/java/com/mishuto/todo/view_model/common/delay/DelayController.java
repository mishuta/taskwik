package com.mishuto.todo.view_model.common.delay;

import android.os.Bundle;

import com.mishuto.todo.model.Delayer;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.dialog.DialogHelper;

import static android.app.Activity.RESULT_OK;
import static com.mishuto.todo.common.BundleWrapper.getSerializable;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.view_model.common.AbstractActivity.getCurrentActivity;

/**
 * Контроллер для отображения диалога Отложить
 * Created by Michael Shishkin on 23.01.2018.
 */

public class DelayController {

    public static void delayTask(Task task) {
        if(task.isStateInPerformed()) {    // завершенная задача - не откладывается
            Snack.show(getCurrentActivity().getMainFragment().getView(), R.string.task_warn_completed);
            return;
        }

        Delayer delayer = new Delayer();

        // вызов основного диалога
        DialogHelper.createDialog(new DelayDialog(), getCurrentActivity().getMainFragment(), new DelayDialogListener(task, delayer), toBundle(delayer));
    }

    // Класс - обработчик результата для диалогов: DelayDialog и DelayCustomDialog
    private static class DelayDialogListener implements DialogHelper.DialogHelperListener {
        Task mTask;
        Delayer mDelayer;

        DelayDialogListener(Task task, Delayer delayer) {
            mTask = task;
            mDelayer = delayer;
        }

        @Override
        public void onCloseDialog(int resultCode, Bundle resultObject) {
            if(resultCode == RESULT_OK) {
                TaskTime taskTime = (TaskTime) getSerializable(resultObject);

                if(taskTime != null) {  // taskTime может быть null только в диалоге DelayDialog, что означает необходимость вызова DelayCustomDialog
                    mTask.setTaskTime(taskTime);
                    mDelayer.onSelected(taskTime);
                }
                else {                  // вызов вспомогательного диалога
                    DialogHelper.createDialog(new DelayCustomDialog(), getCurrentActivity().getMainFragment(), this, null);
                }
            }
        }
    }
}
