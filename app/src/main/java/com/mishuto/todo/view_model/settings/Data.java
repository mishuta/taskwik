package com.mishuto.todo.view_model.settings;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.TextView;

import com.mishuto.todo.common.BackupManager;
import com.mishuto.todo.common.DateFormatWrapper;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.database.DBOpenHelper;
import com.mishuto.todo.database.SQLHelper;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view_model.common.PermissionController;

import java.io.File;

import static com.mishuto.todo.App.restartApplication;
import static com.mishuto.todo.common.DateFormatWrapper.getFormattedDate;
import static com.mishuto.todo.common.ResourceConstants.Time.DT_FORMAT;
import static com.mishuto.todo.common.TCalendar.now;
import static com.mishuto.todo.view_model.settings.SettingsDialog.IMPORT_CODE;

/**
 * Секция манипуляции с БД диалога Settings
 * Created by Michael Shishkin on 19.07.2018.
 */
public class Data {
    private View mRoot;
    private Activity mActivity;
    private Fragment mDialog;
    private static BackupManager sBackupManager = BackupManager.getInstance();

    private static final String FILE_PROVIDER = "com.mishuto.todo.fileprovider";

    Data(View root, Fragment dialog) {
        mRoot = root;
        mActivity = dialog.getActivity();
        mDialog = dialog;

        // кнопка "Очистить"
        mRoot.findViewById(R.id.settings_clearDB_layout).setOnClickListener(clearAlertDialog());

        // инициализация контролов "Восстановить"
        TextView restoreButton = mRoot.findViewById(R.id.settings_restore);
        TextView restoreLabel = mRoot.findViewById(R.id.settings_restore_label);

        if(sBackupManager.isBackupExists()) {   // на данный момент всегда true
            restoreLabel.setText(mActivity.getString(R.string.restoreData_desc,
                    String.format(DT_FORMAT, DateFormatWrapper.getSpeakingDate(sBackupManager.getBackupDate()),
                            DateFormatWrapper.getTimeString(sBackupManager.getBackupDate()))));

            restoreButton.setOnClickListener(restoreAlertDialog());
            restoreLabel.setOnClickListener(restoreAlertDialog());
        }
        else
            restoreLabel.setText(R.string.no_backup);

        mRoot.findViewById(R.id.settings_export_layout).setOnClickListener(exportDB()); // кнопка экспорт
        mRoot.findViewById(R.id.settings_import_layout).setOnClickListener(importDB()); // кнопка импорт
    }

    // Диалог кнопки "Очистить"
    private View.OnClickListener clearAlertDialog() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mActivity)
                        .setTitle(R.string.clearDBTitle)
                        .setMessage(R.string.clearDBMessage)
                        .setPositiveButton(R.string.buttonClear, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {    // по кнопке очистить
                                sBackupManager.makeBackUp();                            // сохраняем текущую версию в бэкапе
                                if(sBackupManager.deleteDB())
                                    restartApplication(mActivity);                       // перезапускаем приложение
                                else
                                    Snack.show(mRoot, R.string.cantDeleteMsg);          // если база не удалилась, выводим совет
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
            }
        };
    }

    // Диалог кнопки "Восстановить"
    private View.OnClickListener restoreAlertDialog() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mActivity)
                        .setTitle(R.string.restoreTitle)
                        .setMessage(mActivity.getString(R.string.restoreMessage, sBackupManager.getBackupDate().toString()))
                        .setPositiveButton(R.string.buttonRecover, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {    // по кнопке Восстановить
                                if(sBackupManager.swapBackup())                         // меняем базу и бэкап местамии
                                    restartApplication(mActivity);                       // перезапускаем
                                else
                                    Snack.show(mRoot, R.string.cantRestoreMsg);         // если восстановление неуспешно, выводим совет
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
            }
        };
    }

    // отправка файла
    private View.OnClickListener exportDB() {
        return v -> {
            String fileName = DBOpenHelper.DB_NAME + getFormattedDate("-yyyyMMdd", now());
            File db = new File(mActivity.getFilesDir(), fileName);   // новый файл базы в папке files
            if(!sBackupManager.exportDB(db)) {
                Snack.show(mRoot, R.string.errorCopyFile);
                return;
            }

            Uri uri = FileProvider.getUriForFile(mActivity, FILE_PROVIDER, db);

            Intent intent = new Intent(Intent.ACTION_SEND);                     // формируем неявный интент для отправки во внешнее приложение
            intent.setType("application/*");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);             // предоставление временного доступа к файлу

            if (intent.resolveActivity(mActivity.getPackageManager()) != null)   // проверка того что есть кому отправить файл
                mActivity.startActivity(Intent.createChooser(intent, mActivity.getString(R.string.exportChooser)));
            else
                Snack.show(mRoot, R.string.noReceivers);
        };
    }

    // загрузка файла из внешнего источника
    private View.OnClickListener importDB() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                if (intent.resolveActivity(mActivity.getPackageManager()) != null) {    // проверка того, что есть кому отправить файл
                    mDialog.startActivityForResult(intent, IMPORT_CODE);                // стартуем селектор выбора файла. Ркзультат вернется в SettingsDialog

                }
                else
                    Snack.show(mRoot, R.string.noReceivers);
            }
        };
    }

    // колбэка импорта
    void doImport(Uri uri) {
        new Importer(mRoot, mActivity).doImport(uri);
    }

    // Класс, реализующий импорт файла БД из Uri внешнего приложения
    // Объект создается в Активности - получателе интента
    public static class Importer {
        PermissionController mPermissionController;
        Uri mUri;
        View mView;
        Activity mActivity;

        public Importer(View view, Activity activity) {
            mView = view;
            mActivity = activity;
            mPermissionController = new PermissionController(mActivity);
        }

        // выполнить импорт
        public void doImport(Uri uri) {
            mUri = uri;
            //пытаемся запросить пермишн
            if(mPermissionController.tryRequestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, new ReadStoragePermission())) {
                importFromUri(mUri);    // если уже есть, запускаем импорт
            }
        }

        // класс для обработки событий PermissionController
        private class ReadStoragePermission implements PermissionController.PermissionRequest {
            // получение пермишена от пользователя
            @Override
            public void onResult(boolean granted) {
                if(granted)                 // если пользователь дал пермишн - запускаем импорт
                    importFromUri(mUri);
                else                        // иначе ругаемся и не запускаем
                    Snack.show(mView, R.string.no_read_storage_permission);
            }

            // вывод расширенного сообщения о цели пермишена
            @Override
            public void onRationalRequest(String permission) {
                mPermissionController.onRationalRequestImp(permission, mView, R.string.read_storage_permission_rational);
            }
        }

        // импорт потока в БД
        private void importFromUri(Uri uri) {
            final File newDB = new File(mActivity.getFilesDir(), "imported.db");    // временный новый файл базы в /files

            if(!TSystem.loadFile(uri, mActivity.getContentResolver(), newDB))       // пытаемся загрузить в него поток из uri
                Snack.show(mView, R.string.cantGetFile);

            else if(!checkDbFile(newDB))                                            // проверка что загрузили нашу базу
                Snack.show(mView, R.string.noTaskwikDB);

            else
                new AlertDialog.Builder(mActivity)                                  // предупреждение что база будет перезаписана
                    .setTitle(R.string.importDB_title)
                    .setMessage(R.string.importDB_msg)
                    .setPositiveButton(R.string.importDB_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(BackupManager.getInstance().importDB(newDB))         // перезаписываем базу новым файлом
                                restartApplication(mActivity);
                            else
                                Snack.show(mView, R.string.cantRestoreMsg);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
        }

        // проверка того что файл БД является файлом базы taskwik
        private boolean checkDbFile(final File db) {
            try {
                SQLHelper.DBOwner dbOwner = new SQLHelper.DBOwner() {   // открываем базу
                    @Override
                    public SQLiteDatabase getOpenedDB() {
                        return SQLiteDatabase.openDatabase(db.getAbsolutePath(), null, SQLiteDatabase.OPEN_READONLY);
                    }
                };

                // проверяем что в ней есть таблица TASKS_LIST
                return new SQLHelper(dbOwner).doesTableExist(TaskList.get().getSQLTable().getTableName());
            }
            catch (SQLiteException e) {
                TSystem.error(this, e.getMessage());
                return false;
            }
        }
    }
}
