package com.mishuto.todo.view_model.common.delay;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.GridView;
import android.widget.TextView;

import com.mishuto.todo.common.DateFormatWrapper;
import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Delayer;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.GridViewWidget;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import static com.mishuto.todo.common.BundleWrapper.getSerializable;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.common.DateFormatWrapper.getFieldPosition;
import static com.mishuto.todo.common.TCalendar.now;
import static com.mishuto.todo.view.dialog.DialogHelper.doOk;
import static com.mishuto.todo.view.dialog.DialogHelper.inParameter;
import static java.lang.Math.round;
import static java.util.Calendar.DATE;
import static java.util.Calendar.HOUR;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;

/**
 * Диалог Отложить
 * Created by Michael Shishkin on 11.12.2017.
 */

public class DelayDialog extends DialogFragment implements GridViewWidget.EventsListener<TaskTime> {
    private static final int CUSTOM_VIEW_POSITION = 4;  // позиция грида, где должна быть кнопка ручной настройки
    private Delayer mDelayer;                           // объект модели

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //noinspection ConstantConditions
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);    // убрать заголовок в Android 5.1 и ниже
        View root = LayoutInflater.from(getActivity()).inflate(R.layout.dlg_delay, (ViewGroup)getView());
        GridView gridView = root.findViewById(R.id.grid_view);

        mDelayer = (Delayer) getSerializable(inParameter(this));
        List<TaskTime> list = mDelayer.getTargetTimes();
        list.add(CUSTOM_VIEW_POSITION, null);

        new CustomGridViewWidget(gridView, list);

        return root;
    }

    // Установка надписей на кнопках
    @Override
    public void onBind(View root, TaskTime item) {
        if(item != null) {  // если не "ручная" кнопка
            TextView interval = root.findViewById(R.id.interval);
            TextView target = root.findViewById(R.id.target_time);

            interval.setText(speakingInterval(item.getCalendar().intervalFrom(MINUTE, mDelayer.getStartTime()), item.getCalendar()));
            target.setText(formattedTime(item));
        }
    }

    // Обработка клика по элементу грида
    @Override
    public void onClickItemView(View view, TaskTime item) {
        doOk(this, toBundle((Serializable) item));
        getDialog().dismiss();
    }

    // генерация строки - интервала (первая строка на кнопке)
     String speakingInterval(int minutes, TCalendar targetDate) {
        final String[] dayPartName = ResourcesFacade.getStringArray(R.array.day_part);
        final int[] dayPartHours = ResourcesFacade.getIntArray(R.array.day_part_hour);
        final int NIGHT = 0;
        final int MORNING = 1;
        final int DAY = 2;
        final int EVENING = 3;

        if(minutes > 6.5 * 24 * 60)
            return getString(R.string.in_week);                         // "неделя"

        if(targetDate.get(Calendar.DATE) != now().get(Calendar.DATE))   // если целевая дата не сегодня, то интервал выводится в виде даты
            return targetDate.getSpeakingDate();

        if(minutes < 60 )
            return getString((R.string.in_minutes), minutes);

        else if(minutes < 4 * 60 ) {
            int h = round(minutes / 60f);
            return getResources().getQuantityString(R.plurals.hour_plural, h, h);
        }

        int h = targetDate.get(Calendar.HOUR_OF_DAY);

        if (h < dayPartHours[NIGHT] || targetDate.get(HOUR) >= dayPartHours[EVENING])
            return dayPartName[NIGHT];

        else if (h < dayPartHours[MORNING])
            return dayPartName[MORNING];

        else if (h < dayPartHours[DAY])
            return dayPartName[DAY];

        else
            return dayPartName[EVENING];
    }

    // генерация строки - времени (вторая строка на кнопке)
     String formattedTime(TaskTime taskTime) {
        if(taskTime.isTimeSet())
            return taskTime.getCalendar().getFormattedTime();

        else
            return String.format(
                    getString(getFieldPosition(DATE) < getFieldPosition(MONTH) ?  R.string.dd_mmm : R.string.mmm_dd),
                    taskTime.getDay(),
                    DateFormatWrapper.getMonthName(taskTime.getMonth(),
                            true));
    }

    // Т.к используется специальная разметка для одной позиции в гриде, необходимо сабклассить GridViewWidget и Adapter, чтобы переопределить createItemView адаптера
    class CustomGridViewWidget extends GridViewWidget<TaskTime> {
        CustomGridViewWidget(GridView gridView, List<TaskTime> list) {
            super(gridView, R.layout.dlg_delay_item, list, DelayDialog.this);
        }

        @Override
        protected Adapter createAdapter() {
            return new CustomAdapter();
        }

        class CustomAdapter extends Adapter {

            // для позиции CUSTOM_VIEW_POSITION подменяем разметку
            @Override
            protected View createItemView(int position, ViewGroup parent, int layoutResId) {
                LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
                return layoutInflater.inflate(position != CUSTOM_VIEW_POSITION ? layoutResId : R.layout.dlg_delay_item_custom, parent, false);
            }
        }
    }
}
