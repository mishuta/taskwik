package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.database.aggregators.DBTreeSet;
import com.mishuto.todo.model.task_filters.Choice;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.ImageCheckedButton;
import com.mishuto.todo.view.dialog.BaseFullScreenDialog.ToolBar;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import static com.mishuto.todo.view.dialog.BaseFullScreenDialog.ToolBar.ButtonIcon.CANCEL;
import static com.mishuto.todo.view.dialog.BaseFullScreenDialog.ToolBar.ButtonIcon.CONFIRM;
import static com.mishuto.todo.view.dialog.BaseFullScreenDialog.ToolBar.ButtonIcon.NONE;
import static com.mishuto.todo.view.dialog.BaseFullScreenDialog.ToolBar.ButtonIcon.UP;

/**
 * Диалог, управляющий списком контекстов.
 *
 * Created by Michael Shishkin on 29.07.2017.
 */

public class ContextFilterFragment extends AbstractFilterTagFragment<DBTreeSet<StringContainer>>
{
    DBTreeSet<StringContainer> mSelectedContexts;    // множество отобранных контекстов

    public ContextFilterFragment() {
        super(R.layout.fs_dlg_filters_item_context, FilterManager.context(), ResourcesFacade.getString(R.string.def_context_name));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        onInit();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onPageSelected() {
        super.onPageSelected();
        onInit();
    }

    // инициализация. Может вызываться как из onCreateView, так и из onPageSelected - что вызовется раньше. Может вызываться многократно при скроллинге страниц
    private void onInit() {
        if(mSelectedContexts == null)
            mSelectedContexts = mFilter.getSelector();  // инициируем диалог копией списка контекстов из фильтра. mSelectedContexts не создается в базе (RecordId==0), поэтому любые изменения не сохраняются в БД
        updateToolbar();
    }

    @Override
    public void onBind(View root, Choice.Item<StringContainer> item, RecyclerViewWidget<Choice.Item<StringContainer>>.ViewHolder viewHolder) {
        super.onBind(root, item, viewHolder);
        ImageCheckedButton checkedButton = root.findViewById(R.id.check);
        checkedButton.setOnClickListener(viewHolder);
        checkedButton.setChecked(mSelectedContexts.contains(item.getValue()));
    }

    // клик по строке списка
    @Override
    public void onClick(View view, RecyclerViewWidget<Choice.Item<StringContainer>>.ViewHolder viewHolder) {
        if(view.getId() == R.id.popup_menu)
            showMenu(view, viewHolder);

        else if(view.getId() == R.id.check) {
            ImageCheckedButton checkedButton = viewHolder.getRootView().findViewById(R.id.check);

            if(checkedButton.isChecked())
                mSelectedContexts.add(viewHolder.getItem().getValue());
            else
                mSelectedContexts.remove(viewHolder.getItem().getValue());

            updateToolbar();
        }

        else {
            mSelectedContexts.clear();
            mSelectedContexts.add(viewHolder.getItem().getValue());
            saveExit();
        }
    }

    // клик по кнопке тулбара
    @Override
    public void onClick(ToolBar.ButtonIcon buttonIcon) {
        if(buttonIcon == UP)
            onUpBtnClick();

        else if(buttonIcon == CONFIRM)
            saveExit();

        else if(buttonIcon == CANCEL) {
            mSelectedContexts.clear();
            mRecyclerViewWidget.refreshList();
            updateToolbar();
        }
    }

    // удаление тега - обновляет тулбар
    @Override
    void onDelete(ViewHolderActions editingTag) {
        super.onDelete(editingTag);
        StringContainer deletedTag = editingTag.mTag;
        if(mSelectedContexts.contains(deletedTag)) {
            mSelectedContexts.remove(deletedTag);
            updateToolbar();
        }
    }

    private void saveExit() {
        mFilter.setSelector(mSelectedContexts); // сохраняем в селекторе отобранные контексты. Старый селектор удаляется из БД, а этот - создается.
        mFilter.setFilter(mSelectedContexts != null && !mSelectedContexts.isEmpty());
        closeDialog();
    }

    // вид тулбара зависит от того, выбраны ли элементы списка (мультиселект) или нет (синглселект)
    private void updateToolbar() {
        ToolBar toolBar = getParentDialog().getToolBar();
        if(mSelectedContexts.isEmpty()) {
            toolBar.setLeftButton(UP);
            toolBar.setRightButton(NONE);
            toolBar.setTitle(R.string.filtersDlg_title);
        }
        else {
            toolBar.setLeftButton(CANCEL);
            toolBar.setRightButton(CONFIRM);
            toolBar.setTitle(String.valueOf(mSelectedContexts.size()));
        }
    }
}
