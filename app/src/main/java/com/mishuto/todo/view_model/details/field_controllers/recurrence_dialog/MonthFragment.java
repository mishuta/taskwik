package com.mishuto.todo.view_model.details.field_controllers.recurrence_dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.mishuto.todo.model.task_fields.recurrence.Month;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.spinner.SpinnerWidget;

/**
 * Дочерний (nested) фрагмент ContainerDialog, отображающий секцию Recurrence.Month
 *
 * Created by Michael Shishkin on 05.12.2016.
 */

public class MonthFragment extends BaseMonthYearFragment<Month> {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View childView = inflater.inflate(R.layout.dlg_rec_page_month, container, false);

        onCreateView(childView);

        Spinner monthFold = childView.findViewById(R.id.rec_mon_foldMonth);
        new SpinnerWidget<>(monthFold, Month.MonthlyFactor.values(), mSegment.getMonthFactor(),
                new SpinnerWidget.OnChangeSelectionListener<Month.MonthlyFactor>() {
                    @Override
                    public void onChangeSelection(Month.MonthlyFactor item, int index, int spinnerId) {
                        mSegment.setMonthFactor(item);
                        onUpdate(true);
                    }
                });

        return childView;
    }
}
