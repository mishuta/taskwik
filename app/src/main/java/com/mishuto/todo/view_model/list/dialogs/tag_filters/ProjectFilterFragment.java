package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;

/**
 * Диалог фильтра проектов
 * Created by Michael Shishkin on 11.08.2017.
 */
public class ProjectFilterFragment extends SingleSelectFilterFragment {

    public ProjectFilterFragment() {
        super(FilterManager.project(), ResourcesFacade.getString(R.string.def_project_name));
    }
}
