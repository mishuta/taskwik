package com.mishuto.todo.view_model.common;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mishuto.todo.model.Help;
import com.mishuto.todo.todo.BuildConfig;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.dialog.BaseFullScreenDialog;

import static com.mishuto.todo.common.TSystem.getLocaleCode;

/**
 * Диалог отображения Справки
 * Created by Michael Shishkin on 24.06.2018.
 */
public class HelpDialog extends BaseFullScreenDialog {

    static final Help STRUCTURE = new Help(R.raw.help, getLocaleCode());   // данные справки, загруженные из файла

    public HelpDialog() {
        super(R.layout.fs_dlg_toolbar_part, R.layout.fs_dlg_help);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        getToolBar().setRightButton(ToolBar.ButtonIcon.NONE);
        getToolBar().setTitle(R.string.listMenu_help);

        // закрыть диалог по кнопке назад
        getToolBar().setButtonClickListener(new ToolBar.OnButtonClickListener() {
            @Override
            public void onClick(ToolBar.ButtonIcon buttonIcon) {
                getDialog().dismiss();
            }
        });

        // отправить email по клику на feedback
        //noinspection ConstantConditions
        root.findViewById(R.id.feedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFeedback();
            }
        });

        //noinspection ConstantConditions
        ViewGroup helpLayout = root.findViewById(R.id.help_list_layout);

        // добавляем в helpLayout категории со списком статей в каждой
        for(Help.Category category : STRUCTURE.getCategories()) {
            TextView catView = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_help_category, helpLayout, false);
            catView.setText(category.getTitle());
            helpLayout.addView(catView);

            for(final Help.Article article : category.getArticles()) {
                TextView artView = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_help_article, helpLayout, false);
                artView.setText(article.getTitle());
                // клик на название статьи открывает окно с текстом
                artView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showArticle(article);
                    }
                });
                helpLayout.addView(artView);
            }
        }

        return root;
    }

    //отображение текста статьи в popup-окне
    private void showArticle(Help.Article article) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).
                setTitle(article.getTitle());

        if(article.isTextType())
            builder.setMessage(article.getContent());
        else
            builder.setView(getResources().getIdentifier(article.getLayout(), "layout", getActivity().getPackageName()));

        builder.show();
    }

    // отправка сообщения через почтового клиента
    private void sendFeedback() {
        String body = getString(R.string.help_body) + "\n\n=========\nSystem info: [" +
                BuildConfig.VERSION_NAME + "] [" + Build.VERSION.RELEASE + "] [" + Build.MANUFACTURER + " " + Build.MODEL + "]";

        String uriText = "mailto:" + getString(R.string.mail) +
                        "?subject=" + Uri.encode(getString(R.string.help_subject)) +
                        "&body=" + Uri.encode(body);

        Uri uri = Uri.parse(uriText);

        Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
        sendIntent.setData(uri);
        startActivity(Intent.createChooser(sendIntent, getString(R.string.mail_chooser)));
    }
}
