package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.task_filters.Choice;
import com.mishuto.todo.model.task_filters.Choice.Item;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.LayoutWrapper;
import com.mishuto.todo.view.dialog.BaseFullScreenDialog;
import com.mishuto.todo.view.dialog.PagedDialog;
import com.mishuto.todo.view.edit.ITextView;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.mishuto.todo.view.Constants.OPAQUE_FULL;
import static com.mishuto.todo.view.Constants.OPAQUE_MID;

/**
 * Фрагмент, отображающий абстрактный фильтр (список значений, которые может принимать фильтр) для списка задач.
 * Фрагмент является частью диалога, создаваемого в классе TabbedDialog
 * Created by Michael Shishkin on 26.01.2019.
 */
public abstract class AbstractFilterFragment<S, E, F extends Choice<S, E>> extends Fragment implements
        RecyclerViewWidget.EventsListener<Item<E>>,
        PagedDialog.OnPageSelectedListener,
        BaseFullScreenDialog.ToolBar.OnButtonClickListener
{
    private BaseFullScreenDialog mParentDialog;         // родительский диалог
    List<Item<E>> mItems = new ArrayList<>();           // список значений фильтра
    RecyclerViewWidget<Item<E>> mRecyclerViewWidget;    // виджет списка значений фильтра
    int mLineLayout;                                    // шаблон строки списка строк. Должен содержать view R.id.title, R.num_tasks, R.plural_tasks
    F mFilter;                                          // отображаемый фильтр

    public AbstractFilterFragment(int lineLayout, F filter) {
        mLineLayout = lineLayout;
        mFilter = filter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fs_dlg_list_part, container, false);

        mItems.clear();                                  // при пересоздании страницы, надо очистить список
        mItems.addAll(mFilter.getItems());
        Collections.sort(mItems, new ItemComparator());  // сортируем список: сначала с задачами

        mRecyclerViewWidget = new RecyclerViewWidget<>(root.findViewById(R.id.recycler_view), mLineLayout, mItems, this);

        return root;
    }

    public BaseFullScreenDialog getParentDialog() {
        return mParentDialog;
    }

    // установка родительского диалога, для получения у него тулбара, FAB и передачи команды dismiss
    // вызывается при создании родительского диалога
    public void setParameters(BaseFullScreenDialog dialog) {
        mParentDialog = dialog;
    }

    // событие смены страницы с фильтрами в диалоге
    // инициируем тулбар
    @Override
    public void onPageSelected() {
        final BaseFullScreenDialog.ToolBar toolbar = getParentDialog().getToolBar();
        toolbar.setTitle(R.string.filtersDlg_title);
        toolbar.setLeftButton(BaseFullScreenDialog.ToolBar.ButtonIcon.UP);
        toolbar.setRightButton(BaseFullScreenDialog.ToolBar.ButtonIcon.NONE);
        toolbar.setButtonClickListener(this);
    }

    // обработка нажатия кнопки на тулбаре: сброс фильтра
    @Override
    public void onClick(BaseFullScreenDialog.ToolBar.ButtonIcon buttonIcon) {
        if(buttonIcon == BaseFullScreenDialog.ToolBar.ButtonIcon.UP) {
            onUpBtnClick();
        }
    }

    // дефолтное поведение нажатия кнопки up
    void onUpBtnClick() {
        mFilter.setFilter(false);
        closeDialog();
    }

    // закрытие диалога
    void closeDialog() {
        mParentDialog.dismiss();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBind(View root, Item<E> item, RecyclerViewWidget<Item<E>>.ViewHolder viewHolder) {
        getTitleView(root).setText(item.toString());    // вывод названия value

        // вывод количества задач для каждого value
        Integer tasksCount = getItemCount(item);
        TextView count = root.findViewById(R.id.num_tasks);
        TextView tasks = root.findViewById(R.id.plural_tasks);
        count.setText(tasksCount.toString());
        tasks.setText(getResources().getQuantityString(R.plurals.task_plural, tasksCount));

        for(View v : LayoutWrapper.createLayoutIterator(root))
            v.setAlpha(tasksCount > 0 ? OPAQUE_FULL : OPAQUE_MID);          // если у value нет задач, затеняем
    }

    protected ITextView getTitleView(View root) {
        return ((ITextView)root.findViewById(R.id.title));
    }

    //вычисление количества задач для данного элемента
    Integer getItemCount(Item item) {
        Integer count = 0;
        for (Task t : TaskList.getTasks() ) {
            if(item.isFiltered(t) && FilterManager.unhidden().isFiltered(t))  // считаем задачи, которые содержат данный элемент и не являются удаленными
                count++;
        }
        return count;
    }

    // компаратор для дополнительной сортировки элементов в диалоге.
    // список элементов должен быть предварительно отсортирован. Компаратор накладывает дополнительную сортировку:
    // сначала отображаются элементы имеющие задачи, потом - "пустые". При этом место Специального элемента в списке не изменяется
    private class ItemComparator implements Comparator<Item> {
        @Override
        public int compare(Item item1, Item item2) {
            return item1.isMissingItem() || item2.isMissingItem() ? 0 : (int)(Math.signum(getItemCount(item2)) - Math.signum(getItemCount(item1)));
        }
    }
}