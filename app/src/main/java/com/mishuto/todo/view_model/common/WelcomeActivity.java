package com.mishuto.todo.view_model.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mishuto.todo.common.BundleWrapper;
import com.mishuto.todo.common.PersistentValues;
import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Constants;

/**
 * Стартовая активность.
 * Отображает welcome-картинки с текстом
 * Отображается только при первом запуске
 * Created by Michael Shishkin on 18.08.2018.
 */

public class WelcomeActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private static final PersistentValues.PVBoolean WAS_SHOWED =
            PersistentValues.getFile("WelcomeActivity").getBoolean("showed", false);                    // флаг - был ли уже показан пользователю Welcome - экран
    private static final String[] PAGE_TITLES = ResourcesFacade.getStringArray(R.array.welcome_titles); // список подписей к картинкам
    private static int COUNT = PAGE_TITLES.length;                                                      // количество страниц
    private ImageView[] mDots = new ImageView[COUNT];                                                   // точки - индексы страниц

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        WAS_SHOWED.set(true);

        // контейнер фрагментов
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(new PagerAdapter());
        viewPager.addOnPageChangeListener(this);

        // кнопка старт
        Button go = findViewById(R.id.go);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // котнейнер точек-индексов страниц
        ViewGroup dotGroup = findViewById(R.id.dots);
        for (int i = 0 ; i < COUNT; i++) {
            mDots[i] = (ImageView) LayoutInflater.from(this).inflate(R.layout.item_welcome_dot, dotGroup, false);
            dotGroup.addView(mDots[i]);
        }

        mDots[0].setAlpha(Constants.OPAQUE_FULL);   // при создании активности, подкрашиваем первую точеку, если onPageSelected() не вызывается
    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < COUNT; i++)
            mDots[i].setAlpha(position == i ? Constants.OPAQUE_FULL: Constants.OPAQUE_MID);
    }

    static boolean wasShowed() {
        return WAS_SHOWED.get();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageScrollStateChanged(int state) {}

    // адаптер страниц ViewPager. Порождает фрагменты страниц
    private class PagerAdapter extends FragmentPagerAdapter {

        private PageFragment[] mPages = new PageFragment[COUNT];

        PagerAdapter() {
            super(getSupportFragmentManager());

            for(int i = 0; i < COUNT; i++)
                mPages[i] = PageFragment.createPage(i);
        }

        @Override
        public int getCount() {
            return mPages.length;
        }

        @Override
        public Fragment getItem(int position) {
            return mPages[position];
        }
    }

    // отображаемый в ViewPager фрагмент с изображением и заголовком
    public static class PageFragment extends Fragment {

        static PageFragment createPage(Integer index) {
            PageFragment pageFragment = new PageFragment();
            pageFragment.setArguments(BundleWrapper.toBundle(index));
            return pageFragment;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.item_welcome, container, false);
            Integer index = BundleWrapper.getInteger(getArguments());

            ImageView image = root.findViewById(R.id.picture);
            image.setImageLevel(index);

            TextView title = root.findViewById(R.id.title);
            title.setText(PAGE_TITLES[index]);
            return root;
        }
    }
}
