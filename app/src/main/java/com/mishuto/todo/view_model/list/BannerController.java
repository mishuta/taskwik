package com.mishuto.todo.view_model.list;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mishuto.todo.common.BundleWrapper;
import com.mishuto.todo.model.ReleaseNotes;
import com.mishuto.todo.todo.BuildConfig;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.LayoutWrapper;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view_model.common.ReleaseNotesDlg;

import static com.mishuto.todo.view.ViewCommon.color;

/**
 * Управление баннером, отвечающим за оповещение о новых фичах в оновлении
 * Created by Michael Shishkin on 05.01.2019.
 */
class BannerController {

    private ViewGroup mBanner;

    BannerController(final View root) {

        //PersistentValues.getFile("Banner").getInt("lastVersion", 22).set(22); // включить для отладки

        ReleaseNotes releaseNotes = ReleaseNotes.getInstance();
        // баннер отображается только если текущая версия > чем "последняя" и в XML-файле есть записи с момента последней версии
        boolean doShow = releaseNotes.areThereNewFeatures(releaseNotes.getLastVersion());

        mBanner = root.findViewById(R.id.banner);

        if(doShow) {    // подготовка отображения баннера
            TextView bannerText = root.findViewById(R.id.banner_text);
            ImageView bannerIco = root.findViewById(R.id.banner_ico);

            bannerText.setText(bannerText.getContext().getString(R.string.banner_text, BuildConfig.VERSION_NAME));

            root.findViewById(R.id.btn_dismiss).setOnClickListener(onDismiss(root));            // кнопка Скрыть
            root.findViewById(R.id.btn_more).setOnClickListener(onMore(releaseNotes, root));    // кнопка Подробнее

            // цвет текста и иконки зависят от наличия hot обновлений
            boolean hot = releaseNotes.areThereNewHotFeatures(releaseNotes.getLastVersion());
            bannerText.setTextColor(color(hot ? R.color.Red : R.color.colorMainText));
            bannerIco.setColorFilter(color(hot ? R.color.Red : R.color.MaterialGray600));
        }

        else {   // если обновлений нет - баннер не показывается
            for (View view : LayoutWrapper.createLayoutIterator(mBanner))
                view.setVisibility(View.GONE);
        }
    }

    // Клик по кнопке Подробнее
    private View.OnClickListener onMore(ReleaseNotes releaseNotes, View root) {
        return v -> {
            DialogHelper.createDialog(new ReleaseNotesDlg(), BundleWrapper.toBundle(releaseNotes.getLastVersion()));    // Открыть диалог ReleaseNotes со списком, начиная с lastVersion
            fadeOut(mBanner, root.findViewById(R.id.recycler_view));                                                    // схлопнуть баннер
        };
    }

    // Клик по кнопке Скрыть
    private View.OnClickListener onDismiss(View root) {
        return v -> {
            fadeOut(mBanner, root.findViewById(R.id.recycler_view));
            Snack.show(root, R.string.what_s_new_reminder);
        };
    }

    // анимация сокрытия баннера. Баннер уезжает одновременно с наездом списка
    private void fadeOut(final ViewGroup vanishing, final View substitutional) {
        vanishing.animate().translationY(-vanishing.getHeight()).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {

                for(View view : LayoutWrapper.createLayoutIterator(vanishing))
                    view.setVisibility(View.GONE);
            }
        });

        substitutional.animate().translationY(-vanishing.getHeight()).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                substitutional.setTranslationY(0);
            }
        });
    }
}
