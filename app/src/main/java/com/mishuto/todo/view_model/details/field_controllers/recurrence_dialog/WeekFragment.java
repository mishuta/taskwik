package com.mishuto.todo.view_model.details.field_controllers.recurrence_dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.mishuto.todo.model.task_fields.recurrence.Week;
import com.mishuto.todo.model.task_fields.recurrence.Week.DayOfWeek;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.CheckedButton;
import com.mishuto.todo.view.CheckedView;
import com.mishuto.todo.view.spinner.SpinnerWidget;

import static com.mishuto.todo.common.TCalendar.DAYS_IN_WEEK;

/**
 * Дочерний (nested) фрагмент ContainerDialog, отображающий секцию Recurrence.Week
 *
 * Created by Michael Shishkin on 28.11.2016.
 */

public class WeekFragment extends BasePageFragment<Week> implements SpinnerWidget.OnChangeSelectionListener<Week.WeeklyFactor>
{
   private CheckedButton[] mDayButtons = new CheckedButton[DAYS_IN_WEEK];    // массив кнопок - дней недели

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View childView = inflater.inflate(R.layout.dlg_rec_page_week, container, false);
        onCreateView(childView);    // инициализация виджетов в базовом классе
        setButtons(childView);      // инициализация кнопок - дней недели

        // инициализация спинера с кратностью недель
        Spinner weeks = childView.findViewById(R.id.rec_week_spinner);
        new SpinnerWidget<>(weeks, Week.WeeklyFactor.values(), mSegment.getWeeklyFactor(), this);

        onUpdate(false);

        return childView;
    }

    // создание массива кнопок - дней недели
    private void setButtons(View root) {
        final int[] ids = {R.id.day1, R.id.day2, R.id.day3, R.id.day4, R.id.day5, R.id.day6, R.id.day7 };

        mSegment.setDefaultWeekDay();               // устанавливается дефолтный день недели, если ничего не выбрано

        for (int i = 0 ; i < DAYS_IN_WEEK; i++) {   // инициализируется каждая пара кнопок
            DayOfWeek dayOfWeek = mSegment.getWeekDays()[i];
            mDayButtons[i] = root.findViewById(ids[i]);
            mDayButtons[i].setText(dayOfWeek.getTitle());
            mDayButtons[i].setChecked(dayOfWeek.isChecked());
            mDayButtons[i].setOnChangeListener(new ChangeStateListener(dayOfWeek));
        }
    }

    // слушатель события нажатия кнопки - дня недели
    class ChangeStateListener implements CheckedView.OnChangeStateListener {
        DayOfWeek mDayOfWeek;

        ChangeStateListener(DayOfWeek dayOfWeek) {
            mDayOfWeek = dayOfWeek;
        }

        @Override
        public void onChangeState(CheckedView button, boolean checked) {
            mDayOfWeek.setChecked(checked);
            onUpdate(true);
        }
    }

    @Override
    public void onChangeSelection(Week.WeeklyFactor item, int index, int spinnerId) {
        mSegment.setWeeklyFactor(item);
        onUpdate(true);
    }
}
