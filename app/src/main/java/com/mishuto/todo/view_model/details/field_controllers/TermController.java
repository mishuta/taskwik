package com.mishuto.todo.view_model.details.field_controllers;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.dialog.DateDialog;

import java.util.Calendar;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.mishuto.todo.common.BundleWrapper.getSerializable;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_PREV_DISCARDS_TIME;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_TIME;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_TIME_DISCARDS_PREV;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.S_PERFORMED;


/**
 * Контроллер DetailsFragmentControl для отображения и изменения атрибута задачи "Дата"
 * Created by Michael Shishkin on 29.10.2016.
 */

public class TermController extends ObserverController {
    private TaskTime mTaskTime;             // Время события
    private ImageView mAlert;               // Иконка с предупреждением прошедшей даты

    public TermController(View view, Task task)  {
        super(view, task, R.id.taskDetails_label_term, R.id.taskDetails_TextView_Term, R.id.taskDetails_clearTerm);
        setEventFilter(A_TIME, A_TIME_DISCARDS_PREV, A_PREV_DISCARDS_TIME, S_PERFORMED);
        mAlert = setClickable(R.id.taskDetails_image_termAlert);
        updateWidgets();
    }

    @Override
    void onAction() {
        createDialog(new DateDialog(), toBundle(mTaskTime.getCalendar()));
    }

    @Override
    void onClear() {
        mTaskTime.resetDate();
        mTask.setTaskTime(mTaskTime);
    }

    @Override
    public void updateWidgets() {
        mTaskTime = mTask.getTaskTime();    // инициализация не в конструкторе, поскольку объект может поменяться за пределами контроллера
        setState(mTaskTime.isDateSet() ? mTaskTime.getCalendar().getFormattedDate() : null);

        // обновление значка с предупреждением о прошедшей дате
        mAlert.setVisibility(mTaskTime.isDateSet() && mTaskTime.getCalendar().intervalFrom(Calendar.DATE, TCalendar.now()) < 0 ? VISIBLE : GONE);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if(v.getId() == mAlert.getId()) //Если нажата иконка с предупреждением - показываем предупреждение
            Snack.show(v, R.string.taskDetails_warn_term);
    }

    @Override
    public void onCloseDialog(int resultCode, Bundle resultObject) {
        if(resultCode == RESULT_OK) {
            TCalendar newDate = (TCalendar)getSerializable(resultObject);
            mTaskTime.setDate(newDate.get(Calendar.YEAR), newDate.get(Calendar.MONTH), newDate.get(Calendar.DAY_OF_MONTH));
            mTask.setTaskTime(mTaskTime);
        }
    }
}
