package com.mishuto.todo.view_model.settings;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.dialog.BaseFullScreenDialog;

import static android.app.Activity.RESULT_OK;
import static com.mishuto.todo.view.dialog.DialogHelper.onCloseDialog;

/**
 * Диалог настроек
 * Класс отвечает за тулбар и создание объектов-контроллеров, которые реализуют логику разных секций диалога
 * Created by Michael Shishkin on 15.01.2018.
 */
public class SettingsDialog extends BaseFullScreenDialog implements BaseFullScreenDialog.ToolBar.OnButtonClickListener
{
    final static int IMPORT_CODE = 0xffff;

    Data mData;

    public SettingsDialog() {
        super(R.layout.fs_dlg_toolbar_part, R.layout.fs_dlg_settings);
    }

    @SuppressWarnings("ConstantConditions")
    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        // установка заголовка и кнопки тулбара
        getToolBar().setTitle(R.string.settingsTitle);
        getToolBar().setRightButton(ToolBar.ButtonIcon.NONE);
        getToolBar().setButtonClickListener(this);

        // создание объектов-контроллеров разных секций диалога
        new WorkTime(root, this);
        new Notifications(root);
        new Etc(root);
        new About(root);
        mData = new Data(root, this);

        return root;
    }

    // обработка нажатия кнопки up
    @Override
    public void onClick(ToolBar.ButtonIcon buttonIcon) {
        getDialog().dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == IMPORT_CODE ) {
            if(resultCode == RESULT_OK)
                mData.doImport(data.getData());
        }
        else
            onCloseDialog(requestCode, resultCode, data);
    }
}
