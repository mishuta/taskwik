package com.mishuto.todo.view_model.details.field_controllers.recurrence_dialog;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.mishuto.todo.common.KeyString;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.task_fields.recurrence.BaseMonthYear;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.edit.TextInputWidget;
import com.mishuto.todo.view.spinner.SpinnerWidget;

/**
 * Базовый класс для страниц MonthFragment и YearFragment, т.к. они различаются только установками для кратности месяцев
 * Created by Michael Shishkin on 18.12.2016.
 */

abstract class BaseMonthYearFragment<T extends BaseMonthYear> extends BasePageFragment<T>
        implements View.OnClickListener, TextInputWidget.OnChangeTextListener  {

    TextInputWidget mDayOfMonth;// текстовое поле число месяца
    RadioButton mButtonDate;    // радио- выбор числа месяца
    RadioButton mButtonWeek;    // радио- выбор номера недели

    @Override
    protected void onCreateView(View root) {
        super.onCreateView(root);

        // связывание виджетов
        mButtonDate =  root.findViewById(R.id.rec_month_radio_date);
        mButtonWeek =  root.findViewById(R.id.rec_month_radio_week);
        TextInputLayout dayOfMonth =  root.findViewById(R.id.rec_mon_monDay);
        Spinner weekNumSpinner = root.findViewById(R.id.rec_mon_weekNum_spinner);
        Spinner weekDaySpinner = root.findViewById(R.id.rec_mon_weekDay_spinner);

        // инициализация виджетов
        mButtonDate.setChecked(mSegment.isDayOfMonthSelected());
        mButtonWeek.setChecked(!mSegment.isDayOfMonthSelected());

        mDayOfMonth = new TextInputWidget(dayOfMonth, mSegment.getDayOfMonth().toString(), this);

        // кратность дня недели
        new SpinnerWidget<>(weekNumSpinner, BaseMonthYear.WeekNumber.values(), mSegment.getWeekNum(),
                new SpinnerWidget.OnChangeSelectionListener<BaseMonthYear.WeekNumber>() {
                    @Override
                    public void onChangeSelection(BaseMonthYear.WeekNumber item, int index, int spinnerId) {
                        mSegment.setWeekNum(item);
                        onUpdate(true);
                    }
                });

        // дни недели
        KeyString[] weekDays = BaseMonthYear.getWeekDaysArray();
        new SpinnerWidget<>(weekDaySpinner, weekDays, mSegment.getWeekDay(),
                new SpinnerWidget.OnChangeSelectionListener<KeyString>() {
                    @Override
                    public void onChangeSelection(KeyString item, int index, int spinnerId) {
                        mSegment.setWeekDay(item.getKey());
                        onUpdate(true);
                    }
                });

        mButtonDate.setOnClickListener(this);
        mButtonWeek.setOnClickListener(this);

        onUpdate(false);
    }

    // событие нажатия радиокнопок
    @Override
    public void onClick(View v) {
        mSegment.setDayOfMonthSelected(mButtonDate.getId() == v.getId());

        if(mSegment.isDayOfMonthSelected())
            mButtonWeek.setChecked(false);
        else
            mButtonDate.setChecked(false);

        onUpdate(true);
    }

    // событие изменения числа месяца
    @Override
    public void onTextChanged(String text) {
        try {
            Integer n = Integer.parseInt(text);                                     // пытаемся распарсить ввод
            if( n <= TCalendar.MAX_MONTH_DAY && n >= TCalendar.FIRST_MONTH_DAY) {   // если день месяца лежит в интервале 1..31
                mSegment.setDayOfMonth(n);                                          // сохраняем номер дня месяца
                mDayOfMonth.hideError();                                            // убираем ошибку
                onUpdate(true);                                                     // обновлем родительскую страницу
            }
            else
                mDayOfMonth.showError(R.string.recurrence_date_err);     //иначе показываем ошибку
        }
        catch (NumberFormatException e) {
            mDayOfMonth.showError(R.string.recurrence_date_err);
        }
    }
}
