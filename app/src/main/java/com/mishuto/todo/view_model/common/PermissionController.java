package com.mishuto.todo.view_model.common;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.mishuto.todo.common.Table;
import com.mishuto.todo.view.Snack;

import static android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale;
import static android.support.v4.content.ContextCompat.checkSelfPermission;

/**
 * Врапер для получения необходимых разрешений в рантайме для Android 6+
 * Created by Michael Shishkin on 30.07.2018.
 */
public class PermissionController {
    // т.к. разрешения даются асинхронно, пока выдается одно разрешение, может быть создано несколько объектов для запроса разрешений
    private static Table<PermissionController> sTable = new Table<>();  // таблица всех объектов PermissionController

    private int mRequestCode;                       // уникальный id объекта
    private Activity mActivity;                     // активити, внутри которой создается разрешение (имеет переопределенный метод onRequestPermissionsResult)
    private PermissionRequest mPermissionRequest;   // объект клиента для обработки колбэков

    // интерфейс, реализуемый клиентом для асинхронной обработки ответов
    public interface PermissionRequest {
        void onResult(boolean granted);             // событие результата разрешения. Разрешение дано, если granted=true
        void onRationalRequest(String permission);  // событие запроса описания цели запроса разрешения. Дефолтная реализация в onRationalRequestImp
    }

    public PermissionController(Activity activity) {
        mActivity = activity;
        mRequestCode = sTable.put(this);
    }

    /**
     * Попытка запроса разрешения
     * @param permission разрешение, например Manifest.permission.READ_EXTERNAL_STORAGE
     * @param permissionRequest клиент
     * @return true, если разрешение уже есть. В противном случае разрешение запрашивается и возвращается false
     */
    public boolean tryRequestPermission(String permission, PermissionRequest permissionRequest) {
        mPermissionRequest = permissionRequest;

        if(checkSelfPermission(mActivity, permission) == PackageManager.PERMISSION_GRANTED)
            return true;

        if (shouldShowRequestPermissionRationale(mActivity, permission))
            permissionRequest.onRationalRequest(permission);
        else
            requestPermission(permission);

        return false;
    }

    // Обработчик метода активити onRequestPermissionsResult
    static void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        sTable.get(requestCode).onRequestPermissionsResult(grantResults);
    }

    // Обработчик метода активити onRequestPermissionsResult в данном объекте
    private void onRequestPermissionsResult(@NonNull int[] grantResults) {
        boolean granted = (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED);
        mPermissionRequest.onResult(granted);
    }

    // запрос разрешения у активити
    @SuppressWarnings("WeakerAccess")
    protected void requestPermission(String permission) {
        ActivityCompat.requestPermissions(mActivity, new String[]{permission}, mRequestCode);
    }

    // дефолтная реализация PermissionRequest.onRationalRequest
    // отображает снек с объяснением цели разрешения
    public void onRationalRequestImp(final String permission, View view, int permissionRationalId) {
        Snack.show(
                view,
                mActivity.getString(permissionRationalId),
                new Snack.OnDismissedEvent() {
                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {
                        super.onDismissed(transientBottomBar, event);
                        requestPermission(permission);
                    }
                },
                null, 0, 0);
    }
}
