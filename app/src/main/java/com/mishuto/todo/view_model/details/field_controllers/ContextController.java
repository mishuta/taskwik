package com.mishuto.todo.view_model.details.field_controllers;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.database.aggregators.DBTreeSet;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.todo.R;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.mishuto.todo.common.BundleWrapper.getDBObject;
import static com.mishuto.todo.common.BundleWrapper.toBundle;

/**
 * Контроллер DetailsFragmentControl для отображения/изменения на карточке задач атрибута "Контексты", включая отображение списка контекстов
 * Created by Michael Shishkin on 12.01.2017.
 */

public class ContextController extends GenericController {

    private ExpandedPanel mExpandedPanel;

    public ContextController(View root, Task task) {
        super(root, task, R.id.taskDetails_label_context, R.id.taskDetails_context_desc, R.id.taskDetails_clearContext);

        mExpandedPanel = new ExpandedPanel(mStateView,
                (ImageButton) root.findViewById(R.id.taskDetails_context_expand),
                (ImageButton)root.findViewById(R.id.taskDetails_context_collapse),
                (RecyclerView) root.findViewById(R.id.taskDetails_context_recycler),
                StringContainer.getStringCollection(task.getContext(), new ArrayList<String>()),
                2, this);

        updateWidgets();
    }

    @Override
    public void onCloseDialog(int resultCode, Bundle resultObject) {
        if(resultCode == RESULT_OK) {
            //noinspection unchecked
            mTask.setContext((DBTreeSet<StringContainer>) getDBObject(resultObject));
            setList();

            updateWidgets();
        }
    }

    // Сброс контекстов задачи (устанавливаем пустой список)
    @Override
    void onClear() {
        mTask.setContext(null);
        setList();
        updateWidgets();
    }

    @Override
    public void updateWidgets() {
        int n = mTask.getContext().size();
        mExpandedPanel.refreshPanel();

        setState( n == 0 ? null
                : n == 1 ? mTask.getContext().toArray()[0].toString()
                : mRoot.getResources().getQuantityString(R.plurals.context_plural, n, n)); // выводится "n контекстов"
    }

    // Обработка нажатия стандартных элементов
    @Override
    public void onClick(View v) {
        super.onClick(v);                                                                   // в суперклассе дефолтное поведение оставляем только для кнопки Clear

        if(v.getId() != R.id.taskDetails_clearContext)
            createDialog(new ContextDialog(), toBundle ((DBTreeSet) mTask.getContext()));

    }

    // установка списка контекстов
    private void setList() {
        mExpandedPanel.setList(StringContainer.getStringCollection(mTask.getContext(), new ArrayList<String>()));
    }

    @Override
    void onAction() {  }
}
