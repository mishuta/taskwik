package com.mishuto.todo.view_model.details.field_controllers;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;

import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view.edit.ITextView;
import com.mishuto.todo.view_model.common.AbstractActivity;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Базовый класс-контроллер для всех контроллеров, имеющих элементы: лейбл, значение, кнопка очистки
 * Created by Michael Shishkin on 11.01.2017.
 */

public abstract class GenericController extends AbstractEditableController
        implements View.OnClickListener, DialogHelper.DialogHelperListener
{

    private View mLabel;            // View для лейбла атрибута
    View mStateView;                // View, в котором отображается состояние атрибута
    private View mClearBtn;         // кнопка очистки
    Task mTask;                     // объект модели, используется в сабклассах
    View mRoot;                     // корневая view карточки задач

    abstract void onAction();       // вызывается во время клика по лейблу или состоянию для изменения атрибута
    abstract void onClear();        // вызывается по клику на кнопку очистки для очистки атрибута

    private final static String NOT_SET = ResourcesFacade.getString(R.string.NOT_SET);

    GenericController(View root, Task task, int label, int value, int clearBtn) {
        mTask = task;
        mRoot = root;

        mLabel = root.findViewById(label);
        mStateView = root.findViewById(value);
        mClearBtn = root.findViewById(clearBtn);
        mClearBtn.setOnClickListener(this);

        mLabel.setOnClickListener(this);
        mStateView.setOnClickListener(this);

        setPerformChangedViews(mLabel, mStateView);
        onPerformEvent(task.isStateInPerformed());
    }

    // обработка нажатия на лейбл и value.
    @Override
    public void onClick(View v) {
        if(mLabel.getId() == v.getId() || mStateView.getId() == v.getId())
            onAction();

        else if(mClearBtn.getId() == v.getId())
            onClear();
    }

    // Событие завершения задачи, обрабатывается всеми контроллерами.
    @Override
    public void onPerformEvent(boolean isPerformed) {
        super.onPerformEvent(isPerformed);
        mClearBtn.setEnabled(!isPerformed && !((ITextView)mStateView).getText().equals(NOT_SET));
    }

    // метод для обработки возвращаемого из диалога значения.
    // Ничего не делает. Должен быть переопределен в сабклассах, где создаются диалоги
    @Override
    public void onCloseDialog(int resultCode, Bundle resultObject) { }

    // Сервисные методы для контроллеров

    // отображение статуса атрибута в статус-виджете (если виджет - TextView)
    // Если s==null, отображается стандартное значение "Нет" и кнопка очистки дизейблится
    void setState(String s) {
        ((ITextView) mStateView).setText(s != null ?  s : NOT_SET);
        mClearBtn.setEnabled(s != null);
    }

    // создать диалог
    final void createDialog(DialogFragment dialogFragment, Bundle inParameter) {
        DialogHelper.createDialog(dialogFragment, AbstractActivity.getCurrentActivity().getMainFragment(), this, inParameter);
    }

    // назначить лисенера на виджет
    final <T extends View> T setClickable(int resId) {
        T view = mRoot.findViewById(resId);
        view.setOnClickListener(this);
        return view;
    }

    final void hide(View ... views) {
        setVisibility(GONE, views);
    }

    final void show(View ... views) {
        setVisibility(VISIBLE, views);
    }

    private void setVisibility(int state, View ... views) {
        for (View view: views)
            view.setVisibility(state);
    }
}
