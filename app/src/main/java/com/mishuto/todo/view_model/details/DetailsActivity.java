package com.mishuto.todo.view_model.details;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.mishuto.todo.App;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view_model.common.AbstractActivity;
import com.mishuto.todo.view_model.common.InterfaceEventsFacade;

import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.CardMsg;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.EventObserver;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.Event;


/**
 * Конкретный класс Активности, создающий фрагмент DetailsFragment.
 * Служит для создания/перезагрузки фрагмента и передает ему событие нажатя [Back]
 * Активность запускается методом из ListFragment при клике на задачу в списке или на FAB
 *
 * Created by mike on 24.08.2016.
 */
public class DetailsActivity extends AbstractActivity implements EventObserver {

    private static final String TASK_ID = App.createTagName("taskId");

    //Возвращает intent для запуска DetailsActivity. Вызывается из MainActivity
    public static Intent getIntent(long taskId) {
        Intent intent = new Intent(App.getAppContext(), DetailsActivity.class);
        intent.putExtra(TASK_ID, taskId);
        return intent;
    }

    // Создает дочерний фрагмент и передает ему id задачи, извлеченный из интента DetailsActivity
    @Override
    protected Fragment createFragment() {
        return createFragment(getIntent().getLongExtra(TASK_ID, 0));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InterfaceEventsFacade.getInstance().register(this);     // подписка на события интерфейса
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        InterfaceEventsFacade.getInstance().unregister(this);
    }

    @Override
    protected int getContentViewLayout() {
        return R.layout.activity_details;
    }

    @Override
    public void onInterfaceEvent(Event event, Object param) {
        switch (event) {
            case CLOSE_CARD:    // событие завершения обработки задачи
                finish();       // завершаем активность
                break;

            case CHANGE_TASK:   // событие загрузки новой задачи без перезагрузки активности
                CardMsg cardMsg = (CardMsg) param;
                Fragment fragment = createFragment(cardMsg.taskToChange.getRecordID());
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit(); // перегружаем фрагмент
                Snack.show(findViewById(R.id.fragment_container), String.format(getString(R.string.new_task_loaded), cardMsg.taskToChange.toString()));
        }
    }

    private Fragment createFragment(long taskId) {
        Fragment fragment = new DetailsFragment();
        fragment.setArguments(toBundle(taskId));
        return fragment;
    }
}
