package com.mishuto.todo.view_model.list;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.common.InterfaceEventsFacade;

/**
 * Контроллер виджетов, появляющихся для пустого списка
 * Created by Michael Shishkin on 28.12.2018.
 */
public class EmptyMsgController implements
        TaskEventsFacade.TaskObserver,
        InterfaceEventsFacade.EventObserver
{
    private TextView mEmptyMsg;                 // виджет сообщения для пустого списка
    private ImageView mEmptyImage;              // виджет изображения для пустого списка


    EmptyMsgController(View root) {
        mEmptyMsg = root.findViewById(R.id.empty_list_msg);
        mEmptyImage = root.findViewById(R.id.empty_list_image);
        emptyMessageUpdate();
    }

    @Override
    public void onTaskEvent(TaskEventsFacade.EventType type, Task task, Object extendedData) {
        emptyMessageUpdate();
    }

    @Override
    public void onInterfaceEvent(InterfaceEventsFacade.Event event, Object param) {
        emptyMessageUpdate();
    }

    // апдейт сообщения пустого списка
    private void emptyMessageUpdate() {
        if(TaskList.getTasks().size() - FilterManager.unhidden().getHiddenTasks().size() == 0) {
            mEmptyImage.setVisibility(View.VISIBLE);
            mEmptyMsg.setText(R.string.no_tasks);               // если задач нет (или они все в удаленных) - выводится сообщение об отсутсвии задач
        }
        else if(FilterManager.computeFilteredTaskList().isEmpty()) {
            mEmptyImage.setVisibility(View.VISIBLE);
            mEmptyMsg.setText(R.string.all_tasks_filtered);     // если задачи есть, но все отфильтрованы выводится сообщение об этом
        }
        else {
            mEmptyImage.setVisibility(View.INVISIBLE);
            mEmptyMsg.setText("");                              // в остальных случаях сообщение не выводится
        }
    }
}
