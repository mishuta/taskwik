package com.mishuto.todo.view_model.details.field_controllers;

import android.view.View;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.edit.AbstractCompoundEditFreezeView;
import com.mishuto.todo.view.edit.AutocompleteFreezeView;

import static com.mishuto.todo.model.tags_collections.Tags.EMPTY_TAG;

/**
 * Контроллер DetailsFragment для отображения и изменения атрибута задачи Исполнитель
 * Created by Michael Shishkin on 29.12.2016.
 */

public class ExecutorController extends GenericController implements AbstractCompoundEditFreezeView.AutoChangeModeListener
{
    private AutocompleteFreezeView mExecutorView; // Виджет ввода исполнителя

    public ExecutorController(View root, Task task) {
        super(root, task, R.id.taskDetails_label_executor, R.id.taskDetails_executor, R.id.taskDetails_clearExecutor);

        mExecutorView = (AutocompleteFreezeView) mStateView;
        mExecutorView.setAutoChangeModeListener(this);
        updateWidgets();
    }

    @Override
    public void updateWidgets() {
        mExecutorView.setAutoCompleteAdapter(Executors.getInstance().getExecutorMap().keySet().toArray());  // получаем массив исполнителей
        setState(mTask.getExecutor() != EMPTY_TAG ? mTask.getExecutor().toString() : null);
    }

    @Override
    void onAction() {
       clearBeforeEdit();
       mExecutorView.setEditMode();
    }

    @Override
    public void onClear () {
        mTask.setExecutor(EMPTY_TAG);
        updateWidgets();
    }

    @Override
    public void onSave() {
        if(mExecutorView.isEditMode()) {
            mExecutorView.setViewMode();
            mTask.setExecutor(new StringContainer(mExecutorView.getText()));
        }
    }

    // событие изменения режима mExecutorView
    @Override
    public void onAutoChangeMode(AbstractCompoundEditFreezeView view, boolean toViewMode) {
        if(toViewMode) {
            mTask.setExecutor(new StringContainer(TextUtils.capitalize(mExecutorView.getText())));
            updateWidgets();
        }
        else
            clearBeforeEdit();
    }

    // очистка поля редактирования от стандартного значения "Нет"
    private void clearBeforeEdit() {
        if(mTask.getExecutor() == EMPTY_TAG)
            mExecutorView.setText(null);
    }
}
