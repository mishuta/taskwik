package com.mishuto.todo.view_model.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.mishuto.todo.common.BackupManager;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;

/*
 Абстрактный класс AppCompactActivity от которого наследуются конкретные активности, определяющие какой фрагмент будет использоваться: TaskDetailsActivity и MainActivity
 */
@SuppressLint("StaticFieldLeak")
public abstract class AbstractActivity extends AppCompatActivity {

    private static AbstractActivity sCurrentActivity;   // Ссылка на текущий Activity для получения контекста из любой точки приложения
    private boolean isInForeground;                     // флаг: активность на переднем плане
    private boolean isRecreated;                        // флаг: активность была пересоздана ОС
    private final static int TASKS_LOADER_CODE = 100;   // идентификатор запуска LauncherScreenActivity;

    //Вызывается для создания фрагмента в конкретной дочерней активности
    protected abstract Fragment createFragment();       // создание основного фрагмента в конкретной активности
    protected abstract int getContentViewLayout();      // layout активности

    public static AbstractActivity getCurrentActivity() {
        return sCurrentActivity;
    }

    public static Context getContext() {
        return sCurrentActivity;
    }

    // возвращает основной фрагмент активити.
    public Fragment getMainFragment() {
        return  getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    }

    // отображается ли активность (главный фрагмент активности) на экране
    public boolean isInForeground() {
        return isInForeground;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        isRecreated = savedInstanceState != null;           // если активность была пересоздана то savedInstanceState содержит предыдущее состояние. При первом запуске всегда null

        super.onCreate(savedInstanceState);
        setContentView(getContentViewLayout());
                                                            // Пытаемся получить уже созданный фрагмент, который размещается в FrameLayout хост-активности.
        if(getMainFragment() == null) {                     // Если фрагмент еще не создан
            Fragment fragment = createFragment();           // создаем фрагмент. Конкретный фрагмент создается в классе-потомке, который переопределяет createFragment()
            getSupportFragmentManager().beginTransaction()  // создаем FragmentTransaction
                    .add(R.id.fragment_container, fragment) // добавляем созданный объект фрагмента в FrameLayout хост-активити
                    .commit();                              // транзакция комитится
        }

        // Если задачи еще не загружены, вызываем активность, визуализирующую асинхронную загрузку задач
        // такой вариант асинхронной загрузки не самый простой, но единственно возможный,
        // т.к. необходимо учитывать возможность восстановления активности после уничтожения приложения ОС. В этом случае вызвать сначала активность-загрузчик невозможно
        if(!TaskList.get().hasLoaded())
            startActivityForResult(new Intent(this, LauncherScreenActivity.class), TASKS_LOADER_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == TASKS_LOADER_CODE)
            InterfaceEventsFacade.send(InterfaceEventsFacade.Event.LIST_LOADED, null);
    }

    @Override
    protected void onStart() {
        sCurrentActivity = this;    // Сохрантять ссылку нужно в onStart, т.к. при возвращении из TaskDetailsActivity onCreate() в MainActivity не вызывается
        super.onStart();            // но до вызова super.onStart(), т.к. в нем вызывается Fragment#onCreateView, при первом вызове которого инициируется DBOpenHelper
        isInForeground = true;      // фиксируем переход активности на передний план
    }

    @Override
    protected void onResume() {
        sCurrentActivity = this;    // Сохраняем ссылку в onResume,
        super.onResume();           // т.к. возможен вариант: Main.onStart()->Details.onStart()->Main.onResumed()->Details.onDestroy(). Т.е. в onStart() сохранять недостаточно
        BackupManager.getInstance().releaseBackup();    // все стартовые проверки прошли, время для возможного бэкапа
    }

    @Override
    protected void onStop() {
        isInForeground = false;     // фиксируем уход активности с переднего плана
        super.onStop();
    }

    public boolean isRecreated() {
        return isRecreated;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(sCurrentActivity == this)    // очищаем sActivity при уничтожении активности, только если sActivity уже не перезаписана другой активностью
            sCurrentActivity = null;
    }

    // Закрывает редактирование *FreezeView при клике вне виджета.
    // Метод ловит все нажатия внутри текущей Активности и, если фокус был у какого-либо EditText, а нажатие было вне этого виджета, то ему передается событие onFocusChange.
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY()))
                    v.clearFocus();
            }
        }
        return super.dispatchTouchEvent( event );
    }

    // Колбэк системного диалога на запрос у пользователя разрешения (permission)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionController.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
