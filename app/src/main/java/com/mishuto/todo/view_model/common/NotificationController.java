package com.mishuto.todo.view_model.common;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.mishuto.todo.App;
import com.mishuto.todo.common.DateFormatWrapper;
import com.mishuto.todo.database.MapStore;
import com.mishuto.todo.database.aggregators.DBHashSet;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.list.MainActivity;

import java.util.Set;

import static android.support.v4.app.NotificationCompat.DEFAULT_LIGHTS;
import static android.support.v4.app.NotificationCompat.DEFAULT_SOUND;
import static android.support.v4.app.NotificationCompat.DEFAULT_VIBRATE;
import static android.support.v4.app.NotificationCompat.VISIBILITY_PUBLIC;
import static com.mishuto.todo.App.getAppContext;
import static com.mishuto.todo.common.TSystem.approveThat;
import static com.mishuto.todo.model.task_filters.FilterManager.activated;
import static com.mishuto.todo.view_model.common.NotificationController.NotificationIntentReceiver.TYPE.ACTION;
import static com.mishuto.todo.view_model.common.NotificationController.NotificationIntentReceiver.TYPE.CANCEL;
import static com.mishuto.todo.view_model.common.NotificationController.NotificationIntentReceiver.createIntent;

/**
 * Класс для управления уведомлениями
 * Создает нотификацию со списком сработавших задач
 * Created by Michael Shishkin on 01.01.2018.
 */
public class NotificationController {

    private static NotificationManager sNotificationManager;
    private static Settings.NotificationSettings sSettings = Settings.getNotificationSettings();    //шоткат синглтона Settings.NotificationSettings

    private NotificationCompat.Builder mNotificationBuilder;                                        //билдер уведомления
    private NotificationCompat.InboxStyle mStyle;                                                   //объект для накопления строк в уведомлени
    private DBHashSet<Task> mShowedTasks;                                                           // список задач в уведомлении. Персистентный объект, т.к. после срабатывания уведомления
                                                                                                    // аппа может быть выгружена из памяти
    private static final int MAX_ROWS = 5;                                                          // макс. отображаемое количество строк в уведомлении
    private static final String CHANNEL_ID = "COMMON_CHANNEL";
    private static final String CHANNEL_NAME = "General notifications";

    private static final NotificationController sInstance = new NotificationController();

    static {
        sNotificationManager = (NotificationManager) getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
        createChannel();                                                                            // создание канала для Android 8
    }

    public static NotificationController getInstance() {
        return sInstance;
    }

    private NotificationController() {
        mShowedTasks = MapStore.getInstance().get(getClass(), "ShowedTasks", new DBHashSet<Task>());    // чтение списка задач в нотификации из базы
        for(Task task : mShowedTasks)                                                                   // если задачи есть
            addLine(task);                                                                              // то они добавляются в нотификацию. Т.к. это множество, задачи не задублируются
    }

    // создание и настройка билдера нотификаций
    // создается перед созданием новой нотификации, поскольку настройки нотификации могут изменяться со временем
    private void setUpNotification() {
        approveThat(sSettings.isOn.get(), "Make sure that notification is enabled before creating the notification");

        mNotificationBuilder = new NotificationCompat.Builder(getAppContext(), CHANNEL_ID)
                .setContentIntent(createIntent(getAppContext(), ACTION, 1))
                .setDeleteIntent(createIntent(getAppContext(), CANCEL, 1))
                .setSmallIcon(R.drawable.ic_alarm_white_18dp)
                .setDefaults((sSettings.doVibrate.get() ? DEFAULT_VIBRATE : 0) |
                        (sSettings.doSound.get() ? DEFAULT_SOUND : 0) |
                        (sSettings.doLight.get() ? DEFAULT_LIGHTS : 0))
                .setShowWhen(false)
                .setVisibility(VISIBILITY_PUBLIC)
                .setAutoCancel(true);

        mStyle = new NotificationCompat.InboxStyle();       // пересоздаем объект Style чтобы очистить список нотификации
        mNotificationBuilder.setStyle(mStyle);
        mStyle.addLine(" ");                                // добавляем пустую строку для android 5, иначе он не отобразит первую строку
    }

    // добавить новую задачу в уведомление
    void addLine(Task task) {
        if(mNotificationBuilder == null)   //если нотификация впервые создана или была очищена пользователем - создаем новую.
           setUpNotification();

        mShowedTasks.add(task);
        String line = DateFormatWrapper.getTimeString(task.getTimerTime()) + ". " + task.toString();    // формируем выводимую строку

        if(getNumLines() >= MAX_ROWS)                                                                   // если уперлись в лимит выводим/обновляем summary (Еще 3)
            mStyle.setSummaryText(getAppContext().getString(R.string.n_more, getNumLines()-MAX_ROWS));
        else
            mStyle.addLine(line);                                                                       // иначе добавляем строку

        mNotificationBuilder.setTicker(line);                                                           // тикер выводится только для API 19-20
    }

    // установить заголовок нотификации
    void setNotificationTitle(String title) {
        mNotificationBuilder.setContentTitle(title);
    }

    // текущее количество задач в нотификации
    public int getNumLines() {
        return mShowedTasks.size();
    }

    // вывести нотификацию на экран
    void show() {
        approveThat(getNumLines() > 0, "Notification must have at least one task");
        sNotificationManager.notify(1, mNotificationBuilder.build());
    }

    // очистить нотификацию
    public void clearNotification() {
        sNotificationManager.cancelAll();
        mShowedTasks.clear();
        mNotificationBuilder = null;
    }

    public Set<Task> getNotifiedTasks() {
        return mShowedTasks.getShallowCopy();
    }

    // класс - обработчик событий нотификации. Ловит интенты нажатия/очистки нотификации
   public static class NotificationIntentReceiver extends BroadcastReceiver {

        enum TYPE { ACTION, CANCEL} // типы обрабатываемых событий
        final static String NOTIFICATION_TAG = App.createTagName("NotificationID");

        // создание интента, который будет обработан
        static PendingIntent createIntent(Context context, TYPE type, int id ) {
            Intent intent = new Intent(context, NotificationIntentReceiver.class);
            intent.setAction(type.name());
            intent.putExtra(NOTIFICATION_TAG, id);
            return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        }

        // событие нотификации action/cancel
        @Override
        public void onReceive(Context context, Intent intent) {
            //android.os.Debug.waitForDebugger();
            if(ACTION.name().equals(intent.getAction())) {  // если пользователь кликнул по нотификации - передаем управление главной активности
                MainActivity.StartActivityBuilder activityBuilder = new MainActivity.StartActivityBuilder(context);
                approveThat(sInstance.getNumLines() > 0, "Notification must have at least one task");
                activated().set(sInstance.mShowedTasks);
                if(sInstance.getNumLines() > 1) {           // если несколько задач в нотификации
                    activated().setFilter(true);            // то устанавливаем фильтр
                    activityBuilder.showList();             // и требуем его обновить
                }
                else                                        // иначе открываем карточку с одной задачей
                    activityBuilder.openTaskCard(activated().getOnlyTask());

                activityBuilder.start();                    // запускаем активность
            }

            sInstance.mShowedTasks.clear();
            sInstance.mNotificationBuilder = null;
        }
   }

   // создание канала в Oreo. Вызывается из статического инициалищатора
    private static void createChannel() {
        if (Build.VERSION.SDK_INT >=26) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(false);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.enableVibration(sSettings.doVibrate.get());
            channel.enableLights(sSettings.doLight.get());
            sNotificationManager.createNotificationChannel(channel);
        }
    }
}
