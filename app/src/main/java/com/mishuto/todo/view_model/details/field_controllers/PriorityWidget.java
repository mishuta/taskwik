package com.mishuto.todo.view_model.details.field_controllers;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mishuto.todo.model.task_fields.Priority;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.spinner.SpinnerAdapter;
import com.mishuto.todo.view.spinner.SpinnerWidget;

/**
 * Класс для реализации виджета spinner для поля Priority, поскольку виджет вызывается из двух мест
 * Created by Michael Shishkin on 06.11.2016.
 */
class PriorityWidget extends SpinnerWidget <Priority> {

    private boolean mLongView = true;   // флаг отображать ли в спинере иконку с текстом (true) или только иконку (false)

    //короткий конструктор без лисенера выбора элемента
    public PriorityWidget(Spinner spinner, Priority[] spinnerItems, Priority initPriority) {
        this(spinner, spinnerItems, initPriority, null);
    }

    // полный конструктор
    PriorityWidget(Spinner spinner, Priority[] spinnerItems, Priority initPriority, OnChangeSelectionListener <Priority> listener) {
        super(spinner, listener );

        spinner.setGravity(Gravity.END);

        setAdapter(createAdapter(spinnerItems, initPriority));
    }

    Adapter createAdapter(Priority[] items, Priority initial) {
        return new Adapter(R.layout.task_priority_spinner, R.id.prioritySpinner_Text, items, initial);
    }

    /**
     * устанавливает короткий формат вывода диалога (только иконка)
     */
    public void setShortFormatView(){
       mLongView = false;
    }


    /*
     *  реализация адаптера спинера.
     *  переопределен метод SpinnerAdapter.getCustomView для отображения иконки и текста
     */
    private class Adapter extends SpinnerAdapter<Priority> {
        Adapter(int resource, int textViewResourceId, Priority[] objects, Priority init) {
            super(resource, textViewResourceId, objects, init);
        }


        @SuppressWarnings("ConstantConditions")
        @Override
        protected View getCustomView(int position, ViewGroup parent, boolean isView) {
            View root = getRoot(parent);                        //корневой элемент task_priority_spinner.xml элемента спинера
            TextView priorityLabel  = getTextViewItem(root);    //текстовое поле

            if (mLongView || !isView )                          //если в спинере должно отображаться текстовое поле, оно отображается
                priorityLabel.setText(getItem(position).title);
            else
                priorityLabel.setText("");

            ImageView priorityIcon = root.findViewById(R.id.prioritySpinner_Icon);
            priorityIcon.setImageLevel(getItem(position).ordinal());

            return root;

        }
    }
}
