package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.view.View;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.task_filters.Choice;
import com.mishuto.todo.model.task_filters.DateFilter;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

/**
 * Фрагмент для установки фильтра по дате
 * Created by Michael Shishkin on 12.02.2019.
 */
public class DateFilterFragment extends AbstractFilterFragment<TCalendar, TCalendar, DateFilter> {

    public DateFilterFragment() {
        super(R.layout.fs_dlg_filters_item_date, FilterManager.date());
    }

    @Override
    public void onClick(View view, RecyclerViewWidget<Choice.Item<TCalendar>>.ViewHolder viewHolder) {
        mFilter.setSelector(viewHolder.getItem().getValue());
        mFilter.setFilter(true);
        closeDialog();
    }

    @Override
    public void onPageSelected() {
        super.onPageSelected();
        getParentDialog().setFABListener(null); // убираем FAB
    }
}
