package com.mishuto.todo.view_model.details.field_controllers;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CheckBox;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.database.aggregators.DBTreeSet;
import com.mishuto.todo.model.tags_collections.ContextsSet;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import java.util.TreeSet;

import static com.mishuto.todo.common.BundleWrapper.getDBObject;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.view.dialog.DialogHelper.doOk;
import static com.mishuto.todo.view.dialog.DialogHelper.inParameter;

/**
 * Диалог для ввода списка контекстов задачи.
 * Отображает глобальный чеклист контекстов и поле для ввода нового контекста
 * Created by Michael Shishkin on 13.01.2017.
 */

public class ContextDialog extends BaseTagDialog implements DialogInterface.OnClickListener {

    DBTreeSet<StringContainer> mOriginalSet;   // Список контекстов текущей задачи (в отличии от глобального списка контекстов, содержащегося в объекте ContextsSet.getInstance())
    TreeSet<StringContainer> mTemporarySet;  // Временная копия списка mOriginalSet для возможности его отмены


    @SuppressWarnings("unchecked") @NonNull @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mOriginalSet = (DBTreeSet<StringContainer>) getDBObject(inParameter(this));
        mTemporarySet = mOriginalSet.getShallowCopy();
        AlertDialog.Builder builder = prepareDialog(ContextsSet.getInstance(), R.layout.dlg_tag_item_context);

        builder.setTitle(R.string.contextDialog_title)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, null);

        return builder.create();
    }

    // связывание чекбоксов списка с контекстами
    @Override
    public void onBind(View tagView, StringContainer item, RecyclerViewWidget<StringContainer>.ViewHolder viewHolder) {
        super.onBind(tagView, item, viewHolder);
        ((CheckBox)tagView).setChecked(mTemporarySet.contains(item));
    }

    // обработка нажатия на чекбокс списка: удалить или добавить контекст из списка контекстов текущей задачи
    @Override
    public void onClick(View view, RecyclerViewWidget<StringContainer>.ViewHolder viewHolder) {
        if(((CheckBox)view).isChecked())
            mTemporarySet.add(viewHolder.getItem());
        else
            mTemporarySet.remove(viewHolder.getItem());
    }

    // обработка нажатия Enter в виджете ввода нового контекста: добавить контекст в глобальный список
    @Override
    boolean onAddCommand() {
        if (super.onAddCommand()) {
            mTemporarySet.add(mTagsSet.getLastAdded());
            return true;
        }
        return false;
    }

    // обработка нажатия кнопки OK диалога
    @Override
    public void onClick(DialogInterface dialog, int which) {
        if(which == DialogInterface.BUTTON_POSITIVE) {
            if(mTagsSet.add(mNewTagWidget.getText()))               //добавляем введенный в поле "новый контекст" контекст в глобальный список контекстов
                mTemporarySet.add(mTagsSet.getLastAdded());

            mOriginalSet.setCollection(mTemporarySet);              // сохраняем измененную копию спика контекстов задач в оригинальном списке
            doOk(this, toBundle(mOriginalSet));                     // передаем оригинальный измененный список в контроллер
        }
    }
}

