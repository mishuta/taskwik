package com.mishuto.todo.view_model.common;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mishuto.todo.database.SQLTable;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.todo.R;

/**
 * Служебная активность, запускаемая перед MainActivity и отображающая процесс загрузки TaskList
 * Акивность запускается либо из LoadCheckerActivity, либо из внешнего приложения при передаче файла,
 * таким образом, если приложение не было запущено, TaskList всегда загружается в этой активности.
 * После окончания загрузки стартует MainActivity, а эта завершается
 * Created by Michael Shishkin on 11.08.2018.
 */
public class LauncherScreenActivity extends AppCompatActivity {

    ProgressBar mProgressBar;   // прогресбар
    TextView mProgressLabel;    // текст под прогресбаром

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(TaskList.get().hasLoaded()) {   // первым делом проверяем не загружен ли уже список
            finish();            // если да (приложение уже было загружено), то контент не грузим, а сразу стартуем MainActivity
            return;
        }

        SQLTable taskTable = new SQLTable(Task.class.getSimpleName());                  // объект таблицы TASK
        int nTasks = taskTable.doesTableExist() ? taskTable.query().countNClose() : 0;  // вычисляем количество задач в таблице TASK
        if(nTasks > 0) {                                                                // если их больше 0
            setContentView(R.layout.activity_launcher);                                 // отображаем контент
            mProgressLabel = findViewById(R.id.loading);
            mProgressBar = findViewById(R.id.progressBar);                              // инициируем прогресбар...
            mProgressBar.setProgress(0);
            mProgressBar.setMax(nTasks);                                                // ...количеством задач
            new TasksLoader().execute((Void)null);                                      // запускаем AsyncTask
        }
        else {
            TaskList.get().load();                                                     // если задач 0, выполняем загрузку сразу
            finish();                                                        // и передаем управление
        }
    }



    // Класс для загрузки задач
    @SuppressLint("StaticFieldLeak")  // время жизни активити всегда больше чем TasksLoader
    class TasksLoader extends AsyncTask<Void, Void, Void> implements TaskEventsFacade.TaskObserver {

        // перед загрузкой регистрируем лисенер, чтобы получать событие загрузки
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            TaskEventsFacade.getInstance().register(this);
        }

        // событие обновления прогресса. Запускается в главном потоке
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            mProgressBar.incrementProgressBy(1);   // каждое событие = 1 задача загрузилась, двигаем прогресбар на 1
        }

        // собственно процедура загрузки. Запускается в отдельном потоке
        @Override
        protected Void doInBackground(Void... voids) {
            TaskList.get().load();
            return null;
        }

        // событие завершения загрузки - вызывается в главном потоке
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressBar.setProgress(mProgressBar.getMax());    // доводим прогресс до 100% (одна задача может быть загружена заранее в DetailsActvivty)
            mProgressLabel.setText(R.string.loaded);            // обновляем надпись на "загружено". Она будет видна короткое время, пока MainActivity грузится
            finish();
        }

        // событие обновления задачи (задача загружена)
        @Override
        public void onTaskEvent(TaskEventsFacade.EventType type, Task task, Object extendedData) {
            if(type == TaskEventsFacade.EventType.LOADED)
                publishProgress((Void)null);
        }
    }
}
