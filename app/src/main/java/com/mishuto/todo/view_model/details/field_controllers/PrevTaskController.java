package com.mishuto.todo.view_model.details.field_controllers;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.mishuto.todo.common.BundleWrapper.getLong;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_PREV;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_PREV_DISCARDS_TIME;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_TIME_DISCARDS_PREV;
import static com.mishuto.todo.view.Constants.OPAQUE_FULL;
import static com.mishuto.todo.view.Constants.OPAQUE_MID;

/**
 * Контроллер DetailsFragmentControl для отображения и изменения атрибута задачи "Предыдущая задача"
 * Created by Michael Shishkin on 24.12.2016.
 */

public class PrevTaskController extends ObserverController {

    private TextView mPrevTaskTitle;    // Выпадающий виджет с названием предыдущей задачи (если существует)
    private ImageView mCompletedIco;    // Иконка завершенной задачи
    private LinearLayout mLayout;       // контейнер для виджетов предыдущей задачи

    public PrevTaskController(View view, Task task) {
        super(view, task, R.id.taskDetails_label_prevTask, R.id.taskDetails_prevTask_state, R.id.taskDetails_clearPrev);

        setEventFilter(A_TIME_DISCARDS_PREV, A_PREV, A_PREV_DISCARDS_TIME);
        mPrevTaskTitle = setClickable(R.id.taskDetails_textView_prevTask);
        mCompletedIco = setClickable(R.id.taskDetails_ico_prevComp);
        mLayout = mRoot.findViewById(R.id.taskDetails_layout_prev);

        updateWidgets();
    }

    @Override
    public void updateWidgets() {
        if(mTask.getPrev() != null) {
            show(mLayout, mPrevTaskTitle);
            mPrevTaskTitle.setText(mTask.getPrev().toString());
            // завершенная задача затемняется и для нее рисуется иконка
            mPrevTaskTitle.setAlpha(mTask.getPrev().isStateCompleted() ? OPAQUE_MID : OPAQUE_FULL);
            mCompletedIco.setVisibility(mTask.getPrev().isStateCompleted() ? VISIBLE : GONE);
            setState("");
        }
        else {
            hide(mLayout, mPrevTaskTitle, mCompletedIco);
            setState(null);
        }
    }

    @Override
    void onAction() {
        createDialog(new PrevTaskDialog(), toBundle(mTask.getRecordID()));
    }

    @Override
    void onClear() {
        mTask.setPrev(null);
    }

    @Override
    public void onCloseDialog(int resultCode, Bundle resultObject) {
        Task prevTask = TaskList.getById(getLong(resultObject));
        mTask.setPrev(prevTask);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if(v.getId() == mPrevTaskTitle.getId())
            onAction();
    }
}
