package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.mishuto.todo.model.task_filters.Choice;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.dialog.BaseFullScreenDialog;
import com.mishuto.todo.view.dialog.TabbedDialog;

import java.util.Arrays;
import java.util.List;

/**
 * Диалог с фильтрами. Наследует TabbedDialog и инициирует страницы с фильтрами
 * Created by Michael Shishkin on 28.11.2017.
 */

public class FilterDialog extends TabbedDialog {

    // создание диалога
    // создает фрагмент и отображает объект FilterDialog
    public static void createDialog() {
        BaseFullScreenDialog.createDialog(new FilterDialog(), null);
    }

    public FilterDialog() {
        super(getTabs());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for(Fragment page : getPages() )
            ((AbstractFilterFragment)page).setParameters(this);
    }

    // создаем вкладки
    private static Tabs getTabs() {
        final Tab PROJECT = new Tab(ProjectFilterFragment.class, R.string.projectDialog_title, R.drawable.ic_timetable_white_24dp);
        final Tab CONTEXT = new Tab(ContextFilterFragment.class, R.string.contextDialog_title, R.drawable.ic_coffee_white_24dp);
        final Tab EXECUTOR = new Tab(ExecutorFilterFragment.class, R.string.executorDialog_title, R.drawable.ic_account_white_24dp);
        final Tab DATE = new Tab(DateFilterFragment.class, R.string.DateDialog_title, R.drawable.ic_calendar_today_white_24dp);

        List<Tab> tabsList = Arrays.asList(DATE, PROJECT, CONTEXT, EXECUTOR);
        Choice filters[] = {FilterManager.date(), FilterManager.project(), FilterManager.context(), FilterManager.executor()};

        Choice currentFilter = Choice.getActiveChoiceFilter();
        int defaultPos = tabsList.indexOf(CONTEXT);   // открытый по умолчанию фильтр. если фильтр не выбран, то CONTEXT

        for (int i = 0; i < tabsList.size(); i++)
            if(currentFilter == filters[i]) {
                defaultPos = i;
                break;
            }

        return new Tabs(tabsList, defaultPos);
    }
}