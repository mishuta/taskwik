package com.mishuto.todo.view_model.list;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mishuto.todo.common.PersistentValues;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TObjects;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.common.Time;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.model.task_filters.Choice;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;
import com.mishuto.todo.view_model.common.InterfaceEventsFacade;
import com.mishuto.todo.view_model.common.delay.DelayController;

import java.util.Collections;
import java.util.List;

import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.Event.CHANGE_TASK;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.Event.OPEN_CARD;

/**
 * Контроллер, управляющий списком задач
 * Created by Michael Shishkin on 23.12.2018.
 */
public class ListController implements
        TaskEventsFacade.TaskObserver,
        InterfaceEventsFacade.EventObserver,
        ListItemView.ItemHandler
{
    public static final int DELETE_SNACK_DURATION = 4500;   // длительность снека с предупреждением об удалении в мс
    private RecyclerViewWidget<Task> mTasksRecyclerView;    // виджет списка задач
    PersistentValues.PVBoolean isNewTaskInCard;             // флаг, устанавливается при передаче управления карточке задачи: новая задача или существующая
    private final View mRoot;

    ListController(View root) {
        mRoot = root;

        // создаем RecyclerView и инициализируем его актуальным отфильтрованным и отсортированным списком задач
        mTasksRecyclerView = new RecyclerViewWidget<>(
            (RecyclerView)mRoot.findViewById(R.id.recycler_view),
            R.layout.fragment_list_item,
            getSortedList(),
            new ListItemView(this));

        isNewTaskInCard = PersistentValues.getFile("ListController").getBoolean("isNewTaskInCard", false);
   }

   private List<Task> getSortedList() {
        List<Task> sorted = FilterManager.computeFilteredTaskList();
        Collections.sort(sorted);
        return sorted;
   }

    private boolean mMidNightPerformed = false;

    //обработка событий модели
    @Override
    public void onTaskEvent(TaskEventsFacade.EventType type, final Task task, Object data) {
        if(type == TaskEventsFacade.EventType.LOADED)
            return;

        if (new Time(TCalendar.now()).toInteger() == 0) {   // в полночь один раз рефрешится весь список. Во-первых, из-за большого количества обновлений.
            if (!mMidNightPerformed) {                      // Во-вторых, необходимо обновить даты в списке, например среда->завтра
                mMidNightPerformed = true;
                mTasksRecyclerView.setDataList(getSortedList());
            }
        }
        else {
            if(type == TaskEventsFacade.EventType.A_TIME)
                warnUnfiltered(task);                       // если задача была отложена в списке, и стоял DateFilter - нужно вывести снек

            updateRecyclerView(task);
            mTasksRecyclerView.scrollTo(0);
            mMidNightPerformed = false;
        }
    }

    // обработка событий интерфейса (от фрагммента карточки)
    @Override
    public void onInterfaceEvent(InterfaceEventsFacade.Event event, Object param) {
        switch (event) {
            case CHANGE_TASK:   // обработка события завершения редактирования задачи в карточке
            case CLOSE_CARD:

                InterfaceEventsFacade.CardMsg msg = (InterfaceEventsFacade.CardMsg) param;

                switch (msg.subEvent) {
                    case TASK_CONFIRM:
                        Snack.show(mRoot, isNewTaskInCard.get() ? R.string.taskList_taskAdded : R.string.taskList_taskChangeSaved);
                        warnUnfiltered(msg.task);
                        break;

                    case TASK_DELETE:
                        if (!isNewTaskInCard.get())
                            removeTask(msg.task);

                    case TASK_UNCHANGED:
                    case TASK_CANCEL:
                        if (isNewTaskInCard.get()) {
                            TaskList.remove(msg.task);
                            updateRecyclerView(msg.task);
                        }
                        break;

                    case TASK_WAS_SAVED:
                        //updateRecyclerView(msg.task);
                        break;
                }

                if (event == CHANGE_TASK)
                    isNewTaskInCard.set(false);

            case FILTER_CHANGED:
            case LIST_LOADED:
                updateRecyclerView(null);
                mTasksRecyclerView.scrollTo(0);
                break;
        }
    }

    /**
     * отрисовка измененного списка при изменении задачи для анимации виджета
     * поскольку изменение одной задачи, может потянуть каскадное обновление других, выполняется приведение текущего списка к целевому по каждому событию изменения задачи
     * @param changedTask Задача, которая обновилась. Может быть null, тогда обновляется список, но не конкретная задача
     */
    private void updateRecyclerView(Task changedTask) {
        TSystem.debug(this, "updateRecyclerView");
        List<Task> currentList = mTasksRecyclerView.getDataList();              // текущий список виджета (обычно близок к целевому, за исключением 1 задачи. Но могут быть и большие отклонения)
        List<Task> targetList = getSortedList();                                // целевой (список задач для отображения)

        for (int i = 0; i< targetList.size(); i++)                              // проход 1. добавляем в текущий список все все отсутствующие в нем задачи, имеющиеся в целевом
            if (!currentList.contains(targetList.get(i)))
                mTasksRecyclerView.insertAt(i, targetList.get(i));

        for(Task t : TObjects.clone(currentList))                               // проход 2. удаляем из текущего списка все задачи, отстутствующие в целевом
            if(!targetList.contains(t))                                          // используется копия списка, чтобы не вызвать ConcurrentException во время удаления
                mTasksRecyclerView.deleteAt(currentList.indexOf(t));

        for (int i = 1; i< currentList.size(); i++) {                           // проход 3. выполняем сортировку с визуальным перемещением
            int j = i;                                                          // используется insertion sort, т.к. список и виджет поддерживают вставку элемента out of the box...
            while ( j > 0 && currentList.get(i).compareTo(currentList.get(j - 1)) < 0 )
                j--;

            if ( j < i )                                                        // ... таким образом фактически перемещать элементы нужно не более чем за O(n) раз,
                mTasksRecyclerView.move(i, j);                                  // а на самом деле гораздо меньше, т.к. массив практически отсортирован и требует в среднем 1-го перемещения
        }

        if(changedTask != null)
            mTasksRecyclerView.changeAt(currentList.indexOf(changedTask)); // обновляем текущую задачу в списке, т.к. атрибуты изменились (а позиция могла не поменяться)
    }

    /**
     * Реализация удаления задачи
     * Реализовано только в фрагменте списка, т.к. в DetailsFragment задача не удаляется
     * Удаление двухфазное. Команда "удалить" помещает задачу в фильтр на удаление.
     * Фрагмент отображает snack-сообщение с кнопкой UNDO
     * UNDO вытаскивает задачу из фильтра. Иначе она удаляется после исчезновения Snack
     * @param task - удаляемая задача
     */
    private void removeTask(final Task task) {
        FilterManager.unhidden().hide(task);

        Snack.show(mRoot,
                String.format(mRoot.getContext().getString(R.string.taskMenuRemove_warning), task.toString()), // Выводим предупреждение
                new Snack.OnDismissedEvent() {                                              // анонимный обработчик исчезновения: ...
                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {       // обработчик события исчезновения снека
                        super.onDismissed(transientBottomBar, event);
                        if(event != DISMISS_EVENT_ACTION &&                                 // кроме события нажатия на кнопку
                                !FilterManager.unhidden().isFiltered(task)) {                  // если задачу уже не удалили
                            TaskList.remove(task);                                          // удаляем задачу физически
                            FilterManager.unhidden().show(task);                   // изымаем задачу из фильтра удаленных

                        }
                    }
                },
                new View.OnClickListener() {                                                // анонимный обработчк UNDO
                    @Override
                    public void onClick(View v) {
                        FilterManager.unhidden().show(task);                       // вытаскиваем задачу из фильтра
                        mTasksRecyclerView.scrollTo(mTasksRecyclerView.getPosition(task));  // скроллим на спасенную задачу
                    }
                },
                R.string.taskMenuRemove_undo, DELETE_SNACK_DURATION);
    }

    // отображает предупреждение, если данная задача не попала в список из-за установленных фильтров
    private void warnUnfiltered(Task task) {
        if(!FilterManager.computeFilteredTaskList().contains(task)) {
            if(!FilterManager.incomplete().isFiltered(task))
                Snack.show(mRoot, R.string.taskList_completedWarn);

            else if(!FilterManager.actual().isFiltered(task))
                Snack.show(mRoot, R.string.taskList_deferredWarn);

            else if(FilterManager.activated().isFilterSet())
                Snack.show(mRoot, R.string.taskList_filteredWarn);

            else {
                Choice choiceFilter = Choice.getActiveChoiceFilter();
                if (choiceFilter != null && !choiceFilter.isFiltered(task))
                    Snack.show(mRoot, R.string.taskList_filteredWarn);
            }
        }
    }

    @Override
    public PopupMenu.OnMenuItemClickListener getMenuHandler(Task task, Menu menu) {
        return new MenuHandler(task, menu);
    }

    @Override
    public void editTask(Task task) {
        InterfaceEventsFacade.send(OPEN_CARD, task);
    }

    //Обработчик меню операций над задачей
    private class MenuHandler implements PopupMenu.OnMenuItemClickListener {

        Task mTask;

        MenuHandler(Task task, Menu menu) {
            mTask = task;
            menu.findItem(R.id.taskMenu_complete).setTitle(findCompleteItemTitle());
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.taskMenu_complete:
                    mTask.setPerformed(!mTask.isStateInPerformed());
                    if(mTask.isStateRepeated() )
                        Snack.show(mRoot, mRoot.getContext().getString(R.string.repeat_task_at, mTask.getRecurrence().getNextEvent().toString()));

                    item.setTitle(findCompleteItemTitle());
                    return true;

                case R.id.taskMenu_delay:
                    DelayController.delayTask(mTask);
                    return true;

                case R.id.taskMenu_remove:
                    removeTask(mTask);
                    return true;

                default:
                    return false;
            }
        }

        // вычисление названия пункта меню завершить
        private int findCompleteItemTitle() {
            return mTask.isStateInPerformed() ? R.string.taskMenu_activate :                // если задача завершена: "Активировать"
                    mTask.getRecurrence().isSet() ?  R.string.taskMenu_completeAndRepeat :  // если задача повторяемая "Завершить и повторить"
                            R.string.taskMenu_complete;                                     // иначе "Завершить"
        }
    }
}
