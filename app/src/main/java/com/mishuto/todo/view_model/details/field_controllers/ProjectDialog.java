package com.mishuto.todo.view_model.details.field_controllers;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.model.tags_collections.ProjectsSet;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.view.dialog.DialogHelper.doOk;

/**
 * Диалог для ввода проекта задачи.
 * Отображает список проектов и поле для ввода нового проекта
 * Created by Michael Shishkin on 04.01.2017.
 */

public class ProjectDialog extends BaseTagDialog {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = prepareDialog(ProjectsSet.getInstance(), R.layout.dlg_tag_item_project);

        builder.setTitle(R.string.projectDialog_title);
        return builder.create();
    }

    //  обработка клика по строке списка
    @Override
    public void onClick(View view, RecyclerViewWidget<StringContainer>.ViewHolder viewHolder) {
        closeDialog(viewHolder.getItem());
    }

    // обработка нажатия Enter при вводе нового проекта
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(super.onEditorAction(v, actionId, event)) {
            closeDialog(mTagsSet.getLastAdded());
            return true;
        }
        return false;
    }

    // нажатие кнопки [+]
    @Override
    public void onClick(View v) {
        super.onClick(v);
        closeDialog(mTagsSet.getLastAdded());

    }

    // упаковка выбранного проекта и закрытие диалога
    void closeDialog(StringContainer projectTag) {
        doOk(this, toBundle(projectTag));
        getDialog().dismiss();
    }
}
