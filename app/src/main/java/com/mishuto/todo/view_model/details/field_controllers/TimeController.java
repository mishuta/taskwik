package com.mishuto.todo.view_model.details.field_controllers;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.Time;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.dialog.TimeDialog;

import static android.app.Activity.RESULT_OK;
import static com.mishuto.todo.common.BundleWrapper.getSerializable;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.common.TCalendar.now;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_PREV_DISCARDS_TIME;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_TIME;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_TIME_DISCARDS_PREV;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.S_PERFORMED;
import static java.util.Calendar.DATE;
import static java.util.Calendar.MINUTE;

/**
 * Контроллер DetailsFragmentControl для отображения и изменения атрибута задачи "Время"
 * Created by Michael Shishkin on 05.11.2016.
 */

public class TimeController extends ObserverController {

    private TaskTime mTaskTime;             // Время события - атрибут модели
    private ImageView mAlert;               // Иконка с предупреждением о прошедшем времени

    public TimeController(View view, Task task) {
        super(view, task, R.id.taskDetails_label_time, R.id.taskDetails_textView_time, R.id.taskDetails_clearTime);
        setEventFilter(A_TIME_DISCARDS_PREV, A_TIME, A_PREV_DISCARDS_TIME, S_PERFORMED);
        mAlert = setClickable(R.id.taskDetails_image_timeAlert);

        updateWidgets();
    }

    @Override
    void onAction() {
        createDialog(new TimeDialog(), toBundle(mTaskTime.isTimeSet() ? new Time(mTaskTime.getHour(), mTaskTime.getMinute()) : null));
    }

    @Override
    void onClear() {
        mTaskTime.resetTime();
        mTask.setTaskTime(mTaskTime);
    }

    @Override
    public void updateWidgets() {
        mTaskTime = mTask.getTaskTime();
        if(mTaskTime.isTimeSet()) {                         // если время установлено
            TCalendar calendar = mTaskTime.getCalendar();
            setState(calendar.getFormattedTime());          // отображается время задачи
            mAlert.setVisibility(                           // значок предупреждения, если время не больше текущего для сегодняшней даты
                    calendar.intervalFrom(DATE, now()) == 0 && calendar.intervalFrom(MINUTE, now()) < 0 ?   // время calendar округлено вниз до минут, поэтому
                            View.VISIBLE : View.GONE                                                        // 22:15:00.interval(MINUTE, 22:14:01)==0
            );
        }
        else {                                              // иначе пиктограммы скрываются
            setState(null);
            hide(mAlert);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v.getId() == mAlert.getId())            // Если нажата иконка с предупреждением - показываем предупреждение
            Snack.show(v, R.string.taskDetails_warn_time);
    }

    // обработчик выходных данных диалога
    @Override
    public void onCloseDialog(int resultCode, Bundle resultObject) {
        if(resultCode == RESULT_OK) {
            Time result = (Time) getSerializable(resultObject);
            mTaskTime.setTimePart(result.getHour(), result.getMinute());
            mTask.setTaskTime(mTaskTime);
        }
    }
}
