package com.mishuto.todo.view_model.details.field_controllers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mishuto.todo.model.task_fields.Comments;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.LayoutWrapper;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view.edit.AbstractCompoundEditFreezeView;
import com.mishuto.todo.view.edit.EditFreezeView;
import com.mishuto.todo.view.edit.EditView;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import java.util.HashMap;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.mishuto.todo.common.BundleWrapper.getDBObject;
import static com.mishuto.todo.view.dialog.DialogHelper.inParameter;

/**
 * Диалог для изменения комментариев к задаче
 * Created by Michael Shishkin on 23.01.2017.
 */

public class CommentDialog extends DialogFragment implements
        View.OnClickListener,
        EditView.OnChangeListener
{
    Comments mComments;                                         // список комментариев
    EditView mNewComment;                                       // виджет ввода нового комментария
    ImageButton mAddButton;                                     // кнопка добавления нового комментария
    RecyclerViewWidget<Comments.Description> mCommentsWidget;   // виджет списка комментариев
    boolean mChanged = false;                                   // флаг - были ли внесены изменения

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dlg_comments, (ViewGroup) getView());

        mComments = ((Comments) getDBObject(inParameter(this)));

        mNewComment =  new EditView (dialogView.findViewById(R.id.dlg_description_newItem), this);
        mAddButton = dialogView.findViewById(R.id.addButton);
        RecyclerView recyclerView =dialogView.findViewById(R.id.dlg_description_cardsList);

        mCommentsWidget = new RecyclerViewWidget<>(recyclerView, R.layout.dlg_comment_card, mComments.getList(), new RecyclerWidgetListener());
        mAddButton.setEnabled(false);
        mAddButton.setOnClickListener(this);

        return new AlertDialog.Builder(getActivity())
                .setView(dialogView)
                .setTitle(R.string.commentDialog_title)
                .create();
    }

    // нажатие кнопки добавления нового комментария
    @Override
    public void onClick(View v) {
        if(mComments.add(mNewComment.getText())) {  // добавляем комментарий в объект модели
            mCommentsWidget.notifyInsertedAt(0);    // отрисовываем список
            mNewComment.reset();                    // сбрасываем поле ввода
            DialogHelper.doOk(this, null);
            getDialog().dismiss();
        }
    }

    // обработка события изменения текста при вводе нового комментария
    @Override
    public boolean onTextValidate(EditView view, String text) {
        boolean isValid = Comments.isValidDesc(text);
        mAddButton.setEnabled(isValid);
        return isValid || text.equals("");          // в случае пустой строки - ошибка не подсвечивается
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if(mChanged)    // если изменения были - возвращаем ОК, иначе CANCEL
            DialogHelper.doOk(this, null);
        else
            DialogHelper.doCancel(this);
    }

    // Класс - обработчик событий списка комментариев
    private class RecyclerWidgetListener implements RecyclerViewWidget.EventsListener<Comments.Description> {

        // Внутренний класс, отвечает за виджеты текущего элемента списка RecyclerView
        private class DescriptionWidgets implements
                EditView.OnChangeListener,
                AbstractCompoundEditFreezeView.AutoChangeModeListener
        {
            TextView mDate;                             // дата. Используется в onBind при инициации
            ImageButton mDeleteBtn, mCancelBtn, mOkBtn; // кнопки
            EditFreezeView mCommentView;                // поле комментария
            LinearLayout mButtonBar;

            DescriptionWidgets(View root) {
                mDate = root.findViewById(R.id.dateView);
                mDeleteBtn = root.findViewById(R.id.buttonDelete);
                mCancelBtn = root.findViewById(R.id.buttonCancel);
                mOkBtn = root.findViewById(R.id.buttonOk);
                mCommentView = root.findViewById(R.id.commentView);
                mButtonBar = root.findViewById(R.id.button_bar);

                mCommentView.setAutoChangeModeListener(this);

                mCommentView.getEditView().setListener(this);
                showControlPanel(false);

                mDate.setOnClickListener(new View.OnClickListener() {  // нажатие на дату переводит комментарий в режим редактирования
                    @Override
                    public void onClick(View v) {
                        mCommentView.setEditMode();
                        mCommentView.getEditTextView().setSelection(0); // устанавливаем курсор в первую позицию, чтобы начало текста оказалось на экране
                        showControlPanel(true);
                    }
                });
            }

            // событие изменения текста комментария (валидация поля на лету)
            @Override
            public boolean onTextValidate(EditView view, String text) {
                boolean valid  = Comments.isValidDesc(text);
                mOkBtn.setEnabled(valid);
                return valid;
            }

            @Override
            public void onAutoChangeMode(AbstractCompoundEditFreezeView view, boolean toViewMode) {
                view.getEditTextView().setSelection(0); // устанавливаем курсор в первую позицию, чтобы начало текста оказалось на экране
                showControlPanel(!toViewMode);
            }

            void showControlPanel(boolean visible) {
               int visibility = visible ? VISIBLE : GONE;
                for(View view : LayoutWrapper.createLayoutIterator(mButtonBar))
                    view.setVisibility(visibility);
                mButtonBar.setVisibility(visibility);
            }
        }

        // HashMap для хранения виджетов текущего элемента списка. Виджеты создаются в onBind и используются в onClick
        HashMap<RecyclerViewWidget<Comments.Description>.ViewHolder, DescriptionWidgets> mDescriptionWidgetsMap = new HashMap<>();

        public void onBind(View root, Comments.Description item, RecyclerViewWidget<Comments.Description>.ViewHolder viewHolder) {

            // Создане и сохранение для повторного использования в onClick виджетов текущего элемента списка
            DescriptionWidgets widgets = new DescriptionWidgets(root);
            mDescriptionWidgetsMap.put(viewHolder, widgets);

            // Инициализация виджетов
            widgets.mDate.setText(item.getDate().toString());
            widgets.mCommentView.setText(item.getComment());

            widgets.mOkBtn.setOnClickListener(viewHolder);
            widgets.mDeleteBtn.setOnClickListener(viewHolder);
            widgets.mCancelBtn.setOnClickListener(viewHolder);
        }

        // обработка нажатия на виджет карточки комментария
        @Override
        public void onClick(View view, RecyclerViewWidget<Comments.Description>.ViewHolder viewHolder) {

            DescriptionWidgets widgets = mDescriptionWidgetsMap.get(viewHolder);    // получаем список виджетов

            switch (view.getId()) {
                case R.id.buttonDelete:           // Удаление комментария
                    viewHolder.remove();
                    mChanged = true;
                    mDescriptionWidgetsMap.remove(viewHolder);
                    break;

                case R.id.buttonCancel:             // Отмена изменений
                    widgets.mCommentView.setViewMode();
                    widgets.mCommentView.setText(viewHolder.getItem().getComment());
                    widgets.showControlPanel(false);
                    break;

                case R.id.buttonOk:               // Сохранение измененного комментария
                    if(Comments.isValidDesc(widgets.mCommentView.getText())) {
                        viewHolder.getItem().setComment(widgets.mCommentView.getText());
                        mChanged = true;
                        // Переход в ViewMode
                        widgets.mCommentView.setViewMode();
                        widgets.showControlPanel(false);
                    }
                    break;

                default:
                    break;
            }
        }
    }
}
