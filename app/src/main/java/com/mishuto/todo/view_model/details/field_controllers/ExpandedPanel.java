package com.mishuto.todo.view_model.details.field_controllers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Раскрываемымй список (контексты, зависимые задачи)
 * Created by Michael Shishkin on 12.01.2017.
 */


 class ExpandedPanel implements RecyclerViewWidget.EventsListener<String>, View.OnClickListener
{
    private View mCaption;                                  // Заголовок панели
    private ImageButton mExpand;                              // Кнопка Развернуть
    private ImageButton mCollapse;                            // Кнопка свернуть
    private RecyclerView mPanel;                            // Выпадающая панель
    private RecyclerViewWidget<String> mRecyclerViewWidget; // Виджет списка
    private List<String> mList;                             // Список элементов
    private int mMinItemsExpands;                           // Минимальное количество элементов, при котором панель отображается
    private View.OnClickListener mClickListener;

    ExpandedPanel(View caption, ImageButton expandBtn, ImageButton collapseBtn, RecyclerView panel, List<String> listItems, int minItems, View.OnClickListener listener)
    {
        mCaption = caption;
        mExpand = expandBtn;
        mCollapse = collapseBtn;
        mPanel = panel;
        mList = listItems;
        mMinItemsExpands = minItems;
        mClickListener = listener;

        hidePanel();                                   // При открытии карточки задачи список свернут

        mExpand.setOnClickListener(this);
        mCollapse.setOnClickListener(this);
        mCaption.setOnClickListener(this);
        mRecyclerViewWidget = new RecyclerViewWidget<>(mPanel, R.layout.fragment_details_expand_item, listItems, this);

    }

    // текущий отображаемый список
    final List<String> getList() {
        return mList;
    }

    // установить список для RecyclerView
    final void setList(List<String> list) {
        mList = list;
        mRecyclerViewWidget.setDataList(list);
    }

    // Обработка нажатия элементов
    @Override
    public void onClick(View v) {
        if(mCaption.getId() == v.getId()) {
            if (getList().size() >= mMinItemsExpands)     // заголовок служит заголовком списка и меняет отображения панели
                changePanel();
            else if (mClickListener != null)
                mClickListener.onClick(v);
        }

        else if(mExpand.getId() == v.getId())                                       // кнопка Развернуть
            showPanel();

        else if(mCollapse.getId() == v.getId())                                     // кнопка Свернуть
            hidePanel();
    }

    @Override
    public void onBind(View view, String item, RecyclerViewWidget<String>.ViewHolder viewHolder) {
        ((TextView)view).setText(item);
    }

    @Override
    public void onClick(View view, RecyclerViewWidget<String>.ViewHolder viewHolder) {
        if(mClickListener != null)
            mClickListener.onClick(view);
    }

    // Развертывание панели
    private void showPanel() {
        if(getList().size() >= mMinItemsExpands ) {
            mPanel.setVisibility(VISIBLE);
            mCollapse.setVisibility(VISIBLE);
            mExpand.setVisibility(GONE);
        }
    }

    // Схлопывание
    private void hidePanel() {
        mPanel.setVisibility(GONE);
        mCollapse.setVisibility(GONE);
        mExpand.setVisibility(getList().size() >= mMinItemsExpands ? VISIBLE: GONE);
    }

    // изменение (открыто<->закрыто
    private void changePanel() {
        if (mPanel.getVisibility() == VISIBLE)
            hidePanel();
        else
            showPanel();
    }

    // перерисовывание панели и кнопок
    final void refreshPanel() {
        if(getList().size() < mMinItemsExpands || mPanel.getVisibility() == GONE)          // Если список пустой или сама панель скрыта
            hidePanel();                                                     // выпадающая панель и кнопки скрываются
        else
            showPanel();
    }
}
