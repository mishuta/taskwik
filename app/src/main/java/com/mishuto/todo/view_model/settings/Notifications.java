package com.mishuto.todo.view_model.settings;

import android.view.View;
import android.widget.Switch;

import com.mishuto.todo.common.Accessor;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.SwitchWidget;

/**
 * Секция Уведомления диалога Settings
 * Created by Michael Shishkin on 19.07.2018.
 */
class Notifications {
    private SwitchWidget mSound;
    private SwitchWidget mVibrate;
    private SwitchWidget mIndicator;

    private static Settings.NotificationSettings NOTIFICATIONS = Settings.getNotificationSettings();

    Notifications(View root) {

        // выключатель isOn
        new SwitchWidget((Switch) root.findViewById(R.id.settings_notifySwitch),
                new Accessor<Boolean>() {
                    @Override
                    public Boolean get() {
                        return NOTIFICATIONS.isOn.get();
                    }

                    @Override
                    public void set(Boolean value) {
                        NOTIFICATIONS.isOn.set(value);
                        onUpdate();
                    }
                });

        mSound = new SwitchWidget((Switch) root.findViewById(R.id.settings_soundSwitch), NOTIFICATIONS.doSound);
        mVibrate = new SwitchWidget((Switch) root.findViewById(R.id.settings_vibrateSwitch), NOTIFICATIONS.doVibrate);
        mIndicator = new SwitchWidget((Switch) root.findViewById(R.id.settings_indicatorSwitch), NOTIFICATIONS.doLight);
        onUpdate();
    }

    private void onUpdate() {
        boolean isNotify = NOTIFICATIONS.isOn.get();
        mSound.getSwitch().setEnabled(isNotify);
        mVibrate.getSwitch().setEnabled(isNotify);
        mIndicator.getSwitch().setEnabled(isNotify);
    }
}
