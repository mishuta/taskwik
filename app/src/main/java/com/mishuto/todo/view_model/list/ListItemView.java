package com.mishuto.todo.view_model.list;

import android.annotation.SuppressLint;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.LayoutWrapper;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import static com.mishuto.todo.common.TCalendar.now;
import static com.mishuto.todo.view.ViewCommon.color;
import static com.mishuto.todo.model.tags_collections.Tags.EMPTY_TAG;
import static com.mishuto.todo.view.Constants.OPAQUE_FULL;
import static com.mishuto.todo.view.Constants.OPAQUE_MID;
import static java.util.Calendar.DATE;

/**
 * Класс для отображения Задачи на шаблон строки списка задач fragment_list_item
 * Created by Michael Shishkin on 25.08.2016.
 */
class ListItemView implements RecyclerViewWidget.EventsListener<Task> {

    // интерфейс обработчика действий при клике на задачу
    interface ItemHandler {
        PopupMenu.OnMenuItemClickListener getMenuHandler(Task task, Menu menu); // возвращает обработчик меню по клику на кнопку меню задачи
        void editTask(Task task);                                               // открывает карточку задачи
    }

    private ItemHandler mItemHandler;

    ListItemView(ItemHandler handler) {
        mItemHandler = handler;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBind(View root, Task task, RecyclerViewWidget<Task>.ViewHolder viewHolder) {
        TextView[] detailViews = {                          // виджеты второй строки
                root.findViewById(R.id.taskListItem_pos1),  // поскольку поле сохраняется не в конкретную позицию
                root.findViewById(R.id.taskListItem_pos2),  // а в первую свободную
                root.findViewById(R.id.taskListItem_pos3)   // виджеты неименованы
        };

        TextView title =    root.findViewById(R.id.taskListItem_title);
        ImageView state =   root.findViewById(R.id.taskListItem_priority);
        ImageView order =   root.findViewById(R.id.taskListItem_Order);
        ImageView menuButton = root.findViewById(R.id.taskListItem_menuButton);

        menuButton.setOnClickListener(viewHolder);

        // инициация виджетов
        title.setText(task.toString());

        // если задача завершена ...
        if(task.isStateCompleted()) {
            state.setImageResource(R.drawable.ic_checkbox_marked_circle_outline_grey600_24dp);
            order.setVisibility(View.INVISIBLE);
        }

        // ... или отложена...
        else if(task.isSateDeferred()) {
            state.setImageResource(R.drawable.ic_alarm_grey600_24dp);
            order.setVisibility(View.INVISIBLE);
        }

        // ... или повторяемая, отображаем статус
        else if(task.isStateRepeated()) {
            state.setImageResource(R.drawable.ic_restore_grey600_24dp);
            order.setVisibility(View.INVISIBLE);
        }

        // если активна - отображаем приоритет и очередь
        else {
            state.setImageResource(R.drawable.ic_numeric_big);
            state.setImageLevel(task.getPriority().ordinal());
            order.setVisibility(View.VISIBLE);
            order.setImageLevel(task.getOrder().ordinal());
        }

        int vacantPos = 0;  // номер текущей позиции для записи поля задачи
        detailViews[vacantPos].setTextColor(color(R.color.colorSecondaryText)); // сбрасываем цвет 0-го поля

        // время
        if (task.getNearestEvent().isDateSet()) {
            TextView timeView = detailViews[vacantPos++];
            TCalendar time = task.getNearestEvent().getCalendar();

            if (task.getNearestEvent().isTimeSet())
                timeView.setText(time.getSpeakingString());
            else
                timeView.setText(time.getSpeakingDate());

            if(time.intervalFrom(DATE, now()) < 0)  // если дата ранее сегодняшнего дня, красим красным
                timeView.setTextColor(color(R.color.colorErroneousText));
        }

        // исполнитель
        if(task.getExecutor() != EMPTY_TAG) {
            if(vacantPos > 0)
                detailViews[vacantPos-1].setText(detailViews[vacantPos-1].getText() + ".");
            detailViews[vacantPos++].setText(task.getExecutor().toString());
        }

        // проект
        if(task.getProject() != EMPTY_TAG) {
            if(vacantPos > 0)
                detailViews[vacantPos-1].setText(detailViews[vacantPos-1].getText() + ".");
            detailViews[vacantPos++].setText(task.getProject().toString());
        }

        // комментарий
        if(vacantPos == 0 && task.getComments().getActual() != null)
            detailViews[vacantPos++].setText(task.getComments().getActual().toString());

        for(int i = vacantPos; i < detailViews.length; i++)
            detailViews[i].setText(null);

        for(View view : LayoutWrapper.createLayoutIterator(root))       // затемнение завершенных задач
            view.setAlpha(task.isStateCompleted() ? OPAQUE_MID : OPAQUE_FULL);
    }

    // клик по задаче
    @Override
    public void onClick(View view, RecyclerViewWidget<Task>.ViewHolder viewHolder) {
        Task task = viewHolder.getItem();

        if (view.getId() == R.id.taskListItem_menuButton) {      // если кликнули по значку меню
            final PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
            popupMenu.inflate(R.menu.task_menu);
            popupMenu.setOnMenuItemClickListener(mItemHandler.getMenuHandler(task, popupMenu.getMenu()));
            popupMenu.show();                                   // показываем popup меню
        }
        else
            mItemHandler.editTask(task);                        // иначе отправляем задачу на редактирование
    }
}
