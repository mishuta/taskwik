package com.mishuto.todo.view_model.details.field_controllers.recurrence_dialog;

import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.model.task_fields.recurrence.Recurrence;
import com.mishuto.todo.model.task_fields.recurrence.SegmentSelector;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.spinner.SpinnerWidget;
import com.mishuto.todo.view_model.details.TransactedFragmentSupporter;

import static com.mishuto.todo.common.BundleWrapper.getDBObject;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.view.ViewCommon.color;
import static com.mishuto.todo.view.dialog.DialogHelper.doCancel;
import static com.mishuto.todo.view.dialog.DialogHelper.doOk;
import static com.mishuto.todo.view.dialog.DialogHelper.inParameter;
import static com.mishuto.todo.view_model.details.TransactedFragmentSupporter.ExitState.CANCEL;
import static com.mishuto.todo.view_model.details.TransactedFragmentSupporter.ExitState.OK;

/**
 * Класс отрисовывает страницу-контейнер для вложенных страниц Recurrence.
 * Содержит спинер для вывода списка страниц и кнопки диалога
 *
 * Created by Michael Shishkin on 10.11.2016.
 */
@SuppressWarnings("ConstantConditions")
public class ContainerDialog
        extends DialogFragment implements View.OnClickListener,
        BasePageFragment.OnPageUpdateListener {

    private TextView mNextDate;                     // Виджет для времени следующего срабатывания
    private Recurrence mRecurrence;                 // Входной параметр
    private SegmentSelector mShowedSegment = null;  // текущий отображаемый сегмент

    final static int BUTTON_OK = R.id.recurrence_buttonOk;
    final static int BUTTON_CANCEL = R.id.recurrence_buttonCancel;
    final static int NEXT_DATE = R.id.recurrence_repeatDate;

    TransactedFragmentSupporter mTransactor;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)  {

        //noinspection ConstantConditions - getWindow returns null, if the activity is not visual
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);    // убрать заголовок в Android 5.1 и ниже

        mRecurrence = ((Recurrence)getDBObject(inParameter(this)));

        // связывание виджетов контейнера дочерних страниц с переменными
        View root = LayoutInflater.from(getActivity()).inflate(R.layout.dlg_rec_page_container, (ViewGroup)getView());
        mTransactor = new TransactedFragmentSupporter(this, savedInstanceState, root);

        mNextDate = root.findViewById(NEXT_DATE);

        // инициация спиннера - селектора сегментов
        SegmentSpinner spinner = new SegmentSpinner((Spinner) root.findViewById(R.id.recurrence_page_selector), SegmentSelector.values(), mRecurrence.getSegmentSelector());

        // индиктор drop-down красится в белый
        spinner.getSpinner().getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.textColorPrimary), PorterDuff.Mode.SRC_ATOP);

        // информационная иконка отображается, если задача незавершена
        if(!mRecurrence.isStartTimeSet()) {
            mNextDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_information_outline_grey600_18dp, 0);
            mNextDate.setOnClickListener(this);
        }

        // установка лисенеров для кнопок
        root.findViewById(BUTTON_OK).setOnClickListener(this);
        root.findViewById(BUTTON_CANCEL).setOnClickListener(this);

        showPage(mRecurrence.getSegmentSelector());

        return root;
    }

    // Событие нажатия OK / Cancel / Information
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case BUTTON_OK:
                getDialog().dismiss();                              // закрываем окно
                mRecurrence.set(true);                              // устанавливаем флаг "Повторение установлено"
                doOk(this, toBundle(mRecurrence));                  // передаем [копию] mRecurrence в контроллер
                mTransactor.setFinishState(OK);
                break;

            case BUTTON_CANCEL:
                getDialog().dismiss();
                mTransactor.setFinishState(CANCEL);
                doCancel(this);
                break;

            case NEXT_DATE:
                Snack.show(getView(), R.string.taskDetails_warn_recurrence);
                break;
        }
    }

    // нештатное завершение диалога, например, по нажжатию за его пределами. Должен вызвать doCancel для отмены транзакции
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        mTransactor.setFinishState(CANCEL);
        doCancel(this);
    }

    // Событие изменения данных на дочерней странице
    @Override
    public void onPageUpdated(boolean changed) {
        mNextDate.setText(mRecurrence.getNextEvent().toString());  // вычисляем время до ближайшего повторения
        if(changed)
            mRecurrence.set(true);
    }

    // Отображение дочерней страницы
    private void showPage(SegmentSelector segmentSelector) {
        if(mShowedSegment == segmentSelector)
            return;

        TSystem.debug(this, "showPage: " + segmentSelector.name());
        mShowedSegment = segmentSelector;

        BasePageFragment pageFragment = getPageFragment(segmentSelector.name());
        if (pageFragment == null)
            pageFragment = createPage(segmentSelector);

        //noinspection unchecked
        pageFragment.setParams(mRecurrence.getCurrentSegment(), this);
        setPage(pageFragment, segmentSelector.name());
    }

    private BasePageFragment createPage(SegmentSelector segmentSelector) {
        TSystem.debug(this, "createPage: " + segmentSelector.name());

        switch (segmentSelector) {
            case DAY:
                return new DayFragment();

            case WEEK:
                return new WeekFragment();

            case MONTH:
                return new MonthFragment();

            case YEAR:
                return new YearFragment();

            default:
                throw new AssertionError("unexpected segmentSelector"); // unreachable
        }
    }

    //получение дочернего фрагмента по тегу
    private BasePageFragment getPageFragment(String tag) {
        return (BasePageFragment) getChildFragmentManager().findFragmentByTag(tag);
    }

    // создание или замена дочерней страницы
    private void setPage(BasePageFragment fragment,  String tag) {

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        // для первой страницы должен использоваться метод add, но используется replace - это допустимо. Код становится проще и
        // избавляемся от ошибочного повторного add в случае перезагрузки фрагмента андроидом
        ft.replace(R.id.recurrence_layout_page, fragment, tag).commit();
        getChildFragmentManager().executePendingTransactions();
    }

    /**
     * Класс для спинера селектора страниц. Переопределяет onItemSelected
     */
    private class SegmentSpinner extends SpinnerWidget<SegmentSelector> {
        SegmentSpinner(Spinner spinner, SegmentSelector[] array, SegmentSelector initItem) {
            super(spinner, array, initItem, null);
        }

        // Переопределение onItemSelected для отрисовки контрола белыми буквами
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            if(view == null)    // баг андроида 5.1+: после поворота экрана может вернуться null в view
                return;

            // получаем TextView, меняем цвет текста
            TextView textView = (TextView)view;
            textView.setTextColor(color(R.color.textColorPrimary));

            SegmentSelector item = mAdapter.getItem(position);

            if(mShowedSegment != item) {
                mRecurrence.setSegmentSelector(item);   // устанавливаем выбранный сегмент как активный
                showPage(item);                         // отображаем активный сегмент
            }
        }
    }
}
