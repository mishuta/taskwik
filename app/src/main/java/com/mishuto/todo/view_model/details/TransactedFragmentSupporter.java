package com.mishuto.todo.view_model.details;

import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;
import android.os.Bundle;
import android.view.View;

import com.mishuto.todo.common.BundleWrapper;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.database.Transactions;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;

import static android.arch.lifecycle.Lifecycle.Event.ON_START;
import static android.arch.lifecycle.Lifecycle.Event.ON_STOP;

/**
 * Поддержка транзакционного сохранения/отката данных в интерактивных фрагментах
 * При старте фрагмента открывается транзакция. На выходе - комитится.
 * Если пользователь отменяет ввод - транзакция откатывается
 * Если пользователь переводит приложение в бэк - данные сохраняются (промежуточное сохранение - транзакция комитится). При возвращении из бэка - транзакция открывается
 * Это сделано, чтобы пользовтаель не потерял свои данные (путем уменьшения возможности для отмены),
 * поскольку нет гарантированного способа отследить уничтожение приложения, когда оно находится в бэкграунде (чтобы сделать коммит перед смертью)
 * Created by Michael Shishkin on 01.03.2018.
 */

@SuppressWarnings("WeakerAccess")
public class TransactedFragmentSupporter implements LifecycleObserver {

    public enum ExitState {OK, CANCEL, SKIP}                                        // перечень состояний на выходе из фрагмента (передается клиентом)

    private View mView;                                                             // главная View фрагмента
    private boolean mChanged = false;                                               // флаг: были ли изменения данных в фрагменте в последней транзакции
    private boolean mChangesSaved = false;                                          // флаг: были ли сохранены изменения за время существования фрагмента (включая автоперезагрузку приложения)
    private ExitState mExitState = ExitState.OK;                                    // состояние на выходе. по умолчанию транзакция коммитится
    private TransactionEvents mEventsListener;                                      // обработчик событий открытия/закрытия транзакций (если необходимо)

    // интерфейс для колбэков событий открытия/закрытия транзакций, поскольку штатных событий фрагментов не хватает
    public interface TransactionEvents {
        void afterTransactionStarted(); // поскольку событие ON_START вызывается ПОСЛЕ обработки onStart() в lifecycleOwner, то метод будет вызыван ПОСЛЕ открытия транзакции
        void beforeTransactionEnds();   // поскольку событие ON_STOP вызывается ДО обработки onStop() в lifecycleOwner, то метод будет вызыван ПЕРЕД закрытием транзакции
    }

    public TransactedFragmentSupporter(LifecycleOwner lifecycleOwner, Bundle savedInstanceState, View fragmentView) {
        TSystem.debug(this, "Created by " + lifecycleOwner.toString());
        lifecycleOwner.getLifecycle().addObserver(this);
        mView = fragmentView;

        if(savedInstanceState != null) {
            BundleWrapper bundleWrapper = new BundleWrapper(savedInstanceState);
            mChanged = bundleWrapper.getBoolean();      // по умолчанию флажки изменений
            mChangesSaved = bundleWrapper.getBoolean(); // равны false
        }
    }

    public void setEventsListener(TransactionEvents eventsListener) {
        mEventsListener = eventsListener;
    }

    // Клиент должен сообщить о завершении фрагмента с кодом выхода
    public void setFinishState(ExitState state) {
        mExitState = state;
    }

    // были ли изменения в текущей транзакции
    public boolean wasChanged() {
        return mChanged | Transactions.getInstance().dataWasChanged();
    }

    public boolean wasChangesSaved() {
        return mChangesSaved;
    }

    @OnLifecycleEvent(ON_START)
    public void onStart() {
        TSystem.debug(this, "ON_START");

        Transactions.beginTransaction();                            // открываем транзакцию
        if(mChanged)
            Snack.show(mView, R.string.taskList_taskChangeSaved);   // если были изменения - выводим снек о том что данные были сохранены

        mChanged = false;                                           // сбрасываем флаг изменений

        if(mEventsListener != null)
            mEventsListener.afterTransactionStarted();
    }

    @OnLifecycleEvent(ON_STOP)
    public void onStop() {
        TSystem.debug(this, "ON_STOP");

        if(mEventsListener != null)
            mEventsListener.beforeTransactionEnds();

        mChanged = wasChanged();

        switch (mExitState) {
            case CANCEL:
                Transactions.rollbackTransaction();
                break;

            case OK:
                Transactions.commitTransaction();
                break;

            case SKIP:  // "ручное" закрытие транзакции клиентом
                break;

        }
    }

    // сохранение изменений
    public void onSaveState(Bundle savedInstanceState) {
        TSystem.debug(this, "Saving transaction state");
        mChanged = wasChanged();
        mChangesSaved |= mChanged;
        new BundleWrapper(savedInstanceState).addParam(mChanged).addParam(mChangesSaved);
    }
}
