package com.mishuto.todo.view_model.details.field_controllers;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mishuto.todo.common.ResourcesFacade;
import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.model.tags_collections.Tags;
import com.mishuto.todo.model.tags_collections.TagsSet;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.edit.TextInputWidget;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import java.util.ArrayList;

/**
 * Диалог для ввода тегов задачи.
 * Отображает список тегов и поле для ввода нового тега
 * Created by Michael Shishkin on 04.01.2017.
 */

public class BaseTagDialog extends DialogFragment implements
        RecyclerViewWidget.EventsListener<StringContainer>,
        EditText.OnEditorActionListener,
        TextInputWidget.OnChangeTextListener,
        View.OnClickListener
{
    RecyclerViewWidget<StringContainer> mRecyclerViewWidget;    // Виджет списка
    ArrayList<StringContainer> mList = new ArrayList<>();       // список, содержащий данные для mRecyclerViewWidget
    TextInputWidget mNewTagWidget;                              // Виджет для ввода нового тега
    ImageButton mAddButton;                                     // Кнопка [+]
    TagsSet mTagsSet;                                           // Множество тэгов

    View mRoot;                                                 // Dialog view
    RecyclerView mRecyclerView;

    @NonNull
    public final AlertDialog.Builder prepareDialog(TagsSet tagsSet, int layout_item) {
        // инициализация виджетов
        mRoot = LayoutInflater.from(getActivity()).inflate(R.layout.dlg_tag_list, (ViewGroup)getView());
        mRecyclerView = mRoot.findViewById(R.id.recyclerView);
        mAddButton = mRoot.findViewById(R.id.dlg_tag_addButton);
        TextInputLayout textInputLayout = mRoot.findViewById(R.id.dlg_tag_newTag);
        mTagsSet = tagsSet;

        resetList();
        mRecyclerViewWidget = new RecyclerViewWidget<>(mRecyclerView, layout_item, mList, this, new LayoutManager());
        mNewTagWidget = new TextInputWidget(textInputLayout, this);
        mNewTagWidget.getEditText().setOnEditorActionListener(this);
        mAddButton.setOnClickListener(this);
        mAddButton.setEnabled(false);

        // создание диалога
        return new AlertDialog.Builder(getActivity()).setView(mRoot);
    }

    // Обработка клика по строке списка.
    // Не выполняет никаких действий, при необходимости переопределяется в сабклассе
    @Override
    public void onClick(View view, RecyclerViewWidget<StringContainer>.ViewHolder viewHolder) {

    }

    // инициализация элемента списка
    @Override
    public void onBind(View tagView, StringContainer item, RecyclerViewWidget<StringContainer>.ViewHolder viewHolder) {
        ((TextView)tagView).setText(item.toString());
    }

    // нажатие Enter при вводе нового тэга
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return (actionId == EditorInfo.IME_ACTION_GO && onAddCommand());
    }

    // нажатие кнопки добавить
    @Override
    public void onClick(View v) {
        onAddCommand();
    }

    // обработка команды Добавить тег
    boolean onAddCommand() {
        if(mTagsSet.add(mNewTagWidget.getText())) {  // Если введенный тег валиден

            // добавляем в список виджета новый тег
            resetList();
            int pos = mList.indexOf(mTagsSet.getLastAdded());
            mRecyclerViewWidget.notifyInsertedAt(pos);

            mNewTagWidget.resetView();              // сбрасываем поле ввода
            return true;
        }
        else
            return false;
    }

    // обработка изменения текста при вводе нового тега для отображения ошибки валидации
    @Override
    public void onTextChanged(String text) {

        boolean isErr = true;

        if(TextUtils.empty(text))
            mNewTagWidget.showError(R.string.tagDialog_noEmptyErr);
        else if(mTagsSet.contains(text))
            mNewTagWidget.showError(R.string.tagDialog_duplicate);
        else {
            mNewTagWidget.hideError();
            isErr = false;
        }

        mAddButton.setEnabled(!isErr);
    }

    // преобразуем отсортированное множество тегов в список
    private void resetList() {
        mList.clear();
        mList.addAll(mTagsSet.getTags());  // добавляем в список все теги
        mList.remove(Tags.EMPTY_TAG);      // кроме пустого - он не нужен
    }

    // Класс, переопределяющий LinearLayoutManager для RecyclerView
    private class LayoutManager extends LinearLayoutManager {

         LayoutManager() {
            super(getContext());
        }

        // Метод, вызываемый после отрисовки RecycleView, корректирует высоту RecycleView.
        // RecycleView растет и увеличивает размер окна диалога. Когда окно диалога упирается в размер экрана,
        // RecycleView начинает вытеснять виджет ввода нового тэга.
        // Чтобы этого не происходило, требуется зафиксировать ее высоту, чтобы она не росла дальше
        @Override
        public void onLayoutCompleted(RecyclerView.State state) {
            super.onLayoutCompleted(state);

            if( mRoot.getHeight() - mRecyclerView.getHeight() < ResourcesFacade.getDimensionPix(R.dimen.textInputWidget_height) ) {
                ViewGroup.LayoutParams layoutParams = mRecyclerView.getLayoutParams();
                layoutParams.height = mRoot.getHeight() - ResourcesFacade.getDimensionPix(R.dimen.textInputWidget_height);
            }
        }
    }
}
