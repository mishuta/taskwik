package com.mishuto.todo.view_model.details;

import android.view.View;
import android.widget.TextView;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view_model.details.field_controllers.CommentController;
import com.mishuto.todo.view_model.details.field_controllers.ContextController;
import com.mishuto.todo.view_model.details.field_controllers.DependedTaskController;
import com.mishuto.todo.view_model.details.field_controllers.ExecutorController;
import com.mishuto.todo.view_model.details.field_controllers.OrderController;
import com.mishuto.todo.view_model.details.field_controllers.PrevTaskController;
import com.mishuto.todo.view_model.details.field_controllers.PriorityController;
import com.mishuto.todo.view_model.details.field_controllers.ProjectController;
import com.mishuto.todo.view_model.details.field_controllers.RecurrenceController;
import com.mishuto.todo.view_model.details.field_controllers.TermController;
import com.mishuto.todo.view_model.details.field_controllers.TimeController;
import com.mishuto.todo.view_model.details.field_controllers.TitleController;

import java.util.ArrayList;
import java.util.List;

import static com.mishuto.todo.view_model.common.AbstractActivity.getCurrentActivity;

/**
 * TaskCardController - отображение карточки Задачи. Создает контроллеры, управляющие виджетами отдельных атрибутов задачи
 * Объект класса создается в DetailsFragment.onCreateView
 * Created by mike on 29.08.2016.
 */
public class TaskCardController implements TaskEventsFacade.TaskObserver {

    //Интерфейс, который должны реализовывать все контроллеры, отвечающие за изменение данных
    public interface EditableController {
        void updateWidgets();                       // обновления виджетов контроллера
        void onSave();                              // событие сохранения данных. Приходит перед закрытием или уходом в бэкграунд
        void onClose();                             // событие закрытия карточки
        void onPerformEvent(boolean isPerformed);   // событие завершения/активации задачи
    }

    private Task mTask;                         // Отображаемая задача

    private List<EditableController> mEditableControllers = new ArrayList<>();

    /**
     * Конструктор инициирует все виджеты шаблона fragment_details.xml.xml. Каждый виджет управляется своим контроллером ViewModel
     * @param v View fragment_details.xml.xml
     * @param t Объект модели задачи, отображаемой в фрагменте
     */
    TaskCardController(View v, Task t) {
        mTask = t;

        mEditableControllers.add(new TitleController(v, t));
        mEditableControllers.add(new TermController(v, t));
        mEditableControllers.add(new TimeController(v, t));
        mEditableControllers.add(new RecurrenceController(v, t));
        mEditableControllers.add(new PrevTaskController(v, t));
        mEditableControllers.add(new PriorityController(v, t));
        mEditableControllers.add(new ExecutorController(v, t));
        mEditableControllers.add(new ProjectController(v, t));
        mEditableControllers.add(new ContextController(v, t));
        mEditableControllers.add(new CommentController(v, t));
        mEditableControllers.add(new OrderController(v, t));
        new DependedTaskController(v, t);                                           // R/O контроллер
        TextView created = v.findViewById(R.id.taskDetails_TextView_CreatedDay);    // время создания задачи
        created.setText(t.getCreated().toString());

        if(mTask.isStateInPerformed())                                              // Если задача выполнена, выводим предупреждение
            Snack.show(v, R.string.taskDetails_completed);

        TaskEventsFacade.getInstance().register(this);  // подписываем представление на события изменения задачи
    }


    @Override
    public void onTaskEvent(final TaskEventsFacade.EventType type, final Task task, Object data) {
        if(task == mTask && type == TaskEventsFacade.EventType.S_PERFORMED)
                getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (EditableController controller : mEditableControllers)
                            controller.onPerformEvent(task.isStateInPerformed());   // если полученка команда завершить задачу - передаем ее всем редактируемым контроллерам
                    }
                });
    }

    void onClose() {
        for (EditableController controller : mEditableControllers)
            controller.onClose();

        TaskEventsFacade.getInstance().unregister(this);   // отписываемя от событий изменения задачи при уничтожении фрагмента
    }

    void onSave() {
        for (EditableController controller : mEditableControllers)
            controller.onSave();
    }
}
