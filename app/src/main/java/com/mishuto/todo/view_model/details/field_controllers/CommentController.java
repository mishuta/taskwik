package com.mishuto.todo.view_model.details.field_controllers;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view_model.common.AbstractActivity;

import static android.app.Activity.RESULT_OK;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_COMMENT;

/**
 * Контроллер DetailsFragmentControl для отображения и изменения атрибута задачи "Комментарии"
 *
 * Created by Michael Shishkin on 24.01.2017.
 */

public class CommentController extends AbstractEditableController
        implements View.OnClickListener, DialogHelper.DialogHelperListener
{
    private TextView mCommentView;      // Поле отображения последнего комментария
    private TextView mCommentsCount;    // Количество комментариев
    private Task mTask;                 // объект модели задачи

    public CommentController(View v, Task t) {
        mTask = t;
        mCommentView = v.findViewById(R.id.taskDetails_comment);
        mCommentsCount = v.findViewById(R.id.taskDetails_comment_count);
        mCommentView.setOnClickListener(this);
        mCommentsCount.setOnClickListener(this);
        updateWidgets();
        setPerformChangedViews(mCommentView, mCommentsCount);
        onPerformEvent(t.isStateInPerformed());
    }

    @Override
    public void updateWidgets() {
        if(mTask.getComments().getActual() != null) {
            mCommentView.setText(mTask.getComments().getActual().getComment());
            Integer count = mTask.getComments().getList().size() - 1;               // количество оставшихся комментариев кроме видимого
            mCommentsCount.setText(count > 0 ? "+ " + count.toString() : null );    // количество комментариев отображается, если их больше 1
        }
        else {
            mCommentView.setText(null);
            mCommentsCount.setText(null);
        }
    }

    // Клик на комментарий или число в карточке
    @Override
    public void onClick(View v) {
        DialogHelper.createDialog(new CommentDialog(), AbstractActivity.getCurrentActivity().getMainFragment(), this, toBundle(mTask.getComments()));
    }

    // Возвращение объекта Comments из диалога
    @Override
    public void onCloseDialog(int resultCode, Bundle resultObject) {
        if(resultCode == RESULT_OK) {                   // если в диалоге были изменения
            TaskEventsFacade.send(A_COMMENT, mTask);  // посылаем уведомление и апдейтим виджет
            updateWidgets();
        }
    }
}
