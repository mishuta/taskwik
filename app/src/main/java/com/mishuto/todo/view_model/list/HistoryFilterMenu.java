package com.mishuto.todo.view_model.list;

import android.annotation.SuppressLint;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.model.task_filters.HistoryFilters;
import com.mishuto.todo.model.task_filters.Selectivity;
import com.mishuto.todo.todo.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Меню, отображающее список последних выбранных фильтров
 * Created by Michael Shishkin on 22.05.2018.
 */
class HistoryFilterMenu implements PopupMenu.OnMenuItemClickListener {

    final static Map<FilterManager, Integer> ICONS = new HashMap<>();
    static {
        ICONS.put(FilterManager.EXECUTOR, R.drawable.ic_account_grey600_24dp);
        ICONS.put(FilterManager.CONTEXT, R.drawable.ic_coffee_outline_grey600_24dp);
        ICONS.put(FilterManager.PROJECT, R.drawable.ic_timetable_grey600_24dp);
        ICONS.put(FilterManager.DATE, R.drawable.ic_calendar_today_grey600_24dp);
        ICONS.put(FilterManager.SEARCH, R.drawable.ic_magnify_grey600_24dp);
    }

    private static final HistoryFilters HISTORY_FILTERS = HistoryFilters.getInstance();

    @SuppressLint("RestrictedApi")  // используется недокументированный класс MenuPopupHelper
    static void showMenu(View menuButton) {

        List<HistoryFilters.HistoryItem> history = HISTORY_FILTERS.getHistory();

        PopupMenu popupMenu = new PopupMenu(menuButton.getContext(), menuButton);
        popupMenu.setOnMenuItemClickListener(new HistoryFilterMenu());

        if(history.size() == 0) {
            MenuItem item = popupMenu.getMenu().add(R.string.lastFilter_noItems);
            item.setEnabled(false);
            popupMenu.show();
        }
        else {
            for (HistoryFilters.HistoryItem historyItem : history) {
                //noinspection unchecked
                MenuItem item = popupMenu.getMenu().add(
                        Menu.NONE,                      // groupId
                        history.indexOf(historyItem),   // itemId меню передаем как индекс в списке
                        Menu.NONE,                      // порядковый номер. Игнорируем, т.к. список уже отосортирован
                        ((Selectivity)historyItem.getFilterManager().filter()).toString(historyItem.getSelector()));  // текстовое представление селектора фильтра
                item.setIcon(ICONS.get(historyItem.getFilterManager()));
            }

            MenuPopupHelper menuHelper = new MenuPopupHelper(menuButton.getContext(), (MenuBuilder) popupMenu.getMenu(), menuButton);
            menuHelper.setForceShowIcon(true);
            menuHelper.show();
        }
    }

    // клик по меню: устанавливаем фильтр из меню. Он обновит тулбар списка задач.
    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        HistoryFilters.HistoryItem item = HISTORY_FILTERS.getHistory().get(menuItem.getItemId());
        Selectivity filter = (Selectivity) item.getFilterManager().filter();
        //noinspection unchecked
        filter.setSelector(item.getSelector());
        filter.setFilter(true);
        return true;
    }
}
