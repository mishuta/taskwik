package com.mishuto.todo.view_model.common.delay;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.LayoutWrapper;
import com.mishuto.todo.view.TimePickerWidget;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view.dialog.TabbedDialog;

import java.io.Serializable;

import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.view.dialog.DialogHelper.doOk;

/**
 * Диалог выбора даты/времени операции Отложить задачу
 * Created by Michael Shishkin on 18.12.2017.
 */

public class DelayCustomDialog extends DialogFragment implements View.OnClickListener, TimePicker.OnTimeChangedListener
{
    private DatePicker mDatePicker;                 // виджет даты
    private TimePickerWidget mTimePickerWidget;     // виджет времени
    private CheckBox mTimeSet;                      // чекбокс: выбрано ли время
    private ViewGroup mTimeTab;                     // вкладка времени

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //noinspection ConstantConditions
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);    // убрать заголовок в Android 5.1 и ниже

        View root = LayoutInflater.from(getActivity()).inflate(R.layout.dlg_delay_date_select, (ViewGroup)getView());
        TabLayout tabLayout = root.findViewById(R.id.tabLayout);
        mDatePicker = root.findViewById(R.id.datePicker);
        mTimeSet = root.findViewById(R.id.checkBox);
        mTimePickerWidget = new TimePickerWidget(root.findViewById(R.id.timePicker));
        mTimeTab = root.findViewById(R.id.timeTab);
        root.findViewById(R.id.buttonOk).setOnClickListener(this);
        root.findViewById(R.id.buttonCancel).setOnClickListener(this);


        new PickerSelectedListener(tabLayout);
        mTimePickerWidget.getTimePicker().setOnTimeChangedListener(this);

        return root;
    }

    // Обработка нажатия кнопок OK/CANCEL
  @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonOk) {
            TaskTime taskTime = new TaskTime();
            taskTime.setDate(mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth());   // сохраняем дату

            if(mTimeSet.isChecked())                                                                        // если время выбрано
                taskTime.setTimePart(mTimePickerWidget.getHour(), mTimePickerWidget.getMinute());           // сохраняем время

            doOk(this, toBundle((Serializable) taskTime));
        }
        else
            DialogHelper.doCancel(this);

        dismiss();
    }

    // любое событие изменение времени - включает чекбокс "учитывать время"
    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        mTimeSet.setChecked(true);
    }

    // класс отвечает за корректное переключение вкладок
    class PickerSelectedListener extends TabbedDialog.TabSelectedListener {
        private static final int DATE_SELECTION = 0;    // вкладка даты
        private static final int TIME_SELECTION = 1;    // вкладка времени

        @SuppressWarnings("ConstantConditions")
        PickerSelectedListener(TabLayout tabLayout) {
            super(tabLayout);
            // инициализация вкладок
            tabLayout.getTabAt(TIME_SELECTION).select();
            tabLayout.getTabAt(DATE_SELECTION).select();
        }

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            super.onTabSelected(tab);
            mDatePicker.setVisibility(tab.getPosition() == DATE_SELECTION ? View.VISIBLE : View.INVISIBLE);

            for(View view : LayoutWrapper.createLayoutIterator(mTimeTab))
                view.setVisibility(tab.getPosition() == TIME_SELECTION ? View.VISIBLE : View.INVISIBLE);
        }
    }
}
