package com.mishuto.todo.view_model.settings;

import android.view.View;
import android.widget.Switch;

import com.mishuto.todo.model.Settings;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.SwitchWidget;

/**
 * Секция Прочие диалога Settings
 * Created by Michael Shishkin on 19.07.2018.
 */
class Etc implements View.OnClickListener {

    private SwitchWidget mExecutorsClear;   // автоудаление исполнителей

    Etc(View root) {
        // автоудаление исполнителей
        mExecutorsClear = new SwitchWidget((Switch)root.findViewById(R.id.settings_autoClearSwitch), Settings.getEtc().executorsAutoClear);
        root.findViewById(R.id.settings_autoClearLabel1).setOnClickListener(this);
        root.findViewById(R.id.settings_autoClearLabel2).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mExecutorsClear.flip();
    }
}
