package com.mishuto.todo.view_model.common;

import android.content.res.Resources;
import android.support.design.widget.Snackbar;

import com.mishuto.todo.App;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;

import static com.mishuto.todo.common.TSystem.debug;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.TIMER;

/**
 * Общий обработчик событий модели TaskEventsFacade. Срабатывает для всех событий, в отличии от обработчиков в конкретных фрагментах, которые добавляют специфичное для себя поведение.
 * Для событий таймера и каскадного изменения атрибутов показывает предупреждающие снеки
 * см. так же описание класса TimerPublisher
 * Created by Michael Shishkin on 12.09.2017.
 */

public class TaskEventHandler implements TaskEventsFacade.TaskObserver {

    private static TaskEventHandler sInstance;
    private int mTaskCount;                                 // количество активированных задач на текущее время, выводимых в снеке
    private Snackbar mSnackBar;                             // объект выводимого снека

    public static void create() {
        if(sInstance == null)
            sInstance = new TaskEventHandler();
    }

    private TaskEventHandler() {
        TaskEventsFacade.getInstance().register(this);
    }

    //обработчик событий
    // т.к. события таймера приходят в разных потоках - обработка события должна быть синхронизирована
    @Override
    public synchronized void onTaskEvent(TaskEventsFacade.EventType type, final Task task, Object data) {

            if (AbstractActivity.getCurrentActivity() == null ||            // если активити не существует или она в бэкграунде
                    !AbstractActivity.getCurrentActivity().isInForeground()) {

                if (type == TIMER && task.getNearestEvent().isTimeSet())    //  то, если это таймер и установлена временная часть
                    showNotify(task);                                       //  отображаем уведомления
            }
            else                                                            // в противном случае выводим снек
                switch (type) {
                    case TIMER:                                             // таймер и изменение статуса предыдущей задачи могут вызвать активацию задач
                    case PREV_STATE:                                        // о чем предупреждается в снеке

                        if(!task.isStateActive())
                            break;

                        debug(this, type + " for " + task.toString());

                        if (mSnackBar == null || mSnackBar != Snack.getSnack().getSnackBar()) { // Если "наш" снек не отображается, считаем событие первым в цепочке
                            mTaskCount = 1;
                            showSnack(resources().getString(R.string.task_activated, task.toString()));
                        }
                        else {                                                                  // если отображается - увеличиваем счетчик
                            mTaskCount++;
                            showSnack(resources().getQuantityString(R.plurals.task_activated_plural, mTaskCount, mTaskCount));
                        }
                        break;

                    case A_PREV_DISCARDS_TIME:
                        showSnack(resources().getString(R.string.task_warn_dateReset));
                        break;

                    case A_TIME_DISCARDS_PREV:
                        showSnack(resources().getString(R.string.task_warn_prevReset, data.toString()));
                        break;
                }
    }

    // выводим снек в потоке GUI
    private void showSnack(final String s) {
        AbstractActivity.getCurrentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSnackBar = Snack.show(AbstractActivity.getCurrentActivity().getMainFragment().getView(), s);
            }
        });
    }

    // отображаем уведомление
    private void showNotify(Task t) {
        if(Settings.getNotificationSettings().isOn.get()) {
            NotificationController controller = NotificationController.getInstance();

            controller.addLine(t);
            int n = controller.getNumLines();
            controller.setNotificationTitle(resources().getQuantityString(R.plurals.task_activated_plural, n, n));
            controller.show();
        }
    }

    private Resources resources() {
        return App.getAppContext().getResources();
    }
}
