package com.mishuto.todo.view_model.common;

import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.events.ObservableSubject;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.model.task_filters.TaskFilter;

/**
 * Фасад для оповещения об интерфейсных событиях
 * Отделен от TaskEventsFacade, чтобы не смешивать события модели и события, которые не затрагивают модель
 * Created by Michael Shishkin on 29.11.2018.
 */
public class InterfaceEventsFacade extends ObservableSubject<InterfaceEventsFacade.EventObserver> {

    private static InterfaceEventsFacade sInstance = new InterfaceEventsFacade();

    public interface EventObserver extends ObservableSubject.Observer {
        void onInterfaceEvent(Event event, Object param);
    }

    public enum Event {
        // операция над задачей
        CHANGE_TASK_REQUEST,    // запрос на перегрузку задачи в карточке
        CHANGE_TASK,            // сменить задачу в карточке
        CLOSE_CARD,             // закрыть карточку

        OPEN_CARD,              // открыть карточку с задачей

        // события
        FILTER_CHANGED,         // фильтр изменен
        LIST_LOADED             // список задач загружен
    }

    // событие, связанное с операцией
    public enum SubEvent {
        TASK_CONFIRM,   // сохранить задачу
        TASK_CANCEL,    // отменить
        TASK_DELETE,    // удалить

        TASK_UNCHANGED, // Задача не была изменена (независимо от того выбран ОК или CANCEL)
        TASK_WAS_SAVED  // Было промежуточное сохранение изменений без последующих изменений
    }

    // структура сообщения, передаваемого в операции
    public static class CardMsg {
        public SubEvent subEvent;   // событие, связанное с операцией
        public Task task;           // задача, над которой выполняется операция
        public Task taskToChange;   // задача, которая должна загрузиться в карточку. Для операций CHANGE_TASK_REQUEST и CLOSE_CARD

        public CardMsg(SubEvent subEvent, Task task, Task taskToChange) {
            this.subEvent = subEvent;
            this.task = task;
            this.taskToChange = taskToChange;
        }
    }

    // гененрация событий изменения фильтров
    private InterfaceEventsFacade() {
        FilterManager.setChangeFilterListener(new TaskFilter.OnFilterChangeListener() {
            @Override
            public void onFilterChanged(TaskFilter filter) {
                notifyAllObservers(Event.FILTER_CHANGED, filter);
            }
        });
    }

    public static InterfaceEventsFacade getInstance() {
        return sInstance;
    }

    public static void send(Event event, Object param) {
        sInstance.notifyAllObservers(event, param);
    }

    @Override
    protected void notifyObserver(EventObserver observer, Object[] parameters) {
        Event event = (Event) parameters[0];
        Object param = parameters[1];

        TSystem.debug(this, "Notify of " + observer.getClass().getSimpleName() + ". Event: " + event + ". Param: " + param);

        observer.onInterfaceEvent(event, param);
    }
}
