package com.mishuto.todo.view_model.details.field_controllers;

import android.view.View;

import com.mishuto.todo.view_model.details.TaskCardController;

import static com.mishuto.todo.view.Constants.OPAQUE_FULL;
import static com.mishuto.todo.view.Constants.OPAQUE_MID;

/**
 * Определяет дефолтное поведение для интерфейса EditableController
 * Created by Michael Shishkin on 14.03.2018.
 */

abstract class AbstractEditableController implements TaskCardController.EditableController {

    private View[] mPerformViews; // список виджетов, которые должны изменяться во время смены статуса задач

    void setPerformChangedViews(View ... performChangedViews) {
        mPerformViews = performChangedViews;
    }

    //команда обновления виджетов. Должна явно быть переопределена в контроллере
    @Override
    abstract public void updateWidgets();

    //событие сохранения данных перед закрытием или уходом в фон. По умолчанию ничего не делает
    @Override
    public void onSave() {
    }

    //событие закрытия карточки. По умолчанию ничего не делает
    @Override
    public void onClose() {
    }

    //событие изменение статуса.
    @Override
    public void onPerformEvent(boolean isPerformed) {
        for (View view : mPerformViews) {
            view.setEnabled(!isPerformed);
            view.setAlpha(isPerformed ? OPAQUE_MID : OPAQUE_FULL );
        }

    }
}
