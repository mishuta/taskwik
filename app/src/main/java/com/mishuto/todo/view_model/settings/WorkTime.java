package com.mishuto.todo.view_model.settings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.mishuto.todo.common.Accessor;
import com.mishuto.todo.common.Time;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Constants;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.SwitchWidget;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view.dialog.TimeDialog;

import static android.app.Activity.RESULT_OK;
import static com.mishuto.todo.common.BundleWrapper.getSerializable;
import static com.mishuto.todo.common.BundleWrapper.toBundle;

/**
 * Created by Michael Shishkin on 16.07.2018.
 */
class WorkTime {
    private View mRoot;
    private TimeController mBegin;
    private TimeController mEnd;
    private SwitchWidget mWorkDay;
    private Fragment mParentDialog;

    private static Settings.WorkTime WORK_TIME = Settings.getWorkTime();

    WorkTime(View root, Fragment parentDialog) {
        mRoot = root;
        mParentDialog = parentDialog;

        new SwitchWidget((Switch) root.findViewById(R.id.settings_workTime),
                new Accessor<Boolean>() {
                    @Override
                    public Boolean get() {
                        return WORK_TIME.isOn.get();
                    }

                    @Override
                    public void set(Boolean value) {
                        WORK_TIME.isOn.set(value);
                        onUpdate();
                    }
                });

        mWorkDay = new SwitchWidget((Switch) root.findViewById(R.id.settings_workDayOnlySwitch), WORK_TIME.onlyWorkDays);

        // контроллеры  рабочего времени
        mBegin = new BeginTimeController(R.id.settings_begin_label, R.id.settings_begin);
        mEnd =   new EndTimeController(R.id.settings_end_label, R.id.settings_end);
        onUpdate();
    }

    // отрисовка состояния зависимых виджетов
    private void onUpdate() {
        boolean isWorkTime = WORK_TIME.isOn.get();
        mBegin.setEnabled(isWorkTime);
        mEnd.setEnabled(isWorkTime);
        mWorkDay.getSwitch().setEnabled(isWorkTime);
    }

    // Контроллеры виджетов начала/окончания рабочего дня
    private abstract class TimeController implements View.OnClickListener {
        View mLabel;
        TextView mValue;

        abstract Time getSettingsTime();
        abstract void setSettingsTime(Time time);

        TimeController(int labelRes, int valueRes) {
            mLabel = mRoot.findViewById(labelRes);
            mValue = mRoot.findViewById(valueRes);

            mLabel.setOnClickListener(this);
            mValue.setOnClickListener(this);
        }

        void setEnabled(boolean enabled) {
            mLabel.setEnabled(enabled);
            mValue.setEnabled(enabled);
            mValue.setAlpha(enabled ? Constants.OPAQUE_FULL : Constants.OPAQUE_MID);
        }

        @Override
        public void onClick(View v) {
            DialogHelper.createDialog(new TimeDialog(), mParentDialog, new DialogHelper.DialogHelperListener() {
                @Override
                public void onCloseDialog(int resultCode, Bundle resultObject) {
                    if(resultCode == RESULT_OK) {
                        Time time = (Time) getSerializable(resultObject);
                        setSettingsTime(time);
                        if(!getSettingsTime().equals(time))
                            Snack.show(mRoot, R.string.errDuration);
                        else
                            mValue.setText(time.toString());
                    }

                }
            }, toBundle(getSettingsTime()));
        }
    }

    private class BeginTimeController extends TimeController {

        BeginTimeController(int label, int value) {
            super(label, value);
            mValue.setText(WORK_TIME.getStartWork().toString());
        }

        @Override
        Time getSettingsTime() {
            return WORK_TIME.getStartWork();
        }

        @Override
        void setSettingsTime(Time time) {
            WORK_TIME.setBeginningWork(time);
        }
    }

    private class EndTimeController extends TimeController {

        EndTimeController(int label, int value) {
            super(label, value);
            mValue.setText(WORK_TIME.getEndWork().toString());
        }

        @Override
        Time getSettingsTime() {
            return WORK_TIME.getEndWork();
        }

        @Override
        void setSettingsTime(Time time) {
            WORK_TIME.setEndWork(time);
        }
    }
}
