package com.mishuto.todo.view_model.details.field_controllers.recurrence_dialog;

import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.text.InputFilter;
import android.view.View;
import android.widget.CompoundButton;

import com.mishuto.todo.common.DateFormatWrapper;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.Time;
import com.mishuto.todo.model.task_fields.recurrence.AbstractSegment;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.edit.TextInputWidget;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import static com.mishuto.todo.common.TCalendar.now;

/**
 * Базовый класс всех страниц Recurrence, являющихся вложенными фрагментами ContainerDialog
 *
 * Created by Michael Shishkin on 28.11.2016.
 */

abstract class BasePageFragment<T extends AbstractSegment> extends Fragment {

    // Интерфейс служит для вызова события обновления страницы в родительском фрагменте ContainerDialog
    interface OnPageUpdateListener {
        void onPageUpdated(boolean changed);    //если changed = true, значит обновление в результате изменения
    }

    T mSegment;                             // Сегмент объекта Recurrence. Тип (DayOfWeek, Week, ...) определяется потомком
    private OnPageUpdateListener mListener; // Слушатель изменений на странице. Передается из вызывающего класса в конструкторе
    TimeWidgets mTimeWidgets;               // Группа виджетов для ввода времени

    void setParams(T segment, OnPageUpdateListener listener) {
         mSegment = segment;
         mListener = listener;
     }

    // Колбэк вызывается из метода OnCreateView потомка для инициализации общих для всех страниц виджетов
    protected void onCreateView(View root) {
        mTimeWidgets = new TimeWidgets(root);   // создаем виджеты установки Времени
    }

    // вызывается при обновлении страницы и вызывает метод родительского фрагмента для обновления результата
    protected void onUpdate(boolean changed) {
        mListener.onPageUpdated(changed);
    }

    /**
     * Виджеты для установки времени повторов. Присутствуют на каждой странице Recurrence:
     * EditText для отображения/ввода и свитч для включения/отключения выбора времени.
     */
    @SuppressWarnings("WeakerAccess")
    class TimeWidgets implements CompoundButton.OnCheckedChangeListener, TextInputWidget.OnChangeTextListener {

        private Time mTime = mSegment.getTime();        // Объект, описывающий Время Recurrence
        private SwitchCompat mSwitch;                   // Выключатель использования времени
        private TextInputWidget mTimeInput;             // Виджет для ввода времени

        final private String mHintString = getNoon();   // Строка выводимой ошибки-подсказки с шаблоном времени

        TimeWidgets(View parentView){
            // связывание
            TextInputLayout inputLayout = parentView.findViewById(R.id.rec_time_text_layout);
            mSwitch = parentView.findViewById(R.id.recurrence_time_switch);

            // инициация виджетов
            mTimeInput = new TextInputWidget(inputLayout, mTime.toString(), this);
            mSwitch.setChecked(mSegment.isTimeSet());
            mSwitch.setOnCheckedChangeListener(this);

            setLenLimit();          // Фиксация максимальной длины строки времени, которую можно ввести
            onUpdateEditStatus();   // Если выключатель выключен, то EditText запрещается

        }

        @Override
        public void onTextChanged(String text) {

            try {                                           // Если время введено корректно
                mSegment.setTime(parseTime(text));          // Оно сохраняется
                mTimeInput.hideError();                     // Ошибка скрывается
                onUpdate(true);                             // Вызывается событие изменения

            } catch (ParseException e){
                mTimeInput.showError(mHintString);          // Иначе появляется подсказка с шаблоном времени
            }
        }

        // событие изменение выключателя времени
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mSegment.setTimeFlag(isChecked);
            onUpdateEditStatus();
            onUpdate(true);
        }

        // если выключатель выключен, то время установить нельзя
        void onUpdateEditStatus() {
            mTimeInput.getEditText().setEnabled(mSwitch.isChecked());
        }

        // строка времени 12:00 в текущей локали
        private String getNoon() {
            TCalendar calendar = now();
            calendar.setTimePart(12,0);
            return calendar.getFormattedTime();
        }

        // Парсинг введенного времени и преобразование его в GregorianCalendar
        private Time parseTime(String time) throws ParseException {
            DateFormat df = DateFormatWrapper.getTimeFormat();
            df.setLenient(false);
            Date date = df.parse(time);
            TCalendar calendar = new TCalendar();
            calendar.setTime(date);
            return new Time(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
        }

        // Фиксация максимальной длины строки времени, которую можно ввести
        private void setLenLimit() {
            int len = mHintString.length(); // максимальная длина строки = длина (12:00) в текущей локали
            InputFilter[] filters = new InputFilter[1];
            filters[0] = new InputFilter.LengthFilter(len);
            mTimeInput.getEditText().setFilters(filters);
        }
    }
}
