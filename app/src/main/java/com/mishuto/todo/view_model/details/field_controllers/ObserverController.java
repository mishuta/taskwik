package com.mishuto.todo.view_model.details.field_controllers;

import android.view.View;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.events.TaskEventsFacade;

/**
 * Расширяет GenericController возможностью наблюдения за определенными событиями задачи
 * Created by Michael Shishkin on 06.02.2018.
 */

abstract class ObserverController extends GenericController implements TaskEventsFacade.TaskObserver {

    private TaskEventsFacade.EventType[] mEventTypes;              // список событий, которые мониторятся

    ObserverController(View root, Task task, int label, int value, int clearBtn) {
        super(root, task, label, value, clearBtn);
        TaskEventsFacade.getInstance().register(this);
    }

    // установка списка событий
    void setEventFilter(TaskEventsFacade.EventType ...eventTypes) {
        mEventTypes = eventTypes;
    }

    @Override
    public void onTaskEvent(TaskEventsFacade.EventType type, Task task, Object extendedData) {
        if(task == mTask && type.in(mEventTypes))
            updateWidgets();
    }

    @Override
    public void onClose() {
        TaskEventsFacade.getInstance().unregister(this);
    }
}