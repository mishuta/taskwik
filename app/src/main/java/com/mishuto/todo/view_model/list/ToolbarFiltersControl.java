package com.mishuto.todo.view_model.list;

import android.view.View;
import android.widget.ImageButton;

import com.mishuto.todo.model.task_filters.Exclusiveness;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.model.task_filters.HistoryFilters;
import com.mishuto.todo.model.task_filters.Selectivity;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.CheckedView;
import com.mishuto.todo.view.ImageCheckedButton;
import com.mishuto.todo.view.edit.AbstractCompoundEditFreezeView;
import com.mishuto.todo.view.edit.EditFreezeView;
import com.mishuto.todo.view.edit.EditView;
import com.mishuto.todo.view_model.common.InterfaceEventsFacade;
import com.mishuto.todo.view_model.list.dialogs.tag_filters.FilterDialog;

import static com.mishuto.todo.model.task_filters.FilterManager.search;

/**
 * Управление фильтрами и поиском в тулбаре списка задач
 * Created by Michael Shishkin on 18.11.2017.
 */

class ToolbarFiltersControl implements InterfaceEventsFacade.EventObserver {
    private EditFreezeView mFilterContent;      // виджет содержимого фильтра списка задач
    private Exclusiveness mFilter;            // значение фильтра. Передается в диалог-фильтр и возвращается из диалога
    private FilterController mFilterController; // контроллер, управляющий фильтрами на тулбаре
    private SearchController mSearchController; // контроллер, управляющий поиском

    // интерфейс для классов *Controller
    private interface Controller {
        void turn(Boolean on);  // включить/выключить контроллер
    }

    ToolbarFiltersControl(View root) {
        mFilterContent = root.findViewById(R.id.toolbar_filter_value);
        mFilter = Exclusiveness.getExclusiveFilter();

        // кнопка выбора из списка последних фильтров
        final ImageButton historyButton = root.findViewById(R.id.history_button);
        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HistoryFilterMenu.showMenu(historyButton);
            }
        });

        // подписка на события фильтров
       InterfaceEventsFacade.getInstance().register(this);

        mFilterController = new FilterController(root);
        mSearchController = new SearchController(root);
        setAppBarWidgets();  // если фильтр установлен при запуске активности, то отображается контроллер фильтров
    }

    void onClose() {
        InterfaceEventsFacade.getInstance().unregister(this);
    }

    // отображение/очистка фильтра в AppBar
    private void setAppBarWidgets() {
        if(mFilter == null || !mFilter.isFilterSet()) {
            mFilterContent.setText(R.string.app_name);
            mFilterController.turn(false);
            mSearchController.turn(false);
        }
        else {
            mFilterController.turn(mFilter != search());
            mSearchController.turn(mFilter == search());
        }
    }

    // событие изменения фильтров
    @Override
    public void onInterfaceEvent(InterfaceEventsFacade.Event event, Object param) {

        if(event == InterfaceEventsFacade.Event.FILTER_CHANGED && param instanceof Exclusiveness) {
            Exclusiveness filter = (Exclusiveness) param;
            // реагируем только на событие установки, либо на сброс всех фильтров. Проверка  filter == mFilter нужна, чтобы многократно не вызывать getExclusiveFilter()
            if (filter.isFilterSet() || filter == mFilter && Exclusiveness.getExclusiveFilter() == null) {
                mFilter = filter;
                if (filter instanceof Selectivity)   // добавляем измененный фильтр в список последних выбранных
                    HistoryFilters.getInstance().pushFilter((Selectivity<?>) filter);

                setAppBarWidgets();                     // toolbar обновляется в любом случае, т.к. фильтр мог быть не выбран в диалоге
            }
        }
    }

    // класс объекта, управляющего работой фильтров
    private class FilterController implements
            Controller,
            EditFreezeView.OnClickListener,
            ImageCheckedButton.OnChangeStateListener
    {
        private ImageCheckedButton mFilterButton;   // кнопка управления фильтрами

        FilterController(View root) {
            mFilterButton = root.findViewById(R.id.toolbar_filter_btn);
            mFilterButton.setOnChangeListener(this);
            mFilterContent.setOnClickListener(this);
        }

        @Override
        public void turn(Boolean on) {
            mFilterButton.setChecked(on);

            if(on)
                mFilterContent.setText(mFilter.toString());
        }

        // обработка нажатия на контент фильтра
        @Override
        public void onClick(AbstractCompoundEditFreezeView v) {
            openFiltersDialog();
        }

        // обработка клика по кнопке фильтра
        @Override
        public void onChangeState(CheckedView button, boolean checked) {
            if(checked) {
                button.setChecked(false);   // при нажатии кнопки фильтра, возвращаем ее в ненажатое (консистентное) состояние, чтобы при отсутствии изменений в диалоге
                openFiltersDialog();        // и возвращении из него по кнопке back, кнопка фильтра осталавалась в том же состоянии что и была
            }
            else
                mFilter.setFilter(false);
        }

        // метод, создающий диалог с фильтрами
        private void openFiltersDialog() {
           FilterDialog.createDialog();
        }
    }

    // класс, управляющий работой поиска на тулбаре
    private class SearchController implements
            Controller,
            EditView.OnChangeListener,
            ImageCheckedButton.OnChangeStateListener
    {
        private ImageCheckedButton mSearchButton;   // кнопка поиска
        private Boolean isOn;                       // статус контроллера

        SearchController(View root) {
            mSearchButton = root.findViewById(R.id.toolbar_search_btn);
            mSearchButton.setOnChangeListener(this);
            mFilterContent.getEditView().setNormalUnderlineColor(R.color.textColorPrimary); // задаем белый цвет для подчеркивания EditView поиска
            mFilterContent.getEditView().setListener(this);                                 // задачем обработчик EditView поиска
        }

        @Override
        public void turn(Boolean on) {
            if(on == isOn)  // предварительная проверка статуса, чтобы метод не вызывался постоянно во время ввода запроса
                return;

            isOn = on;

            mFilterContent.setAutoChangeMode(on);
            mSearchButton.setChecked(on);

            if(on) {
                mFilterContent.setText(search().getSelector());
                mFilterContent.setEditMode();
            }
            else
                mFilterContent.setViewMode();
        }

        // обработка ввода поисковой строки
        @Override
        public boolean onTextValidate(EditView view, String text) {
            if(mFilterContent.isEditMode()) {  // Когда mFilterContent во ViewMode, метод вызываться вообще не должен. Но вызывается при пересоздании активности после уничтожения
                 search().setSelector(text);
            }
            return true;
        }

        // обработчик нажатия кнопки
        @Override
        public void onChangeState(CheckedView button, boolean checked) {
            FilterManager.search().setFilter(checked);
        }
    }
}
