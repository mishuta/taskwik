package com.mishuto.todo.view_model.details.field_controllers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.TaskSearcher;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view.recycler.FilteredListView;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import java.util.List;

import static com.mishuto.todo.common.BundleWrapper.getLong;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.common.TextUtils.empty;
import static com.mishuto.todo.view.dialog.DialogHelper.inParameter;

/**
 * Диалог, отображающий список задач для выбора с возможностью контекстного поиска
 * Created by Michael Shishkin on 25.12.2016.
 */

public class PrevTaskDialog extends DialogFragment implements
        RecyclerViewWidget.EventsListener<Task>,
        FilteredListView.FilterListener<Task>{

    SearchView mSearchView;             // Виджет строка поиска
    FilteredListView<Task> mTasksView;  // Виджет SearchView+RecyclerView
    List<Task> mTasksList;              // Копия списка задач
    Task mTask;                         // текущая задача


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // инициализация виджетов
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dlg_task_search, (ViewGroup)getView());

        Long currentTaskId = getLong(inParameter(this));    //запрашиваем ID текущей задачи
        mTask = TaskList.getById(currentTaskId);
        mSearchView = dialogView.findViewById(R.id.prev_searchView);
        mTasksView = new FilteredListView<>(
                (RecyclerView)dialogView.findViewById(R.id.tasks_recyclerView),
                R.layout.dlg_task_search_item,
                getPrefilteredList(),
                this,
                mSearchView,
                this);

        // подготовка и отображение диалога
        return new AlertDialog.Builder(getActivity())
                .setView(dialogView)
                .setTitle(R.string.taskSelectorDialog_title)
                .create();
    }


    // событе связвания задачи со строкой списка
    @Override
    public void onBind(View root, Task task, RecyclerViewWidget<Task>.ViewHolder viewHolder) {
        TextView name = root.findViewById(R.id.search_taskName);
        name.setText(task.toString());
    }

    // нажатие строки списка
    @Override
    public void onClick(View view, RecyclerViewWidget<Task>.ViewHolder viewHolder) {
        if(mTask.isSucceeding(viewHolder.getItem()))        // если выбранная задача является последователем текущей,
            Snack.show(view, R.string.succeeding_task);     // просто выводим снек
        else {                                              // иначе сохраняем и закрываем диалог
            DialogHelper.doOk(this, toBundle(viewHolder.getItem().getRecordID()));
            getDialog().dismiss();
        }
    }

    // Формирование первоначального списка задач
    List<Task> getPrefilteredList() {
        mTasksList = TaskList.getTasks();    // получаем копию списка задач
        mTasksList.remove(mTask);                   // исключаем текущую
        return mTasksList;
    }

    /**
     * Колбэк для фильтрации списка по подстроке
     * @param constraint строка, приходящая из SearchView
     * @return отфильтрованный список
     */
    @Override
    public List<Task> getFilteredResults(String constraint) {
        if (empty(constraint))
            throw new AssertionError();

        return TaskSearcher.doSearch(mTasksList, constraint, TaskSearcher.MatchLevel.TAGS_CONTENTS);
    }
}
