package com.mishuto.todo.view_model.details.field_controllers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.mishuto.todo.common.ResourceConstants;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.TaskSearcher;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.model.task_fields.Title;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.edit.AbstractCompoundEditFreezeView;
import com.mishuto.todo.view.edit.TIAutocompleteFreezeView;
import com.mishuto.todo.view.edit.TextInputWidget;
import com.mishuto.todo.view_model.common.InterfaceEventsFacade;

import java.util.ArrayList;
import java.util.List;

import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_TITLE;

/**
 * Контроллер названия задачи
 * Created by Michael Shishkin on 19.01.2017.
 */

public class TitleController extends AbstractEditableController
        implements TextInputWidget.OnChangeTextListener,
        AbstractCompoundEditFreezeView.AutoChangeModeListener,
        AdapterView.OnItemClickListener
{
    private TIAutocompleteFreezeView mFreezeView;       // Компаунд-виджет для хранения названия задачи
    private TextInputWidget mInputWidget;               // Виджет редактирования названия, часть mFreezeView
    private Title mTitle;                               // Объект модели Название задачи
    private Task mTask;
    private AutoCompleteAdapter mAutoCompleteAdapter;   // адаптер автозавершения

    private final static int THRESHOLD = 5;             // минимальное количество букв для поиска по вхождению

    public TitleController(View view, Task t) {
        mTitle = t.getTitle();
        mTask = t;

        mAutoCompleteAdapter = new AutoCompleteAdapter(view.getContext());

        mFreezeView = view.findViewById(R.id.taskDetails_titleView);
        mFreezeView.setAutoChangeModeListener(this);
        mFreezeView.getEditTextView().setAdapter(mAutoCompleteAdapter);
        mFreezeView.getEditTextView().setOnItemClickListener(this);

        mInputWidget = mFreezeView.getTextInputWidget(this);
        mInputWidget.setHint(R.string.taskDetails_hint_title);

        updateWidgets();
        setPerformChangedViews(mFreezeView);
    }

    @Override
    public void updateWidgets() {
        mFreezeView.setText(mTitle.getTaskName());
    }

    @Override
    public void onSave() {
        if(mFreezeView.isEditMode()) {
            mFreezeView.setViewMode();
            mTitle.setTaskName(mFreezeView.getText());
        }
    }

    // Обработка обытия изменения текста в виджете
    @Override
    public void onTextChanged(String text) {
        int err = mTitle.validate(text);

        if(err!= ResourceConstants.Validity.OK)
            mInputWidget.showError(err);
        else
            mInputWidget.hideError();
    }

    // Обработка события завершения редактирования
    @Override
    public void onAutoChangeMode(AbstractCompoundEditFreezeView view, boolean toViewMode) {
        if(toViewMode && !mTitle.setTaskName(view.getText()))   // Попытка сохранить название в модели
            view.setEditMode();                                 // Если неуспешно, то возврат в режим редактирования
        else
            TaskEventsFacade.send(A_TITLE, mTask);
    }

    // Обработка события выбора задачи из автокомплита
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        InterfaceEventsFacade.send(
                InterfaceEventsFacade.Event.CHANGE_TASK_REQUEST,
                new InterfaceEventsFacade.CardMsg(null, null, mAutoCompleteAdapter.getItem(position)));
    }
    // адаптер для создания фильтрованного и сортированного списка задач с похожим названием
    private class AutoCompleteAdapter extends ArrayAdapter<Task> {

        AutoCompleteAdapter(Context context) {
            super(context, R.layout.item_dropdown);
        }

        @Override
        @NonNull
        public android.widget.Filter getFilter() {
            return new FilterImp();
        }

        private class FilterImp extends android.widget.Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Task> taskList = new ArrayList<>();
                FilterResults results = new FilterResults();

                if(constraint != null && constraint.length() > 0)
                    taskList = TaskSearcher.doSearch(
                            TaskList.getTasks(),
                            constraint.toString(),
                            constraint.length() < THRESHOLD ?  TaskSearcher.MatchLevel.TITLE_MATCHES : TaskSearcher.MatchLevel.COMMENT_CONTENTS);

                taskList.remove(mTask);
                results.values = taskList;
                results.count = taskList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                clear();
                if (results.count > 0)
                    //noinspection unchecked
                    addAll((List<Task>)results.values);
            }
        }
    }
}
