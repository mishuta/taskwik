package com.mishuto.todo.view_model.details.field_controllers;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.model.task_fields.recurrence.Recurrence;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view_model.details.field_controllers.recurrence_dialog.ContainerDialog;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.mishuto.todo.common.BundleWrapper.getDBObject;
import static com.mishuto.todo.common.BundleWrapper.toBundle;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_RECURRENCE;

/**
 * Контроллер DetailsFragmentControl для отображения и изменения атрибута задачи "Повтор"
 * Created by Michael Shishkin on 10.11.2016.
 */

public class RecurrenceController extends GenericController {

    private Recurrence mRecurrence;     // объект Recurrence
    private ImageView mWarnView;        // значок предупреждения о несрабатывании повтора

    public RecurrenceController(View view, Task task) {
        super(view, task, R.id.taskDetails_label_recurrence, R.id.taskDetails_textView_recurrence, R.id.taskDetails_clearRepeat);

        mRecurrence = task.getRecurrence();
        mWarnView = setClickable (R.id.taskDetails_image_recWarn);

        updateWidgets();
    }

    @Override
    void onAction() {
        createDialog(new ContainerDialog(), toBundle(mRecurrence));
    }

    @Override
    void onClear() {
        mRecurrence.set(false);
        TaskEventsFacade.send(A_RECURRENCE, mTask);
        updateWidgets();
    }

    @Override
    public void updateWidgets() {
        setState(mRecurrence.isSet() ? mRecurrence.getCurrentSegment().getShortValue() : null);
        mWarnView.setVisibility(mRecurrence.isSet() && !mTask.isStateInPerformed() ? VISIBLE : GONE);
    }

    // события обработки клика по виджетам
    @Override
    public void onClick(View v) {
        super.onClick(v);

        if(v.getId() == mWarnView.getId())                          // Если нажата иконка с предупреждением
            Snack.show(v, R.string.taskDetails_warn_recurrence);    // выводим предупреждение
    }

    @Override
    public void onCloseDialog(int resultCode, Bundle resultObject) {
        if(resultCode == RESULT_OK) {
            mRecurrence = (Recurrence) getDBObject(resultObject);
            TaskEventsFacade.send(A_RECURRENCE, mTask);
            updateWidgets();
        }
    }

    @Override
    public void onPerformEvent(boolean isPerformed) {
        if(mRecurrence != null) // если вызывается не из конструктора
            updateWidgets();    // обновляем отображение значка
    }
}
