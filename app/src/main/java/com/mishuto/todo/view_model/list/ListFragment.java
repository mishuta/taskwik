package com.mishuto.todo.view_model.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.model.task_filters.Choice;
import com.mishuto.todo.model.task_filters.Exclusiveness;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.common.InterfaceEventsFacade;
import com.mishuto.todo.view_model.details.DetailsActivity;

import java.util.Iterator;

import static com.mishuto.todo.common.TSystem.debug;
import static com.mishuto.todo.view.dialog.DialogHelper.onCloseDialog;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.Event;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.Event.OPEN_CARD;

/**
 * Класс для отображения списка задач в фрагменте
 * Фрагмент порождается в активности MainActivity
 * Created by Michael Shishkin on 24.08.2016.
 */
public class ListFragment extends Fragment implements
        FloatingActionButton.OnClickListener,
        TaskEventsFacade.TaskObserver,
        InterfaceEventsFacade.EventObserver
{
    private ListController mListController;         // контроллер списка задач
    private EmptyMsgController mEmptyMsgController; // контроллер пустого списка

    // Отображение фрагмента ListFragment
    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_list, container, false);

        final FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(this);       // устанавливаем обработчик нажатия для FAB

        mListController = new ListController(view);
        mEmptyMsgController = new EmptyMsgController(view);
        new BannerController(view);


        TaskEventsFacade.getInstance().register(this);      // подписываемся на события модели после создания рагмента
        InterfaceEventsFacade.getInstance().register(this); // подписываемся на события интерфеса

        return view;
    }

    // обработка события потери фокуса
    // если есть задачи для удаления - они удаляются немедленно, чтобы не вызывать коллизий в новом окне
    // необходимо обрабатывать событие потери фокуса т.к. открытие диалога с фильтрами или контекстного меню не вызывают onPause
    void onFocusLost() {
        debug(this, "ON_FOCUS_LOST");
        removeMarkedTasks();
    }

    // остановка фрагмента приводит к удалению задач, помеченных как удаленные
    // необходимо обрабатывать onPause, т.к. для Android 8 в случае открытия карточки задачи, onFocusLost вызывается позднее чем onCreate карточки
    @Override
    public void onPause() {
        super.onPause();
        debug(this, "ON_PAUSE");
        removeMarkedTasks();
    }

    // немедленное удаление задач, помеченных как удаленные
    private void removeMarkedTasks() {
        if(FilterManager.unhidden().anyHiddenTasks()) {
            Iterator<Task> iterator = FilterManager.unhidden().getHiddenTasks().iterator();
            while (iterator.hasNext()) {
                TaskList.remove(iterator.next());
                iterator.remove();
            }
        }
    }

    @Override
    public void onDestroyView() {
        TaskEventsFacade.getInstance().unregister(this);   // отписываемся от событий модели перед уничтожением фрагмента
        InterfaceEventsFacade.getInstance().unregister(this);
        super.onDestroyView();
    }

    // обработчик нажатия FAB "Добавить"
    @Override
    public void onClick(View v) {
        Task task = TaskList.addNew();  // Добавляем задачу в список задач

        // Если установлен фильтр, инициируем им новую задачу
        Exclusiveness filter = Exclusiveness.getExclusiveFilter();
        if(filter instanceof Choice)
            ((Choice) filter).putInTask(task);

        openTaskCard(task, true);                                                           // Передаем на редактирование
    }

    // Колбэк, вызваемый дочерним фрагментом, вызвавшем setTargetFragment после его завершения.
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        onCloseDialog(requestCode, resultCode, data);
    }

    //обработка событий модели
    @Override
    public void onTaskEvent(final TaskEventsFacade.EventType type, final Task task, final Object data) {
        //noinspection ConstantConditions
        getActivity().runOnUiThread(() -> {
            mListController.onTaskEvent(type, task, data);
            mEmptyMsgController.onTaskEvent(type, task, data);
        });
    }

    // обработка событий интерфейса (от фрагммента карточки)
    @Override
    public void onInterfaceEvent(Event event, Object param) {

        if(event == OPEN_CARD)
            openTaskCard((Task)param, false);
        else {
            mListController.onInterfaceEvent(event, param);
            mEmptyMsgController.onInterfaceEvent(event, param);
        }
    }

    // Передача управления активности DetailsActivity для отображения карточки задачи
    void openTaskCard(Task task, boolean isNewTask){
        mListController.isNewTaskInCard.set(isNewTask);
        Intent intent = DetailsActivity.getIntent(task.getRecordID());
        startActivity(intent);
    }
}
