package com.mishuto.todo.view_model.list;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.mishuto.todo.App;
import com.mishuto.todo.common.BundleWrapper;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.task_filters.FilterManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.dialog.BaseFullScreenDialog;
import com.mishuto.todo.view_model.common.AbstractActivity;
import com.mishuto.todo.view_model.common.HelpDialog;
import com.mishuto.todo.view_model.common.InterfaceEventsFacade;
import com.mishuto.todo.view_model.common.NotificationController;
import com.mishuto.todo.view_model.details.DetailsActivity;
import com.mishuto.todo.view_model.settings.SettingsDialog;

/**
 * Основная активность
 * В вертикальной орипнтации отвечает за отображение фрагмента со списком задач.
 * Так же управляет options menu, создает ToolbarFiltersControl
 * Класс MainActivity определяется основным в манифесте
 *
 * Created by mike on 24.08.2016.
 */
public class MainActivity extends AbstractActivity
{
    private ToolbarFiltersControl mToolbar;                 // Объект для управления фильтами в тулбаре

    // команды в параметрах вызова активности
    private final static int OPEN_LIST = 1;
    private final static int OPEN_CARD = 2;
    private final static String IN_PARAM = App.createTagName("InParam");

    @Override
    protected Fragment createFragment() {
        return new ListFragment();
    }

    @Override
    protected int getContentViewLayout() {
        return R.layout.activity_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);                           // Устанавливаем тулбар (и меню)
        mToolbar = new ToolbarFiltersControl(toolbar);
    }


    // Создание меню. Устанавливаем чекбоксы
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.list_app_bar, menu);
        menu.findItem(R.id.listMenu_item_showCompleted).setChecked(!FilterManager.incomplete().isFilterSet());
        menu.findItem(R.id.listMenu_item_showDelayed).setChecked(!FilterManager.actual().isFilterSet());
        return true;
    }

    // Обработка выбора меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.listMenu_item_showCompleted:  // показывать завершенные
                FilterManager.incomplete().flip();
                item.setChecked(!FilterManager.incomplete().isFilterSet());
                return true;

            case R.id.listMenu_item_showDelayed:    // показывать отложенные
                FilterManager.actual().flip();
                item.setChecked(!FilterManager.actual().isFilterSet());
                return true;

            case R.id.listMenu_item_settings:       // диалог Settings
                BaseFullScreenDialog.createDialog(new SettingsDialog(), null);
                return true;

            case R.id.listMenu_item_help:           // диалог Справка
                BaseFullScreenDialog.createDialog(new HelpDialog(), null);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        final int notifiedTasks =  NotificationController.getInstance().getNumLines();
        if(notifiedTasks > 0) {                                             // если есть необработанные нотификации (т.е. пользователь кликает не по уведомлению, а запускает активность)
            FilterManager.activated().set(NotificationController.getInstance().getNotifiedTasks()); // копируем нотифицированные задачи в фильтр
            // реакция на нажатие кнопки снека
            Snack.show(getMainFragment().getView(),                         // отображаем снек "активированно N задач" с кнопкой
                getResources().getQuantityString(R.plurals.task_activated_plural, notifiedTasks, notifiedTasks),
                null,
                    v -> {
                        if(notifiedTasks == 1)                             // если одна задача - сразу ее открываем
                            InterfaceEventsFacade.send(InterfaceEventsFacade.Event.OPEN_CARD, FilterManager.activated().getOnlyTask());
                        else {
                            FilterManager.activated().setFilter(true);       // иначе устанавливаем фильтр активированных задач
                        }
                    },
                R.string.go_activated,                                      // сообщение на кнопке
                3000                                                        // снек отображается 3 сек
            );
            NotificationController.getInstance().clearNotification();       // нотификация удаляется
        }

        // обрабатываем intent в onStart, т.к. для обработки требутестся существование фрагмента
        if(!isRecreated())                  // если активность была создана пользователем, интентом от нотификации или другого приложения (SEND) -
            handleIntent(getIntent());      // то обрабатываем порождающий интент. Если она была восстановлена ОС, то интент обрабатывть не нужно - он уже обрабатывался
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(!hasFocus)
            ((ListFragment) getMainFragment()).onFocusLost();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mToolbar.onClose();
    }

    // Разбор входных параметров интента
    // ожидаются интенты от NotificationController OPEN_CARD или OPEN_LIST. Для последней команды требуется только очистить нотификацию
    private void handleIntent(Intent intent) {
            Bundle inParam = intent.getBundleExtra(IN_PARAM);
            if (inParam != null && inParam.size() > 0) {
                BundleWrapper bundleWrapper = new BundleWrapper(inParam);

                if(bundleWrapper.getInteger() == OPEN_CARD)
                    InterfaceEventsFacade.send(InterfaceEventsFacade.Event.OPEN_CARD, bundleWrapper.getDBObject());

                intent.removeExtra(IN_PARAM);

                //todo перенести в NotificationController для универсализации обработчика. Для этого вызываться должна startForActivityResult
                NotificationController.getInstance().clearNotification();           // нотификация удаляется;
        }
    }

    // класс для запуска активности с параметрами.
    // используется в контроллере нотификаций
    public static class StartActivityBuilder {
        private Intent mIntent;
        private Context mContext;
        private BundleWrapper mBundleWrapper = new BundleWrapper();

        public StartActivityBuilder(Context context) {
            mContext = context;
            mIntent = new Intent(context, MainActivity.class);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        // запуск актианости
        public void start() {
            if(isCurrentActivityThe(DetailsActivity.class))              // если запущена карточка задачи, то сначала завершаем ее
                AbstractActivity.getCurrentActivity().finish();

            mIntent.putExtra(IN_PARAM, mBundleWrapper.getBundle());
            mContext.startActivity(mIntent);
        }

        // активность должна просто отобразиться
        public void showList() {
            mBundleWrapper.addParam(OPEN_LIST);
        }

        // активность должна открыть карточку с задачей
        public void openTaskCard(Task task) {
            mBundleWrapper.addParam(OPEN_CARD);
            mBundleWrapper.addParam(task);
        }

        private boolean isCurrentActivityThe(Class activityClass) {
            Activity currentActivity = AbstractActivity.getCurrentActivity();
            return currentActivity != null && currentActivity.getClass() == activityClass;
        }
    }
}