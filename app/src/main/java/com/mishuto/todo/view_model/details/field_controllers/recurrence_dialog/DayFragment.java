package com.mishuto.todo.view_model.details.field_controllers.recurrence_dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.task_fields.recurrence.Day;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.spinner.SpinnerWidget;

import static com.mishuto.todo.view.LayoutWrapper.createLayoutIterator;

/**
 * Дочерний (nested) фрагмент ContainerDialog, отображающий секцию Recurrence.Day
 * Created by Michael Shishkin on 11.11.2016.
 */
public class DayFragment extends BasePageFragment<Day> implements
        View.OnClickListener,
        CompoundButton.OnCheckedChangeListener
{

    RadioButton mHoursButton;       // Радиокнопка для выбора часовых интервалов
    RadioButton mDaysButton;        // Радиокнопка для выбора дневных интервалов
    SpinnerWidget<Day.HourlyFactor> mHoursSpinner;  // Спинер часовых интервалов
    SpinnerWidget<Day.DailyFactor> mDaysSpinner;    // Спинер дневных интервалов
    SwitchCompat mWorkHours;        // Свитч "только в рабочее время"

    ViewGroup mLayout2;             // Layout дополнительный виджетов дневного интервала

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View childView = inflater.inflate(R.layout.dlg_rec_page_day, container, false);

        // связывание переменных с виджетами
        onCreateView(childView);
        mHoursButton =  childView.findViewById(R.id.recurrence_day_radio_hour);
        mDaysButton =   childView.findViewById(R.id.recurrence_day_radio_day);
        Spinner hours = childView.findViewById(R.id.recurrence_day_hour_spinner);
        mWorkHours =    childView.findViewById(R.id.recurrence_day_switch_workHours);
        Spinner days =  childView.findViewById(R.id.recurrence_day_daily_spinner);

        mLayout2 =  childView.findViewById(R.id.recurrence_day_layout2);

        // инициация виджетов
        mHoursSpinner = new SpinnerWidget<>(hours, Day.HourlyFactor.values(), mSegment.getHourlyFactor(),
                new SpinnerWidget.OnChangeSelectionListener<Day.HourlyFactor>() {
                    @Override
                    public void onChangeSelection(Day.HourlyFactor item, int index, int spinnerId) {
                        mSegment.setHourlyFactor(item);
                        onUpdate(true);
                    }
                });

        mDaysSpinner = new SpinnerWidget<>(days, Day.DailyFactor.values(), mSegment.getDailyFactor(),
                new SpinnerWidget.OnChangeSelectionListener<Day.DailyFactor>() {
                    @Override
                    public void onChangeSelection(Day.DailyFactor item, int index, int spinnerId) {
                        mSegment.setDailyFactor(item);
                        onUpdate(true);
                    }
                });

        mWorkHours.setChecked(mSegment.isOnlyWorkTime());

        // установка лисенеров
        mHoursButton.setOnClickListener(this);
        mDaysButton.setOnClickListener(this);
        mWorkHours.setOnCheckedChangeListener(this);

        showUp(!mSegment.isDaySelected());  // установка состояний виджетов
        onUpdate(false);                    // обновление информации в родительском диалоге

        return childView;
    }

    // событие выбора радиокнопок
    @Override
    public void onClick(View v) {
        boolean isHoursSelected = v.getId() == mHoursButton.getId();
        mSegment.setDaySelected(!isHoursSelected);        // сохраняем выбор секции
        showUp(isHoursSelected);
        onUpdate(true);
    }


    // событие переключения выключателя WorkTime
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        // Если в настройках рабочее время не задано, свитч не активируется, а пользователь получает снек-сообщение об этом
        if (isChecked && !Settings.getWorkTime().isOn.get()) {
            Snack.show(getView(), R.string.rec_day_warning);
            mWorkHours.setChecked(false);
        }
        else
            mSegment.setOnlyWorkTime(isChecked);

        onUpdate(true);
    }

    // установка состояний для виджетов секции
    private void showUp(boolean hoursSelected) {
        mDaysButton.setChecked(!hoursSelected);         // кнопки переключаются. Сделано таким образом, поскольку
        mHoursButton.setChecked(hoursSelected);         // кнопки расположены в разных layout , что не позволяет их объединить в одну радиогруппу
                                                        // запрещаем неактивные параметры  и разрешаем активные

        mWorkHours.setEnabled(hoursSelected);

        mHoursSpinner.getSpinner().setEnabled(hoursSelected);


        for(View view : createLayoutIterator(mLayout2))
            view.setEnabled(!hoursSelected);

        mDaysSpinner.getSpinner().setEnabled(!hoursSelected);

        if (!hoursSelected)                             // если выбрана секция дневных интервалов,
            mTimeWidgets.onUpdateEditStatus();          // отдельно обновляем статусы TimeWidgets
    }
}

