package com.mishuto.todo.view_model.details.field_controllers;

import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.model.task_fields.Priority;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.spinner.SpinnerAdapter;
import com.mishuto.todo.view.spinner.SpinnerWidget;

import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_PREV;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_PREV_DISCARDS_TIME;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_TIME;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_TIME_DISCARDS_PREV;
import static com.mishuto.todo.model.task_fields.Priority.DEFAULT_NEW_PRIORITY;
import static com.mishuto.todo.model.task_fields.Priority.DO_TODAY;
import static com.mishuto.todo.model.task_fields.Priority.URGENTLY;

/**
 * Класс-контроллер, связывающий атрибут Priority модели с Spinner виджетом формы
 * Created by mike on 31.08.2016.
 */
public class PriorityController extends AbstractEditableController
        implements SpinnerWidget.OnChangeSelectionListener<Priority>,
        View.OnClickListener,
        TaskEventsFacade.TaskObserver
{
    private Task mTask;
    private final PriorityWidget mPriorityWidget;
    private final SpinnerAdapter<Priority> ALL_PRI_ADAPTER;     // адаптер со всеми приоритетами
    private final SpinnerAdapter<Priority> TIME_PRI_ADAPTER;    // адаптер с приоритетами по событию дата/время
    private final SpinnerAdapter<Priority> TERM_PRI_ADAPTER;    // адаптер с приоритетами по событию дата или предыд. задача
    private final View mWarn;                                   // иконка с предупреждением об установке приоритета после события

    public PriorityController(View view, Task t) {

        mTask = t;
        mPriorityWidget = new PriorityWidget(
                (Spinner)view.findViewById(R.id.taskDetails_Spinner_Priority),
                Priority.values(),                                                                  // инициируем основным списком
                mTask.getPriority(),
                this);

        //инициация адаптеров списков, которые можно менять в зависимости от условий
        ALL_PRI_ADAPTER = mPriorityWidget.getAdapter();
        TIME_PRI_ADAPTER = mPriorityWidget.createAdapter(new Priority[]{URGENTLY, DO_TODAY}, DEFAULT_NEW_PRIORITY);
        TERM_PRI_ADAPTER = mPriorityWidget.createAdapter(new Priority[]{DO_TODAY}, DO_TODAY);
        mWarn = view.findViewById(R.id.taskDetails_image_priorityWarn);
        mWarn.setOnClickListener(this);
        TextView label = view.findViewById(R.id.taskDetails_label_priority);

        TaskEventsFacade.getInstance().register(this);

        updateWidgets();
        setPerformChangedViews(mPriorityWidget.getSpinner(), label);
        onPerformEvent(t.isStateInPerformed());
    }

    @Override
    public void onChangeSelection(Priority item, int index, int spinnerId) {
        if(mTask.getPriority() != item)   // защита от лишних срабатываний
            mTask.setPriority(item);
    }

    @Override
    public void updateWidgets() {
        if(mTask.isSateDeferred()) {                    // Если задача отложена
            mWarn.setVisibility(View.VISIBLE);          // отображаем предупреждение

            if(mTask.getTaskTime().isTimeSet())         // выбираем приоритеты в зависимости от того
                setPriorityAdapter(TIME_PRI_ADAPTER);   // установлено время или нет
            else
                setPriorityAdapter(TERM_PRI_ADAPTER);
        }
        else {                                          // иначе (событий в будущем нет)
            mWarn.setVisibility(View.GONE);             // скрываем предупреждение
            setPriorityAdapter(ALL_PRI_ADAPTER);        // устанавливаем все приоритеты
        }
    }

    @Override
    public void onClose() {
        TaskEventsFacade.getInstance().unregister(this);
    }

    @Override
    public void onTaskEvent(TaskEventsFacade.EventType type, Task task, Object extendedData) {
        if(task == mTask && type.in(A_TIME, A_TIME_DISCARDS_PREV, A_TIME_DISCARDS_PREV, A_PREV_DISCARDS_TIME, A_PREV))
        updateWidgets();
    }

    // смена адаптера в спиннере в зависимости от требуемого набора приоритетов
    private void setPriorityAdapter(SpinnerAdapter<Priority> adapter) {
        if (mPriorityWidget.getAdapter() != adapter) {
            mPriorityWidget.setAdapter(adapter);
            if(adapter.getPosition(mTask.getPriority()) != -1 )
                mPriorityWidget.setSelected(mTask.getPriority());
            else
                mTask.setPriority(mPriorityWidget.getSelectedItem());
        }
    }

    // событие клика по иконке предупреждения
    @Override
    public void onClick(View v) {
        if(v.getId() == mWarn.getId())                              // Если нажата иконка с предупреждением
            Snack.show(mWarn, R.string.taskDetails_warn_priority);  // выводим предупреждение
    }
}
