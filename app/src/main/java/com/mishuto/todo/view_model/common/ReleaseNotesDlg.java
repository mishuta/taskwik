package com.mishuto.todo.view_model.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mishuto.todo.common.BundleWrapper;
import com.mishuto.todo.model.ReleaseNotes;
import com.mishuto.todo.model.ReleaseNotes.Feature;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.ViewCommon;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;

import java.util.ArrayList;
import java.util.List;

/**
 * Диалог, отображающий ReleaseNotes
 * Вызывается из настроек и из баннера
 *
 * Created by Michael Shishkin on 16.01.2019.
 */
public class ReleaseNotesDlg extends DialogFragment implements RecyclerViewWidget.EventsListener<Feature> {

    RecyclerViewWidget<Feature> mRecyclerWidget;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dlg_release_notes, (ViewGroup) getView());

        int fromVersion = BundleWrapper.getInteger(DialogHelper.inParameter(this));
        List<Feature> featureList = new ArrayList<>();

        for (Feature feature : ReleaseNotes.getInstance().getFeatures(fromVersion))
            featureList.add(0, feature);

        mRecyclerWidget = new RecyclerViewWidget<>(dialogView.findViewById(R.id.recycler_view), R.layout.dlg_release_notes_card, featureList, this);

        return new AlertDialog.Builder(getActivity())
                .setView(dialogView)
                .setTitle(R.string.release_notes)
                .create();
    }

    @Override
    public void onBind(View root, Feature item, RecyclerViewWidget<Feature>.ViewHolder viewHolder) {
        TextView build = root.findViewById(R.id.buildNo);
        TextView title = root.findViewById(R.id.title);
        TextView desc = root.findViewById(R.id.description);
        Feature feature = viewHolder.getItem();

        build.setText(feature.getBuildVersion());
        title.setText(feature.getTitle());
        desc.setText(feature.getDescription());
        title.setTextColor(ViewCommon.color(feature.isHot() ? R.color.Red : R.color.colorMainText));
    }

    @Override
    public void onClick(View view, RecyclerViewWidget<Feature>.ViewHolder viewHolder) { }
}
