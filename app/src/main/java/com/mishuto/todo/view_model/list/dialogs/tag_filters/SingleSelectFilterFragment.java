package com.mishuto.todo.view_model.list.dialogs.tag_filters;

import android.view.View;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.model.task_filters.Choice;
import com.mishuto.todo.model.task_filters.ChoiceTagFilter;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.recycler.RecyclerViewWidget;


/**
 * Диалог, управляющий списком одиночных тегов (проекты, исполнители). Наследуется конкретным классом диалога
 *
 * Created by Michael Shishkin on 17.07.2017.
 */

abstract class SingleSelectFilterFragment extends AbstractFilterTagFragment<StringContainer> {

    public SingleSelectFilterFragment(ChoiceTagFilter<StringContainer> tagFilter, String defaultTagName) {
        super(R.layout.fs_dlg_filters_item_single_tag, tagFilter, defaultTagName);
    }

    // обработка нажатия на строку списка
    @Override
    public void onClick(View view, RecyclerViewWidget<Choice.Item<StringContainer>>.ViewHolder viewHolder) {
        if(view.getId() == R.id.popup_menu)
            showMenu(view, viewHolder);
        else {
            mFilter.setSelector(viewHolder.getItem().getValue());
            mFilter.setFilter(mFilter.getSelector() != null);
            closeDialog();
        }
    }
}
