package com.mishuto.todo.view_model.details.field_controllers;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.todo.R;

import java.util.ArrayList;

/**
 * Контроллер поля Зависимые задачи
 * Created by Michael Shishkin on 17.01.2017.
 */

public class DependedTaskController  {

    @SuppressLint("SetTextI18n")
    public DependedTaskController(View root, Task task) {

        ArrayList<String> list = getDependentTasks(task);
        TextView value = root.findViewById(R.id.taskDetails_state_dependentTask);

        ExpandedPanel expandedPanel = new ExpandedPanel(
                value,
                (ImageButton) root.findViewById(R.id.taskDetails_dependent_expand),
                (ImageButton) root.findViewById(R.id.taskDetails_dependent_collapse),
                (RecyclerView) root.findViewById(R.id.taskDetails_dependent_panel),
                list, 1, null);

        expandedPanel.refreshPanel();
        int n = list.size();
        value.setText( n == 0 ? null : n + " " + root.getResources().getQuantityString(R.plurals.task_plural, n));
    }


    // получение начального списка зависимых задач и передача его в конструктор базового класса
    private static ArrayList<String> getDependentTasks(Task currentTask) {
        ArrayList<String> dependentTasks = new ArrayList<>();

        for(Task t : TaskList.getTasks())
            if(t.getPrev() != null && currentTask.equals(t.getPrev()))
                dependentTasks.add(t.toString());

        return dependentTasks;
    }
}
