package com.mishuto.todo.view_model.details.field_controllers.recurrence_dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.mishuto.todo.common.KeyString;
import com.mishuto.todo.model.task_fields.recurrence.Year;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.spinner.SpinnerWidget;

/**
 * Дочерний (nested) фрагмент ContainerDialog, отображающий секцию Recurrence.Year
 * Created by Michael Shishkin on 18.12.2016.
 */
public class YearFragment extends BaseMonthYearFragment<Year> {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View childView = inflater.inflate(R.layout.dlg_rec_page_year, container, false);

        onCreateView(childView);

        Spinner month = childView.findViewById(R.id.rec_year_month);
        Spinner year =  childView.findViewById(R.id.rec_year_foldYear);

        KeyString[] months = Year.getMonthsInYearArray();
        new SpinnerWidget<>(month, months, months[mSegment.getMonth()],
                new SpinnerWidget.OnChangeSelectionListener<KeyString>() {
                    @Override
                    public void onChangeSelection(KeyString item, int index, int spinnerId) {
                        mSegment.setMonth(item.getKey());
                        onUpdate(true);
                    }
                });

        new SpinnerWidget<>(year, Year.YearlyFactor.values(), mSegment.getYearFactor(),
                new SpinnerWidget.OnChangeSelectionListener<Year.YearlyFactor>() {
                    @Override
                    public void onChangeSelection(Year.YearlyFactor item, int index, int spinnerId) {
                        mSegment.setYearFactor(item);
                        onUpdate(true);
                    }
                });

        return childView;
    }
}
