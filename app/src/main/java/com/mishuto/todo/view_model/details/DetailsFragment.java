package com.mishuto.todo.view_model.details;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.database.Transactions;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.ImageCheckedButton;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view_model.common.InterfaceEventsFacade;
import com.mishuto.todo.view_model.common.delay.DelayController;

import static com.mishuto.todo.common.BundleWrapper.getLong;
import static com.mishuto.todo.view.ViewCommon.setBackPressListener;
import static com.mishuto.todo.view.dialog.DialogHelper.onCloseDialog;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.CardMsg;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.Event;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.Event.CHANGE_TASK;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.Event.CHANGE_TASK_REQUEST;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.Event.CLOSE_CARD;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.SubEvent;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.SubEvent.TASK_CANCEL;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.SubEvent.TASK_CONFIRM;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.SubEvent.TASK_DELETE;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.SubEvent.TASK_UNCHANGED;
import static com.mishuto.todo.view_model.common.InterfaceEventsFacade.SubEvent.TASK_WAS_SAVED;
import static com.mishuto.todo.view_model.details.TransactedFragmentSupporter.ExitState.OK;
import static com.mishuto.todo.view_model.details.TransactedFragmentSupporter.ExitState.SKIP;

/**
 * Фрагмент для отображения/изменения деталей задачи
 * Created by mike on 20.08.2016.
 */
public class DetailsFragment extends Fragment implements View.OnClickListener,
        TaskEventsFacade.TaskObserver,
        InterfaceEventsFacade.EventObserver,
        TransactedFragmentSupporter.TransactionEvents
{
    private Task mTask;                             // Объект модели, отображаемый фрагментом
    private TaskCardController mTaskCardController; // Объект, отображающий атрибуты задачи
    private ImageCheckedButton mDelay;              // кнопка отложить
    private ImageCheckedButton mComplete;           // кнопка завершить

    TransactedFragmentSupporter mTransactor;        // поддержка транзакций

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Long id = getLong(getArguments());  //Получаем сохраненный DetailsActivity при создании DetailsFragment recordId задачи
        TSystem.approveThat(id > 0);

        mTask = TaskList.getById(id);
    }

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_details, container, false);
        initToolbar();                                          // инициализируем виджеты тулбара
        mTaskCardController = new TaskCardController(v, mTask); // создаем представление карточки с контроллерами
        mTransactor = new TransactedFragmentSupporter(this, savedInstanceState, v);
        mTransactor.setEventsListener(this);

        TaskEventsFacade.getInstance().register(this);
        InterfaceEventsFacade.getInstance().register(this);

        setBackPressListener(v, this::doCancel);    // перехват кнопки back с вызовом doCancel()

        return v;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mTransactor.onSaveState(outState);  // сохранение состояния mTransactor на случай прибивания приложения ОС
    }

    @Override
    public void afterTransactionStarted() { }

    @Override
    public void beforeTransactionEnds() {
        mTaskCardController.onSave();      // перед уходом в бэкграунд сохраняются введенные изменения, поскольку не все (Title) сохраняются на лету
    }

    // Уничтожение фрагмента. Отписываемя от рассылки
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        TaskEventsFacade.getInstance().unregister(this);
        InterfaceEventsFacade.getInstance().unregister(this);
    }

    /**
     * Колбэк, вызваемый дочерним фрагментом после его завершения. Используется для обработки значений, возвращаемых диалогами
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        onCloseDialog(requestCode, resultCode, data);
    }

    // инициализация виджетов тулбара
    private void initToolbar() {
        //noinspection ConstantConditions
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);

        mComplete = toolbar.findViewById(R.id.toolbar_perform);
        mComplete.setChecked(mTask.isStateInPerformed());
        mComplete.setOnClickListener(this);

        mDelay = toolbar.findViewById(R.id.toolbar_delay);
        mDelay.setChecked(mTask.isSateDeferred());
        mDelay.setOnClickListener(this);

        toolbar.findViewById(R.id.toolbar_remove).setOnClickListener(this);
        toolbar.findViewById(R.id.toolbar_save).setOnClickListener(this);
        toolbar.findViewById(R.id.toolbar_cancel).setOnClickListener(this);
    }

    // обработка кнопок меню тулбара
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.toolbar_perform:                                      // завершить
                mTask.setPerformed(!mTask.isStateInPerformed());
                if(mTask.isStateRepeated() )
                    Snack.show(getView(), getString(R.string.repeat_task_at, mTask.getRecurrence().getNextEvent().toString()));
                break;

            case R.id.toolbar_delay:                                        // отложить
                mDelay.setChecked(mTask.isSateDeferred());
                DelayController.delayTask(mTask);
                break;

            case R.id.toolbar_remove:                                       // удалить
                complete(TASK_DELETE);
                break;

            case R.id.toolbar_save:                                         // сохранить
                complete(mTransactor.wasChanged() ? TASK_CONFIRM            // если были изменения, то они должны быть сохранены
                        : mTransactor.wasChangesSaved() ? TASK_WAS_SAVED    // иначе уведомляем о том что измения уже были сохранены
                        : TASK_UNCHANGED);                                  // иначе - с задачей ничего не делали;
                break;

            case R.id.toolbar_cancel:                                       // отменить
                doCancel();
                break;
        }
    }

    void complete(SubEvent subEvent) {
        complete(CLOSE_CARD, subEvent, null);
    }

    /**
     * процедура завершения редактирования
     * @param event команда завершения сообщения InterfaceEventsFacade
     * @param subEvent событие завершения
     * @param task задача, которая должна загрузиться в карточку. Используется, если Event==CHANGE_TASK
     */
    void complete(Event event, SubEvent subEvent, Task task) {
        mTaskCardController.onSave();                               // сообщить всем контроллерам о сохранении значений перед закрытием

        if(subEvent == TASK_CANCEL && mTransactor.wasChanged()) {   // Если пользователь отменяет ввод - все изменения с момента открытия транзакции - откатываются
            boolean isPerform = mTask.isStateInPerformed();         // Изменения БД, которые шли параллельно от таймеров в своих потоках - применяются,
            Transactions.rollbackTransaction();                     // т.к. потоки разблокируются завершением транзакции
                                                                    // Восстанавливаем неперсистентные атрибуты объектов:
            mTask.setTimerEvent();                                  // переустанавливаем таймер

            if(mTask.isStateInPerformed() != isPerform)             // если менялось состояние задачи, переустанавливаем его на то же значение
                mTask.setPerformed(!isPerform);                     // чтобы оповестить зависимые задачи

            mTransactor.setFinishState(SKIP);                               // транзакцию явно откатили, следовательно, на выходе ничего с транзакциями делать не надо
        }
        else
            mTransactor.setFinishState(OK);                                 // в противном случае - коммитим транзакцию

        mTaskCardController.onClose();                              // закрываем контроллеры

        InterfaceEventsFacade.send(event, new CardMsg(subEvent, mTask, task)); // уведомляем список и активность о завершении редактирования
    }

    // Операция отмены задачи
    // Выполняется при нажатии кнопки [X], back button
    void doCancel() {
        if(mTransactor.wasChanged())                                                                // если были изменения
            new AlertDialog.Builder(getContext())                                                   // запрашиваем подтверждение отмены у пользователя
                    .setMessage(R.string.cancel_confirmation)
                    .setNegativeButton(R.string.no, null)                                           // если не подтверждает - ничего не делаем
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            complete(mTransactor.wasChangesSaved() ? TASK_WAS_SAVED : TASK_CANCEL); // если подтверждает отмену - закрываем
                        }
                    })
                    .create()
                    .show();
        else
            complete(mTransactor.wasChangesSaved() ? TASK_WAS_SAVED : TASK_UNCHANGED);              // если изменений не было - то закрываем карточку без окна отмены
    }

    // обработка событий задачи для изменения статуса кнопки
    @Override
    public void onTaskEvent(TaskEventsFacade.EventType type, final Task task, Object extendedData) {
        if(task == mTask) {
            //noinspection ConstantConditions
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDelay.setChecked(task.isSateDeferred());
                    mComplete.setChecked(task.isStateInPerformed());
                }
            });
        }
    }

    // обработка событий интерфейса для перегрузки задачи
    @Override
    public void onInterfaceEvent(Event event, Object param) {
        if(event == CHANGE_TASK_REQUEST) {
            final CardMsg msg = (CardMsg) param;
            if (mTransactor.wasChanged())                                                           // если в текущей задаче были изменения
                new AlertDialog.Builder(getContext())                                               // запрашиваем подтверждение сохранения изменений текущей задачи
                        .setMessage(R.string.reset_save_confirmation)
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {     // кнопка Не сохранять
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                complete(CHANGE_TASK,                                               // закрыть задачу с отменой сохранения
                                        mTransactor.wasChangesSaved() ? TASK_WAS_SAVED : TASK_CANCEL,
                                        msg.taskToChange);
                            }
                        })
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {    // кнопка Сохранить
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                complete(CHANGE_TASK, TASK_CONFIRM, msg.taskToChange);              // закрыть задачу с сохранением изменений
                            }
                        })
                        .setNeutralButton(android.R.string.cancel, null)                            // по кнопке отмена - ничего не делаем
                        .create()
                        .show();
            else
                complete(CHANGE_TASK,                                                               // если изменений не было - то закрываем задачу без уведомления
                        mTransactor.wasChangesSaved() ? TASK_WAS_SAVED : TASK_UNCHANGED,
                        msg.taskToChange);
        }
    }
}