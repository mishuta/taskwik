package com.mishuto.todo.view_model.common;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.mishuto.todo.common.BackupManager;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view_model.list.MainActivity;

/**
 * Активность, которая запускается до загрузки данных из БД (и основной активности) и выявляет проблемы с БД.
 * Если все ок, управление передается в MainActivity
 * Created by Michael Shishkin on 04.07.2018.
 */
public class LoadCheckerActivity extends AppCompatActivity {
    private BackupManager mManager;

    @Override
    protected void onStart() {
        super.onStart();
        mManager = BackupManager.getInstance();

        if(!mManager.isFailureDetected()) { // если проблем с загрузкой не обнаружены
            mManager.onStart();             // увеличить счетчик неудачных попыток перед запуском активности (только когда приложение запускается вручную, а не по таймеру)
            startNextActivity();            // передаем управление основной активности
        }

        else if(mManager.isBackupExists())
            showRestoreDialog();            // иначе если есть бэкап - предлагаем грузиться из него

        else
            showClearDialog();              // если бэкапа нет - предлагаем очистить базу
    }

    // диалог восстановления из бэкапа
    private void showRestoreDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.LoadFailureTitle)
                .setMessage(R.string.LoadFailure_recover)
                .setPositiveButton(R.string.buttonRecover, restoreDB())             // восстановить
                .setNegativeButton(R.string.buttonCancel, cancelDialog())           // выйти
                .setNeutralButton(R.string.buttonClear, showClearWarningDialog())   // очистить
                .show();
    }

    // диалог очистки базы
    private void showClearDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.LoadFailureTitle)
                .setMessage(R.string.LoadFailure_clear)
                .setNegativeButton(R.string.buttonCancel, cancelDialog())       // выйти
                .setPositiveButton(R.string.buttonClear, clearDB())             // очистить
                .show();
    }

    // предупреждение перед очисткой
    private DialogInterface.OnClickListener showClearWarningDialog() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new AlertDialog.Builder(LoadCheckerActivity.this)
                        .setMessage(R.string.LoadFailure_clearWarning)
                        .setNegativeButton(android.R.string.cancel, returnToRestoreDialog())
                        .setPositiveButton(R.string.buttonClear, clearDB())                     // очистить
                        .show();

            }
        };
    }

    // операция: выйти из диалога. Активности завершаются, но Application может выжить.
    private DialogInterface.OnClickListener cancelDialog() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mManager.resetAttemptsCounter();    // сбрасываем количество неудачных попыток (чтобы попробовать еще раз запустить)
                finish();                           // завершаем текущую активность
            }
        };
    }

    // операция: очистка БД
    private DialogInterface.OnClickListener clearDB(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!mManager.deleteDB())
                    showAlertAndFinish(R.string.cantDeleteMsg); // если очистка неуспешна, показываем сообщение с советом и выходим
                else
                    startNextActivity();                        // иначе продолжаем загрузку после очистки
            }
        };
    }

    // операция: восстановление из бэкапа
    private DialogInterface.OnClickListener restoreDB() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new AlertDialog.Builder(LoadCheckerActivity.this)
                        .setMessage(String.format(getResources().getString(R.string.LoadFailure_restoreWarning), mManager.getBackupDate().toString()))
                        .setPositiveButton(R.string.buttonRecover, new DialogInterface.OnClickListener() {  // восстановить
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(!mManager.restoreBackup())
                                    showAlertAndFinish(R.string.cantRestoreMsg);                // если восстановление неуспешно, показываем сообщение с советом и выходим
                                else
                                    startNextActivity();                                        // иначе продолжаем загрузку после восстановления
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, returnToRestoreDialog())    // отмена
                        .show();
            }
        };
    }

    // показать сообщение и выйти из активности по нажатию кнопки
    private void showAlertAndFinish(int message) {
        new AlertDialog.Builder(LoadCheckerActivity.this)
                .setMessage(message)
                .setPositiveButton(R.string.buttonCancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }

    //возвращение в showRestoreDialog
    private DialogInterface.OnClickListener returnToRestoreDialog() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showRestoreDialog();
            }
        };
    }

    // Передача управления следующей активности
    void startNextActivity() {
        if(!WelcomeActivity.wasShowed())                                        // если WelcomeActivity еще не отображалась (при первом запуске)
            startActivityForResult(new Intent(this, WelcomeActivity.class), 1); // вызываем WelcomeActivity и получаем управление назад, чтобы корректно обработать нажатие back в WelcomeActivity
        else {                                                                  // иначе
            startActivity(new Intent(this, MainActivity.class));                // стартуем MainActivity
            finish();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        startActivity(new Intent(this, MainActivity.class));  // стартуем MainActivity
        finish();
    }
}
