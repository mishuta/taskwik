package com.mishuto.todo.view_model.details.field_controllers;

import android.os.Bundle;
import android.view.View;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.todo.R;

import static com.mishuto.todo.common.BundleWrapper.getDBObject;
import static com.mishuto.todo.model.tags_collections.Tags.EMPTY_TAG;

/**
 * Контроллер DetailsFragmentControl для отображения и изменения атрибута задачи Проект
 * Created by Michael Shishkin on 04.01.2017.
 */

public class ProjectController extends GenericController {

    public ProjectController(View view, Task task) {
        super(view, task, R.id.taskDetails_label_project, R.id.taskDetails_project, R.id.taskDetails_clearProject);
        updateWidgets();
    }

    @Override
    void onAction() {
        createDialog(new ProjectDialog(), null);
    }

    @Override
    void onClear() {
        mTask.setProject(EMPTY_TAG);
        updateWidgets();
    }

    @Override
    public void updateWidgets() {
        setState(mTask.getProject() != EMPTY_TAG ? mTask.getProject().toString() : null);
    }

    @Override
    public void onCloseDialog(int resultCode, Bundle resultObject) {
        mTask.setProject((StringContainer) getDBObject(resultObject));
        updateWidgets();
    }
}
