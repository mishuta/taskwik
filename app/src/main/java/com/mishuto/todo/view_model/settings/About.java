package com.mishuto.todo.view_model.settings;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;

import com.mishuto.todo.common.BundleWrapper;
import com.mishuto.todo.common.PersistentValues;
import com.mishuto.todo.common.TLog;
import com.mishuto.todo.todo.BuildConfig;
import com.mishuto.todo.todo.R;
import com.mishuto.todo.view.Snack;
import com.mishuto.todo.view.dialog.DialogHelper;
import com.mishuto.todo.view_model.common.ReleaseNotesDlg;

/**
 * Класс для отображения статической секции Settings
 * Created by Michael Shishkin on 19.07.2018.
 */
class About {
    @SuppressLint("SetTextI18n")
    About(View root ) {
        final TextView appVersion = root.findViewById(R.id.settings_appVersion);

        appVersion.setText(BuildConfig.VERSION_NAME);
        appVersion.setOnLongClickListener( view -> {                    // скрытая настройка включения/выключения записи лога в файл по длинному клику по версии приложения
                boolean state = PersistentValues.getFile(TLog.SHARED_FILE).getBoolean(TLog.IS_LOG_SAVED, false).flip();
                Snack.show(appVersion, state ? R.string.logFile_on : R.string.logFile_off);
                return true;
            }
        );

        root.findViewById(R.id.settings_whats_new).setOnClickListener(  // открытие диалога ReleaseNotes со списком версий, начиная с 0
                view -> DialogHelper.createDialog(new ReleaseNotesDlg(), BundleWrapper.toBundle(0))
        );
    }
}
