package com.mishuto.todo.common;

import java.util.Arrays;

/**
 * Статические методы для преобразования текста
 *
 * Created by mike on 02.09.2016.
 */
@SuppressWarnings("WeakerAccess")
public final class TextUtils {

    // Замена первой буквы в строке на заглавную
    public static String capitalize(String s){
        return empty(s) ? s : s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    // Непустая строка (без учета пробелов)?
    public static boolean empty(String s) {
        return (s == null || s.trim().isEmpty());
    }

    // сравнение двух строк без учета регистра и пробелов
    public static boolean insensitiveEquals(String s1, String s2) {
        return s1.trim().equalsIgnoreCase(s2.trim());
    }

    // Служит для представления массива элементов в виде строки. Элементы должны поддерживать toString()
    public static <T> String itrToString(Iterable<T> iterable, char separator) {
        if (iterable == null || !iterable.iterator().hasNext())
            return "";

        StringBuilder out = new StringBuilder("");

        for(T a : iterable) {
            out.append (a == null ? "null" : a.toString()).append(separator).append(" ");
        }
        return out.substring(0, out.length()-2); // отрезаем завершающие "; "
    }

    public static <T> String itrToString(T[] array, char separator) {
        return itrToString(array == null ? null : Arrays.asList(array), separator);
    }

    //поиск первой вакантной строки в коллекции соотвествующей префиксу
    // объекты коллекции должны поддерживать toString()
    //например, допустим prefix = "проект"
    //если в коллекции нет строки "проект", то вернется значение префикса: "проект". Иначе вернется "проект 1". Если и такой есть, то "проект 2" и т.д.
    public static <T> String uniqueStr(Iterable<T> collection, String prefix) {

        boolean found;
        String probe = prefix;
        int i = 0;

        do {
            found = false;

            for (T s : collection)
                if (insensitiveEquals(probe, s.toString())) {
                    found = true;
                    break;
                }

            if( found )
                probe = prefix + " " + ++i;
        }
        while ( found );

        return probe;
    }

    // содержит ли объект заданную подстроку
    public static boolean isSubString(Object o, String s) {
        return o.toString().toLowerCase().contains(s.toLowerCase());
    }

    // содержит ли хотя бы один элемент коллекции объектов заданную подстроку
    public static boolean contains(Iterable collection, String s) {
        for(Object item: collection)
            if(isSubString(item, s))
                return true;

        return false;
    }
}
