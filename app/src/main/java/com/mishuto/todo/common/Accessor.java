package com.mishuto.todo.common;

/**
 * Интерфейс для унификации доступа к объектам.
 * Обычно служит оберткой для объектов immutable типов
 * Created by Michael Shishkin on 16.07.2018.
 */
public interface Accessor<T> {
    T get();
    void set(T value);
}
