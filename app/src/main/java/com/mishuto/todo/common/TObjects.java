package com.mishuto.todo.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Содержит общие статические методы для работы с объектами
 *
 * Created by Michael Shishkin on 01.12.2016.
 */

public class TObjects {

    // возвращает результат проверки равенства двух объектов. Оба могут быть null
    public static boolean eq(Object t1, Object t2) {
        if(t1 == null)
            return t2 == null;
        else
            return t1.equals(t2);
    }

    // возвращает true, если value найден среди перечисленных объектов
    @SafeVarargs
    public static <T> boolean in(T value, T... vars) {
        for (T t : vars)
            if(t.equals(value))
                return true;

        return false;
    }

    // возвращает результат clonedObject.clone(), если этот метод определен в классе передаваемого объекта, и является публичным
    // для immutable классов возвращается сам объект
    // используется для случаев, когда нужно вызывать метод clone() для объекта, который представлен в виде общего типа,
    // для которого clone() является по умолчанию protected.
    // Например, для сабклассов интерфейса List метода public clone() определен, но он не определен в самом интерфейсе
    public static <T> T clone(T clonedObject) {
        Class<?> clazz = clonedObject.getClass();
        // если объект immutable, то возвращаем сам объект
        if(in(clazz, String.class, Integer.class, Long.class, Boolean.class, Class.class) || Enum.class.isAssignableFrom(clazz) )
            return clonedObject;

        try {
            Method func = clazz.getMethod("clone", (Class<?>[]) null);
            //noinspection unchecked
            return (T)func.invoke(clonedObject, (Object[]) null);
        }
        catch(NoSuchMethodException e) {
            throw new AssertionError("method clone for class " + clazz.toString() + " not defined or declared as a public");
        }
        catch(Exception e){
            throw new Error(e);
        }
    }

    /**
     * Клонирования любого объекта через сериализацию в байтовый поток
     * @param inObject Клонируемый объект
     * @return Копия объекта
     */
    public static <T extends Serializable> T copy(T inObject)  {

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();   // побайтовый поток для записи
            ObjectOutputStream oos = new ObjectOutputStream(baos);      // объектный поток для записи инициируется байтовым потоком

            oos.writeObject(inObject);                                  // сохраняем входной объект объект
            oos.close();                                                // закрываем поток

            // создаем побайтовый поток для чтения и передаем ему на вход буфер из потока для записи
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);        // объектный поток для чтения инициируется байтовым потоком

            @SuppressWarnings("unchecked")
            T outObject = (T) ois.readObject();                          // читаем выходной объект
            ois.close();                                                 // закрываем поток для чтения

            return outObject;
        }

        // Поскольку события ошибки чтени/записи в байтовый поток и ошибки преобразование типа T->ByteArray->T имеют близкую нулю вероятность,
        // не выбрасываем проверяемое исключение в вызывающие функции, а генерируем непроверяемые
        catch (IOException | ClassNotFoundException e){
            TSystem.debug(null, "TObjects.copy(): Exception while object coping");
            throw new Error(e);
        }
    }

    /*
     * Создание объекта заданного класса.
     * Класс создаваемого объекта должен иметь дефолтовый конструктор
     */
    public static <T> T createInstance(Class<T> tClass) {
        try {
            @SuppressWarnings("unchecked")
            Constructor<T> constructor = tClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            return constructor.newInstance();
        }
        catch (NoSuchMethodException e){            // нет дефолтного конструктора
            throw new Error("The class '" + tClass.getName() + "' doesn't have a default constructor\n" + e.toString());
        }
        catch(InvocationTargetException e) {        // эксепшн в конструкторе
            e.getTargetException().printStackTrace();
            throw new Error("Error while instantiate of object of " + tClass.getName() + "\n" + e.getTargetException().getMessage());
        }
        catch(Exception e){
            throw new Error("Can't create object of " + tClass.getName() + "\n" + e.toString());
        }
    }
}
