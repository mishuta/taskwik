package com.mishuto.todo.common;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Множество на базе списка ArrayList, содержащего уникальные значения
 * Created by Michael Shishkin on 20.04.2018.
 */
public class ArraySet<E> extends ArrayList<E> implements Set<E>, List<E> {
    @Override
    public boolean add(E e) {
        return !contains(e) && super.add(e);    // добавляем только уникальные значения
    }

    @Override
    public boolean addAll(@NonNull Collection<? extends E> c) {
        Set<E> set = new HashSet<>(this);
        boolean b = set.addAll(c);
        clear();
        super.addAll(set);
        return b;
    }
}
