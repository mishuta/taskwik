package com.mishuto.todo.common;

import android.os.Bundle;

import com.mishuto.todo.database.DBObject;

import java.io.Serializable;

import static com.mishuto.todo.App.createTagName;

/**
 * Обертка, позволяющая упростить сохранение и извлечение объектов из бандла.
 * Используется при передаче параметров между фрагментами, активностями и т.д.
 *
 * Параметры должны извлекаться из бандла в том же порядке что и сохранялись (чтобы избежать создания и расшаривания ключей)
 * Для частного случая создания/извлечения одного объекта в/из бандл/а используются статические функции-шоткаты
 * Для упаковки параметров в бандл используется паттерн Строитель.
 * Пример использования: Bundle bundle = new BundleWrapper().addParam(a).addParam(b).addParam(c).getBundle();
 *
 * Упаковка/распаковка DBObject вместо сериализации драматически экономит время и место, т.к. объект уже есть в кеше,
 * и достаточно просто сохранить идентификаторы объекта, чтобы получить потом объект из кеша
 * Даже если его в кеше нет, то время экономится на сериализации. А вместо десериализации выполлняется операция чтения из БД
 * Цена за скорость - следующие ограничения:
 * На момент упаковки объекта в бандл должна храниться актуальная версия объекта (выполнен update)
 * Если объект в БД вообще не сохранялся (RecordId==0), то упаковщик создает объект в БД, а распаковщик удаляет.
 * Это занимает время, кроме того, важно чтобы после упаковки такой объект был распакован, причем только один раз.
 *
 * Created by Michael Shishkin on 28.01.2018.
 */

@SuppressWarnings("WeakerAccess")
public class BundleWrapper {
    // ключи для сохранения в бандле
    private final static String SERIALIZABLE = createTagName("serializable");
    private final static String DB_OBJECT_ID = createTagName("DBObject.id");
    private final static String DB_OBJECT_CLASS = createTagName("DBObject.class");
    private final static String DB_OBJECT_DEL = createTagName("DBObject.isDelete");
    private final static String LONG = createTagName("long");
    private final static String BOOLEAN = createTagName("bool");
    private final static String INTEGER = createTagName("integer");

    private final static long NULL_SIGN = -1;   // признак того, что сохраняемый объек DBObject = null

    private Bundle mBundle;                     // используемый бандл
    private Integer mPosition = 1;              // порядковый номер создаваемого/читаемого ключа (добавляется к ключу)

    // конструктор, используемый при создании бандла
    public BundleWrapper() {
        mBundle = new Bundle();
    }

    // конструктор, используемый при чтении из бандла
    public BundleWrapper(Bundle bundle) {
        mBundle = bundle;
    }

    public Bundle getBundle() {
        return mBundle;
    }

    // **************** add методы ********************

    public BundleWrapper addParam(Serializable param) {
        mBundle.putSerializable(SERIALIZABLE + mPosition++, param);
        return this;
    }

    public BundleWrapper addParam(DBObject param) {
        if(param == null)                                           // если объект == null, сохраняем в DB_OBJECT_ID признак нулевого объекта
            mBundle.putLong(DB_OBJECT_ID + mPosition++, NULL_SIGN);    // остальные ключи не заполняются
        else {
            long id = param.getRecordID();
            mBundle.putBoolean(DB_OBJECT_DEL, id == 0);             // если объект еще не создан, то сохраняем эту информацию
                                                                    // чтобы создать, а при получении - удалить
            if (id == 0)
                id = param.create();

            mBundle.putLong(DB_OBJECT_ID + mPosition++, id);
            mBundle.putString(DB_OBJECT_CLASS, param.getClass().getName());
        }
        return this;
    }

    public BundleWrapper addParam(Long param) {
        mBundle.putLong(LONG + mPosition++, param);
        return this;
    }

    public BundleWrapper addParam(Boolean param) {
        mBundle.putBoolean(BOOLEAN + mPosition++, param);
        return this;
    }

    public BundleWrapper addParam(Integer param) {
        mBundle.putInt(INTEGER + mPosition++, param);
        return this;
    }

    // **************** get методы ********************

    public Serializable getSerializable() {
        return mBundle.getSerializable(SERIALIZABLE + mPosition++);
    }

    public DBObject getDBObject() {
        long id = mBundle.getLong(DB_OBJECT_ID + mPosition++);
        if(id == NULL_SIGN)                                         // если объект имеет признак null
            return null;                                            // то возвращается null

        Class dbObjectClass;

        try {
            dbObjectClass = Class.forName(mBundle.getString(DB_OBJECT_CLASS));
        }
        catch (ClassNotFoundException e) {
            throw new Error(e);
        }

        //noinspection unchecked
        DBObject dbObject = DBObject.load(dbObjectClass, id);
        if(mBundle.getBoolean(DB_OBJECT_DEL))                       // если addParam объект создал, то getDBObject - удаляет
            dbObject.delete();

        return dbObject;
    }

    // если параметр не найден - возвращает 0L
    public Long getLong() {
        return mBundle.getLong(LONG + mPosition++);
    }

    // если параметр не найден - возвращает false
    public Boolean getBoolean() {
        return mBundle.getBoolean(BOOLEAN + mPosition++);
    }

    // если параметр не найден - возвращает 0L
    public Integer getInteger() {
        return mBundle.getInt(INTEGER + mPosition++);
    }

    // **************** toBundle методы ********************

    public static Bundle toBundle(Serializable param) {
        return new BundleWrapper().addParam(param).mBundle;
    }

    public static Bundle toBundle(DBObject param) {
        return new BundleWrapper().addParam(param).mBundle;
    }

    public static Bundle toBundle(Long param) {
        return new BundleWrapper().addParam(param).mBundle;
    }

    public static Bundle toBundle(Boolean param) {
        return new BundleWrapper().addParam(param).mBundle;
    }

    public static Bundle toBundle(Integer param) {
        return new BundleWrapper().addParam(param).mBundle;
    }

    // **************** static get методы ********************

    public static Serializable getSerializable(Bundle bundle) {
        return new BundleWrapper(bundle).getSerializable();
    }

    public static DBObject getDBObject(Bundle bundle) {
        return new BundleWrapper(bundle).getDBObject();
    }

    public static Long getLong(Bundle bundle) {
        return new BundleWrapper(bundle).getLong();
    }

    public static Boolean getBoolean(Bundle bundle) {
        return new BundleWrapper(bundle).getBoolean();
    }

    public static Integer getInteger(Bundle bundle) {
        return new BundleWrapper(bundle).getInteger();
    }
}
