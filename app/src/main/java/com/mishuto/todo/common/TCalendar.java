package com.mishuto.todo.common;

import java.util.GregorianCalendar;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import static com.mishuto.todo.common.DateFormatWrapper.getTimeString;
import static com.mishuto.todo.common.ResourceConstants.Time.DT_FORMAT;
import static java.lang.Math.round;

/**
 * Класс, расширяющий функционал стандартного календаря
 * Created by Michael Shishkin on 10.12.2016.
 */

public class TCalendar extends GregorianCalendar {

    // Массив констант дней недели класса Calendar
    private static final int[] WEEK_DAYS = {SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY};

    public static final int FIRST_MONTH_DAY = 1;                // первый день месяца
    public static final int MAX_MONTH_DAY = 31;                 // последний день месяца для Григорианского календаря
    public static final int DAYS_IN_WEEK = WEEK_DAYS.length;    // количество дней в неделе
    public static final int MONTHS_IN_YEAR = 12;                // количество месяцев в году

    public TCalendar() {
        super();
    }

    public TCalendar(int year, int month, int dayOfMonth) {
        super(year, month, dayOfMonth);
    }

    public TCalendar(int year, int month, int dayOfMonth, int hourOfDay, int minute) {
        super(year, month, dayOfMonth, hourOfDay, minute);
    }

    public TCalendar(long ms) {
        setTimeInMillis(ms);
    }

    public static TCalendar now() {
        return new TCalendar();
    }

    /**
     * Возвращает первый день недели текущего месяца
     * @return день недели Calendar.SUNDAY ... Calendar.SATURDAY
     */
    public int getFirstDayOfWeekInMonth() {
        TCalendar calendar = clone();
        calendar.set(DAY_OF_MONTH, FIRST_MONTH_DAY);

        return calendar.get(DAY_OF_WEEK);
    }

    /**
     * Возвращает последний день недели текущего месяца
     * @return день недели Calendar.SUNDAY ... Calendar.SATURDAY
     */
    public int getLastDayOfWeekInMonth() {
        TCalendar calendar = clone();
        calendar.set(DAY_OF_MONTH, getActualMaximum(DAY_OF_MONTH));

        return calendar.get(DAY_OF_WEEK);
    }

    // устанавливает часы и минуты, не меняя дату
    public TCalendar setTimePart(int hour, int minute) {
        set(HOUR_OF_DAY, hour);
        set(MINUTE, minute);
        return this;
    }

    // устанавливает дату, не меняя время
    public void setDatePart(int year, int month, int dayOfMonth) {
        set(YEAR, year);
        set(MONTH, month);
        set(DAY_OF_MONTH, dayOfMonth);
    }

    @Override
    public String toString() {
        return DateFormatWrapper.getDateTimeString(this);
    }

    public String getFormattedDate() {
        return DateFormatWrapper.getDateString(this);
    }

    public String getFormattedTime() {
        return getTimeString(this);
    }

    public String getSpeakingDate() {
        return DateFormatWrapper.getSpeakingDate(this);
    }

    public String getSpeakingString() {
        return String.format(DT_FORMAT, getSpeakingDate(), DateFormatWrapper.getTimeString(this));
    }

    /**
     * Возвращает первый день недели для текущей локали
     * Возможно два варианта календаря:
     * Первый день воскресенье (США, Канада, Япония),
     * либо первый день понедельник - все остальные страны
     */
    public static boolean isFirstDaySunday() {
        return (new TCalendar()).getFirstDayOfWeek() == SUNDAY;
    }

    /**
     * Возвращает день недели по порядковому номеру с учетом текущей локали
     * @param ordinal - порядковый номер от 0 до 6
     * @return день недели SUNDAY ... SATURDAY или MONDAY ... SUNDAY
     */
    public static int getDayOfWeekByOrdinal(int ordinal) {
        if(ordinal < 0 || ordinal >= DAYS_IN_WEEK)
            throw new AssertionError ("Illegal index of the day of week: " + ordinal);

        if(isFirstDaySunday())
            return WEEK_DAYS[ordinal];
        else
            return WEEK_DAYS[(ordinal + 1) % DAYS_IN_WEEK ];

    }

    /**
     * Возвращает порядковый номер дня недели, с учетом текущей локали
     * @param dayOfWeek день недели SUNDAY ... SATURDAY (0..6)
     * @return порядковый номер. Например, для SUNDAY вернется 0 в США или 6 в Европе
     */
     //Код можно заметно упростить, если сделать допущение о том, что константы SUNDAY, MONDAY, ... - последовательные целые числа,
     //но для безопасности не делается никаких допущений
    public static int getOrdinalOfDayOfWeek(int dayOfWeek) {
        int ordinal;
        for(ordinal = 0; ordinal < DAYS_IN_WEEK; ordinal++)         // пробегаемся по массиву WEEK_DAYS, чтобы найти
            if(WEEK_DAYS[ordinal] == dayOfWeek) break;              // индекс входного дня недели

        if (ordinal == DAYS_IN_WEEK)                                // проверяем входной папаметр
            throw new AssertionError ("Illegal the day of week: " + dayOfWeek);

        if((new TCalendar()).getFirstDayOfWeek() == MONDAY)         // поправка на первый день недели
            ordinal = (ordinal - 1 + DAYS_IN_WEEK) % DAYS_IN_WEEK;  // если это понедельник, то индекс смещается назад на 1, а индекс SUNDAY становится 6

        return ordinal;
    }

    /**
     * Возвращает интервал между двумя датами по выбранному полю.
     * Для дат (года, месяцы и дни) вычисляется количество прошедших периодов без учета более мелких единиц
     * Например, TCalendar(2017, JANUARY, 1).intervalFrom(YEAR, TCalendar(2016, DECEMBER, 29) == 1 (1 год = 2017-2016)
     * Для часов, минут, секунд вычисляется количество полных периодов
     * Напрмер, TCalendar(2017, 1, 1, 12, 5).intervalFrom(HOUR, TCalendar(2017, 1, 1, 11, 59) == 0 (0 полных часов)
     * @param field принимет одно из значений: YEAR, MONTH, DATE, HOUR, MINUTE, SECOND
     * @param calendar2 вторая дата интервала, поле которой вычитается из первой
     * @return интервал this - calendar2 , в периодах field
     */
    public int intervalFrom(int field, TCalendar calendar2){

        int yearDiff = get(YEAR) - calendar2.get(YEAR);
        int secDiff = (int) ((getTimeInMillis() - calendar2.getTimeInMillis())/1000);

        switch (field) {
            case YEAR:
                return yearDiff;

            case MONTH:
                return yearDiff * 12 + (get(MONTH) - calendar2.get(MONTH));

            case DATE:  // вычисление дней между двумя датами, включая такие нюансы как таймзоны и летнее время
                int sign = this.compareTo(calendar2) < 0 ? -1 : 1;
                TCalendar early = sign == 1 ? calendar2.clone() : this.clone(); // минимальная дата
                TCalendar late = sign == -1 ? calendar2 : this;                 // максимальная дата
                int days = 0;
                while (early.get(YEAR) < late.get(YEAR)) {
                    days += early.getActualMaximum(DAY_OF_YEAR);                // если годы разные, суммируем количество дней в году в каждый год
                    early.add(YEAR, 1);
                }
                days += late.get(DAY_OF_YEAR) - early.get(DAY_OF_YEAR);         // суммируем разницу между порядковыми днями в году (для дат с одинаковыми годами)
                return days * sign;                                             // вычисляем знак

            case HOUR:
            case HOUR_OF_DAY:
                return secDiff/3600;

            case MINUTE:
                return secDiff/60;

            case SECOND:
                return secDiff;

            default:
                throw new AssertionError("undefined field");
        }
    }

    /**
     * Обнуляет все младшие поля. Пример
     * now().clipTo(MINUTE) == 15/09/2017 14:53:00.000
     * now().clipTo(DAY) == 15/09/2017 00:00:00.000
     * @param field Поле ДО которого идет округление
     * @return возвращает текущий объект
     */
    @SuppressFBWarnings("SF_SWITCH_FALLTHROUGH")
    public TCalendar clipTo(int field) {
        switch (field) {
            case DATE:
                set(HOUR_OF_DAY, 0);

            case HOUR_OF_DAY:
            case HOUR:
                set(MINUTE, 0);

            case MINUTE:
                set(SECOND, 0);

            case SECOND:
                set(MILLISECOND, 0);
                break;

            default:
                throw new AssertionError("unsupported field");
        }

        return this;
    }

    /**
     * Округлить до заданного значения минут.
     * Пример: пусть t = 01/01/17 22:13
     * t.roundTo(15) = 22:15
     * t.roundTo(60) = 22:00
     * t.roundTo(8 * 60) = 02/01/17 00:00
     *
     * @param minutes минимальное значение минут, до которого округляется дата
     * @return возвращает текущий объект
     */
    public TCalendar roundTo(int minutes) {
         long intervals = round((getTimeInMillis() + getTimeZone().getRawOffset()) / (minutes * 60 * 1000f));
         long ms = intervals * minutes * 60 * 1000;
         setTimeInMillis(ms - getTimeZone().getRawOffset());
         return this;
    }

    @Override
    public TCalendar clone() {
        return (TCalendar) super.clone();
    }
}
