package com.mishuto.todo.common;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.mishuto.todo.database.DBField;

import java.io.Serializable;

import static com.mishuto.todo.common.TCalendar.now;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;

/**
 * Класс для хранения времени суток
 * Применяется для различных периодических событий, для которых не зафиксирована конкретная дата.
 * Created by Michael Shishkin on 17.01.2018.
 */
public class Time implements Comparable<TCalendar>, DBField.StoredField<Time>, Serializable {
    private int hour;
    private int minute;

    private Time() {}

    public Time(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public Time(@NonNull TCalendar time) {
        hour = time.get(HOUR_OF_DAY);
        minute = time.get(MINUTE);
    }

    public Time(int time) {
        fromInteger(time);
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public void set(int h, int m) {
        hour = h;
        minute = m;
    }

    @Override
    public int compareTo(@NonNull TCalendar o) {
        return toInteger() - new Time(o).toInteger();
    }

    @Override
    public String toString() {
        return DateFormatWrapper.getTimeString(now().setTimePart(hour, minute));
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && (obj instanceof Time) && inMinutes() == ((Time) obj).inMinutes();
    }

    @Override
    public int hashCode() {
        return inMinutes();
    }

    @Override
    public void put(String columnName, ContentValues cv) {
        DBField.put(columnName, toInteger(), cv);
    }

    @Override
    public Time get(String columnName, Cursor cursor, Object parent) {
        fromInteger(DBField.getInteger(columnName, cursor));
        return this;
    }

    @Override
    public String asClauseParam() {
        return toInteger().toString();
    }

    public int inMinutes() {
        return hour * 60 + minute;
    }

    // при сохранении время преобразуется в число вида HHMM для удобства чтения человеком
    public Integer toInteger() {
        return hour * 100 + minute;
    }

    // преобразование числа HHMM в часы и минуты
    public void fromInteger(int t) {
        hour = t/100;
        minute = t % 100;
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    public Object clone() {
        return new Time(hour, minute);
    }
}
