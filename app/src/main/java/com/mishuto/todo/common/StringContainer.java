package com.mishuto.todo.common;

import android.support.annotation.NonNull;

import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.DBUtils;

import java.util.Collection;

/**
 * Сохраняемый в БД класс содержащий объект String.
 * Используется для возможности замены строки без изменения объекта, а так же для восстановления ссылок на объект после загрузки из БД
 *
 * Created by Michael Shishkin on 15.07.2017.
 */

public class StringContainer extends DBObject implements Comparable<StringContainer>
{
    private String mString;
    final static String STRING_DB_FIELD = DBUtils.getDBCompatName("mString");
    private Boolean mCaseSens = false;    // различать регистры при сравнении. По умолчанию - нет

    public StringContainer() {  // в дефолтном конструкторе создается пустая строка, т.к. ненулевой StringContainer
        mString = "";           // должен содержать ненулевой объект String, чтобы корректно отрабатывал hashCode и т.д.
    }

    public StringContainer(String string) {
        mString = string;
    }

    @SuppressWarnings("WeakerAccess")
    public boolean isCaseSens() {
        return mCaseSens;
    }

    public void setCaseSens(boolean caseSens) {
        mCaseSens = caseSens;
    }

    @Override
    public String toString() {
        return mString;
    }

    public void setString(String string) {
        mString = string;
        update();
    }

    @Override
    public int hashCode() {
        return casePurified().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof StringContainer && casePurified().equals(((StringContainer)obj).casePurified());
    }

    @Override
    public int compareTo(@NonNull StringContainer o) {
        return casePurified().compareTo(o.casePurified());
    }

    public boolean isEmpty() {
        return mString.trim().isEmpty();
    }

    public void trim() {
        mString = mString.trim();
        update();
    }

    // Конвертация коллекции StringContainer в коллекцию String
    public static <T extends Collection<String>> T getStringCollection(Collection<StringContainer> src, T dst) {
        dst.clear();
        for(StringContainer stringContainer : src)
            dst.add(stringContainer.toString());

        return dst;
    }

    // отдает строку с учетом режима сравнения регистров
    @NonNull
    private String casePurified() {
        TSystem.approveThat(mString != null);
        return isCaseSens() ? mString : mString.toLowerCase();
    }
}
