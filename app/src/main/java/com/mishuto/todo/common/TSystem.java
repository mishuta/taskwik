package com.mishuto.todo.common;

import android.content.ContentResolver;
import android.net.Uri;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.Locale;

/**
 * Врапер для API-шных методов
 * Created by Michael Shishkin on 22.01.2017.
 */

@SuppressWarnings("ConstantConditions")
public class TSystem {

    public static void approveThat(boolean condition) {
        approveThat(condition, null);
    }

    public static void approveThat(boolean condition, String msg) {
        if(!condition)
            throw new AssertionError(msg);
    }

    // вывод строки в Debug Log
    public static void debug(Object o, String msg){
        debug(o.getClass().getSimpleName(), msg);
    }
    public static void debug(String prefix, String msg){
        TLog.getInstance().log(TLog.LEVEL.DEBUG, prefix, msg);
    }

    // вывод строки в Error Log
    public static void error(Object o, String msg){
        TLog.getInstance().log(TLog.LEVEL.ERR, o.getClass().getSimpleName(), msg);
    }

    // копирование файла в пользовательской области src->dst
    // dst может быть файлом или каталогом
    public static boolean copyFile(File src, File dst) {
        if(!src.exists())
            return false;

        if(dst.isDirectory())
            dst = new File(dst, src.getName());

        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
            outputChannel = new FileOutputStream(dst).getChannel();
            inputChannel = new FileInputStream(src).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
        }
        catch (IOException e) {
            error(null, "copyFile: " + e.getMessage());
            return false;
        }

        finally {
                try {
                    if (inputChannel != null) inputChannel.close();
                    if (outputChannel != null) outputChannel.close();
                }
                catch (IOException e) {
                    error(null, "Channel closing error: " + e.getMessage());
                }
            }
        return true;
    }

    // загрузка файла из uri
    public static boolean loadFile(Uri uri, ContentResolver contentResolver,  File dst) {
        OutputStream output = null;
        InputStream input = null;

        try {
            input = contentResolver.openInputStream(uri);
            output = new FileOutputStream(dst);
            byte[] buffer = new byte[128 * 1024];   // 128kb buffer
            int read;

            while ((read = input.read(buffer)) != -1) {
                output.write(buffer, 0, read);
            }

            output.flush();
        }
        catch (IOException e) {
            error(null, e.getMessage());
            return false;
        }
        finally {
            try {
                if(input != null) input.close();
                if(output != null) output.close();
            }
            catch (IOException e) {
                error(null, e.getMessage());
            }
        }
        return true;
    }

    // возвращает код текущей локали
    public static String getLocaleCode() {
        String code = Locale.getDefault().getLanguage();

        if(code.equals(new Locale("ru").getLanguage()))
            return "ru";

        // сравнение с другими локалями

        else
            return "en"; // дефолтовая локаль
    }

}
