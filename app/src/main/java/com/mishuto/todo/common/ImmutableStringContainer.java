package com.mishuto.todo.common;

import com.mishuto.todo.database.SQLHelper;
import com.mishuto.todo.database.SQLTable;

import java.util.HashMap;

import static com.mishuto.todo.database.SQLHelper.whereString;

/**
 * Класс, содержащий неизменяемые объекты типа StringContainer
 * Гарантирует уникальность и неизменяемость объектов в БД
 * Используется для специальных значений StringContainer, которые всегда должны читаться в один объект
 * Created by Michael Shishkin on 19.10.2017.
 */

public class ImmutableStringContainer extends StringContainer {

    // Мапа временных объектов ImmutableStringContainer (созданных в текущем сеансе и, возможно, не сохраненных в БД)
    private static HashMap<String, ImmutableStringContainer> sTransientObjects = new HashMap<>();

    // Конструктор приватный. Объекты должны создаваться в методе getObject()
    private ImmutableStringContainer(String key) {
        super(key);
    }

    private ImmutableStringContainer() {} // обязательный дефолтный конструктор для DBObject - не используется

    /**
     * Метод загружает существующий объект с данной строкой, либо создает новый, если такого объекта еще не существует
     * @param key Строка, соответствующая объекту
     * @return Существующий или созданный объект
     */
    public static ImmutableStringContainer getObject(String key) {
        if(sTransientObjects.containsKey(key))  // сначала ищем объект в мапе. Если найден, то возвращаем
            return sTransientObjects.get(key);

        // Если в мапе не найден, то ищем в БД
        ImmutableStringContainer isc = new ImmutableStringContainer("");
        SQLTable sqlTable = isc.getSQLTable();

        SQLHelper.DBCursor cursor = sqlTable.query( whereString(STRING_DB_FIELD + "=?", key));
        if(cursor.getCount() == 1) {            // Если найден - загружаем существующий
            long id = cursor.getLong(cursor.getColumnIndex(SQLTable.ID));
            isc = load(ImmutableStringContainer.class, id);
        }
        else if(cursor.getCount() == 0) {       // Если в БД не найден, то создаем
            isc = new ImmutableStringContainer(key);
            sTransientObjects.put(key, isc);
        }

        else                                    // Если было найдено более одного объекта, тогда бросаем эксепшн
            throw new AssertionError("non-unique value in table: " + sqlTable.getTableName());

        cursor.close();
        return isc;
    }

    // добавление индекса в таблицу при ее первом создании
    @Override
    protected SQLTable.Column[] getColumns() {
        SQLTable.Column[] columns = super.getColumns();
        for (SQLTable.Column column : columns)
            if(column.getName().equals(STRING_DB_FIELD))
                column.setIndexed(true);

        return columns;
    }

    // запрет попытки изменения объекта
    @Override
    public void setString(String string) {
        throw new UnsupportedOperationException("Trying to change immutable object");
    }
}
