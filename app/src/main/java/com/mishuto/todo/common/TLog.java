package com.mishuto.todo.common;

import com.mishuto.todo.App;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Класс для вывода в стандартный лог.
 * Поддерживается вывод в файл taskwik.log, если задан соответствующий флаг isLogSaved в SharedPreference
 * Файл лога перезаписывается при каждом старте программы.
 * Если программа закончилась эксепшеном, записанный лог сохраняется в отдельном файле taskwik1.log, потом в taskwik2.log и т.д.
 * При достижении N максимального значения, taskwik1.log перезаписывается, потом taskwik2.log и т.д.
 * Created by Michael Shishkin on 14.09.2018.
 */
public class TLog {
    private final static String TAG = "TASKWIK";            // тэг вывода в стандартный лог
    final static String LOG_FILE_NAME = "taskwik";          // имя файла лога без расширения
    public final static String SHARED_FILE = "Log";         // имя xml-файла с настройками
    public final static String IS_LOG_SAVED = "isLogSaved"; // имя настройки
    final static int MAX_LOG_NUM = 5;                       // максимальное количество отдельных файлов лога

    private final static TLog sInstance = new TLog();

    private boolean isLogSaved;                             // флаг - сохранять ли лог в файл
    private File mLogFile;                                  // файл лога
    private FileWriter mFileWriter;                         // поток вывода в файл
    private PersistentValues.PVInt mCurrentLogNo;           // номер текущего отдельного файла лога


    private TLog() {
        PersistentValues pv = PersistentValues.getFile(SHARED_FILE);
        isLogSaved = pv.getBoolean(IS_LOG_SAVED, false).get();

        if(isLogSaved) {
            mLogFile = new File(App.getAppContext().getFilesDir(), LOG_FILE_NAME + ".log");
            mCurrentLogNo = pv.getInt("currentLogNo", 0);

            try {
                mFileWriter = new FileWriter(mLogFile, false);  // при открытии файла лога, файл перезаписывается, а не добавляется
            }
            catch (IOException e) {
                throw new Error(e);
            }
        }
    }

    // возможные уровни логирования
    public enum LEVEL {INFO, DEBUG, WARN, ERR }

    public static TLog getInstance() {
        return sInstance;
    }

    // сохранение сообщения в лог
    synchronized public void log(LEVEL level, String prefix, String msg) {

        String logMsg = "[" + prefix + "] " + msg;

        switch (level) {
            case ERR:
                android.util.Log.e(TAG, logMsg);
                break;

            default:
                android.util.Log.d(TAG, logMsg);

        }

        if(isLogSaved)
            writeLogLn(logMsg);
    }

    // сохранение текущего файлоа лога в отдельный файл лога
    void saveLog() {
        if(isLogSaved)
            try {
                mFileWriter.close();

                if (mCurrentLogNo.increment() > MAX_LOG_NUM)
                    mCurrentLogNo.set(1);

                File savedLogFile = new File(App.getAppContext().getFilesDir(), LOG_FILE_NAME + mCurrentLogNo.get() + ".log");
                TSystem.copyFile(mLogFile, savedLogFile);

                mFileWriter = new FileWriter(mLogFile, false);
            }
            catch (IOException e) {
                throw new Error(e);
            }
    }

    // сохранение эксепшена в файл лога и сохранение файла лога в отдельном файле
    public void logUnhandledException(Throwable throwable) {
        if(isLogSaved) {
            PrintWriter printWriter = new PrintWriter(mFileWriter);
            writeLogLn(throwable.toString());
            throwable.printStackTrace(printWriter);
            saveLog();
        }
    }

    // запись отдельной строчки лога в файл
    private void writeLogLn(String line) {
        String time = DateFormatWrapper.getFormattedDate("yyyy-MM-dd HH:mm:ss.SSS ", TCalendar.now());
        try {
            mFileWriter.append(time).append(line).append("\n"); // к сообщению добавляется метка времени и перевод строки
        }
        catch (IOException e) {
            throw new Error(e);
        }
    }
}
