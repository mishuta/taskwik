package com.mishuto.todo.common;

import com.mishuto.todo.App;
import com.mishuto.todo.database.DBOpenHelper;

import java.io.File;
import java.util.Calendar;

import static com.mishuto.todo.common.TSystem.copyFile;

/**
 * Класс для управления бэкапированием.
 * Автосоздание бэкапа:
 * Если загрузка успешна, БД сохраняется как бэкап копия (если бэкап не существовал или с момента бэкапа прошло больше чем BACKUP_MIN_INTERVAL сек).
 * Если неуспешна, база восстанавливается из бэкапа.
 * Успешность определяется по вызову releaseBackup()
 * Бэкап сохраняется при вызове функций Очистить, Восстановить, Импортировать пользователем
 * Created by Michael Shishkin on 03.07.2018.
 */
public class BackupManager {
    private final static int MAX_ATTEMPTS = 2;                  // количество неудачных попыток загрузок до восстановления из бэкапа
    private final static int BACKUP_MIN_INTERVAL = 2 * 60 * 60; // минимальное время в сек, которое должно пройти между двумя бэкапами

    public final static String BACKUP_NAME = DBOpenHelper.DB_NAME + ".bak";         // файл бэкапа
    private final static String BACKUP_TMP_NAME = DBOpenHelper.DB_NAME + ".tmp";    // временный файл бэкапа (для свопа файлов)
    private final static BackupManager INSTANCE = new BackupManager();

    private PersistentValues.PVInt mAttempts;           // количество неуспешных попыток загрузиться
    private PersistentValues.PVCalendar mBackupDate;    // время последнего бэкапа
    private PersistentValues.PVBoolean isBackupExists;  // флаг наличия бэкапа

    public BackupManager() {
        final PersistentValues values = PersistentValues.getFile("BackupManager");
        mAttempts = values.getInt("Attempts", 0);
        mBackupDate = values.getCalendar("BackupDate", TCalendar.now());
        isBackupExists = values.getBoolean("BackupExists", false);
    }

    public static BackupManager getInstance() {
        return INSTANCE;
    }

    // Вызывается при старте, увеличивает счетчик стартов без releaseBackup
    public void onStart() {
        mAttempts.increment();
    }

    // Вызывается после успешной загрузки и готовности к работе.
    // Сбрасывает счетчик, создает бэкап при необходимости
    public void releaseBackup() {
        resetAttemptsCounter();
        if(!isBackupExists() || TCalendar.now().intervalFrom(Calendar.SECOND, getBackupDate()) > BACKUP_MIN_INTERVAL)
            makeBackUp();
    }

    // обнаружен сбой
    public boolean isFailureDetected() {
        return mAttempts.get() >= MAX_ATTEMPTS;
    }

    // существует бэкап.
    // Т.к. бэкап всегда создается при отсутствии и никогда не удаляется - должен быть всегда true
    public boolean isBackupExists() {
        return isBackupExists.get();
    }

    // дата последнего бэкапа
    public TCalendar getBackupDate() {
        return mBackupDate.get();
    }

    // удаление файла БД
    public boolean deleteDB() {
        closeDB();
        resetAttemptsCounter();
        return App.getAppContext().deleteDatabase(DBOpenHelper.DB_NAME);
    }

    // копирование бэкап-файла на место основного файла
    public boolean restoreBackup() {
        closeDB();
        File db = getDB();
        File backup = getBackup();
        if(copyFile(backup, db)) {
            mAttempts.set(0);
            isBackupExists.set(false);
            return true;
        }
        else
            return false;
    }

    // создание бэкапа
    public boolean makeBackUp() {
        try {
            closeDB();
        }
        catch (DBOpenHelper.DBError error) {
            TSystem.error(this, error.getMessage() + "\nBackup not performed.");
            return false;
        }

        File db = getDB();
        File backup = new File(db.getParent(), BACKUP_NAME);
        if(copyFile(db, backup)) {
            isBackupExists.set(true);
            mBackupDate.set(TCalendar.now());
            return true;
        }
        else
            return false;
    }

    //копирование файла бэкапа на место основоной базы, а базы - на место бэкапа
    public boolean swapBackup() {
        if(!isBackupExists())
            return false;

        File backupFile = getBackup();
        File tmp = new File(backupFile.getParent(), BACKUP_TMP_NAME);
        boolean res = false;

        if(copyFile(backupFile, tmp))       // 1. сохраняем в tmp существующий бэкап

            if(makeBackUp())                // 2. сохраняем текущее состояние базы в бэкапе
                if(copyFile(tmp, getDB()))  // 3. копируем сохраненный в tmp бэкап в текущий файл базы
                    res = true;

        //noinspection ResultOfMethodCallIgnored
        tmp.delete();                   // 4. удаляем tmp
        return res;
    }

    // перезаписать базу новым файлом (с предварительным бэкапом старого)
    public boolean importDB(File newDb) {
        return makeBackUp() && copyFile(newDb, getDB());
    }

    // сохранить файл базы в другой файл (папку)
    public boolean exportDB(File newDB) {
        closeDB();
        return copyFile(getDB(), newDB);
    }

    // сброс счетчика неуспешных загрузок
    public void resetAttemptsCounter() {
        mAttempts.set(0);
    }

    public static File getDB() {
        return App.getAppContext().getDatabasePath(DBOpenHelper.DB_NAME);
    }

    public static File getBackup() {
        return App.getAppContext().getDatabasePath(BACKUP_NAME);
    }

    // закрытие базы перед копированием имеет две цели:
    // во-первых, чтобы гарантированно сбросить кеш базы в файл перед его копированием,
    // во-вторых, чтобы следующее обращение к базе было гарантированно из нового файла (если файл базы подменяется)
    private void closeDB() {
        DBOpenHelper.getInstance().close();
    }
}
