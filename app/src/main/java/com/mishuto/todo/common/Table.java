package com.mishuto.todo.common;

import android.support.annotation.NonNull;
import android.util.SparseArray;

import java.util.Iterator;

/**
 * Класс Таблица объектов на базе SparseArray
 * Аналогия таблицы БД. Представляет собой расширяемый массив пар id:value, где id - автоинкрементное целочисленное целое.
 * Используется для случаев, когда нужно хранить массив объектов переменной длинны с досупом к объектам по id
 * Поскольку id автоикрементное, нет опасений коллизий. id нового объекта никогда не совпадет с id удаленного ранее.
 * Created by Michael Shishkin on 24.12.2017.
 */

public class Table<T> implements Iterable<T> {
    private SparseArray<T> mArray = new SparseArray<>();
    private int id = 0;

    /**
     * сохраняет объект в массиве
     * @param element сохраняемый объект
     * @return id объекта
     */
    public int put(T element) {
        mArray.append(id, element);
        return id++;
    }

    /**
     * извлекает объект из массива (объект из массива удаляется)
     * @param id id объекта
     * @return извлекаемый объект
     */
    public T pull(int id) {
        T element = get(id);
        remove(id);
        return element;
    }

    /**
     * получает объект из массива (объект остается в массиве)
     * @param id id объекта
     * @return извлекаемый объект
     */
    public T get(int id) {
        return mArray.get(id);
    }

    /**
     * удаляет объект из массива. Работает в 2 раза быстрее чем pull
     * @param id id объекта
     */
    public void remove(int id) {
        mArray.delete(id);
    }

    /**
     * получить размер массива
     * @return размер массива
     */
    public int size() {
        return mArray.size();
    }

    /**
     * найти id объекта в массиве
     * операция имеет сложность O(n), поиск осуществляется по совпадению ссылок ( element == element(i) )
     * @param element объект, для которого требуется найти id
     * @return id объекта или -1, если объект не найден
     */
    public int findId(T element) {
        int index = mArray.indexOfValue(element);
        return index == -1 ? -1 : mArray.keyAt(index);
    }

    /**
     * получить итератор таблицы
     * @return итератор таблицы
     */
    @NonNull
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int index = 0;

            @Override
            public boolean hasNext() {
                return index < size();
            }

            @Override
            public T next() {
                int id =  mArray.keyAt(index++);
                return get(id);
            }
        };
    }
}
