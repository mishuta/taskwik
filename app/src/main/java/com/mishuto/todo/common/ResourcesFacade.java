package com.mishuto.todo.common;

import android.content.res.TypedArray;
import android.util.DisplayMetrics;

import static com.mishuto.todo.App.getAppContext;
import static com.mishuto.todo.view_model.common.AbstractActivity.getContext;
import static java.lang.Math.round;


/**
 * Набор статических методов для использования ресурсов (в т.ч. из модели), для того чтобы отвязать модель от платформо-зависимых ресурсов
 *
 * Created by Michael Shishkin on 26.11.2016.
 */
@SuppressWarnings("WeakerAccess")
public class ResourcesFacade {

    // Возращает строковый массив из ресурса
    public static String[] getStringArray (int id) {
        return getAppContext().getResources().getStringArray(id);
    }

    // Возвращает массив id изображенй из массива ресурсов
    public static int[] getResourceIdsArray(int arrayId) {
        TypedArray typedArray = getContext().getResources().obtainTypedArray(arrayId);
        int[] array = new int[typedArray.length()];

        for(int i = 0; i < array.length; i++)
            array[i] = typedArray.getResourceId(i, 0);

        typedArray.recycle();

        return  array;
    }

    // Запрос строкового ресурса
    public static String getString(int id) {
        return getAppContext().getResources().getString(id);
    }

    // получение размера ресурса в px
    public static int getDimensionPix(int id) {
        return getAppContext().getResources().getDimensionPixelSize(id);
    }

    // конвертация размера из px в dp
    public static int pxToDp(int px) {
        DisplayMetrics displayMetrics = getAppContext().getResources().getDisplayMetrics();
        return round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    // конвертация dp в px
    // Обратить внимание: есть подозрение что требуется применить к результату эмпирический коэффициент 1.1
    public static int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getAppContext().getResources().getDisplayMetrics();
        return round(dp * displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    // Запрос массива типа int[] из ресурсов
    public static  int[] getIntArray (int id) {
        return getAppContext().getResources().getIntArray(id);
    }

    // Запрос массива типа Integer[] из ресурсов
    public static Integer[] getIntegerArray (int id) {
        int [] dataIn = getIntArray(id);
        Integer[] dataOut = new Integer[dataIn.length];

        for (int i = 0; i < dataIn.length ; i++)
            dataOut[i] = dataIn[i];

        return dataOut;
    }
}
