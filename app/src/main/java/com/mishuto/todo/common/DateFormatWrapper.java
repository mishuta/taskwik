package com.mishuto.todo.common;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.text.format.DateFormat;

import com.mishuto.todo.App;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static com.mishuto.todo.App.getAppContext;
import static com.mishuto.todo.common.ResourceConstants.Time.DT_FORMAT;
import static com.mishuto.todo.common.ResourceConstants.Time.TODAY;
import static com.mishuto.todo.common.ResourceConstants.Time.TOMORROW;
import static com.mishuto.todo.common.ResourceConstants.Time.YESTERDAY;
import static com.mishuto.todo.common.TCalendar.now;
import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

/**
 * Класс содержит вспомогательные статические методы для форматирования даты и времени
 * Created by Michael Shishkin on 19.11.2016.
 */

public final class DateFormatWrapper {

    // Формат даты
    public static String getDateString(GregorianCalendar calendar) {
        return DateFormat.getDateFormat(getAppContext()).format(calendar.getTime());
    }

    // Возращает дату в заданном формате
    public static String getFormattedDate(String format, TCalendar calendar) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);   // используется SimpleDateFormat, т.к. DateFormat не распознает SSS
        return simpleDateFormat.format(calendar.getTime());
    }

    // Формат времени
    public static String getTimeString(GregorianCalendar calendar) {
        return getTimeFormat().format(calendar.getTime());
    }

    // Формат даты и времени
    static String getDateTimeString(GregorianCalendar calendar){
        return String.format(DT_FORMAT, getDateString(calendar), getTimeString(calendar));
    }

    public static java.text.DateFormat getTimeFormat() {
        return DateFormat.getTimeFormat(getAppContext());
    }

    private static final String FS_DOW_SHORT = "EE";    //короткий формат дня недели
    private static final String FS_DOW_LONG = "EEEE";   //длинный формат дня недели
    private static final String FS_MONTH_LONG = "MMMM"; //формат месяца
    private static final String FS_MONTH_SHORT = "MMM";

    /**
     * Название дня недели
     * @param dayOfWeek идентификатор дня недели Calendar.SUNDAY ... Calendar.SATURDAY
     * @param isShort короткое SU или длинное SUNDAY название
     * @return название
     */
    @NonNull
    public static String getDayOfWeekName(int dayOfWeek, boolean isShort) {
        return getFieldFormat(Calendar.DAY_OF_WEEK, dayOfWeek, isShort ? FS_DOW_SHORT : FS_DOW_LONG);
    }

    // Название месяца
    @NonNull
    public static String getMonthName(int month, boolean isShort) {
        final int SHORT_LEN = 3;    // максимальная длина названия месяца, до которой надо обрезать
        String name = getFieldFormat(Calendar.MONTH, month, isShort ? FS_MONTH_SHORT : FS_MONTH_LONG);
        if(isShort && name.length() > SHORT_LEN )   // в некоторых языках месяц может быть короче 3-х букв
            name = name.substring(0, SHORT_LEN);    // короткий дополнительно обрезаем до 3-х букв

        return name;
    }

    // Разговорное обозначение дат
    public static String getSpeakingDate(TCalendar date) {

        int days = date.intervalFrom(DATE, now());

        if(days == -1 )
            return YESTERDAY;

        if(days == 0 )
            return TODAY;

        if(days == 1)
            return TOMORROW;

        if(days >= 2 && days <= 6)
            return TextUtils.capitalize(getDayOfWeekName(date.get(Calendar.DAY_OF_WEEK), false));

        return getDateString(date);
    }

    public static int getFieldPosition(int field) {
        char[] fields = DateFormat.getDateFormatOrder(App.getAppContext());
        char f;

        switch (field) {
            case DATE: f = 'd'; break;
            case MONTH: f = 'M'; break;
            case YEAR: f = 'y'; break;
            default: throw new AssertionError("unexpected field");
        }

        for (int i = 0; i < fields.length; i++)
            if(f==fields[i])
                return i;

        throw new AssertionError("unexpected position");
    }

    // получить текстовое значение поля в заданном формате
    @NonNull
    private static String getFieldFormat(int field, int dateValue, String format) {
        TCalendar calendar = now();
        calendar.set(Calendar.DAY_OF_MONTH, 1); // устанавливаем день месяца в 1, чтобы при получении значения месяца в месяце не оказалось больше дней чем он может иметь
        calendar.set(field, dateValue);
        return getFormattedDate(format, calendar);
    }
}
