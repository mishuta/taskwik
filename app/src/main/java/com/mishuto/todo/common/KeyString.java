package com.mishuto.todo.common;

/**
 * Класс, содержащий пару: целый ключ/строка - значение.
 * Используется в спинерах для представления элемента спинера
 * Created by Michael Shishkin on 06.04.2017.
 */

public class KeyString  {
    private Integer key;    //ключевое поле
    private String title;   //текстовое поле

    public KeyString(Integer key, String title) {
        this.key = key;
        this.title = title;
    }

    public String toString() {
        return title;
    }

    @SuppressWarnings("WeakerAccess")
    public Integer getKey() {
        return key;
    }

    public void set(Integer key, String title) {
        this.key = key;
        this.title = title;
    }

    // Переопределение hashCode/equals для корректной обработки в коллекциях [спинера]
    @Override
    public int hashCode() {
        return super.hashCode() + 0x100 * title.hashCode() + 0x10000 * key.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof  KeyString))
            return false;

        KeyString bro = (KeyString) obj;
        return bro.key.equals(key) && bro.title.equals(title);
    }
}
