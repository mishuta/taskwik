package com.mishuto.todo.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.mishuto.todo.App;

import java.util.HashMap;

/**
 * Сохраняемые переменные, базирующиеся на SharedPreferences
 * Created by Michael Shishkin on 06.08.2018.
 */
public class PersistentValues {
    private static final int PV_VERSION = 10004;                                // текущая версия приложения

    private SharedPreferences mSharedPreferences;
    private static HashMap<String, PersistentValues> sPVMap = new HashMap<>();  // мапа всех объектов PersistentValues, для получения одного объекта для разлых запросов с одним ключом
    private HashMap<String, TypeValue> mTypeValueMap = new HashMap<>();         // мапа для всех переменных внутри объекта PersistentValues

    // при инициализации класса выполняем миграцию SharedPreferences
    static {
        PVInt version = getFile("PVMigrator").getInt("version", 1); // получаем сохраненную весию PV
        if (version.get() != PV_VERSION) {                          // если она отличается от текущей
            int oldVersion = version.get();
            TSystem.approveThat(oldVersion < PV_VERSION);
            version.set(PV_VERSION);                                // сохраняем новую версию
            doMigrate(oldVersion);                                  // выполняем миграцию со старой
        }
    }

    //создание/получение объекта PersitantValues
    public static PersistentValues getFile(String key) {
        if(!sPVMap.containsKey(key))
            sPVMap.put(key, new PersistentValues(key));

        return sPVMap.get(key);
    }

    private PersistentValues(String file) {
        mSharedPreferences = App.getAppContext().getSharedPreferences(file, Context.MODE_PRIVATE);
    }

    // геттеры для создания и получения персистентных объектов

    public PVBoolean getBoolean(String key, boolean def) {
        if(!mTypeValueMap.containsKey(key))
            mTypeValueMap.put(key, new PVBoolean(key, def));

        return (PVBoolean) mTypeValueMap.get(key);
    }

    public PVInt getInt(String key, int def) {
        if(!mTypeValueMap.containsKey(key))
            mTypeValueMap.put(key, new PVInt(key, def));

        return (PVInt) mTypeValueMap.get(key);
    }

    public PVCalendar getCalendar(String key, TCalendar def) {
        if(!mTypeValueMap.containsKey(key))
            mTypeValueMap.put(key, new PVCalendar(key, def));

        return (PVCalendar) mTypeValueMap.get(key);
    }

    // Базовый класс всех персистентных типов
    abstract class TypeValue<T> implements Accessor<T> {
        String mKey;
        T mValue;

        TypeValue(String key, T defValue) {
            mKey = key;
            mValue = read(defValue);
        }

        abstract T read(T defValue);
        abstract void write(T value, SharedPreferences.Editor editor);

        @Override
        public T get() {
            return mValue;
        }

        @Override
        public void set(T value) {
            if(!mValue.equals(value) || !mSharedPreferences.contains(mKey)) {
                mValue = value;
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                write(value, editor);
                editor.apply();
            }
        }
    }

    public class PVBoolean extends TypeValue<Boolean> {

        PVBoolean(String name, boolean defVal) {
            super(name, defVal);
        }

        @Override
        Boolean read(Boolean defValue) {
            return mSharedPreferences.getBoolean(mKey, defValue);
        }

        @Override
        void write(Boolean value, SharedPreferences.Editor editor) {
            editor.putBoolean(mKey, value);
        }

        public boolean flip() {
            set(!mValue);
            return mValue;
        }
    }

    public class PVInt extends TypeValue<Integer> {
        PVInt(String name, int defVal) {
            super(name, defVal);
        }

        @Override
        Integer read(Integer defValue) {
            return mSharedPreferences.getInt(mKey, defValue);
        }

        @Override
        void write(Integer value, SharedPreferences.Editor editor) {
            editor.putInt(mKey, value);
        }

        Integer increment() {
           set(mValue + 1);
           return mValue;
        }
    }

    class PVCalendar extends TypeValue<TCalendar> {
        PVCalendar(String name, TCalendar defValue) {
            super(name, defValue);
        }

        @Override
        TCalendar read(TCalendar defValue) {
            return new TCalendar(mSharedPreferences.getLong(mKey, 0));
        }

        @Override
        void write(TCalendar value, SharedPreferences.Editor editor) {
            editor.putLong(mKey, mValue.getTimeInMillis());
        }
    }

    // миграция PV на новые версии
    //todo странный код. Надо вынести из PV связь с классом WelcomeActivity
    private static void doMigrate(int oldVersion) {
        if(oldVersion < 10004) {
            if(getFile("App").getBoolean("loaded", false).get())    // различаем приложения, установленные до 1.0.4 (и потому не имеющие версию), от новых приложений, установленных после 1.0.4
                PersistentValues.getFile("WelcomeActivity").getBoolean("showed", false).set(true);
            else
                oldVersion = PV_VERSION;                            // если приложение новое  - то его версия = текущей
        }
    }
}
