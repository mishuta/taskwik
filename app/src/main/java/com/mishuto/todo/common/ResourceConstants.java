package com.mishuto.todo.common;

import com.mishuto.todo.todo.R;

import static com.mishuto.todo.common.ResourcesFacade.getString;
import static com.mishuto.todo.common.ResourcesFacade.getStringArray;

/**
 * Создание объектов из ресурсов
 * Класс позволяет отвязать модель от платформо-зависимых ресурсов
 *
 * Created by Michael Shishkin on 26.11.2016.
 */

public interface ResourceConstants {

    interface Validity {                                                // Коды валидации текстовых форм:
        int OK = 0;                                                     // успешно
        int ERR_EMPTY = R.string.error_fieldIsEmpty;                    // пустая строка
        int ERR_EXISTS = R.string.error_exists;                         // строка существует
    }

    String[] PRIORITY_TITLES = getStringArray(R.array.priority_text);   // названия приоритетов

    String TITLE_NOT_FOUND = getString(R.string.taskTitleNotDefined);   // название задачи не задано

    String[] START_CONTEXTS = getStringArray(R.array.start_contexts);   // набор стартовых контекстов

    interface Recurrence {
        // названия страниц Повторения для спинера
        String[] PAGES = ResourcesFacade.getStringArray(R.array.recurrence_pages);

        // сообщение, отображаемое при расчете следующего события, если корректный срок не выбран
        String NOT_SET = getString(R.string.recurrence_not_set);

        // массив  кратности часов
        String[] HOURLY_FACTOR_ARRAY = getStringArray(R.array.rec_day_hoursFoldTitles);

        // массив  кратности дней
        String[] DAILY_FACTOR_ITEMS = getStringArray(R.array.day_factor_item);
        String[] DAILY_FACTOR_VALUES = getStringArray(R.array.day_factor_value);

        // массив кратности недель
        String[] WEEKLY_FACTOR_ITEMS = getStringArray(R.array.week_factor_item);
        String[] WEEKLY_FACTOR_VALUES = getStringArray(R.array.week_factor_value);

        // массив номеров недель месяца
        String[] NUM_WEEKS_ARRAY = getStringArray(R.array.rec_numWeek);

        // массив кратности месяцев
        String[] MONTHLY_FACTOR_ITEMS = getStringArray(R.array.month_factor_item);
        String[] MONTHLY_FACTOR_VALUES = getStringArray(R.array.month_factor_value);

        // массив кратности лет
        String[] YEARLY_FACTOR_ITEMS = getStringArray(R.array.year_factor_item);
        String[] YEARLY_FACTOR_VALUES = getStringArray(R.array.year_factor_value);
    }

    interface Time {
        String TODAY = ResourcesFacade.getString(R.string.today);
        String YESTERDAY = ResourcesFacade.getString(R.string.yesterday);
        String TOMORROW = ResourcesFacade.getString(R.string.tomorrow);
        String DT_FORMAT = ResourcesFacade.getString(R.string.dateTime);
    }

    interface Filters {
        String EMPTY_PROJECT_NAME = ResourcesFacade.getString(R.string.no_project);
        String EMPTY_EXECUTOR_NAME = ResourcesFacade.getString(R.string.no_executor);
        String EMPTY_CONTEXT_NAME = ResourcesFacade.getString(R.string.no_context);
        String EMPTY_DATE_NAME = ResourcesFacade.getString(R.string.no_date);

        String ACTIVATED_TASK_FILTER = ResourcesFacade.getString(R.string.activated_task_filter);
    }
}
