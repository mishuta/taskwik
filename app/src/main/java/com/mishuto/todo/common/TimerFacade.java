package com.mishuto.todo.common;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import com.mishuto.todo.App;
import com.mishuto.todo.model.TaskList;

import java.util.Locale;

import static android.app.PendingIntent.FLAG_NO_CREATE;
import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static com.mishuto.todo.common.TCalendar.now;
import static java.util.Calendar.MINUTE;

/**
 * Интерфейс работы с таймером
 * Таймер реализован на базе AlarmManager. Каждый объект TimerFacade представляет собой один таймер
 * Created by Michael Shishkin on 23.12.2017.
 */

public class TimerFacade {

    //Будильник. Реализуется клиентом для обработки срабатывания таймера
    public interface Alarm {
        void onFire(int timerId, TCalendar time);
    }

    private static final String TAG_TIMER = App.createTagName("TimerID");  //Тег для хранения идентификатора таймера
    private static final int NOT_SET = -1;
    private static final AlarmManager ALARM_MANAGER = (AlarmManager) App.getAppContext().getSystemService(Context.ALARM_SERVICE);

    private TCalendar mAlarmTime;                       // время срабатывания таймера (если установлено)
    private Alarm mAlarm;                               // ссылка на клиента, реализующего обработку будильника
    private int mTimerId = NOT_SET;                     // id таймера, возвращаемый AlarmReceiver, по которому можно идентифицировать объект TimerFacade

    public TCalendar getAlarmTime() {
        return mAlarmTime;
    }

    // (Пере)Установка таймера на конкретное время
    @SuppressWarnings("ConstantConditions")
    public int setTimer(Alarm alarm, TCalendar alarmTime) {
        if(alarmTime == null || alarmTime.compareTo(now().clipTo(MINUTE)) < 0)  // таймер не устанавливается, если время не задано или в прошлом,
            return mTimerId;                                                    // но установится если время совпадает с текущей минутой (чтобы можно было послать нотификацию после рестарта приложения)

        if(mTimerId == NOT_SET)                                                 // Если будильник не установлен, то создается, иначе переустанавливается с тем же id
            mTimerId = AlarmReceiver.addTimer(this);
        mAlarmTime = alarmTime;
        mAlarm = alarm;

        Intent intent = createIntent();
        intent.putExtra(TAG_TIMER, mTimerId);

        PendingIntent pendingIntent = getPendingIntent(intent, FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= 23)
            ALARM_MANAGER.setAlarmClock(new AlarmManager.AlarmClockInfo(alarmTime.getTimeInMillis(),pendingIntent),pendingIntent);
        else
            ALARM_MANAGER.setExact(AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(), pendingIntent);

        TSystem.debug(this, "Timer #" + mTimerId + " set to " + mAlarmTime.toString());
        return mTimerId;
    }

    // сбросить установленный таймер
    public void cancel() {
        if(mTimerId != NOT_SET)
            clearAlarm();
    }

    // генерация интента для Alarm Manager, который однозначно идентифицирует создаваемый или уже существующий таймер
    private Intent createIntent() {
        Intent intent = new Intent(App.getAppContext(), TimerFacade.AlarmReceiver.class);   // Получатель интента - класс AlarmReceiver
        intent.setData(Uri.parse(String.format(Locale.ENGLISH, "alarm:%d", mTimerId)));     // мутируем интент так, чтобы для каждого таймера существовал уникальный интент
        return intent;
    }

    private static PendingIntent getPendingIntent(Intent intent, int flags) {
        return PendingIntent.getBroadcast(App.getAppContext(), 1,  intent, flags);
    }

    // распаковка timerID из интента. Механизм получения timerID инкапсулирован в классе TimerFacade
    private static int getTimerId(Intent intent) {
        return intent.getIntExtra(TAG_TIMER, -1);
    }

    private void onFire() {
        if(mAlarmTime.compareTo(now()) <= 0) {  // Фильтруем некорректные срабатывания, возникающие после пересоздания приложения
            int id = mTimerId;                  // сохраняем id и время
            TCalendar time = mAlarmTime;
            clearAlarm();                       // очищаем объект перед вызовом onFire, чтобы можно было создать новый таймер в обработчике onFire
            TSystem.debug(this, "Timer #" + id + " fired");
            mAlarm.onFire(id, time);
        }
    }

    //удаляем таймер в AlarmManager и PendingItem(если есть), удаляем объект из таблицы AlarmReceiver, сбрасываем TimerId
    private void clearAlarm() {
        PendingIntent pendingIntent = getPendingIntent(createIntent(), FLAG_NO_CREATE);
        if (pendingIntent != null) {
            //noinspection ConstantConditions
            ALARM_MANAGER.cancel(pendingIntent);
            pendingIntent.cancel();
        }

        AlarmReceiver.removeTimer(mTimerId);
        mTimerId = NOT_SET;
        mAlarmTime = null;
    }

    /**
     * Обработчик событий от AlarmService. Сервис создается системой по приходу соответствующего интента
     * AlarmReceiver хранит таблицу id таймеров с объектами TimerFacade, чтобы выстро найти нужный объект по приходу таймера
     * id таймера сохраняется в интенте.
     * В случае уничтожения приложения, таблица тоже уничтожается
     * По срабатыванию таймера, создается App и таймеры с таблицей пересоздаются. Но уже пришедшие события в ресивер содержат некорректные id и отфильтровываются
     */
    public static class AlarmReceiver extends BroadcastReceiver {

        private static Table<TimerFacade> sTimerTable = new Table<>();  // таблица таймеров, ожидающих срабатывания
        private static int addTimer(TimerFacade timer) {
            return sTimerTable.put(timer);
        }

        private static void removeTimer(int id) {
            sTimerTable.remove(id);
        }

        // обработка события срабатывания таймера от Alarm Service
        @Override
        public void onReceive(Context context, Intent intent) {
            //android.os.Debug.waitForDebugger();
            TaskList.get().load();                          // При получении onReceive, в случае если приложение было уничтожено, необходимо восстановить таблицу с таймерами.
                                                            // Она пересоздется при загрузке задач и создании таймеров
            int timerId = getTimerId(intent);               // вытаскиваем timerID из интента
            TimerFacade timer = sTimerTable.get(timerId);   // получаем объект TimerFacade по timerID
            if(timer != null)                               // Игнорируем некорректные id, возникающие после пересоздания приложения
                timer.onFire();                             // вызываем срабатывание таймера
        }
    }
}
