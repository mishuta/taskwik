package com.mishuto.todo.model;

import com.mishuto.todo.xml.XmlDoc;
import com.mishuto.todo.xml.XmlNode;
import com.mishuto.todo.xml.XmlNodeList;
import com.mishuto.todo.common.PersistentValues;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.todo.BuildConfig;
import com.mishuto.todo.todo.R;

import static java.lang.Integer.parseInt;

/**
 * Парсер для файла whatsnew.xml
 * Created by Michael Shishkin on 15.01.2019.
 */
public class ReleaseNotes {
    private final static Integer RELEASE_NOTES_FILE = R.raw.whatsnew;   // xml-файл со списком Release Notes. Может подменяться в тестах

    private final static String NAME = "feature";                       // теги файла
    private final static String CODE = "code_version";
    private final static String HOT = "hot";
    private final static String BUILD = "build_version";
    private final static String DESC = "description";
    private final static String TITLE = "title";
    private final static String LANG = "lang";

    private static ReleaseNotes sReleaseNotes;                          // для тестов требуется ленивая инициализация
    private final XmlNodeList XML_FEATURES =
            new XmlDoc(RELEASE_NOTES_FILE).getNodeList(NAME);           // список тегов

    private final static int CURRENT_VER = BuildConfig.VERSION_CODE;
    private final int mLastVersion;                                     // предыдущая сохраненная версия приложения

    public static ReleaseNotes getInstance() {
        if(sReleaseNotes == null)
            sReleaseNotes = new ReleaseNotes();
        return sReleaseNotes;
    }

    private ReleaseNotes() {
        PersistentValues.PVInt pvLastVersion = PersistentValues.getFile("Banner").getInt("lastVersion", CURRENT_VER);
        mLastVersion = pvLastVersion.get();    // сохраняем "последнюю версию"
        pvLastVersion.set(CURRENT_VER);        // записываем текущую версию в последнюю, чтобы при следующей загрузке баннер не отображался

    }

    private XmlNodeList.NodeMatcher newFeature(final int fromVersion) {
        return node -> new Feature(node).getCodeVersion() > fromVersion;
    }

    private XmlNodeList.NodeMatcher newHotFeature(final int fromVersion) {
        return node -> newFeature(fromVersion).match(node) &&  new Feature(node).isHot();
    }

    public int getLastVersion() {
        return mLastVersion;
    }

    public boolean areThereNewFeatures(int fromVersion) {
        return fromVersion < CURRENT_VER && XML_FEATURES.findFirst(newFeature(fromVersion)) != null;
    }

    public boolean areThereNewHotFeatures(int fromVersion) {
        return fromVersion < CURRENT_VER && XML_FEATURES.findFirst(newHotFeature(fromVersion)) != null;
    }

    public Iterable<Feature> getFeatures(int fromVersion) {
        return XML_FEATURES.getIterable(newFeature(fromVersion), Feature::new);
    }

    public static class  Feature {
        private final static String LOCAL = TSystem.getLocaleCode();

        XmlNode mNode;

        Feature(XmlNode node) {
            mNode = node;
        }

        @SuppressWarnings("WeakerAccess")
        public int getCodeVersion() {
            return parseInt(mNode.getAttrValue(CODE));
        }

        public String getBuildVersion() {
            return mNode.getAttrValue(BUILD);
        }

        public boolean isHot() {
            return mNode.getAttrValue(HOT).equals("true");
        }

        public String getDescription() {
            return getChildValue(DESC);
        }

        public String getTitle() {
            return getChildValue(TITLE);
        }

        private String getChildValue(String tag) {
            return mNode.getChildNodes().findFirst(node -> node.getName().equals(tag) && node.getAttrValue(LANG).equals(LOCAL)).getContent();
        }
    }
}
