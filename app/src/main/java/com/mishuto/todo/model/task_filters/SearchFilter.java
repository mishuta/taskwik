package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.model.Task;

import static com.mishuto.todo.common.TextUtils.contains;
import static com.mishuto.todo.common.TextUtils.empty;
import static com.mishuto.todo.common.TextUtils.isSubString;

/**
 * Фильтр по подстроке. Ищет вхождение подстроки во всех значимых атрибутах задачи
 * Created by Michael Shishkin on 19.11.2017.
 */

public class SearchFilter extends Exclusiveness.ExclusivenessImp implements Selectivity<String> {

    private String mQuery;

    @Override
    public boolean isTaskFiltered(Task task) {
        String query = mQuery;
        if(!empty(query))
            query = query.toLowerCase();                        // если строка не пустая, ищем ее среди
        else
            return true;

        return  isSubString(task, query) ||                     // названий,
                isSubString(task.getExecutor(), query) ||       // исполнителей,
                isSubString(task.getProject(), query) ||        // проектов
                contains(task.getContext(), query) ||           // контекстов,
                contains(task.getComments().getList(), query);  // комментариев
    }

    @Override
    public String getSelector() {
        return mQuery;
    }

    @Override
    public String toString(String selector) {
        return selector;
    }

    @Override
    public void setSelector(String query) {
        mQuery = query;
        if(mSet)
            onFilterChanged();
    }

    @Override
    public void setFilter(boolean set) {
        super.setFilter(set);
        if(!set)                                                // сброс фильтра - сбрасывает селектор
            mQuery = null;
    }
}
