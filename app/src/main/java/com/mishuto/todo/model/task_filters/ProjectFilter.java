package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.tags_collections.ProjectsSet;
import com.mishuto.todo.model.tags_collections.Tags;

import static com.mishuto.todo.common.ResourceConstants.Filters.EMPTY_PROJECT_NAME;
import static com.mishuto.todo.model.tags_collections.Tags.EMPTY_TAG;

/**
 * Фильтр списка задач по проекту
 * Created by Michael Shishkin on 31.07.2017.
 */
 @SuppressWarnings("WeakerAccess")
 public class ProjectFilter extends ChoiceTagFilter<StringContainer> {

    ProjectFilter() { }

    @Override
    boolean isTaskFiltered(Task task) {
        return getSelector() == task.getProject();
    }

    @Override
    public Tags getTagCollection() {
        return ProjectsSet.getInstance();
    }

    @Override
    public String toString(StringContainer selector) {
        return selector == EMPTY_TAG ? EMPTY_PROJECT_NAME : selector.toString();
    }

    @Override
    public TagItem createItem(StringContainer tag) {
        return new TagItem(tag) {
            @Override
            public String missingItemName() {
                return EMPTY_PROJECT_NAME;
            }

            @Override
            public boolean isFiltered(Task task) {
                return task.getProject() == mItem;
            }
        };
    }

    @Override
    public void putInTask(Task task) {
        task.setProject(mSelector);
    }
}
