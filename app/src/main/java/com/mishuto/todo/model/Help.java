package com.mishuto.todo.model;

import com.mishuto.todo.xml.XmlDoc;
import com.mishuto.todo.xml.XmlNode;
import com.mishuto.todo.xml.XmlNodeList;

import static com.mishuto.todo.common.TSystem.approveThat;

/**
 * Формирование структуры данных Справки из XML-файла
 * Created by Michael Shishkin on 24.06.2018.
 */
public class Help {

    private XmlNodeList mCategories; // список категорий
    private final String mLang;

    private final static String LANG_ATTR = "lang";

    // Категории справки
    public class Category {
        final static String TAG = "category";
        final static String TITLE = "name";
        private String mTitle;          // Название категории
        XmlNode mNode;

        // создание объекта Категория из узла XML
        Category(XmlNode node) {
            mNode = node;
            mTitle = getChildNodeValue(node, Category.TITLE);
            approveThat(mTitle != null, "missing name for category tag #" + node.getIndex());
        }

        public String getTitle() {
            return mTitle;
        }

        public Iterable<Article> getArticles() {
            return mNode.getChildNodes().getIterable(
                    new XmlNodeList.NodeMatcher() {
                        @Override
                        public boolean match(XmlNode node) {
                            return node.getName().equals(Article.TAG);
                        }
                    },
                    new XmlNodeList.Element<Article>() {
                        @Override
                        public Article createElement(XmlNode node) {
                            return new Article(node);
                        }
                    });
        }
    }

    //Статья справки
    public class Article {
        private String mTitle;      // заголовок статьи
        private String mContent;    // содержимое сататьи, если статья отображается как обычный текст
        private String mLayout;     // название ресурса шаблона, если статья сверстана в отдельном шаблоне
        private boolean textType;   // флаг, указывающий что статья текстовая

        final static String TAG = "article";
        final static String TITLE = "title";
        final static String CONTENT = "text";
        final static String LAYOUT = "layout";

        Article(XmlNode node) {
            mTitle = getChildNodeValue(node, TITLE);
            mContent = getChildNodeValue(node, CONTENT);
            XmlNode layoutNode = node.getChildNodes().findFirst(LAYOUT);
            if(layoutNode!= null)
                mLayout = layoutNode.getContent();
            textType = mContent != null;

            approveThat(mTitle != null && (mContent != null ^ mLayout != null), "Error in article tag #" + node.getIndex() + ". Title is: " + (mTitle == null ? "null" : mTitle));
        }

        public String getTitle() {
            return mTitle;
        }

        public String getContent() {
            return mContent;
        }

        public String getLayout() {
            return mLayout;
        }

        public boolean isTextType() {
            return textType;
        }
    }

    public Help(int structureFileId, String lang) {
        mLang = lang;
        mCategories = new XmlDoc(structureFileId).getNodeList(Category.TAG);    // загрузка XML-файла
    }

    // загрузка дочернего узла с заданным названием и текущей локалью
    private String getChildNodeValue(XmlNode node, final String name) {
        XmlNode found = node.getChildNodes().findFirst(new XmlNodeList.NodeMatcher() {
            @Override
            public boolean match(XmlNode node) {
                return node.getName().equals(name) && node.getAttrValue(LANG_ATTR).equals(mLang);
            }
        });

        return found == null ? null : found.getContent();
    }

    public Iterable<Category> getCategories() {
        return mCategories.getIterable(new XmlNodeList.Element<Category>() {
            @Override
            public Category createElement(XmlNode node) {
                return new Category(node);
            }
        });
    }
}
