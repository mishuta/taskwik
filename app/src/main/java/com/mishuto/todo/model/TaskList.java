package com.mishuto.todo.model;

import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.aggregators.DBArrayList;

import java.util.List;

/**
 * Created by mike on 24.08.2016.
 * Контейнер для списка задач Task
 * !!! При создании не загружает себя из базы, т.к. это может быть длительной опрерацией.
 * Клиент должен вызвать метод load() самостоятельно
 *
 * Поскольку задачи загружаются в отдельном потоке, методы синхронизированы
 */
public class TaskList extends DBObject {
    private static final TaskList sTaskList = new TaskList();
    private DBArrayList<Task> mTasks;               // список задач
    transient private boolean hasLoaded = false;    // список загружен

    private TaskList() {
        mTasks = new DBArrayList<>();
    }

    public static TaskList get() {
        return sTaskList;
    }

    public boolean hasLoaded() {
        return hasLoaded;
    }

    synchronized public void load() {
        if(!hasLoaded) {
            loadAsSingleton();
            hasLoaded = true;
        }
    }

    public static List<Task> getTasks() {
        return get().mTasks.getShallowCopy();
    }

    // Ззапросить задачу по id
    public static Task getById(long id){
        Task task;

        if(!sTaskList.hasLoaded())                 // если список не загружен
            task = DBObject.load(Task.class, id);   // загружаем задачу из базы. Требуется для DetailsActivity, когда она восстанавливается до загрузки списка в LauncherScreenActivity
        else {
            task = (Task) getCached(Task.class, id);
            if (task == null || get().mTasks.indexOf(task) == -1)
                throw new AssertionError("task for id=" + id + " is not found in the tasks list");
        }
        return task;
    }

    public static Task addNew(){
        Task newTask = new Task(true);
        get().mTasks.add(newTask);
        return newTask;
    }

    public static Task getLast() {
        return get().mTasks.get(get().mTasks.size()-1);
    }

    public static boolean remove(Task task) {
        return get().mTasks.remove(task);
    }
}
