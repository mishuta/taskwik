package com.mishuto.todo.model.task_fields;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.aggregators.DBLinkedList;

/**
 * Список комментариев по задаче
 *
 * Created by Michael Shishkin on 27.12.2016.
 */

public class Comments extends DBObject  {

    private DBLinkedList<Description> mList;  // список комментариев

    public Comments() {
        mList = new DBLinkedList<>();
    }

    public DBLinkedList<Description> getList() {
        return mList;
    }

    // последний (по времени) комментарий
    public Description getActual(){
        return mList.isEmpty() ? null : mList.getFirst();
    }

    // добавить комментарий
    public boolean add(String description) {
        if(isValidDesc(description)) {
            mList.addFirst(new Description(description));
            return true;
        }
        return false;
    }

    // валидация комментария
    public static boolean isValidDesc(String desc) {
        return !TextUtils.empty(desc);
    }

    // класс комментария
    public static class Description extends DBObject {
        private TCalendar mDate;    // дата комментария
        private String mComment;    // комментарий

        public  Description() {}    // дефолтный конструктор для создания из БД

        private Description(String comment) {
            mComment = comment;
            mDate = TCalendar.now();
        }

        public TCalendar getDate() {
            return mDate;
        }

        public String getComment() {
            return mComment;
        }

        @Override
        public String toString() {
            return mComment;
        }

        public void setComment(String comment) {
            if(isValidDesc(comment)) {
                mComment = comment;
                update();
            }
        }
    }
}
