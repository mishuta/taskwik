package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.task_fields.TaskTime;

import java.util.Calendar;

import static com.mishuto.todo.common.ResourceConstants.Recurrence.MONTHLY_FACTOR_ITEMS;
import static com.mishuto.todo.common.ResourceConstants.Recurrence.MONTHLY_FACTOR_VALUES;

/**
 * Сегмент для ежемесячных повторений
 * Created by Michael Shishkin on 29.11.2016.
 */
public class Month extends BaseMonthYear {
    //Кратности месяца
    public enum MonthlyFactor {
        //Ожидается что кратность "каждый месяц" подразумевает дату в этом месяце, если она наступит в будущем.
        //Например, для 01.01.18 ближайшая дата для "каждое 2 число месяца" = 02.01.18. Поэтому EVERY_MONTH=0
        //Ожидается что кратность "каждый второй месяц" подразумевает дату не ранее чем через месяц.
        //Например, для 01.01.18 ближайшая дата для "каждое 2 число каждого второго месяца" = 02.03.18. Поэтому SECOND_MONTH=2
        //Так же для всех прочих кратнстей
        EVERY_MONTH(0), SECOND_MONTH(2), THIRD_MONTH(3), FOURTH_MONTH(4), SIXTH_MONTH(6), NINTH_DAY(9);
        private int factor;

        MonthlyFactor(int factor) {
            this.factor = factor;
        }

        public int getFactor() { return  factor; }

        @Override
        public String toString() {
            return MONTHLY_FACTOR_ITEMS[ordinal()];
        }
    }

    private MonthlyFactor mMonthFactor = MonthlyFactor.EVERY_MONTH;  // кратность месяцев

    private Month() { this(null); }
    Month(TaskTime startTime) { super(startTime); }


    @Override
    public TCalendar getNextEvent() {
        TCalendar startDate = getStartTime();                       // стартовая дата
        startDate.add(Calendar.MONTH, mMonthFactor.getFactor());    // добавляем кратность месяцев

        return calculateDate(startDate);
    }

    @Override
    void shiftDate(TCalendar date) {
        date.add(Calendar.MONTH, 1);
    }

    public MonthlyFactor getMonthFactor() {
        return mMonthFactor;
    }

    public void setMonthFactor(MonthlyFactor monthFactor) {
        mMonthFactor = monthFactor;
        update();
    }

    @Override
    public String getShortValue() {
        return MONTHLY_FACTOR_VALUES[mMonthFactor.ordinal()];
    }
}
