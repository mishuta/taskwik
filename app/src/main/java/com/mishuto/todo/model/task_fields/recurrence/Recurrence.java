package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.model.task_fields.TaskTime;

/**
 * Класс, описывающий свойство повторяемыость у задачи
 * Created by Michael Shishkin on 07.11.2016.
 */

public class Recurrence extends DBObject
{
    private Boolean isSet;                      // флаг валиден ли объект
    private SegmentSelector mSegmentSelector;   // указатель активного сегмента

    //момент времени, от которого отсчитываются повторы (переход задачи в статус завершено): если время не задано, то будет испольоваться текщее время
    private TaskTime mStartEvent;

    // объекты каждого сегмента
    private Day mDay;
    private Week mWeek;
    private Month mMonth;
    private Year mYear;

    public Recurrence() {
        isSet = false;
        mSegmentSelector = SegmentSelector.DEFAULT_PAGE;
        mStartEvent = new TaskTime();

        mDay =  new Day(mStartEvent);
        mWeek = new Week(mStartEvent);
        mMonth= new Month(mStartEvent);
        mYear = new Year(mStartEvent);
    }

    // Запрос текущего сегмета
    @SuppressWarnings("WeakerAccess")
    public AbstractSegment getCurrentSegment(){
        return getSegment(mSegmentSelector);
    }

    // Методы получения сегментов

    public Day getDay() {
        return mDay;
    }

    public Week getWeek() {
        return mWeek;
    }

    public Month getMonth() {
        return mMonth;
    }

    public Year getYear() {
        return mYear;
    }

    private AbstractSegment getSegment(SegmentSelector segmentSelector) {
        switch (segmentSelector){
            case DAY:   return getDay();
            case WEEK:  return getWeek();
            case MONTH: return getMonth();
            case YEAR:  return getYear();
        }
        return null; // никогда не возвращается
    }

    public boolean isSet() {
        return isSet;
    }

    public void set(boolean recurrenceSet) {
        isSet = recurrenceSet;
        update();
    }

    // установленно ли время повтора (была ли задача переведена в статус Завершено)
    public boolean isStartTimeSet() {
        return mStartEvent.isDateSet();
    }

    // Событие завершения/активации задачи
    // Сохраняет/сбрасывает стартовое время повторов. Вызывается в момент завершения задачи -  активации Recurrence
    public void onTaskPerformed(boolean flag) {
        if(flag)
            mStartEvent.setCalendar(TCalendar.now(), true);
        else
            mStartEvent.resetDate();
    }

    public SegmentSelector getSegmentSelector() {
        return mSegmentSelector;
    }

    public void setSegmentSelector(SegmentSelector segmentSelector) {
        mSegmentSelector = segmentSelector;
        update();
    }

    // вычисление времени ближайшего события из текущего сегмента
    public TaskTime getNextEvent(){

        TaskTime nextEvent = new TaskTime();
        nextEvent.setCalendar(
                getCurrentSegment().getNextEvent(),
                getCurrentSegment().isTimeSet() || (getCurrentSegment() == mDay && !mDay.isDaySelected()));

        return nextEvent;
    }
}

