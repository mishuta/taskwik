package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.model.tags_collections.Tags;

import static com.mishuto.todo.common.ResourceConstants.Filters.EMPTY_EXECUTOR_NAME;
import static com.mishuto.todo.model.tags_collections.Tags.EMPTY_TAG;

/**
 * Фильтр списка задач по исполнителю
 * Created by Michael Shishkin on 31.07.2017.
 */

 @SuppressWarnings("WeakerAccess")
 public class ExecutorFilter extends ChoiceTagFilter<StringContainer> {

    ExecutorFilter() { }

    @Override
    boolean isTaskFiltered(Task task) {
        return task.getExecutor() == getSelector();
    }

    @Override
    public Tags getTagCollection() {
        return Executors.getInstance();
    }

    @Override
    public String toString(StringContainer selector) {
        return selector == EMPTY_TAG ? EMPTY_EXECUTOR_NAME : selector.toString();
    }

    @Override
    public TagItem createItem(StringContainer tag) {
        return new TagItem(tag) {
            @Override
            public String missingItemName() {
                return EMPTY_EXECUTOR_NAME;
            }

            @Override
            public boolean isFiltered(Task task) {
                return task.getExecutor().equals(tag);
            }
        };
    }

    @Override
    public void putInTask(Task task) {
        task.setExecutor(mSelector);
    }
}
