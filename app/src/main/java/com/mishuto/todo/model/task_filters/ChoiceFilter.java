package com.mishuto.todo.model.task_filters;

/**
 * Базовая реализация Choice фильтров для сохраняемых фильтров
 * Created by Michael Shishkin on 29.01.2019.
 */
abstract class ChoiceFilter<S, E> extends AbstractFilter implements Choice<S, E>, Persistence {

    private transient PersistentHelper mAdapter;
    S mSelector;    // не используем Selectivity.SelectivityHelper, поскольку требуется сохранять селектор в БД. Проще реализовать интерфейс еще раз

    // Имплементация Selectivity

    @Override
    public S getSelector() {
        return mSelector;
    }

    @Override
    public void setSelector(S selector) {
        mSelector = selector;
        if(mSet)
            onFilterChanged();
        mAdapter.update();
    }

    // Имплементация Exclusiveness

    @Override
    public void setFilter(boolean set) {
        super.setFilter(set);
        Exclusiveness.setExclusiveFilter(this, set);
        mAdapter.update();
    }

    @Override
    public String toString() {
        return toString(getSelector());
    }

    // Имплементация Persistence

    @Override
    public void setHelper(PersistentHelper helper) {
        mAdapter = helper;
    }


    @Override
    public void flip() {
        super.flip();
        mAdapter.update();
    }

    // Базовая реализация интерфейса Item
    abstract static class ChoiceItem<E> implements Item<E> {
        E mItem;

        ChoiceItem(E item) {
            mItem = item;
        }

        @Override
        public String toString() {
            return  isMissingItem() ? missingItemName() : mItem.toString();
        }

        @Override
        public E getValue() {
            return mItem;
        }
    }
}
