package com.mishuto.todo.model;

import android.support.annotation.NonNull;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.aggregators.DBTreeSet;
import com.mishuto.todo.model.events.TaskEventsFacade;
import com.mishuto.todo.model.events.TimerPublisher;
import com.mishuto.todo.model.tags_collections.Executors;
import com.mishuto.todo.model.tags_collections.Tags;
import com.mishuto.todo.model.task_fields.Comments;
import com.mishuto.todo.model.task_fields.Order;
import com.mishuto.todo.model.task_fields.Priority;
import com.mishuto.todo.model.task_fields.TaskTime;
import com.mishuto.todo.model.task_fields.Title;
import com.mishuto.todo.model.task_fields.recurrence.Recurrence;

import java.util.HashSet;
import java.util.Set;

import static com.mishuto.todo.common.ResourceConstants.TITLE_NOT_FOUND;
import static com.mishuto.todo.common.TCalendar.now;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_CONTEXT;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_EXECUTOR;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_ORDER;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_PREV;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_PREV_DISCARDS_TIME;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_PRIORITY;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_PROJECT;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_RECURRENCE;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_TIME;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.A_TIME_DISCARDS_PREV;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.CREATED;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.DELETED;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.LOADED;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.PREV_STATE;
import static com.mishuto.todo.model.events.TaskEventsFacade.EventType.S_PERFORMED;
import static com.mishuto.todo.model.events.TaskEventsFacade.send;
import static java.util.Calendar.DATE;
import static java.util.Calendar.MINUTE;

/**
 * Класс модели Задача.
 * Created by mike on 19.08.2016.
 */
public class Task extends DBObject implements Comparable<Task>,     // реализует упорядочивание задач в списке
        TimerPublisher.TimerObserver,                               // реализет обработку событий таймера
        TaskEventsFacade.TaskObserver                               // реализует обработку событий по задаче
{
    private Title mTitle;                           // Название
    private Priority mPriority;                     // Приоритет
    private Order mOrder;                           // Очередность
    private TaskTime mTaskTime;                     // Время начала задачи
    private Recurrence mRecurrence;                 // Повторения задачи
    private Task mPrev;                             // Задача, после которой стартует эта
    private Comments mComments;                     // Список комментариев в хронологическом порядке
    private StringContainer mProject;               // Проект (элемент списка проектов)
    private StringContainer mExecutor;              // Исполнитель (элемент списка исполнителей)
    private DBTreeSet<StringContainer> mContext;    // Список контекстов
    private TCalendar mCreated;                     // Время создания
    private Boolean mPerformed;                     // Флаг задача выполнена

    // объект таймера задачи
    private transient TimerPublisher<Task> mTimer;
    /*
    * Схема распространения событий для Task:
    * TimerPublisher создается в Task. Task подписывается на рассылку от TimerPublisher
    * TimerPublisher создает таймер в TimerFacade
    * По TimerFacade.onFire идет рассылка событий всем подписантам (сейчас только Task)
    * Task (в частности) в ответ на событие от TimerPublisher через TaskEventsFacade, рассылает событие которое получают подписчики TaskEventsFacade (TaskEventHandler, ListFragment, TaskCardController)
    * В отличии от TimerPublisher (один экземпляр на одну задачу), TaskEventsFacade имеет один экземпляр для всех задач.
    */

    // дефолтный конструктор для восстановления объекта из БД
    private Task() {
        mTimer = new TimerPublisher<>();   // в дефолтном конструкторе создается только объект таймер, который не загружается из БД
        mTimer.register(this);
        TaskEventsFacade.getInstance().register(this);
    }

    // конструктор вызывается при создании задачи объектом TaskLists
    // инициирует поля дефолтными значениями
    Task(boolean newTask) {     // параметр конструктора нужен только для того, чтобы его сигнатура отличалась от дефолтного конструктора
        this();
        mTitle = new Title();
        mPriority = Priority.DEFAULT_PRIORITY;
        mOrder = Order.DEFAULT_ORDER;
        mTaskTime = new TaskTime();
        mRecurrence = new Recurrence();
        mComments = new Comments();
        mContext = new DBTreeSet<>();
        mCreated = now();
        mPerformed = false;
        mExecutor = Tags.EMPTY_TAG;
        mProject = Tags.EMPTY_TAG;
        send(CREATED, this);            // сообщаем миру о рождении задачи
    }

    // **** Методы DBObject

    @Override
    public void read(long id) {
        super.read(id);             // после загрузки задачи из БД актуализируем таймеры и статусы, зависящие от времени

        if(isStateRepeated() && getNearestEvent().hasCome()) {
            mPerformed = false;                 // Если задача Повторяемая, а срок повторения истек - переводим ее в состояние Активная
            update();
            mRecurrence.onTaskPerformed(false); // сбрасываем время Recurrence момент, от которого отсчитывать повторы
        }
        else
            setTimerEvent();                    // иначе, устанавливаем таймер (если есть)

        send(LOADED, this);                     // посылаем событие "задача загружена"
    }

    private transient boolean deleteInProgress = false;

    @Override
    public void delete() {
        if(getRefCounter() == 1) {                              // если задача удаляется окончательно, то ...

            mTimer.cancelTimer();                               // перед ее удалением сбрасываем таймер
            mTimer.unregister(this);                            // и выполняем разрегистрацию
            TaskEventsFacade.getInstance().unregister(this);
        }

        // если на задачу ссылаются только зависимые задачи + TaskList), удаляем ссылки на нее, чтобы ORM смог ее удалить
        else if(!deleteInProgress && getRefCounter() == getDependantTasks().size() + 1 ) {
            deleteInProgress = true;                            // устанавливаем флаг, чтобы не запустилось повторное удаление во время обнуления ссылки
            boolean notPerformed = !mPerformed;
            mPerformed = true;

            for(Task task : getDependantTasks()) {
                task.setPrev(null);                             // обнуляем ссылки
                if(notPerformed)
                    send(PREV_STATE, task);                     // и отправляем изменение статуса (если задача была незавершена)
            }

            deleteInProgress = false;
            send(DELETED, this);
        }

        super.delete();
    }

    // ***** Методы получения состояния задачи

    // Задача АКТИВНА, если она не является ОТЛОЖЕННОЙ или ВЫПОЛНЕННОЙ
    @SuppressWarnings("WeakerAccess")
    public boolean isStateActive() {
        return !isStateInPerformed() && !isSateDeferred();
    }

    // Задача ОТЛОЖЕНА, если установлен срок задачи в будущем или она зависит от другой задачи, которая еще не завершена
    public boolean isSateDeferred() {
        return mTaskTime.isDateSet() && !mTaskTime.hasCome() || mPrev != null &&  !mPrev.isStateInPerformed();
    }

    // Задача ПОВТОРЯЕМАЯ, если она ВЫПОЛНЕНА и установлен повтор
    public boolean isStateRepeated() {
        return mPerformed && mRecurrence.isSet();
    }

    // Задача ЗАВЕРШЕНА, если она ВЫПОЛНЕНА и повтор не установлен
    public boolean isStateCompleted() {
        return isStateInPerformed() && !mRecurrence.isSet();
    }

    // Виртуальный статус. Задача ОТСРОЧЕНА, еслит она ПОВТОРЯЕМАЯ или ОТЛОЖЕННАЯ
    public boolean isStateInPostponed() {
        return isStateRepeated() || isSateDeferred();
    }

    // Виртуальный статус. Задача ВЫПОЛНЕНА, если пользователь отметил задачу как завершенную
    public boolean isStateInPerformed() {
        return mPerformed;
    }

    public Title getTitle() {
        return mTitle;
    }

    public Priority getPriority() {
        return mPriority;
    }

    public void setPriority(Priority priority) {
        mPriority  = priority;
        update();
        send(A_PRIORITY, this);
    }

    public Order getOrder() {
        return mOrder;
    }

    public void setOrder(Order order) {
        mOrder = order;
        update();
        send(A_ORDER, this);
    }

    public TaskTime getTaskTime() {
        return mTaskTime;
    }

    public void setTaskTime(TaskTime taskTime) {
        if(isStateInPerformed())
            return;                                                                 // невозможно задать дату завершенной задаче

        mTaskTime = taskTime;
        update();
        setTimerEvent();

        if (mTaskTime.isDateSet() && mPrev != null) {                               // если время установлено, то предыдущая задача сбрасывается
            String prevTitle = mPrev.toString();                                    // сохраняем имя сбрасываемой задачи
            updatePrev(null);
            send(A_TIME_DISCARDS_PREV, this, prevTitle);                            // ... чтобы передать его в сообщении
        }
        else
            send(A_TIME, this);                                                    // рассылка оповещений об изменении атрибута
    }

    public TCalendar getCreated() {
        return mCreated;
    }

    public Comments getComments() {
        return mComments;
    }

    public StringContainer getProject() {
        return mProject;
    }

    public void setProject(StringContainer project) {
        mProject = project;
        update();
        send(A_PROJECT, this);
    }

    public StringContainer getExecutor() {
        return mExecutor;
    }

    public void setExecutor(StringContainer executor) {
        Executors.getInstance().add(executor);
        executor = Executors.getInstance().getOriginal(executor); // получаем оригинальный объект, который лежит в мапе Executor, чтобы охранить ссылку именно на него
        if(executor == null)
            throw new AssertionError("trying to put the nullable Executor");

        mExecutor = executor;
        update();
        send(A_EXECUTOR, this);
    }

    public Task getPrev() {
        return mPrev;
    }

    public void setPrev(Task prev) {
        updatePrev(prev);

        if(mPrev != null && mTaskTime.isDateSet()) {    // если предыдущая задача установлена, время сбрасывается
            mTaskTime.resetDate();
            setTimerEvent();
            send(A_PREV_DISCARDS_TIME, this);
        }
        else
            send(A_PREV, this);
    }

    private void updatePrev(Task prev) {
        mPrev = prev;
        update();
    }

    // является ли данная задача последователем текущей
    public boolean isSucceeding(Task task) {
        while(task.getPrev() != null) {
            task = task.getPrev();
            if(task == this)
                return true;
        }

        return false;
    }

    public Recurrence getRecurrence() {
        return mRecurrence;
    }

    // событие изменения Повтора
    private void onRecurrenceUpdate() {
        if(mPerformed && mRecurrence.isSet())   // если задача была завершена, а Повтор изменился (и установлен)
            if(getNearestEvent().hasCome())     // то если время повтора прошло
                setPerformed(false);            // активируем задачу
            else
                setTimerEvent();                // иначе - переустанавливаем таймер
    }

    // Перевод (сброс) задачи в (из) состостояние Performed (ВЫПОЛНЕНА) по команде пользователя и таймеру при установленном повторе
    public void setPerformed(Boolean performed) {
        if(mPerformed == performed)
            return;

        mPerformed = performed;
        update();
        mRecurrence.onTaskPerformed(mPerformed);                    // сообщаем Recurrence момент, от которого отсчитывать повторы

        if(mPerformed)                                              // сбрасываем время задачи
            mTaskTime.resetDate();                                  // примечание: здесь можно было бы послать сообщение A_TIME, если требуется выдавать пользователю сообщение о сбросе даты

        send(S_PERFORMED, this);                                    // сообщаем слушателям об изменении текущей задачи

        for(Task t: getDependantTasks())                            // сообщаем всем зависимым задачам, что текущая - изменила статус
            send(PREV_STATE, t);

        setTimerEvent();                                            // (пере)устанавливаем таймер (если требуется: recurrence либо устанавливается, либо отменяется)
    }


    // множество задач, для которых эта является предыудщей
    private Set<Task> getDependantTasks() {
        Set<Task> dependantTasks = new HashSet<>();
        for(Task t : TaskList.getTasks())
            if(t.mPrev == this)
                dependantTasks.add(t);

        return dependantTasks;
    }

    public Set<StringContainer> getContext() {
        return mContext;
    }

    public void setContext(DBTreeSet<StringContainer> context) {
        if(context == null) {
            context = new DBTreeSet<>();
        }

        mContext = context;
        update();
        send(A_CONTEXT, this);
    }

    public TCalendar getTimerTime() {
        return mTimer != null ? mTimer.getTime() : null;
    }

    public TimerPublisher<Task> getTimer() {
        return mTimer;
    }

    // установка таймера задачи
    // объект таймера создается только тогда, когда он нужен
    public void setTimerEvent() {
        TaskTime taskTime = getNearestEvent();

        if(!taskTime.isDateSet())
            mTimer.cancelTimer();             // если событие задачи не установлено, сбрасываем таймер

        else {
            TCalendar time = taskTime.getCalendar();
            time.clipTo(MINUTE);                       // округляем время до минут, чтобы времена задач можно было сравнивать

            if (!taskTime.isTimeSet())                 // если у события нет временной части
                time.clipTo(DATE);                     // округляем время события до дня (сработает в 00:00)

            mTimer.setTime(time);
        }
    }

    // сортировка задач
    @Override
    public int compareTo(@NonNull Task t) {
        int c;

        if(isStateCompleted() != t.isStateCompleted())
            return isStateCompleted() ? 1 : -1;                             // завершенные ниже незавершенных

        if(isStateInPostponed() != t.isStateInPostponed())
            return isStateInPostponed() ? 1 : - 1;                          // отложенные ниже активных

        if(isStateActive()) {                                               // для активных:
            if (!mPriority.equals(t.mPriority))                                 // по приоритету
                return mPriority.compareTo(t.mPriority);

            if (!mOrder.equals(t.mOrder))
                return t.mOrder.compareTo(mOrder);                             // по очереди (в обратном порядке)
        }
        else {                                                              // для неактивных
            if(mPrev!=null && mPrev == t)                                       // если у задачи есть предыдущая,
                return 1;                                                       // то она располагается ниже предыдущей

            if(t.mPrev != null && t.mPrev == this)
                return -1;
        }
                                                                            // для активных и отложенных
        if((c = getNearestEvent().compareTo(t.getNearestEvent())) != 0)     // по ближайшему событию
            return c;

        if(!mExecutor.equals(t.mExecutor))                                  // по исполнителю
            return mExecutor.compareTo(t.mExecutor);

        return mCreated.compareTo(t.getCreated());                          // последняя - по дате создания
    }

    @Override
    public String toString() {
        String name = getTitle().getTaskName();
        return name == null ? TITLE_NOT_FOUND : name;
    }

    // обработка события от таймера
    @Override
    public void onTime(int timerId, TCalendar time) {
        if(isStateRepeated())
            setPerformed(false);                                // если таймер пришел по повторяемой задаче, переводим в активную

        send(TaskEventsFacade.EventType.TIMER, this, time);     // при получении события от таймера - передаем его всем подписчикам на события задачи
    }

    // получить ближайшее событие задачи
    public TaskTime getNearestEvent() {
        if(!mPerformed) {                       // Если задача незавершена
            if (mTaskTime.isDateSet())          //      если задан срок, то возращаем его
                return mTaskTime;
            else
                return new TaskTime();          //      иначе пустой объект
        }
        else if(mRecurrence.isSet())            // Если завершена, возвращаем Recurrence, если задан
            return mRecurrence.getNextEvent();
        else
            return new TaskTime();
    }

    @Override
    public void onTaskEvent(TaskEventsFacade.EventType type, Task task, Object extendedData) {
        if(task == this) {
            if(type == A_RECURRENCE)
                onRecurrenceUpdate();
        }
    }
}
