package com.mishuto.todo.model.task_fields;

import com.mishuto.todo.common.ResourceConstants.Validity;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;

/**
 * Атрибут Название задачи
 * Created by Michael Shishkin on 22.01.2017.
 */

public class Title extends DBObject {
    private String mTaskName;   // название задачи

    public Title() {}           // дефолтный конструктор для создания из БД

    public String getTaskName() {
        return mTaskName;
    }

    public boolean setTaskName(String title) {
        if(validate(title) == Validity.OK) {
            mTaskName = title.trim();
            update();
            return true;
        }
        return false;
    }

    // Валидация имени задачи
    public int validate(String name) {
        if(TextUtils.empty(name))       // Имя должно быть непустым ...
            return Validity.ERR_EMPTY;

                                        // ... и не повторяться в других задачах
        for(Task t: TaskList.getTasks()) {
            if(t.getTitle() != this &&
                    t.getTitle().getTaskName() != null &&
                    TextUtils.insensitiveEquals(t.getTitle().getTaskName(), name))
                return Validity.ERR_EXISTS;
        }

        return Validity.OK;
    }
}
