package com.mishuto.todo.model;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.database.DBField;
import com.mishuto.todo.database.SQLTable;
import com.mishuto.todo.model.task_fields.TaskTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.mishuto.todo.common.TCalendar.now;
import static com.mishuto.todo.model.Settings.getWorkTime;

/**
 * Класс, реализующий функцию "Отложить задачу"
 * Создает список времен для выбора.
 *
 * Created by Michael Shishkin on 12.12.2017.
 */

public class Delayer implements Serializable {
    private ArrayList<TaskTime> mTargetTimes;

    // Таблица DELAYER_STAT служит для сохранения статистики нажатия кнопок (для дальнейшей адаптации алгоритма)
    private static String TABLE = "DELAYER_STAT";
    private static String BUT_NO = "BUTTON_NUMBER";
    private static String START = "START_DATE";
    private static String TARGET = "TARGET_DATE";

    private static SQLTable sSQLTable = new SQLTable(TABLE,
                    new SQLTable.Column(BUT_NO,  SQLTable.ColumnType.INTEGER, false),
                    new SQLTable.Column(START,   SQLTable.ColumnType.INTEGER, false),
                    new SQLTable.Column(TARGET,  SQLTable.ColumnType.INTEGER, false));

    private TCalendar mStartTime = now();   // время создания объекта

    public Delayer() {
       initTargetTimes();
    }

    // создание списка целевых времен для кнопок отложить
    private void initTargetTimes() {
        mTargetTimes = new ArrayList<>(8);
        TCalendar t = mStartTime;

        t = addValue(t, 30, 0, true);                   // 0. +30 минут
        t = addValue(t, 30, 0, true);                   // 1. 30 + 30 = +1 час
        t = addValue(t, 60, 30, true);                  // 2. 60 + 60 = +2 часа
        addValue(t, 2 * 60, 60, true);                  // 3. 2 + 2 = +4 часа
        t = addValue(mStartTime, 24 * 60, 0, false);    // 4. + сутки (завтра)
        t = addValue(t, 24 * 60, 0, false);             // 5. 1 + 1 = послезавтра
        addValue(t, 48 * 60, 0, false);                 // 6. 1 + 1 + 2 - четвертый день
        addValue(mStartTime, 7 * 24 * 60, 0, false);    // 8. + 7 (неделя)
    }

    @SuppressWarnings("unchecked")
    public List<TaskTime> getTargetTimes() {
        return (List<TaskTime>)mTargetTimes.clone();
    }

    public TCalendar getStartTime() {
        return mStartTime;
    }

    // Вычисление времени задачи и добавление в список
    private TCalendar addValue(TCalendar start, int offset, int round, boolean inDay) {
        TCalendar t = start.clone();
        if(!inDay)              // если время у интервала отстутствует - сбрасываем время у стартовой даты
            t.setTimePart(0,0); // иначе может быть нежелательный переход на следующую дату в getNearestWorkDate

        t.add(Calendar.MINUTE, offset);
        t = getWorkTime().getNearestWorkDate(t);
        if(round > 0)
            t.roundTo(round);

        mTargetTimes.add(new TaskTime(t, inDay));   // сохраняем полученную дату

        return t;
    }

    // Сохранение статистики нажатия кнопки
    public void onSelected(TaskTime taskTime) {
        int position = mTargetTimes.indexOf(taskTime);  // для кастом-объекта position = -1

        DBField.put(BUT_NO, position, sSQLTable.getCV());
        DBField.put(START, mStartTime, sSQLTable.getCV());
        DBField.put(TARGET, taskTime.getCalendar(), sSQLTable.getCV());
        sSQLTable.insert();
    }
}
