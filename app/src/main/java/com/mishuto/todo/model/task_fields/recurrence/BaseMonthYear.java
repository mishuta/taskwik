package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.DateFormatWrapper;
import com.mishuto.todo.common.KeyString;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.task_fields.TaskTime;

import java.util.Calendar;

import static com.mishuto.todo.common.ResourceConstants.Recurrence.NUM_WEEKS_ARRAY;
import static com.mishuto.todo.common.TCalendar.DAYS_IN_WEEK;
import static com.mishuto.todo.common.TCalendar.FIRST_MONTH_DAY;
import static com.mishuto.todo.model.task_fields.recurrence.BaseMonthYear.WeekNumber.LAST_WEEK;

/**
 * Базовый класс для сегментов Месяц и Год, т.к. они различаются только кратностью повторений месяцев (n для месяца и 12n для года)
 * Created by Michael Shishkin on 17.12.2016.
 */

public abstract class BaseMonthYear extends AbstractSegment {

    private Integer mDayOfMonth;            // выбранный день месяца
    private Boolean isDayOfMonthSelected;   // селектор: используется ли день месяца или номер и день недели

    private WeekNumber mWeekNum;            // кратность недели месяца
    private Integer mWeekDay;               // день недели SUNDAY...SATURDAY

    // недели месяца
    public enum WeekNumber {
        FIRST_WEEK(0), SECOND_WEEK(1), THIRD_WEEK(2), FORTH_WEEK(4), LAST_WEEK(-1);
        private int factor;

        WeekNumber(int factor) {
            this.factor = factor;
        }

        public int getFactor() { return  factor; }

        @Override
        public String toString() {
            return NUM_WEEKS_ARRAY[ordinal()];
        }
    }

    BaseMonthYear(TaskTime startTime) {
        super(startTime);

        // инициализация значениями по умолчанию
        isDayOfMonthSelected = true;
        mWeekDay = getWeekDaysArray()[0].getKey();  // первый день недели
        mWeekNum = WeekNumber.FIRST_WEEK;
    }

    abstract void shiftDate(TCalendar date);    // перевод даты в следующий период

    //Вычисление конкретной даты повтора, после предварительного вычисления года и месяца в сабклассе
    TCalendar calculateDate(TCalendar baseDate) {
        int day;                                                        // день месяца, на который придется первый повтор
        TCalendar calcDate = baseDate.clone();                          // вычисляемая дата

        if(isDayOfMonthSelected) {                                      // если выбран день месяца
            day = getDayOfMonth();                                      // то день повтора = выбранному дню

            if (baseDate.getActualMaximum(Calendar.DAY_OF_MONTH) < day) // если день месяца больше максимального дня текущего (для заданной даты) месяца
                day = baseDate.getActualMaximum(Calendar.DAY_OF_MONTH); // день месяца устанавливаем на последний день

            calcDate.set(Calendar.DATE, day);
            if (calcDate.compareTo(getStartTime()) <= 0)                // если дата меньше или равна начальной
                shiftDate(calcDate);                                    // переходим в следующий период

        }
        else {
            day = calculateDayOfMonth(baseDate);                        // вычисляем день месяца по номеру недели и дню недели
            calcDate.set(Calendar.DATE, day);

            if (calcDate.compareTo(getStartTime()) <= 0) {              // если дата меньше или равна стартовой
                shiftDate(calcDate);                                    // переходим в следующий период
                day = calculateDayOfMonth(calcDate);                    // и заново вычисляем день месяца
                calcDate.set(Calendar.DATE, day);
            }
        }

        setTimePart(calcDate);                                          // учитываем время

        return calcDate;
    }

    // вычисление дня месяца по номеру недели и дню недели
    private int calculateDayOfMonth(TCalendar startDate) {
        if(mWeekNum != LAST_WEEK)   // если номер недели = 0...3
                                    // день месяца = (выбранный день недели - первый день недели месяца +7) mod 7 + 1 + 7 * номер недели
            return  (mWeekDay - startDate.getFirstDayOfWeekInMonth() + DAYS_IN_WEEK) % DAYS_IN_WEEK +
                    FIRST_MONTH_DAY + mWeekNum.getFactor() * DAYS_IN_WEEK;

        else                        // если выбрана последняя неделя месяца
                                    // день месяца = последний день месяца - (последний день недели месяца - выбранный день недели + 7) mod 7
            return startDate.getActualMaximum(Calendar.DAY_OF_MONTH) -
                    (startDate.getLastDayOfWeekInMonth() - mWeekDay + DAYS_IN_WEEK) % DAYS_IN_WEEK;
    }


    public WeekNumber getWeekNum() {
        return mWeekNum;
    }

    public void setWeekNum(WeekNumber num) {
        mWeekNum = num;
        update();
    }

    //возвращает текущий день недели
    public KeyString getWeekDay() {
        for(KeyString weekDay : getWeekDaysArray())
            if(weekDay.getKey().equals(mWeekDay) )
                return weekDay;

        throw new AssertionError("unexpected day of week");
    }

    public void setWeekDay(Integer weekDay) {
        mWeekDay = weekDay;
        update();
    }

    public boolean isDayOfMonthSelected() {
        return isDayOfMonthSelected;
    }

    public void setDayOfMonthSelected(boolean dayOfMonthSelected) {
        isDayOfMonthSelected = dayOfMonthSelected;
        update();
    }

    public Integer getDayOfMonth() {
        if(mDayOfMonth == null) // mDayOfMonth не инициируется в конструкторе, потому что в момент создания Recurrence#mStartEvent может быть неопределен
            mDayOfMonth = getStartTime().get(Calendar.DAY_OF_MONTH);

        return mDayOfMonth;
    }

    public void setDayOfMonth(Integer dayOfMonth) {
        mDayOfMonth = dayOfMonth;
        update();
    }

    // генерация массива названий дней недели для спинера
    public static KeyString[] getWeekDaysArray() {
        KeyString[] weekDays = new KeyString[TCalendar.DAYS_IN_WEEK];

        for(int i = 0; i < TCalendar.DAYS_IN_WEEK; i++) {
            int key = TCalendar.getDayOfWeekByOrdinal(i);                   // key = идентификатор дня недели
            String title = DateFormatWrapper.getDayOfWeekName(key, false);  // title = полное название дня недели
            weekDays[i] = new KeyString(key, title);
        }

        return weekDays;
    }
}
