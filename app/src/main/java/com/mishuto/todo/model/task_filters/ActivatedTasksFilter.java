package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.common.ResourceConstants;
import com.mishuto.todo.model.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.mishuto.todo.common.TSystem.approveThat;

/**
 * Фильтр содержит список задач, всплывших в окне нотификации
 * Created by Michael Shishkin on 27.03.2018.
 */

public class ActivatedTasksFilter extends Exclusiveness.ExclusivenessImp {

    private List<Task> mTaskSet = new ArrayList<>();

    @Override
    boolean isTaskFiltered(Task task) {
        return mTaskSet.contains(task);
    }

    @Override
    public String toString() {
        return ResourceConstants.Filters.ACTIVATED_TASK_FILTER;
    }

    public void set(Collection<Task> taskCollection) {
        mTaskSet.clear();
        mTaskSet.addAll(taskCollection);
        if(mSet)
            onFilterChanged();
    }

    // Возвращает задачу в случае, если она единственная в списке
    public Task getOnlyTask() {
        approveThat(mTaskSet.size() == 1);
        return mTaskSet.get(0);
    }
}
