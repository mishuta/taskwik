package com.mishuto.todo.model.task_fields.recurrence;

import static com.mishuto.todo.common.ResourceConstants.Recurrence.PAGES;

/**
 * Сегменты объекта Recurrence. Активным может быть только один сегмент
 */
public enum SegmentSelector {
    DAY,
    WEEK,
    MONTH,
    YEAR;

    // дефолтная страница в интерфейсе настроек повторения
    public static final SegmentSelector DEFAULT_PAGE = DAY;

    // названия сегментов
    public final String title =  PAGES[ordinal()];

    // переопределение стандартного метода для вывода названия адаптером спинера
    @Override
    public String toString() {
        return title;
    }
}
