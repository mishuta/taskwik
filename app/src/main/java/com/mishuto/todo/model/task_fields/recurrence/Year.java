package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.DateFormatWrapper;
import com.mishuto.todo.common.KeyString;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.model.task_fields.TaskTime;

import java.util.Calendar;

import static com.mishuto.todo.common.ResourceConstants.Recurrence.YEARLY_FACTOR_ITEMS;
import static com.mishuto.todo.common.ResourceConstants.Recurrence.YEARLY_FACTOR_VALUES;
import static com.mishuto.todo.common.TCalendar.MONTHS_IN_YEAR;

/**
 * Сегмент ежегодных повторений
 * Created by Michael Shishkin on 29.11.2016.
 */
public class Year extends BaseMonthYear {
    // годовые кратности
    public enum YearlyFactor {
        EVERY_YEAR(0), SECOND_YEAR(1), THIRD_YEAR(2), FOURTH_YEAR(3);
        private int factor;

        YearlyFactor(int factor) {
            this.factor = factor;
        }

        public int getFactor() { return  factor; }

        @Override
        public String toString() {
            return YEARLY_FACTOR_ITEMS[ordinal()];
        }
    }

    private Integer mMonth = 0;                                 // выбранный месяц, по умолчанию январь
    private YearlyFactor mYearFactor = YearlyFactor.EVERY_YEAR; // кратность лет

    private Year() { this(null);}

    Year(TaskTime startTime) {
        super(startTime);
    }

    // возвращает массив месяцев с названиями в текущей локали
    public static KeyString[] getMonthsInYearArray() {
        KeyString[] months = new KeyString[MONTHS_IN_YEAR];
        for(int i = 0; i < MONTHS_IN_YEAR; i++)
            months[i] = new KeyString(i, TextUtils.capitalize(DateFormatWrapper.getMonthName(i, false)));

        return months;
    }

    public TCalendar getNextEvent() {
        TCalendar startDate = getStartTime();                   // стартовая дата

        if( mMonth <= startDate.get(Calendar.MONTH))            // если выбранный месяц не больше стартового, то переходим в следующий год
            startDate.add(Calendar.YEAR, 1);

        startDate.set(Calendar.MONTH, mMonth);                  // устанавливаем выбранный месяц
        startDate.add(Calendar.YEAR, mYearFactor.getFactor());  // добавляем кратность лет

        return calculateDate(startDate);                        // рассчитываем конкретную дату в месяце
    }

    @Override
    void shiftDate(TCalendar date) {
        date.add(Calendar.YEAR, 1);
    }

    public Integer getMonth() {
        return mMonth;
    }

    public void setMonth(Integer month) {
        mMonth = month;
        update();
    }

    public YearlyFactor getYearFactor() {
        return mYearFactor;
    }

    @Override
    public String getShortValue() {
        return YEARLY_FACTOR_VALUES[mYearFactor.ordinal()];
    }

    public void setYearFactor(YearlyFactor yearFactor) {
        mYearFactor = yearFactor;
        update();
    }
}
