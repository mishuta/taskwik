package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.database.DBPersistentAdapter;

/**
 * Свойство, позволяющее фильтру стать сохраняемым в БД
 * Created by Michael Shishkin on 31.01.2019.
 */
public interface Persistence extends TaskFilter, DBPersistentAdapter.Persistable {

    void setHelper(PersistentHelper helper); // сохраняемый фильтр должен сохранять у себя ссылку на хелпер

    // создание объекта фильтра
    static Persistence createPersistentFilter(Class<? extends Persistence> filterClass) {
        DBPersistentAdapter<? extends Persistence> adapter = new DBPersistentAdapter<>(filterClass); // сначала создаем адаптер
        return new PersistentHelper<>(adapter).createFilter();                                       // затем, с его помощью, создаем хелпер и фильтр
    }

    // хелпер, облегчающий получение объекта из БД
    class PersistentHelper<T extends Persistence>  {
        DBPersistentAdapter<T> mAdapter;

        void update() {
            mAdapter.getProxy().update();
        }

        private PersistentHelper(DBPersistentAdapter<T> adapter) {
            mAdapter = adapter;
        }

        private Persistence createFilter() {
            mAdapter.getProxy().loadAsSingleton();
            Persistence filter = mAdapter.getProxy().getPersist();
            filter.setHelper(this);
            if(((AbstractFilter)filter).mSet == null)
                filter.setFilter(false); // дефолтное значение фильтра
            return filter;
        }
    }

    // класс для прямого наследования персистентности. В этом случае хелпер не нужен
    abstract class PersistentFilter extends AbstractFilter implements Persistence {
        transient PersistentHelper mHelper;

        @Override
        public void setHelper(PersistentHelper helper) {
            mHelper = helper;
        }

        @Override
        public void setFilter(boolean set) {
            super.setFilter(set);
            mHelper.update();
        }

        @Override
        public void flip() {
            super.flip();
            mHelper.update();
        }
    }
}
