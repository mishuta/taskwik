package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.model.Task;

/**
 * Фильтр текущих задач
 * Если фильтр установлен, фильтруются только задачи для которых срок не установлен или в прошлом
 * Created by Michael Shishkin on 31.07.2017.
 */

@SuppressWarnings("WeakerAccess")
public class ActualFilter extends Persistence.PersistentFilter {

    @Override
    boolean isTaskFiltered(Task task) {
        return  !task.isStateInPostponed();
    }
}
