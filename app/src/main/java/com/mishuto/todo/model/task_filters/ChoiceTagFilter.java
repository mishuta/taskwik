package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.model.tags_collections.Tags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.mishuto.todo.model.tags_collections.Tags.EMPTY_TAG;

/**
 * Имплементация фильтров, содержащих теги: Context, Executor, Project
 * Created by Michael Shishkin on 02.02.2019.
 */
public abstract class ChoiceTagFilter<S> extends ChoiceFilter<S, StringContainer> {

    public abstract Tags getTagCollection();                    // конкретная коллекция тегов
    public abstract TagItem createItem(StringContainer tag);    // создание элемента Choice

    // преобразуем коллекцию тегов в коллекцию Choice-элементов
    @Override
    public Collection<Item<StringContainer>> getItems() {
        List<Item<StringContainer>> tagItems = new ArrayList<>(getTagCollection().getTags().size());
        for (StringContainer tag : getTagCollection().getTags())
            tagItems.add(createItem(tag));

        return tagItems;
    }

    @Override
    public boolean canBeChosen(S selector) {
        return getTagCollection().contains(selector.toString());
    }

    // класс-заготовка для элемента фильтра
    abstract static class TagItem extends ChoiceItem<StringContainer> {

        TagItem(StringContainer tag) {
            super(tag);
        }

        @Override
        public boolean isMissingItem() {
            return mItem == EMPTY_TAG;
        }

        @Override
        public boolean equals(Object obj) {
            //noinspection unchecked
            return (obj instanceof TagItem) && ((TagItem) obj).mItem.equals(mItem) ;
        }
    }
}
