package com.mishuto.todo.model.events;

import com.mishuto.todo.common.TObjects;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.model.Task;

/**
 * Фасад для оповещения контроллеров и представлений об изменении состояния задачи
 * Предоставляет интерфейс для отправки и получению событий задачи множеству объектов.
 * Отправлять события могут как контроллеры, изменяющие атрибуты задачи, так и модель
 * Приемники событий: контроллеры, фрагменты, TaskEventHandler
 *
 * См. так же комментарий к классу TimerPublisher
 * Created by Michael Shishkin on 13.09.2017.
 */

public class TaskEventsFacade extends ObservableSubject<TaskEventsFacade.TaskObserver> {

    private static TaskEventsFacade sInstance = new TaskEventsFacade();

    public interface TaskObserver extends ObservableSubject.Observer {
        /**
         * Колбэк клиенту о событии задачи
         * @param type Тип события
         * @param task Задача, вызвавшая событие
         * @param extendedData, Дополнительные данные, зависящие от типа события (см. описание для конкретного типа)
         */
        void onTaskEvent(EventType type, Task task, Object extendedData);
    }

    // Типы событий
    public enum EventType {
        TIMER,                  // Событие срабатывания таймера по сроку задачи.            extendedData: TCalendar  - время события
        PREV_STATE,             // Изменение статуса предыдущей задачи.
        LOADED,                 // Задача загружена из БД
        CREATED,                // Задача создана
        DELETED,                // Задача удалена
        S_PERFORMED,            // Изменение статуса ВЫПОЛНЕНО.
        A_TIME,                 // Изменение даты или времени.
        A_TIME_DISCARDS_PREV,   // Установка даты или времени со сбросом предыдущей задачи.
        A_PREV,                 // Изменение предыдущей задачи.
        A_PREV_DISCARDS_TIME,   // Изменение предыдущей задачи со сбросом времени.          extendedData: String название предыдущей задачи
        A_COMMENT,              // Изменение комментария
        A_CONTEXT,              // Изменение контекста
        A_EXECUTOR,             // Изменение исполнителя
        A_ORDER,                // Изменение очереди
        A_PRIORITY,             // Изменение приоритета
        A_PROJECT,              // Изменение проекта
        A_RECURRENCE,           // Изменение повтора
        A_TITLE;                // Изменение названия

        // проверяет: находится ли событие внутри заданного массива (для фильтров событий)
        public boolean in(EventType... eventTypes) {
            return TObjects.in(this, eventTypes);
        }
    }

    public static TaskEventsFacade getInstance() {
        return sInstance;
    }

    private TaskEventsFacade() {}

    public static void send(EventType type, Task task) {
        sInstance.notifyObservers(type ,task, null);
    }

    public static void send(EventType type, Task task, Object extendedData) {
        sInstance.notifyObservers(type ,task, extendedData);
    }

    private void notifyObservers(EventType type, Task task, Object extendedData) {
        notifyAllObservers(type, task, extendedData);
    }

    @Override
    protected void notifyObserver(TaskObserver observer, Object[] param) {
        EventType type = (EventType) param[0];
        Task task = (Task) param[1];
        Object data = param[2];

        TSystem.debug(this, "Notify of " + observer.getClass().getSimpleName() + ". Type: " + type.toString() + ". Task: " + task.toString());
        observer.onTaskEvent(type, task, data);
    }
}
