package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.model.Task;

import java.util.Collection;

/**
 * Интерфейс фильтров, содержащих элементы выбора, принадлежащие конкретному атрибуту задачи, которые могут быть отобраны пользователем.
 * Является подтипом Selectivity. Если в Selectivity можно использовать произвольное значение атрибута (например SearchFilter), то в Choice только фиксированные
 * Created by Michael Shishkin on 28.01.2019.
 */
public interface Choice<S, E> extends Exclusiveness, Selectivity<S> {

    Collection<Item<E>> getItems();     // получить список элементов выбора фильтра
    boolean canBeChosen(S selector);    // Есть ли данный селектор в списке для выбора фильтра
    void putInTask(Task task);          // Инициировать соответствующее фильтру поле задачи селектором фильтра

    /**
     * Интерфейс элемента выбора фильтра
     * @param <E> Тип значения элемента выбора, инкапсулированного в интерфейсе
     */
    interface Item<E> {
        boolean isMissingItem();        // является ли элемент специальным значением, означающим отсутствием значения в атрибуте. Например: EMPTY_TAG
        String missingItemName();       // текстовое отображения специального элемента. Например: "[без исполнителя]"

        boolean isFiltered(Task task);  // содержит ли атрибут задачи данный элемент

        E getValue();                   // получить значение элемента

        @Override
        String toString();              // текстовое отображение элемента. Обычно getValue.toString()
    }


    // получить текущий установленный фильтр
    static Choice getActiveChoiceFilter() {
        Exclusiveness filter = Exclusiveness.getExclusiveFilter();
        return filter instanceof Choice ? (Choice) filter : null;
    }
}
