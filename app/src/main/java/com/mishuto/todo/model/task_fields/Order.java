package com.mishuto.todo.model.task_fields;

/**
 * Очередность задачи среди задач с одинаковым приоритетом - атрибут задачи
 * Created by mike on 19.08.2016.
 */
public enum Order {
    LOW,
    NORMAL,
    HIGH;

    public static final Order DEFAULT_ORDER = NORMAL;       // Дефолтный номер очереди
}
