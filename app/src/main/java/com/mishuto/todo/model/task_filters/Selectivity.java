package com.mishuto.todo.model.task_filters;

/**
 * Интерфейс фильтров, которые используют для фильтрации задач значение какого-то (одного или нескольких) атрибута задачи.
 * Значение используется как селектор для отбора задач
 * Created by Michael Shishkin on 18.05.2018.
 */
public interface Selectivity<S> extends TaskFilter {
    void setSelector(S selector);
    S getSelector();

    String toString(S selector);
}
