package com.mishuto.todo.model.task_fields;

import android.support.annotation.NonNull;

import com.mishuto.todo.common.ResourceConstants;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.database.DBObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;

import static com.mishuto.todo.common.TCalendar.now;
import static java.util.Calendar.DATE;

/**
 * Обобщенный класс, для хранения времени события задачи (срок задачи, повторение)
 * Класс может сериализоваться, сохраняя только атрибуты самого класса, не включая суперкласс
 * Created by Michael Shishkin on 26.05.2017.
 */

public class TaskTime extends DBObject implements Comparable<TaskTime>, Serializable {
    private TCalendar mCalendar;            // Дата и время события
    private Boolean isTimeSet = false;      // флаг установки временнОй части даты

    public TaskTime() {}

    public TaskTime(TCalendar calendar, boolean timeSet) {
        mCalendar = calendar;
        isTimeSet = timeSet;
    }

    public TCalendar getCalendar() {
        return mCalendar;
    }

    public void setCalendar(TCalendar calendar, boolean timeSet) {
        mCalendar = calendar;
        isTimeSet = timeSet;
        update();
    }

    public Boolean isDateSet() {
        return mCalendar != null;
    }

    public Boolean isTimeSet() {
        return isTimeSet;
    }

    public void setTimePart(int hour, int minute){
        if (!isDateSet()) mCalendar = now();
        mCalendar.setTimePart(hour, minute);
        isTimeSet = true;
        update();
    }

    public void setDate(int year, int month, int day) {
        if(!isDateSet())
            mCalendar = new TCalendar(year, month, day);
        else
            mCalendar.setDatePart(year, month, day);
        update();
    }

    public int getHour()  {
        if (!isDateSet() || !isTimeSet())
            throw new AssertionError("Time wasn't set");
        return mCalendar.get(Calendar.HOUR_OF_DAY);
    }

    public int getMinute()  {
        if (!isDateSet() || !isTimeSet())
            throw new AssertionError("Time wasn't set");
        return mCalendar.get(Calendar.MINUTE);
    }

    public int getYear() {
        if(!isDateSet()) throw new AssertionError("Date was not set");
        return mCalendar.get(Calendar.YEAR);
    }

    public int getMonth()  {
        if(!isDateSet()) throw new AssertionError("Date was not set");
        return mCalendar.get(Calendar.MONTH);
    }

    public int getDay() {
        if(!isDateSet()) throw new AssertionError("Date was not set");
        return mCalendar.get(Calendar.DAY_OF_MONTH);
    }

    public void resetDate(){
        mCalendar = null;
        resetTime();
        update();
    }

    public void resetTime() {
        isTimeSet = false;
        update();
    }

    @Override
    public int compareTo(@NonNull TaskTime o) {
        if(mCalendar == null && o.mCalendar == null)
            return 0;

        if(mCalendar != null) {
            if (o.mCalendar == null)
                return -1;                                              // задача с установленной датой - выше задачи без даты

            else if (mCalendar.intervalFrom(DATE, o.mCalendar) == 0)    // если две задачи приходятся на одну дату
                if (isTimeSet && !o.isTimeSet)                          // и у одной время установлено, а у другой - нет
                    return mCalendar.compareTo(now());                  // то задача с установленным временем - выше, либо ниже задачи с такой же датой без времени
                else if (!isTimeSet && o.isTimeSet)                     // в зависимости от того в прошлом она или в будущем
                    return now().compareTo(mCalendar);                  // т.е. с задача, которая не имеет время  = имеет текущее время (актуально для сегодняшних задач)
                else if(!isTimeSet)                                     // если у обоих задач время неустановленно = они равнозначны.
                    return 0;

            return mCalendar.compareTo(o.mCalendar);
        }
        else
            return 1;                                                   // задача без даты - всегда ниже задачи с датой
    }

    // Возвращает true, если событие на данный момент уже наступило
    public boolean hasCome() {
        return now().compareTo(isTimeSet ? mCalendar : mCalendar.clone().clipTo(DATE)) >= 0;
    }


    @Override
    public String toString() {
        if(!isDateSet())
            return ResourceConstants.Recurrence.NOT_SET;

        if(!isTimeSet)
            return mCalendar.getSpeakingDate();

        return mCalendar.getSpeakingString();
    }

    // колбэк методы сериализации.
    // при сериализации сохраняются только атрибуты данного класса, без суперкласса

    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        stream.writeObject(mCalendar);
        stream.writeBoolean(isTimeSet);
    }

    private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
        mCalendar = (TCalendar) stream.readObject();
        isTimeSet = stream.readBoolean();
    }
}
