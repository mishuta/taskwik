package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.task_fields.TaskTime;

import java.util.Calendar;

import static com.mishuto.todo.common.ResourceConstants.Recurrence.DAILY_FACTOR_ITEMS;
import static com.mishuto.todo.common.ResourceConstants.Recurrence.DAILY_FACTOR_VALUES;
import static com.mishuto.todo.common.ResourceConstants.Recurrence.HOURLY_FACTOR_ARRAY;


/**
 * Сегмент, содержащий настройки для дневных повторений
 *
 * Created by Michael Shishkin on 29.11.2016.
 */
public class Day extends AbstractSegment {

    // Часовые кратности
    public enum HourlyFactor {
        SECOND_HOUR(2), FORTH_HOUR(4), SIXTH_HOUR(6), EIGHTH_HOUR(8);
        private int factor;

        HourlyFactor(int factor) {
            this.factor = factor;
        }

        public int getFactor() { return  factor; }

        @Override
        public String toString() {
            return HOURLY_FACTOR_ARRAY[ordinal()];
        }
    }

    // Дневные кратности
    public enum DailyFactor {
        EVERY_DAY(1), SECOND_DAY(2), THIRD_DAY(3), FOURTH_DAY(4), FIFTH_DAY(5), TENTH_DAY(10);
        private int factor;

        DailyFactor(int factor) {
            this.factor = factor;
        }

        public int getFactor() { return  factor; }

        @Override
        public String toString() {
            return DAILY_FACTOR_ITEMS[ordinal()];
        }
    }

    private Boolean isDaySelected;              // выбрана дневная (true) или часовая (false) кратность
    private transient boolean mOnlyWorkTime;    // часовые повторы срабатывают только в рабочее (true) или в любое (false) время

    private HourlyFactor mHourlyFactor;         //часовая кратность
    private DailyFactor mDailyFactor;           //дневная кратность

    private Day() { this(null); }

    Day(TaskTime startTime) {
        super(startTime);
        // умолчательное состояние объекта
        mOnlyWorkTime = Settings.getWorkTime().isOn.get(); // часовые интервалы срабатывают только в рабочее время если установлен рабочий интервал в настройках
        mHourlyFactor = HourlyFactor.SECOND_HOUR;
        mDailyFactor = DailyFactor.EVERY_DAY;

        isDaySelected = true;
    }

    @Override
    public TCalendar getNextEvent() {
        TCalendar startTime = getStartTime();

        if (isDaySelected) {
            startTime.add(Calendar.DATE, mDailyFactor.getFactor());
            setTimePart(startTime);
        }
        else {
            startTime.add(Calendar.HOUR_OF_DAY, mHourlyFactor.getFactor());

            if (mOnlyWorkTime)
                startTime = Settings.getWorkTime().getNearestWorkDate(startTime);

        }
        return startTime;
    }

    public HourlyFactor getHourlyFactor() {
        return mHourlyFactor;
    }

    public void setHourlyFactor(HourlyFactor factor) {
        mHourlyFactor = factor;
        update();
    }

    public DailyFactor getDailyFactor() {
        return mDailyFactor;
    }

    public void setDailyFactor(DailyFactor factor) {
        mDailyFactor = factor;
        update();
    }

    public boolean isOnlyWorkTime() {
        return mOnlyWorkTime;
    }

    public void setOnlyWorkTime(boolean onlyWorkTime) {
        mOnlyWorkTime = onlyWorkTime;
    }

    public boolean isDaySelected() {
        return isDaySelected;
    }

    public void setDaySelected(boolean daySelected) {
        isDaySelected = daySelected;
        update();
    }

    @Override
    public String getShortValue() {
        return DAILY_FACTOR_VALUES[isDaySelected ? mDailyFactor.ordinal() : 0];
    }
}
