package com.mishuto.todo.model.tags_collections;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.aggregators.DBHashMap;
import com.mishuto.todo.database.aggregators.DBMap;
import com.mishuto.todo.database.aggregators.DBTreeMap;
import com.mishuto.todo.model.Settings;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;

import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;

import static com.mishuto.todo.common.TCalendar.now;

/**
 * Класс - синглтон содержит список исполнителей задач.
 * По каждому исполнителю собирается статистика, позволяющая со временем очищать список от неиспользуемых исполнителей
 *
 * Created by mike on 30.08.2016.
 */
public class Executors extends DBObject implements Tags {
    private DBTreeMap<StringContainer, Statistics> mExecutorMap;   // Список исполнителей и статистика по исполнителю
    private static Executors sExecutors;        // Указатель на единственный объект класса

    private final static int DAYS_RATIO = 2;    // Константы, для эмпирического  алгоритма удаления исполнителя, спустя n дней после последнего использования:
    private final static int DAYS_MINIMAL = 12; // n > DAYS_RATIO * количество использований + DAYS_MINIMAL

    // получение ссылки на класс
    public static Executors getInstance() {
        if (sExecutors == null) sExecutors = new Executors();
        return sExecutors;
    }

    // Класс сбора статистики по исполнителю для его последующего удаления
    static class Statistics extends DBObject {
        Integer mUsedCount = 1;                 // Количество обращений
        TCalendar mLastUsed = now();  // Дата последнего обращения

        // обновление статистики по исполнителю
        private void touch() {
            mUsedCount++;
            mLastUsed = now();
            update();
        }
    }

    private Executors() {
        mExecutorMap = new DBTreeMap<>();
        loadAsSingleton();
        if(mExecutorMap.isEmpty())
            mExecutorMap.put(EMPTY_TAG, new Statistics());
    }

    public DBMap<StringContainer, Statistics> getExecutorMap() {
        return mExecutorMap;
    }

    @Override
    public Set<StringContainer> getTags() {
        return mExecutorMap.keySet();
    }

    // добавить нового исполнителя
    @Override
    public boolean add(StringContainer s){
        if( s==null || s.isEmpty() )
            return false;

        s.trim();
        Statistics stat = mExecutorMap.get(s);
        if(stat==null){
            stat = new Statistics();
            mExecutorMap.put(s,stat);
        }
        else {
            stat.touch();
        }

        return true;
    }

    @Override
    public boolean remove(StringContainer tag) {
        return mExecutorMap.remove(tag) != null;
    }

    @Override
    public boolean rename(StringContainer tag, String newTagName) {
        if(TextUtils.empty(newTagName) || contains(newTagName))
            return false;

        mExecutorMap.remove(tag);
        tag.setString(newTagName);
        add(tag);
        return true;
    }

    @Override
    public boolean contains(String tagName) {
        StringContainer tag = new StringContainer(tagName);                     // т.к. сопоставление в StringContainer и мапе Case Insensitive
        StringContainer existingName = getOriginal(tag);          // мы должны проверить равенство строк как Case Sense
        return existingName != null && existingName.toString().equals(tagName); // чтобы убедиться что это действительно один и тот же тег
    }

    // получение оригинального тега из множества исполнителей, равного данному
    public StringContainer getOriginal(StringContainer tag) {
        return getExecutorMap().getKeyObject(tag);
    }

    // Очистить список от неиспользуемых исполнителей.
    public int clearUnused() {
        if(!Settings.getEtc().executorsAutoClear.get())
            return 0;

        int count = 0;

        // Проходим по всему списку исполнителей
        for(Iterator <DBHashMap.Entry<StringContainer, Statistics>> iterator = mExecutorMap.entrySet().iterator(); iterator.hasNext(); ) {
            DBHashMap.Entry<StringContainer, Statistics> entry = iterator.next();
            final Statistics stat = entry.getValue();           // Статистика по исполнителю
            final StringContainer executor = entry.getKey();    // Исполнитель

            if(executor == EMPTY_TAG)                           // EMPTY_TAG не чистится
                continue;

            boolean isUsing = false;                            // Флаг, что исполнитель используется в списке задач

            //Проходим по списку задач, пока не встретим текущего исполнителя
            for(Task t : TaskList.getTasks()) {
                if(t.getExecutor() == executor) {
                    isUsing = true;
                    break;
                }
            }

            if(!isUsing) {                                                          // Если текущий исполнитель не используется
                final int days = now().intervalFrom(Calendar.DATE, stat.mLastUsed); // вычисляем интервал после последнего использования
                if (days > DAYS_RATIO * stat.mUsedCount + DAYS_MINIMAL) {           // Если использовался редко и давно
                    TSystem.debug(this, entry.getKey() + " was removed");
                    iterator.remove();                                              // тогда его удаляем
                    count++;
                }
            }
        }
        return count;
    }
}

