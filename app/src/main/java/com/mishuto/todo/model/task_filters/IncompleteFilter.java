package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.model.Task;


/**
 * Фильтр незавершенных задач
 * Если фильтр установлен, фильтруются только незавершенные задачи
 * Created by Michael Shishkin on 31.07.2017.
 */
 public class IncompleteFilter extends Persistence.PersistentFilter {

    @Override
    boolean isTaskFiltered(Task task) {
        return !task.isStateCompleted();
    }


}
