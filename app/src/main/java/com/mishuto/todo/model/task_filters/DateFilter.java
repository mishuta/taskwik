package com.mishuto.todo.model.task_filters;

import android.support.annotation.NonNull;

import com.mishuto.todo.common.DateFormatWrapper;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;

import java.util.Calendar;
import java.util.Collection;
import java.util.TreeSet;

import static com.mishuto.todo.common.ResourceConstants.Filters.EMPTY_DATE_NAME;
import static com.mishuto.todo.common.TObjects.eq;

/**
 * Фильтр по дате задачи
 * Created by Michael Shishkin on 27.01.2019.
 */
public class DateFilter extends ChoiceFilter<TCalendar, TCalendar> {
    @Override
    boolean isTaskFiltered(Task task) {
        return new DateItem(mSelector).isFiltered(task);
    }

    @Override
    public Collection<Item<TCalendar>> getItems() {
        TreeSet<Item<TCalendar>> items = new TreeSet<>();
        items.add(new DateItem(null));

        for (Task task : TaskList.getTasks())
            if(task.getTaskTime().isDateSet())
                items.add(new DateItem(task.getTaskTime().getCalendar().clone().clipTo(Calendar.DATE)));

        return items;
    }

    @Override
    public String toString(TCalendar selector) {
        return new DateItem(selector).toString();
    }

    @Override
    public boolean canBeChosen(TCalendar selector) {
        return getItems().contains(new DateItem(selector));
    }

    @Override
    public void putInTask(Task task) {
        if(mSelector != null)
            task.getTaskTime().setCalendar(mSelector.clone(), false);
    }

    /* Класс элемента списка дат фильтра */
    static class DateItem extends ChoiceItem<TCalendar> implements Comparable<DateItem> {

        DateItem(TCalendar item) {
            super(item);
        }

        @Override
        public boolean isMissingItem() {
            return mItem == null;
        }

        @Override
        public String missingItemName() {
            return EMPTY_DATE_NAME;
        }

        @Override
        public String toString() {
            return isMissingItem() ? missingItemName() : DateFormatWrapper.getSpeakingDate(mItem);
        }

        @Override
        public boolean isFiltered(Task task) {
            if(isMissingItem())
                return  !task.getTaskTime().isDateSet();                                        // если выбран [без срока] фильтруются задачи без календаря
            else if(task.getTaskTime().isDateSet())
                return task.getTaskTime().getCalendar().intervalFrom(Calendar.DATE, mItem)==0;  // иначе фильтруются задачи с тем же сроком без учета даты
            else
                return false;                                                                   // а задачи без срока не фильтруются
        }

        // сортировка в порядке возрастания дат
        @Override
        public int compareTo(@NonNull DateItem o) {
            return mItem == null ? -1 :
                    o.mItem == null ? 1 :
                    mItem.compareTo(o.mItem);
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof DateItem && eq(mItem, ((DateItem) obj).mItem);
        }
    }
}
