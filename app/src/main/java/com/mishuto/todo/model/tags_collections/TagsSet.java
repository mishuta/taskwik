package com.mishuto.todo.model.tags_collections;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.aggregators.DBTreeSet;

import java.util.TreeSet;

/**
 * Класс для хранения множества любых уникальных текстовых тегов.
 * Created by Michael Shishkin on 10.01.2017.
 */
public abstract class TagsSet extends DBObject implements Tags {
    private DBTreeSet<StringContainer> mSet;        // множество тегов. Выбран TreeSet, поскольку теги обычно отображаются упорядоченными в интерфейсе
    transient private StringContainer mLastAdded;   // последний добавленный элемент в множество

    // Конструктор пакетно-закрытый, т.к. объкт должен создаваться только в сабклассах, оформленных как синглтон
    TagsSet() {
        mSet = new DBTreeSet<>();
        loadAsSingleton();
        if(mSet.isEmpty()) {
            mSet.add(EMPTY_TAG);
            onFirstCreate();
        }
    }

    // событие первого создания объекта после очистки БД
    void onFirstCreate() {}

    // Проверка содержит ли множество данную строку
    public boolean contains(String tagName) {
        return mSet.contains(new StringContainer(tagName));
    }

    // добавление тега из строки
    public boolean add(String tagName) {
        return add(new StringContainer(tagName));
    }

    // добавление тега из StringContainer
    public boolean add(StringContainer newTag) {
        if(newTag == null || mSet.contains(newTag))
            return false;

        newTag.trim();
        mLastAdded = newTag;
        mSet.add(mLastAdded);
        return true;
    }

    // удаление тэга
    public boolean remove(StringContainer tag) {
        TSystem.approveThat(mSet.getRecordID() == 0 || mSet.getRefCounter() == 1, "attempt to delete the tag, possibly belonging to the task");
        return mSet.remove(tag);
    }

    // изменение тега
    public boolean rename(StringContainer tag, String newName) {
        if(TextUtils.empty(newName) || contains(newName))
            return false;

        mSet.remove(tag);
        tag.setString(newName);
        mSet.add(tag);
        return true;
    }

    // возращает последний добавленный тег
    public StringContainer getLastAdded() {
        return mLastAdded;
    }

    @Override
    public TreeSet<StringContainer> getTags() {
        return mSet.getShallowCopy();
    }
}
