package com.mishuto.todo.model.task_filters;

/**
 * Интерфейс, который помечает "эксклюзивные фильтры".
 * Это такие фильтры, из которых не более чем один может быть выбран.
 *
 * Created by Michael Shishkin on 28.03.2018.
 */

public interface Exclusiveness extends TaskFilter {

    String toString();

    //метод гарантирует, что только один эксклюзивный фильтр может быть установлен. Должен вызываться в setFilter()
    static void setExclusiveFilter(Exclusiveness filter, boolean isSet) {
        if(isSet)
            for (TaskFilter f : FilterManager.filters())
                if (f != filter && f instanceof Exclusiveness)
                    f.setFilter(false);
    }

    // получение установленного эксклюзивного фильтра
    static Exclusiveness getExclusiveFilter() {
        for (TaskFilter f : FilterManager.filters())
            if (f instanceof Exclusiveness && f.isFilterSet())
                return (Exclusiveness) f;

        return null;
    }

    // имплементация для прямого наследования
    abstract class ExclusivenessImp extends AbstractFilter implements Exclusiveness {
        @Override
        public void setFilter(boolean set) {                    // при установке фильтра сбрасываются все остальные
            super.setFilter(set);
            setExclusiveFilter(this, set);
        }
    }
}
