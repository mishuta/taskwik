package com.mishuto.todo.model.tags_collections;

import static com.mishuto.todo.common.ResourceConstants.START_CONTEXTS;

/**
 * Класс для хранения множества контекстов всех задач
 * Created by Michael Shishkin on 12.01.2017.
 */

public class ContextsSet extends TagsSet {
    private static ContextsSet sContextsSet;

    public static ContextsSet getInstance() {
        if(sContextsSet == null)
            sContextsSet = new ContextsSet();
        return sContextsSet;
    }

    @Override
    void onFirstCreate() {
        for(String c : START_CONTEXTS)  // Создаются дефолтные контексты
            add(c);
    }
}
