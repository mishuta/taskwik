package com.mishuto.todo.model;

import com.mishuto.todo.common.PersistentValues;
import com.mishuto.todo.common.PersistentValues.PVBoolean;
import com.mishuto.todo.common.PersistentValues.PVInt;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.Time;

import java.util.Calendar;

import static java.util.Calendar.MINUTE;


/**
 * Синглтон для хранения объекта с пользовательскими настройками
 * Created by Michael Shishkin on 25.11.2016.
 */

public class Settings {
    private static final PersistentValues PV = PersistentValues.getFile("Settings");    // все настройки хранятся как PersistentValue в файле Settings
    private static Settings sSettings = new Settings();

    private WorkTime mWorkTime = new WorkTime();                                        // Рабочие часы. Используется при настройке диалога отложить
    private NotificationSettings mNotificationSettings = new NotificationSettings();    // настройки уведомлений
    private Etc mEtc = new Etc();

    public static WorkTime getWorkTime() {
        return sSettings.mWorkTime;
    }

    public static NotificationSettings getNotificationSettings() {
        return sSettings.mNotificationSettings;
    }

    public static Etc getEtc() {
        return sSettings.mEtc;
    }

    // Класс, задающий рабочие часы. Используется при настройке повторений
    public static class WorkTime {
        final static int MINIMAL_DURATION = 4 * 60;     // минимальная длина дня в минутах
        final static int MAXIMAL_DURATION = 24 * 60;    // максимальная длина дня в минутах

        public PVBoolean isOn = PV.getBoolean("WorkTime_IsOn", true);                   // режим "только рабочие часы" включен
        private PVInt mStartWork = PV.getInt("StartWork", new Time(9, 0).toInteger());  // начало рабочего дня (9:00)
        private PVInt mEndWork = PV.getInt("EndWork", new Time(18, 0).toInteger());     // конец рабочего дня (18:00)
        public PVBoolean onlyWorkDays = PV.getBoolean("onlyWorkDays", false);           // режим "только в рабочие дни" - выключен

        public Time getStartWork() {
            return new Time(mStartWork.get());
        }

        public Time getEndWork() {
            return new Time(mEndWork.get());
        }

        public void setBeginningWork(Time startWork) {
            if(checkDuration(startWork, getEndWork()))
                mStartWork.set(startWork.toInteger());
        }

        public void setEndWork(Time endWork) {
            if(checkDuration(getStartWork(), endWork))
                mEndWork.set(endWork.toInteger());
        }

        //проверка корректности длительности дня
        private static boolean checkDuration(Time begin, Time end) {
            int d = end.inMinutes() - begin.inMinutes();
            return d >= MINIMAL_DURATION && d <= MAXIMAL_DURATION;
        }

        // Возвращает ближайшее рабочее время, к заданной дате
        public TCalendar getNearestWorkDate(TCalendar startDate) {

            if(!isOn.get())
                return startDate;                                   // если рабочее время не включено, дата не двигается

            TCalendar workDate = startDate.clone();

            if (onlyWorkDays.get()) {                               // если включены рабочие дни ...
                int dayOfWeek = startDate.get(Calendar.DAY_OF_WEEK);

                if(dayOfWeek == Calendar.SATURDAY) {                // ... а дата попадает на субботу ...
                    workDate.add(Calendar.DATE, 2);
                    setBeginningWork(workDate);
                }

                else if(dayOfWeek == Calendar.SUNDAY) {             // ... или воскресенье ...
                    workDate.add(Calendar.DATE, 1);                 // ... двигаем на начало понедельника
                    setBeginningWork(workDate);
                }
            }

            if(getStartWork().compareTo(workDate) > 0)              // если дата раньше рабочего дня
                setBeginningWork(workDate);                         // двигаем часы на начало рабочего дня


            else if(getEndWork().compareTo(workDate) < 0) {         // если дата позже конца рабочего дня - двигаем на начало следующего рабочего дня
                if (onlyWorkDays.get() && startDate.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
                    workDate.add(Calendar.DATE, 3);
                else
                    workDate.add(Calendar.DATE, 1);

                setBeginningWork(workDate);
            }

            return workDate;
        }

        // Установка времени calendar на начало рабочего дня
        private void setBeginningWork(TCalendar calendar) {
            calendar.setTimePart(getStartWork().getHour(), getStartWork().getMinute());
            calendar.clipTo(MINUTE);
        }
    }

    // настройки уведомлений
    public static class NotificationSettings {
        public PVBoolean isOn = PV.getBoolean("Notifies_IsOn", true);
        public PVBoolean doSound = PV.getBoolean("sound", true);
        public PVBoolean doVibrate = PV.getBoolean("vibrate", true);
        public PVBoolean doLight = PV.getBoolean("light", true);
    }

    // прочие
    public static class Etc  {
        public PVBoolean executorsAutoClear = PV.getBoolean("executorsAutoClear", true);
    }

    private Settings() { }  // приватный конструктор синглтона
}
