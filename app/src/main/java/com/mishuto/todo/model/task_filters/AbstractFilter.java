package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.model.Task;

/**
 * Дефолтная реализация интерфейса TaskFilter
 * Created by Michael Shishkin on 19.11.2017.
 */

abstract class AbstractFilter implements TaskFilter {

    Boolean mSet = false;   // состояние фильтра: установлен/сброшен. По умолчанию фильтр сброшен => все задачи фильтруются.
    private transient OnFilterChangeListener mChangeListener;

    //отфильтровывается ли задача в конкретном фильтре, без учета флажка состояния
    abstract boolean isTaskFiltered(Task task);

    //отфильтровывается ли задача фильтром
    @Override
    public boolean isFiltered(Task task) {
        return !mSet || isTaskFiltered(task);   // если фильтр сброшен, задача всегда считается отфильтрованной
    }

    @Override
    public boolean isFilterSet() {
        return mSet;
    }

    // установка/сброс фильтра вызывает событие его изменения, но только если он был изменен
    @Override
    public void setFilter(boolean set) {
        if(mSet != set) {
            mSet = set;
            onFilterChanged();
        }
    }

    @Override
    public void flip() {
        setFilter(!mSet);
    }

    @Override
    public void setChangeListener(OnFilterChangeListener changeListener) {
        mChangeListener = changeListener;
    }

    void onFilterChanged() {
        if(mChangeListener != null)
            mChangeListener.onFilterChanged(this);
    }
}
