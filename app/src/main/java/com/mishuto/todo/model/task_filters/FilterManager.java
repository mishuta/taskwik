package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;

import java.util.ArrayList;
import java.util.List;

import static com.mishuto.todo.model.task_filters.Persistence.createPersistentFilter;

/**
 * Класс для порождения и манипуляции фильтрами.
 * Каждый фильтр -  объект-одиночка
 * Created by Michael Shishkin on 31.07.2017.
 */

public enum FilterManager {

    INCOMPLETE(createPersistentFilter(IncompleteFilter.class)),
    CONTEXT(createPersistentFilter(ContextFilter.class)),
    ACTUAL(createPersistentFilter(ActualFilter.class)),
    EXECUTOR(createPersistentFilter(ExecutorFilter.class)),
    PROJECT(createPersistentFilter(ProjectFilter.class)),
    DATE(createPersistentFilter(DateFilter.class)),
    UNHIDDEN(new UnHidden()),
    SEARCH(new SearchFilter()),
    ACTIVATED(new ActivatedTasksFilter());

    TaskFilter mFilter;

    FilterManager(TaskFilter filter) {
        mFilter = filter;
    }

     public TaskFilter filter() {
        return mFilter;
    }

    public static IncompleteFilter incomplete() {
        return (IncompleteFilter) INCOMPLETE.filter();
    }

    public static ContextFilter context() {
        return (ContextFilter) CONTEXT.filter();
    }

    public static ActualFilter actual() {
        return (ActualFilter) ACTUAL.filter();
    }

    public static ExecutorFilter executor() {
        return (ExecutorFilter) EXECUTOR.filter();
    }

    public static ProjectFilter project() {
        return (ProjectFilter) PROJECT.filter();
    }

    public static DateFilter date() {
        return (DateFilter) DATE.filter();
    }

    public static UnHidden unhidden() {
        return (UnHidden) UNHIDDEN.filter();
    }

    public static SearchFilter search() {
        return (SearchFilter) SEARCH.filter();
    }

    public static ActivatedTasksFilter activated() {
        return (ActivatedTasksFilter) ACTIVATED.filter();
    }

    private static List<TaskFilter> sFilters;

    static {
        sFilters = new ArrayList<>();
        for(FilterManager fl : values())
            sFilters.add(fl.filter());
    }

    public static Iterable<TaskFilter> filters() {
        return sFilters;
    }

    // получение объекта FilterManager по содержащемуся в нем фильтру
    public static FilterManager getFilterTag(TaskFilter filter) {
        for (FilterManager filterManager : FilterManager.values())
            if(filterManager.filter() == filter)
                return filterManager;

        throw new AssertionError("unexpected filter");
    }

    // наложение фильтров на задачу
    @SuppressWarnings("ConstantConditions")
    public static boolean applyAllFilters(Task task) {
        boolean filtered = true;
        for(FilterManager filterManager : FilterManager.values())
            if( !(filtered &= filterManager.filter().isFiltered(task)))
                break;

        return filtered;
    }

    //возвращает отфильтрованный список задач
    public static List<Task> computeFilteredTaskList() {
        List<Task> filteredList = new ArrayList<>();

        for (Task t: TaskList.getTasks()) {
            if(applyAllFilters(t))
                filteredList.add(t);
        }

        return filteredList;
    }


    // подписать клиента на события от всех фильтров
    public static void setChangeFilterListener(AbstractFilter.OnFilterChangeListener listener) {
        for (TaskFilter filter : filters())
            filter.setChangeListener(listener);
    }
}
