package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.Time;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.model.task_fields.TaskTime;

import java.util.Calendar;

import static com.mishuto.todo.common.TCalendar.now;

/**
 * абстрактный класс, базовый для каждого сегмента
 * содержит общие для всех страниц класс Time и подготовка расчета следующего события для сабклассов
 *
 * Created by Michael Shishkin on 29.11.2016.
 */
public abstract class AbstractSegment extends DBObject {

    private Time mTime;             // Время суток срабатывания (в каждом сегменте)
    private Boolean isTimeSet;      // Время суток установленно
    private TaskTime mStartTime;    // Время, от которого отсчитываются повторы - прошедший момент перехода задачи в завершенную

    AbstractSegment(TaskTime startTime ) {
        mTime = new Time(now().get(Calendar.HOUR_OF_DAY), 0);   // по умолчанию выставляется текущее время, обрезанное до часов.
        isTimeSet = false;
        mStartTime = startTime;
    }

    // Время ближайшего срабатывания повторения.
    // Зависит от стартового события mStartTime и может оказаться в прошлом.
    // В этом случае считается, что повтор сработал
    abstract TCalendar getNextEvent();

    // Короткое текстовое значение повторяемости сегмента.
    // Примеры: "каждый 2-й день", "ПН, СР"
    abstract public String getShortValue();

    // добавляет в кадендарь часы и минуты из объекта Time
    void setTimePart(TCalendar calendar) {
        if (isTimeSet)
            calendar.setTimePart(mTime.getHour(), mTime.getMinute());
    }

    // Вычисление начального времени для повторения:
    // Момент перехода задачи в завершенную, если установлено, или текущее время в противном случае.
    TCalendar getStartTime() {
        return mStartTime.isDateSet() ? mStartTime.getCalendar().clone() : now();
    }

    public Time getTime() {
        return mTime;
    }

    public Boolean isTimeSet() {
        return isTimeSet;
    }

    public void setTimeFlag(Boolean timeSet) {
        isTimeSet = timeSet;
        update();
    }

    public void setTime(Time time) {
        mTime = time;
        isTimeSet = time != null;
        update();
    }
}
