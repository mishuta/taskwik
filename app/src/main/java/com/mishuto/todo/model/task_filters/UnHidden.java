package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.model.Task;

import java.util.HashSet;
import java.util.Set;

/**
 * Фильтр задач, скрываемых от пользователя. Фильтрует только нескрытые задачи
 * Фильтр скрыт от пользователя и всегда включен. Позволяет скрыть "удаленные" пользователем, но еще не удаленные физически (для реализации undo) задачи
 * Created by Michael Shishkin on 23.08.2017.
 */
 public class UnHidden extends AbstractFilter {

    private HashSet<Task> mHiddenTasks = new HashSet<>();  // множество задач, помеченных к удалению

    UnHidden() {
        mSet = true;    // всегда установлен
    }

    @Override
    boolean isTaskFiltered(Task task) {
        return !mHiddenTasks.contains(task);
    }

    public void hide(Task task) {
        mHiddenTasks.add(task);
        onFilterChanged();
    }

    public void show(Task task) {
        mHiddenTasks.remove(task);
        onFilterChanged();
    }

    public boolean anyHiddenTasks() {
        return mHiddenTasks.size() > 0;
    }

    public Set<Task> getHiddenTasks() {
        return mHiddenTasks;
    }
}
