package com.mishuto.todo.model.tags_collections;

/**
 * Класс для хранения списка всех проектов
 * Created by Michael Shishkin on 12.01.2017.
 */

public class ProjectsSet extends TagsSet {
    private static ProjectsSet sProjectsSet;

    public static ProjectsSet getInstance() {
        if(sProjectsSet == null)
            sProjectsSet = new ProjectsSet();
        return sProjectsSet;
    }

}
