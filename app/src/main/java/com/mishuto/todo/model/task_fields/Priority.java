package com.mishuto.todo.model.task_fields;

import static com.mishuto.todo.common.ResourceConstants.PRIORITY_TITLES;


/**
 * Приоритеты задачи
 * Created by mike on 19.08.2016.
 */
public enum Priority {

    URGENTLY,       // Срочно
    DO_TODAY,       // Сделать сегодня
    IMPORTANT,      // Важно, несрочно
    NO_IMPORTANT;   // Неважно, несрочно

    public final static Priority DEFAULT_PRIORITY = DO_TODAY;                                           // Приоритет задачи по умолчанию
    public final static Priority DEFAULT_NEW_PRIORITY = DO_TODAY;                                       // Приоритет задачи по умолчанию после наступления события

    public final String title = PRIORITY_TITLES[ordinal()];                                             // Наименование приоритета
}
