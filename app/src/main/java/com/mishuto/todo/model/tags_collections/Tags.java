package com.mishuto.todo.model.tags_collections;

import com.mishuto.todo.common.ImmutableStringContainer;
import com.mishuto.todo.common.StringContainer;

import java.util.Set;

/**
 * Интерфейс объекта, содержащего коллекцию тэгов
 * Created by Michael Shishkin on 17.07.2017.
 */

public interface Tags {
    boolean add(StringContainer newTag);                    // добавить тэг в коллекцию с данным названием
    boolean remove(StringContainer tag);                    // удалить тэг из коллекции
    boolean rename(StringContainer tag, String newTagName); // переименовать тэг
    boolean contains(String tagName);                       // содержит ли коллекция тэг с данным названием
    Set<StringContainer> getTags();                         // множество тэгов

    //Пустой тэг, используется как дефолтное значение при создании задач и для фильтрации задач без тегов
    StringContainer EMPTY_TAG = ImmutableStringContainer.getObject("");
}
