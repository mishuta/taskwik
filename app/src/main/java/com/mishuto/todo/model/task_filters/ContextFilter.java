package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.common.StringContainer;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.database.aggregators.DBTreeSet;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.tags_collections.ContextsSet;
import com.mishuto.todo.model.tags_collections.Tags;

import static com.mishuto.todo.common.ResourceConstants.Filters.EMPTY_CONTEXT_NAME;
import static com.mishuto.todo.model.tags_collections.Tags.EMPTY_TAG;

/**
 * Фильтр списка задач по контексту
 * Created by Michael Shishkin on 31.07.2017.
 */
@SuppressWarnings("WeakerAccess")
public class ContextFilter extends ChoiceTagFilter<DBTreeSet<StringContainer>> {

    ContextFilter() {
        if(mSelector == null)                         // инициируем селектор пустым мгожеством при первичном создании фильтра в базе.
            mSelector = new DBTreeSet<>();
    }


    //задача отфильтруется, если существует пересечение списка контекстов между множествами selector и task.mContext
    @Override
    boolean isTaskFiltered(Task task) {
        if(task.getContext().isEmpty())
            return mSelector.contains(EMPTY_TAG);

        boolean filtered = false;
        for(StringContainer context: task.getContext())
            if(mSelector.contains(context)) {
                filtered = true;
                break;
            }

        return filtered;
    }

    @Override
    public boolean canBeChosen(DBTreeSet<StringContainer> selector) {
        return  ContextsSet.getInstance().getTags().containsAll(selector);
    }

    @Override
    public void setFilter(boolean set) {
        if(!set && !mSelector.isEmpty() )    // при сбросе фильтра -
            mSelector.clear();               // очищаем список фильтруемых тегов (апдейт в следующей команде)

        super.setFilter(set);
    }

    // возвращает клон списка тегов (сами теги не клонируются)
    @Override
    public DBTreeSet<StringContainer> getSelector() {
        DBTreeSet<StringContainer> clonedSelector = new DBTreeSet<>();
        clonedSelector.setCollection(mSelector.getShallowCopy());
        return clonedSelector;
    }

    // представление селектора в виде строки.
    @Override
    public String toString(DBTreeSet<StringContainer> selector) {
        String s = TextUtils.itrToString(selector.toArray(), ',');
        if(selector.contains(EMPTY_TAG))
            s = EMPTY_CONTEXT_NAME + s;

        return s;
    }

    @Override
    public Tags getTagCollection() {
        return ContextsSet.getInstance();
    }

    @Override
    public TagItem createItem(StringContainer tag) {
        return new TagItem(tag) {
            @Override
            public String missingItemName() {
                return EMPTY_CONTEXT_NAME;
            }

            @Override
            public boolean isFiltered(Task task) {
                return task.getContext().contains(mItem) || (mItem == EMPTY_TAG && task.getContext().isEmpty());
            }
        };
    }

    @Override
    public void putInTask(Task task) {
        DBTreeSet<StringContainer> context = getSelector(); // инициируем контекст копией селектора фильтра
        context.remove(EMPTY_TAG);                          // удалить пустое значение [без контекста], если есть
        task.setContext(context);
    }
}
