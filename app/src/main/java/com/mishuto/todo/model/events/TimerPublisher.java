package com.mishuto.todo.model.events;

import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TSystem;
import com.mishuto.todo.common.TimerFacade;

/**
 * Наблюдаемый объект, рассылающий подписчикам события таймера.
 * Наблюдатель создает свой объект TimerPublisher. Чтобы установить время, наблюдатель вызывает setTime()
 * Пример использования:
 *
 * private static class TimerListener implements TimerPublisher.TimerObserver {
 *
 *  private TimerListener(TCalendar scheduleTime) {
 *      mTimerPublisher = new TimerPublisher<>();   // создаем таймер
 *      mTimerPublisher.register(this);             // регистриуемся как подписчик
 *      mTimerPublisher.setTime(scheduleTime);      // устанавливаем время
 *  }
 *
 *  public void onTime(int timerId, TCalendar time) {
 *      // обработка таймера
 *  }
 * }
 *
 * Created by Michael Shishkin on 11.09.2017.
 */

public class TimerPublisher<T> extends ObservableSubject<TimerPublisher.TimerObserver> implements TimerFacade.Alarm {

    public interface TimerObserver extends ObservableSubject.Observer {
        void onTime(int timerId, TCalendar time);
    }

    private TimerFacade mTimerFacade;   // таймер
    private int mTimerId;               // id таймера
    private TCalendar mFiredTime;       // время установленного или сработавшего таймера

    public TimerPublisher() {
        mTimerFacade = new TimerFacade();
    }

    public int setTime(TCalendar newTime) {
        mFiredTime = newTime;
        mTimerId = mTimerFacade.setTimer(this, newTime);
        return mTimerId;
    }

    public void cancelTimer() {
        mTimerFacade.cancel();
    }

    public TCalendar getTime() {
        return mFiredTime;
    }

    public boolean isAlarmOn() {
        return mTimerFacade.getAlarmTime() != null;
    }

    @Override
    public void notifyObserver(final TimerObserver observer, Object[] params) {
        observer.onTime(mTimerId, mFiredTime);
    }

    // событие срабатывания таймера
    @Override
    public void onFire(int timerId, TCalendar time) {
        new Thread(new Runnable() {                     // событие таймера запускается в отдельном потоке, чтобы изменения БД
            @Override                                   // блокировались на транзакции основного потока (в карточке задачи), а не "встраивались" в нее
            public void run() {                         // Рекомендация: по возможности стараться операции над БД делать последними в методе - обработчике таймера
                TSystem.debug(this, "Timer created the thread #" + Thread.currentThread().getId());
                notifyAllObservers();                   // Все подписчики одного таймера обрабатываются в одном потоке (последовательно), чтобы избежать гонок
                TSystem.debug(this, "Thread #" + Thread.currentThread().getId() + " closed");
            }
        }).start();
    }
}
