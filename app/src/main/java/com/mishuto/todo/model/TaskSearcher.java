package com.mishuto.todo.model;

import com.mishuto.todo.common.TSystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mishuto.todo.common.TextUtils.contains;
import static com.mishuto.todo.common.TextUtils.isSubString;
import static com.mishuto.todo.common.TextUtils.empty;
import static com.mishuto.todo.model.TaskSearcher.MatchLevel.COMMENT_CONTENTS;
import static com.mishuto.todo.model.TaskSearcher.MatchLevel.TAGS_CONTENTS;
import static com.mishuto.todo.model.TaskSearcher.MatchLevel.TITLE_CONTENTS;
import static com.mishuto.todo.model.TaskSearcher.MatchLevel.TITLE_MATCHES;

/**
 * Реализация поиска задачи по подстроке с сортировкой списка по релевантности
 * Created by Michael Shishkin on 27.11.2018.
 */
public class TaskSearcher {

    // атрибуты поиска и одновременно уровни релевантности
    public enum MatchLevel {
        TITLE_MATCHES,      // точное соответствие запроса названию задачи (максимальная релевантность)
        TITLE_CONTENTS,     // название содержит запрос
        COMMENT_CONTENTS,   // комментарий содержит запрос
        TAGS_CONTENTS       // теги содержат запрос (минимальная релевантность)
    };

    // компаратор для сортировки по релевантности
    private static class Sorter implements Comparator<Task> {

        private Map<Task, MatchLevel> mMap;

        Sorter(Map<Task, MatchLevel> map) {
            mMap = map;
        }

        @Override
        public int compare(Task t1, Task t2) {
            TSystem.approveThat(mMap.containsKey(t1) && mMap.containsKey(t2));
            return mMap.get(t1).ordinal() - mMap.get(t2).ordinal();
        }
    }


    /**
     * Поиск задачи по подстроке
     * @param original Исходный список
     * @param query Запрос - подстрока
     * @param level Атрибуты поиска
     * @return Отсортированный по релевантности список найденных задач
     */
    public static List<Task> doSearch(List<Task> original, String query, MatchLevel level) {

        if(empty(query))
            return new ArrayList<>();

        Map<Task, MatchLevel> taskLevelMap = new HashMap<>();

        for (Task task : original)
            switch (level) {
                case TAGS_CONTENTS:     // поиск в тегах
                    if(isSubString(task.getExecutor(), query) ||
                            isSubString(task.getProject(), query) ||
                            contains(task.getContext(), query))
                        taskLevelMap.put(task, TAGS_CONTENTS);

                case COMMENT_CONTENTS:  // потом поиск в комментариях
                    if(contains(task.getComments().getList(), query.trim()))
                        taskLevelMap.put(task, COMMENT_CONTENTS);

                case TITLE_CONTENTS:    // потом поиск в названии
                    if(isSubString(task, query.trim()))
                        taskLevelMap.put(task, TITLE_CONTENTS);

                case TITLE_MATCHES:     // совпадение названия
                    if(task.toString().equalsIgnoreCase(query))
                        taskLevelMap.put(task, TITLE_MATCHES);
            }

        List<Task> selectedTasks = new ArrayList<>(taskLevelMap.keySet());  // заполняем список найденными задачами
        Collections.sort(selectedTasks, new Sorter(taskLevelMap));          // сортируем

        return selectedTasks;
    }
}

