package com.mishuto.todo.model.task_fields.recurrence;

import com.mishuto.todo.common.DateFormatWrapper;
import com.mishuto.todo.common.TCalendar;
import com.mishuto.todo.common.TextUtils;
import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.aggregators.DBArray;
import com.mishuto.todo.model.task_fields.TaskTime;

import java.util.Calendar;

import static com.mishuto.todo.common.ResourceConstants.Recurrence.WEEKLY_FACTOR_ITEMS;
import static com.mishuto.todo.common.ResourceConstants.Recurrence.WEEKLY_FACTOR_VALUES;
import static com.mishuto.todo.common.TCalendar.DAYS_IN_WEEK;
import static java.util.Calendar.SUNDAY;

/**
 * Сегмент, содержащий настройки для еженедельных повторений
 *
 * Created by Michael Shishkin on 29.11.2016.
 */
public class Week extends AbstractSegment {

    // Кратность недель
    public enum WeeklyFactor {
        EVERY_WEEK, SECOND_WEEK, THIRD_WEEK, FOURTH_WEEK;

        public int getFactor() { return  ordinal(); }

        @Override
        public String toString() {
            return WEEKLY_FACTOR_ITEMS[ordinal()];
        }
    }

    private DBArray mWeekDayArray;                                  // Массив DayOfWeek[] дней недели
    private WeeklyFactor mWeeklyFactor = WeeklyFactor.EVERY_WEEK;   // Кратность по умолчанию еженедельно

    private Week() { this(null); }

    Week(TaskTime startTime) {
        super(startTime);

        DayOfWeek[] weekDays = new DayOfWeek[DAYS_IN_WEEK];

        for (int i = 0; i < DAYS_IN_WEEK; i++) {    // создаем объекты класса DayOfWeek и присваиваем им названия
            String dayTitle = getShortWekDayName(TCalendar.getDayOfWeekByOrdinal(i));
            weekDays[i] = new DayOfWeek(dayTitle);
        }

        mWeekDayArray = new DBArray();
        mWeekDayArray.setArray(weekDays);
    }

    // вычисление следующего срабатывания
    public TCalendar getNextEvent() {
        TCalendar startDate = getStartTime();

        int dayOfWeek = startDate.get(Calendar.DAY_OF_WEEK);    // день недели стартовой даты
        int postponeDays =  getDaysToNext(dayOfWeek);           // количество дней, оставшихся до дня недели: от 1 до 7 (если день совпадает с текущим)

        if ( postponeDays == 0 )
            return null;                                        // если ничего не выбрано, возращаем null

        postponeDays += mWeeklyFactor.getFactor() * DAYS_IN_WEEK;  // прибавляем задержку в неделях

        startDate.add(Calendar.DATE, postponeDays);             // прибавляем к стартовой дате
        setTimePart(startDate);                                 // устанавливаем часы-минуты

        return startDate;
    }

    // возвращает массив дней недели
    public DayOfWeek[] getWeekDays() {
        return (DayOfWeek[]) mWeekDayArray.getArray();
    }

    public WeeklyFactor getWeeklyFactor() {
        return mWeeklyFactor;
    }

    public void setWeeklyFactor(WeeklyFactor factor) {
        mWeeklyFactor = factor;
        update();
    }

    @Override
    public void update() {
        super.update();
        mWeekDayArray.update();
    }

    // Устанавливает дефолтовый день недели, в случае если никакой день недели не был выбран
    public void setDefaultWeekDay() {
        if(getDaysToNext(SUNDAY) == 0) {                                                        // если никакие дни еще не выбраны
            int currentWeekDay = getStartTime().get(Calendar.DAY_OF_WEEK);                      // получаем стартовый день недели
            getWeekDays()[TCalendar.getOrdinalOfDayOfWeek(currentWeekDay)].setChecked(true);    // и устанавливаем его по умолчанию
        }
    }

    @Override
    public String getShortValue() {
        if(mWeeklyFactor == WeeklyFactor.EVERY_WEEK) {              // для ежедневных повторений возвращается список дней недели
            StringBuilder days = new StringBuilder();
            for(DayOfWeek dayOfWeek : getWeekDays())
                if(dayOfWeek.checked)
                    days.append(TextUtils.capitalize(dayOfWeek.title)).append(", ");

            return days.length() > 0 ? days.substring(0, days.length()-2) : null;
        }
        else
            return WEEKLY_FACTOR_VALUES[mWeeklyFactor.ordinal()];   // иначе кратность
    }

    /*
    * Вычисляет количество дней до ближайшего выбранного дня недели, начиная с startDayOfWeek
    * startDayOfWeek задается как Calendar.DayOfWeek, например Calendar.MONDAY
    * количество дней до ближайшего выбранного дня недели, начиная с startDayOfWeek
    * возращается число от 1 до 7. Если выбран только один день недели равный startDayOfWeek, то возращается 7
    * если не выбрано ничего, возращается 0
    */
    private int getDaysToNext(int startDayOfWeek) {

        int ordinal = TCalendar.getOrdinalOfDayOfWeek(startDayOfWeek);  // порядковый номер дня недели 0..6

        for (int i = 0; i < DAYS_IN_WEEK; i++)
            if (getWeekDays()[++ordinal % DAYS_IN_WEEK].checked)
                return i + 1;

        return 0;
    }

    static String getShortWekDayName(int day) {
        return (DateFormatWrapper.getDayOfWeekName(day, true)).substring(0, 2);
    }

    //Объект день недели
    public static class DayOfWeek extends DBObject {

        String title;                               // короткое название дня недели
        Boolean checked = false;                    // день недели по умолчанию не выбран

        DayOfWeek() { }

        DayOfWeek(String title) {
            this.title = title;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
            update();
        }

        public String getTitle() {
            return title;
        }
    }
}
