package com.mishuto.todo.model.events;

import com.mishuto.todo.common.ArraySet;

import static com.mishuto.todo.common.TSystem.approveThat;

/**
 * Базовый класс для наблюдаемых объектов, реализующих паттерн Наблюдатель
 * Created by Michael Shishkin on 13.09.2017.
 */

public abstract class ObservableSubject<T extends ObservableSubject.Observer> {
    public interface Observer {}    // интерфейс должен быть расширен в конкретном классе для определения метода, вызываемого в классе-наблюдателе

    private ArraySet<T> mObservers = new ArraySet<>();

    public void register(T observer) {
        approveThat(!mObservers.contains(observer), "attempt to re-register the observer");
        mObservers.add(observer);
    }

    public void unregister(T observer) {
        approveThat(mObservers.contains(observer), "attempt to unregister nonexistent observer");
        mObservers.remove(observer);
    }

    /**
     * Оповещение всех подписчиков о событии
     * Переопределяется сабклассом для передачи параметров конкретного события
     * Сабкласс не должен сохранять параметры и другое состояние между вызовами notifyAllObservers и notifyObserver,
     * если  существует обработчик notifyObserver спровоцирует новое уведомление для этого же объекта,
     * то оно перезатрет данные
     * @param parameters Параметры, которые передаются в вызов уведомления для каждого подписчика
     */
    synchronized protected void notifyAllObservers(Object ... parameters) { // notifyAllObservers должен быть синхронизирован, чтобы избежать ConcurrentModificationException
        for(T t : mObservers)                                               // Пример: если во время итерирования по команде TaskEventFacade.send(), вызванной из Task в главном потоке, сработает таймер,
            notifyObserver(t, parameters);                                  // который запустит итерацию по TaskEventFacade.send() из Task в своем потоке, то возникнет Exception.
    }

    // вызов метода уведомления наблюдателя
    protected abstract void notifyObserver(T observer, Object[] parameters);
}
