package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.model.Task;

/**
 * Интерфейс фильтров задач
 * Created by Michael Shishkin on 19.11.2017.
 */

public interface TaskFilter {
    // интерфейс события изменения фильтра
    interface OnFilterChangeListener {
        // onFilterChanged должен вызываться, если меняется состояние фильтра (включен/выключен) или поля, влияющие на фильтрацию (selector), но только для включенного фильтра
        void onFilterChanged(TaskFilter filter);
    }

    boolean isFiltered(Task task);  // прошла ли заданная задача фильтр
    boolean isFilterSet();          // включен ли фильтр
    void setFilter(boolean set);    // установить состояние фильтра: включен/выключен
    void flip();                    // изменить состояние фильтра на противоположное

    void setChangeListener(OnFilterChangeListener changeListener);  // установка слушателя события изменения фильтра
}
