package com.mishuto.todo.model.task_filters;

import com.mishuto.todo.database.DBObject;
import com.mishuto.todo.database.aggregators.DBLinkedList;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Список последних использованных фильтров
 * Created by Michael Shishkin on 18.05.2018.
 */
public class HistoryFilters extends DBObject {
    static final int MAX_SIZE = 7;  // максимальная длина списка
    private DBLinkedList<HistoryItem> mHistoryItems = new DBLinkedList<>();
    static final private HistoryFilters sInstance = new HistoryFilters();

    // элемент очереди
    public static class HistoryItem extends DBObject {
        private FilterManager mFilterManager;   // фильтр, которому принадлежит тег. Хранится enum-значение фильтра, а не сам объект фильтра, т.к. сам фильтр нам не нужен
        private Object mSelector;                // значение фильтра.

        private HistoryItem() {}

        private HistoryItem(Selectivity filter) {
            mFilterManager = FilterManager.getFilterTag(filter);
            mSelector = filter.getSelector();
        }

        public Object getSelector() {
            return mSelector;
        }

        public FilterManager getFilterManager() {
            return mFilterManager;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof HistoryItem && mFilterManager.equals(((HistoryItem) obj).mFilterManager) && mSelector.equals(((HistoryItem) obj).mSelector);
        }

        @Override
        public int hashCode() {
            return mSelector.hashCode() * 0x100 + mFilterManager.ordinal();
        }
    }

    private HistoryFilters() {
        loadAsSingleton();
    }

    public static HistoryFilters getInstance() {
        return sInstance;
    }

    // добавить фильтр в список
    public void pushFilter(Selectivity filter) {
        if(filter == null || !filter.isFilterSet() || filter.getSelector() == null)   // неустановленные фильтры и фильтры без значения - отбрасываются
            return;

        // обработка дублей для Search, чтобы последовательно набираемые/стираемые символы не сохранялись как отдельные фильтры
        if(filter == FilterManager.search() ) {
            String query = (String) filter.getSelector();
            if (query.equals(""))                                   // отбрасываем пустой запрос
                return;

            if(mHistoryItems.size() > 0) {                          // если история уже есть
                HistoryItem top = mHistoryItems.getFirst();
                                                                    // и последний сохраненный является подстрокой нового
                if(top.mFilterManager == FilterManager.SEARCH && query.startsWith(top.mSelector.toString()))
                    mHistoryItems.removeFirst();                    // удаляем последний, чтобы потом записать новый

                else if(top.mFilterManager == FilterManager.SEARCH &&  top.mSelector.toString().startsWith(query))
                    return;                                         //если новый является подстрокой, последнего - то он игнорируется
            }                                                       // таким образом, запоминается самый длинный фильтр
        }

        HistoryItem newItem = new HistoryItem(filter);

        for(HistoryItem item : mHistoryItems)   // проверяем что в списке уже нет такого же элемента
            if(item.equals(newItem)) {
                mHistoryItems.remove(item);     // но если есть - удаляем, чтобы новый элемент всегда был на вершине списка
                break;
            }

        mHistoryItems.addFirst(newItem);
        if(mHistoryItems.size() > MAX_SIZE)
            mHistoryItems.removeLast();
    }

    // получить список последних фильтров
    // если используемый тег в фильтре отсутствует, то элемент удаляется из списка
    public LinkedList<HistoryItem> getHistory() {
        for (Iterator<HistoryItem> iterator = mHistoryItems.listIterator(); iterator.hasNext();) {
            HistoryItem item = iterator.next();
            if(item.getFilterManager().filter() instanceof Choice)
                //noinspection unchecked
                if(!((Choice) item.getFilterManager().filter()).canBeChosen(item.getSelector()))
                    iterator.remove();
        }
        return mHistoryItems.getShallowCopy();
    }
}
