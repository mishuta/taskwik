package com.mishuto.todo;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.ApplicationErrorReport;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mishuto.todo.common.TLog;
import com.mishuto.todo.model.Task;
import com.mishuto.todo.model.TaskList;
import com.mishuto.todo.view_model.common.LoadCheckerActivity;
import com.mishuto.todo.view_model.common.TaskEventHandler;

/**
 * Класс, расширяющий Application, служит для сохранения Application Context, для доступа к нему из любого места
 * Класс определен в AndroidMainfest
 * Используется в DBOpenHelper
 * Created by Michael Shishkin on 15.07.2017.
 */

public class App extends Application {
    @SuppressWarnings("FieldCanBeLocal")
    private static boolean TEST_MODE = false; // флаг режима тестирования (устанавливается в тестах)

    @SuppressLint("StaticFieldLeak")
    private static App sInstance;
    private boolean isFirstTime;            // флаг: является ли этот запуск первым после инсталяции приложения

    // Инициализация приложения.
    // Здесь инициируются объекты, используемые как внутри активностей, так и внутри ресиверов интентов
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;                                                                                   // сохраняем App для доступа к нему из любой точки приложения
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());                                  // обработчик непойманых Exception

        TaskEventHandler.create();                                                                          // создаем обработчик событий задач
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }

    public static App get() {
        return sInstance;
    }

    // возвращает имя тега вида "com.mishuto.to_do.%tag%" для параметров Intent.putExtra(tag, ...)
    public static String createTagName(String tag) {
        return App.class.getPackage().getName() + "." + tag;
    }

    public boolean isFirstTime() {
        return isFirstTime;
    }

    // рестарт приложения
    // служит для полной очистки всех статических контекстов
    public static void restartApplication(Context activity) {
        if(TEST_MODE) return;

        for(Task t: TaskList.getTasks())                                       // сначала уничтожаем все таймеры, чтобы не было рассинхронизации нотификаций с БД
            t.getTimer().cancelTimer();

        activity.startActivity(new Intent(activity, LoadCheckerActivity.class));// создаем интент на рестарт приложения
        System.exit(0);                                                         // завершаем приложение
    }

    // дефолтный обработчик всех необработанных исключений
    // если включена запись лога в файл,
    // то при получении эксепшена, он сохраняется в логе, после чего управление передается штатному обработчику андроида.
    private static class ExceptionHandler implements Thread.UncaughtExceptionHandler {
        Thread.UncaughtExceptionHandler mDefault = Thread.getDefaultUncaughtExceptionHandler(); // дефолтный обработчик

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            try {                                               // блок try необходим, чтобы избежать возможного зацикливания вызовов uncaughtException()
                TLog.getInstance().logUnhandledException(e);    // сообщаем логу об эксепшене

                // передаем данные в консоль маркета
                String packageName = sInstance.getPackageName();

                ApplicationErrorReport report = new ApplicationErrorReport();
                report.installerPackageName   = sInstance.getPackageManager().getInstallerPackageName(packageName);
                report.packageName = packageName;
                report.processName = packageName;
                report.time        = System.currentTimeMillis();
                report.systemApp   = false;
                report.type        = ApplicationErrorReport.TYPE_CRASH;
                report.crashInfo   = new ApplicationErrorReport.CrashInfo(e);

                sInstance.startActivity(new Intent(Intent.ACTION_APP_ERROR)
                        .putExtra(Intent.EXTRA_BUG_REPORT, report)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

            }
            catch (Throwable throwable) {
                Log.e("uncaughtException", e.getMessage());
            }
            if(mDefault != null)
                mDefault.uncaughtException(t, e);
        }
    }
}