DROP table if exists HISTORY_ITEM;
DROP table if exists HISTORY_FILTERS;
DROP table if exists CONTEXT_FILTER;
DROP table if exists EXECUTOR_FILTER;
DROP table if exists PROJECT_FILTER;